package edu.mst.concurrent;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import edu.mst.db.util.JdbcConnectionPool;

/**
 * 
 * @author gjs
 * @deprecated
 * @param <E>
 */
public abstract class BlockingQueueWorker<E> implements AutoCloseable {

    protected JdbcConnectionPool connPool = null;
    private CallableBlockingQueue<E> blockingQ = null;
    private static int nbrOfProcessors;
    private static ExecutorService pool;
    private CompletionService<E> svc;
    protected Map<UUID, Future<E>> futuresByCallableUuid = new HashMap<UUID, Future<E>>();

    public BlockingQueueWorker(int qSize, JdbcConnectionPool connPool) {
	this.connPool = connPool;
	blockingQ = new CallableBlockingQueue<E>(qSize);
	nbrOfProcessors = Runtime.getRuntime().availableProcessors();
	pool = Executors.newFixedThreadPool(nbrOfProcessors *= 3);
	svc = new ExecutorCompletionService<E>(pool);
    }

    /**
     * Blocks submission of callable to thread pool until room available in
     * queue
     * 
     * @param callable
     * @throws InterruptedException
     */
    public void submit(KeyedCallable<E> callable) throws InterruptedException {
	blockingQ.put(callable);
	Future<E> future = svc.submit(callable);
	futuresByCallableUuid.put(callable.getUuid(), future);
    }

    /**
     * Removes callable from queue when thread finished
     * 
     * @param callable
     * @throws InterruptedException
     */
    public void done(KeyedCallable<E> callable) throws InterruptedException {
	blockingQ.take(callable);
	futuresByCallableUuid.remove(callable.getUuid());
    }

    /*
     * Stops in-progress thread
     */
    public void kill(KeyedCallable<E> callable) {
	if (futuresByCallableUuid.containsKey(callable.getUuid())) {
	    Future<E> future = futuresByCallableUuid.get(callable.getUuid());
	    future.cancel(true);
	}
    }

    public abstract void run() throws Exception;

    /*
     * Process callable results
     */
    public abstract void callableFinished(KeyedCallable<E> callable)
	    throws Exception;

    protected void shutdownAndAwaitTermination(ExecutorService pool) {
	// pool.shutdown(); // Disable new tasks from being submitted
	pool.shutdownNow();
	try {
	    // Wait a while for existing tasks to terminate
	    if (!pool.awaitTermination(5, TimeUnit.SECONDS)) {
		pool.shutdownNow(); // Cancel currently executing tasks
		// Wait a while for tasks to respond to being cancelled
		if (!pool.awaitTermination(15, TimeUnit.SECONDS))
		    System.err.println("Pool did not terminate");
	    }
	} catch (InterruptedException ie) {
	    // (Re-)Cancel if current thread also interrupted
	    pool.shutdownNow();
	    // Preserve interrupt status
	    Thread.currentThread().interrupt();
	}
    }

    @Override
    public void close() throws Exception {
	this.shutdownAndAwaitTermination(pool);
	System.out.println("Parallel processor closed");
    }

}

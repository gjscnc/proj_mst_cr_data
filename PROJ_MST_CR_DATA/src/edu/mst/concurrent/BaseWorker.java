/**
 * 
 */
package edu.mst.concurrent;

import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import edu.mst.db.util.Constants;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * @author gjs
 *
 */
public abstract class BaseWorker<E> implements AutoCloseable {

    protected JdbcConnectionPool connPool;
    
    protected static int processors;
    protected static ExecutorService pool;
    protected CompletionService<E> svc;
    
    protected int threadCallablesBatchSize = Constants.defaultThreadCallablesBatchSize;
    protected int currentPos = 0;
    protected int maxPos = 0;
    protected int startPos = 0;

    public BaseWorker(JdbcConnectionPool connPool) {
	this.connPool = connPool;
	processors = Runtime.getRuntime().availableProcessors();
	processors *= 3;
	pool = Executors.newFixedThreadPool(processors);
	svc = new ExecutorCompletionService<E>(pool);
    }
    
    public BaseWorker(int threadCallablesBatchSize, JdbcConnectionPool connPool) {
	this.connPool = connPool;
	this.threadCallablesBatchSize = threadCallablesBatchSize;
	processors = Runtime.getRuntime().availableProcessors();
	processors *= 3;
	pool = Executors.newFixedThreadPool(processors);
	svc = new ExecutorCompletionService<E>(pool);
    }

    public abstract void run() throws Exception;
    
    protected abstract int getNextBatchMaxPos() throws Exception;
    
    protected void shutdownAndAwaitTermination(ExecutorService pool) {
	// pool.shutdown(); // Disable new tasks from being submitted
	pool.shutdownNow();
	try {
	    // Wait a while for existing tasks to terminate
	    if (!pool.awaitTermination(5, TimeUnit.SECONDS)) {
		pool.shutdownNow(); // Cancel currently executing tasks
		// Wait a while for tasks to respond to being cancelled
		if (!pool.awaitTermination(15, TimeUnit.SECONDS))
		    System.err.println("Pool did not terminate");
	    }
	} catch (InterruptedException ie) {
	    // (Re-)Cancel if current thread also interrupted
	    pool.shutdownNow();
	    // Preserve interrupt status
	    Thread.currentThread().interrupt();
	}
    }

    @Override
    public void close() throws Exception {
	this.shutdownAndAwaitTermination(pool);
	System.out.println("Parallel processor closed");
    }
    
}

/**
 * 
 */
package edu.mst.concurrent;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * A fixed size blocking queue to aid in maximizing CPU time and simplifying
 * submitting callables.
 * <p>
 * Buffers available Callables with same blocking
 * {@link java.util.concurrent.LinkedBlockingQueue#put} and
 * {@link java.util.concurrent.LinkedBlockingQueue#take} methods.
 * 
 * @deprecated
 * @author gjs
 *
 */
public class CallableBlockingQueue<E> {

    private BlockingQueue<KeyedCallable<E>> callableQ = null;
    private Map<UUID, KeyedCallable<E>> callableByUuid = new HashMap<UUID, KeyedCallable<E>>();

    public CallableBlockingQueue(int qSize) {
	callableQ = new LinkedBlockingQueue<KeyedCallable<E>>(qSize);
    }

    /**
     * Blocks until new capacity available
     * 
     * @param callable
     * @return
     * @throws InterruptedException
     */
    public boolean put(KeyedCallable<E> callable) throws InterruptedException {
	callableQ.put(callable); // blocks
	callableByUuid.put(callable.getUuid(), callable);
	return true;
    }

    /**
     * Removes callable from queue when thread finished
     * 
     * @return
     * @throws InterruptedException
     */
    public KeyedCallable<E> take(KeyedCallable<E> callable)
	    throws InterruptedException {
	callableQ.remove(callable);
	callableByUuid.remove(callable.getUuid());
	return callable;
    }

}

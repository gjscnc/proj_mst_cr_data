/**
 * 
 */
package edu.mst.concurrent;

import java.util.UUID;
import java.util.concurrent.Callable;

/**
 * Extends Callable for tracking thread progress and maximizing CPU usage.
 * 
 * @deprecated
 * @author gjs
 *
 */
public abstract class KeyedCallable<E>
	implements Callable<E>, Comparable<KeyedCallable<E>> {

    protected UUID uuid = null;
    protected E result = null;
    protected BlockingQueueWorker<E> worker = null;

    public KeyedCallable(E result, BlockingQueueWorker<E> worker) {
	uuid = UUID.randomUUID();
	this.result = result;
	this.worker = worker;
    }

    @Override
    public int compareTo(KeyedCallable<E> callable) {
	return uuid.compareTo(callable.uuid);
    }

    @Override
    public boolean equals(Object obj) {
	if (obj instanceof KeyedCallable<?>) {
	    KeyedCallable<?> callable = (KeyedCallable<?>) obj;
	    return uuid.equals(callable.uuid);
	}
	return false;
    }

    @Override
    public int hashCode() {
	return uuid.hashCode();
    }

    @Override
    public abstract E call() throws Exception;

    public UUID getUuid() {
	return uuid;
    }
    
    public E getResult() {
	return result;
    }

}

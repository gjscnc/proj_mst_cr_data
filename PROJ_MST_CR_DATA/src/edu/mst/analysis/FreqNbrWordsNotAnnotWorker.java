package edu.mst.analysis;

import java.io.FileOutputStream;
import java.util.Iterator;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import edu.mst.concurrent.BaseWorker;
import edu.mst.db.nlm.goldstd.corpora.annot.GoldStdTag;
import edu.mst.db.nlm.goldstd.corpora.annot.GoldStdTagDao;
import edu.mst.db.util.ConnDbName;
import edu.mst.db.util.JdbcConnectionPool;

public class FreqNbrWordsNotAnnotWorker extends BaseWorker<Integer> {

    private static final String csvDirName = "/home/gjs/Documents/MST/PhD/Publications/IOC/Submission3/Resubmission_2020_October/";
    private static final String csvFileName = "FreqNbrWordsNotAnnotatedInPhrase_20201018.xlsx";

    private int nbrTags = 0;
    private GoldStdTag[] tags;

    private int nbrTagsProcessed = 0;
    private int rptInterval = 500;

    private SortedMap<Integer, Integer> freqByNbrWordsNotTagged = new TreeMap<Integer, Integer>();

    public FreqNbrWordsNotAnnotWorker(int threadCallablesBatchSize,
	    JdbcConnectionPool connPool) {
	super(threadCallablesBatchSize, connPool);
    }

    @Override
    public void run() throws Exception {

	// get all tags
	{
	    System.out.println("Marshaling all tags where CUI is not null");
	    GoldStdTagDao tagDao = new GoldStdTagDao();
	    SortedSet<GoldStdTag> tagSet = tagDao.marshalCuiNotNull(connPool);
	    nbrTags = tagSet.size();
	    tags = new GoldStdTag[nbrTags];
	    Iterator<GoldStdTag> tagIter = tagSet.iterator();
	    int pos = 0;
	    while (tagIter.hasNext()) {
		GoldStdTag tag = tagIter.next();
		tags[pos] = tag;
		pos++;
	    }
	    System.out.printf("Marshaled %1$,d tags. %n", nbrTags);
	}

	System.out.println(
		"Now computing nbr words not tagged per phrase, and compiling frequences");

	while (currentPos < nbrTags) {

	    maxPos = getNextBatchMaxPos();
	    startPos = currentPos;

	    for (int i = startPos; i <= maxPos; i++) {
		GoldStdTag tag = tags[i];
		FreqNbrWordsNotAnnotCallable callable = new FreqNbrWordsNotAnnotCallable(
			tag, connPool);
		svc.submit(callable);
	    }

	    for (int i = startPos; i <= maxPos; i++) {
		Integer nbrWordsNotTagged = svc.take().get();
		if (nbrWordsNotTagged >= 0) {
		    Integer freq = 0;
		    if (freqByNbrWordsNotTagged
			    .containsKey(nbrWordsNotTagged)) {
			freq = freqByNbrWordsNotTagged.get(nbrWordsNotTagged);
		    }
		    freq++;
		    freqByNbrWordsNotTagged.put(nbrWordsNotTagged, freq);
		}
		nbrTagsProcessed++;
		if (nbrTagsProcessed % rptInterval == 0) {
		    System.out.printf("Processed %1$,d tags. %n",
			    nbrTagsProcessed);
		}
	    }

	    currentPos = maxPos + 1;

	}

	System.out.printf("Processed grand total of %1$,d tags. %n%n",
		nbrTagsProcessed);

	Iterator<Integer> nbrWordsIter = freqByNbrWordsNotTagged.keySet()
		.iterator();
	while (nbrWordsIter.hasNext()) {
	    Integer nbrWordsNotTagged = nbrWordsIter.next();
	    Integer freq = freqByNbrWordsNotTagged.get(nbrWordsNotTagged);
	    System.out.printf("%1$d - %2$,d %n", nbrWordsNotTagged.intValue(),
		    freq.intValue());
	}

	System.out.println("Now exporting to Excel CSV file");

	try (Workbook workbook = new XSSFWorkbook()) {
	    Sheet sheet = workbook.createSheet("WordsInPhraseNotAnnot");

	    // header
	    int rowNbr = 0;
	    int cellNbr = 0;
	    Row hdrRow = sheet.createRow(rowNbr++);
	    Cell nbrMissingHdr = hdrRow.createCell(cellNbr++);
	    nbrMissingHdr.setCellValue("NbrWordsNotAnnoted");
	    Cell freqHdr = hdrRow.createCell(cellNbr++);
	    freqHdr.setCellValue("freq");

	    nbrWordsIter = null;
	    nbrWordsIter = freqByNbrWordsNotTagged.keySet().iterator();
	    while (nbrWordsIter.hasNext()) {
		Integer nbrWordsNotTagged = nbrWordsIter.next();
		Integer freq = freqByNbrWordsNotTagged.get(nbrWordsNotTagged);
		cellNbr = 0;
		Row row = sheet.createRow(rowNbr++);
		Cell nbrMissCell = row.createCell(cellNbr++);
		nbrMissCell.setCellValue(nbrWordsNotTagged.intValue());
		Cell freqCell = row.createCell(cellNbr++);
		freqCell.setCellValue(freq.intValue());
	    }

	    try (FileOutputStream fos = new FileOutputStream(
		    csvDirName + csvFileName)) {
		workbook.write(fos);
	    }

	}

    }

    @Override
    protected int getNextBatchMaxPos() throws Exception {
	// subtract 1 from max pos since positions are inclusive
	int maxPos = currentPos + threadCallablesBatchSize - 1;
	if (maxPos >= nbrTags - 1)
	    maxPos = nbrTags - 1;
	return maxPos;
    }

    public static void main(String[] args) {

	try {

	    JdbcConnectionPool connPool = new JdbcConnectionPool(
		    ConnDbName.CONCEPT_RECOGN);

	    int threadCallablesBatchSize = 24;
	    try (FreqNbrWordsNotAnnotWorker worker = new FreqNbrWordsNotAnnotWorker(
		    threadCallablesBatchSize, connPool)) {
		worker.run();
	    }

	} catch (Exception ex) {
	    ex.printStackTrace();
	}

    }

}

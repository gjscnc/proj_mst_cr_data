package edu.mst.analysis;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.concurrent.Callable;

import edu.mst.db.nlm.goldstd.corpora.GoldStdPhrase;
import edu.mst.db.nlm.goldstd.corpora.GoldStdPhraseDao;
import edu.mst.db.nlm.goldstd.corpora.GoldStdPhraseWord;
import edu.mst.db.nlm.goldstd.corpora.GoldStdPhraseWordDao;
import edu.mst.db.nlm.goldstd.corpora.annot.GoldStdTag;
import edu.mst.db.nlm.goldstd.corpora.annot.GoldStdTagWord;
import edu.mst.db.nlm.goldstd.corpora.annot.GoldStdTagWordDao;
import edu.mst.db.util.JdbcConnectionPool;

public class FreqNbrWordsNotAnnotCallable implements Callable<Integer> {
    
    private GoldStdTag tag;
    private JdbcConnectionPool connPool;
    
    private GoldStdPhraseDao phraseDao = new GoldStdPhraseDao();
    private GoldStdPhraseWordDao phraseWordDao = new GoldStdPhraseWordDao();
    private GoldStdTagWordDao tagWordDao = new GoldStdTagWordDao();
    
    public FreqNbrWordsNotAnnotCallable(GoldStdTag tag, JdbcConnectionPool connPool) {
	this.tag = tag;
	this.connPool = connPool;
    }

    @Override
    public Integer call() throws Exception {
	
	int nbrWordsNotTagged = 0;
	
	SortedSet<GoldStdTagWord> tagWords = tagWordDao.marshal(tag.getPmid(),
		tag.getTagId(), connPool);
	if (tagWords.size() == 0) {
	    return -1;
	}

	GoldStdTagWord firstWord = tagWords.first();
	int pmid = firstWord.getPmid();
	int sentNbr = firstWord.getSentNbr();
	int sentWordNbr = firstWord.getSentWordNbr();
	
	Set<Integer> tagSentWordNbrs = new HashSet<Integer>();
	Iterator<GoldStdTagWord> tagWordIter = tagWords.iterator();
	while(tagWordIter.hasNext()) {
	    GoldStdTagWord tagWord = tagWordIter.next();
	    tagSentWordNbrs.add(tagWord.getSentWordNbr());
	}

	GoldStdPhrase phrase = phraseDao.retrievePhraseAtSentWordPos(
		pmid, sentNbr, sentWordNbr, connPool);
	if(phrase == null) {
	    return -1;
	}
	phraseWordDao.marshalWordsForPhrase(phrase, connPool);
	List<GoldStdPhraseWord> phraseWords = phrase.getWords();
	Iterator<GoldStdPhraseWord> phraseWordIter = phraseWords.iterator();
	while(phraseWordIter.hasNext()) {
	    GoldStdPhraseWord phraseWord = phraseWordIter.next();
	    if(tagSentWordNbrs.contains(phraseWord.getSentWordNbr())) {
		continue;
	    }
	    nbrWordsNotTagged++;
	}

	return nbrWordsNotTagged;
    }

}

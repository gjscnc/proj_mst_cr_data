/**
 * 
 */
package edu.mst.analysis;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import edu.mst.db.nlm.goldstd.corpora.GoldStdAbstractDao;
import edu.mst.db.nlm.goldstd.corpora.GoldStdPhrase;
import edu.mst.db.nlm.goldstd.corpora.GoldStdPhraseDao;
import edu.mst.db.nlm.goldstd.corpora.GoldStdPhraseWord;
import edu.mst.db.nlm.goldstd.corpora.GoldStdPhraseWordDao;
import edu.mst.db.nlm.goldstd.corpora.annot.GoldStdTag;
import edu.mst.db.nlm.goldstd.corpora.annot.GoldStdTagDao;
import edu.mst.db.nlm.goldstd.corpora.annot.GoldStdTagWord;
import edu.mst.db.nlm.goldstd.corpora.annot.GoldStdTagWordDao;
import edu.mst.db.util.ConnDbName;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * Computes histogram of number of words in a phrase that are not annotated in
 * the NLM gold standard data.
 * <p>
 * Includes exporting comma-delimited file, suitable for Excel, with results.
 * 
 * @author gjs
 *
 */
public class FreqWordsNotAnnotatedInPhrase {
    
    private static final String csvDirName = "/home/gjs/Documents/MST/PhD/Publications/IOC/Submission3/Resubmission_2020_October/";
    private static final String csvFileName = "FreqNbrWordsNotAnnotatedInPhrase_20201018.xlsx";

    private JdbcConnectionPool connPool;
    private GoldStdPhraseDao phraseDao = new GoldStdPhraseDao();
    private GoldStdPhraseWordDao phraseWordDao = new GoldStdPhraseWordDao();
    private GoldStdTagDao tagDao = new GoldStdTagDao();
    private GoldStdTagWordDao tagWordDao = new GoldStdTagWordDao();

    public FreqWordsNotAnnotatedInPhrase(JdbcConnectionPool connPool) {
	this.connPool = connPool;
    }

    public void extractFreq() throws SQLException, IOException {

	int[] goldStdAbstrPmids = GoldStdAbstractDao.getAllPmids(connPool);
	System.out.printf("Retrieved %1$,d abstract PMIDs. %n",
		goldStdAbstrPmids.length);

	System.out.println("Marshaling gold standard tags");
	int nbrTagsMarshaled = 0;
	Map<Integer, SortedSet<String>> tagIdsByPmid = new HashMap<Integer, SortedSet<String>>();
	for (int i = 0; i < goldStdAbstrPmids.length; i++) {
	    Integer pmid = goldStdAbstrPmids[i];
	    SortedSet<GoldStdTag> tags = tagDao.marshal(pmid, connPool);
	    if (tags.size() == 0) {
		continue;
	    }
	    SortedSet<String> tagIds = new TreeSet<String>();
	    for (GoldStdTag tag : tags) {
		tagIds.add(tag.getTagId());
		nbrTagsMarshaled++;
	    }
	    tagIdsByPmid.put(pmid, tagIds);
	}
	System.out.printf("Marshaled %1$,d tags. %n", nbrTagsMarshaled);

	System.out.println("Now computing frequency by size of phrase");
	SortedMap<Integer, Integer> freqByNbrWordsNotTaggedInPhrase = new TreeMap<Integer, Integer>();
	Iterator<Integer> pmidIter = tagIdsByPmid.keySet().iterator();
	while (pmidIter.hasNext()) {
	    Integer pmid = pmidIter.next();
	    SortedSet<String> tagIds = tagIdsByPmid.get(pmid);
	    Iterator<String> tagIdIter = tagIds.iterator();
	    while (tagIdIter.hasNext()) {
		String tagId = tagIdIter.next();
		SortedSet<GoldStdTagWord> tagWords = tagWordDao.marshal(pmid,
			tagId, connPool);
		if (tagWords.size() == 0) {
		    continue;
		}
		Set<Integer> tagWordPositions = new HashSet<Integer>();
		Iterator<GoldStdTagWord> tagWordIter = tagWords.iterator();
		while(tagWordIter.hasNext()) {
		    GoldStdTagWord tagWord = tagWordIter.next();
		    tagWordPositions.add(tagWord.getSentWordNbr());
		}
		GoldStdTagWord firstTagWord = tagWords.first();
		int sentNbr = firstTagWord.getSentNbr();
		int sentWordNbr = firstTagWord.getSentWordNbr();
		GoldStdPhrase phrase = phraseDao.retrievePhraseAtSentWordPos(
			pmid, sentNbr, sentWordNbr, connPool);
		phraseWordDao.marshalWordsForPhrase(phrase, connPool.getConnection());
		List<GoldStdPhraseWord> phraseWords = phrase.getWords();
		Iterator<GoldStdPhraseWord> phraseWordIter = phraseWords.iterator();
		int nbrWordsNotTagged = 0;
		while(phraseWordIter.hasNext()) {
		    GoldStdPhraseWord phraseWord = phraseWordIter.next();
		    if(tagWordPositions.contains(phraseWord.getSentWordNbr())) {
			continue;
		    }
		    nbrWordsNotTagged++;
		}
		Integer freq = 0;
		if (freqByNbrWordsNotTaggedInPhrase.containsKey(nbrWordsNotTagged)) {
		    freq = freqByNbrWordsNotTaggedInPhrase.get(nbrWordsNotTagged);
		}
		freq++;
		freqByNbrWordsNotTaggedInPhrase.put(nbrWordsNotTagged, freq);
	    }
	}

	Iterator<Integer> phraseSizeIter = freqByNbrWordsNotTaggedInPhrase.keySet()
		.iterator();
	while (phraseSizeIter.hasNext()) {
	    Integer phraseSize = phraseSizeIter.next();
	    Integer freq = freqByNbrWordsNotTaggedInPhrase.get(phraseSize);
	    System.out.printf("%1$,d, %2$,d %n", phraseSize.intValue(),
		    freq.intValue());
	}
	
	try (Workbook workbook = new XSSFWorkbook()) {
	    Sheet sheet = workbook.createSheet("FreqNbrWordsNotAnnotatedInPhrase");
	    
	}

    }

    /**
     * @param args
     */
    public static void main(String[] args) {

	try {

	    JdbcConnectionPool connPool = new JdbcConnectionPool(
		    ConnDbName.CONCEPT_RECOGN);

	    FreqWordsNotAnnotatedInPhrase freq = new FreqWordsNotAnnotatedInPhrase(
		    connPool);
	    freq.extractFreq();

	} catch (Exception ex) {
	    ex.printStackTrace();
	}

    }

}

-- conceptrecogn.mrconso definition

CREATE TABLE `mrconso` (
  `CUI` char(8) CHARACTER SET utf16 COLLATE utf16_bin NOT NULL,
  `LAT` char(3) CHARACTER SET utf16 COLLATE utf16_bin NOT NULL,
  `TS` char(1) CHARACTER SET utf16 COLLATE utf16_bin NOT NULL,
  `LUI` varchar(10) CHARACTER SET utf16 COLLATE utf16_bin NOT NULL,
  `STT` varchar(3) CHARACTER SET utf16 COLLATE utf16_bin NOT NULL,
  `SUI` varchar(10) CHARACTER SET utf16 COLLATE utf16_bin NOT NULL,
  `ISPREF` char(1) CHARACTER SET utf16 COLLATE utf16_bin NOT NULL,
  `AUI` varchar(9) CHARACTER SET utf16 COLLATE utf16_bin NOT NULL,
  `SAUI` varchar(50) CHARACTER SET utf16 COLLATE utf16_bin DEFAULT NULL,
  `SCUI` varchar(100) CHARACTER SET utf16 COLLATE utf16_bin DEFAULT NULL,
  `SDUI` varchar(100) CHARACTER SET utf16 COLLATE utf16_bin DEFAULT NULL,
  `SAB` varchar(40) CHARACTER SET utf16 COLLATE utf16_bin NOT NULL,
  `TTY` varchar(40) CHARACTER SET utf16 COLLATE utf16_bin NOT NULL,
  `CODE` varchar(100) CHARACTER SET utf16 COLLATE utf16_bin NOT NULL,
  `STR` text CHARACTER SET utf16 COLLATE utf16_bin NOT NULL,
  `SRL` int unsigned NOT NULL,
  `SUPPRESS` char(1) CHARACTER SET utf16 COLLATE utf16_bin NOT NULL,
  `CVF` int unsigned DEFAULT NULL,
  PRIMARY KEY (`AUI`),
  KEY `mrconso_SAUI_IDX` (`SAUI`) USING BTREE,
  KEY `mrconso_CUI_IDX` (`CUI`) USING BTREE,
  KEY `mrconso_SCUI_IDX` (`SCUI`) USING BTREE,
  KEY `mrsonso_STR_IDX` (`STR`(250)) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf16 COLLATE=utf16_bin;
-- conceptrecogn.stopwords definition
CREATE TABLE `stopwords`
(
   `stopword` varchar (100) CHARACTER SET utf16 COLLATE utf16_bin NOT NULL,
   `baseWordId` int NOT NULL,
   `variantWordId` int NOT NULL,
   PRIMARY KEY (`stopword`),
   KEY `stopwords_baseid_idx` (`baseWordId`),
   KEY `stopwords_variantid_idx` (`variantWordId`)
)
ENGINE= MyISAM DEFAULT CHARSET= utf16 COLLATE= utf16_bin;
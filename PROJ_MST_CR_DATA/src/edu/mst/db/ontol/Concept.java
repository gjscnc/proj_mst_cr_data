/*
=================================================================================
Copyright (c) 2010 RAI 
All rights reserved. 
=================================================================================
 */
package edu.mst.db.ontol;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import edu.mst.db.util.CondProbFreqType;
import edu.mst.db.lexicon.LexWord;

/**
 * @author RAI
 * 
 */
public class Concept implements Comparable<Concept> {

    private long uid;
    private String cui;
    private String aui;
    private boolean isRelType = false;
    private boolean isLeaf = false;
    private boolean isRoot = false;
    private String fullySpecifiedName;
    private Set<Concept> rootConcepts = new HashSet<Concept>();
    private Set<Relationship> incomingRelationships = new HashSet<Relationship>();
    private Set<Relationship> outgoingRelationships = new HashSet<Relationship>();
    private List<LexWord> baseWordByPosition = new ArrayList<LexWord>();
    private Map<LexWord, Set<Integer>> positionsByBaseWord = new HashMap<LexWord, Set<Integer>>();
    private Map<LexWord, Map<CondProbFreqType, Double>> lnOntolCogencyByPredicateAndType = new HashMap<LexWord, Map<CondProbFreqType, Double>>();

    @Override
    public int compareTo(Concept concept) {
	return Long.compare(uid, concept.uid);
    }

    @Override
    public boolean equals(Object obj) {
	if (obj instanceof Concept) {
	    Concept concept = (Concept) obj;
	    return concept.uid == uid;
	}
	return false;
    }

    @Override
    public int hashCode() {
	return Long.valueOf(uid).hashCode();
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public String getCui() {
        return cui;
    }

    public void setCui(String cui) {
        this.cui = cui;
    }

    public String getAui() {
        return aui;
    }

    public void setAui(String aui) {
        this.aui = aui;
    }

    public boolean isRelType() {
        return isRelType;
    }

    public void setIsRelType(boolean isRelType) {
        this.isRelType = isRelType;
    }

    public boolean isLeaf() {
        return isLeaf;
    }

    public void setIsLeaf(boolean isLeaf) {
        this.isLeaf = isLeaf;
    }

    public boolean isRoot() {
        return isRoot;
    }

    public void setIsRoot(boolean isRoot) {
        this.isRoot = isRoot;
    }

    public String getFullySpecifiedName() {
        return fullySpecifiedName;
    }

    public void setFullySpecifiedName(String fullySpecifiedName) {
        this.fullySpecifiedName = fullySpecifiedName;
    }

    public Set<Concept> getRootConcepts() {
        return rootConcepts;
    }

    public void setRootConcepts(Set<Concept> rootConcepts) {
        this.rootConcepts = rootConcepts;
    }

    public Set<Relationship> getIncomingRelationships() {
        return incomingRelationships;
    }

    public void setIncomingRelationships(Set<Relationship> incomingRelationships) {
        this.incomingRelationships = incomingRelationships;
    }

    public Set<Relationship> getOutgoingRelationships() {
        return outgoingRelationships;
    }

    public void setOutgoingRelationships(Set<Relationship> outgoingRelationships) {
        this.outgoingRelationships = outgoingRelationships;
    }

    public List<LexWord> getBaseWordByPosition() {
        return baseWordByPosition;
    }

    public void setBaseWordByPosition(List<LexWord> baseWordByPosition) {
        this.baseWordByPosition = baseWordByPosition;
    }

    public Map<LexWord, Set<Integer>> getPositionsByBaseWord() {
        return positionsByBaseWord;
    }

    public void setPositionsByBaseWord(
    	Map<LexWord, Set<Integer>> positionsByBaseWord) {
        this.positionsByBaseWord = positionsByBaseWord;
    }

    public Map<LexWord, Map<CondProbFreqType, Double>> getLnOntolCogencyByPredicateAndType() {
        return lnOntolCogencyByPredicateAndType;
    }

    public void setLnOntolCogencyByPredicateAndType(
    	Map<LexWord, Map<CondProbFreqType, Double>> lnOntolCogencyByPredicateAndType) {
        this.lnOntolCogencyByPredicateAndType = lnOntolCogencyByPredicateAndType;
    }

}

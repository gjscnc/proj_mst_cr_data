/**
 * Components to parse concept names and compute cogency-related values
 */
/**
 * @author gjs
 *
 */
package edu.mst.db.ontol;
/**
 * 
 */
package edu.mst.db.ontol;

/**
 * @author RAI
 * 
 */
public class Relationship implements Comparable<Relationship> {

    private long sourceUid;
    private long fromConceptUid;
    private long toConceptUid;
    private long relTypeUid;

    public static long getSourceIdFromRui(String rui){
	return Long.valueOf(rui.substring(1));
    }
    
    @Override
    public int compareTo(Relationship rel) {
	return Long.valueOf(rel.sourceUid).compareTo(Long.valueOf(sourceUid));
    }
    
    @Override
    public boolean equals(Object obj){
	if(obj instanceof Relationship){
	    Relationship rel = (Relationship)obj;
	    return rel.sourceUid == sourceUid;
	}
	return false;
    }
    
    @Override
    public int hashCode(){
	return Long.valueOf(sourceUid).hashCode();
    }

    public long getSourceUid() {
        return sourceUid;
    }

    public void setSourceUid(long sourceUid) {
        this.sourceUid = sourceUid;
    }

    public long getFromConceptUid() {
        return fromConceptUid;
    }

    public void setFromConceptUid(long fromConceptUid) {
        this.fromConceptUid = fromConceptUid;
    }

    public long getToConceptUid() {
        return toConceptUid;
    }

    public void setToConceptUid(long toConceptUid) {
        this.toConceptUid = toConceptUid;
    }

    public long getRelTypeUid() {
        return relTypeUid;
    }

    public void setRelTypeUid(long relTypeUid) {
        this.relTypeUid = relTypeUid;
    }

}

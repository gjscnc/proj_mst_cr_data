package edu.mst.db.ontol;

import java.util.HashSet;

public abstract class Allowable {

    protected HashSet<Integer> allowableStatus = new HashSet<Integer>();;

    public boolean allowable(Integer status) {
	if (allowableStatus.contains(status)) {
	    return true;
	} else {
	    return false;
	}
    }

}

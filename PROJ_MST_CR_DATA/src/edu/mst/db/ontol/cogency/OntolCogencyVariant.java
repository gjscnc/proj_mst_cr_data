/**
 * 
 */
package edu.mst.db.ontol.cogency;

import java.util.Comparator;

/**
 * Maximum cogency for a concept and predicate word combination.
 * <p>
 * One instance for each predicate word in the concept name.
 * 
 * @author gjs
 *
 */
public class OntolCogencyVariant implements Comparable<OntolCogencyVariant> {

    private long conceptNameUid = 0L;
    private long conceptUid = 0L;
    private int originalPredWordId = 0;
    private int variantPredWordId = 0;
    private double cogency = 1.0d;
    private double lnCogency = 0.0d;

    public OntolCogencyVariant() {
    }

    public OntolCogencyVariant(long conceptNameUid, long conceptUid,
	    int originalPredWordId, int variantPredWordId, double cogency, double lnCogency) {
	this.conceptNameUid = conceptNameUid;
	this.conceptUid = conceptUid;
	this.originalPredWordId = originalPredWordId;
	this.variantPredWordId = variantPredWordId;
	this.cogency = cogency;
	this.lnCogency = lnCogency;
    }

    public static class Comparators {
	public static final Comparator<OntolCogencyVariant> byNameUid = (
		OntolCogencyVariant cogency1,
		OntolCogencyVariant cogency2) -> Long.compare(
			cogency1.conceptNameUid, cogency2.conceptNameUid);
	public static final Comparator<OntolCogencyVariant> byConceptUid = (
		OntolCogencyVariant cogency1,
		OntolCogencyVariant cogency2) -> Long
			.compare(cogency1.conceptUid, cogency2.conceptUid);
	public static final Comparator<OntolCogencyVariant> byOriginalPredWordId = (
		OntolCogencyVariant cogency1,
		OntolCogencyVariant cogency2) -> Integer.compare(
			cogency1.originalPredWordId,
			cogency2.originalPredWordId);
	public static final Comparator<OntolCogencyVariant> byVariantPredWordId = (
		OntolCogencyVariant cogency1,
		OntolCogencyVariant cogency2) -> Integer.compare(
			cogency1.variantPredWordId, cogency2.variantPredWordId);
	/**
	 * This is primary key for database
	 */
	public static final Comparator<OntolCogencyVariant> byNameUid_OrigPredWordId_VarPredWordId = (
		OntolCogencyVariant cogency1,
		OntolCogencyVariant cogency2) -> byNameUid
			.thenComparing(byOriginalPredWordId)
			.thenComparing(byVariantPredWordId)
			.compare(cogency1, cogency2);
	public static final Comparator<OntolCogencyVariant> byLnCogencyDesc = (
		OntolCogencyVariant cogency1,
		OntolCogencyVariant cogency2) -> Double
			.compare(cogency2.lnCogency, cogency1.lnCogency);
	public static final Comparator<OntolCogencyVariant> byOrigPredWordId_LnCogencyDesc = (
		OntolCogencyVariant cogency1,
		OntolCogencyVariant cogency2) -> byOriginalPredWordId
			.thenComparing(byLnCogencyDesc)
			.compare(cogency1, cogency2);
	public static final Comparator<OntolCogencyVariant> byVariantPredWordId_LnCogencyDesc = (
		OntolCogencyVariant cogency1,
		OntolCogencyVariant cogency2) -> byVariantPredWordId
			.thenComparing(byLnCogencyDesc)
			.compare(cogency1, cogency2);
    }

    @Override
    public int compareTo(OntolCogencyVariant cogency) {
	return Comparators.byNameUid_OrigPredWordId_VarPredWordId.compare(this,
		cogency);
    }

    @Override
    public boolean equals(Object obj) {
	if (obj instanceof OntolCogencyVariant) {
	    OntolCogencyVariant cogency = (OntolCogencyVariant) obj;
	    return conceptNameUid == cogency.conceptNameUid
		    && originalPredWordId == cogency.originalPredWordId
		    && variantPredWordId == cogency.variantPredWordId;
	}
	return false;
    }

    @Override
    public int hashCode() {
	return Long.hashCode(conceptNameUid)
		+ 7 * Integer.hashCode(originalPredWordId)
		+ 31 * Integer.hashCode(variantPredWordId);
    }

    public long getConceptNameUid() {
	return conceptNameUid;
    }

    public void setConceptNameUid(long conceptNameUid) {
	this.conceptNameUid = conceptNameUid;
    }

    public long getConceptUid() {
	return conceptUid;
    }

    public void setConceptUid(long conceptUid) {
	this.conceptUid = conceptUid;
    }

    public int getOriginalPredWordId() {
	return originalPredWordId;
    }

    public void setOriginalPredWordId(int originalPredWordId) {
	this.originalPredWordId = originalPredWordId;
    }

    public int getVariantPredWordId() {
	return variantPredWordId;
    }

    public void setVariantPredWordId(int variantPredWordId) {
	this.variantPredWordId = variantPredWordId;
    }

    public double getCogency() {
	return cogency;
    }

    public void setCogency(double cogency) {
	this.cogency = cogency;
    }

    public double getLnCogency() {
	return lnCogency;
    }

    public void setLnCogency(double lnCogency) {
	this.lnCogency = lnCogency;
    }

}

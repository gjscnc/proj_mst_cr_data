/**
 * 
 */
package edu.mst.db.ontol.cogency.persist;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;

import edu.mst.db.ontol.cogency.OntolCogency;
import edu.mst.db.ontol.cogency.OntolCogencyDao;
import edu.mst.db.ontol.names.ConceptName;
import edu.mst.db.ontol.names.ConceptNameDao;
import edu.mst.db.ontol.words.ConceptNameWord;
import edu.mst.db.ontol.words.ConceptNameWordDao;
import edu.mst.db.ontol.words.condprob.OntolCondWordProb;
import edu.mst.db.ontol.words.condprob.OntolCondWordProbDao;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * @author gjs
 *
 */
public class OntolCogencyCallable implements Callable<Integer> {

    private long conceptNameUid = 0L;
    private boolean isPersist = false;
    private JdbcConnectionPool connPool = null;

    private ConceptNameDao nameDao = new ConceptNameDao();
    private ConceptNameWordDao nameWordDao = new ConceptNameWordDao();
    private OntolCondWordProbDao condWordProbDao = new OntolCondWordProbDao();
    private OntolCogencyDao ontolCogencyDao = new OntolCogencyDao();

    public OntolCogencyCallable(long conceptNameUid, boolean isPersist,
	    JdbcConnectionPool connPool) {
	this.conceptNameUid = conceptNameUid;
	this.isPersist = isPersist;
	this.conceptNameUid = conceptNameUid;
	this.connPool = connPool;
    }

    @Override
    public Integer call() throws Exception {

	ConceptName name = nameDao.marshal(conceptNameUid, connPool);
	long conceptUid = name.getConceptUid();
	List<ConceptNameWord> wordsForName = nameWordDao
		.marshalWordsForName(conceptNameUid, connPool);
	int nbrWords = wordsForName.size();
	Set<OntolCogency> ontolCogencies = new HashSet<OntolCogency>();
	for (int i = 0; i < nbrWords; i++) {
	    ConceptNameWord predicateWord = wordsForName.get(i);
	    int predicateWordId = predicateWord.getLexWordId();
	    OntolCogency ontolCogency = new OntolCogency();
	    ontolCogency.setConceptNameUid(conceptNameUid);
	    ontolCogency.setPredicateWordId(predicateWord.getLexWordId());
	    ontolCogency.setConceptUid(conceptUid);
	    for (int j = 0; j < nbrWords; j++) {
		if (j == i) {
		    continue;
		}
		ConceptNameWord assumedFactWord = wordsForName.get(j);
		int assumedFactWordId = assumedFactWord.getLexWordId();
		if (assumedFactWordId == predicateWordId) {
		    continue;
		}
		OntolCondWordProb condProb = condWordProbDao
			.marshal(assumedFactWordId, predicateWordId, connPool);
		if (condProb == null) {
		    /*
		     * Spurious occurrences of lexicon word ID = 0 for a word in
		     * the concept name. These are infrequent and may be
		     * associated with tokens that are unimportant. It results
		     * in conditional probability not existing in database, so
		     * ignore.
		     */
		    continue;
		}
		double cogency = ontolCogency.getCogency()
			* condProb.getCondProb();
		ontolCogency.setCogency(cogency);
		double lnCogency = ontolCogency.getLnCogency()
			+ condProb.getLnCondProb();
		ontolCogency.setLnCogency(lnCogency);
	    }
	    ontolCogencies.add(ontolCogency);
	}

	if (isPersist) {
	    ontolCogencyDao.persistBatch(ontolCogencies, connPool);
	}

	return ontolCogencies.size();
    }

}

package edu.mst.db.ontol.cogency.persist;

import java.util.Iterator;
import java.util.SortedSet;

import edu.mst.concurrent.BaseWorker;
import edu.mst.db.ontol.cogency.OntolCogencyDao;
import edu.mst.db.ontol.names.ConceptNameDao;
import edu.mst.db.util.ConnDbName;
import edu.mst.db.util.JdbcConnectionPool;

public class OntolCogencyWorker extends BaseWorker<Integer> {

    private boolean isPersist = false;
    private boolean isDeleteExisting = false;

    private long[] conceptNameUids = null;
    private int nbrConceptNames = 0;

    private int nbrNamesProcessed = 0;
    private int nbrNewCogencies = 0;
    private int rptInterval = 5000;

    public OntolCogencyWorker(boolean isPersist, boolean isDeleteExisting,
	    JdbcConnectionPool connPool) {
	super(connPool);
	this.isPersist = isPersist;
	this.isDeleteExisting = isDeleteExisting;
    }

    @Override
    public void run() throws Exception {

	if (isDeleteExisting) {
	    System.out.println("Deleting existing cogency values");
	    OntolCogencyDao.deleteExisting(connPool);
	}

	{
	    System.out.println("Retrieving concept name UIDs");
	    SortedSet<Long> conceptNameUidSet = ConceptNameDao
		    .getAllNameIds(connPool);
	    nbrConceptNames = conceptNameUidSet.size();
	    conceptNameUids = new long[nbrConceptNames];
	    int pos = 0;
	    Iterator<Long> conceptNameUidIter = conceptNameUidSet.iterator();
	    while (conceptNameUidIter.hasNext()) {
		long conceptNameUid = conceptNameUidIter.next();
		conceptNameUids[pos] = conceptNameUid;
		pos++;
	    }
	    System.out.printf("Marshaled %1$,d concept name UIDs. %n",
		    nbrConceptNames);
	}

	System.out.println("Now computing ontology cogencies");

	while (currentPos < nbrConceptNames) {

	    maxPos = getNextBatchMaxPos();
	    startPos = currentPos;

	    for (int i = startPos; i <= maxPos; i++) {
		long conceptNameUid = conceptNameUids[i];
		OntolCogencyCallable callable = new OntolCogencyCallable(
			conceptNameUid, isPersist, connPool);
		svc.submit(callable);
	    }

	    for (int i = startPos; i <= maxPos; i++) {
		nbrNewCogencies += svc.take().get();
		nbrNamesProcessed++;
		if (nbrNamesProcessed % rptInterval == 0) {
		    System.out.printf(
			    "Processed %1$,d concept names, "
				    + "creating %2$,d cogencies. %n",
			    nbrNamesProcessed, nbrNewCogencies);
		}
	    }

	    currentPos = maxPos + 1;

	}

	System.out.printf(
		"Processed total of %1$,d concept names, "
			+ "creating %2$,d cogencies. %n",
		nbrNamesProcessed, nbrNewCogencies);
    }

    @Override
    protected int getNextBatchMaxPos() throws Exception {
	int maxPos = currentPos + threadCallablesBatchSize - 1;
	if (maxPos >= nbrConceptNames - 1) {
	    maxPos = nbrConceptNames - 1;
	}
	return maxPos;
    }

    public static void main(String[] args) {

	try {

	    JdbcConnectionPool connPool = new JdbcConnectionPool(
		    ConnDbName.CONCEPT_RECOGN);

	    boolean isPersist = true;
	    boolean isDeleteExisting = true;

	    try (OntolCogencyWorker worker = new OntolCogencyWorker(isPersist,
		    isDeleteExisting, connPool)) {
		worker.run();
	    }

	} catch (Exception ex) {
	    ex.printStackTrace();
	}
    }

}

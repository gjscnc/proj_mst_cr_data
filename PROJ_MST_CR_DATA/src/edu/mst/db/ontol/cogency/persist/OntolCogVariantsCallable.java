/**
 * 
 */
package edu.mst.db.ontol.cogency.persist;

import java.util.Iterator;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.Callable;

import edu.mst.db.lexicon.LexWordVariantDao;
import edu.mst.db.ontol.cogency.OntolCogency;
import edu.mst.db.ontol.cogency.OntolCogencyVariant;
import edu.mst.db.ontol.cogency.OntolCogencyVariantDao;
import edu.mst.db.ontol.cogency.OntolCogencyDao;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * @author gjs
 *
 */
public class OntolCogVariantsCallable implements Callable<Integer> {

    private long conceptNameUid = 0L;
    private boolean isPersist = false;
    private JdbcConnectionPool connPool = null;

    private OntolCogencyDao cogencyDao = new OntolCogencyDao();
    private OntolCogencyVariantDao cogVarDao = new OntolCogencyVariantDao();
    private LexWordVariantDao variantDao = new LexWordVariantDao();

    public OntolCogVariantsCallable(long conceptNameUid,
	    boolean isPersist, JdbcConnectionPool connPool) {
	this.conceptNameUid = conceptNameUid;
	this.isPersist = isPersist;
	this.connPool = connPool;
    }

    @Override
    public Integer call() throws Exception {

	SortedSet<OntolCogency> ontolCogencies = cogencyDao
		.marshalForName(conceptNameUid, connPool);
	Iterator<OntolCogency> cogencyIter = ontolCogencies.iterator();
	SortedSet<OntolCogencyVariant> cogencyVariants = new TreeSet<OntolCogencyVariant>();
	while (cogencyIter.hasNext()) {
	    OntolCogency ontolCogency = cogencyIter.next();
	    Set<Integer> variantPredIds = variantDao.marshalVariantIdsForLexWord(
		    ontolCogency.getPredicateWordId(), connPool);
	    variantPredIds.add(ontolCogency.getPredicateWordId());
	    Iterator<Integer> variantIdIter = variantPredIds.iterator();
	    while (variantIdIter.hasNext()) {
		int variantPredId = variantIdIter.next();
		OntolCogencyVariant cogencyVariant = new OntolCogencyVariant();
		cogencyVariant.setConceptNameUid(conceptNameUid);
		cogencyVariant.setConceptUid(ontolCogency.getConceptUid());
		cogencyVariant
			.setOriginalPredWordId(ontolCogency.getPredicateWordId());
		cogencyVariant.setVariantPredWordId(variantPredId);
		cogencyVariant.setCogency(ontolCogency.getCogency());
		cogencyVariant.setLnCogency(ontolCogency.getLnCogency());
		cogencyVariants.add(cogencyVariant);
	    }
	}

	if (isPersist) {
	    cogVarDao.persistBatch(cogencyVariants, connPool);
	}

	return cogencyVariants.size();
    }

}

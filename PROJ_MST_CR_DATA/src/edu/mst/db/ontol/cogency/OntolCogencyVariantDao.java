/**
 * 
 */
package edu.mst.db.ontol.cogency;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.SortedSet;
import java.util.TreeSet;

import edu.mst.db.util.JdbcConnectionPool;

/**
 * @author gjs
 *
 */
public class OntolCogencyVariantDao {

    public static final String allFields = "conceptNameUid, conceptUid, originalPredWordId, variantPredWordId, cogency, lnCogency";

    public static final String tableName = "conceptrecogn.ontolcogency_variants";

    public static final String deleteExistingSql = "truncate table "
	    + tableName;

    public static final String persistSql = "insert into " + tableName + "("
	    + allFields + ") values (?,?,?,?,?,?)";

    public static final String marshalSql = "select " + allFields + " from "
	    + tableName
	    + " where conceptNameUid=? and originalPredWordId=? and variantPredWordId=?";

    public static final String marshalForVariantSql = "select " + allFields
	    + " from " + tableName
	    + " where conceptNameUid=? and variantPredWordId=?";

    public static void deleteExisting(JdbcConnectionPool connPool)
	    throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement deleteStmt = conn
		    .prepareStatement(deleteExistingSql)) {
		deleteStmt.executeUpdate();
	    }
	}
    }

    public void persist(OntolCogencyVariant cogency,
	    JdbcConnectionPool connPool) throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement persistStmt = conn
		    .prepareStatement(persistSql)) {
		setPersistValues(cogency, persistStmt);
		persistStmt.executeUpdate();
	    }
	}
    }

    public void persistBatch(Collection<OntolCogencyVariant> cogencies,
	    JdbcConnectionPool connPool) throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement persistStmt = conn
		    .prepareStatement(persistSql)) {
		for (OntolCogencyVariant cogency : cogencies) {
		    setPersistValues(cogency, persistStmt);
		    persistStmt.addBatch();
		}
		persistStmt.executeBatch();
	    }
	}
    }

    private void setPersistValues(OntolCogencyVariant cogency,
	    PreparedStatement persistStmt) throws SQLException {
	/*
	 * 1-conceptNameUid, 2-conceptUid, 3-originalPredWordId,
	 * 4-variantPredWordId, 5-cogency, 6-lnCogency
	 */
	persistStmt.setLong(1, cogency.getConceptNameUid());
	persistStmt.setLong(2, cogency.getConceptUid());
	persistStmt.setInt(3, cogency.getOriginalPredWordId());
	persistStmt.setInt(4, cogency.getVariantPredWordId());
	persistStmt.setDouble(5, cogency.getCogency());
	persistStmt.setDouble(6, cogency.getLnCogency());
    }

    public OntolCogencyVariant marshal(long conceptNameUid,
	    int originalLexWordId, int variantLexWordId,
	    JdbcConnectionPool connPool) throws SQLException {
	OntolCogencyVariant cogency = null;
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement marshalStmt = conn
		    .prepareStatement(marshalSql)) {
		marshalStmt.setLong(1, conceptNameUid);
		marshalStmt.setInt(2, originalLexWordId);
		marshalStmt.setInt(3, variantLexWordId);
		try (ResultSet rs = marshalStmt.executeQuery()) {
		    if (rs.next()) {
			cogency = instantiateFromResultSet(rs);
		    }
		}
	    }
	}
	return cogency;
    }

    public SortedSet<OntolCogencyVariant> marshalForVariant(long conceptNameUid,
	    int variantLexWordId, JdbcConnectionPool connPool)
	    throws SQLException {
	SortedSet<OntolCogencyVariant> cogencyVariants = new TreeSet<OntolCogencyVariant>();
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement marshalStmt = conn
		    .prepareStatement(marshalForVariantSql)) {
		marshalStmt.setLong(1, conceptNameUid);
		marshalStmt.setInt(2, variantLexWordId);
		try (ResultSet rs = marshalStmt.executeQuery()) {
		    while (rs.next()) {
			OntolCogencyVariant cogencyVariant = instantiateFromResultSet(
				rs);
			cogencyVariants.add(cogencyVariant);
		    }
		}
	    }
	}
	return cogencyVariants;
    }

    private OntolCogencyVariant instantiateFromResultSet(ResultSet rs)
	    throws SQLException {
	OntolCogencyVariant cogency = new OntolCogencyVariant();
	/*
	 * 1-conceptNameUid, 2-conceptUid, 3-originalPredWordId,
	 * 4-variantPredWordId, 5-cogency, 6-lnCogency
	 */
	cogency.setConceptNameUid(rs.getLong(1));
	cogency.setConceptUid(rs.getLong(2));
	cogency.setOriginalPredWordId(rs.getInt(3));
	cogency.setVariantPredWordId(rs.getInt(4));
	cogency.setCogency(rs.getDouble(5));
	cogency.setLnCogency(rs.getDouble(6));
	return cogency;
    }

}

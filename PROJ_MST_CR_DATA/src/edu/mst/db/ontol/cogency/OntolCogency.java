/**
 * 
 */
package edu.mst.db.ontol.cogency;

import java.util.Comparator;

/**
 * Maximum cogency for a concept and predicate word combination.
 * <p>
 * One instance for each predicate word in the concept name.
 * 
 * @author gjs
 *
 */
public class OntolCogency implements Comparable<OntolCogency> {

    private long conceptNameUid = 0L;
    private long conceptUid = 0L;
    private int predicateWordId = 0;
    private double cogency = 1.0d;
    private double lnCogency = 0.0d;

    public OntolCogency() {
    }

    public OntolCogency(long conceptNameUid, long conceptUid, int predicateWordId, double cogency,
	    double lnCogency) {
	this.conceptNameUid = conceptNameUid;
	this.conceptUid = conceptUid;
	this.predicateWordId = predicateWordId;
	this.cogency = cogency;
	this.lnCogency = lnCogency;
    }

    public static class Comparators {
	public static final Comparator<OntolCogency> ByNameUid = (
		OntolCogency cogency1, OntolCogency cogency2) -> Long
			.compare(cogency1.conceptNameUid, cogency2.conceptNameUid);
	public static final Comparator<OntolCogency> ByConceptUid = (
		OntolCogency cogency1, OntolCogency cogency2) -> Long
			.compare(cogency1.conceptUid, cogency2.conceptUid);
	public static final Comparator<OntolCogency> ByPredicateWordId = (
		OntolCogency cogency1,
		OntolCogency cogency2) -> Integer.compare(
			cogency1.predicateWordId, cogency2.predicateWordId);
	/**
	 * This is primary key for database
	 */
	public static final Comparator<OntolCogency> ByNameUid_ConceptUid_PredWordId = (
		OntolCogency cogency1, OntolCogency cogency2) -> ByNameUid
			.thenComparing(ByConceptUid)
			.thenComparing(ByPredicateWordId)
			.compare(cogency1, cogency2);
	public static final Comparator<OntolCogency> ByLnCogencyDesc = (
		OntolCogency cogency1, OntolCogency cogency2) -> Double
			.compare(cogency2.lnCogency, cogency1.lnCogency);
	public static final Comparator<OntolCogency> ByPredWordIdThenLnCogencyDesc = (
		OntolCogency cogency1,
		OntolCogency cogency2) -> ByPredicateWordId
			.thenComparing(ByLnCogencyDesc)
			.compare(cogency1, cogency2);
    }

    @Override
    public int compareTo(OntolCogency cogency) {
	return Comparators.ByNameUid_ConceptUid_PredWordId.compare(this, cogency);
    }

    @Override
    public boolean equals(Object obj) {
	if (obj instanceof OntolCogency) {
	    OntolCogency cogency = (OntolCogency) obj;
	    return this.conceptUid == cogency.conceptUid
		    && this.predicateWordId == cogency.predicateWordId;
	}
	return false;
    }

    @Override
    public int hashCode() {
	return Long.hashCode(conceptUid)
		+ 31 * Integer.hashCode(predicateWordId);
    }

    public long getConceptNameUid() {
        return conceptNameUid;
    }

    public void setConceptNameUid(long conceptNameUid) {
        this.conceptNameUid = conceptNameUid;
    }

    public long getConceptUid() {
	return conceptUid;
    }

    public void setConceptUid(long conceptUid) {
	this.conceptUid = conceptUid;
    }

    public int getPredicateWordId() {
	return predicateWordId;
    }

    public void setPredicateWordId(int predicateWordId) {
	this.predicateWordId = predicateWordId;
    }

    public double getCogency() {
	return cogency;
    }

    public void setCogency(double cogency) {
	this.cogency = cogency;
    }

    public double getLnCogency() {
	return lnCogency;
    }

    public void setLnCogency(double lnCogency) {
	this.lnCogency = lnCogency;
    }

}

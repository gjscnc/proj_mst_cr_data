/**
 * 
 */
package edu.mst.db.ontol.cogency;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import edu.mst.db.util.JdbcConnectionPool;

/**
 * @author gjs
 *
 */
public class OntolCogencyDao {

    public static final String allFields = "conceptNameUid, conceptUid, predicateWordId, cogency, lnCogency";
    public static final String tableName = "conceptrecogn.ontolcogency";
    public static final String deleteExistingSql = "truncate " + tableName;
    public static final String persistSql = "insert into " + tableName + "("
	    + allFields + ") VALUES (?,?,?,?,?)";
    public static final String marshalSql = "select " + allFields + " from "
	    + tableName
	    + " where conceptNameUid = ? and conceptUid = ? and predicateWordId = ?";
    public static final String marshalForPredicateSql = "select " + allFields
	    + " from " + tableName + " where predicateWordId = ?";
    public static final String marshalForNameSql = "select " + allFields
	    + " from " + tableName + " where conceptNameUid = ?";

    public static void deleteExisting(JdbcConnectionPool connPool)
	    throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement deleteExistingStmt = conn
		    .prepareStatement(deleteExistingSql)) {
		deleteExistingStmt.executeUpdate();
	    }
	}
    }

    public void persist(OntolCogency cogency, JdbcConnectionPool connPool)
	    throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement persistStmt = conn
		    .prepareStatement(persistSql)) {
		persistStmt.setLong(1, cogency.getConceptNameUid());
		persistStmt.setLong(2, cogency.getConceptUid());
		persistStmt.setInt(3, cogency.getPredicateWordId());
		persistStmt.setDouble(4, cogency.getCogency());
		persistStmt.setDouble(5, cogency.getLnCogency());
		persistStmt.executeUpdate();
	    }
	}
    }

    public void persistBatch(Collection<OntolCogency> cogencies,
	    JdbcConnectionPool connPool) throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement persistStmt = conn
		    .prepareStatement(persistSql)) {
		Iterator<OntolCogency> cogencyIter = cogencies.iterator();
		while (cogencyIter.hasNext()) {
		    OntolCogency cogency = cogencyIter.next();
		    persistStmt.setLong(1, cogency.getConceptNameUid());
		    persistStmt.setLong(2, cogency.getConceptUid());
		    persistStmt.setInt(3, cogency.getPredicateWordId());
		    persistStmt.setDouble(4, cogency.getCogency());
		    persistStmt.setDouble(5, cogency.getLnCogency());
		    persistStmt.addBatch();
		}
		persistStmt.executeBatch();
	    }
	}

    }

    public Set<OntolCogency> marshalAllForPredicate(int predicateWordId,
	    JdbcConnectionPool connPool) throws SQLException {
	Set<OntolCogency> results = new HashSet<OntolCogency>();
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement getForPredStmt = conn
		    .prepareStatement(marshalForPredicateSql)) {
		getForPredStmt.setInt(1, predicateWordId);
		try (ResultSet rs = getForPredStmt.executeQuery()) {
		    while (rs.next()) {
			OntolCogency ontolCogency = instantiateFromResultSet(rs);
			results.add(ontolCogency);
		    }
		}
	    }
	}
	return results;
    }

    public SortedSet<OntolCogency> marshalAllForPredicateSorted(
	    int predicateWordId, Comparator<OntolCogency> comparator,
	    JdbcConnectionPool connPool) throws SQLException {
	SortedSet<OntolCogency> sortedResults = new TreeSet<OntolCogency>(
		comparator);
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement getForPredStmt = conn
		    .prepareStatement(marshalForPredicateSql)) {
		getForPredStmt.setInt(1, predicateWordId);
		try (ResultSet rs = getForPredStmt.executeQuery()) {
		    while (rs.next()) {
			OntolCogency ontolCogency = instantiateFromResultSet(rs);
			sortedResults.add(ontolCogency);
		    }
		}
	    }
	    return sortedResults;
	}
    }

    public SortedSet<OntolCogency> marshalForName(long conceptNameUid,
	    JdbcConnectionPool connPool) throws SQLException {
	SortedSet<OntolCogency> cogencies = new TreeSet<OntolCogency>();
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement marshalStmt = conn
		    .prepareStatement(marshalForNameSql)) {
		marshalStmt.setLong(1, conceptNameUid);
		try (ResultSet rs = marshalStmt.executeQuery()) {
		    while (rs.next()) {
			OntolCogency ontolCogency = instantiateFromResultSet(
				rs);
			cogencies.add(ontolCogency);
		    }
		}
	    }
	}
	return cogencies;
    }

    public OntolCogency marshal(long conceptNameUid, long conceptUid,
	    int predicateWordId, JdbcConnectionPool connPool)
	    throws SQLException {

	OntolCogency ontolCogency = null;
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement marshalStmt = conn
		    .prepareStatement(marshalSql)) {
		marshalStmt.setLong(1, conceptNameUid);
		marshalStmt.setLong(2, conceptUid);
		marshalStmt.setInt(3, predicateWordId);
		try (ResultSet rs = marshalStmt.executeQuery()) {
		    if (rs.next()) {
			ontolCogency = instantiateFromResultSet(rs);
		    }
		}
	    }
	}
	return ontolCogency;
    }

    private OntolCogency instantiateFromResultSet(ResultSet rs)
	    throws SQLException {
	/*
	 * conceptNameUid, conceptUid, predicateWordId, cogency, lnCogency
	 */
	OntolCogency cogency = new OntolCogency();
	cogency.setConceptNameUid(rs.getLong(1));
	cogency.setConceptUid(rs.getLong(2));
	cogency.setPredicateWordId(rs.getInt(3));
	cogency.setCogency(rs.getDouble(4));
	cogency.setLnCogency(rs.getDouble(5));
	return cogency;
    }

}

/**
 * Components used to import the UMLS concepts file 'MRCONSO.RRF'. 
 * This provides references (fields CUI and AUI) required to parse
 * text annotated by the NIH.
 */
/**
 * @author gjs
 *
 */
package edu.mst.db.ontol.umls;
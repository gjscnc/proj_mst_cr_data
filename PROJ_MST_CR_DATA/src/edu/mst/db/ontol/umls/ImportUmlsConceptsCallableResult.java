package edu.mst.db.ontol.umls;

public class ImportUmlsConceptsCallableResult {
    
    public Exception ex = null;
    public String msg = null;
    public long endTimeSnapShot = 0L;
    public String cui = null;
    public String aui = null;
    public int recordsInserted = 0;

}

package edu.mst.db.ontol.umls;

import java.util.List;
import java.util.ArrayList;
import java.io.BufferedReader;
import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import edu.mst.db.text.util.NlpUtil;
import edu.mst.db.util.ConnDbName;
import edu.mst.db.util.JdbcConnectionPool;

public class ImportUmlsConceptsConcurrentWorker implements AutoCloseable {

    private static int nbrOfProcessors;
    private static ExecutorService pool;

    private File conceptsFile = null;
    private boolean deleteExisting = false;
    private JdbcConnectionPool connPool;
    private CompletionService<ImportUmlsConceptsCallableResult> svc;

    int rptInterval = 50;
    int maxNbrBatchSubmissions = rptInterval;
    int nbrSubmissionsForRetrievingResults = 0;
    int maxNbrRecords = 10000;
    int batchSizeDivisor = 50;
    int batchSize = maxNbrRecords / batchSizeDivisor;
    int resultsCount = 0;
    int totalNbrBatchSubmissions = 0;

    public ImportUmlsConceptsConcurrentWorker(File conceptsFile,
	    boolean deleteExisting, JdbcConnectionPool connPool) {
	this.conceptsFile = conceptsFile;
	this.deleteExisting = deleteExisting;
	this.connPool = connPool;
	nbrOfProcessors = Runtime.getRuntime().availableProcessors();
	pool = Executors.newFixedThreadPool(nbrOfProcessors *= 3);
	svc = new ExecutorCompletionService<ImportUmlsConceptsCallableResult>(
		pool);
    }

    public void run() throws Exception {

	if (deleteExisting) {
	    System.out.printf("Deleting existing MRCONSO records %n%n");
	    try (Connection conn = connPool.getConnection()) {
		String deleteSql = "truncate table conceptrecogn.mrconso";
		try (PreparedStatement deleteStmt = conn
			.prepareStatement(deleteSql)) {
		    deleteStmt.executeUpdate();
		}
	    }
	}

	svc = new ExecutorCompletionService<ImportUmlsConceptsCallableResult>(
		pool);

	System.out.printf("Importing UMLS Concepts...%n%n");

	try (BufferedReader bufferedReader = NlpUtil
		.getUTF16Reader(conceptsFile)) {

	    long lastSnapShot = System.currentTimeMillis();

	    ImportUmlsConceptsCallableResult errorResult = null;

	    double deltaNbrLoadedAsDbl = Double
		    .valueOf(String.valueOf(maxNbrRecords));

	    List<String> batchOfRecords = new ArrayList<String>();
	    boolean eof = false;
	    try {
		while (!eof) {

		    String nextRecord = bufferedReader.readLine();

		    eof = nextRecord == null;
		    if (!eof) {
			batchOfRecords.add(nextRecord);
		    }

		    if (batchOfRecords.size() == batchSize
			    || (eof && batchOfRecords.size() > 0)) {

			ImportUmlsConceptsCallable callable = new ImportUmlsConceptsCallable(
				new ArrayList<String>(batchOfRecords),
				connPool);
			svc.submit(callable);

			totalNbrBatchSubmissions++;
			nbrSubmissionsForRetrievingResults++;
			batchOfRecords.clear();

			if (nbrSubmissionsForRetrievingResults == maxNbrBatchSubmissions
				|| (eof && batchOfRecords.size() > 0)) {
			    for (int i = 0; i < nbrSubmissionsForRetrievingResults; i++) {
				// take() blocks so loop executes as results
				// available
				Future<ImportUmlsConceptsCallableResult> f = svc
					.take();
				ImportUmlsConceptsCallableResult result = f
					.get();
				if (result.ex != null) {
				    errorResult = result;
				    throw result.ex;
				}
				resultsCount += result.recordsInserted;
			    }
			    nbrSubmissionsForRetrievingResults = 0;
			}

			errorResult = null;

			if (totalNbrBatchSubmissions % rptInterval == 0) {
			    long now = System.currentTimeMillis();
			    double duration = (now - lastSnapShot) / 1000.0d;
			    double avg = duration / deltaNbrLoadedAsDbl;
			    System.out.printf(
				    "Cumulative %1$,d batch submissions, "
					    + "loaded %2$,d UMLS concepts, "
					    + "avg load time for last %3$,d concepts"
					    + " = %4$9.6f sec/concept %n",
				    totalNbrBatchSubmissions, resultsCount,
				    maxNbrRecords, avg);
			    lastSnapShot = System.currentTimeMillis();
			}
		    }

		}

		System.out.printf("Loaded a grand total of %1$,d UMLS concepts %n%n",
			resultsCount);

	    } catch (Exception ex) {
		String errMsg = String.format(
			"Error importing UMLS Concepts file. %n"
				+ "Results count: %1$,d %n"
				+ "Error description: %2$s %n"
				+ "Error description from callable: %3$s %n",
			resultsCount, ex.getMessage(),
			errorResult.ex.getMessage());
		System.out.println(errMsg);
		ex.printStackTrace();
		throw ex;
	    }
	}

    }

    protected void shutdownAndAwaitTermination(ExecutorService pool) {
	// pool.shutdown(); // Disable new tasks from being submitted
	pool.shutdownNow();
	try {
	    // Wait a while for existing tasks to terminate
	    if (!pool.awaitTermination(5, TimeUnit.SECONDS)) {
		pool.shutdownNow(); // Cancel currently executing tasks
		// Wait a while for tasks to respond to being cancelled
		if (!pool.awaitTermination(15, TimeUnit.SECONDS))
		    System.err.println("Pool did not terminate");
	    }
	} catch (InterruptedException ie) {
	    // (Re-)Cancel if current thread also interrupted
	    pool.shutdownNow();
	    // Preserve interrupt status
	    Thread.currentThread().interrupt();
	}
    }

    @Override
    public void close() throws Exception {
	this.shutdownAndAwaitTermination(pool);
	System.out.println("Parallel processor closed");
    }

    public static void main(String[] args) {

	try {

	    JdbcConnectionPool connPool = new JdbcConnectionPool(
		    ConnDbName.CONCEPT_RECOGN);
	    String fileDir = "/home/gjs/umls/metathesauras/umls-2019AB-full/2019AB-full/2019AB/META";
	    String fileName = "MRCONSO.RRF";
	    File conceptsFile = new File(fileDir + "/" + fileName);
	    boolean deleteExisting = true;

	    try (ImportUmlsConceptsConcurrentWorker worker = new ImportUmlsConceptsConcurrentWorker(
		    conceptsFile, deleteExisting, connPool)) {
		worker.run();
	    }

	} catch (Exception ex) {
	    ex.printStackTrace();
	}

    }
}

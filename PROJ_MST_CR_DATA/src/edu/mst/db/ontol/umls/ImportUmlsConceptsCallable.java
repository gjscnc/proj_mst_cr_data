package edu.mst.db.ontol.umls;

import java.sql.Connection;
import java.sql.PreparedStatement;
//import java.sql.ResultSet;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

import edu.mst.db.util.JdbcConnectionPool;

public class ImportUmlsConceptsCallable
	implements Callable<ImportUmlsConceptsCallableResult> {

    List<String> conceptRecords = null;
    private JdbcConnectionPool connPool;

    public ImportUmlsConceptsCallable(List<String> conceptRecords,
	    JdbcConnectionPool connPool) {
	this.conceptRecords = conceptRecords;
	this.connPool = connPool;
    }

    public FutureTask<ImportUmlsConceptsCallableResult> getTask() {
	return new FutureTask<ImportUmlsConceptsCallableResult>(this);
    }

    public synchronized ImportUmlsConceptsCallableResult call()
	    throws Exception {

	ImportUmlsConceptsCallableResult result = new ImportUmlsConceptsCallableResult();

	try (Connection conn = connPool.getConnection()) {

	    String conceptExistsSql = "SELECT aui FROM conceptrecogn.mrconso WHERE aui = ? and sab = ?";
	    String insertBatchSql = "INSERT INTO conceptrecogn.mrconso "
		    + "(cui, lat, ts, lui, stt, sui, isPref, aui, saui, scui, sdui, sab, tty, code, str, srl, suppress, cvf) "
		    + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	    try (PreparedStatement conceptExistsStmt = conn
		    .prepareStatement(conceptExistsSql);
		    PreparedStatement batchInsertStmt = conn
			    .prepareStatement(insertBatchSql)) {

		for (String inputText : conceptRecords) {
		    
		    String[] tokens = inputText.replace('|', '\t').split("\t");
		    int nbrTokens = tokens.length;
		    
		    String cui = "";
		    String lat = "";
		    String ts = "";
		    String lui = "";
		    String stt = "";
		    String sui = "";
		    String isPref = "";
		    String aui = "";
		    String saui = "";
		    String scui = "";
		    String sdui = "";
		    String sab = "";
		    String tty = "";
		    String code = "";
		    String str = "";
		    int srl = 0;
		    String suppress = "";
		    int cvf = 0;

		    for (int i = 0; i < nbrTokens; i++) {
			String token = tokens[i];
			switch (i) {
			// CUI
			case 0:
			    cui = token;
			    break;
			// LAT
			case 1:
			    lat = token;
			    break;
			// TS
			case 2:
			    ts = token;
			    break;
			// LUI
			case 3:
			    lui = token;
			    break;
			// STT
			case 4:
			    stt = token;
			    break;
			// SUI
			case 5:
			    sui = token;
			    break;
			// ISPREF
			case 6:
			    isPref = token;
			    break;
			// AUI
			case 7:
			    aui = token;
			    break;
			// SAUI
			case 8:
			    saui = token;
			    break;
			// SCUI
			case 9:
			    scui = token;
			    break;
			// SDUI
			case 10:
			    sdui = token;
			    break;
			// SAB
			case 11:
			    sab = token;
			    break;
			// TTY
			case 12:
			    tty = token;
			    break;
			// CODE
			case 13:
			    code = token;
			    break;
			// STR
			case 14:
			    str = token;
			    break;
			// SRL
			case 15:
			    srl = Integer.valueOf(token).intValue();
			    break;
			// SUPPRESS
			case 16:
			    suppress = token;
			    break;
			// CVF
			case 17:
			    cvf = Integer.valueOf(token).intValue();
			    break;
			default:
			    // mrconso.setCvf(Integer.valueOf(token), spsIdx);
			    break;
			}
		    }
		    batchInsertStmt.setString(1, cui);
		    batchInsertStmt.setString(2, lat);
		    batchInsertStmt.setString(3, ts);
		    batchInsertStmt.setString(4, lui);
		    batchInsertStmt.setString(5, stt);
		    batchInsertStmt.setString(6, sui);
		    batchInsertStmt.setString(7, isPref);
		    batchInsertStmt.setString(8, aui);
		    batchInsertStmt.setString(9, saui);
		    batchInsertStmt.setString(10, scui);
		    batchInsertStmt.setString(11, sdui);
		    batchInsertStmt.setString(12, sab);
		    batchInsertStmt.setString(13, tty);
		    batchInsertStmt.setString(14, code);
		    batchInsertStmt.setString(15, str);
		    batchInsertStmt.setInt(16, srl);
		    batchInsertStmt.setString(17, suppress);
		    batchInsertStmt.setInt(18, cvf);
		    batchInsertStmt.addBatch();
		}

		int[] batchResults = batchInsertStmt.executeBatch();
		result.recordsInserted = batchResults.length;
		result.endTimeSnapShot = System.currentTimeMillis();
	    }
	} catch (Exception ex) {
	    // System.out.println(msg);
	    result.ex = ex;
	    result.endTimeSnapShot = System.currentTimeMillis();
	    // ex.printStackTrace();
	    return result;
	    // throw ex;
	}

	return result;
    }

}

/**
 * 
 */
package edu.mst.db.ontol.words.persist;

/**
 * Enumeration of steps to take for extracting and persisting words from concept
 * names.
 * <p>
 * Usage: Break long process into incremental steps that can be repeated or
 * broken into incremental series of steps.
 * 
 * @author gjs
 *
 */
public enum StepToRunWordsFromConceptNames {

    DELETE_EXISTING(1, "Delete existing lexicon records"), 
    GET_ALL_CONCEPT_WORDS(2, "Retrieve set of distinct words found in concept names"), 
    CACHE_CONCEPT_WORDS(3, "Cache distinct words found in concept names to flat file"),
    COMPILE_VARIANTS(4, "Compile variants for each distinct word found in concept names"), 
    CACHE_VARIANTS_TO_FLAT_FILE(5, "Cache word variants to flat file"), 
    PERSIST_LEXICON(6, "Persist lexicon words (each unique word found in concept names and its variants)"), 
    PERSIST_VARIANTS(7, "Persist lexicon word variants");

    private int stepNbr;
    private static int maxNbrSteps = StepToRunWordsFromConceptNames.values().length;
    public static int getMaxNbrSteps() {return maxNbrSteps;}
    
    private String description;
    public String getDescription() {return description;}
    public String getFullDescr() {
	return String.format("Step %1$d of %2$d: %3$s", stepNbr, maxNbrSteps, description);
    }

    StepToRunWordsFromConceptNames(int stepNbr, String description) {
	this.stepNbr = stepNbr;
	this.description = description;
    }
    
    public int getStepNbr() {
	return stepNbr;
    }

}

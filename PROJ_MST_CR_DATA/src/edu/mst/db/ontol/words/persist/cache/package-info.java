/**
 * 
 */
/**
 * Folder for cached records when parsing and persisting words from concept
 * names or text.
 * 
 * @author gjs
 *
 */
package edu.mst.db.ontol.words.persist.cache;
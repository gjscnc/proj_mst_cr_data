package edu.mst.db.ontol.words.persist;

import java.util.SortedSet;
import java.util.concurrent.Callable;

import edu.mst.db.lexicon.StopWords;
import edu.mst.db.text.util.LexVariantsSynType;
import edu.mst.db.text.util.LexVariantsUtil;
import edu.mst.db.text.util.LexVariantsUtil.Variants;

/**
 * Callable for concurrent processing to generate variants a set of words
 * extracted from concept names.
 * <p>
 * Worker class is
 * {@link edu.mst.db.ontol.words.persist.LexWordsFromConceptNameWorker}
 * 
 * @author gjs
 *
 */
public class LexWordsFromConceptNameCallable
	implements Callable<SortedSet<Variants>> {

    private SortedSet<String> tokens = null;
    private LexVariantsUtil variantsUtil = null;

    public LexWordsFromConceptNameCallable(boolean isMutateVariants,
	    SortedSet<String> tokens, StopWords stopWords) {
	this.tokens = tokens;
	variantsUtil = new LexVariantsUtil(isMutateVariants, stopWords);
    }

    @Override
    public SortedSet<Variants> call() throws Exception {
	return variantsUtil.getVariants(tokens,
		LexVariantsSynType.SYNONYMS_NOT_RECURSIVE);
    }

}

/**
 * 
 */
package edu.mst.db.ontol.words.persist;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import edu.mst.db.lexicon.LexWordDao;
import edu.mst.db.lexicon.LexWordVariantDao;
import edu.mst.db.lexicon.StopWords;
import edu.mst.db.lexicon.acrabbrev.AcrAbbrev;
import edu.mst.db.lexicon.acrabbrev.AcrAbbrevDao;
import edu.mst.db.text.util.LexConstants;
import edu.mst.db.text.util.LexVariantsUtil.Variants;
import edu.mst.db.text.util.NlpUtil;
import edu.mst.db.util.ConnDbName;
import edu.mst.db.util.DateTimeUtil;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * Extracts lexical information from concept words and persists to database.
 * <p>
 * Process flow is to first run this worker to extract all lexicon words from
 * the ontology concepts to first build a complete lexicon.
 * <p>
 * Execute the steps defined in {@link StepToRunWordsFromConceptNames};
 * <p>
 * Then, after the complete lexicon is built, run
 * {@link ParseConceptWordsWorker} to persist the word sequences from the
 * concept names and persist in the database.
 * 
 * @author gjs
 *
 */
public class LexWordsFromConceptNameWorker implements AutoCloseable {

    private StepToRunWordsFromConceptNames[] stepsToRun = null;
    private boolean isMutateVariants = false;
    private JdbcConnectionPool connPool = null;
    private SortedSet<String> conceptNameWordTokens = new TreeSet<String>();
    private SortedMap<String, SortedSet<String>> variantsByLexWord = new TreeMap<String, SortedSet<String>>();
    private SortedMap<String, AcrAbbrev> acrAbbrevByToken = new TreeMap<String, AcrAbbrev>();
    private SortedMap<String, Integer> lexWordIdByToken = new TreeMap<String, Integer>();
    private SortedSet<String> parsableTokens = new TreeSet<String>();
    private StopWords stopWords = null;
    private NlpUtil nlpUtil = new NlpUtil();

    private static int nbrOfProcessors;
    private static ExecutorService pool;

    private CompletionService<SortedSet<Variants>> lexParserSvc;
    private CompletionService<Integer> lexVarPersistSvc;

    private int rptIntervalLexParse = 50;
    private int maxNbrBatchSubmissions = rptIntervalLexParse;
    private int currentNbrBatchSubmissions = 0;
    private int maxNbrRecords = 10000;
    private int batchSizeDivisor = 50;
    private int batchSize = maxNbrRecords / batchSizeDivisor;
    private int totalNbrBatchSubmissions = 0;
    private int nbrDistinctTokensProcessed = 0;
    private int nbrVariantsExtracted = 0;
    private int nbrTokensParsable = 0;
    private int rptIntervalLexPersist = 5000;
    private int nbrLexWordsPersisted = 0;
    private int nbrLexVariantsPersisted = 0;

    private static final String cacheFileDir = "/home/gjs/Documents/workspaceSPS_MySQL/PROJ_SPS_DATA/"
	    + "src/edu/mst/db/ontol/words/persist/cache";
    private static final String conceptNamesDistinctWordsFileName = "conceptNamesDistinctWords.txt";
    private static final String variantsFileName = "lexVariants.txt";
    private static final String acrAbbrevFileName = "acrAbbrev.txt";
    private static final String parsableFileName = "parsable.txt";

    public LexWordsFromConceptNameWorker(
	    StepToRunWordsFromConceptNames[] stepsToRun,
	    boolean isMutateVariants, JdbcConnectionPool connPool)
	    throws Exception {
	this.stepsToRun = stepsToRun;
	this.isMutateVariants = isMutateVariants;
	this.connPool = connPool;
	nbrOfProcessors = Runtime.getRuntime().availableProcessors();
	pool = Executors.newFixedThreadPool(nbrOfProcessors *= 3);
	lexParserSvc = new ExecutorCompletionService<SortedSet<Variants>>(pool);
	lexVarPersistSvc = new ExecutorCompletionService<Integer>(pool);
	stopWords = new StopWords(connPool);
    }

    public void run() throws Exception {

	for (int i = 0; i < stepsToRun.length; i++) {

	    StepToRunWordsFromConceptNames stepToRun = stepsToRun[i];
	    System.out.printf("%n%n%1$s%n", stepToRun.getFullDescr());
	    System.out.printf("-----------------------------------------%n%n");
	    switch (stepToRun) {
	    case DELETE_EXISTING:
		deleteExisting();
		break;
	    case GET_ALL_CONCEPT_WORDS:
		getAllConceptWords();
		break;
	    case CACHE_CONCEPT_WORDS:
		writeConceptWordsToFlatFile();
		break;
	    case COMPILE_VARIANTS:
		compileVariants();
		break;
	    case CACHE_VARIANTS_TO_FLAT_FILE:
		writeVariantsToFlatFile();
		break;
	    case PERSIST_LEXICON:
		persistLexicon();
		break;
	    case PERSIST_VARIANTS:
		persistVariants();
		break;
	    }

	}

	System.out.printf("%n%n ----- DONE -----%n");

    }

    private void deleteExisting() throws Exception {
	System.out.println("Deleting existing lexical records");
	String deleteLexWordsSql = "truncate table conceptrecogn.lexwords";
	String deleteVariantsSql = "truncate table conceptrecogn.lexwordsvariants";
	String deleteStopWordsSql = "truncate table conceptrecogn.stopwords";
	String deleteAcrAbbrevSql = "truncate table conceptrecogn.acrabbrev";
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement deleteLexWordsStmt = conn
		    .prepareStatement(deleteLexWordsSql);
		    PreparedStatement deleteVariantsStmt = conn
			    .prepareStatement(deleteVariantsSql);
		    PreparedStatement deleteStopWordsStmt = conn
			    .prepareStatement(deleteStopWordsSql);
		    PreparedStatement deleteAcrAbbrevStmt = conn
			    .prepareStatement(deleteAcrAbbrevSql)) {
		deleteLexWordsStmt.executeUpdate();
		deleteVariantsStmt.executeUpdate();
		deleteStopWordsStmt.executeUpdate();
		deleteAcrAbbrevStmt.executeUpdate();
	    }
	}
	// reload stop words from file
	stopWords = new StopWords(connPool);
	System.out.println(
		"Existing lexical records deleted, and reloaded stop words into database.");
    }

    private void getAllConceptWords() throws SQLException {
	System.out.println("Retrieving words from concept names");
	try (Connection conn = connPool.getConnection()) {
	    String getWordsSql = "select distinct token from conceptrecogn.conceptnamewords order by token";
	    try (PreparedStatement getWordsStmt = conn
		    .prepareStatement(getWordsSql)) {
		try (ResultSet rs = getWordsStmt.executeQuery()) {
		    while (rs.next()) {
			String wordToken = rs.getString(1);
			if (wordToken == null || wordToken.isBlank()
				|| wordToken.isEmpty()) {
			    continue;
			}
			conceptNameWordTokens
				.add(rs.getString(1).toLowerCase());
		    }
		}
	    }
	}
	System.out.printf("Retrieved total of %1$,d unique word tokens %n%n",
		conceptNameWordTokens.size());
    }

    private void writeConceptWordsToFlatFile() throws IOException {
	System.out.println("Writing distinct concept words to flat file");
	Path distinctWordsPath = Paths.get(cacheFileDir,
		conceptNamesDistinctWordsFileName);
	Files.deleteIfExists(distinctWordsPath);
	File distinctWordsFile = Files.createFile(distinctWordsPath).toFile();
	try (BufferedWriter writer = new BufferedWriter(
		new FileWriter(distinctWordsFile, Charset.forName("UTF-16")))) {
	    Iterator<String> distinctWordIter = conceptNameWordTokens
		    .iterator();
	    while (distinctWordIter.hasNext()) {
		String wordToken = distinctWordIter.next();
		writer.append(wordToken);
		writer.newLine();
	    }
	}
    }

    private void loadConceptWordsFromFlatFile() throws IOException {
	System.out.println(
		"Loading set of distinct concept words from flat file");
	Path distinctWordsPath = Paths.get(cacheFileDir,
		conceptNamesDistinctWordsFileName);
	if (Files.notExists(distinctWordsPath)) {
	    String errMsg = String.format(
		    "Flat file cache for distince concept words (%1$s/%2$s) was not found ",
		    cacheFileDir, conceptNamesDistinctWordsFileName);
	    throw new IOException(errMsg);
	}
	File distinctWordsFile = distinctWordsPath.toFile();
	try (BufferedReader reader = new BufferedReader(
		new FileReader(distinctWordsFile, Charset.forName("UTF-16")))) {
	    String line = null;
	    while ((line = reader.readLine()) != null) {
		String wordToken = line.strip();
		conceptNameWordTokens.add(wordToken);
	    }
	}
	System.out.printf(
		"%nLoaded %1$,d distinct concept words from flat file%n",
		conceptNameWordTokens.size());
    }

    private void compileVariants()
	    throws InterruptedException, ExecutionException, IOException {
	if (conceptNameWordTokens == null
		|| conceptNameWordTokens.size() == 0) {
	    if (conceptNameWordTokens == null) {
		conceptNameWordTokens = new TreeSet<String>();
	    }
	    loadConceptWordsFromFlatFile();
	}
	Iterator<String> wordTokensIter = conceptNameWordTokens.iterator();
	SortedSet<String> batchTokens = new TreeSet<String>();
	boolean isLastToken = false;
	long beginSnapShot = System.currentTimeMillis();
	long rptBeginSnapShot = beginSnapShot;
	while (wordTokensIter.hasNext()) {

	    String wordToken = wordTokensIter.next();
	    isLastToken = wordTokensIter.hasNext() == false ? true : false;
	    nbrDistinctTokensProcessed++;

	    if (variantsByLexWord.containsKey(wordToken)) {
		continue;
	    }

	    if (batchTokens.size() < batchSize || isLastToken) {
		batchTokens.add(wordToken);
	    }

	    if (batchTokens.size() == batchSize
		    || (isLastToken && batchTokens.size() > 0)) {
		LexWordsFromConceptNameCallable callable = new LexWordsFromConceptNameCallable(
			isMutateVariants, new TreeSet<String>(batchTokens),
			stopWords);
		lexParserSvc.submit(callable);
		totalNbrBatchSubmissions++;
		currentNbrBatchSubmissions++;
		batchTokens.clear();
	    }

	    if (currentNbrBatchSubmissions == maxNbrBatchSubmissions
		    || (isLastToken && currentNbrBatchSubmissions > 0)) {

		for (int i = 0; i < currentNbrBatchSubmissions; i++) {
		    // take() blocks so loop executes as results
		    // available
		    Future<SortedSet<Variants>> f = lexParserSvc.take();
		    SortedSet<Variants> result = f.get();
		    for (Variants variants : result) {
			for (String token : variants.variantTokens) {
			    if (variantsByLexWord.containsKey(token)) {
				variantsByLexWord.get(token)
					.addAll(variants.variantTokens);
			    } else {
				variantsByLexWord.put(token,
					variants.variantTokens);
			    }
			}
			for (String acrAbbrevToken : variants.acrAbbrevByToken
				.keySet()) {
			    acrAbbrevByToken.put(acrAbbrevToken,
				    variants.acrAbbrevByToken
					    .get(acrAbbrevToken));
			}
			nbrVariantsExtracted += variants.nbrVariantsExtracted;
			parsableTokens.addAll(variants.parsableTokens);
			nbrTokensParsable = parsableTokens.size();

		    }
		}
		currentNbrBatchSubmissions = 0;

		if (totalNbrBatchSubmissions > 0 && totalNbrBatchSubmissions
			% rptIntervalLexParse == 0) {
		    long now = System.currentTimeMillis();
		    double deltaTime = (double) (now - rptBeginSnapShot);
		    double avgPerDistinctToken = deltaTime
			    / (double) nbrDistinctTokensProcessed;
		    System.out.printf("%1$,d batch submissions, "
			    + "%2$,d distinct tokens (%3$,d parsable), "
			    + "%4$6.4f millisec/token, "
			    + "%5$,d variants extracted, " + "duration %6$s %n",
			    totalNbrBatchSubmissions,
			    nbrDistinctTokensProcessed, nbrTokensParsable,
			    avgPerDistinctToken, nbrVariantsExtracted,
			    DateTimeUtil
				    .milliToHumanReadable(now - beginSnapShot));
		    rptBeginSnapShot = System.currentTimeMillis();
		}
	    }

	}

	long now = System.currentTimeMillis();
	double totalTime = (double) (now - beginSnapShot);
	double avgPerDistinctToken = totalTime
		/ (double) nbrDistinctTokensProcessed;
	System.out.printf("%n%nFinished generating lexical variants%n");
	System.out.printf(
		"%nGrand total of %1$,d batch submissions, "
			+ "%2$,d distinct tokens (%3$,d parsable), "
			+ "%4$6.4f millisec/token, "
			+ "%5$,d variants extracted, " + "duration %6$s %n",
		totalNbrBatchSubmissions, nbrDistinctTokensProcessed,
		nbrTokensParsable, avgPerDistinctToken, nbrVariantsExtracted,
		DateTimeUtil.milliToHumanReadable(now - beginSnapShot));

    }

    private void writeVariantsToFlatFile() throws IOException {
	System.out.println(
		"Writing lexicon words, variants, and acronyms/abbreviations to flat file");
	if (variantsByLexWord.size() > 0) {
	    Path variantsPath = Paths.get(cacheFileDir, variantsFileName);
	    Files.deleteIfExists(variantsPath);
	    File variantsFile = Files.createFile(variantsPath).toFile();
	    try (BufferedWriter writer = new BufferedWriter(
		    new FileWriter(variantsFile, Charset.forName("UTF-16")))) {
		Iterator<String> lexWordIter = variantsByLexWord.keySet()
			.iterator();
		while (lexWordIter.hasNext()) {
		    String lexWordToken = lexWordIter.next();
		    StringBuilder stringBuilder = new StringBuilder();
		    stringBuilder.append(lexWordToken);
		    stringBuilder.append(LexConstants.fieldDelim);
		    SortedSet<String> variants = variantsByLexWord
			    .get(lexWordToken);
		    if (variants == null) {
			continue;
		    }
		    Iterator<String> varWordIter = variants.iterator();
		    while (varWordIter.hasNext()) {
			String varToken = varWordIter.next();
			stringBuilder.append(varToken);
			if (varWordIter.hasNext()) {
			    stringBuilder.append(LexConstants.fieldDelim);
			}
		    }
		    writer.append(stringBuilder.toString());
		    writer.newLine();
		    writer.flush();
		}
	    }
	}
	if (acrAbbrevByToken.size() > 0) {
	    Path acrAbbrevPath = Paths.get(cacheFileDir, acrAbbrevFileName);
	    Files.deleteIfExists(acrAbbrevPath);
	    File acrAbbrevFile = Files.createFile(acrAbbrevPath).toFile();
	    try (BufferedWriter writer = new BufferedWriter(
		    new FileWriter(acrAbbrevFile, Charset.forName("UTF-16")))) {
		Iterator<String> acrAbbrevTokenIter = acrAbbrevByToken.keySet()
			.iterator();
		while (acrAbbrevTokenIter.hasNext()) {
		    String acrAbbrevToken = acrAbbrevTokenIter.next();
		    String line = acrAbbrevByToken.get(acrAbbrevToken)
			    .toStringWithDelim();
		    writer.append(line);
		    writer.newLine();
		    writer.flush();
		}
	    }
	}
	if (parsableTokens.size() > 0) {
	    Path parsablePath = Paths.get(cacheFileDir, parsableFileName);
	    Files.deleteIfExists(parsablePath);
	    File parsableFile = Files.createFile(parsablePath).toFile();
	    try (BufferedWriter writer = new BufferedWriter(
		    new FileWriter(parsableFile, Charset.forName("UTF-16")))) {
		Iterator<String> parsableTokenIter = parsableTokens.iterator();
		while (parsableTokenIter.hasNext()) {
		    String parsableToken = parsableTokenIter.next();
		    writer.append(parsableToken);
		    writer.newLine();
		    writer.flush();
		}
	    }
	}
    }

    private void loadVariantsFromFlatFile() throws IOException {
	System.out.println(
		"Loading lexicon words, variants, and acronyms/abbreviations from flat file");
	// load variants
	Path variantsPath = Paths.get(cacheFileDir, variantsFileName);
	if (Files.notExists(variantsPath)) {
	    String errMsg = String.format(
		    "Flat file cache for variants (%1$s/%2$s) was not found ",
		    cacheFileDir, variantsFileName);
	    throw new IOException(errMsg);
	}
	File variantsFile = variantsPath.toFile();
	NlpUtil nlpUtil = new NlpUtil();
	try (BufferedReader reader = new BufferedReader(
		new FileReader(variantsFile, Charset.forName("UTF-16")))) {
	    String line = null;
	    while ((line = reader.readLine()) != null) {
		List<String> tokens = nlpUtil.split(line,
			LexConstants.fieldDelim);
		String wordToken = tokens.get(0);
		SortedSet<String> variants = new TreeSet<String>();
		for (int i = 1; i < tokens.size(); i++) {
		    variants.add(tokens.get(i));
		}
		variantsByLexWord.put(wordToken, variants);
	    }
	}
	Path acrAbbrevPath = Paths.get(cacheFileDir, acrAbbrevFileName);
	if (Files.notExists(variantsPath)) {
	    String errMsg = String.format(
		    "Flat file cache for acronyms/abbreviations (%1$s/%2$s) was not found ",
		    cacheFileDir, acrAbbrevFileName);
	    throw new IOException(errMsg);
	}
	// retrieve acronyms and abbreviations
	File acrAbbrevFile = acrAbbrevPath.toFile();
	try (BufferedReader reader = new BufferedReader(
		new FileReader(acrAbbrevFile, Charset.forName("UTF-16")))) {
	    String line = null;
	    while ((line = reader.readLine()) != null) {
		AcrAbbrev acrAbbrev = AcrAbbrev.fromStringWithDelim(line,
			nlpUtil);
		acrAbbrevByToken.put(acrAbbrev.getToken(), acrAbbrev);
	    }
	}
    }

    private void persistLexicon() throws Exception {
	if (variantsByLexWord == null || variantsByLexWord.size() == 0) {
	    if (variantsByLexWord == null) {
		variantsByLexWord = new TreeMap<String, SortedSet<String>>();
	    }
	    loadVariantsFromFlatFile();
	}
	deleteExisting();
	marshalLexWordsFromDb(); // required - stop words added when deleting
				 // lexicon (deleteExisting method just run)
	// compile words in lexicon
	System.out.printf(
		"%nCompiling words from variants into single set of unique words for lexicon.%n");
	SortedSet<String> lexicon = new TreeSet<String>();
	for (String lexWord : variantsByLexWord.keySet()) {
	    if (!lexWordIdByToken.containsKey(lexWord)) {
		lexicon.add(lexWord);
	    }
	    SortedSet<String> variants = variantsByLexWord.get(lexWord);
	    if (variants != null && variants.size() > 0) {
		for (String variantWord : variantsByLexWord.get(lexWord)) {
		    if (!lexWordIdByToken.containsKey(variantWord)) {
			lexicon.add(variantWord);
		    }
		}
	    }
	}
	System.out.printf("%nCompiled %1$,d unique lexicon tokens.%n",
		lexicon.size());
	System.out.println("Now persisting lexicon");
	nbrLexWordsPersisted = 0;
	int currentNbrInBatch = 0;
	int batchSize = 200;
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement persistLexWordStmt = conn
		    .prepareStatement(LexWordDao.persistSql);
		    PreparedStatement getAllLexWordsStmt = conn
			    .prepareStatement(LexWordDao.getAllSql)) {
		for (String lexWordToken : lexicon) {
		    persistLexWordStmt.setString(1, lexWordToken);
		    persistLexWordStmt.setBoolean(2,
			    nlpUtil.isPunctOrDelimChar(lexWordToken));
		    persistLexWordStmt.setBoolean(3,
			    acrAbbrevByToken.containsKey(lexWordToken));
		    persistLexWordStmt.setBoolean(4, stopWords
			    .getStopWordTokens().contains(lexWordToken));
		    persistLexWordStmt.setInt(5, 0);
		    persistLexWordStmt.addBatch();
		    nbrLexWordsPersisted++;
		    currentNbrInBatch++;
		    if (currentNbrInBatch == batchSize) {
			persistLexWordStmt.executeBatch();
			currentNbrInBatch = 0;
		    }
		    if (nbrLexWordsPersisted % rptIntervalLexPersist == 0) {
			System.out.printf("Persisted %1$,d lexicon words. %n",
				nbrLexWordsPersisted);
		    }
		}
		if (currentNbrInBatch > 0) {
		    persistLexWordStmt.executeBatch();
		}
	    }
	}

	// now marshal all into map of word ID by token
	lexWordIdByToken = LexWordDao.marshalAllIdsByToken(connPool);

	System.out.printf("Persisted grand total of %1$,d lexicon words. %n",
		nbrLexWordsPersisted);

	System.out.println("Now persisting acronyms and abbreviations");
	AcrAbbrevDao acrAbbrevDao = new AcrAbbrevDao();
	for (String acrAbbrevToken : acrAbbrevByToken.keySet()) {
	    AcrAbbrev acrAbbrev = acrAbbrevByToken.get(acrAbbrevToken);
	    acrAbbrev.setLexWordId(lexWordIdByToken.get(acrAbbrevToken));
	    acrAbbrevDao.persist(acrAbbrev, connPool);
	}
	System.out.printf("Persisted %1$,d acronyms and abbreviations. %n",
		acrAbbrevByToken.size());

    }

    private void persistVariants() throws InterruptedException,
	    ExecutionException, IOException, SQLException {
	if (variantsByLexWord == null || variantsByLexWord.size() == 0) {
	    if (variantsByLexWord == null) {
		variantsByLexWord = new TreeMap<String, SortedSet<String>>();
	    }
	    loadVariantsFromFlatFile();
	}
	marshalLexWordsFromDb();
	long beginSnapShot = System.currentTimeMillis();
	long rptBeginSnapShot = beginSnapShot;

	SortedMap<Integer, SortedSet<Integer>> batchVarIdsByWordId = new TreeMap<Integer, SortedSet<Integer>>();

	// delete existing records
	LexWordVariantDao.deleteExisting(connPool);

	int nbrLexWords = lexWordIdByToken.keySet().size();
	int currentLexWordNbr = 0;
	totalNbrBatchSubmissions = 0;
	currentNbrBatchSubmissions = 0;
	nbrLexVariantsPersisted = 0;
	for (String lexWordToken : lexWordIdByToken.keySet()) {

	    Integer lexWordId = lexWordIdByToken.get(lexWordToken);
	    SortedSet<String> lexVariantTokens = variantsByLexWord
		    .get(lexWordToken);
	    if (lexVariantTokens == null || lexVariantTokens.size() == 0) {
		continue;
	    }
	    SortedSet<Integer> lexVariantIds = new TreeSet<Integer>();
	    for (String lexVariantToken : lexVariantTokens) {
		Integer lexVariantId = lexWordIdByToken.get(lexVariantToken);
		if (lexVariantId == null) {
		    String errMsg = String.format(
			    "Lex word ID not found for token = %1$s",
			    lexVariantToken);
		    throw new SQLException(errMsg);
		}
		lexVariantIds.add(lexVariantId);
	    }

	    currentLexWordNbr++;
	    boolean isLastLexWord = currentLexWordNbr < nbrLexWords == false
		    ? true
		    : false;

	    if (batchVarIdsByWordId.keySet().size() < batchSize
		    || isLastLexWord) {
		batchVarIdsByWordId.put(lexWordId, lexVariantIds);
	    }

	    if (batchVarIdsByWordId.keySet().size() == batchSize
		    || (isLastLexWord
			    && batchVarIdsByWordId.keySet().size() > 0)) {
		SortedMap<Integer, SortedSet<Integer>> batchData = new TreeMap<Integer, SortedSet<Integer>>();
		for (Integer wordId : batchVarIdsByWordId.keySet()) {
		    batchData.put(wordId, batchVarIdsByWordId.get(wordId));
		}
		LexWordVariantsPersistCallable callable = new LexWordVariantsPersistCallable(
			batchData, connPool);
		lexVarPersistSvc.submit(callable);

		totalNbrBatchSubmissions++;
		currentNbrBatchSubmissions++;
		batchVarIdsByWordId.clear();

	    }

	    if (currentNbrBatchSubmissions == maxNbrBatchSubmissions
		    || (isLastLexWord && currentNbrBatchSubmissions > 0)) {
		for (int i = 0; i < currentNbrBatchSubmissions; i++) {
		    Future<Integer> f = lexVarPersistSvc.take();
		    Integer result = f.get();
		    nbrLexVariantsPersisted += result;
		}
		currentNbrBatchSubmissions = 0;

		if (totalNbrBatchSubmissions % rptIntervalLexParse == 0) {
		    long now = System.currentTimeMillis();
		    double deltaTime = (double) (now - rptBeginSnapShot);
		    double avgPerDistinctToken = deltaTime
			    / (double) nbrLexVariantsPersisted;
		    System.out.printf("%1$,d batch submissions, "
			    + "%2$,d variant word combinations @ %3$5.2f millisec/combination, "
			    + "duration %4$s %n", totalNbrBatchSubmissions,
			    nbrLexVariantsPersisted, avgPerDistinctToken,
			    DateTimeUtil
				    .milliToHumanReadable(now - beginSnapShot));
		    rptBeginSnapShot = System.currentTimeMillis();
		}
	    }

	}

	long now = System.currentTimeMillis();
	double deltaTime = (double) (now - rptBeginSnapShot);
	double avgPerDistinctToken = deltaTime
		/ (double) nbrLexVariantsPersisted;
	System.out.printf("%n%nGrand total of %1$,d batch submissions, "
		+ "%2$,d variant word combinations @ %3$5.2f millisec/combination, "
		+ "duration %4$s %n", totalNbrBatchSubmissions,
		nbrLexVariantsPersisted, avgPerDistinctToken,
		DateTimeUtil.milliToHumanReadable(now - beginSnapShot));
    }

    private void marshalLexWordsFromDb() throws SQLException {
	lexWordIdByToken = new TreeMap<String, Integer>();
	try (Connection conn = connPool.getConnection()) {
	    String getSql = "select id, token from conceptrecogn.lexwords";
	    try (PreparedStatement getStmt = conn.prepareStatement(getSql)) {
		try (ResultSet rs = getStmt.executeQuery()) {
		    while (rs.next()) {
			int lexWordId = rs.getInt(1);
			String lexWordToken = rs.getString(2);
			lexWordIdByToken.put(lexWordToken, lexWordId);
		    }
		}
	    }
	}
	System.out.printf("%nMarshaled %1$,d lexicon words from database.%n",
		lexWordIdByToken.size());
    }

    protected void shutdownAndAwaitTermination(ExecutorService pool) {
	// pool.shutdown(); // Disable new tasks from being submitted
	pool.shutdownNow();
	try {
	    // Wait a while for existing tasks to terminate
	    if (!pool.awaitTermination(5, TimeUnit.SECONDS)) {
		pool.shutdownNow(); // Cancel currently executing tasks
		// Wait a while for tasks to respond to being cancelled
		if (!pool.awaitTermination(15, TimeUnit.SECONDS))
		    System.err.println("Pool did not terminate");
	    }
	} catch (InterruptedException ie) {
	    // (Re-)Cancel if current thread also interrupted
	    pool.shutdownNow();
	    // Preserve interrupt status
	    Thread.currentThread().interrupt();
	}
    }

    @Override
    public void close() throws Exception {
	this.shutdownAndAwaitTermination(pool);
	System.out.println("Parallel processor closed");
    }

    public static void main(String[] args) {

	try {

	    StepToRunWordsFromConceptNames[] stepsToRun = new StepToRunWordsFromConceptNames[] {
		    StepToRunWordsFromConceptNames.DELETE_EXISTING,
		    StepToRunWordsFromConceptNames.COMPILE_VARIANTS,
		    StepToRunWordsFromConceptNames.PERSIST_LEXICON,
		    StepToRunWordsFromConceptNames.PERSIST_VARIANTS };
	    JdbcConnectionPool connPool = new JdbcConnectionPool(
		    ConnDbName.CONCEPT_RECOGN);
	    boolean isMutateVariants = true;

	    try (LexWordsFromConceptNameWorker worker = new LexWordsFromConceptNameWorker(
		    stepsToRun, isMutateVariants, connPool)) {
		worker.run();
	    }

	} catch (Exception ex) {
	    ex.printStackTrace();
	}

    }

}

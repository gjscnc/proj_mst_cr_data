/**
 * 
 */
/**
 * Components for parsing and persisting lexicon words and variants.
 * 
 * @author gjs
 *
 */
package edu.mst.db.ontol.words.persist;
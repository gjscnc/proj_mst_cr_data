package edu.mst.db.ontol.words.persist;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.SortedMap;
import java.util.TreeMap;

import edu.mst.db.util.JdbcConnectionPool;

public class CountLexWords {

    private JdbcConnectionPool connPool = null;

    public CountLexWords(JdbcConnectionPool connPool) {
	this.connPool = connPool;
    }

    public void updateCount() throws SQLException {

	SortedMap<Integer, Integer> countsByWordId = new TreeMap<Integer, Integer>();

	try (Connection conn = connPool.getConnection()) {
	    String getCountsSql = "select lexWordId, count(*) as countForWord "
		    + "from conceptrecogn.conceptnamewords group by lexWordId order by lexWordId";
	    String updateSql = "update conceptrecogn.lexwords set countForAllNames = ? where id = ?";
	    try (PreparedStatement getCountsStmt = conn
		    .prepareStatement(getCountsSql);
		    PreparedStatement updateStmt = conn
			    .prepareStatement(updateSql)) {
		System.out.println("Retrieving counts for each lexicon word");
		try (ResultSet rs = getCountsStmt.executeQuery()) {
		    while (rs.next()) {
			int lexWordId = rs.getInt(1);
			int count = rs.getInt(2);
			countsByWordId.put(lexWordId, count);
		    }
		}
		System.out.printf("Retrieved count for %1$,d lexicon words.%n",
			countsByWordId.size());
		System.out.println("Now updating count for each lexicon word");
		Iterator<Integer> lexWordIdIter = countsByWordId.keySet()
			.iterator();
		int nbrInBatch = 0;
		int batchSize = 10000;
		int nbrUpdated = 0;
		while (lexWordIdIter.hasNext()) {
		    Integer lexWordId = lexWordIdIter.next();
		    Integer count = countsByWordId.get(lexWordId);
		    updateStmt.setInt(1, count);
		    updateStmt.setInt(2, lexWordId);
		    updateStmt.addBatch();
		    if (nbrInBatch == batchSize) {
			updateStmt.executeBatch();
			nbrUpdated += nbrInBatch;
			nbrInBatch = 0;
			System.out.printf(
				"Updated count for %1$,d lexicon words.%n",
				nbrUpdated);
		    }
		}
		updateStmt.executeBatch();
		System.out.printf(
			"Updated count for grand total of %1$,d lexicon words.%n",
			nbrUpdated);
	    }
	}

    }

    public static void main(String[] args) {

    }
}

package edu.mst.db.ontol.words.persist;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Arrays;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import edu.mst.db.lexicon.StopWords;
import edu.mst.db.util.ConnDbName;
import edu.mst.db.util.DateTimeUtil;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * Parses words from concept names and persists the word sequence in the
 * database.
 * <p>
 * Do this after extracting the lexicon using
 * {@link LexWordsFromConceptNameWorker}.
 * 
 * @author gjs
 *
 */
public class ParseConceptWordsWorker implements AutoCloseable {

    private boolean deleteExisting = false;
    private JdbcConnectionPool connPool;

    private static int nbrOfProcessors;
    private static ExecutorService pool;
    private CompletionService<ParseConceptWordsResult> svc;

    private int nbrSourceNames = 0;
    private long[] sourceNameUids;

    private StopWords stopWords = null;

    private int batchSize = 50;
    private int currentPos = 0;
    private int maxPos = 0;
    private int startPos = 0;

    private int rptProgressInterval = 50;
    private int nbrNamesAnalyzed = 0;
    private int nbrNewNameWords = 0;

    public ParseConceptWordsWorker(boolean deleteExisting,
	    JdbcConnectionPool connPool) throws Exception {

	this.deleteExisting = deleteExisting;
	this.connPool = connPool;
	stopWords = new StopWords(connPool);

	nbrOfProcessors = Runtime.getRuntime().availableProcessors();
	pool = Executors.newFixedThreadPool(nbrOfProcessors *= 3);
	svc = new ExecutorCompletionService<ParseConceptWordsResult>(pool);

    }

    public void persist() throws Exception {

	retrieveConceptNameIds();

	/*
	 * Need to adjust delete process to remove only those records associated
	 * with the specified corpus. Until then, delete records for each corpus
	 * manually via sql
	 */

	if (deleteExisting) {
	    System.out.println("Deleting all name word records");
	    try (Connection conn = connPool.getConnection()) {
		String deleteSql = "truncate table conceptrecogn.conceptnamewords";
		try (PreparedStatement deleteStmt = conn
			.prepareStatement(deleteSql)) {
		    deleteStmt.executeUpdate();
		}
	    }
	    System.out.println("All name words deleted");
	}

	/*
	 * now iterate over each name to parse name into words
	 */
	nbrNamesAnalyzed = 0;
	System.out.println("Now parsing concept names into words");

	long timeBegin = System.currentTimeMillis();
	while (currentPos < nbrSourceNames) {

	    maxPos = getNextBatchMaxPos();
	    startPos = currentPos;
	    for (int pos = startPos; pos <= maxPos; pos++) {
		long uid = sourceNameUids[pos];
		ParseConceptWordsCallable callable = new ParseConceptWordsCallable(
			uid, stopWords, connPool);
		svc.submit(callable);
	    }
	    for (int pos = startPos; pos <= maxPos; pos++) {
		nbrNamesAnalyzed++;
		Future<ParseConceptWordsResult> f = svc.take();
		ParseConceptWordsResult result = f.get();
		nbrNewNameWords += result.nbrNewNameWords;
		if (nbrNamesAnalyzed % rptProgressInterval == 0) {
		    long timeDiff = System.currentTimeMillis() - timeBegin;
		    double avgMilliPerName = (double) timeDiff
			    / (double) nbrNamesAnalyzed;
		    int nbrRemaining = nbrSourceNames - nbrNamesAnalyzed;
		    long milliRemaining = (long) nbrRemaining
			    * (long) avgMilliPerName;
		    System.out.printf(
			    "Parsed %1$,d concept source names, "
				    + "avg of %2$.4f millisec. per name, "
				    + "%3$,d remaining (%4$s to finish). "
				    + "Cumulative new name words = %5$,d %n",
			    nbrNamesAnalyzed, avgMilliPerName, nbrRemaining,
			    DateTimeUtil.milliToHumanReadable(milliRemaining),
			    nbrNewNameWords);
		}
	    }
	    currentPos = maxPos + 1;
	}

	System.out.printf(
		"Parsed grand total of %1$,d concept source names, created "
			+ "%2$,d new name words %n",
		nbrNamesAnalyzed, nbrNewNameWords);

    }

    private int getNextBatchMaxPos() throws Exception {
	// subtract 1 from max pos since positions are inclusive
	int maxPos = currentPos + batchSize - 1;
	if (maxPos >= nbrSourceNames - 1)
	    maxPos = nbrSourceNames - 1;
	return maxPos;
    }

    private void retrieveConceptNameIds() throws Exception {

	try (Connection conn = connPool.getConnection()) {
	    String countNamesSql = "select count(*) from conceptrecogn.conceptnames ";
	    String getNameUidsSql = "select uid from conceptrecogn.conceptnames ";
	    try (PreparedStatement countNamesStmt = conn
		    .prepareStatement(countNamesSql);
		    PreparedStatement getNameUidsStmt = conn
			    .prepareStatement(getNameUidsSql)) {
		try (ResultSet rs = countNamesStmt.executeQuery()) {
		    if (rs.next()) {
			nbrSourceNames = rs.getInt(1);
		    }
		}
		if (nbrSourceNames == 0) {
		    throw new Exception(
			    "Could not retrieve PMIDs from database.");
		}
		sourceNameUids = new long[nbrSourceNames];
		try (ResultSet rs = getNameUidsStmt.executeQuery()) {
		    int pos = 0;
		    while (rs.next()) {
			sourceNameUids[pos] = rs.getLong(1);
			pos++;
		    }
		}
	    }
	}

	Arrays.sort(sourceNameUids);
	System.out.printf(
		"Retrieved total of %1$,d concept name UIDs from database.%n",
		nbrSourceNames);
    }

    protected void shutdownAndAwaitTermination(ExecutorService pool) {
	// pool.shutdown(); // Disable new tasks from being submitted
	pool.shutdownNow();
	try {
	    // Wait a while for existing tasks to terminate
	    if (!pool.awaitTermination(5, TimeUnit.SECONDS)) {
		pool.shutdownNow(); // Cancel currently executing tasks
		// Wait a while for tasks to respond to being cancelled
		if (!pool.awaitTermination(15, TimeUnit.SECONDS))
		    System.err.println("Pool did not terminate");
	    }
	} catch (InterruptedException ie) {
	    // (Re-)Cancel if current thread also interrupted
	    pool.shutdownNow();
	    // Preserve interrupt status
	    Thread.currentThread().interrupt();
	}
    }

    @Override
    public void close() throws Exception {
	this.shutdownAndAwaitTermination(pool);
	System.out.println("Parallel processor closed");
    }

    public static void main(String[] args) throws Exception {

	JdbcConnectionPool connPool = new JdbcConnectionPool(
		ConnDbName.CONCEPT_RECOGN);
	boolean deleteExisting = true;

	try (ParseConceptWordsWorker worker = new ParseConceptWordsWorker(
		deleteExisting, connPool)) {
	    worker.persist();
	}

    }

}

package edu.mst.db.ontol.words.persist;

import java.sql.SQLException;

public class ParseConceptWordsResult {

    public long sourceNameUid = 0L;
    public String sourceNameText = "";
    public int nbrNewNameWords = 0;
    public SQLException ex = null;

}

/**
 * 
 */
package edu.mst.db.ontol.words.persist;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.concurrent.Callable;

import edu.mst.db.lexicon.LexWordVariantDao;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * Callable for concurrent processing to persist lexicon variants.
 * <p>
 * Worker class is {@link edu.mst.db.ontol.words.persist.LexWordsFromConceptNameWorker}
 * 
 * @author gjs
 *
 */
public class LexWordVariantsPersistCallable implements Callable<Integer> {

    private SortedMap<Integer, SortedSet<Integer>> batchVarIdsByWordId = null;
    private JdbcConnectionPool connPool = null;

    public LexWordVariantsPersistCallable(
	    SortedMap<Integer, SortedSet<Integer>> batchVarIdsByWordId,
	    JdbcConnectionPool connPool) {
	this.batchVarIdsByWordId = batchVarIdsByWordId;
	this.connPool = connPool;
    }

    @Override
    public Integer call() throws Exception {
	Integer nbrVariantsPersisted = 0;
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement persistStmt = conn
		    .prepareStatement(LexWordVariantDao.persistIdsSql)) {
		for (Integer lexWordId : batchVarIdsByWordId.keySet()) {
		    for (int variantWordId : batchVarIdsByWordId.get(lexWordId)) {
			persistStmt.setInt(1, lexWordId);
			persistStmt.setInt(2, variantWordId);
			persistStmt.addBatch();
			nbrVariantsPersisted++;
		    }
		}
		persistStmt.executeBatch();
	    }
	}
	return nbrVariantsPersisted;
    }

}

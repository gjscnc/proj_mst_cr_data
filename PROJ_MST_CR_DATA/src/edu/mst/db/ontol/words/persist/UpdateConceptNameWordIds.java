package edu.mst.db.ontol.words.persist;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import edu.mst.db.lexicon.LexWord;
import edu.mst.db.lexicon.LexWordDao;
import edu.mst.db.util.ConnDbName;
import edu.mst.db.util.JdbcConnectionPool;

public class UpdateConceptNameWordIds {

    private UpdateOptions updateOptions = null;
    private JdbcConnectionPool connPool = null;
    private SortedMap<String, LexWord> wordsByToken = null;
    private SortedSet<Long> sourceNameUids = new TreeSet<Long>();
    private SortedMap<Integer, Integer> lexWordCountByUid = new TreeMap<Integer, Integer>();

    private int batchSize = 10000;
    private int rptInterval = 5000;
    private int currentNbrInBatch = 0;
    private int nbrNamesProcessed = 0;
    private int nbrWordsUpdated = 0;

    public UpdateConceptNameWordIds(UpdateOptions updateOptions,
	    JdbcConnectionPool connPool) throws SQLException {
	this.updateOptions = updateOptions;
	this.connPool = connPool;
	System.out.println("Retrieve all words in lexicon");
	wordsByToken = LexWordDao.marshalAllWordsByToken(connPool);
	System.out.printf("Retrieved %1$,d lexicon words%n",
		wordsByToken.size());
    }

    public void updateWordIds() throws SQLException {

	// retrieve all concept name UIDs
	System.out.println("Retrieving concept name UIDs");
	retrieveConceptNameIds();

	System.out.println(
		"Now updating lexicon data for words in concept names");

	Iterator<Long> nameUidIter = sourceNameUids.iterator();
	try (Connection conn = connPool.getConnection()) {
	    String marshalWordsForNameSql = "SELECT wordNbr, token "
		    + "FROM conceptrecogn.conceptnamewords where conceptNameUid = ? order by wordNbr";
	    String updateLexWordSql = "update conceptrecogn.conceptnamewords set lexWordId = ?, isAcrAbbrev = ?, "
		    + "isStopWord = ?, isPunct = ? where conceptNameUid = ? and wordNbr = ?";
	    try (PreparedStatement marshalWordsForNameStmt = conn
		    .prepareStatement(marshalWordsForNameSql);
		    PreparedStatement updateLexWordStmt = conn
			    .prepareStatement(updateLexWordSql)) {
		while (nameUidIter.hasNext()) {
		    long nameUid = nameUidIter.next();
		    marshalWordsForNameStmt.setLong(1, nameUid);
		    try (ResultSet rs = marshalWordsForNameStmt
			    .executeQuery()) {
			while (rs.next()) {
			    int wordNbr = rs.getInt(1);
			    String tokenLc = rs.getString(2).toLowerCase();
			    if(!wordsByToken.containsKey(tokenLc)) {
				continue;
			    }
			    LexWord lexWord = wordsByToken.get(tokenLc);
			    updateLexWordStmt.setInt(1, lexWord.getId());
			    updateLexWordStmt.setBoolean(2,
				    lexWord.isAcrAbbrev());
			    updateLexWordStmt.setBoolean(3,
				    lexWord.isStopWord());
			    updateLexWordStmt.setBoolean(4, lexWord.isPunct());
			    updateLexWordStmt.setLong(5, nameUid);
			    updateLexWordStmt.setInt(6, wordNbr);
			    updateLexWordStmt.addBatch();
			    nbrWordsUpdated++;
			    currentNbrInBatch++;
			    if (currentNbrInBatch == batchSize) {
				updateLexWordStmt.executeBatch();
				currentNbrInBatch = 0;
			    }
			}
		    }
		    nbrNamesProcessed++;
		    if (nbrNamesProcessed % rptInterval == 0) {
			System.out.printf(
				"Processed %1$,d names, updated %2$,d words%n",
				nbrNamesProcessed, nbrWordsUpdated);
		    }
		}
		updateLexWordStmt.executeBatch();
	    }
	}

	System.out.printf("%n%nUpdating count of words in concept names%n%n");
	try (Connection conn = connPool.getConnection()) {
	    String countSql = "select lexWordId, count(*) from conceptrecogn.conceptnamewords "
		    + "group by lexWordId order by lexWordId";
	    String updateCountSql = "update conceptrecogn.lexwords set countForAllNames = ? where id = ?";
	    try (PreparedStatement countStmt = conn.prepareStatement(countSql);
		    PreparedStatement updateStmt = conn
			    .prepareStatement(updateCountSql)) {
		System.out.println(
			"Counting number of occurrences of each word in concept names.");
		try (ResultSet rs = countStmt.executeQuery()) {
		    while (rs.next()) {
			int lexWordId = rs.getInt(1);
			int count = rs.getInt(2);
			lexWordCountByUid.put(lexWordId, count);
		    }
		}
		System.out.println("Updating lexicon database");
		int updateBatchSize = 1000;
		int currentNbrInBatch = 0;
		int totalNbrUpdates = 0;
		for (Integer lexWordUid : lexWordCountByUid.keySet()) {
		    updateStmt.setInt(1, lexWordCountByUid.get(lexWordUid));
		    updateStmt.setInt(2, lexWordUid);
		    updateStmt.addBatch();
		    currentNbrInBatch++;
		    totalNbrUpdates++;
		    if (currentNbrInBatch == updateBatchSize) {
			updateStmt.executeBatch();
			System.out.printf(
				"Updated word count for %1$,d lexicon words%n",
				totalNbrUpdates);
			currentNbrInBatch = 0;
		    }
		}
		updateStmt.executeBatch();
		System.out.printf(
			"Updated word count for grand total of %1$,d lexicon words%n",
			totalNbrUpdates);
	    }
	}
    }

    private void retrieveConceptNameIds() throws SQLException {

	String getNameUidsSql = null;
	switch(updateOptions) {
	case CONTINUE:
	    getNameUidsSql = "select distinct conceptNameUid from "
	    	+ "conceptrecogn.conceptnamewords where lexWordId = 0 ";
	    break;
	case RESTART:
	    getNameUidsSql = "select uid from conceptrecogn.conceptnames order by uid";
	    break;
	}
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement getNameUidsStmt = conn
		    .prepareStatement(getNameUidsSql)) {
		try (ResultSet rs = getNameUidsStmt.executeQuery()) {
		    while (rs.next()) {
			sourceNameUids.add(rs.getLong(1));
		    }
		}
	    }
	}

	System.out.printf(
		"Retrieved total of %1$,d concept name UIDs from database.%n",
		sourceNameUids.size());
    }

    public static void main(String[] args) {
	try {
	    JdbcConnectionPool connPool = new JdbcConnectionPool(
		    ConnDbName.CONCEPT_RECOGN);
	    UpdateConceptNameWordIds updater = new UpdateConceptNameWordIds(
		    UpdateOptions.CONTINUE, connPool);
	    updater.updateWordIds();

	} catch (Exception ex) {
	    ex.printStackTrace();
	}
    }

    public enum UpdateOptions {
	RESTART, CONTINUE;
    }

}

package edu.mst.db.ontol.words.persist;

import java.util.List;
import java.util.concurrent.Callable;

import edu.mst.db.lexicon.StopWords;
import edu.mst.db.ontol.names.ConceptName;
import edu.mst.db.ontol.names.ConceptNameDao;
import edu.mst.db.ontol.words.ConceptNameWord;
import edu.mst.db.ontol.words.ConceptNameWordDao;
import edu.mst.db.text.util.NlpUtil;
import edu.mst.db.text.util.StanfordParser;
import edu.mst.db.util.JdbcConnectionPool;

public class ParseConceptWordsCallable implements Callable<ParseConceptWordsResult> {

    private long sourceNameUid = 0L;
    private JdbcConnectionPool connPool = null;
    private StopWords stopWords = null;
    private NlpUtil nlpUtil = null;
    private StanfordParser stanfordWordParse = null;
    private ConceptNameDao sourceNameDao = null;
    private ConceptNameWordDao nameWordDao = null;

    public ParseConceptWordsCallable(long sourceNameUid, StopWords stopWords,
	    JdbcConnectionPool connPool) throws Exception {
	this.sourceNameUid = sourceNameUid;
	this.stopWords = stopWords;
	this.connPool = connPool;
	nlpUtil = new NlpUtil();
	stanfordWordParse = new StanfordParser(connPool);
	sourceNameDao = new ConceptNameDao();
	nameWordDao = new ConceptNameWordDao();
    }

    @Override
    public ParseConceptWordsResult call() throws Exception {

	ParseConceptWordsResult result = new ParseConceptWordsResult();
	result.sourceNameUid = sourceNameUid;

	ConceptName sourceName = sourceNameDao.marshal(sourceNameUid,
		connPool);

	// parse name into words
	List<ConceptNameWord> words = stanfordWordParse.parseNameWords(
		sourceName.getNameText(), sourceName.getUid(),
		sourceName.getConceptUid());

	// for each word, determine if it is an acronym or abbreviation
	for (ConceptNameWord word : words) {
	    String wordTokenLc = word.getToken().toLowerCase();
	    boolean isStopWord = stopWords.getLexWordsByToken()
		    .containsKey(wordTokenLc);
	    word.setIsStopWord(isStopWord);
	    boolean isPunct = nlpUtil.isPunctOrDelimChar(wordTokenLc);
	    word.setIsPunct(isPunct);
	    nameWordDao.persist(word, connPool);
	    result.nbrNewNameWords++;
	}

	return result;
    }

}

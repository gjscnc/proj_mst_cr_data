package edu.mst.db.ontol.words;

import java.util.Comparator;

/**
 * This class instantiates a word from a concept name and provides methods for
 * persisting in the database. Formatting may occur depending upon the method
 * for instantiating. Always use this class for instantiating words in concept
 * names.
 * 
 * @author George J. Shannon
 *
 */
public class ConceptNameWord implements Comparable<ConceptNameWord> {

    private long conceptNameUid = 0;
    private int wordNbr = 0;
    private long conceptUid;
    private String token = "";
    private int beginPos = 0;
    private int endPos = 0;
    private int lexWordId = 0;
    private boolean isAcrAbbrev = false;
    private boolean isStopWord = false;
    private boolean isPunct = false;

    /**
     * Default constructor
     */
    public ConceptNameWord() {
    }

    /**
     * Constructor for new word not yet persisted.
     * 
     * @param conceptNameUid
     * @param wordNbr
     * @param beginPos
     * @param endPos
     * @param conceptUid
     * @param token
     */
    public ConceptNameWord(long conceptNameUid, int wordNbr, int beginPos,
	    int endPos, long conceptUid, String token) {
	this.conceptNameUid = conceptNameUid;
	this.wordNbr = wordNbr;
	this.beginPos = beginPos;
	this.endPos = endPos;
	this.conceptUid = conceptUid;
	this.token = token;
    }

    /**
     * Constructor for instantiating word previously persisted.
     * 
     * @param conceptNameUid
     * @param nameWordNbr
     * @param beginPos
     * @param endPos
     * @param conceptUid
     * @param token
     * @param lexWordId
     * @param isAcrAbbrev
     * @param acrAbbrevId
     * @param isStopWord
     * @param isPunct
     */
    public ConceptNameWord(int conceptNameUid, int nameWordNbr, int beginPos,
	    int endPos, long conceptUid, String token, int lexWordId,
	    boolean isAcrAbbrev,
	    boolean isStopWord, boolean isPunct) {
	this.conceptNameUid = conceptNameUid;
	this.wordNbr = nameWordNbr;
	this.beginPos = beginPos;
	this.endPos = endPos;
	this.token = token;
	this.lexWordId = lexWordId;
	this.isAcrAbbrev = isAcrAbbrev;
	this.isStopWord = isStopWord;
	this.isPunct = isPunct;
    }

    @Override
    public int compareTo(ConceptNameWord word) {
	return Comparators.byNameUid_WordNbr.compare(this, word);
    }

    public static class Comparators {
	public static final Comparator<ConceptNameWord> byNameUid_WordNbr = (
		ConceptNameWord w1, ConceptNameWord w2) -> {
	    int i = Long.compare(w1.conceptNameUid, w2.conceptNameUid);
	    if (i != 0) {
		return i;
	    }
	    return Integer.compare(w1.wordNbr, w2.wordNbr);
	};
    }

    @Override
    public boolean equals(Object obj) {
	if (obj instanceof ConceptNameWord) {
	    ConceptNameWord word = (ConceptNameWord) obj;
	    return conceptNameUid == word.conceptNameUid
		    && wordNbr == word.wordNbr;
	} else
	    return false;
    }

    @Override
    public int hashCode() {
	int prime = 31;
	long result = 1;
	result = prime * conceptNameUid + result;
	result = prime * wordNbr + result;
	return Long.valueOf(result).hashCode();
    }

    public long getConceptNameUid() {
        return conceptNameUid;
    }

    public void setConceptNameUid(long conceptNameUid) {
        this.conceptNameUid = conceptNameUid;
    }

    public int getWordNbr() {
        return wordNbr;
    }

    public void setWordNbr(int wordNbr) {
        this.wordNbr = wordNbr;
    }

    public long getConceptUid() {
        return conceptUid;
    }

    public void setConceptUid(long conceptUid) {
        this.conceptUid = conceptUid;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getBeginPos() {
        return beginPos;
    }

    public void setBeginPos(int beginPos) {
        this.beginPos = beginPos;
    }

    public int getEndPos() {
        return endPos;
    }

    public void setEndPos(int endPos) {
        this.endPos = endPos;
    }

    public int getLexWordId() {
        return lexWordId;
    }

    public void setLexWordId(int lexWordId) {
        this.lexWordId = lexWordId;
    }

    public boolean isAcrAbbrev() {
        return isAcrAbbrev;
    }

    public void setIsAcrAbbrev(boolean isAcrAbbrev) {
        this.isAcrAbbrev = isAcrAbbrev;
    }

    public boolean isStopWord() {
        return isStopWord;
    }

    public void setIsStopWord(boolean isStopWord) {
        this.isStopWord = isStopWord;
    }

    public boolean isPunct() {
        return isPunct;
    }

    public void setIsPunct(boolean isPunct) {
        this.isPunct = isPunct;
    }

}

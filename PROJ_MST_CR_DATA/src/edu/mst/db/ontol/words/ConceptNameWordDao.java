package edu.mst.db.ontol.words;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import edu.mst.db.util.JdbcConnectionPool;

public class ConceptNameWordDao {

    public static final String persistSql = "INSERT INTO conceptrecogn.conceptnamewords "
	    + "(conceptNameUid, wordNbr, token, beginPos, endPos, conceptUid, lexWordId, "
	    + "isAcrAbbrev, isStopWord, isPunct) "
	    + "VALUES(?,?,?,?,?,?,?,?,?,?)";

    public static final String marshalWordSql = "SELECT token, beginPos, endPos, conceptUid, lexWordId, "
	    + "isAcrAbbrev, isStopWord, isPunct "
	    + "FROM conceptrecogn.conceptnamewords where conceptNameUid = ? and wordNbr = ?";

    public static final String marshalWordsForNameSql = "SELECT wordNbr, token, beginPos, endPos, conceptUid, lexWordId, "
	    + "isAcrAbbrev, isStopWord, isPunct "
	    + "FROM conceptrecogn.conceptnamewords where conceptNameUid = ? order by wordNbr";

    public static final String marshalWordsIdsSql = "SELECT distinct lexWordId FROM conceptrecogn.conceptnamewords order by lexWordId";

    public static final String getNameIdsWithWordSql = "select distinct conceptNameUid "
	    + "from conceptrecogn.conceptnamewords where lexWordId = ?";

    public static final String getNbrWordsInNameSql = "select max(wordNbr) from conceptrecogn.conceptnamewords "
	    + "where conceptNameUid = ?";

    public static final String getAcrAbbrevSql = "select w.conceptNameUid, n.conceptUid, n.nameText, w.firstWordToken from "
	    + "(select uid, conceptUid, nameText from conceptrecogn.conceptnames where isAcrAbbrev = TRUE and conceptUid = ?) n, "
	    + "(select conceptNameUid, token as firstWordToken from conceptrecogn.conceptnamewords where wordNbr=0 and lexWordId = ?) w "
	    + "where n.uid = w.conceptNameUid";

    public static final String getSingleWordNameNotAcrAbbrevSql = "select n.uid, w.maxWordNbr from "
	    + "(select conceptNameUid, max(wordNbr) as maxWordNbr from conceptrecogn.conceptnamewords where lexWordId = ? group by conceptNameUid ) w, "
	    + "(select uid, conceptUid from conceptrecogn.conceptnames where isAcrAbbrev = FALSE and conceptUid = ?) n "
	    + "where w.maxWordNbr = 0 and w.conceptNameUid = n.uid";

    public static final String delWordsSql = "delete from conceptrecogn.conceptnamewords where conceptNameUid=?";

    public static int[] marshalAllWordIds(JdbcConnectionPool connPool)
	    throws SQLException {
	SortedSet<Integer> wordIdsSet = new TreeSet<Integer>();
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement marshalStmt = conn
		    .prepareStatement(marshalWordsIdsSql)) {
		try(ResultSet rs = marshalStmt.executeQuery()){
		    while(rs.next()) {
			wordIdsSet.add(rs.getInt(1));
		    }
		}
	    }
	}
	int[] wordIds = new int[wordIdsSet.size()];
	int pos = 0;
	for(int wordId : wordIdsSet) {
	    wordIds[pos] = wordId;
	    pos++;
	}
	return wordIds;
    }

    public synchronized boolean persist(ConceptNameWord word,
	    JdbcConnectionPool connPool) throws SQLException {
	boolean isPersisted = false;
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement persistStmt = conn
		    .prepareStatement(persistSql)) {
		persistStmt.setLong(1, word.getConceptNameUid());
		persistStmt.setInt(2, word.getWordNbr());
		persistStmt.setString(3, word.getToken());
		persistStmt.setInt(4, word.getBeginPos());
		persistStmt.setInt(5, word.getEndPos());
		persistStmt.setLong(6, word.getConceptUid());
		persistStmt.setInt(7, word.getLexWordId());
		persistStmt.setBoolean(8, word.isAcrAbbrev());
		persistStmt.setBoolean(9, word.isStopWord());
		persistStmt.setBoolean(10, word.isPunct());
		isPersisted = persistStmt.executeUpdate() > 0 ? true : false;
	    }
	}
	return isPersisted;
    }

    public List<ConceptNameWord> marshalWordsForName(long conceptNameUid,
	    JdbcConnectionPool connPool) throws SQLException {
	List<ConceptNameWord> words = new ArrayList<ConceptNameWord>();
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement marshalStmt = conn
		    .prepareStatement(marshalWordsForNameSql)) {
		marshalStmt.setLong(1, conceptNameUid);
		try (ResultSet rs = marshalStmt.executeQuery()) {
		    while (rs.next()) {
			int wordNbr = rs.getInt(1);
			String token = rs.getString(2);
			int beginPos = rs.getInt(3);
			int endPos = rs.getInt(4);
			long conceptUid = rs.getLong(5);
			int lexWordId = rs.getInt(6);
			boolean isAcrAbbrev = rs.getBoolean(7);
			boolean isStopWord = rs.getBoolean(8);
			boolean isPunct = rs.getBoolean(9);
			ConceptNameWord word = new ConceptNameWord(
				conceptNameUid, wordNbr, beginPos, endPos,
				conceptUid, token);
			word.setLexWordId(lexWordId);
			word.setIsAcrAbbrev(isAcrAbbrev);
			word.setIsStopWord(isStopWord);
			word.setIsPunct(isPunct);
			words.add(word);
		    }
		}
	    }
	}
	return words;
    }

    public ConceptNameWord marshalWord(long conceptNameUid, int wordNbr,
	    JdbcConnectionPool connPool) throws SQLException {
	ConceptNameWord word = null;
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement marshalStmt = conn
		    .prepareStatement(marshalWordSql)) {
		marshalStmt.setLong(1, conceptNameUid);
		marshalStmt.setInt(2, wordNbr);
		try (ResultSet rs = marshalStmt.executeQuery()) {
		    if (rs.next()) {
			String token = rs.getString(1);
			int beginPos = rs.getInt(2);
			int endPos = rs.getInt(3);
			long conceptUid = rs.getLong(4);
			int lexWordId = rs.getInt(5);
			boolean isAcrAbbrev = rs.getBoolean(6);
			boolean isStopWord = rs.getBoolean(7);
			boolean isPunct = rs.getBoolean(8);
			word = new ConceptNameWord(conceptNameUid, wordNbr,
				beginPos, endPos, conceptUid, token);
			word.setLexWordId(lexWordId);
			word.setIsAcrAbbrev(isAcrAbbrev);
			word.setIsStopWord(isStopWord);
			word.setIsPunct(isPunct);
		    }
		}
	    }
	}
	return word;
    }

    public SortedSet<Long> getNameIdsWithWord(int lexWordId,
	    JdbcConnectionPool connPool) throws SQLException {
	SortedSet<Long> nameIds = new TreeSet<Long>();
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement getNameIdsStmt = conn
		    .prepareStatement(getNameIdsWithWordSql)) {
		getNameIdsStmt.setInt(1, lexWordId);
		try (ResultSet rs = getNameIdsStmt.executeQuery()) {
		    while (rs.next()) {
			nameIds.add(rs.getLong(1));
		    }
		}
	    }
	}
	return nameIds;
    }

    public int getNbrWordsInName(long conceptNameUid,
	    JdbcConnectionPool connPool) throws SQLException {
	int nbrWords = 0;
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement getNbrWordsStmt = conn
		    .prepareStatement(getNbrWordsInNameSql)) {
		getNbrWordsStmt.setLong(1, conceptNameUid);
		try (ResultSet rs = getNbrWordsStmt.executeQuery()) {
		    if (rs.next()) {
			nbrWords = rs.getInt(1) + 1;
		    }
		}
	    }
	}
	return nbrWords;
    }

    public void deleteWords(long conceptNameUid, JdbcConnectionPool connPool)
	    throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement delStmt = conn
		    .prepareStatement(delWordsSql)) {
		delStmt.setLong(1, conceptNameUid);
		delStmt.executeUpdate();
	    }
	}
    }

    /**
     * Returns concept word if name is an acronym.
     * 
     * @param lexWordId
     *                       Lexicon word ID for acronym/abbreviation token
     * @param conceptUid
     *                       UID for concept in ontology
     * @param connPool
     * @return
     * @throws SQLException
     */
    public ConceptNameWord getAcrAbbrevWord(int lexWordId, long conceptUid,
	    JdbcConnectionPool connPool) throws SQLException {

	ConceptNameWord word = null;
	long conceptNameUid = 0L;

	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement getNameUidStmt = conn
		    .prepareStatement(getAcrAbbrevSql)) {
		getNameUidStmt.setInt(1, lexWordId);
		getNameUidStmt.setLong(2, conceptUid);
		try (ResultSet rs = getNameUidStmt.executeQuery()) {
		    if (rs.next()) {
			conceptNameUid = rs.getLong(1);
		    }
		}
	    }
	}

	if (conceptNameUid > 0L) {
	    word = marshalWord(conceptNameUid, 0, connPool);
	}

	return word;
    }

    public ConceptNameWord getSingleWordNameNotAcrAbbrev(int lexWordId,
	    long conceptUid, JdbcConnectionPool connPool) throws SQLException {

	ConceptNameWord word = null;
	long conceptNameUid = 0L;

	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement getNameUidStmt = conn
		    .prepareStatement(getSingleWordNameNotAcrAbbrevSql)) {
		getNameUidStmt.setInt(1, lexWordId);
		getNameUidStmt.setLong(2, conceptUid);
		try (ResultSet rs = getNameUidStmt.executeQuery()) {
		    if (rs.next()) {
			conceptNameUid = rs.getLong(1);
		    }
		}
	    }
	}

	if (conceptNameUid > 0L) {
	    word = marshalWord(conceptNameUid, 0, connPool);
	}

	return word;

    }

}

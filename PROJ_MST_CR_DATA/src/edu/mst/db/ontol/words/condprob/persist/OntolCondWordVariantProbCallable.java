/**
 * 
 */
package edu.mst.db.ontol.words.condprob.persist;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Set;
import java.util.concurrent.Callable;

import edu.mst.db.lexicon.LexWordVariantDao;
import edu.mst.db.ontol.words.condprob.OntolCondWordProb;
import edu.mst.db.ontol.words.condprob.OntolCondWordVariantProbDao;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * @author gjs
 *
 */
public class OntolCondWordVariantProbCallable implements Callable<Integer> {

    private OntolCondWordProb condProb = null;
    private boolean isPersist = false;
    private int persistBatchSize = 0;
    private JdbcConnectionPool connPool = null;

    private LexWordVariantDao lexVariantDao = new LexWordVariantDao();

    private int nbrNewCondProbs = 0;

    public OntolCondWordVariantProbCallable(OntolCondWordProb condProb,
	    boolean isPersist, int persistBatchSize,
	    JdbcConnectionPool connPool) {
	this.condProb = condProb;
	this.isPersist = isPersist;
	this.persistBatchSize = persistBatchSize;
	this.connPool = connPool;
    }

    @Override
    public Integer call() throws Exception {

	int originalPredWordId = condProb.getPredicateWordId();
	int originalAssumedFactId = condProb.getAssumedFactWordId();

	int[] variantPredicates = null;
	{
	    Set<Integer> variantsSet = lexVariantDao
		    .marshalVariantIdsForLexWord(originalPredWordId, connPool);
	    variantsSet.add(originalPredWordId);
	    variantPredicates = new int[variantsSet.size()];
	    int pos = 0;
	    for (int predVariantId : variantsSet) {
		variantPredicates[pos] = predVariantId;
		pos++;
	    }
	}
	int[] variantAssumedFacts = null;
	{
	    Set<Integer> variantsSet = lexVariantDao
		    .marshalVariantIdsForLexWord(originalAssumedFactId,
			    connPool);
	    variantsSet.add(originalAssumedFactId);
	    variantAssumedFacts = new int[variantsSet.size()];
	    int pos = 0;
	    for (int variantId : variantsSet) {
		variantAssumedFacts[pos] = variantId;
		pos++;
	    }
	}
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement persistStmt = conn
		    .prepareStatement(OntolCondWordVariantProbDao.persistSql)) {
		for (int i = 0; i < variantPredicates.length; i++) {
		    int variantPredWordId = variantPredicates[i];
		    for (int j = 0; j < variantAssumedFacts.length; j++) {
			int variantAssumedFactWordId = variantAssumedFacts[j];
			/*
			 * Conditions: assumed fact and predicate must not the
			 * same word
			 */
			if (variantAssumedFactWordId == variantPredWordId) {
			    continue;
			}
			/*
			 * 1-origPredWordId, 2-origAssumedFactWordId,
			 * 3-variantPredWordId, 4-variantAssumedFactWordId,
			 * 5-freq, 6-condProb, 7-lnCondProb
			 */
			persistStmt.setInt(1, originalPredWordId);
			persistStmt.setInt(2, originalAssumedFactId);
			persistStmt.setInt(3, variantPredWordId);
			persistStmt.setInt(4, variantAssumedFactWordId);
			persistStmt.setInt(5, condProb.getFreq());
			persistStmt.setDouble(6, condProb.getCondProb());
			persistStmt.setDouble(7, condProb.getLnCondProb());

			persistStmt.addBatch();
			nbrNewCondProbs++;
			if (isPersist
				&& nbrNewCondProbs % persistBatchSize == 0) {
			    persistStmt.executeBatch();
			}
		    }
		}
		if (isPersist) {
		    // pick up any stragglers
		    persistStmt.executeBatch();
		}
	    }
	}

	return nbrNewCondProbs;
    }

}

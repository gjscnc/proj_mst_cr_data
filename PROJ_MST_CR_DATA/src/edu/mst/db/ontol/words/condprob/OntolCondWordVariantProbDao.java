/**
 * 
 */
package edu.mst.db.ontol.words.condprob;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.SortedSet;
import java.util.TreeSet;

import edu.mst.db.util.JdbcConnectionPool;

/**
 * @author gjs
 *
 */
public class OntolCondWordVariantProbDao {

    public static final String allFields = "origPredWordId, origAssumedFactWordId, variantPredWordId, variantAssumedFactWordId, "
	    + "freq, condProb, lnCondProb";

    public static final String tableName = "conceptrecogn.ontolcondwordprob_variants";

    public static final String deleteExistingSql = "truncate table "
	    + tableName;

    public static final String persistSql = "insert into " + tableName + "("
	    + allFields + ") values(?,?,?,?,?,?,?)";

    public static final String marshalSql = "select " + allFields + " from "
	    + tableName + " where origPredWordId=? and origAssumedFactWordId=? "
	    + "and variantPredWordId=? and variantAssumedFactWordId=?";

    public static final String marshalForVariantsSql = "select " + allFields
	    + " from " + tableName + " where "
	    + "(origPredWordId = ? or variantPredWordId = ?) and (variantAssumedFactWordId = ? or variantAssumedFactWordId = ?)";

    /*
     * 1-origPredWordId, 2-origAssumedFactWordId, 3-variantPredWordId,
     * 4-variantAssumedFactWordId, 5-freq, 6-condProb, 7-lnCondProb
     */

    public static void deleteExisting(JdbcConnectionPool connPool)
	    throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement deleteStmt = conn
		    .prepareStatement(deleteExistingSql)) {
		deleteStmt.executeUpdate();
	    }
	}
    }

    public void persist(OntolCondWordVariantProb condProb,
	    JdbcConnectionPool connPool) throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement persistStmt = conn
		    .prepareStatement(persistSql)) {
		setPersistValues(condProb, persistStmt);
		persistStmt.executeUpdate();
	    }
	}
    }

    public void persistBatch(Collection<OntolCondWordVariantProb> condProbs,
	    JdbcConnectionPool connPool) throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement persistStmt = conn
		    .prepareStatement(persistSql)) {
		for (OntolCondWordVariantProb condProb : condProbs) {
		    setPersistValues(condProb, persistStmt);
		    persistStmt.addBatch();
		}
		persistStmt.executeBatch();
	    }
	}
    }

    public void setPersistValues(OntolCondWordVariantProb condProb,
	    PreparedStatement persistStmt) throws SQLException {
	/*
	 * 1-origPredWordId, 2-origAssumedFactWordId, 3-variantPredWordId,
	 * 4-variantAssumedFactWordId, 5-freq, 6-condProb, 7-lnCondProb
	 */
	persistStmt.setInt(1, condProb.getOrigPredWordId());
	persistStmt.setInt(2, condProb.getOrigAssumedFactWordId());
	persistStmt.setInt(3, condProb.getVariantPredWordId());
	persistStmt.setInt(4, condProb.getVariantAssumedFactWordId());
	persistStmt.setInt(5, condProb.getFreq());
	persistStmt.setDouble(6, condProb.getCondProb());
	persistStmt.setDouble(7, condProb.getLnCondProb());
    }

    public OntolCondWordVariantProb marshal(int origPredWordId,
	    int origAssumedFactWordId, int variantPredWordId,
	    int variantAssumedFactWordId, JdbcConnectionPool connPool)
	    throws SQLException {
	OntolCondWordVariantProb condProb = null;
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement marshalStmt = conn
		    .prepareStatement(marshalSql)) {
		marshalStmt.setInt(1, origPredWordId);
		marshalStmt.setInt(2, origAssumedFactWordId);
		marshalStmt.setInt(3, variantPredWordId);
		marshalStmt.setInt(4, variantAssumedFactWordId);
		try (ResultSet rs = marshalStmt.executeQuery()) {
		    if (rs.next()) {
			condProb = instantiateFromResultSet(rs);
		    }
		}
	    }
	}
	return condProb;
    }

    public SortedSet<OntolCondWordVariantProb> marshalForVariants(
	    int predWordId, int assumedFactWordId,
	    JdbcConnectionPool connPool) throws SQLException {
	SortedSet<OntolCondWordVariantProb> condProbs = new TreeSet<OntolCondWordVariantProb>();
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement marshalStmt = conn
		    .prepareStatement(marshalForVariantsSql)) {
		marshalStmt.setInt(1, predWordId);
		marshalStmt.setInt(2, predWordId);
		marshalStmt.setInt(3, assumedFactWordId);
		marshalStmt.setInt(4, assumedFactWordId);
		try (ResultSet rs = marshalStmt.executeQuery()) {
		    while (rs.next()) {
			OntolCondWordVariantProb condProb = instantiateFromResultSet(
				rs);
			condProbs.add(condProb);
		    }
		}
	    }
	}
	return condProbs;
    }

    private OntolCondWordVariantProb instantiateFromResultSet(ResultSet rs)
	    throws SQLException {
	/*
	 * 1-origPredWordId, 2-origAssumedFactWordId, 3-variantPredWordId,
	 * 4-variantAssumedFactWordId, 5-freq, 6-condProb, 7-lnCondProb
	 */
	OntolCondWordVariantProb condProb = new OntolCondWordVariantProb();
	condProb.setOrigPredWordId(rs.getInt(1));
	condProb.setOrigAssumedFactWordId(rs.getInt(2));
	condProb.setVariantPredWordId(rs.getInt(3));
	condProb.setVariantAssumedFactWordId(rs.getInt(4));
	condProb.setFreq(rs.getInt(5));
	condProb.setCondProb(rs.getDouble(6));
	condProb.setLnCondProb(rs.getDouble(7));
	return condProb;
    }

}

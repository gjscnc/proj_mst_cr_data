package edu.mst.db.ontol.words.condprob;

import java.util.Comparator;

public class OntolCondWordProb implements Comparable<OntolCondWordProb> {

    private int assumedFactWordId = 0;
    private int predicateWordId = 0;
    private int freq = 0;
    private double condProb = 0.0d;
    private double lnCondProb = 0.0d;

    public OntolCondWordProb() {

    }

    public OntolCondWordProb(int assumedFactWordId, int predicateWordId,
	    int freq, double condProb, double lnCondProb) {
	this.assumedFactWordId = assumedFactWordId;
	this.predicateWordId = predicateWordId;
	this.freq = freq;
	this.condProb = condProb;
	this.lnCondProb = lnCondProb;
    }

    public static class Comparators {
	public static final Comparator<OntolCondWordProb> ByAssumedFactWord = (
		OntolCondWordProb condProb1,
		OntolCondWordProb condProb2) -> Integer.compare(
			condProb1.assumedFactWordId,
			condProb2.assumedFactWordId);
	public static final Comparator<OntolCondWordProb> ByPredWordId = (
		OntolCondWordProb condProb1,
		OntolCondWordProb condProb2) -> Integer.compare(
			condProb1.predicateWordId, condProb2.predicateWordId);
	/**
	 * This is the primary key
	 */
	public static final Comparator<OntolCondWordProb> ByAssumedFactWord_PredicateWord = (
		OntolCondWordProb condProb1,
		OntolCondWordProb condProb2) -> ByAssumedFactWord
			.thenComparing(ByPredWordId).compare(condProb1, condProb2);
	public static final Comparator<OntolCondWordProb> ByCondProbDesc = (
		OntolCondWordProb condProb1,
		OntolCondWordProb condProb2) -> Double
			.compare(condProb2.condProb, condProb1.condProb);
	public static final Comparator<OntolCondWordProb> ByLnCondProbDesc = (
		OntolCondWordProb condProb1,
		OntolCondWordProb condProb2) -> Double
			.compare(condProb2.lnCondProb, condProb1.lnCondProb);
    }

    @Override
    public boolean equals(Object obj) {
	if (obj instanceof OntolCondWordProb) {
	    OntolCondWordProb condProb = (OntolCondWordProb) obj;
	    return assumedFactWordId == condProb.assumedFactWordId
		    && predicateWordId == condProb.predicateWordId;
	}
	return false;
    }

    @Override
    public int compareTo(OntolCondWordProb condProb) {
	return Comparators.ByAssumedFactWord_PredicateWord.compare(this, condProb);
    }

    @Override
    public int hashCode() {
	return Integer.hashCode(assumedFactWordId)
		+ 7 * Integer.hashCode(predicateWordId);
    }

    public int getAssumedFactWordId() {
	return assumedFactWordId;
    }

    public void setAssumedFactWordId(int assumedFactWordId) {
	this.assumedFactWordId = assumedFactWordId;
    }

    public int getPredicateWordId() {
	return predicateWordId;
    }

    public void setPredicateWordId(int predicateWordId) {
	this.predicateWordId = predicateWordId;
    }

    public int getFreq() {
	return freq;
    }

    public void setFreq(int freq) {
	this.freq = freq;
    }

    public double getCondProb() {
	return condProb;
    }

    public void setCondProb(double condProb) {
	this.condProb = condProb;
    }

    public double getLnCondProb() {
	return lnCondProb;
    }

    public void setLnCondProb(double lnCondProb) {
	this.lnCondProb = lnCondProb;
    }

}

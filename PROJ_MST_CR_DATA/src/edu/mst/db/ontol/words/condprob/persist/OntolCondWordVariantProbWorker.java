/**
 * 
 */
package edu.mst.db.ontol.words.condprob.persist;

import edu.mst.concurrent.BaseWorker;
import edu.mst.db.ontol.words.condprob.OntolCondWordProb;
import edu.mst.db.ontol.words.condprob.OntolCondWordProbDao;
import edu.mst.db.ontol.words.condprob.OntolCondWordVariantProbDao;
import edu.mst.db.util.ConnDbName;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * Extract and persist ontology conditional word probabilities for lexicon word
 * variants.
 * 
 * @author gjs
 *
 */
public class OntolCondWordVariantProbWorker extends BaseWorker<Integer> {

    private boolean isPersist = false;
    private int persistBatchSize = 0;
    private boolean isDeleteExisting = false;

    private OntolCondWordProb[] condProbs = null;
    private int nbrCondProbs = 0;

    private int nbrCondProbsProcessed = 0;
    private int nbrNewCondProbs = 0;
    private int rptInterval = 10000;

    public OntolCondWordVariantProbWorker(boolean isPersist,
	    int persistBatchSize, boolean isDeleteExisting,
	    JdbcConnectionPool connPool) throws Exception {
	super(connPool);
	this.isPersist = isPersist;
	this.persistBatchSize = persistBatchSize;
	this.isDeleteExisting = isDeleteExisting;
    }

    @Override
    public void run() throws Exception {

	if (isDeleteExisting) {
	    System.out.println(
		    "Deleting existing conditional probabilies for word variants.");
	    OntolCondWordVariantProbDao.deleteExisting(connPool);
	}

	{
	    System.out.println("Marshaling conditional probabilities.");
	    OntolCondWordProbDao condProbDao = new OntolCondWordProbDao();
	    condProbs = condProbDao.marshalAll(connPool);
	    nbrCondProbs = condProbs.length;
	    System.out.printf("Marshaled %1$,d conditional probabilities. %n",
		    nbrCondProbs);
	}

	System.out.println(
		"Now persisting conditional word probabilities for word variants.");

	while (currentPos < nbrCondProbs) {

	    maxPos = getNextBatchMaxPos();
	    startPos = currentPos;

	    for (int i = startPos; i <= maxPos; i++) {
		OntolCondWordProb condProb = condProbs[i];
		OntolCondWordVariantProbCallable callable = new OntolCondWordVariantProbCallable(
			condProb, isPersist, persistBatchSize, connPool);
		svc.submit(callable);
	    }

	    for (int i = startPos; i <= maxPos; i++) {
		nbrNewCondProbs += svc.take().get();
		nbrCondProbsProcessed++;
		if (nbrCondProbsProcessed % rptInterval == 0) {
		    System.out.printf(
			    "Processed %1$,d conditional probabilities, "
				    + "generating %2$,d new conditional probabilities for word variants. %n",
			    nbrCondProbsProcessed, nbrNewCondProbs);
		}
	    }

	    currentPos = maxPos + 1;

	}
	System.out.printf("Processed total of %1$,d conditional probabilities, "
		+ "generating %2$,d new conditional probabilities for word variants. %n",
		nbrCondProbsProcessed, nbrNewCondProbs);
    }

    @Override
    protected int getNextBatchMaxPos() throws Exception {
	int maxPos = currentPos + threadCallablesBatchSize - 1;
	if (maxPos >= nbrCondProbs - 1) {
	    maxPos = nbrCondProbs - 1;
	}
	return maxPos;
    }

    public static void main(String[] args) {

	try {

	    JdbcConnectionPool connPool = new JdbcConnectionPool(
		    ConnDbName.CONCEPT_RECOGN);
	    boolean isPersist = true;
	    int persistBatchSize = 1000000;
	    boolean isDeleteExisting = true;

	    try (OntolCondWordVariantProbWorker worker = new OntolCondWordVariantProbWorker(
		    isPersist, persistBatchSize, isDeleteExisting, connPool)) {
		worker.run();
	    }

	} catch (Exception ex) {
	    ex.printStackTrace();
	}

    }

}

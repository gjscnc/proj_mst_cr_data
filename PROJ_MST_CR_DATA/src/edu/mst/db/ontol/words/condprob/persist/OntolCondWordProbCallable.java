package edu.mst.db.ontol.words.condprob.persist;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.concurrent.Callable;

import edu.mst.db.ontol.words.ConceptNameWord;
import edu.mst.db.ontol.words.ConceptNameWordDao;
import edu.mst.db.ontol.words.condprob.OntolCondWordProb;
import edu.mst.db.ontol.words.condprob.OntolCondWordProbDao;
import edu.mst.db.util.JdbcConnectionPool;

public class OntolCondWordProbCallable implements Callable<Integer> {

    private int predLexWordId = 0;
    private boolean isPersist = false;
    private JdbcConnectionPool connPool = null;

    private ConceptNameWordDao nameWordDao = new ConceptNameWordDao();
    private OntolCondWordProbDao condProbDao = new OntolCondWordProbDao();

    public OntolCondWordProbCallable(int predLexWordId, boolean isPersist,
	    JdbcConnectionPool connPool) {
	this.predLexWordId = predLexWordId;
	this.isPersist = isPersist;
	this.connPool = connPool;
    }

    @Override
    public Integer call() throws Exception {

	SortedSet<Long> conceptNameUids = nameWordDao
		.getNameIdsWithWord(predLexWordId, connPool);
	if (conceptNameUids.size() == 0) {
	    return 0;
	}

	Map<Integer, Integer> freqByLexWordId = new HashMap<Integer, Integer>();
	Iterator<Long> conceptNameUidIter = conceptNameUids.iterator();
	while (conceptNameUidIter.hasNext()) {
	    long conceptNameUid = conceptNameUidIter.next();
	    List<ConceptNameWord> nameWords = nameWordDao
		    .marshalWordsForName(conceptNameUid, connPool);
	    for (ConceptNameWord assumedFactWord : nameWords) {
		int assumedFactWordId = assumedFactWord.getLexWordId();
		if (assumedFactWordId == predLexWordId) {
		    continue;
		}
		if (freqByLexWordId.containsKey(assumedFactWordId)) {
		    Integer newFreq = freqByLexWordId.get(assumedFactWordId)
			    + 1;
		    freqByLexWordId.put(assumedFactWordId, newFreq);
		} else {
		    freqByLexWordId.put(assumedFactWordId, 1);
		}
	    }
	}

	if (freqByLexWordId.size() == 0) {
	    return 0;
	}

	// sum frequencies
	int totalFreq = freqByLexWordId.values().stream()
		.mapToInt(Integer::intValue).sum();

	// persist probabilities
	Set<OntolCondWordProb> condProbs = new HashSet<OntolCondWordProb>();
	Iterator<Integer> lexWordIdsIter = freqByLexWordId.keySet().iterator();
	while (lexWordIdsIter.hasNext()) {
	    Integer factLexWordId = lexWordIdsIter.next();
	    Integer freq = freqByLexWordId.get(factLexWordId);
	    double condProb = (double) freq / (double) totalFreq;
	    double lnCondProb = Math.log(condProb);
	    OntolCondWordProb condWordProb = new OntolCondWordProb();
	    condWordProb.setPredicateWordId(predLexWordId);
	    condWordProb.setAssumedFactWordId(factLexWordId);
	    condWordProb.setCondProb(condProb);
	    condWordProb.setLnCondProb(lnCondProb);
	    condWordProb.setFreq(freq);
	    condProbs.add(condWordProb);
	}

	if (isPersist) {
	    condProbDao.persistBatch(condProbs, connPool);
	}

	return condProbs.size();
    }

}

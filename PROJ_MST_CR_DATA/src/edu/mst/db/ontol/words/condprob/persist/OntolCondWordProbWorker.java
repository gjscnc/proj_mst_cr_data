/**
 * 
 */
package edu.mst.db.ontol.words.condprob.persist;

import edu.mst.concurrent.BaseWorker;
import edu.mst.db.ontol.words.ConceptNameWordDao;
import edu.mst.db.ontol.words.condprob.OntolCondWordProbDao;
import edu.mst.db.util.ConnDbName;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * @author gjs
 *
 */
public class OntolCondWordProbWorker extends BaseWorker<Integer> {

    private boolean isPersist = false;
    private boolean isDeleteExisting = false;
    
    private int[] lexWordIds = null;
    private int nbrLexWordIds = 0;

    private int nbrLexWordsProcessed = 0;
    private int nbrCondProbs = 0;
    private int rptInterval = 5000;

    public OntolCondWordProbWorker(boolean isPersist, boolean isDeleteExisting,
	    JdbcConnectionPool connPool) {
	super(connPool);
	this.isPersist = isPersist;
	this.isDeleteExisting = isDeleteExisting;
    }

    @Override
    public void run() throws Exception {

	if (isDeleteExisting) {
	    System.out.println("Deleting existing conditional probabilities");
	    OntolCondWordProbDao.deleteAll(connPool);
	}

	System.out.println("Marshaling lexicon word IDs");
	lexWordIds = ConceptNameWordDao.marshalAllWordIds(connPool);
	nbrLexWordIds = lexWordIds.length;
	System.out.printf("Marshaled %1$,d lexicon word IDs. %n",
		nbrLexWordIds);

	System.out.println(
		"Now generating ontology conditional word probabilities.");
	
	while (currentPos < nbrLexWordIds) {

	    maxPos = getNextBatchMaxPos();
	    startPos = currentPos;

	    for (int i = startPos; i <= maxPos; i++) {
		int predLexWordId = lexWordIds[i];
		OntolCondWordProbCallable callable = new OntolCondWordProbCallable(
			predLexWordId, isPersist, connPool);
		svc.submit(callable);
	    }

	    for (int i = startPos; i <= maxPos; i++) {
		nbrCondProbs += svc.take().get();
		nbrLexWordsProcessed++;
		if (nbrLexWordsProcessed % rptInterval == 0) {
		    System.out.printf("Processed %1$,d lexicon words, "
			    + "generating %2$,d ontology conditional word probabilities. %n",
			    nbrLexWordsProcessed, nbrCondProbs);
		}
	    }

	    currentPos = maxPos + 1;

	}

	System.out.printf("Processed total of %1$,d lexicon words, "
		+ "generating %2$,d ontology conditional word probabilities. %n",
		nbrLexWordsProcessed, nbrCondProbs);
    }

    @Override
    protected int getNextBatchMaxPos() throws Exception {
	int maxPos = currentPos + threadCallablesBatchSize - 1;
	if (maxPos >= nbrLexWordIds - 1)
	    maxPos = nbrLexWordIds - 1;
	return maxPos;
    }

    public static void main(String[] args) {

	try {

	    JdbcConnectionPool connPool = new JdbcConnectionPool(
		    ConnDbName.CONCEPT_RECOGN);

	    boolean isPersist = true;
	    boolean isDeleteExisting = true;

	    try (OntolCondWordProbWorker worker = new OntolCondWordProbWorker(
		    isPersist, isDeleteExisting, connPool)) {
		worker.run();
	    }

	} catch (Exception ex) {
	    ex.printStackTrace();
	}

    }

}

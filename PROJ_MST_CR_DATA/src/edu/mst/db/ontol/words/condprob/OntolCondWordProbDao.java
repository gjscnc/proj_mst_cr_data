package edu.mst.db.ontol.words.condprob;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.SortedSet;
import java.util.TreeSet;

import edu.mst.db.util.JdbcConnectionPool;

public class OntolCondWordProbDao {

    public static final String allFields = "assumedFactWordId, predicateWordId, freq, condProb, lnCondProb";
    public static final String tableName = "conceptrecogn.ontolcondwordprob";
    public static final String deleteAllSql = "truncate table " + tableName;
    public static final String persistSql = "INSERT INTO " + tableName + "("
	    + allFields + ") VALUES(?,?,?,?,?)";
    public static final String marshalSql = "SELECT " + allFields + " from "
	    + tableName
	    + " where assumedFactWordId = ? and predicateWordId = ?";
    public static final String marshalAllForPredSql = "SELECT " + allFields
	    + " from " + tableName + " where predicateWordId = ?";
    public static final String marshalAllForAssumedFactSql = "SELECT "
	    + allFields + " from " + tableName + " where assumedFactWordId = ?";
    public static final String marshalPredWordIdsSql = "select distinct predicateWordId from conceptrecogn.ontolcondwordprob "
	    + "order by predicateWordId";
    public static final String marshalAllSql = "SELECT " + allFields + " from "
	    + tableName;

    public static void deleteAll(JdbcConnectionPool connPool)
	    throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement deleteAllStmt = conn
		    .prepareStatement(deleteAllSql)) {
		deleteAllStmt.executeUpdate();
	    }
	}
    }

    public void persist(OntolCondWordProb condProb, JdbcConnectionPool connPool)
	    throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement persistStmt = conn
		    .prepareStatement(persistSql)) {
		setPersistValues(condProb, persistStmt);
		persistStmt.executeUpdate();
	    }
	}
    }

    public void persistBatch(Collection<OntolCondWordProb> condProbs,
	    JdbcConnectionPool connPool) throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement persistStmt = conn
		    .prepareStatement(persistSql)) {
		for(OntolCondWordProb condProb : condProbs) {
		    setPersistValues(condProb, persistStmt);
		    persistStmt.addBatch();
		}
		persistStmt.executeBatch();
	    }
	}
    }

    private void setPersistValues(OntolCondWordProb condProb,
	    PreparedStatement persistStmt) throws SQLException {
	persistStmt.setInt(1, condProb.getAssumedFactWordId());
	persistStmt.setInt(2, condProb.getPredicateWordId());
	persistStmt.setInt(3, condProb.getFreq());
	persistStmt.setDouble(4, condProb.getCondProb());
	persistStmt.setDouble(5, condProb.getLnCondProb());
    }

    public static int[] marshalPredicateWordIds(JdbcConnectionPool connPool)
	    throws SQLException {
	SortedSet<Integer> predWordIdSet = new TreeSet<Integer>();
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement marshalIdsStmt = conn
		    .prepareStatement(marshalPredWordIdsSql)) {
		try (ResultSet rs = marshalIdsStmt.executeQuery()) {
		    while (rs.next()) {
			predWordIdSet.add(rs.getInt(1));
		    }
		}
	    }
	}
	int nbrPredWords = predWordIdSet.size();
	int[] predWordIds = new int[nbrPredWords];
	int pos = 0;
	for (int predWordId : predWordIdSet) {
	    predWordIds[pos] = predWordId;
	    pos++;
	}
	return predWordIds;
    }

    public OntolCondWordProb[] marshalAll(JdbcConnectionPool connPool)
	    throws SQLException {
	SortedSet<OntolCondWordProb> condProbSet = new TreeSet<OntolCondWordProb>();
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement marshalStmt = conn
		    .prepareStatement(marshalAllSql)) {
		try (ResultSet rs = marshalStmt.executeQuery()) {
		    while (rs.next()) {
			OntolCondWordProb condProb = instantiateFromResultSet(
				rs);
			condProbSet.add(condProb);
		    }
		}
	    }
	}
	OntolCondWordProb[] condProbs = condProbSet
		.toArray(new OntolCondWordProb[condProbSet.size()]);
	return condProbs;
    }

    public OntolCondWordProb marshal(int assumedFactWordId, int predicateWordId,
	    JdbcConnectionPool connPool) throws SQLException {
	OntolCondWordProb condProb = null;
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement getStmt = conn
		    .prepareStatement(marshalSql)) {
		getStmt.setInt(1, assumedFactWordId);
		getStmt.setInt(2, predicateWordId);
		try (ResultSet rs = getStmt.executeQuery()) {
		    if (rs.next()) {
			condProb = instantiateFromResultSet(rs);
		    }
		}
	    }
	}
	return condProb;
    }

    public SortedSet<OntolCondWordProb> marshalAllForPredicate(
	    int predicateWordId, JdbcConnectionPool connPool)
	    throws SQLException {
	SortedSet<OntolCondWordProb> results = new TreeSet<OntolCondWordProb>();
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement getForPredStmt = conn
		    .prepareStatement(marshalAllForPredSql)) {
		getForPredStmt.setInt(1, predicateWordId);
		try (ResultSet rs = getForPredStmt.executeQuery()) {
		    while (rs.next()) {
			OntolCondWordProb condProb = instantiateFromResultSet(
				rs);
			results.add(condProb);
		    }
		}
	    }
	}
	return results;
    }

    public SortedSet<OntolCondWordProb> marshalAllForAssumedFact(
	    int assumedFactWordId, SortBy sortBy, JdbcConnectionPool connPool)
	    throws Exception {
	SortedSet<OntolCondWordProb> results = instantiateSortedSet(sortBy);
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement getStmt = conn
		    .prepareStatement(marshalAllForAssumedFactSql)) {
		getStmt.setInt(1, assumedFactWordId);
		try (ResultSet rs = getStmt.executeQuery()) {
		    while (rs.next()) {
			OntolCondWordProb condProb = instantiateFromResultSet(
				rs);
			results.add(condProb);
		    }
		}
	    }
	}
	return results;
    }

    private OntolCondWordProb instantiateFromResultSet(ResultSet rs)
	    throws SQLException {
	/*
	 * assumedFactWordId, predicateWordId, freq, condProb, lnCondProb
	 */
	OntolCondWordProb condProb = new OntolCondWordProb();
	condProb.setAssumedFactWordId(rs.getInt(1));
	condProb.setPredicateWordId(rs.getInt(2));
	condProb.setFreq(rs.getInt(3));
	condProb.setCondProb(rs.getDouble(4));
	condProb.setLnCondProb(rs.getDouble(5));
	return condProb;
    }

    private SortedSet<OntolCondWordProb> instantiateSortedSet(SortBy sortBy)
	    throws Exception {
	SortedSet<OntolCondWordProb> results = null;
	switch (sortBy) {
	case NONE:
	    results = new TreeSet<OntolCondWordProb>();
	    break;
	case COND_PROB_DESC:
	    results = new TreeSet<OntolCondWordProb>(
		    OntolCondWordProb.Comparators.ByCondProbDesc);
	    break;
	case LN_COND_PROB_DESC:
	    results = new TreeSet<OntolCondWordProb>(
		    OntolCondWordProb.Comparators.ByLnCondProbDesc);
	    break;
	default:
	    throw new Exception(String.format("Invalid sort criteria = %1$s",
		    sortBy.name()));
	}
	return results;
    }

    public enum SortBy {
	COND_PROB_DESC, LN_COND_PROB_DESC, NONE;
    }

}

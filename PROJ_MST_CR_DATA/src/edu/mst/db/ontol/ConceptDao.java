/**
 * 
 */
package edu.mst.db.ontol;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import edu.mst.db.util.JdbcConnectionPool;

/**
 * @author gjs
 *
 */
public class ConceptDao {

    public static final String marshalSql = "SELECT cui, aui, isRelType, isLeaf, isRoot, fullySpecifiedName "
	    + "FROM conceptrecogn.concepts where uid=?";
    public static final String getCuiSql = "SELECT cui FROM conceptrecogn.concepts where uid=?";
    public static final String insertConceptSql = "INSERT INTO conceptrecogn.concepts "
	    + "(uid, cui, aui, isRelType, isLeaf, isRoot, defaultName) "
	    + "VALUES (?, ?, ?, ?, ?, ?, ?)";

    
    public Concept marshal(long uid, JdbcConnectionPool connPool) throws SQLException {
	Concept concept = null;
	try(Connection conn = connPool.getConnection()){
	    try(PreparedStatement marshalStmt = conn.prepareStatement(marshalSql)){
		marshalStmt.setLong(1, uid);
		try(ResultSet rs = marshalStmt.executeQuery()){
		    if(rs.next()) {
			concept = new Concept();
			concept.setUid(uid);
			concept.setAui(rs.getString(1));
			concept.setIsRelType(rs.getBoolean(2));
			concept.setIsLeaf(rs.getBoolean(3));
			concept.setIsRoot(rs.getBoolean(4));
		    }
		}
	    }
	    
	}
	return concept;
    }
    
    public String getCui(long uid, JdbcConnectionPool connPool) throws SQLException {
	String cui = null;
	try(Connection conn = connPool.getConnection()){
	    try(PreparedStatement getCuiStmt = conn.prepareStatement(getCuiSql)){
		getCuiStmt.setLong(1, uid);
		try(ResultSet rs = getCuiStmt.executeQuery()){
		    if(rs.next()) {
			cui = rs.getString(1);
		    }
		}
	    }
	}
	return cui;
    }

}

package edu.mst.db.ontol.snomed;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.Callable;

import edu.mst.db.util.JdbcConnectionPool;
import edu.mst.db.ontol.ConceptDao;
import edu.mst.db.util.Constants;

public class LoadSnomedConceptsCallable
	implements Callable<LoadSnomedConceptsResult> {

    private List<ConceptForBatchLoad> conceptsToBatchLoad = null;
    private JdbcConnectionPool connPool = null;

    public LoadSnomedConceptsCallable(List<ConceptForBatchLoad> conceptsToBatchLoad,
	    JdbcConnectionPool connPool) {
	this.conceptsToBatchLoad = conceptsToBatchLoad;
	this.connPool = connPool;
    }

    @Override
    public LoadSnomedConceptsResult call() throws Exception {

	getUmlsFields();

	LoadSnomedConceptsResult result = new LoadSnomedConceptsResult();

	try (Connection conn = connPool.getConnection()) {

	    try (PreparedStatement insertConceptStmt = conn
		    .prepareStatement(ConceptDao.insertConceptSql)) {

		for (ConceptForBatchLoad concept : conceptsToBatchLoad) {
		    
		    insertConceptStmt.setLong(1, concept.conceptUid);
		    insertConceptStmt.setString(2, concept.cui);
		    insertConceptStmt.setString(3, concept.aui);
		    insertConceptStmt.setBoolean(4, concept.isRelType);
		    insertConceptStmt.setBoolean(5, concept.isLeaf);
		    insertConceptStmt.setBoolean(6, concept.isRoot);
		    insertConceptStmt.setString(7, concept.defaultName);

		    insertConceptStmt.addBatch();
		    result.nbrConceptsLoaded++;
		}
		
		insertConceptStmt.executeBatch();
	    }

	}

	return result;
    }

    private void getUmlsFields() throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    String getCuiAuiSql = "SELECT cui, aui FROM conceptrecogn.mrconso "
		    + "WHERE scui = ? AND sab = ? ORDER BY TTY";
	    try (PreparedStatement getCuiAuiStmt = conn
		    .prepareStatement(getCuiAuiSql)) {
		for (ConceptForBatchLoad concept : conceptsToBatchLoad) {
		    String scui = String.valueOf(concept.conceptUid);
		    getCuiAuiStmt.setString(1, scui);
		    getCuiAuiStmt.setString(2, Constants.umlsSnomedId);
		    try (ResultSet rs = getCuiAuiStmt.executeQuery()) {
			if (rs.next()) {
			    concept.cui = rs.getString(1);
			    concept.aui = rs.getString(2);
			}
		    }
		}
	    }
	}
    }

}

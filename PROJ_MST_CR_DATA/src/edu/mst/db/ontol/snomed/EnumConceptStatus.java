/**
 * 
 */
package edu.mst.db.ontol.snomed;

/**
 * Enumerated concept status values for SNOMED
 * 
 * @author George
 *
 */
public enum EnumConceptStatus {

    CURRENT(0, "The concept is in current use and is considered active"),
    RETIRED(1,"The Concept has been withdrawn without a specified reason. These concepts are considered inactive."),
    DUPLICATE(2,"The Concept has been withdrawn from current use because it duplicates another Concept.These concepts are considered inactive."),
    OUTDATED(3,"The Concept has been withdrawn from current use because it is no longer recognized as a valid clinical concept. These concepts are considered inactive."),
    AMBIGUOUS(4,"The Concept has been withdrawn from current use because it is inherently ambiguous. These concepts are considered inactive."),
    ERRONEOUS(5,"The Concept has been withdrawn from current use as it contains an error. "
    	+ "A corrected but otherwise similar Concept may have been added to replace it. These concepts are considered inactive."),
    LIMITED(6,"The Concept is of limited clinical value as it is based on a classification concept or an administrative definition. "
    	+ "Concepts with this status are not valid for current use and are considered inactive."),
    MOVED_ELSEWHERE(10,"The Concept has been moved to an extension, to a different extension, or to the International Release. "
    	+ "Use the | Moved To |Relationship to locate the namespace to which the concept has been moved. These concepts are considered inactive."),
    PENDING_MOVE(11,"The Concept will be moved to an extension, to a different extension, or to the International Release. "
    	+ "Use the | Moved To |Relationship to locate the namespace to which the concept will be moved when the "
    	+ "recipient organization confirms the move. These concepts are considered active.");
    

    private int statusCode;
    private String description;

    EnumConceptStatus(int statusCode, String description) {
	this.statusCode = statusCode;
	this.description = description;
    }

    public int statusCode() {
	return statusCode;
    }
    
    public String description(){
	return description;
    }

}

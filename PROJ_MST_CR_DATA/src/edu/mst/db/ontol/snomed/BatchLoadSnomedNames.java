/**
 * 
 */
package edu.mst.db.ontol.snomed;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

import edu.mst.db.util.ConnDbName;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * Loads source file for SNOMED names.
 * <p>
 * This version only for RF2 release format.
 * </p>
 * 
 * @author George
 *
 */
public class BatchLoadSnomedNames {

    // TODO add support for SNOMED RF1 format

    private JdbcConnectionPool connPool;
    private File descrFile = null;
    private List<DescrRF2> descriptions = null;
    private int msgInterval = 50000;
    private int batchUpdateInterval = 250000;

    public BatchLoadSnomedNames(JdbcConnectionPool connPool) {
	this.connPool = connPool;
    }

    /**
     * This version currently supports the RF2 distribution format only
     * 
     * @param namesFileDir
     * @param namesFileName
     * @param batchType
     * @param distType
     * @throws Exception
     */
    public void batchLoad(String namesFileDir, String namesFileName,
	    EnumBatchType batchType, SnomedDistType distType)
	    throws Exception {

	// remove existing first
	int nbrDescrDel = 0;
	try (Connection conn = connPool.getConnection()) {
	    String nbrDescrSql = "SELECT count(*) FROM conceptrecogn.snomed_descr_rf2 ";
	    String delDescrSql = "TRUNCATE conceptrecogn.snomed_descr_rf2";
	    try (PreparedStatement nbrDescrStmt = conn
		    .prepareStatement(nbrDescrSql);
		    PreparedStatement delDescrStmt = conn
			    .prepareStatement(delDescrSql)) {
		try (ResultSet rs = nbrDescrStmt.executeQuery()) {
		    if (rs.next()) {
			nbrDescrDel = rs.getInt(1);
		    }
		}
		delDescrStmt.executeUpdate();
	    }
	}
	System.out.printf("Deleted %1$,d existing records.%n", nbrDescrDel);

	String fileName = String
		.format("%1$s\\%2$s", namesFileDir, namesFileName);
	descrFile = new File(fileName);

	switch (distType) {
	case RF1:
	    throw new Exception(
		    "RF1 format not supported for SNOMED descriptions");
	case RF2:
	    descriptions = SnomedFileParser.readDescrFileRF2(descrFile);
	    break;
	}

	// batch inserts
	switch (batchType) {
	case SINGLE_BATCH:
	    insertOneBatch();
	    break;
	case MULTI_BATCH:
	    insertMultiBatch();
	    break;
	}
    }

    private void insertMultiBatch() throws Exception {

	int nbrDescr = 0;
	int nbrInBatch = 0;
	long testConceptUid = 59741002;
	boolean isTextConceptLoaded = false;
	try (Connection conn = connPool.getConnection()) {
	    String insertDescrSql = "INSERT INTO conceptrecogn.snomed_descr_rf2 "
		    + "(uid, effectiveTime, active, moduleId, conceptUid, "
		    + "languageCode, typeId, term, caseSignifId) "
		    + "VALUES (?,?,?,?,?,?,?,?,?)";
	    try (PreparedStatement insertDescrStmt = conn
		    .prepareStatement(insertDescrSql)) {

		for (DescrRF2 descr : descriptions) {

		    insertDescrStmt.setLong(1, descr.uid);
		    insertDescrStmt.setString(2, descr.effectiveDate);
		    insertDescrStmt.setBoolean(3, descr.active);
		    insertDescrStmt.setLong(4, descr.moduleId);
		    insertDescrStmt.setLong(5, descr.conceptUid);
		    insertDescrStmt.setString(6, descr.languageCode);
		    insertDescrStmt.setLong(7, descr.typeId);
		    insertDescrStmt.setString(8, descr.term);
		    insertDescrStmt.setLong(9, descr.caseSignificanceId);
		    insertDescrStmt.addBatch();
		    nbrInBatch++;

		    // qc check
		    if (descr.conceptUid == testConceptUid) {
			isTextConceptLoaded = true;
		    }

		    nbrDescr++;
		    if (nbrDescr % msgInterval == 0) {
			System.out.printf(
				"%1$,d SQL commands added to batch.%n",
				nbrDescr);
		    }
		    if (nbrDescr % batchUpdateInterval == 0) {
			System.out.printf("Executing batch insert "
				+ "(batch size %1$,d)%n", batchUpdateInterval);
			insertDescrStmt.executeBatch();
			nbrInBatch = 0;
		    }
		}
		// insert any stragglers
		System.out.printf(
			"Executing batch insert for %1$,d stragglers.%n",
			nbrInBatch);
		insertDescrStmt.executeBatch();
	    }
	}
	System.out.printf("%n%nTotal of %1$,d SNOMED description records "
		+ "inserted into database.%n", nbrDescr);
	if (isTextConceptLoaded) {
	    System.out.printf(
		    "%n%n--->>> Test concept found (UID = %1$d).%n%n",
		    testConceptUid);
	} else {
	    System.out.printf(
		    "%n%n--->>> Test concept NOT loaded (UID = %1$d).%n%n",
		    testConceptUid);
	}

    }

    private void insertOneBatch() throws Exception {

	int nbrDescr = 0;
	long testConceptUid = 59741002;
	boolean isTextConceptLoaded = false;
	try (Connection conn = connPool.getConnection()) {
	    String insertDescrSql = "INSERT INTO conceptrecogn.snomed_descr_rf2 "
		    + "(uid, effectiveTime, active, moduleId, conceptUid, "
		    + "languageCode, typeId, term, caseSignifId) "
		    + "VALUES (?,?,?,?,?,?,?,?,?)";
	    try (PreparedStatement insertDescrStmt = conn
		    .prepareStatement(insertDescrSql)) {

		for (DescrRF2 descr : descriptions) {

		    insertDescrStmt.setLong(1, descr.uid);
		    insertDescrStmt.setString(2, descr.effectiveDate);
		    insertDescrStmt.setBoolean(3, descr.active);
		    insertDescrStmt.setLong(4, descr.moduleId);
		    insertDescrStmt.setLong(5, descr.conceptUid);
		    insertDescrStmt.setString(6, descr.languageCode);
		    insertDescrStmt.setLong(7, descr.typeId);
		    insertDescrStmt.setString(8, descr.term);
		    insertDescrStmt.setLong(9, descr.caseSignificanceId);
		    insertDescrStmt.addBatch();

		    nbrDescr++;
		    if (nbrDescr % msgInterval == 0) {
			System.out.printf(
				"%1$,d SQL commands added to batch.%n",
				nbrDescr);
		    }

		    // qc check
		    if (descr.conceptUid == testConceptUid) {
			isTextConceptLoaded = true;
		    }

		}
		// insert any stragglers
		System.out.printf(
			"Executing batch insert for %1$,d descriptions.%n",
			nbrDescr);
		insertDescrStmt.executeUpdate();
	    }
	}
	System.out.printf("%n%nTotal of %1$,d SNOMED description records "
		+ "inserted into database.%n", nbrDescr);
	if (isTextConceptLoaded) {
	    System.out.printf(
		    "%n%n--->>> Test concept found (UID = %1$d).%n%n",
		    testConceptUid);
	} else {
	    System.out.printf(
		    "%n%n--->>> Test concept NOT loaded (UID = %1$d).%n%n",
		    testConceptUid);
	}
	// check count actual versus estimated load count
	int nbrActualLoad = 0;
	try (Connection conn = connPool.getConnection()) {
	    String nbrActualSql = "SELECT count(*) FROM sps.snomed_descr_rf2 ";
	    try (PreparedStatement nbrActualStmt = conn
		    .prepareStatement(nbrActualSql)) {
		try (ResultSet rs = nbrActualStmt.executeQuery()) {
		    if (rs.next()) {
			nbrActualLoad = rs.getInt(1);
		    }
		}
	    }
	}
	if (nbrDescr != nbrActualLoad) {
	    System.out
		    .printf("%n%nError with estimated nbr loaded versus actual. %n"
			    + "%1$,d estimated loaded into database, "
			    + "but descriptions table contains %2$,d records.%n",
			    nbrDescr, nbrActualLoad);
	}
    }

    /**
     * @param args
     */
    public static void main(String[] args) {

	try {

	    JdbcConnectionPool connPool = new JdbcConnectionPool(ConnDbName.CONCEPT_RECOGN);
	    BatchLoadSnomedNames loader = new BatchLoadSnomedNames(connPool);

	    String namesFileDir = "G:\\UMLS\\SnomedCT\\SnomedCT_RF2Release_US1000124_20160901\\Full\\Terminology";
	    String namesFileName = "sct2_Description_Full-en_US1000124_20160901.txt";
	    EnumBatchType batchType = EnumBatchType.MULTI_BATCH;
	    SnomedDistType distType = SnomedDistType.RF2;

	    loader.batchLoad(namesFileDir, namesFileName, batchType, distType);

	} catch (Exception ex) {
	    ex.printStackTrace();
	}

    }

}

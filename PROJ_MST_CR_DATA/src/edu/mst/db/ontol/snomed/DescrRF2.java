/**
 * 
 */
package edu.mst.db.ontol.snomed;

import edu.mst.db.util.Constants;

/**
 * Simple class to hold description record read from SNOMED distribution file in
 * RF2 format.
 * 
 * @author George
 *
 */
public class DescrRF2 {
    
    public long uid = Constants.uninitializedLongVal;
    public String effectiveDate = Constants.nullText;
    public boolean active = false;
    public long moduleId = Constants.uninitializedLongVal;
    public long conceptUid = Constants.uninitializedLongVal;
    public String languageCode = Constants.nullText;
    public long typeId = Constants.uninitializedLongVal;
    public String term = Constants.nullText;
    public long caseSignificanceId = Constants.uninitializedLongVal;
    
    
}

/**
 * 
 */
package edu.mst.db.ontol.snomed;

import edu.mst.db.util.Constants;

/**
 * Simple class to hold the reference set retrieved from a SNOMED distribution
 * file in the RF2 format.
 * 
 * @author George
 *
 */
public class RefSetRF2 implements Comparable<RefSetRF2>{
    
    public String uuid = Constants.nullText;
    public String effectiveDate = Constants.nullText;
    public boolean active = false;
    public long moduleId = Constants.uninitializedLongVal;
    public long refsetId = Constants.uninitializedLongVal;
    public long refComponentId = Constants.uninitializedLongVal;
    public long acceptabilityId = Constants.uninitializedLongVal;

    @Override
    public int compareTo(RefSetRF2 refSet) {
	return uuid.compareTo(refSet.uuid);
    }
    
    @Override
    public boolean equals(Object obj){
	if(obj instanceof RefSetRF2){
	    RefSetRF2 refSet = (RefSetRF2)obj;
	    return uuid.equals(refSet.uuid);
	}
	return false;
    }
    
    @Override
    public int hashCode(){
	return uuid.hashCode();
    }

}

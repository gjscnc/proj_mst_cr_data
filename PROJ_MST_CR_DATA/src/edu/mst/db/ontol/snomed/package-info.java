/**
 * Components for loading SNOMED ontology data
 */
/**
 * @author gjs
 *
 */
package edu.mst.db.ontol.snomed;
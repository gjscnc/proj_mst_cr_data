/**
 * 
 */
package edu.mst.db.ontol.snomed;

/**
 * This enumerates the formats for the SNOMED distribution.
 * 
 * @author George
 *
 */
public enum SnomedDistType {
    
    RF1("Release Formate 1"),
    RF2("Release Format 2");
    
    private String name;
    
    SnomedDistType(String name){
	this.name = name;
    }
    
    public String getName(){
	return name;
    }
}

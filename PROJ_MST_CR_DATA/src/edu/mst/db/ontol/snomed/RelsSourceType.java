package edu.mst.db.ontol.snomed;

public enum RelsSourceType {
    
    STATED_RELS ("sps.snomed_statedrels_rf2"),
    ALL_RELS ("sps.snomed_rels_rf2");
    
    private String tableName;
    
    RelsSourceType(String tableName){
	this.tableName = tableName;
    }
    
    public String getTableName(){
	return tableName;
    }

}

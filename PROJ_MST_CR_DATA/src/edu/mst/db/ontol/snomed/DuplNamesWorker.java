package edu.mst.db.ontol.snomed;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import edu.mst.db.util.ConnDbName;
import edu.mst.db.util.DateTimeUtil;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * Performs concurrent processing to calculate and persist ontology cogency (OC)
 * for concepts or synonym names. This code currently persists OC for one
 * version of OC only, the one with stop words excluded.
 * <p>
 * Ontology cogency is persisted in database instead of inverse cogency. Inverse
 * ontology cogency (IOC) is the inverse of OC. IOC is calculated based upon the
 * combination of predicate and assumed word facts, so for simplicity and
 * testability the OC is persisted in the database instead of the IOC. This is
 * not required, but is an application architecture choice.
 * </p>
 * <p>
 * NOT for use with SPS windows application; must modify to add functionality
 * necessary to be called from the SPS Swing application.
 * 
 * @author George
 *
 */
public class DuplNamesWorker implements AutoCloseable{

    private static int nbrOfProcessors;
    private static ExecutorService pool;

    private JdbcConnectionPool connPool;
    private CompletionService<DuplNamesResult> svc;

    private int nbrConcepts = 0;
    private long[] uids;
    private int batchSize = 50;
    private int currentPos = 0;
    private int maxPos = 0;
    private int startPos = 0;
    private int msgInterval = 1500;

    private int nbrConceptsProcessed = 0;
    private int nbrNamesUpdated = 0;
    private int nbrDuplNames = 0;

    public DuplNamesWorker(JdbcConnectionPool connPool) {
	this.connPool = connPool;
	nbrOfProcessors = Runtime.getRuntime().availableProcessors();
	pool = Executors.newFixedThreadPool(nbrOfProcessors *= 3);
	svc = new ExecutorCompletionService<DuplNamesResult>(pool);
    }

    /**
     * Executes parallel processing to calculate and persist ontology cogency
     * without stop words.
     */
    public void run() throws Exception {

	System.out.printf("Executing class %1$s. %n",
		DuplNamesWorker.class.getName());

	getConceptUids();

	long timeBegin = System.currentTimeMillis();
	while (currentPos < nbrConcepts) {
	    maxPos = getNextBatchMaxPos();
	    startPos = currentPos;
	    for (int pos = startPos; pos <= maxPos; pos++) {
		long uid = uids[pos];
		DuplNamesCallable callable = new DuplNamesCallable(uid,
			connPool);
		svc.submit(callable);
	    }

	    for (int pos = startPos; pos <= maxPos; pos++) {
		Future<DuplNamesResult> f = svc.take();
		DuplNamesResult result = f.get();
		nbrConceptsProcessed++;
		nbrNamesUpdated += result.nameIdByText.size();
		nbrDuplNames += result.duplNameIdByText.size();
		if (nbrConceptsProcessed % msgInterval == 0) {
		    long timeDiff = System.currentTimeMillis() - timeBegin;
		    double avgMilliPerConcept = (double) timeDiff
			    / (double) nbrConceptsProcessed;
		    int nbrRemaining = nbrConcepts - nbrConceptsProcessed;
		    long milliRemaining = (long) nbrRemaining
			    * (long) avgMilliPerConcept;
		    System.out.printf("%1$,d concepts processed, "
			    + "avg of %2$.2f millisec. per concept, "
			    + "%3$,d remaining (%4$s to finish). "
			    + "%5$,d names updated, %6$,d duplicate names %n",
			    nbrConceptsProcessed, avgMilliPerConcept,
			    nbrRemaining,
			    DateTimeUtil.milliToHumanReadable(milliRemaining),
			    nbrNamesUpdated, nbrDuplNames);
		}
	    }
	    currentPos = maxPos + 1;
	}

	long timeDiff = System.currentTimeMillis() - timeBegin;
	double avgMilliPerConcept = (double) timeDiff
		/ (double) nbrConceptsProcessed;
	System.out.printf(
		"%1$,d concepts processed, "
			+ "avg of %2$.2f millisec. per name, "
			+ "total of %3$,d names updated. %n",
		nbrConceptsProcessed, avgMilliPerConcept, nbrNamesUpdated);

    }

    private int getNextBatchMaxPos() throws Exception {
	// subtract 1 from max pos since positions are inclusive
	int maxPos = currentPos + batchSize - 1;
	if (maxPos >= nbrConcepts - 1)
	    maxPos = nbrConcepts - 1;
	return maxPos;
    }

    private void getConceptUids() throws Exception {

	try (Connection conn = connPool.getConnection()) {
	    String countConceptsSql = "select count(*) from conceptrecogn.concepts where isExcluded = 0";
	    try (PreparedStatement countConceptsQry = conn
		    .prepareStatement(countConceptsSql)) {
		try (ResultSet rs = countConceptsQry.executeQuery()) {
		    if (rs.next())
			nbrConcepts = rs.getInt(1);
		}
	    }
	}
	if (nbrConcepts == 0)
	    throw new Exception("No concepts found in database.");
	uids = new long[nbrConcepts];
	try (Connection conn = connPool.getConnection()) {
	    String getConceptUidsSql = "select uid from conceptrecogn.concepts where isExcluded = 0";
	    try (PreparedStatement nameUidsQry = conn
		    .prepareStatement(getConceptUidsSql)) {
		try (ResultSet rs = nameUidsQry.executeQuery()) {
		    int pos = 0;
		    while (rs.next()) {
			uids[pos] = rs.getLong(1);
			pos++;
		    }
		}
	    }
	}
	String msg = String.format("Retrieved %1$,d concepts", nbrConcepts);
	System.out.println(msg);
    }

    protected void shutdownAndAwaitTermination(ExecutorService pool) {
	// pool.shutdown(); // Disable new tasks from being submitted
	pool.shutdownNow();
	try {
	    // Wait a while for existing tasks to terminate
	    if (!pool.awaitTermination(5, TimeUnit.SECONDS)) {
		pool.shutdownNow(); // Cancel currently executing tasks
		// Wait a while for tasks to respond to being cancelled
		if (!pool.awaitTermination(15, TimeUnit.SECONDS))
		    System.err.println("Pool did not terminate");
	    }
	} catch (InterruptedException ie) {
	    // (Re-)Cancel if current thread also interrupted
	    pool.shutdownNow();
	    // Preserve interrupt status
	    Thread.currentThread().interrupt();
	}
    }

    @Override
    public void close() throws Exception {
	this.shutdownAndAwaitTermination(pool);
	System.out.println("Parallel processor closed");
    }

    public static void main(String[] args) throws Exception {

	try {
	    
	    JdbcConnectionPool connPool = new JdbcConnectionPool(
		    ConnDbName.CONCEPT_RECOGN);

	    try(DuplNamesWorker worker = new DuplNamesWorker(connPool)){
		    worker.run();
	    }

	} catch (Exception ex) {
	    ex.printStackTrace();
	    throw ex;
	}

    }

}

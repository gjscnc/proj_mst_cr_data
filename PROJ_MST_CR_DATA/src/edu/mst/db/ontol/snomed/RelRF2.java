package edu.mst.db.ontol.snomed;

import edu.mst.db.util.Constants;

public class RelRF2 extends Rel {

    public String effectiveDate = null;
    public boolean active = false;
    public long moduleId = Constants.uninitializedLongVal;
    public long modifierId = Constants.uninitializedLongVal;
}

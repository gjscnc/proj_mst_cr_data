package edu.mst.db.ontol.snomed;

public enum EnumBatchType {

    SINGLE_BATCH,
    MULTI_BATCH
    
}

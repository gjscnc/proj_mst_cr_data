package edu.mst.db.ontol.snomed;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import edu.mst.db.util.ConnDbName;
import edu.mst.db.util.JdbcConnectionPool;

public class LoadSnomedConceptsWorker implements AutoCloseable {

    private static int nbrOfProcessors;
    private static ExecutorService pool;

    private File conceptsFile = null;
    private boolean deleteExisting = false;
    private JdbcConnectionPool connPool;
    private CompletionService<LoadSnomedConceptsResult> svc;

    private int rptInterval = 50;
    private int maxNbrBatchSubmissions = rptInterval;
    private int nbrSubmissionsForRetrievingResults = 0;
    private int maxNbrRecords = 10000;
    private int batchSizeDivisor = 50;
    private int batchSize = maxNbrRecords / batchSizeDivisor;
    private int resultsCount = 0;
    private int totalNbrBatchSubmissions = 0;

    private SnomedRootConcept rootConcepts = null;
    private ConceptForBatchLoad[] conceptsToLoad = null;
    private int nbrConceptsToLoad = 0;
    private int currentConceptArrayPos = 0;

    public LoadSnomedConceptsWorker(File conceptsFile, boolean deleteExisting,
	    JdbcConnectionPool connPool) {
	this.conceptsFile = conceptsFile;
	this.deleteExisting = deleteExisting;
	this.connPool = connPool;
	nbrOfProcessors = Runtime.getRuntime().availableProcessors();
	pool = Executors.newFixedThreadPool(nbrOfProcessors *= 3);
	svc = new ExecutorCompletionService<LoadSnomedConceptsResult>(pool);
    }

    public void run() throws Exception {

	if (deleteExisting) {
	    System.out.printf("Deleting existing SNOMED records %n%n");
	    try (Connection conn = connPool.getConnection()) {
		String deleteSql = "truncate table conceptrecogn.concepts";
		try (PreparedStatement deleteStmt = conn
			.prepareStatement(deleteSql)) {
		    deleteStmt.executeUpdate();
		}
	    }
	}

	rootConcepts = new SnomedRootConcept(connPool);

	svc = new ExecutorCompletionService<LoadSnomedConceptsResult>(pool);

	// instantiate concepts to load
	System.out.printf("Marshaling SNOMED concepts from file %1$s %n",
		conceptsFile.getAbsolutePath());
	conceptsToLoad = SnomedFileParser
		.readConceptsFileRF2(conceptsFile, rootConcepts)
		.toArray(new ConceptForBatchLoad[0]);
	nbrConceptsToLoad = conceptsToLoad.length;

	System.out.printf(
		"Marshaled %1$,d SNOMED concepts from file %2$s...%n%n",
		nbrConceptsToLoad, conceptsFile.getAbsolutePath());

	long lastSnapShot = System.currentTimeMillis();

	double deltaNbrLoadedAsDbl = Double
		.valueOf(String.valueOf(maxNbrRecords));

	List<ConceptForBatchLoad> batchOfConceptsToLoad = new ArrayList<ConceptForBatchLoad>();
	boolean eof = false;
	while (!eof) {

	    eof = currentConceptArrayPos == nbrConceptsToLoad;
	    if (!eof) {
		batchOfConceptsToLoad
			.add(conceptsToLoad[currentConceptArrayPos]);
		currentConceptArrayPos++;
	    }

	    if (batchOfConceptsToLoad.size() == batchSize
		    || (eof && batchOfConceptsToLoad.size() > 0)) {

		LoadSnomedConceptsCallable callable = new LoadSnomedConceptsCallable(
			new ArrayList<ConceptForBatchLoad>(batchOfConceptsToLoad),
			connPool);
		svc.submit(callable);

		totalNbrBatchSubmissions++;
		nbrSubmissionsForRetrievingResults++;
		batchOfConceptsToLoad.clear();

		if (nbrSubmissionsForRetrievingResults == maxNbrBatchSubmissions
			|| (eof && batchOfConceptsToLoad.size() > 0)) {
		    for (int i = 0; i < nbrSubmissionsForRetrievingResults; i++) {
			Future<LoadSnomedConceptsResult> f = svc.take();
			LoadSnomedConceptsResult result = f.get();
			resultsCount += result.nbrConceptsLoaded;
		    }
		    nbrSubmissionsForRetrievingResults = 0;
		}

		if (totalNbrBatchSubmissions % rptInterval == 0) {
		    long now = System.currentTimeMillis();
		    double duration = (now - lastSnapShot) / 1000.0d;
		    double avg = duration / deltaNbrLoadedAsDbl;
		    System.out.printf(
			    "Cumulative %1$,d batch submissions, "
				    + "loaded %2$,d SNOMED concepts, "
				    + "avg load time for last %3$,d concepts"
				    + " = %4$9.6f sec/concept %n",
			    totalNbrBatchSubmissions, resultsCount,
			    maxNbrRecords, avg);
		    lastSnapShot = System.currentTimeMillis();
		}
	    }
	}

	System.out.printf("Loaded a grand total of %1$,d UMLS concepts %n%n",
		resultsCount);

    }

    protected void shutdownAndAwaitTermination(ExecutorService pool) {
	// pool.shutdown(); // Disable new tasks from being submitted
	pool.shutdownNow();
	try {
	    // Wait a while for existing tasks to terminate
	    if (!pool.awaitTermination(5, TimeUnit.SECONDS)) {
		pool.shutdownNow(); // Cancel currently executing tasks
		// Wait a while for tasks to respond to being cancelled
		if (!pool.awaitTermination(15, TimeUnit.SECONDS))
		    System.err.println("Pool did not terminate");
	    }
	} catch (InterruptedException ie) {
	    // (Re-)Cancel if current thread also interrupted
	    pool.shutdownNow();
	    // Preserve interrupt status
	    Thread.currentThread().interrupt();
	}
    }

    @Override
    public void close() throws Exception {
	this.shutdownAndAwaitTermination(pool);
	System.out.println("Parallel processor closed");
    }

    public static void main(String[] args) {

	try {

	    JdbcConnectionPool connPool = new JdbcConnectionPool(
		    ConnDbName.CONCEPT_RECOGN);
	    String fileDir = "/home/gjs/umls/SNOMEDCT/2016/RF2"
		    + "/SnomedCT_RF2Release_US1000124_20160901/Full/Terminology";
	    String fileName = "sct2_Concept_Full_US1000124_20160901.txt";
	    File conceptsFile = new File(fileDir + "/" + fileName);
	    boolean deleteExisting = true;

	    try (LoadSnomedConceptsWorker worker = new LoadSnomedConceptsWorker(
		    conceptsFile, deleteExisting, connPool)) {
		worker.run();
	    }

	} catch (Exception ex) {
	    ex.printStackTrace();
	}

    }

}

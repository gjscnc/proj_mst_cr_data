/**
 * 
 */
package edu.mst.db.ontol.snomed;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.SortedSet;

import edu.mst.db.util.ConnDbName;
import edu.mst.db.util.JdbcConnectionPool;
import edu.mst.db.util.Constants;

/**
 * Class to load all SNOMED concepts as provided in the concepts distribution
 * file, i.e., this is the raw data.
 * <p>
 * Loads SNOMED release dated September 1, 2016. Loads data into the
 * sps.snomedconcepts table. This is a batch load process to minimize time.
 * </p>
 * 
 * @author George
 *
 */
public class BatchLoadSnomedConcepts {

    public static final String defaultConceptsFileDir = "/home/gjs/umls/SNOMEDCT/2016/RF2"
	    + "/SnomedCT_RF2Release_US1000124_20160901/Full/Terminology";
    public static final String defaultConceptsFileName = "sct2_Concept_Full_US1000124_20160901.txt";

    private JdbcConnectionPool connPool;
    private File conceptsFile = null;
    private SortedSet<ConceptForBatchLoad> concepts = null;

    private int nbrRecords = 0;

    public BatchLoadSnomedConcepts(JdbcConnectionPool connPool) {
	this.connPool = connPool;
    }

    public void batchImport(String conceptsFileDir, String conceptsFileName,
	    SnomedDistType distType, SnomedRootConcept rootConcepts)
	    throws Exception {

	// remove existing records
	int nbrConceptsDel = 0;
	try (Connection conn = connPool.getConnection()) {
	    String nbrConceptsSql = "SELECT count(*) FROM conceptrecogn.concepts";
	    String removeExistingConceptSql = "TRUNCATE conceptrecogn.concepts";
	    try (PreparedStatement nbrConceptsStmt = conn
		    .prepareStatement(nbrConceptsSql);
		    PreparedStatement removeExistingConceptsStmt = conn
			    .prepareStatement(removeExistingConceptSql)) {
		try (ResultSet rs = nbrConceptsStmt.executeQuery()) {
		    if (rs.next()) {
			nbrConceptsDel = rs.getInt(1);
		    }
		}
		removeExistingConceptsStmt.executeUpdate();
	    }
	}

	System.out.printf("Deleted %1$,d existing concept records.%n",
		nbrConceptsDel);

	String fileName = conceptsFileDir + "/" + conceptsFileName;
	conceptsFile = new File(fileName);

	switch (distType) {
	case RF1:
	    concepts = SnomedFileParser.readConceptsFileRF1(conceptsFile,
		    rootConcepts);
	    break;
	case RF2:
	    concepts = SnomedFileParser.readConceptsFileRF2(conceptsFile,
		    rootConcepts);
	    break;
	}
	if (concepts == null) {
	    String errMsg = String.format(
		    "SNOMED distribution type '%1$s' not recognized",
		    distType.toString());
	    throw new Exception(errMsg);
	}
	if (concepts.size() == 0) {
	    String errMsg = String.format("No concepts found in file %1$s",
		    fileName);
	    throw new Exception(errMsg);
	}

	nbrRecords = concepts.size();
	System.out.printf("Loaded %1$,d concepts from flat file.%n",
		nbrRecords);
	getCuiAuiValues();
	try (Connection conn = connPool.getConnection()) {

	    String insertConceptSql = "INSERT INTO conceptrecogn.Concepts "
		    + "(uid, cui, aui, isRelType, isLeaf, isRoot, defaultName) "
		    + "VALUES (?, ?, ?, ?, ?, ?, ?)";
	    try (PreparedStatement insertConceptStmt = conn
		    .prepareStatement(insertConceptSql)) {

		System.out.println("Compiling statements for batch insert");

		for (ConceptForBatchLoad concept : concepts) {
		    insertConceptStmt.setLong(1, concept.conceptUid);
		    insertConceptStmt.setString(2, concept.cui);
		    insertConceptStmt.setString(3, concept.aui);
		    insertConceptStmt.setBoolean(4, concept.isRelType);
		    insertConceptStmt.setBoolean(5, concept.isLeaf);
		    insertConceptStmt.setBoolean(6, concept.isRoot);
		    insertConceptStmt.setString(7, concept.defaultName);

		    insertConceptStmt.addBatch();

		}
		System.out.println("Executing batch insert");
		insertConceptStmt.executeBatch();
	    }

	}
	System.out.println("Batch inserts completed, all concepts loaded.");
    }

    private void getCuiAuiValues() throws Exception {

	System.out.println(
		"Retrieving CUI and AUI values for each SNOMED concept.");

	int nbrRetrieved = 0;

	try (Connection conn = connPool.getConnection()) {
	    String getCuiAuiSql = "SELECT cui, aui FROM conceptrecogn.mrconso "
		    + "WHERE scui = ? AND sab = ? ORDER BY TTY";
	    try (PreparedStatement getCuiAuiStmt = conn
		    .prepareStatement(getCuiAuiSql)) {
		for (ConceptForBatchLoad concept : concepts) {
		    String scui = String.valueOf(concept.conceptUid);
		    getCuiAuiStmt.setString(1, scui);
		    getCuiAuiStmt.setString(2, Constants.umlsSnomedId);
		    try (ResultSet rs = getCuiAuiStmt.executeQuery()) {
			if (rs.next()) {
			    concept.cui = rs.getString(1);
			    concept.aui = rs.getString(2);
			    nbrRetrieved++;
			    if (nbrRetrieved % 5000 == 0) {
				System.out.printf(
					"Retrieved CUI and AUI for "
						+ "total of %1$,d concepts. %n",
					nbrRetrieved);
			    }
			}
		    }
		}
	    }
	}

	System.out.println(
		"Finished retrieving CUI and AUI values for SNOMED concepts");
	System.out.printf("Retrieved CUI and AUI for "
		+ "grand total of %1$,d concepts. %n", nbrRetrieved);

    }

    /**
     * @param args
     */
    public static void main(String[] args) {

	try {

	    JdbcConnectionPool connPool = new JdbcConnectionPool(
		    ConnDbName.CONCEPT_RECOGN);
	    BatchLoadSnomedConcepts batchLoad = new BatchLoadSnomedConcepts(
		    connPool);
	    String conceptsFileDir = "/home/gjs/umls/SNOMEDCT/2016/RF2"
		    + "/SnomedCT_RF2Release_US1000124_20160901/Full/Terminology";
	    String conceptsFileName = "sct2_Concept_Full_US1000124_20160901.txt";
	    SnomedRootConcept rootConcepts = new SnomedRootConcept(connPool);
	    batchLoad.batchImport(conceptsFileDir, conceptsFileName,
		    SnomedDistType.RF2, rootConcepts);

	} catch (Exception ex) {
	    ex.printStackTrace();
	}

    }

}

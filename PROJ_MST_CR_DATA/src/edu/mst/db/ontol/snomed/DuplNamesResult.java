package edu.mst.db.ontol.snomed;

import java.util.HashMap;
import java.util.Map;

public class DuplNamesResult {

    long conceptUid = 0L;
    Map<String, Long> nameIdByText = new HashMap<String, Long>();
    Map<String, Long> duplNameIdByText = new HashMap<String, Long>();
    Map<String, Long> updateFailNameIdByText = new HashMap<String, Long>();

}

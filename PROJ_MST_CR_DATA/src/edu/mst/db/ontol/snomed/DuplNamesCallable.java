package edu.mst.db.ontol.snomed;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.Callable;

import edu.mst.db.util.JdbcConnectionPool;

public class DuplNamesCallable implements Callable<DuplNamesResult> {

    private long conceptUid;
    private JdbcConnectionPool connPool;

    public DuplNamesCallable(long conceptUid, JdbcConnectionPool connPool) {
	this.conceptUid = conceptUid;
	this.connPool = connPool;
    }

    @Override
    public DuplNamesResult call() throws Exception {

	DuplNamesResult result = new DuplNamesResult();
	result.conceptUid = conceptUid;

	String currentName = "";
	long currentNameId = 0L;

	try (Connection conn = connPool.getConnection()) {
	    
	    String getNamesSql = "select uid, nameText, nameType "
	    	+ "from conceptrecogn.conceptsourcename where isExcluded = 0 and conceptUid = ? order by nameText";
	    try(PreparedStatement getNamesStmt = conn.prepareStatement(getNamesSql)){
		getNamesStmt.setLong(1, conceptUid);
		try(ResultSet rs = getNamesStmt.executeQuery()){
		    if(!rs.next()) {
			return result;
		    }
		    while(rs.next()) {
			long uid = rs.getLong(1);
			String conceptNameLowerCase = rs.getString(2).toLowerCase();
			if(result.nameIdByText.containsKey(conceptNameLowerCase)) {
			    result.duplNameIdByText.put(conceptNameLowerCase, uid);
			} else {
			    result.nameIdByText.put(conceptNameLowerCase, uid);
			}
		    }
		}
	    }
	    
	    if(result.duplNameIdByText.size() != 0) {
		// duplicates found
		String updateNamesSql = "update conceptrecogn.conceptsourcename " + 
			"set isDupl = 1 where uid = ?";
		try(PreparedStatement updateNamesStmt = conn.prepareStatement(updateNamesSql)){
		    for(String name : result.duplNameIdByText.keySet()) {
			long nameUid = result.duplNameIdByText.get(name);
			updateNamesStmt.setLong(1, nameUid);
			try {
			    int row = updateNamesStmt.executeUpdate();
			    if(row == 0) {
				result.updateFailNameIdByText.put(name, nameUid);
			    }
			} catch (SQLException sqlEx) {
			    String msg = String.format("\n Error updating data record. \n"
			    	+ "Name id: %1$d \n Error description: %2$s \n", nameUid, sqlEx.getMessage());
			    throw new SQLException(msg, sqlEx);
			}
		    }
		}
	    }
	    
	} catch (Exception ex) {
	    String msg = String.format(
		    "\n Error setting is duplicate field for concept names.\n"
			    + "Concept name: %1$s (Concept UID %2$d, Name UID: %3$d) \n Error description: %4$s \n",
		    currentName, conceptUid, currentNameId, ex.getMessage());
	    System.out.println(msg);
	    throw ex;
	}

	return result;
    }

}

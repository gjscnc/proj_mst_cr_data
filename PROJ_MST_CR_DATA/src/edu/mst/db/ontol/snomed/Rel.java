/**
 * 
 */
package edu.mst.db.ontol.snomed;

import edu.mst.db.util.Constants;

/**
 * Wrapper for relationship record loaded from SNOMED. Do not use the UID for
 * determining uniqueness. Use the from concept and to concept. This required
 * for consistency with database design.
 * 
 * @author George
 *
 */
public class Rel implements Comparable<Rel> {

    public long uid = Constants.uninitializedLongVal;
    public long fromConceptUid = Constants.uninitializedLongVal;
    public long relTypeUid = Constants.uninitializedLongVal;
    public long toConceptUid = Constants.uninitializedLongVal;
    public long characteristicType = Constants.uninitializedLongVal;
    public int relationshipGroup = Constants.uninitializedIntVal;

    @Override
    public int compareTo(Rel rel) {
	return Long.compare(uid, rel.uid);
    }

    @Override
    public boolean equals(Object obj) {
	if (obj instanceof Rel) {
	    Rel rel = (Rel) obj;
	    return uid == rel.uid;
	}
	return false;
    }

    @Override
    public int hashCode() {
	return Long.hashCode(uid);
    }

}

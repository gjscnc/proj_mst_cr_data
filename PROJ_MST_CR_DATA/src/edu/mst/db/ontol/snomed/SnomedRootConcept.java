/*
=================================================================================
Copyright (c) 2010 RAI 
All rights reserved. 
=================================================================================
 */
package edu.mst.db.ontol.snomed;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import cern.colt.list.DoubleArrayList;
import cern.colt.map.OpenDoubleIntHashMap;
import cern.colt.map.OpenLongObjectHashMap;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * @author George
 * 
 */
public final class SnomedRootConcept {

    private JdbcConnectionPool connPool = null;

    /**
     * Map storing root concept UIDs, and name of root.
     * <ul>
     * <li>Key = root concept source id
     * <li>Value = root concept name (stored as Object, not String, need to cast
     * to String)
     * </ul>
     */
    private OpenLongObjectHashMap nameByRootUid = null;

    /**
     * Map storing root concept UIDs, and boolean for whether or not concepts
     * that are descendants of this root are searched.
     * <ul>
     * <li>Key = root concept source id
     * <li>Value = 'isSearchedByDefault'
     * </ul>
     * <p>
     * The concepts in this map are from the SNOMED documentation and not from a
     * SNOMED distribution file. These values, along with their names, are
     * hard-coded in this class.
     * </p>
     * CERN hash map used for performance optimization.
     */
    private OpenDoubleIntHashMap isSearchedByRootUid = null;
    
    public SnomedRootConcept(JdbcConnectionPool connPool) throws Exception {
	this.connPool = connPool;
	loadRoots();
    }

    private void loadRoots() throws Exception {

	isSearchedByRootUid = new OpenDoubleIntHashMap();
	nameByRootUid = new OpenLongObjectHashMap();

	isSearchedByRootUid.put(123037004L, 1); // Body structure
	isSearchedByRootUid.put(404684003L, 1); // Clinical finding
	isSearchedByRootUid.put(308916002L, 1); // Environment or geographical
						// location
	isSearchedByRootUid.put(272379006L, 1); // Event
	isSearchedByRootUid.put(363787002L, 1); // Observable entity
	isSearchedByRootUid.put(410607006L, 1); // Organism
	isSearchedByRootUid.put(373873005L, 1); // Pharmaceutical / biologic
						// product
	isSearchedByRootUid.put(78621006L, 1); // Physical force
	isSearchedByRootUid.put(260787004L, 1); // Physical object
	isSearchedByRootUid.put(71388002L, 1); // Procedure
	isSearchedByRootUid.put(362981000L, 0); // Qualifier value
	isSearchedByRootUid.put(419891008L, 0); // Record artifact
	isSearchedByRootUid.put(243796009L, 0); // Situation with explicit
						// context
	isSearchedByRootUid.put(900000000000441003L, 0); // SNOMED CT Model
							 // Component
	isSearchedByRootUid.put(48176007L, 0); // Social context
	isSearchedByRootUid.put(370115009L, 0); // Special concept
	isSearchedByRootUid.put(123038009L, 1); // Specimen
	isSearchedByRootUid.put(254291000L, 1); // Staging and scales
	isSearchedByRootUid.put(105590001L, 1); // Substance

	try (Connection conn = connPool.getConnection()) {
	    String getNameSql = "select defaultName from conceptrecogn.concepts "
		    + "where uid = ?";
	    try (PreparedStatement getNameStmt = conn
		    .prepareStatement(getNameSql)) {
		DoubleArrayList rootUids = isSearchedByRootUid.keys();
		for (int i = 0; i < rootUids.size(); i++) {
		    long rootUid = (long) rootUids.get(i);
		    getNameStmt.setLong(1, rootUid);
		    try (ResultSet rs = getNameStmt.executeQuery()) {
			if (rs.next()) {
			    String rootName = rs.getString(1);
			    nameByRootUid.put(rootUid, rootName);
			}
		    }
		}

	    }
	}

    }

    /**
     * Map storing root concept UIDs, and boolean for whether or not concepts
     * that are descendants of this root are searched.
     * <ul>
     * <li>Key = root concept source id
     * <li>Value = 'isSearchedByDefault'
     * </ul>
     * <p>
     * The concepts in this map are from the SNOMED documentation and not from a
     * SNOMED distribution file. These values, along with their names, are
     * hard-coded in this class.
     * </p>
     * 
     * @return Colt hash map - for lookup performance when performing relevance
     *         calculations.
     */
    public OpenDoubleIntHashMap getIsSearchedByRootUid() {
	return isSearchedByRootUid;
    }

    /**
     * Map storing root concept names, lookup by root concept UID.
     * <ul>
     * <li>Key = root concept source id
     * <li>Value = root concept name (stored as Object, not String, need to cast
     * to String)
     * </ul>
     * 
     * @return
     */
    public OpenLongObjectHashMap getNameByRootUid() {
	return nameByRootUid;
    }

    public boolean isSearched(long rootUid) {
	return isSearchedByRootUid.get(rootUid) == 1 ? true : false;
    }

    public String getName(long rootUid) {
	return (String) nameByRootUid.get(rootUid);
    }

}

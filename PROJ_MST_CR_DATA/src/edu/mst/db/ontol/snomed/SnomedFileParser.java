/**
 * 
 */
package edu.mst.db.ontol.snomed;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import edu.mst.db.text.util.NlpUtil;

/**
 * Contains static methods for reading different SNOMED distribution formats
 * 
 * @author George
 *
 */
public abstract class SnomedFileParser {

    /**
     * Reads SNOMED concepts file in the RF1 format.
     * 
     * @param conceptsFile
     * @return
     * @throws Exception
     */
    public static SortedSet<ConceptForBatchLoad> readConceptsFileRF1(
	    File conceptsFile, SnomedRootConcept rootConcepts)
	    throws Exception {

	SortedSet<ConceptForBatchLoad> concepts = new TreeSet<ConceptForBatchLoad>();

	try (BufferedReader bufferedReader = NlpUtil
		.getDefaultReader(conceptsFile)) {

	    boolean isFirstRecord = true;
	    String record = null;
	    while ((record = bufferedReader.readLine()) != null) {
		if (isFirstRecord) {
		    isFirstRecord = false;
		    continue;
		}
		ConceptForBatchLoadRF1 concept = new ConceptForBatchLoadRF1();
		String[] tokens = record.replace('|', '\t').split("\t");

		for (int i = 0; i < tokens.length; i++) {
		    String token = tokens[i];
		    switch (i) {
		    case 0:
			concept.conceptUid = Long.valueOf(token).longValue();
			break;
		    case 1:
			concept.status = Integer.valueOf(token).intValue();
			break;
		    case 2:
			concept.defaultName = token;
			break;
		    default:
			// concept.setIsPrimitive(Integer
			// .valueOf(token));
			break;
		    }
		}
		if (concept.status == EnumConceptStatus.CURRENT.statusCode()) {
		    concepts.add(concept);
		}
	    }

	}

	return concepts;
    }

    /**
     * Reads SNOMED concepts file in RF2 format.
     * <p>
     * Reads all ACTIVE concepts.
     * </p>
     * 
     * @param conceptsFile
     * @return
     * @throws Exception
     */
    public static SortedSet<ConceptForBatchLoad> readConceptsFileRF2(
	    File conceptsFile, SnomedRootConcept rootConcepts)
	    throws Exception {

	SortedSet<ConceptForBatchLoad> concepts = new TreeSet<ConceptForBatchLoad>();

	try (BufferedReader bufferedReader = NlpUtil
		.getDefaultReader(conceptsFile)) {

	    /*
	     * List of fields in RF2 file: id effectiveTime active moduleId
	     * definitionStatusId
	     */
	    boolean isFirstRecord = true;
	    String record = null;
	    while ((record = bufferedReader.readLine()) != null) {
		if (isFirstRecord) {
		    isFirstRecord = false;
		    continue;
		}
		ConceptForBatchLoadRF2 concept = new ConceptForBatchLoadRF2();
		String[] tokens = record.split("\t");

		for (int i = 0; i < tokens.length; i++) {

		    switch (i) {
		    case 0: // id
			concept.conceptUid = Long.valueOf(tokens[i]);
			break;
		    case 1: // effectiveTime
			break;
		    case 2: // active
			concept.active = Integer.valueOf(tokens[i]) == 1 ? true
				: false;
			break;
		    case 3: // moduleId
			break;
		    case 4: // definitionStatusId
			break;
		    }
		}

		boolean isRoot = rootConcepts.getIsSearchedByRootUid()
			.containsKey(concept.conceptUid);
		concept.isRoot = isRoot;
		if (concept.active) {
		    concepts.add(concept);
		}

	    }

	}

	return concepts;
    }

    /**
     * Reads SNOMED relationships file in RF1 format.
     * 
     * @param relsFile
     * @return
     * @throws Exception
     */
    public static List<Rel> readRelsFileRF1(File relsFile) throws Exception {

	List<Rel> rels = new ArrayList<Rel>();
	Map<String, RelRF1> relsByFromToConcepts = new HashMap<String, RelRF1>();

	int nbrDuplRels = 0;
	try (BufferedReader bufferedReader = NlpUtil
		.getDefaultReader(relsFile)) {
	    boolean isFirstRecord = true;
	    String record = "";
	    while ((record = bufferedReader.readLine()) != null) {
		if (isFirstRecord) {
		    isFirstRecord = false;
		    continue;
		}
		RelRF1 rel = new RelRF1();
		String[] tokens = record.split("\t");
		for (int i = 0; i < 7; i++) {
		    switch (i) {
		    case 0:
			rel.uid = Long.valueOf(tokens[i]);
			break;
		    case 1:
			rel.fromConceptUid = Long.valueOf(tokens[i]);
			break;
		    case 2:
			rel.relTypeUid = Long.valueOf(tokens[i]);
			break;
		    case 3:
			rel.toConceptUid = Long.valueOf(tokens[i]);
			break;
		    case 4:
			rel.characteristicType = Integer.valueOf(tokens[i]);
			break;
		    case 5:
			rel.refinability = Integer.valueOf(tokens[i]);
			break;
		    case 6:
			rel.relationshipGroup = Integer.valueOf(tokens[i]);
			break;
		    default:
			break;
		    }
		}
		// map key is from concept UID + '-' + to concept UID
		String relKey = String.valueOf(rel.fromConceptUid).concat("-")
			.concat(String.valueOf(rel.toConceptUid));
		if (relsByFromToConcepts.containsKey(relKey)) {
		    /*
		     * Check to see if the relationship type is the same. If so,
		     * this relationship is essentially a duplicate, insofar as
		     * the purposes of this research.
		     */
		    RelRF1 existing = relsByFromToConcepts.get(relKey);
		    if (rel.relTypeUid == existing.relTypeUid) {
			continue;
		    }
		    /*
		     * If relationship type is different, keep the smaller one.
		     * I'm guessing here, could go either way.
		     */
		    if (rel.relationshipGroup < existing.relationshipGroup) {
			relsByFromToConcepts.put(relKey, rel);
			nbrDuplRels++;
		    }
		    String newRelErr = String.format(
			    "Duplicate relationship being added. "
				    + "from concept = %1$d, to concept = %2$d, rel type = %3$d. "
				    + "characteristic type = %4$d, refinability = %5$d, relationship group = %6$d %n",
			    rel.fromConceptUid, rel.toConceptUid,
			    rel.relTypeUid, rel.characteristicType,
			    rel.refinability, rel.relationshipGroup);
		    String existingRelErr = String.format("Existing rel. "
			    + "from concept = %1$d, to concept = %2$d, rel type = %3$d. "
			    + "characteristic type = %4$d, refinability = %5$d, relationship group = %6$d %n",
			    existing.fromConceptUid, existing.toConceptUid,
			    existing.relTypeUid, existing.characteristicType,
			    existing.refinability, existing.relationshipGroup);
		    String errMsg = String.format("%1$s%2$s", newRelErr,
			    existingRelErr);
		    System.out.println(errMsg);
		    // throw new Exception(errMsg);
		} else {
		    relsByFromToConcepts.put(relKey, rel);
		}
	    }
	}

	System.out.printf(
		"Import file contained %1$,d duplicate relationships. %n",
		nbrDuplRels);

	rels.addAll(relsByFromToConcepts.values());
	Collections.sort(rels);

	return rels;

    }

    /**
     * Reads SNOMED relationships file in RF2 format.
     * <p>
     * This method imports all ACTIVE relationships, including duplicates. This
     * is necessary since it is more appropriate to implement logic to remove
     * duplicates in a different class.
     * </p>
     * 
     * @param relsDistrFile
     *                          relationships distribution file
     * @return
     * @throws Exception
     */
    public static List<Rel> readAllRelsFileRF2(File relsDistrFile)
	    throws Exception {

	List<Rel> rels = new ArrayList<Rel>();

	int msgInterval = 50000;
	int nbrLoaded = 0;
	try (BufferedReader bufferedReader = NlpUtil
		.getDefaultReader(relsDistrFile)) {
	    boolean isFirstRecord = true;
	    String record = "";
	    while ((record = bufferedReader.readLine()) != null) {
		if (isFirstRecord) {
		    isFirstRecord = false;
		    continue;
		}

		RelRF2 rel = new RelRF2();
		String[] tokens = record.split("\t");
		for (int i = 0; i < tokens.length; i++) {

		    /*
		     * Fields: id effectiveTime active moduleId sourceId
		     * destinationId relationshipGroup typeId
		     * characteristicTypeId modifierId
		     */

		    switch (i) {
		    case 0: // id
			rel.uid = Long.valueOf(tokens[i]).longValue();
			break;
		    case 1: // effectiveTime
			rel.effectiveDate = tokens[i];
			break;
		    case 2: // active
			rel.active = Integer.valueOf(tokens[i]) == 1 ? true
				: false;
			break;
		    case 3: // moduleId
			rel.moduleId = Long.valueOf(tokens[i]).longValue();
			break;
		    case 4: // sourceId
			rel.fromConceptUid = Long.valueOf(tokens[i])
				.longValue();
			break;
		    case 5: // destinationId
			rel.toConceptUid = Long.valueOf(tokens[i]).longValue();
			break;
		    case 6: // relationshipGroup
			rel.relationshipGroup = Integer.valueOf(tokens[i])
				.intValue();
			break;
		    case 7: // typeId
			rel.relTypeUid = Long.valueOf(tokens[i]).longValue();
			break;
		    case 8: // characteristicTypeId
			rel.characteristicType = Long.valueOf(tokens[i])
				.longValue();
			break;
		    case 9: // modifierId
			rel.modifierId = Long.valueOf(tokens[i]).longValue();
			break;
		    }

		}

		// retain active relationships only
		if (!rel.active) {
		    continue;
		} else {
		    rels.add(rel);
		    nbrLoaded++;
		    if (nbrLoaded % msgInterval == 0) {
			System.out.printf("Loaded %1$,d relationships.%n",
				nbrLoaded);
		    }
		}
	    }
	}

	System.out.printf("Loaded grand total of %1$,d relationships.%n",
		nbrLoaded);
	System.out.println("Sorting...");
	Collections.sort(rels);
	return rels;
    }

    /**
     * Reads SNOMED descriptions file in RF2 format.
     * <p>
     * This method imports ALL descriptions, including duplicates and inactive
     * descriptions.
     * </p>
     * 
     * @param descrDistrFile
     * @throws IOException
     * @throws FileNotFoundException
     * @throws UnsupportedEncodingException
     * @throws Exception
     */
    public static List<DescrRF2> readDescrFileRF2(File descrDistrFile)
	    throws UnsupportedEncodingException, FileNotFoundException,
	    IOException {

	List<DescrRF2> descriptions = new ArrayList<DescrRF2>();

	int msgInterval = 50000;
	int nbrLoaded = 0;
	try (BufferedReader bufferedReader = NlpUtil
		.getDefaultReader(descrDistrFile)) {

	    boolean isFirstRecord = true;
	    String record = "";
	    while ((record = bufferedReader.readLine()) != null) {
		if (isFirstRecord) {
		    isFirstRecord = false;
		    continue;
		}

		/*
		 * Fields: id effectiveTime active moduleId conceptId
		 * languageCode typeId term caseSignificanceId
		 */

		DescrRF2 descr = new DescrRF2();
		String[] tokens = record.split("\t");
		for (int i = 0; i < tokens.length; i++) {

		    switch (i) {
		    case 0: // id
			descr.uid = Long.valueOf(tokens[i]).longValue();
			break;
		    case 1: // effectiveTime
			descr.effectiveDate = tokens[i];
			break;
		    case 2: // active
			descr.active = Integer.valueOf(tokens[i])
				.intValue() == 1 ? true : false;
			break;
		    case 3: // moduleId
			descr.moduleId = Long.valueOf(tokens[i]).longValue();
			break;
		    case 4: // conceptId
			descr.conceptUid = Long.valueOf(tokens[i]).longValue();
			break;
		    case 5: // languageCode
			descr.languageCode = tokens[i];
			break;
		    case 6: // typeId
			descr.typeId = Long.valueOf(tokens[i]).longValue();
			break;
		    case 7: // term
			descr.term = tokens[i];
			break;
		    case 8: // caseSignificanceId
			descr.caseSignificanceId = Long.valueOf(tokens[i])
				.longValue();
			break;
		    }

		}
		descriptions.add(descr);
		nbrLoaded++;
		if (nbrLoaded % msgInterval == 0) {
		    System.out.printf("Loaded %1$,d name records from file.%n",
			    nbrLoaded);
		}
	    }

	}
	System.out.printf(
		"Loaded grand total of %1$,d name records loaded from file.%n",
		nbrLoaded);
	return descriptions;
    }

    public static List<RefSetRF2> readRefSetFileRF2(File refsetDistrFile)
	    throws Exception {

	List<RefSetRF2> refSets = new ArrayList<RefSetRF2>();

	int msgInterval = 50000;
	int nbrLoaded = 0;
	try (BufferedReader bufferedReader = NlpUtil
		.getDefaultReader(refsetDistrFile)) {

	    boolean isFirstRecord = true;
	    String record = "";
	    while ((record = bufferedReader.readLine()) != null) {
		if (isFirstRecord) {
		    isFirstRecord = false;
		    continue;
		}

		/*
		 * Fields: id effectiveTime active moduleId refsetId
		 * referencedComponentId acceptabilityId
		 */
		RefSetRF2 refset = new RefSetRF2();
		String[] tokens = record.split("\t");
		for (int i = 0; i < tokens.length; i++) {
		    switch (i) {
		    case 0: // id
			refset.uuid = tokens[i];
			break;
		    case 1: // effectiveTime
			refset.effectiveDate = tokens[i];
			break;
		    case 2: // active
			refset.active = Integer.valueOf(tokens[i])
				.intValue() == 1 ? true : false;
			break;
		    case 3: // moduleId
			refset.moduleId = Long.valueOf(tokens[i]).longValue();
			break;
		    case 4: // refsetId
			refset.refsetId = Long.valueOf(tokens[i]).longValue();
			break;
		    case 5: // referencedComponentId
			refset.refComponentId = Long.valueOf(tokens[i])
				.longValue();
			break;
		    case 6: // acceptabilityId
			refset.acceptabilityId = Long.valueOf(tokens[i])
				.longValue();
			break;
		    }
		}
		refSets.add(refset);
		nbrLoaded++;
		if (nbrLoaded % msgInterval == 0) {
		    System.out.printf(
			    "Loaded %1$,d refset records from file.%n",
			    nbrLoaded);
		}
	    }
	}

	System.out.printf(
		"Loaded grand total of %1$,d refset records from file.%n",
		nbrLoaded);
	return refSets;

    }

}

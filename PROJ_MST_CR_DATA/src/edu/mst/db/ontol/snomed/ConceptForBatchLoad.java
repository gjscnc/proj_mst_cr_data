/**
 * 
 */
package edu.mst.db.ontol.snomed;

import edu.mst.db.util.Constants;

/**
 * Primitive concept properties, extracted from concepts distribution file, to
 * persist in database.
 * 
 * @author George
 *
 */
public class ConceptForBatchLoad implements Comparable<ConceptForBatchLoad> {

    public long conceptUid = Constants.uninitializedLongVal;
    public boolean isRoot = false;
    public boolean isSearchedByDefault = false;
    public boolean isRelType = false;
    public boolean isLeaf = false;
    public String cui = null;
    public String aui = null;
    public String defaultName = Constants.nullText;

    @Override
    public int compareTo(ConceptForBatchLoad c) {
	return Long.compare(conceptUid, c.conceptUid);
    }

    @Override
    public boolean equals(Object obj) {
	if (obj instanceof ConceptForBatchLoad) {
	    ConceptForBatchLoad c = (ConceptForBatchLoad) obj;
	    return conceptUid == c.conceptUid;
	}
	return false;
    }

    @Override
    public int hashCode() {
	return Long.hashCode(conceptUid);
    }

}

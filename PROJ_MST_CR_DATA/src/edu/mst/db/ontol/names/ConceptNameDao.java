package edu.mst.db.ontol.names;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import edu.mst.db.ontol.words.ConceptNameWord;
import edu.mst.db.ontol.words.ConceptNameWordDao;
import edu.mst.db.util.JdbcConnectionPool;

public class ConceptNameDao {

    private static final String insertFields = "nameText, conceptUid, effectiveDate, "
	    + "isAcrAbbrev, isFullySpecified, isSynonym, acrAbbrevId";
    private static final String allFields = "uid, " + insertFields;
    public static final String persistSql = "INSERT INTO conceptrecogn.conceptnames "
	    + "(" + insertFields + ") VALUES(?,?,?,?,?,?,?)";
    public static final String getNameForUidSql = "SELECT " + allFields
	    + " FROM conceptrecogn.conceptnames WHERE uid = ?";
    public static final String getNameForConceptUidSql = "SELECT " + allFields
	    + " FROM conceptrecogn.conceptnames WHERE conceptUid = ?";
    public static final String getAllNamesSql = "select " + allFields
	    + " FROM conceptrecogn.conceptnames";
    public static final String getAllAcrAbbrevNamesSql = "select " + allFields
	    + " FROM conceptrecogn.conceptnames where isAcrAbbrev = true";
    public static final String getAllNameIdsSql = "select uid from conceptrecogn.conceptnames order by uid";
    public static final String setIsAcrAbbrevAndIdSql = "update conceptrecogn.conceptnames "
	    + "set isAcrAbbrev = ?, acrAbbrevId=? where uid = ?";
    public static final String resetIsAcrAbbrevSql = "update conceptrecogn.conceptnames set isAcrAbbrev = ?, acrAbbrevId = ?";
    public static final String setAcrAbbrevIdSql = "update conceptrecogn.conceptnames set acrAbbrevId = ? where uid = ?";

    /**
     * Fields uid, nameText, conceptUid, effectiveDate, isAcrAbbrev,
     * isFullySpecified, isSynonym
     */

    public boolean persist(ConceptName conceptName, JdbcConnectionPool connPool)
	    throws SQLException {
	boolean isPersisted = false;
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement persistStmt = conn
		    .prepareStatement(persistSql)) {
		persistStmt.setLong(1, conceptName.getUid()); // uid
		persistStmt.setString(2, conceptName.getNameText()); // name
								     // text
		persistStmt.setLong(3, conceptName.getConceptUid()); // concept
								     // uid
		persistStmt.setDate(4, conceptName.getEffectiveDate()); // effective
									// date
		persistStmt.setBoolean(5, conceptName.isAcrAbbrev()); // is
								      // acronym
								      // or
								      // abbreviation
		persistStmt.setBoolean(6, conceptName.isFullySpecified()); // is
									   // fully
									   // specified
									   // name
		persistStmt.setBoolean(7, conceptName.isSynonym()); // is
								    // synonym
		persistStmt.setInt(8, conceptName.getAcrAbbrevId()); // acr/abbrev
								     // ID
		isPersisted = persistStmt.executeUpdate() > 0 ? true : false;
	    }
	}
	return isPersisted;
    }

    public ConceptName marshal(long uid, JdbcConnectionPool connPool)
	    throws SQLException {

	ConceptName name = null;
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement getNameQry = conn
		    .prepareStatement(getNameForUidSql)) {
		getNameQry.setLong(1, uid);
		try (ResultSet rs = getNameQry.executeQuery()) {
		    if (rs.next()) {
			name = instantiateFromResultSet(rs);
		    }
		}
	    }
	}
	return name;
    }

    public SortedSet<ConceptName> marshalForConcept(long conceptUid,
	    JdbcConnectionPool connPool) throws SQLException {

	SortedSet<ConceptName> names = new TreeSet<ConceptName>();

	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement marshalStmt = conn
		    .prepareStatement(getNameForConceptUidSql)) {
		marshalStmt.setLong(1, conceptUid);
		try (ResultSet rs = marshalStmt.executeQuery()) {
		    while (rs.next()) {
			ConceptName name = instantiateFromResultSet(rs);
			names.add(name);
		    }
		}
	    }
	}

	return names;
    }

    private ConceptName instantiateFromResultSet(ResultSet rs)
	    throws SQLException {
	/*
	 * 1-uid, 2-nameText, 3-conceptUid, 4-effectiveDate, 5-isAcrAbbrev,
	 * 6-isFullySpecified, 7-isSynonym, 8-acrAbbrevId
	 */
	ConceptName name = new ConceptName();
	name.setUid(rs.getLong(1));
	name.setNameText(rs.getString(2));
	name.setConceptUid(rs.getLong(3));
	name.setEffectiveDate(rs.getDate(4));
	name.setIsAcrAbbrev(rs.getBoolean(5));
	name.setIsFullySpecified(rs.getBoolean(6));
	name.setIsSynonym(rs.getBoolean(7));
	name.setAcrAbbrevId(rs.getInt(8));
	return name;
    }

    /**
     * Sets both the is acronym/abbreviation flag, and zeros out the
     * acronym/abbreviation lexicon word ID.
     * 
     * @param sourceName
     * @param connPool
     * @return
     * @throws SQLException
     */
    public boolean setIsAcrAbbrev(ConceptName sourceName,
	    JdbcConnectionPool connPool) throws SQLException {
	boolean isUpdated = false;
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement updateStmt = conn
		    .prepareStatement(setIsAcrAbbrevAndIdSql)) {
		updateStmt.setBoolean(1, sourceName.isAcrAbbrev());
		updateStmt.setInt(2, sourceName.getAcrAbbrevId());
		updateStmt.setLong(3, sourceName.getUid());
		isUpdated = updateStmt.executeUpdate() > 0 ? true : false;
	    }
	}
	return isUpdated;
    }

    public void setAcrAbbrevId(int acrAbbrevId, long conceptNameUid,
	    JdbcConnectionPool connPool) throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement setAcrAbbrevIdStmt = conn
		    .prepareStatement(setAcrAbbrevIdSql)) {
		setAcrAbbrevIdStmt.setInt(1, acrAbbrevId);
		setAcrAbbrevIdStmt.setLong(2, conceptNameUid);
		setAcrAbbrevIdStmt.executeUpdate();
	    }
	}
    }

    public String getAcrAbbrevToken(long conceptNameUid,
	    JdbcConnectionPool connPool) throws SQLException {

	String acrAbbrevToken = null;
	ConceptName conceptName = marshal(conceptNameUid, connPool);
	if (conceptName == null) {
	    String errMsg = String.format("Name not found for UID %1$d",
		    conceptNameUid);
	    throw new SQLException(errMsg);
	}
	ConceptNameWordDao nameWordDao = new ConceptNameWordDao();
	ConceptNameWord firstWord = nameWordDao.marshalWord(conceptNameUid, 0,
		connPool);
	acrAbbrevToken = firstWord.getToken().toLowerCase();

	return acrAbbrevToken;
    }

    public String getAcrAbbrevExpandedForm(long conceptNameUid,
	    JdbcConnectionPool connPool) throws SQLException {

	String expandedForm = null;
	ConceptName conceptName = marshal(conceptNameUid, connPool);
	if (conceptName == null) {
	    String errMsg = String.format("Name not found for UID %1$d",
		    conceptNameUid);
	    throw new SQLException(errMsg);
	}
	ConceptNameWordDao nameWordDao = new ConceptNameWordDao();
	ConceptNameWord firstWord = nameWordDao.marshalWord(conceptNameUid, 0,
		connPool);
	String firstWordTokenLc = firstWord.getToken().toLowerCase();
	/*
	 * Parse expanded form. Name that is a acronym/abbreviation begins with
	 * the acronym/abbreviation token followed by ' - '
	 */
	int beginExpandedPos = firstWordTokenLc.length()
		+ ConceptName.dashStr.length();
	expandedForm = conceptName.getNameText().substring(beginExpandedPos);

	return expandedForm;
    }

    public static SortedSet<Long> getAllNameIds(JdbcConnectionPool connPool)
	    throws SQLException {
	SortedSet<Long> nameIds = new TreeSet<Long>();
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement getNameIdsStmt = conn
		    .prepareStatement(getAllNameIdsSql)) {
		try (ResultSet rs = getNameIdsStmt.executeQuery()) {
		    while (rs.next()) {
			nameIds.add(rs.getLong(1));
		    }
		}
	    }
	}
	return nameIds;
    }

    /**
     * Retrieves set of names with unique text.
     * <p>
     * Name text is duplicated in database, so uniquely named concept names are
     * retrieved
     * 
     * @param connPool
     * @return
     * @throws SQLException
     */
    public Map<String, Long> getAllUniqueNames(JdbcConnectionPool connPool)
	    throws SQLException {
	Map<String, Long> uniqueNames = new HashMap<String, Long>();
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement getAllNamesStmt = conn
		    .prepareStatement(getAllNamesSql)) {
		try (ResultSet rs = getAllNamesStmt.executeQuery()) {
		    while (rs.next()) {
			ConceptName name = instantiateFromResultSet(rs);
			uniqueNames.put(name.getNameText(), name.getUid());
		    }
		}
	    }
	}
	return uniqueNames;
    }

    /**
     * Retrieves set of names with unique text that are acronyms/abbreviations.
     * <p>
     * Name text is duplicated in database, so uniquely named concept names are
     * retrieved
     * 
     * @param connPool
     * @return
     * @throws SQLException
     */
    public Map<String, Long> getAllUniqueAcrAbbrevNames(
	    JdbcConnectionPool connPool) throws SQLException {
	Map<String, Long> uniqueNames = new HashMap<String, Long>();
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement getAllNamesStmt = conn
		    .prepareStatement(getAllAcrAbbrevNamesSql)) {
		try (ResultSet rs = getAllNamesStmt.executeQuery()) {
		    while (rs.next()) {
			ConceptName name = instantiateFromResultSet(rs);
			uniqueNames.put(name.getNameText(), name.getUid());
		    }
		}
	    }
	}
	return uniqueNames;
    }

    public static void resetAcrAbbrevFlag(JdbcConnectionPool connPool)
	    throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement resetAcrAbbrevStmt = conn
		    .prepareStatement(resetIsAcrAbbrevSql)) {
		resetAcrAbbrevStmt.setBoolean(1, false);
		resetAcrAbbrevStmt.setInt(2, 0);
		resetAcrAbbrevStmt.executeUpdate();
	    }
	}
    }

}

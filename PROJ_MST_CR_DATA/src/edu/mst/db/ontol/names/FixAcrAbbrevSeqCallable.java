/**
 * 
 */
package edu.mst.db.ontol.names;

import java.util.concurrent.Callable;

import edu.mst.db.lexicon.LexWord;
import edu.mst.db.lexicon.LexWordDao;
import edu.mst.db.ontol.words.ConceptNameWord;
import edu.mst.db.ontol.words.ConceptNameWordDao;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * @author gjs
 *
 */
public class FixAcrAbbrevSeqCallable implements Callable<Integer> {

    private long conceptNameUid = 0L;
    private JdbcConnectionPool connPool;

    private ConceptNameDao nameDao = new ConceptNameDao();
    private ConceptNameWordDao nameWordDao = new ConceptNameWordDao();
    private LexWordDao lexWordDao = new LexWordDao();

    public FixAcrAbbrevSeqCallable(long conceptNameUid,
	    JdbcConnectionPool connPool) {
	this.conceptNameUid = conceptNameUid;
	this.connPool = connPool;
    }

    @Override
    public Integer call() throws Exception {

	// make sure this is an acronym/abbreviation
	ConceptName name = nameDao.marshal(conceptNameUid, connPool);
	if (!name.isAcrAbbrev()) {
	    String errMsg = String.format(
		    "Concept name UID = %1$d (%2$s) is not an acronym/abbreviation",
		    conceptNameUid, name.getNameText());
	    throw new Exception(errMsg);
	}

	LexWord lexWord = lexWordDao.marshalById(name.getAcrAbbrevId(),
		connPool);
	ConceptNameWord firstNameWord = new ConceptNameWord();
	firstNameWord = new ConceptNameWord();
	firstNameWord.setBeginPos(0);
	firstNameWord.setEndPos(lexWord.getToken().length());
	firstNameWord.setConceptNameUid(name.getUid());
	firstNameWord.setConceptUid(name.getConceptUid());
	firstNameWord.setIsAcrAbbrev(true);
	firstNameWord.setIsPunct(false);
	firstNameWord.setIsStopWord(false);
	firstNameWord.setLexWordId(name.getAcrAbbrevId());
	firstNameWord.setToken(lexWord.getToken());

	nameWordDao.deleteWords(conceptNameUid, connPool);

	nameWordDao.persist(firstNameWord, connPool);

	return 1;
    }

}

/*
=================================================================================
Copyright (c) 2007 RAI 
All rights reserved. 
=================================================================================
 */
package edu.mst.db.ontol.names;

import java.util.HashSet;

import edu.mst.db.ontol.Allowable;

/**
 * @author George J. Shannon
 * 
 */
public final class AllowableName extends Allowable {

    public static final int CURRENT = 0;
    public static final int NON_CURRENT = 1;
    public static final int DUPLICATE = 2;
    public static final int OUTDATED = 3;
    public static final int ERRONEOUS = 5;
    public static final int LIMITED = 6;
    public static final int INNAPPROPRIATE = 7;
    public static final int CONCEPT_NONCURRENT = 8;
    public static final int MOVED_ELSEWHERE = 10;
    public static final int PENDING_MOVE = 11;
    
    public AllowableName() {
	allowableStatus = new HashSet<Integer>();
	allowableStatus.add(Integer.valueOf(0));
	allowableStatus.add(Integer.valueOf(6));
    }
    
}

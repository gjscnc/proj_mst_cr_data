/**
 * 
 */
package edu.mst.db.ontol.names;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.concurrent.Future;

import edu.mst.concurrent.BaseWorker;
import edu.mst.db.util.ConnDbName;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * Update acronym/abbreviation flag in the concept names table.
 * <p>
 * Sets the acronym/abbreviation flag in the name as follows:
 * <ul>
 * <li>If the first word in the name is an acronym/abbreviation, and the name
 * text contains the expansion of the acronym/abbreviation</li>
 * <li>If the contains only one word, the word is all caps, and this word is an
 * acronym/abbreviation</li>
 * </ul>
 * 
 * @author gjs
 *
 */
public class UpdateNameAcrAbbrevWorker extends BaseWorker<Integer> {

    private int nbrConceptUids = 0;
    private long[] conceptUids = null;

    private int rptInterval = 1000;

    private boolean isPersistData = false;
    private boolean isResetAcrAbbrevFlag = true;
    private int nbrConceptsAnalyzed = 0;
    private int nbrPersisted = 0;

    public UpdateNameAcrAbbrevWorker(boolean isPersistData,
	    boolean isResetAcrAbbrevFlag, JdbcConnectionPool connPool) {
	super(connPool);
	this.isPersistData = isPersistData;
	this.isResetAcrAbbrevFlag = isResetAcrAbbrevFlag;
    }

    @Override
    public void run() throws Exception {

	// reset acronym/abbreviation flag
	if (isResetAcrAbbrevFlag) {
	    System.out.println(
		    "Resetting acronym/abbreviation flag for all concept names");
	    ConceptNameDao.resetAcrAbbrevFlag(connPool);
	}

	// load concept UIDs
	System.out.println("Retrieving concept UIDs");
	try (Connection conn = connPool.getConnection()) {
	    String getCountConceptsSql = "select count(*) from conceptrecogn.concepts";
	    String getConceptUidsSql = "select uid from conceptrecogn.concepts";
	    try (PreparedStatement getCountStmt = conn
		    .prepareStatement(getCountConceptsSql);
		    PreparedStatement getUidsStmt = conn
			    .prepareStatement(getConceptUidsSql)) {
		try (ResultSet rs = getCountStmt.executeQuery()) {
		    if (rs.next()) {
			nbrConceptUids = rs.getInt(1);
		    }
		}
		conceptUids = new long[nbrConceptUids];
		try (ResultSet rs = getUidsStmt.executeQuery()) {
		    int pos = 0;
		    while (rs.next()) {
			conceptUids[pos] = rs.getLong(1);
			pos++;
		    }
		}
	    }
	}
	System.out.printf("Retrieved %1$,d concept UIDs %n", nbrConceptUids);

	while (currentPos < nbrConceptUids) {

	    maxPos = getNextBatchMaxPos();
	    startPos = currentPos;

	    for (int i = startPos; i <= maxPos; i++) {
		long conceptUid = conceptUids[i];
		UpdateNameAcrAbbrevCallable callable = new UpdateNameAcrAbbrevCallable(
			conceptUid, isPersistData, connPool);
		svc.submit(callable);
	    }

	    for (int i = startPos; i <= maxPos; i++) {
		Future<Integer> future = svc.take();
		nbrPersisted += future.get();
		nbrConceptsAnalyzed++;
		if (nbrConceptsAnalyzed % rptInterval == 0) {
		    System.out.printf(
			    "Analyzed %1$,d concepts, persisted changes to %2$,d names %n",
			    nbrConceptsAnalyzed, nbrPersisted);
		}
	    }

	    currentPos = maxPos + 1;

	}
	System.out.println("Finished");
	System.out.printf(
		"Analyzed %1$,d concepts, persisted changes to %2$,d named %n",
		nbrConceptsAnalyzed, nbrPersisted);

    }

    @Override
    protected int getNextBatchMaxPos() throws Exception {
	// subtract 1 from max pos since positions are inclusive
	int maxPos = currentPos + threadCallablesBatchSize - 1;
	if (maxPos >= nbrConceptUids - 1)
	    maxPos = nbrConceptUids - 1;
	return maxPos;
    }

    public static void main(String[] args) {

	try {

	    JdbcConnectionPool connPool = new JdbcConnectionPool(
		    ConnDbName.CONCEPT_RECOGN);
	    boolean isPersistData = true;
	    boolean isResetAcrAbbrevFlag = true;
	    try (UpdateNameAcrAbbrevWorker worker = new UpdateNameAcrAbbrevWorker(
		    isPersistData, isResetAcrAbbrevFlag, connPool)) {
		worker.run();
	    }

	} catch (Exception ex) {
	    ex.printStackTrace();
	}

    }

}

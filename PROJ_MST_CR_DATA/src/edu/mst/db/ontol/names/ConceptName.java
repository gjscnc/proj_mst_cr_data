/*
=================================================================================
Copyright (c) 2010 RAI 
All rights reserved. 
=================================================================================
 */
package edu.mst.db.ontol.names;

import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;

import edu.mst.db.ontol.snomed.DescrRF2;
import edu.mst.db.ontol.words.ConceptNameWord;
import edu.mst.db.util.Constants;

/**
 * @author George
 * 
 */
public class ConceptName implements Comparable<ConceptName> {

    public static final int TYPE_UNSPECIFIED = 0;
    public static final int TYPE_PREFERRED = 1;
    public static final int TYPE_SYNONYM = 2;
    public static final int TYPE_FULLY_QUALIFIED = 3;
    /**
     * String indicating the name is a potential acronym/acronym (' - ')
     */
    public static final String dashStr = " - ";

    public static final String NULL_NAME = "NULL_NAME";

    private long uid = 0L;
    private String nameText = "null";
    private long conceptUid = 0L;
    private Date effectiveDate = null;
    private boolean isAcrAbbrev = false;
    private int acrAbbrevId = 0;
    private boolean isFullySpecified = false;
    private boolean isSynonym = false;

    public ConceptName() {
    }

    public ConceptName(DescrRF2 nameRf2) throws ParseException {
	uid = nameRf2.uid;
	nameText = nameRf2.term;
	conceptUid = nameRf2.conceptUid;
	setEffectiveDate(nameRf2.effectiveDate);
	isFullySpecified = nameRf2.typeId == Constants.snomedTypeIdForFullySpecifiedName;
	isSynonym = nameRf2.typeId == Constants.snomedTypeIdForSynonymName;
	isAcrAbbrev = false;
    }

    @Override
    public boolean equals(Object obj) {
	if (obj instanceof ConceptName) {
	    ConceptName other = (ConceptName) obj;
	    return this.uid == other.uid;
	}
	return false;
    }

    @Override
    public int compareTo(ConceptName other) {
	return Comparators.ByUid.compare(this, other);
    }

    @Override
    public int hashCode() {
	return Long.hashCode(uid);
    }

    public static class Comparators {
	public static final Comparator<ConceptName> ByUid = (ConceptName name1,
		ConceptName name2) -> Long.compare(name1.getUid(),
			name2.getUid());
	public static final Comparator<ConceptName> ByConceptUid = (
		ConceptName name1, ConceptName name2) -> Long
			.compare(name1.getConceptUid(), name2.getConceptUid());
	public static final Comparator<ConceptName> ByNameText = (
		ConceptName name1,
		ConceptName name2) -> name1.nameText.compareTo(name2.nameText);
	public static final Comparator<ConceptName> ByEffectiveDateDesc = (
		ConceptName name1, ConceptName name2) -> name2.effectiveDate
			.compareTo(name1.effectiveDate);
	public static final Comparator<ConceptName> ByConceptUid_Name = (
		ConceptName name1, ConceptName name2) -> ByConceptUid
			.thenComparing(ByNameText).compare(name1, name2);
	public static final Comparator<ConceptName> ByName_EffDateDesc = (
		ConceptName name1, ConceptName name2) -> ByNameText
			.thenComparing(ByEffectiveDateDesc)
			.compare(name1, name2);
	public static final Comparator<ConceptName> ByConceptUid_Name_EffDateDesc = (
		ConceptName name1, ConceptName name2) -> ByConceptUid
			.thenComparing(ByNameText)
			.thenComparing(ByEffectiveDateDesc)
			.compare(name1, name2);
    }

    /**
     * Checks if this name is an acronym or abbreviation.
     * <p>
     * Test - name begins with an acronym/abbreviation followed by string
     * {@link #dashStr}.
     * 
     * @param firstWord
     */
    public void checkIsAcrAbbrev(ConceptNameWord firstWord) {
	String acrAbbrevStr = firstWord.getToken().concat(dashStr);
	isAcrAbbrev = nameText.startsWith(acrAbbrevStr) ? true : false;
    }

    public long getUid() {
	return uid;
    }

    public void setUid(long uid) {
	this.uid = uid;
    }

    public String getNameText() {
	return nameText;
    }

    public void setNameText(String nameText) {
	this.nameText = nameText;
    }

    public long getConceptUid() {
	return conceptUid;
    }

    public void setConceptUid(long conceptUid) {
	this.conceptUid = conceptUid;
    }

    public boolean isAcrAbbrev() {
	return isAcrAbbrev;
    }

    public void setIsAcrAbbrev(boolean isAcrAbbrev) {
	this.isAcrAbbrev = isAcrAbbrev;
    }

    public Date getEffectiveDate() {
	return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
	this.effectiveDate = effectiveDate;
    }

    public void setEffectiveDate(String effectiveDateStr)
	    throws ParseException {
	DateFormat formatter = new SimpleDateFormat("yyyyMMdd");
	java.util.Date myDate = formatter.parse(effectiveDateStr);
	java.sql.Date sqlDate = new java.sql.Date(myDate.getTime());
	this.effectiveDate = sqlDate;
    }

    public boolean isFullySpecified() {
	return isFullySpecified;
    }

    public void setIsFullySpecified(boolean isFullySpecified) {
	this.isFullySpecified = isFullySpecified;
    }

    public boolean isSynonym() {
	return isSynonym;
    }

    public void setIsSynonym(boolean isSynonym) {
	this.isSynonym = isSynonym;
    }

    public int getAcrAbbrevId() {
        return acrAbbrevId;
    }

    public void setAcrAbbrevId(int acrAbbrevId) {
        this.acrAbbrevId = acrAbbrevId;
    }

}
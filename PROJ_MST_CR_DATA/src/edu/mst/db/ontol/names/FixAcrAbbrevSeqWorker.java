/**
 * 
 */
package edu.mst.db.ontol.names;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.concurrent.Future;

import edu.mst.concurrent.BaseWorker;
import edu.mst.db.util.ConnDbName;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * @author gjs
 *
 */
public class FixAcrAbbrevSeqWorker extends BaseWorker<Integer> {

    private long[] conceptNameUids;
    private int nbrConceptNames = 0;

    private int nbrConceptNamesProcessed = 0;
    private int rptInterval = 1000;

    public FixAcrAbbrevSeqWorker(JdbcConnectionPool connPool) {
	super(connPool);
    }

    @Override
    public void run() throws Exception {

	System.out.println(
		"Marshaling concept name UIDs which are acronyms/abbreviations");
	try (Connection conn = connPool.getConnection()) {
	    String getCountSql = "select count(*) from conceptrecogn.conceptnames where isAcrAbbrev = TRUE";
	    String getUidsSql = "select uid from conceptrecogn.conceptnames where isAcrAbbrev = TRUE order by uid";
	    try (PreparedStatement countStmt = conn
		    .prepareStatement(getCountSql);
		    PreparedStatement getUidsStmt = conn
			    .prepareStatement(getUidsSql)) {
		try (ResultSet rs = countStmt.executeQuery()) {
		    if (rs.next()) {
			nbrConceptNames = rs.getInt(1);
		    }
		}
		conceptNameUids = new long[nbrConceptNames];
		int pos = 0;
		try (ResultSet rs = getUidsStmt.executeQuery()) {
		    while (rs.next()) {
			conceptNameUids[pos] = rs.getLong(1);
			pos++;
		    }
		}
	    }
	}
	System.out.printf(
		"Marshaled %1$,d concept names that are acronyms/abbreviations.%n",
		nbrConceptNames);

	System.out.println(
		"Now fixing word sequence for concept names that are acronyms/abbreviations");

	while (currentPos < nbrConceptNames) {

	    maxPos = getNextBatchMaxPos();
	    startPos = currentPos;

	    for (int i = startPos; i <= maxPos; i++) {
		long conceptNameUid = conceptNameUids[i];
		FixAcrAbbrevSeqCallable callable = new FixAcrAbbrevSeqCallable(
			conceptNameUid, connPool);
		svc.submit(callable);
	    }

	    for (int i = startPos; i <= maxPos; i++) {
		Future<Integer> future = svc.take();
		nbrConceptNamesProcessed += future.get();
		if (nbrConceptNamesProcessed % rptInterval == 0) {
		    System.out.printf("Processed %1$,d concept names %n",
			    nbrConceptNamesProcessed);
		}
	    }

	    currentPos = maxPos + 1;

	}
	System.out.printf("Processed %1$,d concept names %n",
		nbrConceptNamesProcessed);
    }

    @Override
    protected int getNextBatchMaxPos() throws Exception {
	int maxPos = currentPos + threadCallablesBatchSize - 1;
	if (maxPos >= nbrConceptNames - 1)
	    maxPos = nbrConceptNames - 1;
	return maxPos;
    }
    
    public static void main(String[] args) {
	
	try {
	    
	    JdbcConnectionPool connPool = new JdbcConnectionPool(ConnDbName.CONCEPT_RECOGN);
	    
	    try(FixAcrAbbrevSeqWorker worker = new FixAcrAbbrevSeqWorker(connPool)){
		worker.run();
	    }
	    
	}catch(Exception ex) {
	    ex.printStackTrace();
	}
    }

}

/**
 * 
 */
package edu.mst.db.ontol.names;

import java.util.Iterator;
import java.util.SortedSet;
import java.util.Vector;
import java.util.concurrent.Callable;

import edu.mst.db.lexicon.LexWord;
import edu.mst.db.lexicon.LexWordDao;
import edu.mst.db.ontol.words.ConceptNameWord;
import edu.mst.db.ontol.words.ConceptNameWordDao;
import edu.mst.db.text.util.LexConstants;
import edu.mst.db.util.JdbcConnectionPool;
import gov.nih.nlm.nls.lvg.Api.LvgApi;
import gov.nih.nlm.nls.lvg.Flows.ToExpansions;
import gov.nih.nlm.nls.lvg.Lib.LexItem;

/**
 * @author gjs
 *
 */
public class UpdateNameAcrAbbrevCallable implements Callable<Integer> {

    private long conceptUid;
    private boolean isPersistData;
    private JdbcConnectionPool connPool;
    private ConceptNameDao nameDao = new ConceptNameDao();
    private ConceptNameWordDao nameWordDao = new ConceptNameWordDao();
    private LexWordDao lexWordDao = new LexWordDao();

    private static final String lvgPropsFilePath = LexConstants.lvgPropertiesDirLinux
	    .concat(LexConstants.lvgPropertiesFileName);
    private LvgApi lvgApi = new LvgApi(lvgPropsFilePath);

    public UpdateNameAcrAbbrevCallable(long conceptUid, boolean isPersistData,
	    JdbcConnectionPool connPool) {
	this.isPersistData = isPersistData;
	this.conceptUid = conceptUid;
	this.connPool = connPool;
    }

    @Override
    public Integer call() throws Exception {

	Integer nbrUpdates = 0;

	SortedSet<ConceptName> names = nameDao.marshalForConcept(conceptUid,
		connPool);
	Iterator<ConceptName> namesIter = names.iterator();

	while (namesIter.hasNext()) {
	    ConceptName name = namesIter.next();
	    String nameText = name.getNameText();
	    if (nameText == null || nameText.isBlank() || nameText.isEmpty()) {
		continue;
	    }
	    int nbrNameWords = nameWordDao.getNbrWordsInName(name.getUid(), connPool);
	    ConceptNameWord firstNameWord = nameWordDao.marshalWord(name.getUid(), 0,
		    connPool);
	    if (firstNameWord == null) {
		continue;
	    }
	    String firstWordToken = firstNameWord.getToken();
	    if (firstWordToken == null || firstWordToken.isBlank() || firstWordToken.isEmpty()) {
		continue;
	    }
	    String firstWordTokenLc = firstWordToken.toLowerCase();
	    Vector<LexItem> outs = ToExpansions.Mutate(new LexItem(firstWordTokenLc),
		    lvgApi.GetConnection(), false, true);
	    if (outs == null || outs.size() == 0) {
		continue;
	    }
	    Iterator<LexItem> lexItemIter = outs.elements().asIterator();
	    // if is single word in sentence, then is acronym/abbreviation
	    if (nbrNameWords == 1) {
		continue;
	    } else {
		/*
		 * make sure beginning of name consists of acronym/abbreviation
		 * followed by ' - '
		 */
		String beginStrRqmt = firstWordTokenLc + " - ";
		String nameLc = nameText.toLowerCase();
		if (nameLc.startsWith(beginStrRqmt)) {
		    int firstPosForDescr = beginStrRqmt.length();
		    String nameDescrToMatchLc = nameLc
			    .substring(firstPosForDescr);
		    while (lexItemIter.hasNext()) {
			LexItem lexItem = lexItemIter.next();
			String lexTargetTerm = lexItem.GetTargetTerm();
			if (lexTargetTerm == null || lexTargetTerm.isBlank()
				|| lexTargetTerm.isEmpty()) {
			    continue;
			}
			String targetTermLc = lexTargetTerm.toLowerCase();
			boolean isAcrAbbrev = nameDescrToMatchLc
				.equals(targetTermLc);
			if (isAcrAbbrev) {
			    LexWord lexWord = lexWordDao.marshalByToken(firstWordTokenLc, connPool);
			    if (isPersistData) {
				name.setIsAcrAbbrev(true);
				name.setAcrAbbrevId(lexWord.getId());
				nameDao.setIsAcrAbbrev(name, connPool);
			    }
			    nbrUpdates++;
			}
		    }
		}
	    }
	}

	lvgApi.CleanUp();
	lvgApi.CloseConnection(lvgApi.GetConnection());

	return nbrUpdates;
    }

}

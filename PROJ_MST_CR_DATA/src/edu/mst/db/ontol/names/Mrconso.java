/**
 * 
 */
package edu.mst.db.ontol.names;

/**
 * Records from the NLM table 'MRCONSO'
 * <p>
 * This table contains the list of concepts in the UMLS ontology.
 * 
 * @author gjs
 *
 */
public class Mrconso {

    private String CUI = null;
    private String LAT = null;
    private String TS = null;
    private String LUI = null;
    private String STT = null;
    private String SUI = null;
    private String ISPREF = null;
    private String AUI = null;
    private String SAUI = null;
    private String SCUI = null;
    private String SDUI = null;
    private String SAB = null;
    private String TTY = null;
    private String CODE = null;
    private String STR = null;
    private int SRL = 0;
    private String SUPPRESS = null;
    private int CVF = 0;

    public String getCUI() {
	return CUI;
    }

    public void setCUI(String cUI) {
	CUI = cUI;
    }

    public String getLAT() {
	return LAT;
    }

    public void setLAT(String lAT) {
	LAT = lAT;
    }

    public String getTS() {
	return TS;
    }

    public void setTS(String tS) {
	TS = tS;
    }

    public String getLUI() {
	return LUI;
    }

    public void setLUI(String lUI) {
	LUI = lUI;
    }

    public String getSTT() {
	return STT;
    }

    public void setSTT(String sTT) {
	STT = sTT;
    }

    public String getSUI() {
	return SUI;
    }

    public void setSUI(String sUI) {
	SUI = sUI;
    }

    public String getISPREF() {
	return ISPREF;
    }

    public void setISPREF(String iSPREF) {
	ISPREF = iSPREF;
    }

    public String getAUI() {
	return AUI;
    }

    public void setAUI(String aUI) {
	AUI = aUI;
    }

    public String getSAUI() {
	return SAUI;
    }

    public void setSAUI(String sAUI) {
	SAUI = sAUI;
    }

    public String getSCUI() {
	return SCUI;
    }

    public void setSCUI(String sCUI) {
	SCUI = sCUI;
    }

    public String getSDUI() {
	return SDUI;
    }

    public void setSDUI(String sDUI) {
	SDUI = sDUI;
    }

    public String getSAB() {
	return SAB;
    }

    public void setSAB(String sAB) {
	SAB = sAB;
    }

    public String getTTY() {
	return TTY;
    }

    public void setTTY(String tTY) {
	TTY = tTY;
    }

    public String getCODE() {
	return CODE;
    }

    public void setCODE(String cODE) {
	CODE = cODE;
    }

    public String getSTR() {
	return STR;
    }

    public void setSTR(String sTR) {
	STR = sTR;
    }

    public int getSRL() {
	return SRL;
    }

    public void setSRL(int sRL) {
	SRL = sRL;
    }

    public String getSUPPRESS() {
	return SUPPRESS;
    }

    public void setSUPPRESS(String sUPPRESS) {
	SUPPRESS = sUPPRESS;
    }

    public int getCVF() {
	return CVF;
    }

    public void setCVF(int cVF) {
	CVF = cVF;
    }

}

/**
 * 
 */
package edu.mst.db.ontol.names;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import edu.mst.db.util.JdbcConnectionPool;

/**
 * @author gjs
 *
 */
public class MrconsoDao {

    public static final String snomedIdentifier = "SNOMEDCT_US";

    public static final String allFields = "CUI, LAT, TS, LUI, STT, SUI, ISPREF, AUI, SAUI, SCUI, SDUI, SAB, TTY, "
	    + "CODE, STR, SRL, SUPPRESS, CVF";

    public static final String getNamesForCuiSql = "select " + allFields
	    + " from conceptrecogn.mrconso where CUI=? and SAB=? order by AUI";
    
    /**
     * 
     * @param cui
     *                     Concept unique identifier
     * @param sab
     *                     Name of source for the name, e.g., 'SNOMEDCT_US'
     * @param connPool
     * @return
     * @throws SQLException
     */
    public List<Mrconso> marshal(String cui, String sab,
	    JdbcConnectionPool connPool) throws SQLException {
	List<Mrconso> nlmNames = new ArrayList<Mrconso>();
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement marshalForCuiStmt = conn
		    .prepareStatement(getNamesForCuiSql)) {
		marshalForCuiStmt.setString(1, cui);
		marshalForCuiStmt.setString(2, sab);
		try (ResultSet rs = marshalForCuiStmt.executeQuery()) {
		    while (rs.next()) {
			Mrconso mrconso = instantiateFromResultSet(rs);
			nlmNames.add(mrconso);
		    }
		}
	    }
	}
	return nlmNames;
    }

    private Mrconso instantiateFromResultSet(ResultSet rs) throws SQLException {

	/*
	 * 1-CUI, 2-LAT, 3-TS, 4-LUI, 5-STT, 6-SUI, 7-ISPREF, 8-AUI, 9-SAUI,
	 * 10-SCUI, 11-SDUI, 12-SAB, 13-TTY, 14-CODE, 15-STR, 16-SRL,
	 * 17-SUPPRESS, 18-CVF
	 */
	Mrconso mrconso = new Mrconso();
	mrconso.setCUI(rs.getString(1));
	mrconso.setLAT(rs.getString(2));
	mrconso.setTS(rs.getString(3));
	mrconso.setLUI(rs.getString(4));
	mrconso.setSTT(rs.getString(5));
	mrconso.setSUI(rs.getString(6));
	mrconso.setISPREF(rs.getString(7));
	mrconso.setAUI(rs.getString(8));
	mrconso.setSAUI(rs.getString(9));
	mrconso.setSCUI(rs.getString(10));
	mrconso.setSDUI(rs.getString(11));
	mrconso.setSAB(rs.getString(12));
	mrconso.setTTY(rs.getString(13));
	mrconso.setCODE(rs.getString(14));
	mrconso.setSTR(rs.getString(15));
	mrconso.setSRL(rs.getInt(16));
	mrconso.setSUPPRESS(rs.getString(17));
	mrconso.setCVF(rs.getInt(18));
	return mrconso;
    }
    
}

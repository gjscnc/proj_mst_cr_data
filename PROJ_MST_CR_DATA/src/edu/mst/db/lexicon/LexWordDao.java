package edu.mst.db.lexicon;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import edu.mst.db.lexicon.acrabbrev.AcrAbbrev;
import edu.mst.db.text.util.NlpUtil;
import edu.mst.db.util.JdbcConnectionPool;

public class LexWordDao {

    public static final String persistSql = "INSERT INTO conceptrecogn.lexwords "
	    + "(token, isPunct, isAcrAbbrev, isStopWord, countForAllNames) "
	    + "VALUES(?,?,?,?,?)";
    public static final String getByIdSql = "select token, isPunct, isAcrAbbrev, isStopWord, countForAllNames "
	    + "from conceptrecogn.lexwords where id = ?";
    public static final String getByTokenSql = "select id, isPunct, isAcrAbbrev, isStopWord, countForAllNames "
	    + "from conceptrecogn.lexwords where token = ?";
    public static final String getIdByTokenSql = "select id from conceptrecogn.lexwords where token = ?";
    public static final String getAllSql = "select id, token, isPunct, isAcrAbbrev, isStopWord, countForAllNames "
	    + "from conceptrecogn.lexwords";
    public static final String getAllIdsSql = "select id from conceptrecogn.lexwords order by id";
    public static final String getAllTokensSql = "select token from conceptrecogn.lexwords";
    public static final String getIdsInNamesSql = "select id from conceptrecogn.lexwords "
	    + "where countForAllNames > 0 and isPunct = false and isStopWord = false";
    public static final String updateIsAcrAbbrevSql = "update conceptrecogn.lexwords set isAcrAbbrev = ? where id = ?";

    public void persist(LexWord lexWord, JdbcConnectionPool connPool)
	    throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement persistStmt = conn.prepareStatement(
		    persistSql, Statement.RETURN_GENERATED_KEYS)) {
		persistStmt.setString(1, lexWord.getToken());
		persistStmt.setBoolean(2, lexWord.isPunct());
		persistStmt.setBoolean(3, lexWord.isAcrAbbrev());
		persistStmt.setBoolean(4, lexWord.isStopWord());
		persistStmt.setInt(5, lexWord.getCountForAllNames());
		try {
		    persistStmt.executeUpdate();
		} catch (SQLException ex) {
		    String errMsg = String.format("Error persisting token %1$s",
			    lexWord.getToken());
		    throw new SQLException(errMsg, ex);
		}
		try (ResultSet rs = persistStmt.getGeneratedKeys()) {
		    if (rs.next()) {
			lexWord.setId(rs.getInt(1));
		    }
		}
	    }
	}
    }

    public LexWord persist(String lexWordToken, StopWords stopWords,
	    NlpUtil nlpUtil, SortedMap<String, AcrAbbrev> acrAbbrevByToken,
	    JdbcConnectionPool connPool) throws SQLException {
	LexWord lexWord = new LexWord();
	String wordTokenLc = lexWordToken.toLowerCase();
	lexWord.setToken(wordTokenLc);
	lexWord.setIsStopWord(
		stopWords.getLexWordsByToken().containsKey(wordTokenLc));
	lexWord.setIsPunct(nlpUtil.isPunctOrDelimChar(wordTokenLc));
	lexWord.setIsAcrAbbrev(acrAbbrevByToken.containsKey(wordTokenLc));
	persist(lexWord, connPool);
	return lexWord;
    }

    /**
     * When persisting a batch the generated key for each new lexicon word is
     * <b>not</b> returned.
     * 
     * @param wordBatch
     * @param connPool
     * @throws SQLException
     */
    public void persistBatch(Collection<LexWord> wordBatch,
	    JdbcConnectionPool connPool) throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement persistStmt = conn
		    .prepareStatement(persistSql)) {
		for (LexWord lexWord : wordBatch) {
		    persistStmt.setString(1, lexWord.getToken());
		    persistStmt.setBoolean(2, lexWord.isPunct());
		    persistStmt.setBoolean(3, lexWord.isAcrAbbrev());
		    persistStmt.setBoolean(4, lexWord.isStopWord());
		    persistStmt.setInt(5, lexWord.getCountForAllNames());
		    persistStmt.addBatch();
		}
		persistStmt.executeBatch();
	    }
	}
    }

    public void updateIsAcrAbbrev(int id, boolean isAcrAbbrev,
	    JdbcConnectionPool connPool) throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement updateIsAcrAbbrevStmt = conn
		    .prepareStatement(updateIsAcrAbbrevSql)) {
		updateIsAcrAbbrevStmt.setBoolean(1, isAcrAbbrev);
		updateIsAcrAbbrevStmt.setInt(2, id);
		updateIsAcrAbbrevStmt.executeUpdate();
	    }
	}
    }

    public LexWord marshalById(int id, JdbcConnectionPool connPool)
	    throws SQLException {
	LexWord lexWord = null;
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement getByIdStmt = conn
		    .prepareStatement(getByIdSql)) {
		getByIdStmt.setInt(1, id);
		try (ResultSet rs = getByIdStmt.executeQuery()) {
		    if (rs.next()) {
			lexWord = new LexWord();
			lexWord.setId(id);
			lexWord.setToken(rs.getString(1));
			lexWord.setIsPunct(rs.getBoolean(2));
			lexWord.setIsAcrAbbrev(rs.getBoolean(3));
			lexWord.setIsStopWord(rs.getBoolean(4));
			lexWord.setCountForAllNames(rs.getInt(5));
		    }
		}
	    }
	}
	return lexWord;
    }

    public LexWord marshalByToken(String token, JdbcConnectionPool connPool)
	    throws SQLException {
	LexWord lexWord = null;
	String tokenLc = token.toLowerCase();
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement getByTokenStmt = conn
		    .prepareStatement(getByTokenSql)) {
		getByTokenStmt.setString(1, tokenLc);
		try (ResultSet rs = getByTokenStmt.executeQuery()) {
		    if (rs.next()) {
			lexWord = new LexWord();
			lexWord.setId(rs.getInt(1));
			lexWord.setToken(tokenLc);
			lexWord.setIsPunct(rs.getBoolean(2));
			lexWord.setIsAcrAbbrev(rs.getBoolean(3));
			lexWord.setIsStopWord(rs.getBoolean(4));
			lexWord.setCountForAllNames(rs.getInt(5));
		    }
		}
	    }
	}
	return lexWord;
    }

    public int getIdByToken(String token, JdbcConnectionPool connPool)
	    throws SQLException {
	int lexWordId = 0;
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement getIdByTokenStmt = conn
		    .prepareStatement(getIdByTokenSql)) {
		getIdByTokenStmt.setString(1, token);
		try (ResultSet rs = getIdByTokenStmt.executeQuery()) {
		    if (rs.next()) {
			lexWordId = rs.getInt(1);
		    }
		}
	    }
	}
	return lexWordId;
    }

    public boolean exists(String token, JdbcConnectionPool connPool)
	    throws SQLException {
	int lexWordId = getIdByToken(token, connPool);
	boolean isExists = lexWordId > 0 ? true : false;
	return isExists;
    }

    public static SortedMap<String, Integer> marshalAllIdsByToken(
	    JdbcConnectionPool connPool) throws SQLException {
	SortedMap<String, Integer> wordIdsByToken = new TreeMap<String, Integer>();
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement getAllStmt = conn
		    .prepareStatement(getAllSql)) {
		try (ResultSet rs = getAllStmt.executeQuery()) {
		    while (rs.next()) {
			Integer wordId = rs.getInt(1);
			String token = rs.getString(2);
			wordIdsByToken.put(token, wordId);
		    }
		}
	    }
	}
	return wordIdsByToken;
    }

    public static SortedMap<String, LexWord> marshalAllWordsByToken(
	    JdbcConnectionPool connPool) throws SQLException {
	SortedMap<String, LexWord> lexWordsByToken = new TreeMap<String, LexWord>();
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement getAllStmt = conn
		    .prepareStatement(getAllSql)) {
		try (ResultSet rs = getAllStmt.executeQuery()) {
		    while (rs.next()) {
			LexWord lexWord = new LexWord();
			lexWord.setId(rs.getInt(1));
			lexWord.setToken(rs.getString(2));
			lexWord.setIsPunct(rs.getBoolean(3));
			lexWord.setIsAcrAbbrev(rs.getBoolean(4));
			lexWord.setIsStopWord(rs.getBoolean(5));
			lexWord.setCountForAllNames(rs.getInt(6));
			lexWordsByToken.put(lexWord.getToken(), lexWord);
		    }
		}
	    }
	}
	return lexWordsByToken;
    }

    public static Set<String> marshalAllTokens(JdbcConnectionPool connPool)
	    throws SQLException {
	Set<String> tokens = new HashSet<String>();
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement getExistingTokensStmt = conn
		    .prepareStatement(getAllTokensSql)) {
		try (ResultSet rs = getExistingTokensStmt.executeQuery()) {
		    while (rs.next()) {
			tokens.add(rs.getString(1));
		    }
		}
	    }
	}
	return tokens;
    }

    public static SortedSet<Integer> getIdsInNames(JdbcConnectionPool connPool)
	    throws SQLException {
	SortedSet<Integer> wordIds = new TreeSet<Integer>();
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement getIdsInNamesStmt = conn
		    .prepareStatement(getIdsInNamesSql)) {
		try (ResultSet rs = getIdsInNamesStmt.executeQuery()) {
		    while (rs.next()) {
			wordIds.add(rs.getInt(1));
		    }
		}
	    }
	}
	return wordIds;
    }

    public static SortedSet<Integer> marshalAllIds(JdbcConnectionPool connPool)
	    throws SQLException {
	SortedSet<Integer> wordIds = new TreeSet<Integer>();
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement getAllIdsStmt = conn
		    .prepareStatement(getAllIdsSql)) {
		try (ResultSet rs = getAllIdsStmt.executeQuery()) {
		    while (rs.next()) {
			int lexWordId = rs.getInt(1);
			wordIds.add(lexWordId);
		    }
		}
	    }
	}
	return wordIds;
    }
    
    public static int[] marshalAllIdsAsArray(JdbcConnectionPool connPool)
	    throws SQLException {
	SortedSet<Integer> idSet = marshalAllIds(connPool);
	int[] ids = new int[idSet.size()];
	Iterator<Integer> idIter = idSet.iterator();
	int pos = 0;
	while(idIter.hasNext()) {
	    ids[pos] = idIter.next();
	    pos++;
	}
	return ids;
    }

}

package edu.mst.db.lexicon;

import java.util.Comparator;

public class LexWord implements Comparable<LexWord> {

    public static final int maxTokenLen = 100;
    
    private int id = 0;
    private String token = null;
    private boolean isPunct = false;
    private boolean isAcrAbbrev = false;
    private boolean isStopWord = false;
    private int countForAllNames = 0;

    public boolean equals(Object obj) {
	if (obj instanceof LexWord) {
	    LexWord lexWord = (LexWord) obj;
	    return id == lexWord.id;
	}
	return false;
    }

    @Override
    public int compareTo(LexWord lexWord) {
	return Comparators.ById.compare(this, lexWord);
    }

    @Override
    public int hashCode() {
	return Integer.hashCode(id);
    }

    public static class Comparators {
	public static Comparator<LexWord> ById = (LexWord lexWord1,
		LexWord lexWord2) -> Integer.compare(lexWord1.id, lexWord2.id);
	public static Comparator<LexWord> ByToken = (LexWord lexWord1,
		LexWord lexWord2) -> lexWord1.token.compareTo(lexWord2.token);
    }

    public int getId() {
	return id;
    }

    public void setId(int id) {
	this.id = id;
    }

    public String getToken() {
	return token;
    }

    public void setToken(String token) {
	this.token = token.toLowerCase();
    }

    public boolean isPunct() {
        return isPunct;
    }

    public void setIsPunct(boolean isPunct) {
        this.isPunct = isPunct;
    }

    public boolean isAcrAbbrev() {
        return isAcrAbbrev;
    }

    public void setIsAcrAbbrev(boolean isAcrAbbrev) {
        this.isAcrAbbrev = isAcrAbbrev;
    }

    public boolean isStopWord() {
        return isStopWord;
    }

    public void setIsStopWord(boolean isStopWord) {
        this.isStopWord = isStopWord;
    }

    public int getCountForAllNames() {
        return countForAllNames;
    }

    public void setCountForAllNames(int countForAllNames) {
        this.countForAllNames = countForAllNames;
    }

}

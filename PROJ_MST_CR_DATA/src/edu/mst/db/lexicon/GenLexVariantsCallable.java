/**
 * 
 */
package edu.mst.db.lexicon;

import java.util.SortedMap;
import java.util.concurrent.Callable;

import edu.mst.db.text.util.LexVariantsSynType;
import edu.mst.db.text.util.LexVariantsUtil;

/**
 * @author gjs
 *
 */
public class GenLexVariantsCallable implements Callable<GenLexVariantsResult> {

    private String existingLexToken = null;
    private LexVariantsSynType variantsSynType = null;
    private SortedMap<String, Integer> lexWordIdsByToken;
    private StopWords stopWords = null;
    private boolean isMutateVariants = true;

    public GenLexVariantsCallable(String lexToken,
	    LexVariantsSynType variantsSynType,
	    SortedMap<String, Integer> lexWordIdsByToken, StopWords stopWords) {
	this.existingLexToken = lexToken;
	this.variantsSynType = variantsSynType;
	this.lexWordIdsByToken = lexWordIdsByToken;
	this.stopWords = stopWords;
    }

    @Override
    public GenLexVariantsResult call() throws Exception {

	GenLexVariantsResult result = new GenLexVariantsResult();

	try (LexVariantsUtil variantUtil = new LexVariantsUtil(isMutateVariants,
		stopWords)) {

	    result.variants = variantUtil.getVariants(existingLexToken,
		    variantsSynType);

	    for (String variantToken : result.variants.variantTokens) {
		if (lexWordIdsByToken.containsKey(variantToken)) {
		    continue;
		}
		result.tokensMissingInLexicon.add(variantToken);
	    }
	    for (String acrAbbrevToken : result.variants.acrAbbrevByToken
		    .keySet()) {
		if (lexWordIdsByToken.containsKey(acrAbbrevToken)) {
		    continue;
		}
		result.tokensMissingInLexicon.add(acrAbbrevToken);
	    }
	    for (String otherToken : result.variants.lettersAndNbrs) {
		if (lexWordIdsByToken.containsKey(otherToken)) {
		    continue;
		}
		result.tokensMissingInLexicon.add(otherToken);
	    }
	}

	return result;
    }

}

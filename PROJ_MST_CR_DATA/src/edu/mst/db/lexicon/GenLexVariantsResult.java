/**
 * 
 */
package edu.mst.db.lexicon;

import java.util.HashSet;
import java.util.Set;

import edu.mst.db.text.util.LexVariantsUtil.Variants;

/**
 * @author gjs
 *
 */
public class GenLexVariantsResult {

    
    public Set<String> tokensMissingInLexicon = new HashSet<String>();
    public Variants variants = null;
    
}

/**
 * 
 */
/**
 * Components for lexicon objects shared across the ontology and corpora.
 * 
 * @author gjs
 *
 */
package edu.mst.db.lexicon;
package edu.mst.db.lexicon;

import java.util.Comparator;

public class LexWordVariant implements Comparable<LexWordVariant> {

    private int lexWordId = 0;
    private String lexWordToken = null;
    private int lexVariantId = 0;
    private String variantToken = null;

    @Override
    public int compareTo(LexWordVariant var) {
	return Comparators.ByLexWordId_VariantId.compare(this, var);
    }

    @Override
    public boolean equals(Object obj) {
	if (obj instanceof LexWordVariant) {
	    LexWordVariant var = (LexWordVariant) obj;
	    return lexWordId == var.lexWordId
		    && lexVariantId == var.lexVariantId;
	}
	return false;
    }

    @Override
    public int hashCode() {
	return Integer.hashCode(lexWordId) + 7 * Integer.hashCode(lexVariantId);
    }

    public static class Comparators {
	public static final Comparator<LexWordVariant> ByLexVariantId = (
		LexWordVariant var1, LexWordVariant var2) -> Integer
			.compare(var1.lexVariantId, var2.lexVariantId);
	public static final Comparator<LexWordVariant> ByLexWordId = (
		LexWordVariant var1, LexWordVariant var2) -> Integer
			.compare(var1.lexWordId, var2.lexWordId);
	public static final Comparator<LexWordVariant> ByToken = (
		LexWordVariant var1,
		LexWordVariant var2) -> var1.variantToken.compareTo(var2.variantToken);
	public static final Comparator<LexWordVariant> ByLexWordId_VariantId = (
		LexWordVariant var1, LexWordVariant var2) -> ByLexWordId
			.thenComparing(ByLexVariantId).compare(var1, var2);
    }

    public int getLexWordId() {
        return lexWordId;
    }

    public void setLexWordId(int lexWordId) {
        this.lexWordId = lexWordId;
    }

    public String getLexWordToken() {
        return lexWordToken;
    }

    public void setLexWordToken(String lexWordToken) {
        this.lexWordToken = lexWordToken;
    }

    public int getLexVariantId() {
        return lexVariantId;
    }

    public void setLexVariantId(int lexVariantId) {
        this.lexVariantId = lexVariantId;
    }

    public String getVariantToken() {
        return variantToken;
    }

    public void setVariantToken(String variantToken) {
        this.variantToken = variantToken;
    }

}

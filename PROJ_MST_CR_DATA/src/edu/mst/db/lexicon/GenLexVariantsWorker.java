/**
 * 
 */
package edu.mst.db.lexicon;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.Future;

import edu.mst.concurrent.BaseWorker;
import edu.mst.db.lexicon.acrabbrev.AcrAbbrev;
import edu.mst.db.lexicon.acrabbrev.AcrAbbrevDao;
import edu.mst.db.lexicon.acrabbrev.AcrAbbrevUtils;
import edu.mst.db.lexicon.acrabbrev.AcrAbbrevWord;
import edu.mst.db.lexicon.acrabbrev.AcrAbbrevWordDao;
import edu.mst.db.lexicon.acrabbrev.AcrAbbrevUtils.ExpansionWord;
import edu.mst.db.text.util.LexVariantsSynType;
import edu.mst.db.text.util.LexVariantsUtil.Variants;
import edu.mst.db.text.util.NlpUtil;
import edu.mst.db.util.ConnDbName;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * This component re-extracts variants from each word in the lexicon due to
 * errors in other that incorrectly extracts variants of lexicon words.
 * 
 * @author gjs
 *
 */
public class GenLexVariantsWorker extends BaseWorker<GenLexVariantsResult> {

    private LexVariantsSynType synType = null;
    private boolean isDeleteExisting = false;
    private boolean isTest = false;

    private SortedMap<String, Integer> lexWordIdsByToken = null;
    private SortedSet<String> newLexTokens = new TreeSet<String>();
    private SortedSet<Variants> variantsToPersist = new TreeSet<Variants>();
    private SortedMap<String, AcrAbbrev> acrAbbrevByToken = new TreeMap<String, AcrAbbrev>();
    private SortedSet<AcrAbbrev> newAcrAbbrevs = new TreeSet<AcrAbbrev>();
    private int nbrLexWordIds = 0;
    private String[] existingTokensArray = null;

    private StopWords stopWords = null;
    private NlpUtil nlpUtil = new NlpUtil();
    private LexWordDao lexWordDao = new LexWordDao();
    private LexWordVariantDao variantDao = new LexWordVariantDao();
    private AcrAbbrevDao acrAbbrevDao = new AcrAbbrevDao();
    private AcrAbbrevWordDao acrAbbrevWordDao = new AcrAbbrevWordDao();

    int nbrLexWordsProcessed = 0;
    int nbrNewLexWords = 0;
    int batchSize = 100;
    int nbrNewLexWordsSaved = 0;
    int nbrNewVariantsSaved = 0;
    int nbrNewAcrAbbrevSaved = 0;
    int rptInterval = 500;

    public GenLexVariantsWorker(LexVariantsSynType synType,
	    boolean isDeleteExisting, boolean isTest,
	    JdbcConnectionPool connPool) {
	super(connPool);
	this.synType = synType;
	this.isDeleteExisting = isDeleteExisting;
	this.isTest = isTest;
    }

    @Override
    public void run() throws Exception {

	if (isDeleteExisting) {
	    System.out.println("Deleting existing lexicon variants");
	    LexWordVariantDao.deleteExisting(connPool);
	}

	{
	    System.out.println("Extracting lexicon word IDs");
	    lexWordIdsByToken = LexWordDao.marshalAllIdsByToken(connPool);
	    existingTokensArray = lexWordIdsByToken.keySet()
		    .toArray(new String[lexWordIdsByToken.size()]);
	    Arrays.sort(existingTokensArray);
	    nbrLexWordIds = existingTokensArray.length;
	    System.out.printf("Marshaled %1$,d lexicon words IDs. %n",
		    nbrLexWordIds);
	    System.out.println("Marshaling existing acronyms/abbreviations");
	    for (AcrAbbrev acrAbbrev : acrAbbrevDao.marshalAll(connPool)) {
		acrAbbrevByToken.put(acrAbbrev.getToken(), acrAbbrev);
	    }
	}

	System.out.println(
		"Now extracting and persisting new lexicon word for variants.");

	stopWords = new StopWords(connPool);

	while (currentPos < nbrLexWordIds) {

	    maxPos = getNextBatchMaxPos();
	    startPos = currentPos;

	    for (int i = startPos; i <= maxPos; i++) {
		String lexWordToken = existingTokensArray[i];
		GenLexVariantsCallable callable = new GenLexVariantsCallable(
			lexWordToken, synType, lexWordIdsByToken, stopWords);
		svc.submit(callable);
	    }

	    for (int i = startPos; i <= maxPos; i++) {
		Future<GenLexVariantsResult> future = svc.take();
		GenLexVariantsResult result = future.get();
		variantsToPersist.add(result.variants);
		if (result.tokensMissingInLexicon.size() > 0) {
		    newLexTokens.addAll(result.tokensMissingInLexicon);
		    nbrNewLexWords += result.tokensMissingInLexicon.size();
		    // retain new acronyms/abbreviations
		    for (String acrAbbrevToken : result.variants.acrAbbrevByToken
			    .keySet()) {
			if (!acrAbbrevByToken.containsKey(acrAbbrevToken)) {
			    AcrAbbrev newAcrAbbrev = result.variants.acrAbbrevByToken
				    .get(acrAbbrevToken);
			    acrAbbrevByToken.put(acrAbbrevToken, newAcrAbbrev);
			    newAcrAbbrevs.add(newAcrAbbrev);
			}
		    }
		}
		nbrLexWordsProcessed++;
		if (nbrLexWordsProcessed % batchSize == 0) {
		    persistNewLexWordsAndVariants();
		    persistNewAcrAbbrev();
		}
		if (nbrLexWordsProcessed % rptInterval == 0) {
		    System.out.printf(
			    "Processed %1$,d lexicon words identifying %2$,d new lexicon tokens, "
				    + "persisted %3$,d new lexicon tokens, %4$,d new variants, "
				    + "and %5$,d new acronyms/abbreviations. %n",
			    nbrLexWordsProcessed, nbrNewLexWords,
			    nbrNewLexWordsSaved, nbrNewVariantsSaved,
			    nbrNewAcrAbbrevSaved);
		}
	    }

	    if (isTest && nbrNewLexWords > 25) {
		break;
	    }

	    currentPos = maxPos + 1;

	}
	// clean up any stragglers
	persistNewLexWordsAndVariants();
	persistNewAcrAbbrev();

	System.out.printf(
		"Processed total of %1$,d lexicon words identifying %2$,d new lexicon tokens, "
			+ "persisted %3$,d new lexicon tokens, %4$,d new variants, "
			+ "and %5$,d new acronyms/abbreviations. %n",
		nbrLexWordsProcessed, nbrNewLexWords, nbrNewLexWordsSaved,
		nbrNewVariantsSaved, nbrNewAcrAbbrevSaved);
    }

    private void persistNewLexWordsAndVariants() throws SQLException {

	// persist new lexicon words

	List<LexWord> persistedLexWords = new ArrayList<LexWord>();
	for (String newLexWordToken : newLexTokens) {
	    LexWord lexWord = new LexWord();
	    lexWord.setToken(newLexWordToken);
	    lexWord.setIsPunct(nlpUtil.isPunctOrDelimChar(newLexWordToken));
	    lexWord.setIsStopWord(
		    stopWords.getStopWordTokens().contains(newLexWordToken));
	    boolean isAcrAbbrev = acrAbbrevByToken.containsKey(newLexWordToken);
	    lexWord.setIsAcrAbbrev(isAcrAbbrev);
	    lexWordDao.persist(lexWord, connPool);
	    persistedLexWords.add(lexWord);
	}
	nbrNewLexWordsSaved += persistedLexWords.size();
	for (LexWord lexWord : persistedLexWords) {
	    if (lexWord.isAcrAbbrev()) {
		acrAbbrevByToken.get(lexWord.getToken())
			.setLexWordId(lexWord.getId());
	    }
	    lexWordIdsByToken.put(lexWord.getToken(), lexWord.getId());
	}

	// persist lexicon variants
	List<LexWordVariant> lexVariantsToPersist = new ArrayList<LexWordVariant>();
	for (Variants variants : variantsToPersist) {
	    String lexWordToken = variants.token;
	    int lexWordId = lexWordIdsByToken.get(lexWordToken);
	    for (String lexVariantToken : variants.variantTokens) {
		int lexVariantId = lexWordIdsByToken.get(lexVariantToken);
		if (lexWordId == lexVariantId) {
		    continue;
		}
		LexWordVariant lexVariant = new LexWordVariant();
		lexVariant.setLexWordId(lexWordId);
		lexVariant.setLexWordToken(lexWordToken);
		lexVariant.setLexVariantId(lexVariantId);
		lexVariant.setVariantToken(lexVariantToken);
		lexVariantsToPersist.add(lexVariant);
	    }
	}
	variantDao.persist(lexVariantsToPersist, connPool);
	nbrNewVariantsSaved += lexVariantsToPersist.size();

	/*
	 * clear variant results and new lexicon word batch collections
	 */
	newLexTokens.clear();
	variantsToPersist.clear();
    }

    private void persistNewAcrAbbrev() throws Exception {

	for (AcrAbbrev acrAbbrev : newAcrAbbrevs) {
	    acrAbbrevDao.persist(acrAbbrev, connPool);
	    SortedSet<AcrAbbrevWord> newAcrAbbrevWords = new TreeSet<AcrAbbrevWord>();
	    try (AcrAbbrevUtils acrAbbrevUtils = new AcrAbbrevUtils(connPool)) {
		SortedSet<ExpansionWord> expansionWords = acrAbbrevUtils
			.extractExpansionWords(acrAbbrev.getExpanded());
		Iterator<ExpansionWord> expansionWordsIter = expansionWords
			.iterator();
		while (expansionWordsIter.hasNext()) {
		    ExpansionWord expansionWord = expansionWordsIter.next();
		    int lexWordId = lexWordDao.getIdByToken(expansionWord.token,
			    connPool);
		    AcrAbbrevWord acrAbbrevWord = new AcrAbbrevWord(
			    acrAbbrev.getId(), expansionWord.wordNbr,
			    lexWordId);
		    newAcrAbbrevWords.add(acrAbbrevWord);
		}
	    }
	    acrAbbrevWordDao.persist(newAcrAbbrevWords, connPool);
	}

	nbrNewAcrAbbrevSaved += newAcrAbbrevs.size();
	newAcrAbbrevs.clear();
    }

    @Override
    protected int getNextBatchMaxPos() throws Exception {
	int maxPos = currentPos + batchSize - 1;
	if (maxPos >= nbrLexWordIds - 1)
	    maxPos = nbrLexWordIds - 1;
	return maxPos;
    }

    public static void main(String[] args) {

	try {

	    JdbcConnectionPool connPool = new JdbcConnectionPool(
		    ConnDbName.CONCEPT_RECOGN);
	    LexVariantsSynType synType = LexVariantsSynType.RECURSIVE_SYNONYMS;
	    boolean isDeleteExisting = true;
	    boolean isTest = false;
	    try (GenLexVariantsWorker worker = new GenLexVariantsWorker(synType,
		    isDeleteExisting, isTest, connPool)) {
		worker.run();
	    }

	} catch (Exception ex) {
	    ex.printStackTrace();
	}
    }
}

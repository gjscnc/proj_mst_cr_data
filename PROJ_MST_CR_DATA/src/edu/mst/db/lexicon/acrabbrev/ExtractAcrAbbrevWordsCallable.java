/**
 * 
 */
package edu.mst.db.lexicon.acrabbrev;

import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.Callable;

import edu.mst.db.lexicon.LexWordDao;
import edu.mst.db.lexicon.acrabbrev.AcrAbbrevUtils.ExpansionWord;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * @author gjs
 *
 */
public class ExtractAcrAbbrevWordsCallable implements Callable<Integer> {

    private int acrAbbrevDbId = 0;
    private JdbcConnectionPool connPool;

    private AcrAbbrevDao acrAbbrevDao = new AcrAbbrevDao();
    private AcrAbbrevWordDao acrAbbrevWordDao = new AcrAbbrevWordDao();
    private LexWordDao lexWordDao = new LexWordDao();

    public ExtractAcrAbbrevWordsCallable(int acrAbbrevDbId,
	    JdbcConnectionPool connPool) {
	this.acrAbbrevDbId = acrAbbrevDbId;
	this.connPool = connPool;
    }

    @Override
    public Integer call() throws Exception {

	Integer nbrNewAcrAbbrevWords = 0;

	AcrAbbrev acrAbbrev = acrAbbrevDao.marshalById(acrAbbrevDbId, connPool);
	SortedSet<AcrAbbrevWord> newAcrAbbrevWords = new TreeSet<AcrAbbrevWord>();
	try (AcrAbbrevUtils acrAbbrevUtils = new AcrAbbrevUtils(connPool)) {
	    SortedSet<ExpansionWord> expansionWords = acrAbbrevUtils
		    .extractExpansionWords(acrAbbrev.getExpanded());
	    Iterator<ExpansionWord> expansionWordsIter = expansionWords
		    .iterator();
	    while (expansionWordsIter.hasNext()) {
		ExpansionWord expansionWord = expansionWordsIter.next();
		int lexWordId = lexWordDao.getIdByToken(expansionWord.token,
			connPool);
		AcrAbbrevWord acrAbbrevWord = new AcrAbbrevWord(
			acrAbbrev.getId(), expansionWord.wordNbr, lexWordId);
		newAcrAbbrevWords.add(acrAbbrevWord);
	    }
	}
	acrAbbrevWordDao.persist(newAcrAbbrevWords, connPool);

	nbrNewAcrAbbrevWords = newAcrAbbrevWords.size();

	return nbrNewAcrAbbrevWords;
    }

}

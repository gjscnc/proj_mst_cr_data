/**
 * 
 */
package edu.mst.db.lexicon.acrabbrev;

import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.Future;

import edu.mst.concurrent.BaseWorker;
import edu.mst.db.ontol.names.ConceptNameDao;
import edu.mst.db.util.ConnDbName;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * Extracts acronyms and abbreviations flagged in the concept names table.
 * <p>
 * Includes persisting acronym/abbreviation words.
 * 
 * @author gjs
 *
 */
public class PersistAcrAbbrevFromNameWorker extends BaseWorker<Integer> {

    private int nbrNameUids = 0;
    private long[] nameUids = null;

    private int nbrNewAcrAbbrevWords = 0;
    private int nbrNamesProcessed = 0;
    private int rptInterval = 1000;

    private ConceptNameDao conceptNameDao = new ConceptNameDao();

    public PersistAcrAbbrevFromNameWorker(JdbcConnectionPool connPool) {
	super(connPool);
    }

    @Override
    public void run() throws Exception {

	/*
	 * Retrieve unique set of names
	 */
	{
	    System.out.println("Retrieving concept name UIDs for set of "
		    + "unique acronym/abbreviation names (removing duplicates)");
	    Map<String, Long> nameUidByUniqueText = conceptNameDao
		    .getAllUniqueAcrAbbrevNames(connPool);
	    nbrNameUids = nameUidByUniqueText.size();
	    nameUids = new long[nbrNameUids];
	    int pos = 0;
	    for (String uniqueName : nameUidByUniqueText.keySet()) {
		Long nameUid = nameUidByUniqueText.get(uniqueName);
		nameUids[pos] = nameUid;
		pos++;
	    }
	    Arrays.sort(nameUids);
	    System.out.printf(
		    "Retrieved %1$,d concept names with unique text - no duplicates %n",
		    nbrNameUids);
	}

	System.out.println("Now extracting acronyms and abbreviations");

	while (currentPos < nbrNameUids) {

	    maxPos = getNextBatchMaxPos();
	    startPos = currentPos;

	    for (int i = startPos; i <= maxPos; i++) {
		long conceptNameUid = nameUids[i];
		PersistAcrAbbrevFromNameCallable callable = new PersistAcrAbbrevFromNameCallable(
			conceptNameUid, connPool);
		svc.submit(callable);
	    }

	    for (int i = startPos; i <= maxPos; i++) {
		Future<Integer> future = svc.take();
		nbrNewAcrAbbrevWords += future.get();
		nbrNamesProcessed++;
		if (nbrNamesProcessed % rptInterval == 0) {
		    System.out.printf("Processed %1$,d concept names, "
			    + "persisting %2$,d new acronym/abbreviation words %n",
			    nbrNamesProcessed, nbrNewAcrAbbrevWords);
		}
	    }

	    currentPos = maxPos + 1;

	}

	System.out.printf(
		"Processed total of %1$,d concept names, "
			+ "persisting %2$,d new acronym/abbreviation words %n",
		nbrNamesProcessed, nbrNewAcrAbbrevWords);
    }

    @Override
    protected int getNextBatchMaxPos() throws Exception {
	// subtract 1 from max pos since positions are inclusive
	int maxPos = currentPos + threadCallablesBatchSize - 1;
	if (maxPos >= nbrNameUids - 1)
	    maxPos = nbrNameUids - 1;
	return maxPos;
    }

    public static void main(String[] args) {

	try {

	    JdbcConnectionPool connPool = new JdbcConnectionPool(
		    ConnDbName.CONCEPT_RECOGN);

	    try (PersistAcrAbbrevFromNameWorker worker = new PersistAcrAbbrevFromNameWorker(
		    connPool)) {
		worker.run();
	    }

	} catch (Exception ex) {
	    ex.printStackTrace();
	}

    }

}

/**
 * 
 */
package edu.mst.db.lexicon.acrabbrev;

import java.util.Comparator;

/**
 * Word in the acronym/abbreviation expansion
 * 
 * @author gjs
 *
 */
public class AcrAbbrevWord implements Comparable<AcrAbbrevWord> {

    private int acrAbbrevId;
    private int wordNbr;
    private int lexWordId;

    public AcrAbbrevWord(int acrAbbrevId, int wordNbr, int lexWordId) {
	this.acrAbbrevId = acrAbbrevId;
	this.wordNbr = wordNbr;
	this.lexWordId = lexWordId;
    }

    public static class Comparators {
	private static final Comparator<AcrAbbrevWord> ByAcrAbbrevId = (
		AcrAbbrevWord word1, AcrAbbrevWord word2) -> {
	    return Integer.compare(word1.acrAbbrevId, word2.acrAbbrevId);
	};
	private static final Comparator<AcrAbbrevWord> ByWordNbr = (
		AcrAbbrevWord word1, AcrAbbrevWord word2) -> {
	    return Integer.compare(word1.wordNbr, word2.wordNbr);
	};
	public static final Comparator<AcrAbbrevWord> ByLexWordId = (
		AcrAbbrevWord word1, AcrAbbrevWord word2) -> {
	    return Integer.compare(word1.lexWordId, word2.lexWordId);
	};
	public static final Comparator<AcrAbbrevWord> ByAcrAbbrevId_WordNbr = (
		AcrAbbrevWord word1, AcrAbbrevWord word2) -> ByAcrAbbrevId
			.thenComparing(ByWordNbr).compare(word1, word2);
    }

    @Override
    public int compareTo(AcrAbbrevWord word) {
	return Comparators.ByAcrAbbrevId_WordNbr.compare(this, word);
    }

    @Override
    public boolean equals(Object obj) {
	if (obj instanceof AcrAbbrevWord) {
	    AcrAbbrevWord word = (AcrAbbrevWord) obj;
	    return acrAbbrevId == word.acrAbbrevId && wordNbr == word.wordNbr;
	}
	return false;
    }

    @Override
    public int hashCode() {
	return Integer.hashCode(acrAbbrevId) + 7 * Integer.hashCode(wordNbr);
    }

    public int getAcrAbbrevId() {
	return acrAbbrevId;
    }

    public int getWordNbr() {
	return wordNbr;
    }

    public int getLexWordId() {
	return lexWordId;
    }

}

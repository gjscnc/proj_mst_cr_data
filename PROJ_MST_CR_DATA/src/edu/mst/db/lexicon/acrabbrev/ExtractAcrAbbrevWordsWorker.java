/**
 * 
 */
package edu.mst.db.lexicon.acrabbrev;

import java.util.List;
import java.util.concurrent.Future;

import edu.mst.concurrent.BaseWorker;
import edu.mst.db.util.ConnDbName;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * Extracts words from existing acronyms/abbreviations and persists in the
 * database.
 * 
 * @author gjs
 *
 */
public class ExtractAcrAbbrevWordsWorker extends BaseWorker<Integer> {

    private boolean isDeleteExisting = false;

    private int nbrAcrAbbrevIds = 0;
    private int[] acrAbbrevIds = null;

    private int nbrAcrAbbrevProcessed = 0;
    private int nbrNewAcrAbbrevWords = 0;
    private int rptInterval = 200;

    private AcrAbbrevDao acrAbbrevDao = new AcrAbbrevDao();

    public ExtractAcrAbbrevWordsWorker(boolean isDeleteExisting,
	    JdbcConnectionPool connPool) {
	super(connPool);
	this.isDeleteExisting = isDeleteExisting;
    }

    @Override
    public void run() throws Exception {

	if (isDeleteExisting) {
	    System.out.println(
		    "Deleting existing acronym/abbreviation words data.");
	    AcrAbbrevWordDao.deleteExisting(connPool);
	}

	{
	    System.out.println("Retrieving acronym/abbreviation IDs");
	    List<AcrAbbrev> acrAbbrevs = acrAbbrevDao.marshalAll(connPool);
	    nbrAcrAbbrevIds = acrAbbrevs.size();
	    acrAbbrevIds = new int[nbrAcrAbbrevIds];
	    int pos = 0;
	    for (AcrAbbrev acrAbbrev : acrAbbrevs) {
		acrAbbrevIds[pos] = acrAbbrev.getId();
		pos++;
	    }
	    System.out.printf("Retrieved %1$,d acronym/abbreviation IDs. %n",
		    nbrAcrAbbrevIds);
	}

	System.out
		.println("Now persisting acronym/abbreviation word sequences.");

	while (currentPos < nbrAcrAbbrevIds) {

	    maxPos = getNextBatchMaxPos();
	    startPos = currentPos;

	    for (int i = startPos; i <= maxPos; i++) {
		int acrAbbrevDbId = acrAbbrevIds[i];
		ExtractAcrAbbrevWordsCallable callable = new ExtractAcrAbbrevWordsCallable(
			acrAbbrevDbId, connPool);
		svc.submit(callable);
	    }

	    for (int i = startPos; i <= maxPos; i++) {
		Future<Integer> future = svc.take();
		nbrNewAcrAbbrevWords += future.get();
		nbrAcrAbbrevProcessed++;
		if (nbrAcrAbbrevProcessed % rptInterval == 0) {
		    System.out.printf("Processed %1$,d acronyms/abbreviations, "
			    + "persisting %2$,d new acronym/abbreviation words. %n",
			    nbrAcrAbbrevProcessed, nbrNewAcrAbbrevWords);
		}
	    }

	    currentPos = maxPos + 1;

	}

	System.out.printf(
		"Processed total of %1$,d acronyms/abbreviations, "
			+ "persisting %2$,d new acronym/abbreviation words. %n",
		nbrAcrAbbrevProcessed, nbrNewAcrAbbrevWords);
    }

    @Override
    protected int getNextBatchMaxPos() throws Exception {
	// subtract 1 from max pos since positions are inclusive
	int maxPos = currentPos + threadCallablesBatchSize - 1;
	if (maxPos >= nbrAcrAbbrevIds - 1)
	    maxPos = nbrAcrAbbrevIds - 1;
	return maxPos;
    }

    public static void main(String[] args) {

	try {

	    boolean isDeleteExisting = true;
	    JdbcConnectionPool connPool = new JdbcConnectionPool(
		    ConnDbName.CONCEPT_RECOGN);

	    try (ExtractAcrAbbrevWordsWorker worker = new ExtractAcrAbbrevWordsWorker(
		    isDeleteExisting, connPool)) {
		worker.run();
	    }

	} catch (Exception ex) {
	    ex.printStackTrace();
	}

    }

}

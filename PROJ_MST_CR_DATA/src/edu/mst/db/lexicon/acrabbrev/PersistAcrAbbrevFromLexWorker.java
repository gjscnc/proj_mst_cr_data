/**
 * 
 */
package edu.mst.db.lexicon.acrabbrev;

import java.util.Iterator;
import java.util.SortedSet;
import java.util.concurrent.Future;

import edu.mst.concurrent.BaseWorker;
import edu.mst.db.lexicon.LexWordDao;
import edu.mst.db.util.ConnDbName;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * @author gjs
 *
 */
public class PersistAcrAbbrevFromLexWorker extends BaseWorker<Integer> {

    private int nbrLexWordIds = 0;
    private int[] lexWordIds = null;

    private int rptInterval = 200;
    private int nbrLexWordsProcessed = 0;
    private int nbrNewAcrAbbrev = 0;

    public PersistAcrAbbrevFromLexWorker(JdbcConnectionPool connPool) {
	super(connPool);
    }

    @Override
    public void run() throws Exception {

	{
	    System.out.println("Marshaling all lexicon word IDs");
	    SortedSet<Integer> allIds = LexWordDao.marshalAllIds(connPool);
	    nbrLexWordIds = allIds.size();
	    lexWordIds = new int[nbrLexWordIds];
	    Iterator<Integer> idIter = allIds.iterator();
	    int pos = 0;
	    while (idIter.hasNext()) {
		lexWordIds[pos] = idIter.next().intValue();
		pos++;
	    }
	    System.out.printf("Marshaled %1$,d lexicon word IDs %n",
		    nbrLexWordIds);
	}

	System.out.println(
		"Now processing lexicon to extract acronyms/abbreviations");

	while (currentPos < nbrLexWordIds) {

	    maxPos = getNextBatchMaxPos();
	    startPos = currentPos;

	    for (int i = startPos; i <= maxPos; i++) {
		int lexWordId = lexWordIds[i];
		PersistAcrAbbrevFromLexCallable callable = new PersistAcrAbbrevFromLexCallable(
			lexWordId, connPool);
		svc.submit(callable);
	    }

	    for (int i = startPos; i <= maxPos; i++) {
		Future<Integer> future = svc.take();
		nbrNewAcrAbbrev += future.get();
		nbrLexWordsProcessed++;
		if (nbrLexWordsProcessed % rptInterval == 0) {
		    System.out.printf("Processed %1$,d lexicon tokens, "
			    + "creating %2$,d new acronyms/abbreviations. %n",
			    nbrLexWordsProcessed, nbrNewAcrAbbrev);
		}
	    }

	    currentPos = maxPos + 1;

	}

	System.out.printf(
		"Processed total of %1$,d lexicon tokens, "
			+ "creating %2$,d new acronyms/abbreviations . %n",
		nbrLexWordsProcessed, nbrNewAcrAbbrev);
    }

    @Override
    protected int getNextBatchMaxPos() throws Exception {
	// subtract 1 from max pos since positions are inclusive
	int maxPos = currentPos + threadCallablesBatchSize - 1;
	if (maxPos >= nbrLexWordIds - 1)
	    maxPos = nbrLexWordIds - 1;
	return maxPos;
    }

    public static void main(String[] args) {

	try {

	    JdbcConnectionPool connPool = new JdbcConnectionPool(
		    ConnDbName.CONCEPT_RECOGN);

	    try (PersistAcrAbbrevFromLexWorker worker = new PersistAcrAbbrevFromLexWorker(
		    connPool)) {
		worker.run();
	    }

	} catch (Exception ex) {
	    ex.printStackTrace();
	}
    }

}

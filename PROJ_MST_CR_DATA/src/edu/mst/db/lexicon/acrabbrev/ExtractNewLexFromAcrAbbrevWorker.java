/**
 * 
 */
package edu.mst.db.lexicon.acrabbrev;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Future;

import edu.mst.concurrent.BaseWorker;
import edu.mst.db.lexicon.LexWord;
import edu.mst.db.lexicon.LexWordDao;
import edu.mst.db.lexicon.StopWords;
import edu.mst.db.text.util.NlpUtil;
import edu.mst.db.util.ConnDbName;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * Extracts new lexicon words from the expanded form of existing
 * acronyms/abbreviations.
 * <p>
 * Run this as quality check to ensure lexicon is complete;
 * 
 * @author gjs
 *
 */
public class ExtractNewLexFromAcrAbbrevWorker extends BaseWorker<Set<String>> {

    private int nbrAcrAbbrevIds = 0;
    private int[] acrAbbrevIds = null;

    private int nbrAcrAbbrevProcessed = 0;
    private int nbrNewLexTokens = 0;
    private int nbrNewLexTokensPersisted = 0;
    private int rptInterval = 200;

    private AcrAbbrevDao acrAbbrevDao = new AcrAbbrevDao();
    private Set<String> newLexTokens = new HashSet<String>();
    private LexWordDao lexWordDao = new LexWordDao();
    private StopWords stopWords = null;
    private NlpUtil nlpUtil = new NlpUtil();

    public ExtractNewLexFromAcrAbbrevWorker(JdbcConnectionPool connPool)
	    throws Exception {
	super(connPool);
	stopWords = new StopWords(connPool);
    }

    @Override
    public void run() throws Exception {

	{
	    System.out.println("Retrieving acronym/abbreviation IDs");
	    List<AcrAbbrev> acrAbbrevs = acrAbbrevDao.marshalAll(connPool);
	    nbrAcrAbbrevIds = acrAbbrevs.size();
	    acrAbbrevIds = new int[nbrAcrAbbrevIds];
	    int pos = 0;
	    for (AcrAbbrev acrAbbrev : acrAbbrevs) {
		acrAbbrevIds[pos] = acrAbbrev.getId();
		pos++;
	    }
	    System.out.printf("Retrieved %1$,d acronym/abbreviation IDs. %n",
		    nbrAcrAbbrevIds);
	}

	System.out.println(
		"Now extracting new lexicon tokens from acronyms/abbreviations.");
	while (currentPos < nbrAcrAbbrevIds) {

	    maxPos = getNextBatchMaxPos();
	    startPos = currentPos;

	    for (int i = startPos; i <= maxPos; i++) {
		int acrAbbrevId = acrAbbrevIds[i];
		ExtractNewLexFromAcrAbbrevCallable callable = new ExtractNewLexFromAcrAbbrevCallable(
			acrAbbrevId, connPool);
		svc.submit(callable);
	    }

	    for (int i = startPos; i <= maxPos; i++) {
		Future<Set<String>> future = svc.take();
		Set<String> newTokens = future.get();
		newLexTokens.addAll(newTokens);
		nbrNewLexTokens += newTokens.size();
		nbrAcrAbbrevProcessed++;
		if (nbrAcrAbbrevProcessed % rptInterval == 0) {
		    System.out.printf(
			    "Processed %1$,d acronyms/abbreviations, "
				    + "extracting %2$,d new lexicon tokens. %n",
			    nbrAcrAbbrevProcessed, nbrNewLexTokens);
		}
	    }
	    
	    currentPos = maxPos + 1;

	}

	System.out.printf(
		"Processed total of %1$,d acronyms/abbreviations, "
			+ "extracting %2$,d new lexicon tokens. %n",
		nbrAcrAbbrevProcessed, nbrNewLexTokens);

	if (nbrNewLexTokens == 0) {
	    System.out.println("No new lexicon tokens - finished.");
	} else {
	    System.out.println("Now persisting new lexicon words");
	    for (String newToken : newLexTokens) {
		boolean isStopWord = stopWords.getStopWordTokens()
			.contains(newToken);
		boolean isPunctOrDelim = nlpUtil.isPunctOrDelimChar(newToken);
		LexWord lexWord = new LexWord();
		lexWord.setToken(newToken);
		lexWord.setIsStopWord(isStopWord);
		lexWord.setIsPunct(isPunctOrDelim);
		lexWordDao.persist(lexWord, connPool);
		nbrNewLexTokensPersisted++;
		if (nbrNewLexTokensPersisted % rptInterval == 0) {
		    System.out.printf("Persisted %1$,d new lexicon words %n",
			    nbrNewLexTokensPersisted);
		}
	    }
	    System.out.printf("Persisted total of %1$,d new lexicon words %n",
		    nbrNewLexTokensPersisted);
	}
    }

    @Override
    protected int getNextBatchMaxPos() throws Exception {
	// subtract 1 from max pos since positions are inclusive
	int maxPos = currentPos + threadCallablesBatchSize - 1;
	if (maxPos >= nbrAcrAbbrevIds - 1)
	    maxPos = nbrAcrAbbrevIds - 1;
	return maxPos;
    }

    public static void main(String[] args) {

	try {

	    JdbcConnectionPool connPool = new JdbcConnectionPool(
		    ConnDbName.CONCEPT_RECOGN);

	    try (ExtractNewLexFromAcrAbbrevWorker worker = new ExtractNewLexFromAcrAbbrevWorker(
		    connPool)) {
		worker.run();
	    }

	} catch (Exception ex) {
	    ex.printStackTrace();
	}

    }

}

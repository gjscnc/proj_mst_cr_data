package edu.mst.db.lexicon.acrabbrev;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.SortedSet;
import java.util.concurrent.Callable;

import edu.mst.db.lexicon.LexWordDao;
import edu.mst.db.lexicon.acrabbrev.AcrAbbrevUtils.ExpansionWord;
import edu.mst.db.util.JdbcConnectionPool;

public class ExtractNewLexFromAcrAbbrevCallable
	implements Callable<Set<String>> {

    private int acrAbbrevId = 0;
    private JdbcConnectionPool connPool;

    private AcrAbbrevDao acrAbbrevDao = new AcrAbbrevDao();
    private LexWordDao lexWordDao = new LexWordDao();

    public ExtractNewLexFromAcrAbbrevCallable(int acrAbbrevId,
	    JdbcConnectionPool connPool) {
	this.acrAbbrevId = acrAbbrevId;
	this.connPool = connPool;
    }

    @Override
    public Set<String> call() throws Exception {

	Set<String> newLexTokens = new HashSet<String>();
	AcrAbbrev acrAbbrev = acrAbbrevDao.marshalById(acrAbbrevId, connPool);
	try (AcrAbbrevUtils acrAbbrevUtil = new AcrAbbrevUtils(connPool)) {
	    SortedSet<ExpansionWord> expansionWords = acrAbbrevUtil
		    .extractExpansionWords(acrAbbrev.getExpanded());
	    Iterator<ExpansionWord> expansionWordsIter = expansionWords
		    .iterator();
	    while (expansionWordsIter.hasNext()) {
		ExpansionWord expansionWord = expansionWordsIter.next();
		boolean isLexWordExists = lexWordDao.exists(expansionWord.token,
			connPool);
		if (isLexWordExists == false) {
		    newLexTokens.add(expansionWord.token);
		}
	    }
	}
	return newLexTokens;
    }

}

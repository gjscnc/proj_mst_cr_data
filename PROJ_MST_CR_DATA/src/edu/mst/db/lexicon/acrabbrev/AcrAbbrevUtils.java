/**
 * 
 */
package edu.mst.db.lexicon.acrabbrev;

import java.io.StringReader;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.Vector;

import edu.mst.db.lexicon.LexWord;
import edu.mst.db.lexicon.LexWordDao;
import edu.mst.db.text.util.LexConstants;
import edu.mst.db.util.JdbcConnectionPool;
import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.process.DocumentPreprocessor;
import edu.stanford.nlp.process.PTBTokenizer;
import edu.stanford.nlp.process.TokenizerFactory;
import gov.nih.nlm.nls.lvg.Api.LvgApi;
import gov.nih.nlm.nls.lvg.Flows.ToExpansions;
import gov.nih.nlm.nls.lvg.Lib.LexItem;

/**
 * Utilities for determining if a token is an acronym/abbreviation, extracting
 * the expanded form, and persisting acronyms/abbreviations and their word
 * sequence.
 * 
 * @author gjs
 *
 */
public class AcrAbbrevUtils implements AutoCloseable {

    private static final String lvgPropsFilePath = LexConstants.lvgPropertiesDirLinux
	    .concat(LexConstants.lvgPropertiesFileName);
    private LvgApi lvgApi = new LvgApi(lvgPropsFilePath);

    private JdbcConnectionPool connPool;

    private LexWordDao lexWordDao = new LexWordDao();

    private static final String stanfordParserOptions = "untokenizable=noneDelete,splitHyphenated=true,ptb3Escaping=false";

    public AcrAbbrevUtils(JdbcConnectionPool connPool) {
	this.connPool = connPool;
    }

    public Set<String> parseNewAcrAbbrevTokensFromLexWord(int lexWordId)
	    throws SQLException {

	// get lexicon word
	LexWord lexWord = lexWordDao.marshalById(lexWordId, connPool);
	if (lexWord == null) {
	    String errMsg = String.format(
		    "Cannot marshal lexicon token for ID = %1$d", lexWordId);
	    throw new SQLException(errMsg);
	}

	String token = lexWord.getToken();

	Set<String> newLexTokens = parseNewAcrAbbrevTokensFromToken(token);

	return newLexTokens;
    }

    public Set<String> parseNewAcrAbbrevTokensFromToken(String token)
	    throws SQLException {

	Set<String> newLexTokens = new HashSet<String>();
	SortedSet<AcrAbbrevFromLexToken> acrAbbrevs = extractAcrAbbrevsFromToken(
		token, true);
	if (acrAbbrevs.size() == 0) {
	    return newLexTokens;
	}
	for (AcrAbbrevFromLexToken acrAbbrevFromLexToken : acrAbbrevs) {
	    for (ExpansionWord word : acrAbbrevFromLexToken.words) {
		boolean isLexWordExistsInDb = lexWordDao.exists(word.token,
			connPool);
		if (isLexWordExistsInDb == false) {
		    newLexTokens.add(word.token);
		}
	    }
	}
	return newLexTokens;
    }

    public SortedSet<AcrAbbrevFromLexToken> extractAcrAbbrevsFromLexWord(
	    int lexWordId, boolean isExtractWords) throws SQLException {
	String token = lexWordDao.marshalById(lexWordId, connPool).getToken();
	return extractAcrAbbrevsFromToken(token, isExtractWords);
    }

    public SortedSet<AcrAbbrevFromLexToken> extractAcrAbbrevsFromToken(
	    String token, boolean isExtractWords) {

	SortedSet<AcrAbbrevFromLexToken> acrAbbrevs = new TreeSet<AcrAbbrevFromLexToken>();
	Vector<LexItem> outs = ToExpansions.Mutate(new LexItem(token),
		lvgApi.GetConnection(), false, true);
	if (outs == null || outs.size() == 0) {
	    return acrAbbrevs;
	}
	Iterator<LexItem> lexItemIter = outs.elements().asIterator();
	while (lexItemIter.hasNext()) {
	    LexItem lexItem = lexItemIter.next();
	    String lexTargetTerm = lexItem.GetTargetTerm();
	    if (lexTargetTerm == null || lexTargetTerm.isBlank()
		    || lexTargetTerm.isEmpty()) {
		continue;
	    }
	    String targetTermLc = lexTargetTerm.toLowerCase();

	    // do not keep if token and expanded form are the same
	    if (token.equals(targetTermLc)) {
		continue;
	    }

	    AcrAbbrevFromLexToken acrAbbrevFromLexToken = new AcrAbbrevFromLexToken();
	    acrAbbrevFromLexToken.lexToken = token;
	    acrAbbrevFromLexToken.expansion = targetTermLc;
	    if (isExtractWords) {
		SortedSet<ExpansionWord> expansionWords = extractExpansionWords(
			acrAbbrevFromLexToken.expansion);
		acrAbbrevFromLexToken.words.addAll(expansionWords);
	    }

	    acrAbbrevs.add(acrAbbrevFromLexToken);

	}

	return acrAbbrevs;

    }

    public SortedSet<ExpansionWord> extractExpansionWords(
	    String expansionText) {

	SortedSet<ExpansionWord> words = new TreeSet<ExpansionWord>();
	// parse words for expansion
	StringReader reader = new StringReader(expansionText);
	DocumentPreprocessor tokenizer = new DocumentPreprocessor(reader);
	TokenizerFactory<? extends HasWord> factory = PTBTokenizer
		.coreLabelFactory();
	factory.setOptions(stanfordParserOptions);
	tokenizer.setTokenizerFactory(factory);
	Iterator<List<HasWord>> stanfSentIter = tokenizer.iterator();
	/*
	 * should have only one sentence, but just in case iterate over all of
	 * them
	 */
	int wordNbr = 0;
	while (stanfSentIter.hasNext()) {
	    List<HasWord> sentWords = stanfSentIter.next();
	    Iterator<HasWord> sentWordsIter = sentWords.iterator();
	    while (sentWordsIter.hasNext()) {
		HasWord word = sentWordsIter.next();
		String wordToken = word.word().toLowerCase();
		ExpansionWord expansionWord = new ExpansionWord();
		expansionWord.token = wordToken;
		expansionWord.wordNbr = wordNbr;
		words.add(expansionWord);
		wordNbr++;
	    }
	}
	return words;
    }

    public class AcrAbbrevFromLexToken
	    implements Comparable<AcrAbbrevFromLexToken> {
	public String lexToken = null;
	public String expansion = null;
	public SortedSet<ExpansionWord> words = new TreeSet<ExpansionWord>(
		Comparators.ByWordNbr);

	@Override
	public int compareTo(AcrAbbrevFromLexToken acrAbbrevFromLexToken) {
	    int result = lexToken.compareTo(acrAbbrevFromLexToken.lexToken);
	    if (result != 0) {
		return result;
	    }
	    return expansion.compareTo(acrAbbrevFromLexToken.expansion);
	}

	@Override
	public boolean equals(Object obj) {
	    if (obj instanceof AcrAbbrevFromLexToken) {
		AcrAbbrevFromLexToken acrAbbrevFromLexToken = (AcrAbbrevFromLexToken) obj;
		return lexToken.equals(acrAbbrevFromLexToken.lexToken)
			&& expansion.equals(acrAbbrevFromLexToken.expansion);
	    }
	    return false;
	}

	@Override
	public int hashCode() {
	    return lexToken.hashCode() + 7 * expansion.hashCode();
	}
    }

    public static class Comparators {
	public static final Comparator<ExpansionWord> ByWordNbr = (
		ExpansionWord word1, ExpansionWord word2) -> Integer
			.compare(word1.wordNbr, word2.wordNbr);
    }

    public class ExpansionWord implements Comparable<ExpansionWord> {
	public String token = null;
	public int wordNbr = 0;

	@Override
	public int compareTo(ExpansionWord word) {
	    int result = token.compareTo(word.token);
	    if (result != 0) {
		return result;
	    }
	    return Integer.compare(wordNbr, word.wordNbr);
	}

	@Override
	public boolean equals(Object obj) {
	    if (obj instanceof ExpansionWord) {
		ExpansionWord word = (ExpansionWord) obj;
		return token.equals(word.token) && wordNbr == word.wordNbr;
	    }
	    return false;
	}

	@Override
	public int hashCode() {
	    return token.hashCode() + 7 * Integer.hashCode(wordNbr);
	}

    }

    @Override
    public void close() throws Exception {
	lvgApi.CleanUp();
	lvgApi.CloseConnection(lvgApi.GetConnection());
    }

}

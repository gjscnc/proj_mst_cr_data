/**
 * 
 */
package edu.mst.db.lexicon.acrabbrev;

import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.Callable;

import edu.mst.db.ontol.names.ConceptName;
import edu.mst.db.ontol.names.ConceptNameDao;
import edu.mst.db.ontol.words.ConceptNameWord;
import edu.mst.db.ontol.words.ConceptNameWordDao;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * @author gjs
 *
 */
public class PersistAcrAbbrevFromNameCallable implements Callable<Integer> {

    private long conceptNameUid;
    private JdbcConnectionPool connPool;

    private ConceptNameDao nameDao = new ConceptNameDao();
    private ConceptNameWordDao nameWordDao = new ConceptNameWordDao();
    private AcrAbbrevDao acrAbbrevDao = new AcrAbbrevDao();
    private AcrAbbrevWordDao acrAbbrevWordDao = new AcrAbbrevWordDao();

    public PersistAcrAbbrevFromNameCallable(long conceptNameUid,
	    JdbcConnectionPool connPool) {
	this.conceptNameUid = conceptNameUid;
	this.connPool = connPool;
    }

    @Override
    public Integer call() throws Exception {

	ConceptName name = nameDao.marshal(conceptNameUid, connPool);
	if (name.isAcrAbbrev() == false) {
	    return 0;
	}
	
	if(conceptNameUid == 1227242010 || conceptNameUid == 215933015) {
	    System.out.println("Test Case");
	}

	String acrAbbrevTokenLc = nameDao
		.getAcrAbbrevToken(conceptNameUid, connPool).toLowerCase();
	String expandedFormLc = nameDao
		.getAcrAbbrevExpandedForm(conceptNameUid, connPool)
		.toLowerCase();

	Integer nbrNewWords = 0;

	List<ConceptNameWord> conceptNameWords = nameWordDao
		.marshalWordsForName(conceptNameUid, connPool);

	// see if acronym/abbreviation exists
	List<AcrAbbrev> acrAbbrevs = acrAbbrevDao.marshal(acrAbbrevTokenLc,
		connPool);
	boolean isAcrAbbrevExists = false;
	boolean isAcrAbbrevWordsExist = false;
	AcrAbbrev acrAbbrev = null;
	if (acrAbbrevs.size() > 0) {
	    /*
	     * check to see if any acronym/abbreviation has same expanded form
	     */
	    for (AcrAbbrev existingAcrAbbrev : acrAbbrevs) {
		if (expandedFormLc.equals(existingAcrAbbrev.getExpanded())) {
		    acrAbbrev = existingAcrAbbrev;
		    isAcrAbbrevExists = true;
		    break;
		}
	    }
	}

	/*
	 * See if words exist
	 */
	if (isAcrAbbrevExists) {
	    if(acrAbbrev.getId() == 3134 || acrAbbrev.getId() == 9784){
		System.out.println("Test Case");
	    }
	    nameDao.setAcrAbbrevId(acrAbbrev.getId(), conceptNameUid, connPool);
	    name.setAcrAbbrevId(acrAbbrev.getId());
	    AcrAbbrevWord firstAcrAbbrevWord = acrAbbrevWordDao.marshal(acrAbbrev.getId(), 0, connPool);
	    isAcrAbbrevWordsExist = firstAcrAbbrevWord != null;
	}

	// existing acronym/abbreviation not found
	if (!isAcrAbbrevExists) {
	    acrAbbrev = new AcrAbbrev();
	    acrAbbrev.setToken(acrAbbrevTokenLc);
	    acrAbbrev.setLexWordId(conceptNameWords.get(0).getLexWordId());
	    acrAbbrev.setExpanded(expandedFormLc);
	    acrAbbrevDao.persist(acrAbbrev, connPool);
	    if(acrAbbrev.getId() == 3134){
		System.out.println("Test Case");
	    }
	    nameDao.setAcrAbbrevId(acrAbbrev.getId(), conceptNameUid, connPool);
	    name.setAcrAbbrevId(acrAbbrev.getId());
	}
	// create words
	if (!isAcrAbbrevWordsExist) {
	    if(acrAbbrev.getId() == 3134){
		System.out.println("Test Case");
	    }
	    SortedSet<AcrAbbrevWord> newAcrAbbrevWords = new TreeSet<AcrAbbrevWord>();
	    for (int i = 1; i < conceptNameWords.size(); i++) {
		ConceptNameWord conceptNameWord = conceptNameWords.get(i);
		AcrAbbrevWord acrAbbrevWord = new AcrAbbrevWord(
			acrAbbrev.getId(), i - 1,
			conceptNameWord.getLexWordId());
		newAcrAbbrevWords.add(acrAbbrevWord);
	    }
	    acrAbbrevWordDao.persist(newAcrAbbrevWords, connPool);
	    nbrNewWords = newAcrAbbrevWords.size();
	}

	return nbrNewWords;
    }

}

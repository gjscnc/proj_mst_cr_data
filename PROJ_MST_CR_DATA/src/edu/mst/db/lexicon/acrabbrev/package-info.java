/**
 * 
 */
/**
 * Components for the extracting lexicon words from acronyms/abbreviations, and
 * for persisting acronyms/abbreviations and their word sequence.
 * 
 * @author gjs
 *
 */
package edu.mst.db.lexicon.acrabbrev;
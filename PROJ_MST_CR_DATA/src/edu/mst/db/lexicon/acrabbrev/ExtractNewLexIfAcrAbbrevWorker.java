/**
 * 
 */
package edu.mst.db.lexicon.acrabbrev;

import java.util.Iterator;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.Future;

import edu.mst.concurrent.BaseWorker;
import edu.mst.db.lexicon.LexWord;
import edu.mst.db.lexicon.LexWordDao;
import edu.mst.db.lexicon.StopWords;
import edu.mst.db.text.util.NlpUtil;
import edu.mst.db.util.ConnDbName;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * Worker for parallel processing to extract and persist new lexicon words from
 * the expanded form of acronyms/abbreviations found in the lexicon.
 * <p>
 * A concept name may be an acronym/abbreviation, but these names contain the
 * expanded form and are identified and parsed using
 * {@link PersistAcrAbbrevFromNameWorker}.
 * <p>
 * <b>NOTE:</b> After executing this component (to extract and persist any new
 * lexicon words), then execute the other components in this package for
 * persisting acronyms/abbreviations from concept names
 * ({@link PersistAcrAbbrevFromNameWorker}), the lexicon
 * ({@link PersistAcrAbbrevFromLexWorker}), and gold standard annotations
 * ({@link AcrAbbrevFromGoldStdAnnotWorker}).
 * 
 * @author gjs
 *
 */
public class ExtractNewLexIfAcrAbbrevWorker extends BaseWorker<Set<String>> {

    private StopWords stopWords;
    private NlpUtil nlpUtil = new NlpUtil();

    private int nbrLexWordIds = 0;
    private int[] lexWordIds = null;

    private LexWordDao lexWordDao = new LexWordDao();

    private SortedSet<String> newLexWordTokens = new TreeSet<String>();

    private int rptInterval = 200;
    private int nbrLexWordsProcessed = 0;
    private int nbrNewLexTokens = 0;
    private int nbrNewLexTokensPersisted = 0;

    public ExtractNewLexIfAcrAbbrevWorker(JdbcConnectionPool connPool)
	    throws Exception {
	super(connPool);
	stopWords = new StopWords(connPool);
    }

    @Override
    public void run() throws Exception {

	{
	    System.out.println("Marshaling all lexicon word IDs");
	    SortedSet<Integer> allIds = LexWordDao.marshalAllIds(connPool);
	    nbrLexWordIds = allIds.size();
	    lexWordIds = new int[nbrLexWordIds];
	    Iterator<Integer> idIter = allIds.iterator();
	    int pos = 0;
	    while (idIter.hasNext()) {
		lexWordIds[pos] = idIter.next().intValue();
		pos++;
	    }
	    System.out.printf("Marshaled %1$,d lexicon word IDs %n",
		    nbrLexWordIds);
	}

	System.out.println("Now processing all tokens in lexicon");
	while (currentPos < nbrLexWordIds) {

	    maxPos = getNextBatchMaxPos();
	    startPos = currentPos;

	    for (int i = startPos; i <= maxPos; i++) {
		int lexWordId = lexWordIds[i];
		ExtractNewLexIfAcrAbbrevCallable callable = new ExtractNewLexIfAcrAbbrevCallable(
			lexWordId, connPool);
		svc.submit(callable);
	    }

	    for (int i = startPos; i <= maxPos; i++) {
		Future<Set<String>> future = svc.take();
		Set<String> newLexWords = future.get();
		nbrNewLexTokens += newLexWords.size();
		newLexWordTokens.addAll(newLexWords);
		nbrLexWordsProcessed++;
		if (nbrLexWordsProcessed % rptInterval == 0) {
		    System.out.printf(
			    "Processed %1$,d lexicon tokens, identifying %2$,d new tokens. %n",
			    nbrLexWordsProcessed, nbrNewLexTokens);
		}
	    }

	    currentPos = maxPos + 1;

	}

	System.out.printf(
		"Processed total of %1$,d lexicon tokens, identifying %2$,d new tokens. %n",
		nbrLexWordsProcessed, nbrNewLexTokens);

	if (nbrNewLexTokens > 0) {
	    Iterator<String> newTokensIter = newLexWordTokens.iterator();
	    while (newTokensIter.hasNext()) {
		String newToken = newTokensIter.next().toLowerCase();
		boolean isStopWord = stopWords.getStopWordTokens()
			.contains(newToken);
		boolean isPunctOrDelim = nlpUtil.isPunctOrDelimChar(newToken);
		LexWord lexWord = new LexWord();
		lexWord.setToken(newToken);
		lexWord.setIsStopWord(isStopWord);
		lexWord.setIsPunct(isPunctOrDelim);
		lexWordDao.persist(lexWord, connPool);
		nbrNewLexTokensPersisted++;
		if (nbrNewLexTokensPersisted % rptInterval == 0) {
		    System.out.printf("Persisted %1$,d new lexicon words %n",
			    nbrNewLexTokensPersisted);
		}
	    }
	    System.out.printf("Persisted total of %1$,d new lexicon words %n",
		    nbrNewLexTokensPersisted);
	} else {
	    System.out.println("No new lexicon tokens, processing finished");
	}
    }

    @Override
    protected int getNextBatchMaxPos() throws Exception {
	// subtract 1 from max pos since positions are inclusive
	int maxPos = currentPos + threadCallablesBatchSize - 1;
	if (maxPos >= nbrLexWordIds - 1)
	    maxPos = nbrLexWordIds - 1;
	return maxPos;
    }

    public static void main(String[] args) {
	
	try {
	    
	    JdbcConnectionPool connPool = new JdbcConnectionPool(ConnDbName.CONCEPT_RECOGN);
	    
	    try(ExtractNewLexIfAcrAbbrevWorker worker = new ExtractNewLexIfAcrAbbrevWorker(connPool)){
		worker.run();
	    }
	    
	} catch(Exception ex) {
	    ex.printStackTrace();
	}

    }

}

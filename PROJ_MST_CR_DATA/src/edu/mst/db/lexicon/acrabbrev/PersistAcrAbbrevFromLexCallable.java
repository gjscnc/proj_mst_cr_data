/**
 * 
 */
package edu.mst.db.lexicon.acrabbrev;

import java.util.Iterator;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.Callable;

import edu.mst.db.lexicon.LexWord;
import edu.mst.db.lexicon.LexWordDao;
import edu.mst.db.lexicon.acrabbrev.AcrAbbrevUtils.AcrAbbrevFromLexToken;
import edu.mst.db.lexicon.acrabbrev.AcrAbbrevUtils.ExpansionWord;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * @author gjs
 *
 */
public class PersistAcrAbbrevFromLexCallable
	implements Callable<Integer> {

    private int lexWordId;
    private JdbcConnectionPool connPool;

    private AcrAbbrevDao acrAbbrevDao = new AcrAbbrevDao();
    private AcrAbbrevWordDao acrAbbrevWordDao = new AcrAbbrevWordDao();
    private LexWordDao lexWordDao = new LexWordDao();

    public PersistAcrAbbrevFromLexCallable(int lexWordId,
	    JdbcConnectionPool connPool) {
	this.lexWordId = lexWordId;
	this.connPool = connPool;
    }

    @Override
    public Integer call() throws Exception {

	int nbrNewAcrAbbrev = 0;

	// get lexicon word
	LexWord lexWord = lexWordDao.marshalById(lexWordId, connPool);
	String lexToken = lexWord.getToken();
	
	if(lexToken.equals("2cda")) {
	    System.out.println("Text Case");
	}

	List<AcrAbbrev> acrAbbrevsInDb = acrAbbrevDao.marshal(lexToken,
		connPool);

	try (AcrAbbrevUtils acrAbbrevUtils = new AcrAbbrevUtils(connPool)) {

	    SortedSet<AcrAbbrevFromLexToken> acrAbbrevFromToken = acrAbbrevUtils
		    .extractAcrAbbrevsFromToken(lexToken, true);
	    /*
	     * If no acr/abbrev found for token, then return
	     */
	    if (acrAbbrevFromToken.size() == 0) {
		return nbrNewAcrAbbrev;
	    }

	    Iterator<AcrAbbrevFromLexToken> acrAbbrevFromTokenIter = acrAbbrevFromToken
		    .iterator();

	    /*
	     * If token not found in acr/abbrev data, then need to create new
	     * set
	     */
	    boolean isAnyAcrAbbrevInDb = acrAbbrevsInDb.size() > 0;

	    while (acrAbbrevFromTokenIter.hasNext()) {

		AcrAbbrevFromLexToken acrAbbrevFromLexToken = acrAbbrevFromTokenIter
			.next();
		String acrAbbrevTokenLc = acrAbbrevFromLexToken.lexToken;
		String expandedFormLc = acrAbbrevFromLexToken.expansion
			.toLowerCase();
		boolean isAcrAbbrevExists = false;
		boolean isAcrAbbrevWordsExist = false;
		AcrAbbrev acrAbbrev = null;
		if (isAnyAcrAbbrevInDb) {
		    /*
		     * check to see if any acronym/abbreviation has same
		     * expanded form
		     */
		    for (AcrAbbrev existingAcrAbbrev : acrAbbrevsInDb) {
			if (expandedFormLc
				.equals(existingAcrAbbrev.getExpanded())) {
			    acrAbbrev = existingAcrAbbrev;
			    isAcrAbbrevExists = true;
			    break;
			}
		    }

		    /*
		     * See if words exist
		     */
		    if (isAcrAbbrevExists) {
			AcrAbbrevWord firstAcrAbbrevWord = acrAbbrevWordDao
				.marshal(acrAbbrev.getId(), 0, connPool);
			isAcrAbbrevWordsExist = firstAcrAbbrevWord != null;
		    }

		    // existing acronym/abbreviation not found - create
		    if (!isAcrAbbrevExists) {
			acrAbbrev = new AcrAbbrev();
			acrAbbrev.setToken(acrAbbrevTokenLc);
			int lexWordId = lexWordDao
				.getIdByToken(acrAbbrevTokenLc, connPool);
			acrAbbrev.setLexWordId(lexWordId);
			acrAbbrev.setExpanded(expandedFormLc);
			acrAbbrevDao.persist(acrAbbrev, connPool);
			nbrNewAcrAbbrev++;
		    }

		    // existing words not found - create
		    if (!isAcrAbbrevWordsExist) {
			SortedSet<AcrAbbrevWord> newAcrAbbrevWords = new TreeSet<AcrAbbrevWord>();
			for (ExpansionWord expansionWord : acrAbbrevFromLexToken.words) {
			    int lexWordId = lexWordDao.getIdByToken(
				    expansionWord.token.toLowerCase(),
				    connPool);
			    AcrAbbrevWord acrAbbrevWord = new AcrAbbrevWord(
				    acrAbbrev.getId(), expansionWord.wordNbr,
				    lexWordId);
			    newAcrAbbrevWords.add(acrAbbrevWord);
			}
			acrAbbrevWordDao.persist(newAcrAbbrevWords, connPool);
		    }

		} else {
		    
		    acrAbbrev = new AcrAbbrev();
		    acrAbbrev.setToken(acrAbbrevTokenLc);
		    int acrAbbrevLexWordId = lexWordDao
			    .getIdByToken(acrAbbrevTokenLc, connPool);
		    acrAbbrev.setLexWordId(acrAbbrevLexWordId);
		    acrAbbrev.setExpanded(expandedFormLc);
		    acrAbbrevDao.persist(acrAbbrev, connPool);
		    nbrNewAcrAbbrev++;
		    
		    SortedSet<AcrAbbrevWord> newAcrAbbrevWords = new TreeSet<AcrAbbrevWord>();
		    for (ExpansionWord expansionWord : acrAbbrevFromLexToken.words) {
			int expansionLexWordId = lexWordDao.getIdByToken(
				expansionWord.token.toLowerCase(), connPool);
			AcrAbbrevWord acrAbbrevWord = new AcrAbbrevWord(
				acrAbbrev.getId(), expansionWord.wordNbr,
				expansionLexWordId);
			newAcrAbbrevWords.add(acrAbbrevWord);
		    }
		    acrAbbrevWordDao.persist(newAcrAbbrevWords, connPool);
		}
	    }
	}

	return nbrNewAcrAbbrev;

    }

}

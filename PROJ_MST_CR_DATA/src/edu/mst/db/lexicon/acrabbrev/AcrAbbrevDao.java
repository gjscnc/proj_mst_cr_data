package edu.mst.db.lexicon.acrabbrev;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import edu.mst.db.lexicon.LexWord;
import edu.mst.db.lexicon.LexWordDao;
import edu.mst.db.nlm.metamap.MincoManAcrAbbrev;
import edu.mst.db.util.JdbcConnectionPool;

public class AcrAbbrevDao {

    public static final String[] autoPkField = new String[] { "id" };
    public static final String persistAcrAbbrSql = "INSERT INTO conceptrecogn.acrabbrev "
	    + "(token, expanded, lexWordId) VALUES (?,?,?) ";
    public static final String delAllAccrAbbrevSql = "TRUNCATE TABLE conceptrecogn.acrabbrev";
    public static final String resetAutoIdSql = "ALTER TABLE conceptrecogn.acrabbrev AUTO_INCREMENT = 1";
    public static final String getAcrAbbrIdSql = "SELECT id FROM conceptrecogn.acrabbrev WHERE token=?";
    public static final String marshalByTokenSql = "SELECT id, token, expanded, lexWordId "
	    + "FROM conceptrecogn.acrabbrev where token = ?";
    public static final String marshalByIdSql = "SELECT id, token, expanded, lexWordId "
	    + "FROM conceptrecogn.acrabbrev where id = ?";
    public static final String getAllSql = "SELECT id, token, expanded, lexWordId "
	    + "FROM conceptrecogn.acrabbrev ORDER BY token";

    private LexWordDao lexWordDao = new LexWordDao();

    /**
     * Retrieves acronym/abbreviation from database
     * <p>
     * Combination of token and expanded form is a unique key in the database.
     * </p>
     * <p>
     * Returns null if token-expanded form combination not found.
     * </p>
     * 
     * @param token
     * @param expandedForm
     * @param connPool
     * @return
     * @throws Exception
     */
    public List<AcrAbbrev> marshal(String token, JdbcConnectionPool connPool)
	    throws SQLException {

	List<AcrAbbrev> acrAbbrevs = new ArrayList<AcrAbbrev>();

	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement getAcrAbbrevStmt = conn
		    .prepareStatement(marshalByTokenSql)) {
		getAcrAbbrevStmt.setString(1, token);
		try (ResultSet rs = getAcrAbbrevStmt.executeQuery()) {
		    while (rs.next()) {
			AcrAbbrev acrAbbrev = instantiateFromResultSet(rs);
			acrAbbrevs.add(acrAbbrev);
		    }
		}
	    }
	}

	return acrAbbrevs;
    }
    
    public AcrAbbrev marshalById(int id, JdbcConnectionPool connPool) throws SQLException {
	
	AcrAbbrev acrAbbrev = null;
	try(Connection conn = connPool.getConnection()){
	    try(PreparedStatement marshalByIdStmt = conn.prepareStatement(marshalByIdSql)){
		marshalByIdStmt.setInt(1, id);
		try(ResultSet rs = marshalByIdStmt.executeQuery()){
		    if(rs.next()) {
			acrAbbrev = instantiateFromResultSet(rs);
		    }
		}
	    }
	}
	
	return acrAbbrev;
	
    }

    /**
     * Retrieves all acronyms/abbreviations in database.
     * 
     * @param connPool
     * @return
     * @throws Exception
     */
    public List<AcrAbbrev> marshalAll(JdbcConnectionPool connPool)
	    throws SQLException {
	List<AcrAbbrev> allAcrAbbrev = new ArrayList<AcrAbbrev>();
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement getTokensStmt = conn
		    .prepareStatement(getAllSql)) {
		try (ResultSet rs = getTokensStmt.executeQuery()) {
		    while (rs.next()) {
			AcrAbbrev acrAbbrev = instantiateFromResultSet(rs);
			allAcrAbbrev.add(acrAbbrev);
		    }
		}
	    }
	}
	return allAcrAbbrev;
    }
    
    private AcrAbbrev instantiateFromResultSet(ResultSet rs) throws SQLException {
	AcrAbbrev acrAbbrev = new AcrAbbrev();
	acrAbbrev.setId(rs.getInt(1));
	acrAbbrev.setToken(rs.getString(2));
	acrAbbrev.setExpanded(rs.getString(3));
	acrAbbrev.setLexWordId(rs.getInt(4));
	return acrAbbrev;
	
    }

    /**
     * Checks to see if this object currently exists in the database, and then
     * performs either saving or synchronization action depending upon whether
     * or not the object exists in the database.
     * <p>
     * If the object already exists in the database, it retrieves the 'id' for
     * the record in the database using the 'token' attribute from this object.
     * It then populates this 'id' attribute in the object, and then returns.
     * </p>
     * <p>
     * If the object does not yet exist in the database, it persists the object
     * in the database, retrieves the automatically generated 'id' attribute
     * from the database, and then populates the 'id' attribute in the object.
     * </p>
     * <p>
     * In either case, this method ensures that the 'id' attribute in the object
     * matches the 'id' value in the database record.
     * </p>
     * 
     * @param connPool
     * @throws Exception
     */
    public void persist(AcrAbbrev acrAbbrev, JdbcConnectionPool connPool)
	    throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement persistStmt = conn
		    .prepareStatement(persistAcrAbbrSql, autoPkField)) {
		persistStmt.setString(1, acrAbbrev.getToken());
		persistStmt.setString(2, acrAbbrev.getExpanded());
		persistStmt.setInt(3, acrAbbrev.getLexWordId());
		if (persistStmt.executeUpdate() == 1) {
		    try (ResultSet rs = persistStmt.getGeneratedKeys()) {
			if (rs.next()) {
			    acrAbbrev.setId(rs.getInt(1));
			}
		    }
		}
	    }
	}
    }

    /**
     * Persist acronym/abbreviation from MincoMan output.
     * 
     * @param mincoAcrAbbrev
     * @param connPool
     * @return
     * @throws Exception
     */
    public AcrAbbrev persist(MincoManAcrAbbrev mincoAcrAbbrev,
	    JdbcConnectionPool connPool) throws SQLException {

	AcrAbbrev newAcrAbbrev = null;
	
	// see if this acr/abbrev already exists
	List<AcrAbbrev> acrAbbrevs = marshal(mincoAcrAbbrev.text, connPool);
	if (acrAbbrevs.size() == 0) {
	    return newAcrAbbrev;
	}
	
	// see if any have the same expanded form
	for(AcrAbbrev existingAcrAbbrev : acrAbbrevs) {
	    if(mincoAcrAbbrev.expandedForm.toLowerCase().equals(existingAcrAbbrev.getExpanded())) {
		return existingAcrAbbrev;
	    }
	}
	
	// create new acr/abbrev,
	newAcrAbbrev = new AcrAbbrev();
	newAcrAbbrev.setToken(mincoAcrAbbrev.text);
	newAcrAbbrev.setExpanded(mincoAcrAbbrev.expandedForm);
	LexWord lexWord = lexWordDao.marshalByToken(mincoAcrAbbrev.text, connPool);
	newAcrAbbrev.setLexWordId(lexWord.getId());
	persist(newAcrAbbrev, connPool);
	return newAcrAbbrev;
    }

    /**
     * Delete all acronym/abbreviation records
     * 
     * @param connPool
     * @return
     * @throws Exception
     */
    public static void delAllAcrAbbrev(JdbcConnectionPool connPool)
	    throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement delAllAcrAbbrevStmt = conn
		    .prepareStatement(delAllAccrAbbrevSql)) {
		delAllAcrAbbrevStmt.executeUpdate();
	    }
	}
    }

}

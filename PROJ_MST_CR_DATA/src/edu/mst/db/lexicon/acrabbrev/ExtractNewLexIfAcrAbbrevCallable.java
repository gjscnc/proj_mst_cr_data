/**
 * 
 */
package edu.mst.db.lexicon.acrabbrev;

import java.util.Set;
import java.util.concurrent.Callable;

import edu.mst.db.util.JdbcConnectionPool;

/**
 * Analyze lexicon word, and if an acronym/abbreviation, extract the expanded
 * form and identify new lexicon tokens.
 * 
 * @author gjs
 *
 */
public class ExtractNewLexIfAcrAbbrevCallable
	implements Callable<Set<String>> {

    private int tokenSourceId;
    private JdbcConnectionPool connPool;

    public ExtractNewLexIfAcrAbbrevCallable(int tokenSourceId,
	    JdbcConnectionPool connPool) {
	this.tokenSourceId = tokenSourceId;
	this.connPool = connPool;
    }

    @Override
    public Set<String> call() throws Exception {
	try (AcrAbbrevUtils acrAbbrevUtils = new AcrAbbrevUtils(connPool)) {
	    Set<String> newTokens = acrAbbrevUtils
		    .parseNewAcrAbbrevTokensFromLexWord(tokenSourceId);
	    return newTokens;
	}
    }

}

/**
 * 
 */
package edu.mst.db.lexicon.acrabbrev;

import java.util.Comparator;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import edu.mst.db.text.util.LexConstants;
import edu.mst.db.text.util.NlpUtil;

/**
 * Class for acronyms and abbreviations associated with words text being
 * analyzed.
 * 
 * @author George
 *
 */
public class AcrAbbrev implements Comparable<AcrAbbrev> {

    private int id;
    private String token;
    private String expanded;
    private int lexWordId = 0;
    private SortedSet<AcrAbbrevWord> words = new TreeSet<AcrAbbrevWord>();

    public static class Comparators {
	public static final Comparator<AcrAbbrev> ByDbId = (AcrAbbrev acr1,
		AcrAbbrev acr2) -> {
	    return Integer.compare(acr1.id, acr2.id);
	};
	public static final Comparator<AcrAbbrev> ByToken = (AcrAbbrev acr1,
		AcrAbbrev acr2) -> {
	    return acr1.token.compareTo(acr2.token);
	};
	public static final Comparator<AcrAbbrev> ByExpansion = (AcrAbbrev acr1,
		AcrAbbrev acr2) -> {
	    return acr1.expanded.compareTo(acr2.expanded);
	};
	public static final Comparator<AcrAbbrev> ByToken_Expansion = (
		AcrAbbrev acr1, AcrAbbrev acr2) -> ByToken
			.thenComparing(ByExpansion).compare(acr1, acr2);
    }

    @Override
    public int compareTo(AcrAbbrev acrAbbrev) {
	return Comparators.ByDbId.compare(this, acrAbbrev);
    }

    @Override
    public int hashCode() {
	return Integer.hashCode(id);
    }

    @Override
    public boolean equals(Object obj) {
	if (obj instanceof AcrAbbrev) {
	    AcrAbbrev acrAbbrev = (AcrAbbrev) obj;
	    return id == acrAbbrev.id;
	}
	return false;
    }

    /**
     * Returns line suitable for writing to flat file with delimiters.
     * <p>
     * Returns fields separated by
     * {@link edu.mst.db.text.util.LexConstants#fieldDelim}
     * <p>
     * Fields are in following order:
     * <ul>
     * <li>id</li>
     * <li>token</li>
     * <li>expanded</li>
     * <li>lexWordId</li>
     * </ul>
     * 
     * @return
     */
    public String toStringWithDelim() {
	StringBuilder strBuilder = new StringBuilder();
	strBuilder.append(id);
	strBuilder.append(LexConstants.fieldDelim);
	strBuilder.append(token);
	strBuilder.append(LexConstants.fieldDelim);
	strBuilder.append(expanded);
	strBuilder.append(LexConstants.fieldDelim);
	strBuilder.append(lexWordId);
	return strBuilder.toString();
    }

    /**
     * Returns object instantiated from line retrieved from flat file.
     * <p>
     * Fields are separated by
     * {@link edu.mst.db.text.util.LexConstants#fieldDelim}
     * <p>
     * Fields are in following order:
     * <ul>
     * <li>id</li>
     * <li>token</li>
     * <li>expanded</li>
     * <li>lexWordId</li>
     * </ul>
     * 
     * @param line
     * @param nlpUtil
     * @return
     */
    public static AcrAbbrev fromStringWithDelim(String line, NlpUtil nlpUtil) {
	AcrAbbrev acrAbbrev = null;
	List<String> tokens = nlpUtil.split(line, LexConstants.fieldDelim);
	if (tokens == null || tokens.size() == 0) {
	    return acrAbbrev;
	}
	acrAbbrev = new AcrAbbrev();
	for (int i = 0; i < 5; i++) {
	    switch (i) {
	    case 0:
		acrAbbrev.setId(Integer.valueOf(tokens.get(i)));
		break;
	    case 1:
		acrAbbrev.setToken(tokens.get(i));
		break;
	    case 2:
		acrAbbrev.setExpanded(tokens.get(i));
		break;
	    case 3:
		acrAbbrev.setLexWordId(Integer.valueOf(tokens.get(i)));
		break;
	    }
	}
	return acrAbbrev;
    }

    public int getId() {
	return id;
    }

    public void setId(int id) {
	this.id = id;
    }

    public String getToken() {
	return token;
    }

    public void setToken(String token) {
	this.token = token;
    }

    public String getExpanded() {
	return expanded;
    }

    public void setExpanded(String expandedForm) {
	this.expanded = expandedForm.toLowerCase();
    }

    public int getLexWordId() {
	return lexWordId;
    }

    public void setLexWordId(int lexWordId) {
	this.lexWordId = lexWordId;
    }

    public SortedSet<AcrAbbrevWord> getWords() {
        return words;
    }

}

/**
 * 
 */
package edu.mst.db.lexicon.acrabbrev;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import edu.mst.db.util.JdbcConnectionPool;

/**
 * @author gjs
 *
 */
public class AcrAbbrevWordDao {

    public static final String allFields = "acrAbbrevId, wordNbr, lexWordId";
    public static final String persistSql = "INSERT INTO conceptrecogn.acrabbrevwords ("
	    + allFields + ") VALUES(?,?,?)";
    public static final String marshalSql = "SELECT " + allFields
	    + " FROM conceptrecogn.acrabbrevwords where acrAbbrevId=? and wordNbr=?";
    public static final String marshalWordsForAcrAbbrevSql = "SELECT "
	    + allFields
	    + " FROM conceptrecogn.acrabbrevwords where acrAbbrevId=? order by wordNbr";
    public static final String delExistingSql = "truncate conceptrecogn.acrabbrevwords";

    public void persist(AcrAbbrevWord word, JdbcConnectionPool connPool)
	    throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement persistStmt = conn
		    .prepareStatement(persistSql)) {
		persist(word, persistStmt);
	    }
	}
    }

    public void persist(Collection<AcrAbbrevWord> words,
	    JdbcConnectionPool connPool) throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement persistStmt = conn
		    .prepareStatement(persistSql)) {
		for (AcrAbbrevWord word : words) {
		    persist(word, persistStmt);
		}
	    }
	}
    }

    private void persist(AcrAbbrevWord word, PreparedStatement persistStmt)
	    throws SQLException {
	persistStmt.setInt(1, word.getAcrAbbrevId());
	persistStmt.setInt(2, word.getWordNbr());
	persistStmt.setInt(3, word.getLexWordId());
	persistStmt.executeUpdate();
    }

    public AcrAbbrevWord marshal(int acrAbbrevId, int wordNbr,
	    JdbcConnectionPool connPool) throws SQLException {
	AcrAbbrevWord word = null;
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement marshalStmt = conn
		    .prepareStatement(marshalSql)) {
		marshalStmt.setInt(1, acrAbbrevId);
		marshalStmt.setInt(2, wordNbr);
		try (ResultSet rs = marshalStmt.executeQuery()) {
		    if (rs.next()) {
			word = instantiateFromResultSet(rs);
		    }
		}
	    }
	}
	return word;
    }

    public List<AcrAbbrevWord> marshalWordsForAcrAbbrev(int acrAbbrevId,
	    JdbcConnectionPool connPool) throws SQLException {
	List<AcrAbbrevWord> words = new ArrayList<AcrAbbrevWord>();
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement marshalStmt = conn
		    .prepareStatement(marshalWordsForAcrAbbrevSql)) {
		marshalStmt.setInt(1, acrAbbrevId);
		try (ResultSet rs = marshalStmt.executeQuery()) {
		    while (rs.next()) {
			AcrAbbrevWord word = instantiateFromResultSet(rs);
			words.add(word);
		    }
		}
	    }
	}
	return words;
    }

    private AcrAbbrevWord instantiateFromResultSet(ResultSet rs)
	    throws SQLException {
	int acrAbbrevId = rs.getInt(1);
	int wordNbr = rs.getInt(2);
	int lexWordId = rs.getInt(3);
	AcrAbbrevWord word = new AcrAbbrevWord(acrAbbrevId, wordNbr, lexWordId);
	return word;
    }

    public static void deleteExisting(JdbcConnectionPool connPool)
	    throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement delExistingStmt = conn
		    .prepareStatement(delExistingSql)) {
		delExistingStmt.executeUpdate();
	    }
	}
    }

}

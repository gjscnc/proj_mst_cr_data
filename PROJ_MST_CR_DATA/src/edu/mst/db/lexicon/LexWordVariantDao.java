package edu.mst.db.lexicon;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import edu.mst.db.util.JdbcConnectionPool;

public class LexWordVariantDao {

    public static final String allFields = "lexWordId, lexWordVariantId, lexWordToken, lexVariantToken";
    public static final String persistAllFldsSql = "INSERT INTO conceptrecogn.lexwordvariants "
	    + "(" + allFields + ") VALUES(?,?,?,?) ";
    public static final String persistIdsSql = "INSERT INTO conceptrecogn.lexwordvariants (lexWordId, lexWordVariantId) VALUES(?,?)";
    public static final String getVariantIdsForLexWordSql = "select lexWordVariantId from conceptrecogn.lexwordvariants where lexWordId = ?";
    public static final String getLexWordIdsForVariantSql = "select lexWordId from conceptrecogn.lexwordvariants where lexWordVariantId = ?";
    public static final String truncateTblSql = "truncate table conceptrecogn.lexwordvariants";
    public static final String varExistsSql = "select lexWordId, lexWordVariantId from conceptrecogn.lexwordvariants "
    	+ "where lexWordToken=? and lexVariantToken=?";

    public static void deleteExisting(JdbcConnectionPool connPool)
	    throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement deleteStmt = conn
		    .prepareStatement(truncateTblSql)) {
		deleteStmt.executeUpdate();
	    }
	}
    }

    public void persist(int lexWordId, int lexWordVariantId,
	    JdbcConnectionPool connPool) throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement persistStmt = conn
		    .prepareStatement(persistIdsSql)) {
		persistStmt.setInt(1, lexWordId);
		persistStmt.setInt(2, lexWordVariantId);
	    }
	}
    }

    public void persist(Collection<LexWordVariant> variants,
	    JdbcConnectionPool connPool) throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement persistAllStmt = conn
		    .prepareStatement(persistAllFldsSql)) {
		for (LexWordVariant variant : variants) {
		    /*
		     * lexWordId, lexWordVariantId, lexWordToken,
		     * lexVariantToken
		     */
		    persistAllStmt.setInt(1, variant.getLexWordId());
		    persistAllStmt.setInt(2, variant.getLexVariantId());
		    persistAllStmt.setString(3, variant.getLexWordToken());
		    persistAllStmt.setString(4, variant.getVariantToken());
		    persistAllStmt.addBatch();
		}
		persistAllStmt.executeBatch();
	    }
	}
    }

    public Set<Integer> marshalVariantIdsForLexWord(int lexWordId,
	    JdbcConnectionPool connPool) throws SQLException {
	Set<Integer> variantIds = new HashSet<Integer>();
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement getStmt = conn
		    .prepareStatement(getVariantIdsForLexWordSql)) {
		getStmt.setInt(1, lexWordId);
		try (ResultSet rs = getStmt.executeQuery()) {
		    while (rs.next()) {
			variantIds.add(rs.getInt(1));
		    }
		}
	    }
	}
	return variantIds;
    }
    
    public int[] marshalVariantIdsForLexWordAsArray(int lexWordId,
	    JdbcConnectionPool connPool) throws SQLException {
	Set<Integer> variantIdsSet = marshalVariantIdsForLexWord(lexWordId, connPool);
	int[] variantIds = new int[variantIdsSet.size()];
	int pos = 0;
	for(int variantId : variantIdsSet) {
	    variantIds[pos] = variantId;
	    pos++;
	}
	Arrays.sort(variantIds);
	return variantIds;
    }

    public Map<Integer, Set<Integer>> marshalVariantsByLexWordId(
	    Set<Integer> lexWordIds, JdbcConnectionPool connPool)
	    throws SQLException {
	Map<Integer, Set<Integer>> variantsByLexWord = new HashMap<Integer, Set<Integer>>();
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement getStmt = conn
		    .prepareStatement(getVariantIdsForLexWordSql)) {
		for(int lexWordId : lexWordIds) {
		    getStmt.setInt(1, lexWordId);
			Set<Integer> variants = new HashSet<Integer>();
		    try(ResultSet rs = getStmt.executeQuery()){
			while(rs.next()) {
			    variants.add(rs.getInt(1));
			}
		    }
		    variantsByLexWord.put(lexWordId, variants);
		}
	    }
	}
	return variantsByLexWord;
    }

    public Set<Integer> marshalLexWordIdsForVariant(int lexWordVariantId,
	    JdbcConnectionPool connPool) throws SQLException {
	Set<Integer> variantIds = new HashSet<Integer>();
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement getStmt = conn
		    .prepareStatement(getLexWordIdsForVariantSql)) {
		getStmt.setInt(1, lexWordVariantId);
		try (ResultSet rs = getStmt.executeQuery()) {
		    while (rs.next()) {
			variantIds.add(rs.getInt(1));
		    }
		}
	    }
	}
	return variantIds;
    }
    
    public boolean isLexVariantExistsInDb(String lexWordToken, String lexVariantToken, JdbcConnectionPool connPool) throws SQLException {
	boolean variantExists = false;
	try(Connection conn = connPool.getConnection()){
	    try(PreparedStatement existsStmt = conn.prepareStatement(varExistsSql)){
		existsStmt.setString(1, lexWordToken);
		existsStmt.setString(2, lexVariantToken);
		try(ResultSet rs = existsStmt.executeQuery()){
		    variantExists = rs.next();
		}
	    }
	}
	return variantExists;
    }

}

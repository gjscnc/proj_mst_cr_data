package edu.mst.db.lexicon;

import java.io.BufferedReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import edu.mst.db.text.util.NlpUtil;
import edu.mst.db.util.JdbcConnectionPool;

public class StopWords {

    public static final String fileDir = "/home/gjs/Documents/workspaceSPS_MySQL/PROJ_SPS_DATA/"
	    + "src/edu/mst/db/lexicon";
    public static final String fileName = "StopWords.txt";

    public static final String countStopWordsSql = "SELECT count(*) FROM conceptrecogn.stopwords";
    public static final String getLexWordIdSql = "SELECT id FROM conceptrecogn.lexwords "
	    + "WHERE token=?";
    public static final String delStopWordsSql = "DELETE FROM conceptrecogn.stopwords";
    public static final String persistStopWordSql = "INSERT INTO conceptrecogn.stopwords "
	    + "(token, lexWordId) VALUES(?,?)";
    public static final String getStopWordsSql = "SELECT token, lexWordId FROM conceptrecogn.stopwords";

    public static final char delimNotStopWord = '[';

    public static final int maxTokenLength = 100;

    private JdbcConnectionPool connPool;
    private SortedSet<String> stopWordTokens = new TreeSet<String>();
    private Set<Integer> lexWordIds = new HashSet<Integer>();
    private ConcurrentMap<String, LexWord> lexWordsByToken = new ConcurrentHashMap<String, LexWord>();

    public StopWords(JdbcConnectionPool connPool) throws Exception {
	this.connPool = connPool;
	retrieveStopWords();
    }

    public StopWords() {

    }

    public static void deleteExisting(JdbcConnectionPool connPool)
	    throws SQLException {
	String deleteSql = "truncate table conceptrecogn.stopwords";
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement deleteStmt = conn
		    .prepareStatement(deleteSql)) {
		deleteStmt.executeUpdate();
	    }
	}
    }

    public void loadStopWordTokens() throws Exception {
	Path file = Paths.get(fileDir, fileName);
	BufferedReader bufferedReader = NlpUtil.getUTF8Reader(file.toFile());
	String inputText = null;
	while ((inputText = bufferedReader.readLine()) != null) {
	    if (inputText.charAt(0) == delimNotStopWord) {
		continue;
	    }
	    stopWordTokens.add(inputText.trim());
	}
    }

    public void retrieveStopWords() throws Exception {

	LexWordDao lexWordDao = new LexWordDao();
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement countStmt = conn
		    .prepareStatement(countStopWordsSql);
		    PreparedStatement getStopWordsStmt = conn
			    .prepareStatement(getStopWordsSql)) {
		try (ResultSet rs = countStmt.executeQuery()) {
		    if (rs.next()) {
			int nbrStopWords = rs.getInt(1);
			if (nbrStopWords == 0) {
			    StopWords.persistStopWords(connPool);
			}
		    }
		}
		try (ResultSet rs = getStopWordsStmt.executeQuery()) {
		    // stopword, baseWordId, variantWordId
		    while (rs.next()) {
			stopWordTokens.add(rs.getString(1));
			lexWordIds.add(rs.getInt(2));
			LexWord lexWord = lexWordDao.marshalByToken(rs.getString(1),
				connPool);
			lexWordsByToken.put(rs.getString(1), lexWord);
		    }
		}
	    }
	}
    }

    /**
     * File containing stop word tokens - all tokens are lower-case in this file
     * 
     * @param connPool
     * @throws Exception
     */
    public static void persistStopWords(JdbcConnectionPool connPool)
	    throws Exception {

	List<String> stopWordsToPersist = new ArrayList<String>();

	Path file = Paths.get(fileDir, fileName);
	BufferedReader bufferedReader = NlpUtil.getUTF8Reader(file.toFile());
	String inputText = null;
	int rowNbr = 0;
	while ((inputText = bufferedReader.readLine()) != null) {
	    rowNbr++;
	    if (inputText.charAt(0) == delimNotStopWord) {
		continue;
	    }
	    String trimmedText = inputText.trim();
	    if (trimmedText.length() > maxTokenLength) {
		String errMsg = String.format(
			"%nLength is %1$,d, greater than max allowable (%2$,d):%n"
				+ "row nbr = %3$,d,%nfile = %4$s,%ntoken = %5$s ",
			trimmedText.length(), maxTokenLength, rowNbr,
			file.toFile().getAbsolutePath(), trimmedText);
		throw new Exception(errMsg);
	    }
	    stopWordsToPersist.add(trimmedText);
	}

	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement delStopWordsStmt = conn
		    .prepareStatement(delStopWordsSql)) {
		delStopWordsStmt.executeUpdate();
	    }
	    try (PreparedStatement getLexWordIdStmt = conn
		    .prepareStatement(getLexWordIdSql);
		    PreparedStatement insertStopWordStmt = conn
			    .prepareStatement(persistStopWordSql)) {
		LexWordDao lexWordDao = new LexWordDao();

		for (String token : stopWordsToPersist) {
		    int lexWordId = 0;
		    getLexWordIdStmt.setString(1, token);
		    try (ResultSet rs = getLexWordIdStmt.executeQuery()) {
			if (rs.next()) {
			    lexWordId = rs.getInt(2);
			}
		    }
		    if (lexWordId == 0) {
			LexWord lexWord = new LexWord();
			lexWord.setToken(token);
			lexWord.setIsAcrAbbrev(false);
			lexWord.setIsPunct(false);
			lexWord.setIsStopWord(true);
			lexWord.setCountForAllNames(0);
			lexWordDao.persist(lexWord, connPool);
			lexWordId = lexWord.getId();
		    }

		    insertStopWordStmt.setString(1, token);
		    insertStopWordStmt.setInt(2, lexWordId);
		    int nbrInserted = insertStopWordStmt.executeUpdate();
		    if (nbrInserted != 1) {
			throw new Exception(String.format(
				"Error inserting stop word %1$s", token));
		    }
		}
	    }
	}
	System.out.printf("Persisted total of %1$,d stop words.%n",
		stopWordsToPersist.size());
    }

    public SortedSet<String> getStopWordTokens() {
	return stopWordTokens;
    }

    public Set<Integer> getLexWordIds() {
	return lexWordIds;
    }

    public ConcurrentMap<String, LexWord> getLexWordsByToken() {
	return lexWordsByToken;
    }

}

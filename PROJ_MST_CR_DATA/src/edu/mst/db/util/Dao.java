/**
 * 
 */
package edu.mst.db.util;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Iterator;

import org.apache.commons.beanutils.DynaBean;

/**
 * @author George
 *
 */
public abstract class Dao {
    
    protected Iterator<DynaBean> rowIter = null;
    protected Connection conn;

    public synchronized void close() throws SQLException{
	rowIter = null;
	conn.close();
	conn = null;
    }
   

}

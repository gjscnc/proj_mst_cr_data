package edu.mst.db.util;

import java.util.concurrent.TimeUnit;

public class Constants {
	public static final int uninitializedIntVal = -1;
	public static final long uninitializedLongVal = -1L;
	public static final char uninitializedCharVal = '0';
	public static final double uninitializedDblVal = -1.0d;
	public static final double indeterminateLogarithm = Double.NaN;
	public static final int defaultThreadCallablesBatchSize = 24;
	public static final char tabChar = '\t';
	public static final String tabStr = "\t";
	public static final char commaChar = ',';
	public static final String commaStr = ",";
	public static final char spaceChar = ' ';
	public static final char semiColonChar = ';';
	public static final String semiColonStr = ";";
	public static final char colonChar = ':';
	public static final String colonStr = ":";
	public static final char periodChar = '.';
	public static final char questionChar = '?';
	public static final String nullText = "null";
	public static final String DIR_DELIM_LINUX = "/";
	public static final char fileExtenDelim = '.';
	public static final String javaPropertyFileExten = "properties";
	public static final char nonBreakingSpaceChar = '\u00A0';
	public static final String lexiconIsEmailToken = "isemail";
	public static final String umlsSnomedId = "SNOMEDCT_US";
	public static final long snomedTypeIdForFullySpecifiedName = 900000000000003001L;
	public static final long snomedTypeIdForSynonymName = 900000000000013009L;

	/**
	 * Use this maximum length for all tokens in NLP processes.
	 * <p>
	 * In some cases, in particular with resumes and ontology concepts, tokens can
	 * be very long. When they are long, invariably any token whose length is
	 * greater than 32 characters is a mistake or carries no obvious cognitive
	 * value. Always truncate these to this maximum length.
	 * </p>
	 */
	public static final int maxLexiconTokenLength = 32;

	/**
	 * Maximum number of individual statements allowed in a Cassandra batch
	 * statement.
	 */
	public static final int maxNbrInBatchStmt = 65535;

	/**
	 * Use default section number = 0 until code for extracting sections is
	 * developed.
	 */
	public static final int defaultSectionNbr = 0;

	/**
	 * Default keep alive time used for thread pooling.
	 */
	public final static long defaultKeepAliveTime = 10L;

	/**
	 * Default keep alive unit of time used for thread pooling.
	 */
	public final static TimeUnit defaultTimeUnit = TimeUnit.SECONDS;

	public final static String wikiSubclassRelId = "P279";
	public final static String wikiInstanceOfRelId = "P31";
	public final static String wikiPartOfRelId = "P361";

	/**
	 * Default window size for entity recognition when using the window-based ER
	 * approach.
	 */
	public final static int defaultErWindowSize = 8;

	/**
	 * Integer value indicating that a word match does not occur between a word in
	 * the concept name and a word in the text.
	 */
	public static final int notMapped = 0;
	/**
	 * Integer value indicating that a word match occurs between a word in the
	 * concept name and a word in the text.
	 */
	public static final int mapped = 1;
	/**
	 * Integer value indicating that a word in the text should not be ignored.
	 */
	public static final int notIgnoreWord = 0;
	/**
	 * Integer value indicating that a word in the text should be ignored, e.g., it
	 * is a stop word, whitespace, etc.
	 */
	public static final int ignoreWord = 1;

	public static final double dblTrue = 1.0d;
	public static final double dblFalse = 0.0d;
	public static final int intTrue = 1;
	public static final int intFalse = 0;

	/**
	 * WikiData items limited to this language type
	 */
	public static final String defaultWikiLanguage = "en";
	/**
	 * Flag indicating the debugging of all sentences in a collection.
	 */
	public static final int debugAllSentences = -1;
	/**
	 * Flag indicating the debugging of all sections in a collection.
	 */
	public static final int debugAllSections = -1;
	/**
	 * Flag indicating that the ending position for text is the end of the text.
	 * That is, use the {@link String#substring(int)} to get all text starting at a
	 * position and beyond.
	 */
	public static final int flagEndOfText = -1;

	/**
	 * Default prime number used for calculating hash code values.
	 */
	public static final int hashCodePrimeNbr = 31;

	/**
	 * Default name of Spark application that performs parallelized concept
	 * recognition
	 */
	public static final String defaultSparkConceptRecognAppName = "ConceptRecognition";

	/**
	 * Default master name for Spark configuration - local installation using six
	 * threads.
	 */
	public static final String defaultSparkLocalMaster = "local[6]";

}

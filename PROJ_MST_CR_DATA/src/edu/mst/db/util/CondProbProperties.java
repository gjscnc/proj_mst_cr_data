/**
 * 
 */
package edu.mst.db.util;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * @author gjs
 *
 */
public class CondProbProperties {

    public static final String configPropsFileName = "condprob.properties";

    public static Properties getProperties() throws IOException {
	Properties condProbProps = new Properties();
	try (FileInputStream in = new FileInputStream(configPropsFileName)) {
	    condProbProps.load(in);
	}
	return condProbProps;
    }

    public static void updateProperties(Properties condProbProps) throws IOException {
	try(FileOutputStream out = new FileOutputStream(configPropsFileName)){
	    condProbProps.store(out, null);
	}
    }

}

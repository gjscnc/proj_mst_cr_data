package edu.mst.db.util;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.Month;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class DateTimeUtil {

	/**
	 * Converts a string date to local date object
	 * 
	 * @param dateStr
	 *            date string in yyyymmdd format
	 * @return
	 */
	public static LocalDate stringToDate(String dateStr) {
		int year = Integer.valueOf(dateStr.substring(0, 4));
		int mth = Integer.valueOf(dateStr.substring(4, 6));
		int day = Integer.valueOf(dateStr.substring(6));
		Month month = Month.of(mth);
		LocalDate effDate = LocalDate.of(year, month, day);
		return effDate;
	}

	public static String milliToHumanReadable(long millis) {

		long days = TimeUnit.MILLISECONDS.toDays(millis);
		millis -= TimeUnit.DAYS.toMillis(days);
		long hrs = TimeUnit.MILLISECONDS.toHours(millis);
		millis -= TimeUnit.HOURS.toMillis(hrs);
		long min = TimeUnit.MILLISECONDS.toMinutes(millis);
		millis -= TimeUnit.MINUTES.toMillis(min);
		long sec = TimeUnit.MILLISECONDS.toSeconds(millis);
		millis -= (double) TimeUnit.SECONDS.toMillis(sec);
		double decSec = (double) sec + ((double) millis / 1000.00d);

		String readableTime = null;
		if (days == 0L) {
			if (hrs == 0L) {
				if (min == 0L) {
					readableTime = String.format("%1$.5f sec", decSec);
					return readableTime;
				}
				readableTime = String.format("%1$d min, %2$.5f sec", min, decSec);
				return readableTime;
			}
			readableTime = String.format("%1$d hr, %2$d min, %3$.5f sec", hrs, min, decSec);
			return readableTime;
		} else {
			readableTime = String.format("%1$s days, %2$s hrs, %3$d min, %4$.5f sec", days, hrs, min, decSec);
		}

		return readableTime;
	}

	public static Timestamp dateToTimestamp(Date date) {
		long milliTime = date.getTime();
		Timestamp timestamp = new Timestamp(milliTime);
		return timestamp;
	}

	public static Date timestampToDate(Timestamp timestamp) {
		Date date = (Date) timestamp; // up cast
		return date;
	}

}

package edu.mst.db.util;

public enum ConnDbName {

    SPS{
	@Override
	public String descr() {
	    return "Original database for R&D; "
		    + "lacks some data for acronyms, synonyms, abbreviations, etc.";
	}
	@Override
	public String dbName() {
	    return "sps";
	}
    },
    CONCEPT_RECOGN{
	@Override
	public String descr() {
	    return "Database for concept recognition, training, and tagging; "
		    + "includes acronyms, synonyms, abbreviations.";
	}
	@Override
	public String dbName() {
	    return "conceptrecogn";
	}
    };

    public abstract String descr();
    public abstract String dbName();

}

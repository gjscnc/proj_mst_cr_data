/**
 * 
 */
package edu.mst.db.util;

import java.sql.SQLException;
import java.sql.Connection;

import org.apache.commons.dbcp2.BasicDataSource;

/**
 * @author George
 *
 */


public class JdbcConnectionPool {
    
    public static String JDBC_URL = "jdbc:mysql://localhost/"; 
    public static final String JDBC_OPTIONS = "?cachePrepStmts=true&useServerPrepStmts=true&"
    	+ "rewriteBatchedStatements=true&useSSL=false&useUnicode=true&characterEncoding=UTF-8&"
    	+ "useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    public static final int MAX_CONNECTIONS_POOL_SIZE = 36;
    public static final int MAX_STATEMENTS_POOL_SIZE = 0;
    public static final int MAX_STATEMENTS_PER_CONNECTION_SIZE = 10;
    
    //private ComboPooledDataSource dataSource = new ComboPooledDataSource();
    private BasicDataSource dataSource = new BasicDataSource();
    
    public JdbcConnectionPool(ConnDbName dbName) throws Exception{
	
	JDBC_URL += dbName.dbName();
	dataSource.setDriverClassName( "com.mysql.cj.jdbc.Driver" ); //loads the jdbc driver            
	dataSource.setUrl( JDBC_URL + JDBC_OPTIONS );
	dataSource.setUsername("root");                                  
	dataSource.setPassword("BlckMtn!977");
	dataSource.setInitialSize(MAX_CONNECTIONS_POOL_SIZE);
	dataSource.setMaxOpenPreparedStatements(MAX_STATEMENTS_POOL_SIZE);
        
    }
    
    public synchronized Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }
    
}

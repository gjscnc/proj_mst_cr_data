/**
 * 
 */
package edu.mst.db.util;

/**
 * @author gjs
 *
 */
public enum ConfigPropCondProbNames {

    lastOntolCondWordProbPredId("Last ontology predicate word ID persisted"), 
    lastOntolCondWordProbFactId("Last ontology assumed fact word ID persisted"), 
    lastOntolCondWordProbVariantPredId("Last ontology variant predicate word ID persisted"), 
    lastOntolCondWordProbVariantFactId("Last ontology variant assumed fact word ID persisted"), 
    lastCorporaCondWordProbPredId("Last corpora predicate word ID persisted"),
    lastCorporaCondWordProbFactId("Last corpora assumed fact word ID persisted"),
    lastCorporaCondWordProbVariantPredId("Last corpora variant predicate word ID persisted"),
    lastCorporaCondWordProbVariantFactId("Last corpora variant assumed fact word ID persisted");

    private String descr;

    private ConfigPropCondProbNames(String descr) {
	this.descr = descr;
    }
    
    public String getDescr() {
	return descr;
    }

}

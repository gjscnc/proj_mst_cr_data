package edu.mst.db.util;

/**
 * Identifies the type of probability calculations for cogency.
 * <ul>
 * <li>CONCEPT_FREQ refers to the number of concept names in what a word
 * appears</li>
 * <li>BASE_WORD_FREQ refers to the number of times a word appears in concepts,
 * i.e., the total count of word occurrences across all concept names</li>
 * </ul>
 * 
 * @author George
 *
 */
public enum CondProbFreqType {

    CONCEPT_FREQ, BASE_WORD_FREQ
}

/**
 * 
 */
package edu.mst.db.util;

import cern.colt.list.DoubleArrayList;
import cern.colt.list.IntArrayList;
import cern.colt.map.OpenIntIntHashMap;
import cern.colt.matrix.DoubleFactory2D;
import cern.colt.matrix.DoubleMatrix2D;

/**
 * Class for sparse matrix containing conditional frequencies for predicate and
 * fact words
 * <p>
 * Frequency matrix is sparse matrix. Holds long values.
 * 
 * @author gjs
 *
 */
public class FreqMatrix {

    /**
     * Rows are predicate word, columns are assumed fact word
     */
    private DoubleMatrix2D freqMatrix;
    private OpenIntIntHashMap predIdByPos = new OpenIntIntHashMap();
    private OpenIntIntHashMap posByPredId = new OpenIntIntHashMap();
    private OpenIntIntHashMap factIdByPos = new OpenIntIntHashMap();
    private OpenIntIntHashMap posByFactId = new OpenIntIntHashMap();

    public FreqMatrix(long freq, int predWordId, int factWordId) {
	freqMatrix = DoubleFactory2D.sparse.make(1, 1);
	freqMatrix.setQuick(0, 0, freq);
	predIdByPos.put(0, predWordId);
	posByPredId.put(predWordId, 0);
	factIdByPos.put(0, factWordId);
	posByFactId.put(factWordId, 0);
    }

    /**
     * Add count to conditional frequency matrix.
     * 
     * @param freq
     * @param predWordId
     * @param factWordId
     */
    public void addFreq(long freq, int predWordId, int factWordId) {
	// add row and/or column?
	boolean isAddPred = !posByPredId.containsKey(predWordId);
	boolean isAddFact = !posByFactId.containsKey(factWordId);
	int predPos = 0;
	int factPos = 0;
	if (isAddPred) {
	    predPos = appendRow() - 1;
	    posByPredId.put(predWordId, predPos);
	    predIdByPos.put(predPos, predWordId);
	} else {
	    predPos = posByPredId.get(predWordId);
	}
	if (isAddFact) {
	    factPos = appendColumn() - 1;
	    posByFactId.put(factWordId, factPos);
	    factIdByPos.put(factPos, factWordId);
	} else {
	    factPos = posByFactId.get(factWordId);
	}
	double newFreq = freqMatrix.getQuick(predPos, factPos) + freq;
	freqMatrix.set(predPos, factPos, newFreq);
    }

    /**
     * Adds row and returns number of rows in matrix
     * 
     * @return
     */
    private int appendRow() {
	int nbrCols = freqMatrix.columns();
	try {
	    freqMatrix = DoubleFactory2D.sparse.appendRows(freqMatrix,
		    DoubleFactory2D.sparse.make(1, nbrCols));
	} catch (Exception ex) {
	    System.out.printf(
		    "%n%nAdding Row: %1$,d nbr of columns, %2$,d nbr of rows%n%n",
		    nbrCols, freqMatrix.rows());
	    throw ex;
	}
	return freqMatrix.rows();
    }

    /**
     * Adds column and returns number of columns in matrix
     * 
     * @return
     */
    private int appendColumn() {
	int nbrRows = freqMatrix.rows();
	try {
	    freqMatrix = DoubleFactory2D.sparse.appendColumns(freqMatrix,
		    DoubleFactory2D.sparse.make(nbrRows, 1));
	} catch (Exception ex) {
	    System.out.printf(
		    "%n%nAdding Column: %1$,d nbr of columns, %2$,d nbr of rows%n%n",
		    freqMatrix.columns(), nbrRows);
	    throw ex;
	}
	return freqMatrix.columns();
    }

    public double getFreq(int predId, int factId) {
	return freqMatrix.getQuick(posByPredId.get(predId),
		posByFactId.get(factId));
    }

    public NonZeroValues getNonZeros() {
	NonZeroValues nonZeros = new NonZeroValues();
	freqMatrix.getNonZeros(nonZeros.predPositions, nonZeros.factPositions,
		nonZeros.freqList);
	return nonZeros;
    }

    public int nbrNonZeros() {
	return freqMatrix.cardinality();
    }

    /**
     * Rows are predicate word, columns are fact word
     * 
     * @return
     */
    public DoubleMatrix2D getFreqMatrix() {
	return freqMatrix;
    }

    public OpenIntIntHashMap getPredIdByPos() {
	return predIdByPos;
    }

    public OpenIntIntHashMap getPosByPredId() {
	return posByPredId;
    }

    public OpenIntIntHashMap getFactIdByPos() {
	return factIdByPos;
    }

    public OpenIntIntHashMap getPosByFactId() {
	return posByFactId;
    }

    public class NonZeroValues {
	public IntArrayList predPositions = new IntArrayList();
	public IntArrayList factPositions = new IntArrayList();
	public DoubleArrayList freqList = new DoubleArrayList();
    }

}

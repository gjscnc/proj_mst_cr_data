package edu.mst.db.condprob;

public enum CondWordProbType {

    ONTOLOGY("conceptrecogn.ontolcondwordprob"), 
    ONTOLOGY_VARIANT("conceptrecogn.ontolcondwordprob_variants"), 
    CORPORA("conceptrecogn.corporacondwordprob"), 
    CORPORA_VARIANT("conceptrecogn.corporacondwordprob_variants");
    
    private String tableName;
    
    private CondWordProbType(String tableName) {
	this.tableName = tableName;
    }
    
    public String tableName() {
	return tableName;
    }
    
}

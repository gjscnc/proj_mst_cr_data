/**
 * 
 */
package edu.mst.db.condprob;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;

import edu.mst.db.util.JdbcConnectionPool;

/**
 * @author gjs
 *
 */
public class CondWordProbDao {

    public static final String allFields = "assumedFactWordId, predicateWordId, freq, condProb, lnCondProb";

    public String getDeleteExistingSql(CondWordProbType type) {
	return String.format("truncate table %1$s", type.tableName());
    }

    public String getPersistSql(CondWordProbType type) {
	return String.format("insert into %1$s (%2$s) values(?,?,?,?,?,?,?)",
		type.tableName(), allFields);
    }

    public String getMarshalAllSql(CondWordProbType type) {
	return String.format("select %1$s from %2$s", allFields,
		type.tableName());
    }

    public String getMarshalMinPredIdMaxNbrSql(CondWordProbType type,
	    int maxNbr) {
	return String.format(
		"select %1$s from %2$s where predicateWordId >= ? limit %3$d",
		allFields, type.tableName(), maxNbr);
    }

    public String getMarshalForWordIdsSql(CondWordProbType type) {
	return String.format(
		"select %1$s from %2$s where origPredWordId=? and origAssumedFactWordId=? "
			+ "and variantPredWordId=? and variantAssumedFactWordId=?",
		allFields, type.tableName());
    }

    /*
     * 1-origPredWordId, 2-origAssumedFactWordId, 3-variantPredWordId,
     * 4-variantAssumedFactWordId, 5-freq, 6-condProb, 7-lnCondProb
     */

    public void deleteExisting(CondWordProbType type,
	    JdbcConnectionPool connPool) throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement deleteStmt = conn
		    .prepareStatement(getDeleteExistingSql(type))) {
		deleteStmt.executeUpdate();
	    }
	}
    }

    public void persist(CondWordProb condProb, CondWordProbType type,
	    JdbcConnectionPool connPool) throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement persistStmt = conn
		    .prepareStatement(getPersistSql(type))) {
		setPersistValues(condProb, persistStmt);
		persistStmt.executeUpdate();
	    }
	}
    }

    public void persistBatch(Collection<CondWordProb> condProbs,
	    CondWordProbType type, JdbcConnectionPool connPool)
	    throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement persistStmt = conn
		    .prepareStatement(getPersistSql(type))) {
		for (CondWordProb condProb : condProbs) {
		    setPersistValues(condProb, persistStmt);
		    persistStmt.addBatch();
		}
		persistStmt.executeBatch();
	    }
	}
    }

    public void setPersistValues(CondWordProb condProb,
	    PreparedStatement persistStmt) throws SQLException {
	/*
	 * 1-origPredWordId, 2-origAssumedFactWordId, 3-variantPredWordId,
	 * 4-variantAssumedFactWordId, 5-freq, 6-condProb, 7-lnCondProb
	 */
	persistStmt.setInt(1, condProb.getOrigPredWordId());
	persistStmt.setInt(2, condProb.getOrigAssumedFactWordId());
	persistStmt.setInt(3, condProb.getVariantPredWordId());
	persistStmt.setInt(4, condProb.getVariantAssumedFactWordId());
	persistStmt.setInt(5, condProb.getFreq());
	persistStmt.setDouble(6, condProb.getCondProb());
	persistStmt.setDouble(7, condProb.getLnCondProb());
    }

    public SortedSet<CondWordProb> marshalAll(CondWordProbType type,
	    JdbcConnectionPool connPool) throws SQLException {
	SortedSet<CondWordProb> condProbs = new TreeSet<CondWordProb>();
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement getAllStmt = conn
		    .prepareStatement(getMarshalAllSql(type))) {
		try (ResultSet rs = getAllStmt.executeQuery()) {
		    while (rs.next()) {
			CondWordProb condProb = instantiateFromResultSet(rs);
			condProbs.add(condProb);
		    }
		}
	    }
	}
	return condProbs;
    }

    public SortedSet<CondWordProb> marshalSubset(CondWordProbType type,
	    int minPredWordId, int maxNbrRetrieved, JdbcConnectionPool connPool)
	    throws SQLException {
	SortedSet<CondWordProb> condProbs = new TreeSet<CondWordProb>();
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement marshalStmt = conn.prepareStatement(
		    getMarshalMinPredIdMaxNbrSql(type, maxNbrRetrieved))) {
		marshalStmt.setInt(1, minPredWordId);
		try (ResultSet rs = marshalStmt.executeQuery()) {
		    while (rs.next()) {
			CondWordProb condProb = instantiateFromResultSet(rs);
			condProbs.add(condProb);
		    }
		}
	    }
	}
	return condProbs;
    }

    public CondWordProb[] marshalAllAsArray(CondWordProbType type,
	    JdbcConnectionPool connPool) throws SQLException {
	SortedSet<CondWordProb> condProbSet = marshalAll(type, connPool);
	CondWordProb[] condProbs = new CondWordProb[condProbSet.size()];
	int pos = 0;
	Iterator<CondWordProb> condProbIter = condProbSet.iterator();
	while (condProbIter.hasNext()) {
	    condProbs[pos] = condProbIter.next();
	    pos++;
	}
	return condProbs;
    }

    public CondWordProb[] marshalSubsetAsArray(CondWordProbType type,
	    int minPredWordId, int maxNbrRetrieved, JdbcConnectionPool connPool)
	    throws SQLException {
	SortedSet<CondWordProb> condProbSet = marshalSubset(type, minPredWordId,
		maxNbrRetrieved, connPool);
	CondWordProb[] condProbs = new CondWordProb[condProbSet.size()];
	int pos = 0;
	Iterator<CondWordProb> condProbIter = condProbSet.iterator();
	while (condProbIter.hasNext()) {
	    condProbs[pos] = condProbIter.next();
	    pos++;
	}
	return condProbs;
    }

    public CondWordProb marshal(int origPredWordId, int origAssumedFactWordId,
	    int variantPredWordId, int variantAssumedFactWordId,
	    CondWordProbType type, JdbcConnectionPool connPool)
	    throws SQLException {
	CondWordProb condProb = null;
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement marshalStmt = conn
		    .prepareStatement(getMarshalForWordIdsSql(type))) {
		marshalStmt.setInt(1, origPredWordId);
		marshalStmt.setInt(2, origAssumedFactWordId);
		marshalStmt.setInt(3, variantPredWordId);
		marshalStmt.setInt(4, variantAssumedFactWordId);
		try (ResultSet rs = marshalStmt.executeQuery()) {
		    if (rs.next()) {
			condProb = instantiateFromResultSet(rs);
		    }
		}
	    }
	}
	return condProb;
    }

    private CondWordProb instantiateFromResultSet(ResultSet rs)
	    throws SQLException {
	/*
	 * 1-origPredWordId, 2-origAssumedFactWordId, 3-variantPredWordId,
	 * 4-variantAssumedFactWordId, 5-freq, 6-condProb, 7-lnCondProb
	 */
	CondWordProb condProb = new CondWordProb();
	condProb.setOrigPredWordId(rs.getInt(1));
	condProb.setOrigAssumedFactWordId(rs.getInt(2));
	condProb.setVariantPredWordId(rs.getInt(3));
	condProb.setVariantAssumedFactWordId(rs.getInt(4));
	condProb.setFreq(rs.getInt(5));
	condProb.setCondProb(rs.getDouble(6));
	condProb.setLnCondProb(rs.getDouble(7));
	return condProb;
    }

}

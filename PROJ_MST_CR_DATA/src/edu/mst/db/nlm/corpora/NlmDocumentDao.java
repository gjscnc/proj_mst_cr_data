package edu.mst.db.nlm.corpora;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import edu.mst.db.util.JdbcConnectionPool;

public class NlmDocumentDao {

    public static final String deleteAllSql = "truncate table conceptrecogn.nlmdocs";
    public static final String persistSql = "INSERT INTO conceptrecogn.nlmdocs "
	    + "(pmid, journal, title, abstract, abstrLen, body, bodyLen, date) "
	    + "VALUES(?,?,?,?,?,?,?,?";
    public static final String marshalSql = "select journal, title, abstract, abstrLen, body, bodyLen, date "
	    + "from conceptrecogn.nlmdocs where pmid = ?";

    public static void deleteAll(JdbcConnectionPool connPool)
	    throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement deleteAllStmt = conn
		    .prepareStatement(deleteAllSql)) {
		deleteAllStmt.executeUpdate();
	    }
	}
    }

    public void persist(NlmDocument doc, JdbcConnectionPool connPool)
	    throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement persistStmt = conn
		    .prepareStatement(persistSql)) {
		persistStmt.setInt(1, doc.getPmid());
		persistStmt.setString(2, doc.getJournal());
		persistStmt.setString(3, doc.getTitle());
		persistStmt.setString(4, doc.getAbstr());
		persistStmt.setInt(5, doc.getAbstrLen());
		persistStmt.setString(6, doc.getBody());
		persistStmt.setInt(7, doc.getBodyLen());
		persistStmt.setDate(8, doc.getDate());
		persistStmt.executeUpdate();
	    }
	}
    }

    public NlmDocument marshal(int pmid, JdbcConnectionPool connPool)
	    throws SQLException {
	NlmDocument doc = null;
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement marshalStmt = conn
		    .prepareStatement(marshalSql)) {
		marshalStmt.setInt(1, pmid);
		try (ResultSet rs = marshalStmt.executeQuery()) {
		    if (rs.next()) {
			doc = new NlmDocument(pmid, rs.getString(1),
				rs.getString(2), rs.getString(3), rs.getInt(4),
				rs.getString(5), rs.getInt(6), rs.getDate(7));
		    }
		}
	    }
	}
	return doc;
    }

}

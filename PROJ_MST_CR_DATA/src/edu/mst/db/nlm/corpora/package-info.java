/**
 * 
 */
/**
 * Classes used to create NLM document objects and their components - sentences,
 * words, etc.
 * <p>
 * Used when parsing NLM abstracts used for training neural network.
 * 
 * @author gjs
 *
 */
package edu.mst.db.nlm.corpora;
package edu.mst.db.nlm.corpora;

import java.sql.Date;

/**
 * This class instantiates abstract from database and formats the text of the
 * abstract to be suitable for parsing. Always use this class then instantiating
 * an abstract.
 * 
 * @author George
 *
 */
public class NlmAbstract implements Comparable<NlmAbstract> {

    private int pmid = 0;
    private String journal = null;
    private String title = null;
    private String abstrText = null;
    private Date date = null;
    private int abstrLen = 0;
    private boolean isWordsCounted = false;
    private boolean isLexParsed = false;
    private int negPmid = 0;

    public NlmAbstract(int pmid, String journal, String title, String abstrText,
	    Date date) throws IllegalArgumentException {
	if (isMissingData(journal, title, abstrText)) {
	    String msg = String.format(
		    "Missing journal, title or text for abstract PMID %1$d ",
		    pmid);
	    throw new IllegalArgumentException(msg);
	}
	this.pmid = pmid;
	this.journal = journal;
	this.title = title;
	this.abstrText = abstrText;
	abstrLen = abstrText.length();
	this.date = date;
    }

    public NlmAbstract(int pmid, String journal, String title, String abstrText,
	    int abstrLen, Date date) throws IllegalArgumentException {
	if (isMissingData(journal, title, abstrText)) {
	    String msg = String.format(
		    "Missing journal, title or text for abstract PMID %1$d ",
		    pmid);
	    throw new IllegalArgumentException(msg);
	}
	this.pmid = pmid;
	this.journal = journal;
	this.title = title;
	this.abstrText = abstrText;
	this.abstrLen = abstrLen;
	this.date = date;
    }

    private boolean isMissingData(String journal, String title,
	    String abstrText) {
	return journal == null || journal.isBlank() || journal.isEmpty()
		|| title == null || title.isBlank() || title.isEmpty()
		|| abstrText == null || abstrText.isBlank()
		|| abstrText.isEmpty();
    }

    @Override
    public int compareTo(NlmAbstract abstr) {
	return Integer.compare(pmid, abstr.pmid);
    }

    @Override
    public boolean equals(Object obj) {
	if (obj instanceof NlmAbstract) {
	    NlmAbstract abstr = (NlmAbstract) obj;
	    return pmid == abstr.pmid;
	}
	return false;
    }

    @Override
    public int hashCode() {
	return Integer.hashCode(pmid);
    }

    public String getTextToIndex() {
	if (abstrText.startsWith(title)) {
	    return abstrText;
	} else {
	    return title.concat("  ").concat(abstrText);
	}
    }

    public int getPmid() {
	return pmid;
    }

    public String getJournal() {
	return journal;
    }

    public String getTitle() {
	return title;
    }

    public String getAbstrText() {
	return abstrText;
    }

    public Date getDate() {
	return date;
    }

    public void setDate(Date date) {
	this.date = date;
    }

    public int getAbstrLen() {
	return abstrLen;
    }

    public boolean isWordsCounted() {
        return isWordsCounted;
    }

    public void setIsWordsCounted(boolean isWordsCounted) {
        this.isWordsCounted = isWordsCounted;
    }

    public boolean isLexParsed() {
        return isLexParsed;
    }

    public void setIsLexParsed(boolean isLexParsed) {
        this.isLexParsed = isLexParsed;
    }

    public int getNegPmid() {
        return negPmid;
    }

    public void setNegPmid(int negPmid) {
        this.negPmid = negPmid;
    }

}

package edu.mst.db.nlm.corpora;

public class NlmAbstractWord implements Comparable<NlmAbstractWord> {

    private int pmid = 0;
    private int sentNbr = 0;
    private int sentWordNbr = 0;
    private String token = null;
    private int lexWordId = 0;
    private boolean isStopWord = false;
    private boolean isPunctOrDelimChar = false;
    private int beginOffset = 0;
    private int endOffset = 0;
    private double cumLnAbstrCogency = 0.0d;
    private double cumLnSentCogency = 0.0d;

    public NlmAbstractWord() {

    }

    /**
     * 
     * @param pmid
     * @param sentNbr
     * @param wordNbr
     * @param token
     * @param lexWordId
     * @param isStopWord
     * @param isPunctOrDelimChar
     * @param beginOffset
     * @param endOffset
     */
    public NlmAbstractWord(int pmid, int sentNbr, int wordNbr, String token,
	    int lexWordId, boolean isStopWord, boolean isPunctOrDelimChar,
	    int beginOffset, int endOffset) {
	this.pmid = pmid;
	this.sentNbr = sentNbr;
	this.sentWordNbr = wordNbr;
	this.token = token;
	this.lexWordId = lexWordId;
	this.isStopWord = isStopWord;
	this.isPunctOrDelimChar = isPunctOrDelimChar;
	this.beginOffset = beginOffset;
	this.endOffset = endOffset;
    }

    /**
     * Constructor without lexicon word ID.
     * 
     * @param pmid
     * @param sentNbr
     * @param wordNbr
     * @param token
     * @param isStopWord
     * @param isPunctOrDelimChar
     * @param beginOffset
     * @param endOffset
     */
    public NlmAbstractWord(int pmid, int sentNbr, int wordNbr, String token,
	    boolean isStopWord, boolean isPunctOrDelimChar, int beginOffset,
	    int endOffset) {
	this.pmid = pmid;
	this.sentNbr = sentNbr;
	this.sentWordNbr = wordNbr;
	this.token = token;
	this.isStopWord = isStopWord;
	this.isPunctOrDelimChar = isPunctOrDelimChar;
	this.beginOffset = beginOffset;
	this.endOffset = endOffset;
    }

    @Override
    public int compareTo(NlmAbstractWord word) {
	int result = Integer.compare(pmid, word.pmid);
	if (result != 0) {
	    return result;
	}
	result = Integer.compare(sentNbr, word.sentNbr);
	if (result != 0) {
	    return result;
	}
	return Integer.compare(sentWordNbr, word.sentWordNbr);
    }

    @Override
    public boolean equals(Object obj) {
	if (obj instanceof NlmAbstractWord) {
	    NlmAbstractWord word = (NlmAbstractWord) obj;
	    return pmid == word.pmid && sentNbr == word.sentNbr
		    && sentWordNbr == word.sentWordNbr;
	}
	return false;
    }

    @Override
    public int hashCode() {
	return Integer.hashCode(pmid) + 7 * Integer.hashCode(sentNbr)
		+ 31 * Integer.hashCode(sentWordNbr);
    }

    public int getPmid() {
	return pmid;
    }

    public void setPmid(int pmid) {
	this.pmid = pmid;
    }

    public int getSentNbr() {
	return sentNbr;
    }

    public void setSentNbr(int sentNbr) {
	this.sentNbr = sentNbr;
    }

    public int getSentWordNbr() {
	return sentWordNbr;
    }

    public void setSentWordNbr(int sentWordNbr) {
	this.sentWordNbr = sentWordNbr;
    }

    public String getToken() {
	return token;
    }

    public void setToken(String token) {
	this.token = token;
    }

    public int getLexWordId() {
	return lexWordId;
    }

    public void setLexWordId(int lexWordId) {
	this.lexWordId = lexWordId;
    }

    public boolean isStopWord() {
	return isStopWord;
    }

    public void setIsStopWord(boolean isStopWord) {
	this.isStopWord = isStopWord;
    }

    public boolean isPunctOrDelimChar() {
	return isPunctOrDelimChar;
    }

    public void setIsPunctOrDelimChar(boolean isPunctOrDelimChar) {
	this.isPunctOrDelimChar = isPunctOrDelimChar;
    }

    public int getBeginOffset() {
	return beginOffset;
    }

    public void setBeginOffset(int beginOffset) {
	this.beginOffset = beginOffset;
    }

    public int getEndOffset() {
	return endOffset;
    }

    public void setEndOffset(int endOffset) {
	this.endOffset = endOffset;
    }

    public double getCumLnSentCogency() {
	return cumLnSentCogency;
    }

    public void setCumLnSentCogency(double cumLnSentCogency) {
	this.cumLnSentCogency = cumLnSentCogency;
    }

    public double getCumLnAbstrCogency() {
	return cumLnAbstrCogency;
    }

    public void setCumLnAbstrCogency(double cumLnAbstrCogency) {
	this.cumLnAbstrCogency = cumLnAbstrCogency;
    }

}

/**
 * 
 */
package edu.mst.db.nlm.corpora.parse.xml;

import java.io.File;
import java.io.FileReader;
import java.sql.Date;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import edu.mst.db.nlm.corpora.NlmAbstract;

/**
 * Parser for NIH .nxml format.
 * <p>
 * Only extracts the PMID, journal, title, and abstract. No full text.
 * <p>
 * Parses XML for NLM articles with format .nxml
 * 
 * @author gjs
 *
 * @deprecated This version not reading text correctly due to problems with
 *             reading HTML and other tags. Replaced by
 *             {@link ParseNlmDocumentJATS}.
 */
@Deprecated
public class ParseNlmAbstractXml {

    public NlmAbstract abstractFromXml(File file) throws Exception {

	int pmid = 0;
	String journal = null;
	String title = null;
	String abstrText = null;
	int year = 0;
	int month = 0;
	int day = 0;

	SortedSet<YMD> publDates = new TreeSet<YMD>();

	try (FileReader reader = new FileReader(file)) {

	    XMLInputFactory factory = XMLInputFactory.newInstance();
	    XMLEventReader eventReader = factory.createXMLEventReader(reader);

	    String pubMedArticleTag = "article";
	    boolean isBeginPubMedArticle = false;
	    String beginFrontTag = "front";
	    boolean isBeginFront = false;
	    boolean isEndFront = false;
	    String journalMetaTag = "journal-meta";
	    boolean isBeginJournalMeta = false;
	    String journalTitleGroupTag = "journal-title-group";
	    boolean isBeginJournalTitleGroup = false;
	    String journalTitleTag = "journal-title";
	    boolean isBeginJournalTitle = false;
	    String articleMetaTag = "article-meta";
	    boolean isBeginArticleMeta = false;
	    String beginArticleIdTag = "article-id";
	    boolean isBeginArticleId = false;
	    String articleIdTypeAttr = "pub-id-type";
	    String articleIdType = "pmid";
	    boolean isPmidArticleId = false;
	    String articleTitleGroupTag = "title-group";
	    boolean isBeginArticleTitleGroup = false;
	    String articleTitleTag = "article-title";
	    boolean isBeginArticleTitle = false;
	    String pubDateTag = "pub-date";
	    boolean isBeginPubDate = false;
	    String pubYearTag = "year";
	    boolean isBeginPubYear = false;
	    String pubMonthTag = "month";
	    boolean isBeginPubMonth = false;
	    String pubDayTag = "day";
	    boolean isBeginPubDay = false;
	    String abstractTag = "abstract";
	    boolean isBeginAbstract = false;

	    while (!isEndFront && eventReader.hasNext()) {
		XMLEvent event = eventReader.nextEvent();
		switch (event.getEventType()) {
		case XMLStreamConstants.START_ELEMENT:
		    StartElement startElement = event.asStartElement();
		    String qName = startElement.getName().getLocalPart();
		    if (qName.equalsIgnoreCase(pubMedArticleTag)) {
			isBeginPubMedArticle = true;
		    } else if (qName.equalsIgnoreCase(beginFrontTag)) {
			isBeginFront = true;
		    } else if (qName.equalsIgnoreCase(journalMetaTag)) {
			isBeginJournalMeta = true;
		    } else if (qName.equalsIgnoreCase(journalTitleGroupTag)) {
			isBeginJournalTitleGroup = true;
		    } else if (qName.equals(journalTitleTag)) {
			isBeginJournalTitle = true;
		    } else if (qName.equalsIgnoreCase(articleMetaTag)) {
			isBeginArticleMeta = true;
		    } else if (qName.equalsIgnoreCase(beginArticleIdTag)) {
			isBeginArticleId = true;
			Iterator<Attribute> attrIter = startElement
				.getAttributes();
			while (attrIter.hasNext()) {
			    Attribute attribute = attrIter.next();
			    if (attribute.getName().toString()
				    .equals(articleIdTypeAttr)
				    && attribute.getValue()
					    .equals(articleIdType)) {
				isPmidArticleId = true;
			    } else {
				isPmidArticleId = false;
			    }
			}
		    } else if (qName.equalsIgnoreCase(articleTitleGroupTag)) {
			isBeginArticleTitleGroup = true;
		    } else if (qName.equalsIgnoreCase(articleTitleTag)) {
			isBeginArticleTitle = true;
		    } else if (qName.equalsIgnoreCase(pubDateTag)) {
			isBeginPubDate = true;
		    } else if (qName.equalsIgnoreCase(pubYearTag)) {
			isBeginPubYear = true;
		    } else if (qName.equalsIgnoreCase(pubMonthTag)) {
			isBeginPubMonth = true;
		    } else if (qName.equalsIgnoreCase(pubDayTag)) {
			isBeginPubDay = true;
		    } else if (qName.equalsIgnoreCase(abstractTag)) {
			isBeginAbstract = true;
		    }
		    break;
		case XMLStreamConstants.CHARACTERS:
		    Characters characters = event.asCharacters();
		    if (isBeginPubMedArticle && isBeginFront) {
			if (isBeginJournalMeta) {
			    if (isBeginJournalTitleGroup) {
				if (isBeginJournalTitle) {
				    journal = characters.getData();
				}
			    }
			}
			if (isBeginArticleMeta) {
			    if (isBeginArticleId) {
				if (isPmidArticleId) {
				    pmid = Integer
					    .valueOf(characters.getData());
				}
			    } else if (isBeginArticleTitleGroup) {
				if (isBeginArticleTitle) {
				    title = characters.getData();
				}
			    } else if (isBeginPubDate) {
				if (isBeginPubDay) {
				    day = Integer.valueOf(characters.getData());
				} else if (isBeginPubMonth) {
				    month = Integer
					    .valueOf(characters.getData());
				} else if (isBeginPubYear) {
				    year = Integer
					    .valueOf(characters.getData());
				}
			    } else if (isBeginAbstract) {
				abstrText = characters.getData();
			    }
			}
		    }
		    break;
		case XMLStreamConstants.END_ELEMENT:
		    EndElement endElement = event.asEndElement();
		    String endName = endElement.getName().getLocalPart();
		    if (endName.equalsIgnoreCase(pubMedArticleTag)) {
			isBeginPubMedArticle = false;
		    } else if (endName.equalsIgnoreCase(beginFrontTag)) {
			isBeginFront = false;
			isEndFront = true;
		    } else if (endName.equalsIgnoreCase(journalMetaTag)) {
			isBeginJournalMeta = false;
		    } else if (endName.equalsIgnoreCase(journalTitleGroupTag)) {
			isBeginJournalTitleGroup = false;
		    } else if (endName.equalsIgnoreCase(journalTitleTag)) {
			isBeginJournalTitle = false;
		    } else if (endName.equalsIgnoreCase(articleMetaTag)) {
			isBeginArticleMeta = false;
		    } else if (endName.equalsIgnoreCase(beginArticleIdTag)) {
			isBeginArticleId = false;
		    } else if (endName.equalsIgnoreCase(articleTitleGroupTag)) {
			isBeginArticleTitleGroup = false;
		    } else if (endName.equalsIgnoreCase(articleTitleTag)) {
			isBeginArticleTitle = false;
		    } else if (endName.equalsIgnoreCase(pubDateTag)) {
			isBeginPubDate = false;
			YMD ymd = new YMD(year, month, day);
			publDates.add(ymd);
			year = 0;
			month = 0;
			day = 0;
		    } else if (endName.equalsIgnoreCase(pubYearTag)) {
			isBeginPubYear = false;
		    } else if (endName.equalsIgnoreCase(pubMonthTag)) {
			isBeginPubMonth = false;
		    } else if (endName.equalsIgnoreCase(pubDayTag)) {
			isBeginPubDay = false;
		    } else if (endName.equalsIgnoreCase(abstractTag)) {
			isBeginAbstract = false;
		    }
		    break;
		}
	    }

	    eventReader.close();

	}

	if (journal == null || journal.isBlank() || journal.isEmpty()) {
	    throw new Exception("Journal not found");
	} else if (title == null || title.isBlank() || title.isEmpty()) {
	    throw new Exception("Title not found");
	} else if (abstrText == null || abstrText.isBlank()
		|| abstrText.isEmpty()) {
	    throw new Exception("Abstract text not found");
	}

	YMD ymdDate = null;
	Iterator<YMD> ymdIter = publDates.iterator();
	while (ymdIter.hasNext()) {
	    YMD ymd = ymdIter.next();
	    if (ymd.isCompleteDate() || ymd.isYearAndMonthOnly()) {
		ymdDate = ymd;
		break;
	    }
	}

	Date date = ymdDate.getDate();

	XmlTextUnescaper unescaper = new XmlTextUnescaper();
	String unescapedTitle = unescaper.unescapeText(title);
	String unescapedAbstrTxt = unescaper.unescapeText(abstrText);
	NlmAbstract abstr = new NlmAbstract(pmid, journal, unescapedTitle,
		unescapedAbstrTxt, date);

	return abstr;
    }

}

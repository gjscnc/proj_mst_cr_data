package edu.mst.db.nlm.corpora.parse.xml;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

import java.sql.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import edu.mst.db.nlm.corpora.NlmDocument;

/**
 * Class for reading NLM PubMed documents using the NLM JATS format.
 * <p>
 * Reads complete file into memory to simplify processing. This may be a
 * bottleneck.
 * <p>
 * TODO test reading JATS file into memory - issue with memory bottleneck?
 * 
 * @deprecated use {@link ParseNlmJATS} instead.
 * @author gjs
 *
 */
@Deprecated
public class ParseNlmDocumentJATS {

    public NlmDocument parseJats(File inputJatsXmlFile)
	    throws ParserConfigurationException, SAXException, IOException,
	    XPathExpressionException {

	NlmDocument doc = null;

	int pmid = 0;
	String journal = null;
	String title = null;
	String abstr = null;
	String body = null;
	Date date = null;

	DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	DocumentBuilder builder = factory.newDocumentBuilder();

	/*
	 * To avoid errors, create entity resolver that ignores DTD
	 */
	EntityResolver resolver = new EntityResolver() {
	    public InputSource resolveEntity(String publicId, String systemId) {
		String empty = "";
		ByteArrayInputStream bais = new ByteArrayInputStream(
			empty.getBytes());
		System.out
			.println("resolveEntity:" + publicId + "|" + systemId);
		return new InputSource(bais);
	    }
	};
	builder.setEntityResolver(resolver);

	Document document = builder.parse(inputJatsXmlFile);
	XPath xPath = XPathFactory.newInstance().newXPath();

	Node pmidNode = (Node) xPath.compile(
		"/article/front/article-meta/article-id[@pub-id-type='pmid']")
		.evaluate(document, XPathConstants.NODE);
	if (pmidNode != null) {
	    pmid = Integer.valueOf(pmidNode.getTextContent());
	}

	Node journalNode = (Node) xPath.compile(
		"/article/front/journal-meta/journal-id[@journal-id-type='nlm-ta']")
		.evaluate(document, XPathConstants.NODE);
	if (journalNode != null) {
	    journal = journalNode.getTextContent();
	}

	NodeList titleNodes = (NodeList) xPath
		.compile(
			"/article/front/article-meta/title-group/article-title")
		.evaluate(document, XPathConstants.NODESET);
	for (int i = 0; i < titleNodes.getLength(); i++) {
	    Node titleNode = titleNodes.item(i);
	    if (titleNode != null && titleNode.getAttributes()
		    .getNamedItem("xml:lang") == null) {
		title = titleNode.getTextContent();
	    }
	}

	Node abstrNode = (Node) xPath
		.compile("/article/front/article-meta/abstract")
		.evaluate(document, XPathConstants.NODE);
	if (abstrNode != null) {
	    NodeList subNodes = abstrNode.getChildNodes();
	    if (subNodes != null && subNodes.getLength() > 0) {
		for (int i = 0; i < subNodes.getLength(); i++) {
		    Node subNode = subNodes.item(i);
		    String subNodeText = subNode.getTextContent();
		    if(subNodeText == null || subNodeText.isBlank() || subNodeText.isEmpty()) {
			continue;
		    }
		    if(abstr == null) {
			abstr = subNodeText;
		    } else {
			abstr += (" " + subNodeText);
		    }
		    if(subNode.getNodeName().equals("title")) {
			abstr += ": ";
		    }
		}
	    }
	}
	
	System.out.printf("Abstract text:%n%1$s%n", abstr);

	return doc;
    }

}

package edu.mst.db.nlm.corpora.parse;

import java.util.HashSet;
import java.util.Set;

import cern.colt.list.IntArrayList;

public class CorporaLexiconResult {
    
    public Set<String> tokens = new HashSet<String>();
    public IntArrayList pmidsProcessed = new IntArrayList();


}

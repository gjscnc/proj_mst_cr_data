package edu.mst.db.nlm.corpora.parse;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.concurrent.Callable;

import cern.colt.list.IntArrayList;
import edu.mst.db.nlm.corpora.NlmAbstractDao;
import edu.mst.db.util.JdbcConnectionPool;

public class CorporaLexiconSetIsParsedBatchCallable
	implements Callable<Integer> {

    private JdbcConnectionPool connPool = null;
    private IntArrayList pmids = null;

    public CorporaLexiconSetIsParsedBatchCallable(IntArrayList pmids,
	    JdbcConnectionPool connPool) {
	this.connPool = connPool;
	this.pmids = pmids;
    }

    @Override
    public Integer call() throws Exception {

	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement setIsLexParsedStmt = conn
		    .prepareStatement(NlmAbstractDao.setIsLexParsedSql)) {
		for (int i = 0; i < pmids.size(); i++) {
		    int pmid = pmids.getQuick(i);
		    setIsLexParsedStmt.setInt(1, pmid);
		    setIsLexParsedStmt.addBatch();
		}
		setIsLexParsedStmt.executeBatch();
	    }
	}

	return pmids.size();
    }

}

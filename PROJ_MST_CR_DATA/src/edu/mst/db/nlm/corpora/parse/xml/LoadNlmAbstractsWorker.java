package edu.mst.db.nlm.corpora.parse.xml;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import edu.mst.db.nlm.corpora.NlmAbstractDao;
import edu.mst.db.util.ConnDbName;
import edu.mst.db.util.JdbcConnectionPool;

public class LoadNlmAbstractsWorker implements AutoCloseable {

    public static final String gzipFileExtension = "gz";
    public static final String nlmFilePredicate = "pubmed";

    protected JdbcConnectionPool connPool = null;
    private static int nbrOfProcessors;
    private static ExecutorService pool;
    private CompletionService<LoadNlmAbstractsResult> svc;

    private int nbrPubMedFiles = 0;
    private int nbrAbstrFilesParsed = 0;
    private int nbrAbstrPersisted = 0;
    private int nbrMissingData = 0;

    private int rptInterval = 10;
    private int dbPersistBatchSize = 200;

    private String rootDir = null;
    private boolean deleteExisting = false;
    private int batchSize = 0;
    private int currentPos = 0;
    private int maxPos = 0;
    private int startPos = 0;

    private String[] fileNames = null;
    private ConcurrentSkipListSet<Integer> persistedPmids = new ConcurrentSkipListSet<Integer>();

    public LoadNlmAbstractsWorker(String rootDir, boolean deleteExisting,
	    int batchSize, JdbcConnectionPool connPool) {
	this.rootDir = rootDir;
	this.deleteExisting = deleteExisting;
	this.batchSize = batchSize;
	this.connPool = connPool;
	nbrOfProcessors = Runtime.getRuntime().availableProcessors();
	pool = Executors.newFixedThreadPool(nbrOfProcessors *= 3);
	svc = new ExecutorCompletionService<LoadNlmAbstractsResult>(pool);
    }

    public void run() throws Exception {

	if (deleteExisting) {
	    System.out.println("Deleting existing abstracts from database");
	    NlmAbstractDao.deleteAll(connPool);
	    System.out.println("Existing abstracted deleted");
	}

	getFileNames();
	System.out.printf("%nTotal of %1$,d PubMed files to parse%n%n",
		nbrPubMedFiles);

	System.out.printf(
		"Extracting and loading abstract XML files located in root directory %1$s %n%n",
		rootDir);
	while (currentPos < nbrPubMedFiles) {

	    maxPos = getNextBatchMaxPos();
	    startPos = currentPos;
	    for (int pos = startPos; pos <= maxPos; pos++) {
		String fileName = fileNames[pos];
		Path path = Paths.get(fileName);
		LoadNlmAbstractsCallable callable = new LoadNlmAbstractsCallable(
			path.toFile(), dbPersistBatchSize, persistedPmids,
			connPool);
		svc.submit(callable);
	    }

	    for (int pos = startPos; pos <= maxPos; pos++) {

		Future<LoadNlmAbstractsResult> future = svc.take();
		LoadNlmAbstractsResult result = future.get();
		nbrAbstrFilesParsed++;
		nbrAbstrPersisted += result.nbrAbtractsPersisted;
		nbrMissingData += result.nbrMissingData;
		if (nbrAbstrFilesParsed % rptInterval == 0) {
		    System.out.printf(
			    "==>> FILES PARSED: Total of %1$,d abstract files parsed, "
				    + "%2$,d abstracts persisted, %3$,d abstracts with missing data %n",
			    nbrAbstrFilesParsed, nbrAbstrPersisted,
			    nbrMissingData);
		}
	    }
	    currentPos = maxPos + 1;
	}

	System.out.printf("%n%nGrand total of %1$,d abstract files parsed, "
		+ "%2$,d abstracts persisted, %3$,d abstracts with missing data %n",
		nbrAbstrFilesParsed, nbrAbstrPersisted, nbrMissingData);
    }

    private int getNextBatchMaxPos() throws Exception {
	// subtract 1 from max pos since positions are inclusive
	int maxPos = currentPos + batchSize - 1;
	if (maxPos >= nbrPubMedFiles - 1)
	    maxPos = nbrPubMedFiles - 1;
	return maxPos;
    }

    private void getFileNames() {
	Path rootPath = Paths.get(rootDir);
	// file names sorted in reverse order
	SortedSet<String> sortedFileNames = new TreeSet<String>(
		java.util.Collections.reverseOrder());
	for (File file : rootPath.toFile().listFiles()) {
	    if (file.getName().startsWith(nlmFilePredicate)
		    && file.getName().endsWith(gzipFileExtension)) {
		sortedFileNames.add(file.getAbsolutePath());
	    }
	}
	nbrPubMedFiles = sortedFileNames.size();
	this.fileNames = new String[sortedFileNames.size()];
	Iterator<String> fileNamesIter = sortedFileNames.iterator();
	int i = 0;
	while (fileNamesIter.hasNext()) {
	    this.fileNames[i] = fileNamesIter.next();
	    i++;
	}
    }

    protected void shutdownAndAwaitTermination(ExecutorService pool) {
	// pool.shutdown(); // Disable new tasks from being submitted
	pool.shutdownNow();
	try {
	    // Wait a while for existing tasks to terminate
	    if (!pool.awaitTermination(5, TimeUnit.SECONDS)) {
		pool.shutdownNow(); // Cancel currently executing tasks
		// Wait a while for tasks to respond to being cancelled
		if (!pool.awaitTermination(15, TimeUnit.SECONDS))
		    System.err.println("Pool did not terminate");
	    }
	} catch (InterruptedException ie) {
	    // (Re-)Cancel if current thread also interrupted
	    pool.shutdownNow();
	    // Preserve interrupt status
	    Thread.currentThread().interrupt();
	}
    }

    @Override
    public void close() throws Exception {
	this.shutdownAndAwaitTermination(pool);
	System.out.println("Parallel processor closed");
    }

    public static void main(String[] args) {

	try {
	    JdbcConnectionPool connPool = new JdbcConnectionPool(
		    ConnDbName.CONCEPT_RECOGN);
	    boolean deleteExisting = true;
	    String rootDir = "/mnt/ssd/nih";
	    int batchSize = 10;
	    try (LoadNlmAbstractsWorker worker = new LoadNlmAbstractsWorker(
		    rootDir, deleteExisting, batchSize, connPool)) {
		worker.run();
	    }
	} catch (Exception ex) {
	    ex.printStackTrace();
	}

    }

}

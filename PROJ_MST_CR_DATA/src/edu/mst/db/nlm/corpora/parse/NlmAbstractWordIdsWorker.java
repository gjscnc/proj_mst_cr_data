/**
 * 
 */
package edu.mst.db.nlm.corpora.parse;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import cern.colt.list.IntArrayList;
import edu.mst.db.lexicon.LexWordDao;
import edu.mst.db.util.ConnDbName;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * Lookup and persist lexicon word ID for each word in NLM abstracts.
 * 
 * @author gjs
 *
 */
public class NlmAbstractWordIdsWorker implements AutoCloseable {

    private JdbcConnectionPool connPool;

    private IntArrayList pmids = new IntArrayList();
    private int nbrPmidsLimit = 0;
    private int nbrPmids = 0;
    private int threadBatchSize = 0;
    private int callableBatchSize = 100;

    private SortedMap<String, Integer> wordIdsByToken = null;

    private int nbrPmidsProcessed = 0;
    private int nbrWordIdsUpdated = 0;
    private int rptInterval = 10000;

    private static int nbrOfProcessors;
    private static ExecutorService pool;
    private CompletionService<Integer> svc;

    public NlmAbstractWordIdsWorker(int nbrPmidsLimit,
	    JdbcConnectionPool connPool) {
	this.nbrPmidsLimit = nbrPmidsLimit;
	this.connPool = connPool;
	nbrOfProcessors = Runtime.getRuntime().availableProcessors();
	int poolSize = nbrOfProcessors *= 3;
	threadBatchSize = poolSize;
	pool = Executors.newFixedThreadPool(poolSize);
	svc = new ExecutorCompletionService<Integer>(pool);
    }

    public void run() throws Exception {

	System.out.println("Marshaling lexicon into memory");
	wordIdsByToken = LexWordDao.marshalAllIdsByToken(connPool);

	System.out.println("Marshaling PMIDs to process");
	try (Connection conn = connPool.getConnection()) {
	    String getPmidsSql = String.format(
		    "select distinct pmid from conceptrecogn.nlmabstractwords "
			    + "where lexWordId = 0 limit %1$s",
		    nbrPmidsLimit);
	    System.out.printf("SQL to query PMIDs = %1$s %n", getPmidsSql);
	    try (PreparedStatement getPmidsStmt = conn
		    .prepareStatement(getPmidsSql)) {
		try (ResultSet rs = getPmidsStmt.executeQuery()) {
		    while (rs.next()) {
			pmids.add(rs.getInt(1));
		    }
		}
	    }
	}
	nbrPmids = pmids.size();
	System.out.printf("Marshaled %1$,d PMIDs to process %n", nbrPmids);

	System.out.println("Processing PMIDs...");

	List<IntArrayList> pmidBatches = new ArrayList<IntArrayList>();
	IntArrayList pmidBatch = new IntArrayList();
	for (int i = 0; i < nbrPmids; i++) {

	    int pmid = pmids.get(i);

	    if (pmidBatch.size() < callableBatchSize) {
		pmidBatch.add(pmid);
	    }
	    if (pmidBatch.size() == callableBatchSize) {
		// need to copy PMID batch for processing in thread
		IntArrayList batchToSubmit = new IntArrayList();
		batchToSubmit.addAllOf(pmidBatch);
		pmidBatches.add(batchToSubmit);
		pmidBatch = new IntArrayList();
	    }
	    if (pmidBatches.size() == threadBatchSize) {
		executeThreads(pmidBatches);
		pmidBatches.clear();
	    }

	}
	// process stragglers
	executeThreads(pmidBatches);
	System.out.printf(
		"Processed grand total of %1$,d PMIDs, updating %2$,d word IDs %n",
		nbrPmidsProcessed, nbrWordIdsUpdated);

    }

    private void executeThreads(List<IntArrayList> pmidBatches)
	    throws InterruptedException, ExecutionException {
	for (int pos = 0; pos < pmidBatches.size(); pos++) {
	    NlmAbstractWordIdsCallable callable = new NlmAbstractWordIdsCallable(
		    pmidBatches.get(pos), wordIdsByToken, connPool);
	    svc.submit(callable);
	}

	for (int pos = 0; pos < pmidBatches.size(); pos++) {
	    Future<Integer> f = svc.take();
	    nbrWordIdsUpdated += f.get();
	    nbrPmidsProcessed++;
	    if (nbrPmidsProcessed % rptInterval == 0) {
		System.out.printf(
			"Processed %1$,d PMIDs, updating %2$,d word IDs %n",
			nbrPmidsProcessed, nbrWordIdsUpdated);
	    }
	}
    }

    protected void shutdownAndAwaitTermination(ExecutorService pool) {
	// pool.shutdown(); // Disable new tasks from being submitted
	pool.shutdownNow();
	try {
	    // Wait a while for existing tasks to terminate
	    if (!pool.awaitTermination(5, TimeUnit.SECONDS)) {
		pool.shutdownNow(); // Cancel currently executing tasks
		// Wait a while for tasks to respond to being cancelled
		if (!pool.awaitTermination(15, TimeUnit.SECONDS))
		    System.err.println("Pool did not terminate");
	    }
	} catch (InterruptedException ie) {
	    // (Re-)Cancel if current thread also interrupted
	    pool.shutdownNow();
	    // Preserve interrupt status
	    Thread.currentThread().interrupt();
	}
    }

    @Override
    public void close() throws Exception {
	this.shutdownAndAwaitTermination(pool);
	System.out.println("Parallel processor closed");
    }

    /**
     * @param args
     */
    public static void main(String[] args) {

	try {

	    JdbcConnectionPool connPool = new JdbcConnectionPool(
		    ConnDbName.CONCEPT_RECOGN);
	    int nbrPmidsLimit = 10000;
	    try (NlmAbstractWordIdsWorker worker = new NlmAbstractWordIdsWorker(
		    nbrPmidsLimit, connPool)) {
		worker.run();
	    }

	} catch (Exception ex) {
	    ex.printStackTrace();
	}

    }

}

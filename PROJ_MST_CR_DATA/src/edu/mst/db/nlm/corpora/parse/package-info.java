/**
 * 
 */
/**
 * Components for conditional probabilities of word co-occurrence in corpora.
 * <p>
 * Corpora consists of abstracts from the NLM library.
 * 
 * @author gjs
 *
 */
package edu.mst.db.nlm.corpora.parse;
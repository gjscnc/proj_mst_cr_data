package edu.mst.db.nlm.corpora.parse.xml;

public class LoadNlmAbstractsResult {
    
    public int nbrAbtractsPersisted = 0;
    public int nbrMissingData = 0;

}

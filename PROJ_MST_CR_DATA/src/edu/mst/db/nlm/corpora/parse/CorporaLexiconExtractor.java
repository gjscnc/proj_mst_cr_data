/**
 * 
 */
package edu.mst.db.nlm.corpora.parse;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import edu.mst.db.lexicon.LexWordDao;
import edu.mst.db.lexicon.StopWords;
import edu.mst.db.text.util.LexVariantsUtil;
import edu.mst.db.text.util.NlpUtil;
import edu.mst.db.util.ConnDbName;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * Query words parsed from NLM abstracts to identify unique tokens, and add
 * these to the lexicon.
 * 
 * @author gjs
 *
 */
public class CorporaLexiconExtractor {

    private JdbcConnectionPool connPool;
    private Set<String> existingLexTokens = new HashSet<String>();
    private SortedSet<String> newLexTokens = new TreeSet<String>();
    private StopWords stopWords = null;
    private NlpUtil nlpUtil = null;
    private LexVariantsUtil variantsUtil = null;

    private int nbrNewTokens = 0;
    private int rptInterval = 10000;
    private int nbrInBatch = 0;
    private int batchSize = 5000;

    public CorporaLexiconExtractor(boolean isMutateVariants,
	    JdbcConnectionPool connPool) throws Exception {
	this.connPool = connPool;
	stopWords = new StopWords(connPool);
	nlpUtil = new NlpUtil();
	variantsUtil = new LexVariantsUtil(isMutateVariants, stopWords);
    }

    public void run() throws SQLException {

	// load existing lexicon tokens
	System.out.println("Marshaling existing lexicon tokens");
	existingLexTokens = LexWordDao.marshalAllTokens(connPool);
	System.out.printf("Marshaled %1$,d existing lexicon tokens. %n",
		existingLexTokens.size());

	// identify and persist new tokens
	try (Connection conn = connPool.getConnection()) {
	    String getUniqueAbstrTokensSql = "select distinct token from nlmabstractwords";
	    try (PreparedStatement getUniqueAbstrTokensStmt = conn
		    .prepareStatement(getUniqueAbstrTokensSql);
		    PreparedStatement persistLexWordStmt = conn
			    .prepareStatement(LexWordDao.persistSql)) {
		try (ResultSet rs = getUniqueAbstrTokensStmt.executeQuery()) {
		    while (rs.next()) {
			String token = rs.getString(1);
			if (existingLexTokens.contains(token)) {
			    continue;
			}
			newLexTokens.add(token);
		    }
		}
		System.out.printf("Extracted %1$,d new lexicon tokens %n",
			newLexTokens.size());
		System.out.println("Now persisting new lexicon tokens.");
		Iterator<String> newTokenIter = newLexTokens.iterator();
		while (newTokenIter.hasNext()) {
		    String token = newTokenIter.next();
		    boolean isStopWord = stopWords.getLexWordsByToken()
			    .containsKey(token);
		    boolean isPunct = nlpUtil.isPunctOrDelimChar(token);
		    boolean isAcrAbbrev = variantsUtil
			    .getAcrAbbrev(token) == null ? false : true;
		    persistLexWordStmt.setString(1, token);
		    persistLexWordStmt.setBoolean(2, isPunct);
		    persistLexWordStmt.setBoolean(3, isAcrAbbrev);
		    persistLexWordStmt.setBoolean(4, isStopWord);
		    persistLexWordStmt.setInt(5, 0);
		    persistLexWordStmt.addBatch();
		    nbrNewTokens++;
		    nbrInBatch++;
		    if (nbrInBatch == batchSize) {
			persistLexWordStmt.executeBatch();
			nbrInBatch = 0;
		    }
		}
		if (nbrNewTokens % rptInterval == 0) {
		    System.out.printf("Persisted %1$,d new tokens.%n",
			    nbrNewTokens);
		}

		// pick up stragglers
		persistLexWordStmt.executeBatch();
	    }
	}
	System.out.printf("Persisted grand total of %1$,d new tokens.%n",
		nbrNewTokens);
    }

    /**
     * @param args
     */
    public static void main(String[] args) {

	try {

	    JdbcConnectionPool connPool = new JdbcConnectionPool(
		    ConnDbName.CONCEPT_RECOGN);
	    boolean isMutateVariants = true;
	    CorporaLexiconExtractor extractor = new CorporaLexiconExtractor(
		    isMutateVariants, connPool);
	    extractor.run();

	} catch (Exception ex) {
	    ex.printStackTrace();
	}

    }

}

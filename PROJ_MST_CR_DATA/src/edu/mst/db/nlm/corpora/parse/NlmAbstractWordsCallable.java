package edu.mst.db.nlm.corpora.parse;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;

import edu.mst.db.lexicon.LexWord;
import edu.mst.db.lexicon.StopWords;
import edu.mst.db.nlm.corpora.NlmAbstract;
import edu.mst.db.nlm.corpora.NlmAbstractDao;
import edu.mst.db.nlm.corpora.NlmAbstractWord;
import edu.mst.db.nlm.corpora.NlmAbstractWordDao;
import edu.mst.db.text.util.StanfordParser;
import edu.mst.db.util.JdbcConnectionPool;

public class NlmAbstractWordsCallable
	implements Callable<NlmAbstractWordsResult> {

    private JdbcConnectionPool connPool = null;
    private int pmid = 0;
    private StopWords stopWords = null;
    private StanfordParser parser = null;
    private NlmAbstractDao abstrDao = new NlmAbstractDao();
    private NlmAbstractWordDao wordDao = new NlmAbstractWordDao();

    public NlmAbstractWordsCallable(int pmid, StopWords stopWords,
	    JdbcConnectionPool connPool) {
	this.pmid = pmid;
	this.stopWords = stopWords;
	this.connPool = connPool;
	parser = new StanfordParser(connPool);
    }

    @Override
    public NlmAbstractWordsResult call() throws Exception {

	NlmAbstract abstr = abstrDao.marshal(pmid, connPool);
	String title = abstr.getTitle().trim();
	if (title.startsWith("[")
		&& (title.endsWith("]") || title.endsWith("]."))) {
	    title = title.substring(1);
	    int lastBracketPos = title.lastIndexOf(']');
	    title = title.substring(0, lastBracketPos);
	    title = title + ".";
	}
	if(pmid == 31840472) {
	    System.out.println("test");
	}
	StringBuffer strBuffer = new StringBuffer();
	if (abstr.getAbstrText().startsWith(abstr.getTitle())) {
	    strBuffer.append(abstr.getAbstrText());
	} else {
	    strBuffer.append(title + " ");
	    strBuffer.append(abstr.getAbstrText());
	}
	List<List<NlmAbstractWord>> words = parser.parseAbstractWords(pmid,
		strBuffer.toString(), LexWord.maxTokenLen, stopWords);
	Iterator<List<NlmAbstractWord>> sentIter = words.iterator();
	Integer nbrWordsPersisted = 0;
	while (sentIter.hasNext()) {
	    List<NlmAbstractWord> sent = sentIter.next();
	    wordDao.persist(sent, connPool);
	    nbrWordsPersisted += sent.size();
	}

	NlmAbstractWordsResult result = new NlmAbstractWordsResult();
	result.pmid = pmid;
	result.nbrWordsPersisted = nbrWordsPersisted;
	return result;
    }

}

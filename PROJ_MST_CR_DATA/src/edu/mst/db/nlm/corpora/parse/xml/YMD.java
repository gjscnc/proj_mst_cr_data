package edu.mst.db.nlm.corpora.parse.xml;

import java.sql.Date;

/**
 * Class for sorting dates from XML.
 * <p>
 * Accounts for missing month and day.
 * 
 * @author gjs
 *
 */
public class YMD implements Comparable<YMD> {

    private int year = 0;
    private int month = 0;
    private int day = 0;
    private Date date = null;

    public YMD() {

    }

    public YMD(int year, int month, int day) {
	this.year = year;
	this.month = month;
	this.day = day;
	setDate();
    }

    public YMD(int year, String month, int day) {
	this.year = year;
	setMonth(month);
	this.day = day;
	setDate();
    }

    public boolean isCompleteDate() {
	return year > 0 && month > 0 && day > 0;
    }

    public boolean isYearAndMonthOnly() {
	return year > 0 && month > 0 && day == 0;
    }

    @Override
    public boolean equals(Object obj) {
	if (obj instanceof YMD) {
	    YMD ymd = (YMD) obj;
	    boolean isEqual = year == ymd.year;
	    if (month > 0 && ymd.month > 0 && day > 0 && ymd.day > 0) {
		isEqual = isEqual && month == ymd.month && day == ymd.day;
	    }
	    return isEqual;
	}
	return false;
    }

    @Override
    public int compareTo(YMD ymd) {
	int result = Integer.compare(year, ymd.year);
	if (month > 0 && ymd.month > 0 && result == 0) {
	    result = Integer.compare(month, ymd.month);
	} else {
	    return result;
	}
	if (day > 0 && ymd.day > 0 && result == 0) {
	    result = Integer.compare(day, ymd.day);
	} else {
	    return result;
	}
	return result;
    }

    @Override
    public int hashCode() {
	int hash = Integer.hashCode(year);
	if (month > 0) {
	    hash += 7 * Integer.hashCode(month);
	}
	if (day > 0) {
	    hash += 31 * Integer.hashCode(day);
	}
	return hash;
    }

    public int getYear() {
	return year;
    }

    public void setYear(int year) {
	this.year = year;
    }

    public int getMonth() {
	return month;
    }

    public void setMonth(int month) {
	this.month = month;
    }

    public void setMonth(String monthStr) {
	if (monthStr == null || monthStr.isBlank() || monthStr.isEmpty()) {
	    date = null;
	    return;
	}
	switch (monthStr) {
	case "Jan":
	case "January":
	    month = 1;
	    break;
	case "Feb":
	case "February":
	    month = 2;
	    break;
	case "Mar":
	case "March":
	    month = 3;
	    break;
	case "Apr":
	case "April":
	    month = 4;
	    break;
	case "May":
	    month = 5;
	    break;
	case "Jun":
	case "June":
	    month = 6;
	    break;
	case "Jul":
	case "July":
	    month = 7;
	    break;
	case "Aug":
	case "August":
	    month = 8;
	    break;
	case "Sep":
	case "September":
	    month = 9;
	    break;
	case "Oct":
	case "October":
	    month = 10;
	    break;
	case "Nov":
	case "November":
	    month = 11;
	    break;
	case "Dec":
	case "December":
	    month = 12;
	    break;
	}
    }

    public int getDay() {
	return day;
    }

    public void setDay(int day) {
	this.day = day;
    }

    public Date setDate() {
	if (isCompleteDate()) {
	    date = Date
		    .valueOf(String.format("%1$d-%2$d-%3$d", year, month, day));
	} else if (isYearAndMonthOnly()) {
	    date = Date.valueOf(String.format("%1$d-%2$d-1", year, month));
	}
	return date;
    }

    public Date getDate() {
	return date;
    }

}

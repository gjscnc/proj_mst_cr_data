Title tags:
    #text
    alternatives
    article-title
    bold
    break
    ext-link
    fn
    inline-formula
    inline-graphic
    italic
    mml:math
    mml:mfrac
    mml:mi
    mml:mn
    mml:mo
    mml:mover
    mml:mrow
    mml:mspace
    mml:msup
    mml:mtext
    named-content
    p
    sc
    styled-content
    sub
    sup
    tex-math
    underline
    xref
 
Abstract tags:
    #text
    abbrev
    abstract
    alt-text
    alternatives
    bold
    boxed-text
    caption
    chem-struct
    disp-formula
    ext-link
    fig
    glyph-data
    graphic
    hr
    inline-formula
    inline-graphic
    italic
    label
    list
    list-item
    media
    mml:annotation
    mml:math
    mml:mfenced
    mml:mfrac
    mml:mi
    mml:mmultiscripts
    mml:mn
    mml:mo
    mml:mover
    mml:mprescripts
    mml:mrow
    mml:mspace
    mml:msqrt
    mml:mstyle
    mml:msub
    mml:msubsup
    mml:msup
    mml:mtext
    mml:none
    mml:semantics
    monospace
    named-content
    p
    private-char
    roman
    sc
    sec
    strike
    styled-content
    sub
    sup
    table
    table-wrap
    table-wrap-foot
    tbody
    td
    tex-math
    thead
    title
    tr
    underline
    uri
    xref
 
Body tags:
    #comment
    #text
    abbrev
    alt-text
    alternatives
    array
    article-title
    attrib
    award-id
    body
    bold
    boxed-text
    break
    caption
    chem-struct
    chem-struct-wrap
    col
    colgroup
    copyright-holder
    def
    def-item
    def-list
    disp-formula
    disp-formula-group
    disp-quote
    element-citation
    email
    etal
    ext-link
    fig
    fn
    fn-group
    fpage
    funding-source
    given-names
    glossary
    glyph-data
    glyph-ref
    graphic
    hr
    inline-formula
    inline-graphic
    inline-supplementary-material
    institution
    institution-wrap
    issue
    italic
    label
    list
    list-item
    lpage
    media
    mixed-citation
    mml:annotation
    mml:maligngroup
    mml:malignmark
    mml:math
    mml:menclose
    mml:merror
    mml:mfenced
    mml:mfrac
    mml:mi
    mml:mlabeledtr
    mml:mmultiscripts
    mml:mn
    mml:mo
    mml:mover
    mml:mpadded
    mml:mphantom
    mml:mprescripts
    mml:mroot
    mml:mrow
    mml:mspace
    mml:msqrt
    mml:mstyle
    mml:msub
    mml:msubsup
    mml:msup
    mml:mtable
    mml:mtd
    mml:mtext
    mml:mtr
    mml:munder
    mml:munderover
    mml:none
    mml:semantics
    monospace
    name
    named-content
    notes
    object-id
    overline
    p
    permissions
    person-group
    preformat
    private-char
    pub-id
    publisher-loc
    publisher-name
    ref
    ref-list
    roman
    sans-serif
    sc
    sec
    source
    speaker
    speech
    statement
    strike
    styled-content
    sub
    suffix
    sup
    supplementary-material
    surname
    table
    table-wrap
    table-wrap-foot
    table-wrap-group
    tbody
    td
    term
    tex-math
    tfoot
    th
    thead
    title
    tr
    underline
    uri
    verse-group
    verse-line
    volume
    x
    xref
    year
 
Top 20 files with parsing errors
    /mnt/ssd/nih/comm_use.A-B.xml/BMC_Dev_Biol/PMC3995766.nxml
    /mnt/ssd/nih/comm_use.A-B.xml/BMC_Dev_Biol/PMC4995620.nxml
    /mnt/ssd/nih/comm_use.A-B.xml/Biomater_Res/PMC4761214.nxml
    /mnt/ssd/nih/comm_use.A-B.xml/Biomater_Res/PMC5010758.nxml
    /mnt/ssd/nih/comm_use.A-B.xml/Biomater_Res/PMC5139079.nxml
    /mnt/ssd/nih/comm_use.A-B.xml/Biomater_Res/PMC5139110.nxml
    /mnt/ssd/nih/comm_use.A-B.xml/Biomater_Res/PMC5382659.nxml
    /mnt/ssd/nih/comm_use.A-B.xml/Biomater_Res/PMC5960204.nxml
    /mnt/ssd/nih/comm_use.A-B.xml/Biomater_Res/PMC6399916.nxml
    /mnt/ssd/nih/comm_use.A-B.xml/Biosensors_(Basel)/PMC4264369.nxml
    /mnt/ssd/nih/comm_use.A-B.xml/Biosensors_(Basel)/PMC4384079.nxml
    /mnt/ssd/nih/comm_use.A-B.xml/Biosensors_(Basel)/PMC4493554.nxml
    /mnt/ssd/nih/comm_use.A-B.xml/Biosensors_(Basel)/PMC4697136.nxml
    /mnt/ssd/nih/comm_use.A-B.xml/Biosensors_(Basel)/PMC4810395.nxml
    /mnt/ssd/nih/comm_use.A-B.xml/Biosensors_(Basel)/PMC5371779.nxml
    /mnt/ssd/nih/comm_use.A-B.xml/Biosensors_(Basel)/PMC5487962.nxml
    /mnt/ssd/nih/comm_use.A-B.xml/Biosensors_(Basel)/PMC5872053.nxml
    /mnt/ssd/nih/comm_use.A-B.xml/Biosensors_(Basel)/PMC6165452.nxml
    /mnt/ssd/nih/comm_use.A-B.xml/Biosensors_(Basel)/PMC6468486.nxml
    /mnt/ssd/nih/comm_use.A-B.xml/Biosensors_(Basel)/PMC7168154.nxml

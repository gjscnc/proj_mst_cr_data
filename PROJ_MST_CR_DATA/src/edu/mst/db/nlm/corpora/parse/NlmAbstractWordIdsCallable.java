package edu.mst.db.nlm.corpora.parse;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.SortedMap;
import java.util.concurrent.Callable;

import cern.colt.list.IntArrayList;
import edu.mst.db.nlm.corpora.NlmAbstractWordDao;
import edu.mst.db.util.JdbcConnectionPool;

public class NlmAbstractWordIdsCallable implements Callable<Integer> {

    private JdbcConnectionPool connPool = null;
    private IntArrayList pmids = null;
    private SortedMap<String, Integer> wordIdsByToken = null;

    public NlmAbstractWordIdsCallable(IntArrayList pmids,
	    SortedMap<String, Integer> wordIdsByToken,
	    JdbcConnectionPool connPool) {
	this.pmids = pmids;
	this.wordIdsByToken = wordIdsByToken;
	this.connPool = connPool;
    }

    @Override
    public Integer call() throws Exception {

	Integer nbrWordIdsUpdated = 0;

	try (Connection conn = connPool.getConnection()) {
	    String getForPmidSql = "select sentNbr, wordNbr, token from conceptrecogn.nlmabstractwords "
		    + "where pmid=? order by sentNbr, wordNbr";
	    try (PreparedStatement getForPmidStmt = conn
		    .prepareStatement(getForPmidSql);
		    PreparedStatement updateStmt = conn.prepareStatement(
			    NlmAbstractWordDao.updateLexWordIdSql)) {
		for (int i = 0; i < pmids.size(); i++) {
		    int pmid = pmids.get(i);
		    getForPmidStmt.setInt(1, pmid);
		    try (ResultSet rs = getForPmidStmt.executeQuery()) {
			while (rs.next()) {
			    int sentNbr = rs.getInt(1);
			    int wordNbr = rs.getInt(2);
			    String token = rs.getString(3);
			    int lexWordId = wordIdsByToken.get(token);
			    updateStmt.setInt(1, lexWordId);
			    updateStmt.setInt(2, pmid);
			    updateStmt.setInt(3, sentNbr);
			    updateStmt.setInt(4, wordNbr);
			    nbrWordIdsUpdated++;
			    updateStmt.addBatch();
			}
		    }
		}
		updateStmt.executeBatch();
	    }
	}

	return nbrWordIdsUpdated;
    }

}

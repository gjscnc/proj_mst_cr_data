package edu.mst.db.nlm.corpora.parse;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;

import cern.colt.list.IntArrayList;
import edu.mst.db.lexicon.LexWord;
import edu.mst.db.lexicon.StopWords;
import edu.mst.db.nlm.corpora.NlmAbstract;
import edu.mst.db.nlm.corpora.NlmAbstractDao;
import edu.mst.db.text.util.NlpUtil;
import edu.mst.db.text.util.StanfordParser;
import edu.mst.db.util.JdbcConnectionPool;

public class CorporaLexiconCallable implements Callable<CorporaLexiconResult>{

    private IntArrayList pmids = null;
    private JdbcConnectionPool connPool = null;
    private StopWords stopWords = null;
    private NlpUtil nlpUtil = null;
    private StanfordParser parser = null;
    private NlmAbstractDao abstrDao = new NlmAbstractDao();

    public CorporaLexiconCallable(IntArrayList pmids, StopWords stopWords,
	    JdbcConnectionPool connPool) {
	this.pmids = pmids;
	this.stopWords = stopWords;
	this.connPool = connPool;
	nlpUtil = new NlpUtil();
	parser = new StanfordParser(connPool);
    }
    
    @Override
    public CorporaLexiconResult call() throws Exception {
	CorporaLexiconResult result = new CorporaLexiconResult();
	
	int nbrOfPmids = pmids.size();
	for(int pmidPos = 0; pmidPos < nbrOfPmids; pmidPos++) {
	    int pmid = pmids.get(pmidPos);
	    NlmAbstract abstr = abstrDao.marshal(pmid, connPool);
	    StringBuffer strBuffer = new StringBuffer();
	    if (abstr.getAbstrText().startsWith(abstr.getTitle())) {
		strBuffer.append(abstr.getAbstrText());
	    } else {
		strBuffer.append(abstr.getTitle() + " ");
		strBuffer.append(abstr.getAbstrText());
	    }
	    List<List<String>> tokenSentences = parser
		    .parseSentencesAndWords(strBuffer.toString(), LexWord.maxTokenLen);
	    Iterator<List<String>> tokenSentIter = tokenSentences.iterator();
	    while (tokenSentIter.hasNext()) {
		List<String> tokenSent = tokenSentIter.next();
		for (String token : tokenSent) {
		    if(token.length() > LexWord.maxTokenLen) {
			token = token.substring(0, LexWord.maxTokenLen - 1);
		    }
		    if (isStopWordOrPunct(token)) {
			continue;
		    }
		    result.tokens.add(token.toLowerCase());
		}
	    }
	}

	result.pmidsProcessed = pmids;
	return result;
    }
    
    private boolean isStopWordOrPunct(String token) {
	return stopWords.getStopWordTokens().contains(token)
		|| nlpUtil.isPunctOrDelimChar(token);
    }

}

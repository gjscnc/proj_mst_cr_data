package edu.mst.db.nlm.corpora.parse;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import cern.colt.list.IntArrayList;
import edu.mst.db.lexicon.LexWord;
import edu.mst.db.lexicon.LexWordDao;
import edu.mst.db.lexicon.StopWords;
import edu.mst.db.nlm.corpora.NlmAbstractDao;
import edu.mst.db.text.util.LexVariantsUtil;
import edu.mst.db.util.ConnDbName;
import edu.mst.db.util.JdbcConnectionPool;

public class CorporaLexiconWorker implements AutoCloseable {

    private boolean isMutateVariants = false;
    private JdbcConnectionPool connPool = null;
    private Set<String> lexicon = new HashSet<String>();

    private int threadPoolBatchSize = 50;
    private int threadBatchSize = 10;
    private int persistBatchSize = 500;
    private int nbrOfAbstrToProcess = 100;
    private int nbrAbstrProcessed = 0;
    private int nbrAbstrRptInterval = 10000;
    private int nbrNewTokens = 0;
    private int nlmLexParsedUpdateBatchSize = 10000;
    private IntArrayList batchPmids = new IntArrayList();
    private IntArrayList pmidsProcessed = new IntArrayList();
    private List<IntArrayList> threadBatchPmids = new ArrayList<IntArrayList>();
    private List<IntArrayList> isLexedBatchPmids = new ArrayList<IntArrayList>();

    private StopWords stopWords = null;
    private LexVariantsUtil variantsUtil = null;

    private Set<String> newTokens = new HashSet<String>();
    private LexWordDao wordDao = new LexWordDao();

    private static int nbrOfProcessors;
    private static ExecutorService pool;
    private CompletionService<CorporaLexiconResult> lexParseSvc;
    private CompletionService<Integer> lexUpdateIsLexedSvc;

    public CorporaLexiconWorker(boolean isMutateVariants, JdbcConnectionPool connPool) {
	this.isMutateVariants = isMutateVariants;
	this.connPool = connPool;
	nbrOfProcessors = Runtime.getRuntime().availableProcessors();
	pool = Executors.newFixedThreadPool(nbrOfProcessors *= 3);
	lexParseSvc = new ExecutorCompletionService<CorporaLexiconResult>(pool);
	lexUpdateIsLexedSvc = new ExecutorCompletionService<Integer>(pool);
    }

    public void run() throws Exception {

	stopWords = new StopWords(connPool);
	variantsUtil = new LexVariantsUtil(isMutateVariants, stopWords);

	// retrieve words in lexicon
	try (Connection conn = connPool.getConnection()) {
	    String getLexSql = "select token from conceptrecogn.lexwords";
	    try (PreparedStatement getLexStmt = conn
		    .prepareStatement(getLexSql)) {
		try (ResultSet rs = getLexStmt.executeQuery()) {
		    while (rs.next()) {
			lexicon.add(rs.getString(1));
		    }
		}
	    }
	}
	System.out.printf("Retrieved %1$,d lexicon tokens from database.%n",
		lexicon.size());

	int minPmid = NlmAbstractDao.minPmid(connPool);
	int abstrCount = 0;
	int batchAccumulatorSize = threadPoolBatchSize * threadBatchSize;

	try (Connection conn = connPool.getConnection()) {
	    String getPmidsSql = String.format(
		    "select pmid from conceptrecogn.nlmabstracts where isLexParsed = 0 "
			    + "order by negPmid limit %1$d ",
		    nbrOfAbstrToProcess);
	    System.out.printf("SQL query for retrieving PMIDs: %1$s %n",
		    getPmidsSql);
	    try (PreparedStatement getPmidsStmt = conn
		    .prepareStatement(getPmidsSql)) {
		try (ResultSet rs = getPmidsStmt.executeQuery()) {

		    boolean isLastPmid = false;
		    while (rs.next() && !isLastPmid) {

			int pmid = rs.getInt(1);
			nbrAbstrProcessed++;
			if (nbrAbstrProcessed % nbrAbstrRptInterval == 0) {
			    System.out.printf(
				    "Processed %1$,d abstracts, %2$,d new tokens identified%n",
				    nbrAbstrProcessed, nbrNewTokens);
			}
			abstrCount++;
			isLastPmid = abstrCount == nbrOfAbstrToProcess
				|| pmid == minPmid;

			if (batchPmids.size() < batchAccumulatorSize) {
			    batchPmids.add(pmid);
			}

			int currentBatchSize = batchPmids.size();
			if (currentBatchSize == batchAccumulatorSize
				|| isLastPmid) {
			    processAbstractBatch();
			}
		    }
		}
	    }
	    // clean up stragglers
	    if (newTokens.size() > 0) {
		persistBatch();
	    }
	}

	System.out.printf(
		"Processed %1$,d abstracts, %2$,d new tokens identified%n",
		nbrAbstrProcessed, nbrNewTokens);

	System.out.println(
		"Setting 'isLexParsed' flag for all processed abstracts");
	// chunk out batches
	IntArrayList isLexedPmidBatch = new IntArrayList();
	for (int i = 1; i < pmidsProcessed.size(); i++) {
	    int pmid = pmidsProcessed.getQuick(i);
	    if (isLexedPmidBatch.size() < nlmLexParsedUpdateBatchSize) {
		isLexedPmidBatch.add(pmid);
	    } else {
		IntArrayList batchPmids = new IntArrayList();
		batchPmids.addAllOf(isLexedPmidBatch);
		isLexedBatchPmids.add(batchPmids);
		isLexedPmidBatch = new IntArrayList();
	    }
	}
	if (isLexedPmidBatch.size() > 0) {
	    isLexedBatchPmids.add(isLexedPmidBatch);
	}
	for (IntArrayList pmidBatch : isLexedBatchPmids) {
	    CorporaLexiconSetIsParsedBatchCallable callable = new CorporaLexiconSetIsParsedBatchCallable(
		    pmidBatch, connPool);
	    lexUpdateIsLexedSvc.submit(callable);
	}
	int nbrIsLexedUpdated = 0;
	for (int i = 0; i < isLexedBatchPmids.size(); i++) {
	    Future<Integer> f = lexUpdateIsLexedSvc.take();
	    nbrIsLexedUpdated += f.get();
	    if (nbrIsLexedUpdated % nbrAbstrRptInterval == 0) {
		System.out.printf(
			"Updated 'isLexed' field for %1,d abstracts. %n",
			nbrIsLexedUpdated);
	    }
	}

	System.out.printf(
		"Updated 'isLexed' field for grand total of %1,d abstracts. %n",
		nbrIsLexedUpdated);

	// NlmAbstractDao.setIsLexParsedBatch(pmidsProcessed,
	// nlmLexParsedUpdateBatchSize, connPool);

	

    }

    private void processAbstractBatch()
	    throws InterruptedException, ExecutionException, SQLException {

	chunkThreadBatchPmids();
	for (int i = 0; i < threadBatchPmids.size(); i++) {
	    IntArrayList threadPmidBatch = threadBatchPmids.get(i);
	    CorporaLexiconCallable callable = new CorporaLexiconCallable(
		    threadPmidBatch, stopWords, connPool);
	    lexParseSvc.submit(callable);
	}

	for (int i = 0; i < threadBatchPmids.size(); i++) {
	    Future<CorporaLexiconResult> future = lexParseSvc.take();
	    CorporaLexiconResult result = future.get();
	    for (String token : result.tokens) {
		if (!lexicon.contains(token)) {
		    newTokens.add(token);
		    lexicon.add(token);
		    nbrNewTokens++;
		}
	    }
	    pmidsProcessed.addAllOf(result.pmidsProcessed);
	}

	if (newTokens.size() >= persistBatchSize) {
	    persistBatch();
	}

	batchPmids = new IntArrayList();

    }

    private void chunkThreadBatchPmids() {
	threadBatchPmids.clear();
	IntArrayList currentThreadBatch = new IntArrayList();
	for (int i = 0; i < batchPmids.size(); i++) {
	    Integer pmid = batchPmids.get(i);
	    currentThreadBatch.add(pmid);
	    if (currentThreadBatch.size() == threadBatchSize
		    || i == batchPmids.size() - 1) {
		IntArrayList pmids = new IntArrayList(
			currentThreadBatch.size());
		pmids.addAllOf(currentThreadBatch);
		threadBatchPmids.add(pmids);
		currentThreadBatch = new IntArrayList();
	    }
	}
    }

    private void persistBatch() throws SQLException {
	List<LexWord> newWords = new ArrayList<LexWord>();
	for (String token : newTokens) {
	    LexWord lexWord = new LexWord();
	    // computations exclude punctuation, stop words, etc., but not
	    // acronyms/abbreviations
	    boolean isAcrAbbrev = variantsUtil.getAcrAbbrev(token) == null
		    ? false
		    : true;
	    lexWord.setToken(token);
	    lexWord.setIsPunct(false);
	    lexWord.setIsStopWord(false);
	    lexWord.setIsAcrAbbrev(isAcrAbbrev);
	    newWords.add(lexWord);
	}
	wordDao.persistBatch(newWords, connPool);
	System.out.printf("Persisted %1$,d new tokens%n", newTokens.size());
	newTokens.clear();
    }

    protected void shutdownAndAwaitTermination(ExecutorService pool) {
	// pool.shutdown(); // Disable new tasks from being submitted
	pool.shutdownNow();
	try {
	    // Wait a while for existing tasks to terminate
	    if (!pool.awaitTermination(5, TimeUnit.SECONDS)) {
		pool.shutdownNow(); // Cancel currently executing tasks
		// Wait a while for tasks to respond to being cancelled
		if (!pool.awaitTermination(15, TimeUnit.SECONDS))
		    System.err.println("Pool did not terminate");
	    }
	} catch (InterruptedException ie) {
	    // (Re-)Cancel if current thread also interrupted
	    pool.shutdownNow();
	    // Preserve interrupt status
	    Thread.currentThread().interrupt();
	}
    }

    @Override
    public void close() throws Exception {
	this.shutdownAndAwaitTermination(pool);
	System.out.println("Parallel processor closed");
    }

    public static void main(String[] args) {

	try {
	    JdbcConnectionPool connPool = new JdbcConnectionPool(
		    ConnDbName.CONCEPT_RECOGN);
	    boolean isMutateVariants = true;
	    try (CorporaLexiconWorker worker = new CorporaLexiconWorker(
		    isMutateVariants, connPool)) {
		worker.run();
	    }
	} catch (Exception ex) {
	    ex.printStackTrace();
	}

    }

}

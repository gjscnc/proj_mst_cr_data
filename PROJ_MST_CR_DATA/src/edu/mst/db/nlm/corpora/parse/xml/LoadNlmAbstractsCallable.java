/**
 * 
 */
package edu.mst.db.nlm.corpora.parse.xml;

import java.io.File;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentSkipListSet;

import edu.mst.db.nlm.corpora.parse.xml.NlmAbstractParser.NlmAbstractParserResult;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * @author gjs
 *
 */
public class LoadNlmAbstractsCallable
	implements Callable<LoadNlmAbstractsResult> {

    private File xmlFile = null;
    private int batchSize = 0;
    private JdbcConnectionPool connPool = null;

    private ConcurrentSkipListSet<Integer> persistedPmids = null;

    public LoadNlmAbstractsCallable(File xmlFile, int batchSize,
	    ConcurrentSkipListSet<Integer> persistedPmids,
	    JdbcConnectionPool connPool) {
	this.xmlFile = xmlFile;
	this.batchSize = batchSize;
	this.persistedPmids = persistedPmids;
	this.connPool = connPool;
    }

    @Override
    public LoadNlmAbstractsResult call() throws Exception {
	NlmAbstractParser abstrParser = new NlmAbstractParser(xmlFile, true,
		batchSize, connPool);
	abstrParser.setPersistedPmids(persistedPmids);
	NlmAbstractParserResult parserResult = abstrParser.parseSAX();
	LoadNlmAbstractsResult result = new LoadNlmAbstractsResult();
	result.nbrAbtractsPersisted = parserResult.nbrAbstrPersisted;
	result.nbrMissingData = parserResult.nbrAbstrParsed
		- parserResult.nbrAbstrPersisted;
	return result;
    }

}

/**
 * 
 */
package edu.mst.db.nlm.corpora.parse.xml;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.sql.Date;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.zip.GZIPInputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import edu.mst.db.nlm.corpora.NlmAbstract;
import edu.mst.db.nlm.corpora.NlmAbstractDao;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * This class contains methods for parsing abstracts from NLM compressed XML
 * files.
 * <p>
 * NLM abstract files downloaded from <a href=
 * "ftp://ftp.ncbi.nlm.nih.gov/pubmed/baseline">ftp://ftp.ncbi.nlm.nih.gov/pubmed/baseline</a>.
 * <p>
 * All files are in GZIP format. Each file contains multiple abstracts.
 * 
 * @author gjs
 *
 */
public class NlmAbstractParser {

    private static final int maxLenJournalTitle = 250;

    private File gzipFile = null;
    private String gzipFileName = null;
    private JdbcConnectionPool connPool = null;
    private SortedSet<NlmAbstract> abstracts = new TreeSet<NlmAbstract>();

    private ConcurrentSkipListSet<Integer> persistedPmids = null;

    private boolean isPersist = false;
    private int batchSize = 0;
    private int nbrInBatch = 0;

    private int rptInterval = 10000;
    private int nbrAbstrParsed = 0;

    // for SAX parser
    private int nbrMissingData = 0;

    private NlmAbstractDao abstrDao = new NlmAbstractDao();

    public NlmAbstractParser(File gzipFile, boolean isPersist, int batchSize,
	    JdbcConnectionPool connPool) {
	this.gzipFile = gzipFile;
	gzipFileName = gzipFile.getName();
	this.isPersist = isPersist;
	this.batchSize = batchSize;
	this.connPool = connPool;
    }

    /**
     * 
     * Uses XPath to parse NLM abstracts from XML file.
     * 
     * @deprecated Loading entire file into memory not practical.
     * 
     * @return
     * @throws ParserConfigurationException
     * @throws FileNotFoundException
     * @throws IOException
     * @throws SAXException
     * @throws XPathExpressionException
     * @throws ParseException
     * @throws SQLException
     */
    @Deprecated
    public NlmAbstractParserResult parseXPath()
	    throws ParserConfigurationException, FileNotFoundException,
	    IOException, SAXException, XPathExpressionException, ParseException,
	    SQLException {

	NlmAbstractParserResult result = new NlmAbstractParserResult();
	result.fileName = gzipFileName;

	DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	DocumentBuilder builder = factory.newDocumentBuilder();

	GZIPInputStream in = new GZIPInputStream(new FileInputStream(gzipFile));

	try (Reader decoder = new InputStreamReader(in)) {

	    InputSource inputSource = new InputSource(decoder);
	    Document document = builder.parse(inputSource);
	    XPath xPath = XPathFactory.newInstance().newXPath();

	    NodeList articleNodes = (NodeList) xPath
		    .compile("/PubmedArticleSet/PubmedArticle/MedlineCitation")
		    .evaluate(document, XPathConstants.NODESET);

	    result.nbrAbstrNodesParsed = articleNodes.getLength();
	    System.out.printf(
		    "File %1$s: Extracted %2$,d article nodes from xml. %n",
		    gzipFileName, result.nbrAbstrNodesParsed);

	    for (int i = 0; i < articleNodes.getLength(); i++) {

		// Medline citation node
		Node medlineCitationNode = articleNodes.item(i);

		// pmid
		Node pmidNode = (Node) xPath.compile("PMID")
			.evaluate(medlineCitationNode, XPathConstants.NODE);
		int pmid = Integer.valueOf(pmidNode.getTextContent());

		// article node
		Node articleNode = (Node) xPath.compile("Article")
			.evaluate(medlineCitationNode, XPathConstants.NODE);

		Node journalNode = (Node) xPath.compile("Journal")
			.evaluate(articleNode, XPathConstants.NODE);

		// journal title
		String journalTitle = null;
		Node journalTitleNode = (Node) xPath.compile("Title")
			.evaluate(journalNode, XPathConstants.NODE);
		if (journalTitleNode != null
			&& journalTitleNode.getTextContent() != null) {
		    journalTitle = journalTitleNode.getTextContent().strip();
		}

		// date
		int year = 0;
		String mthStr = null;
		int day = 0;
		Date date = null;
		Node dateNode = (Node) xPath.compile("JournalIssue/PubDate")
			.evaluate(journalNode, XPathConstants.NODE);
		if (dateNode != null) {
		    Node yearNode = (Node) xPath.compile("Year")
			    .evaluate(dateNode, XPathConstants.NODE);
		    if (yearNode != null) {
			year = Integer.valueOf(yearNode.getTextContent());
		    }
		    Node monthNode = (Node) xPath.compile("Month")
			    .evaluate(dateNode, XPathConstants.NODE);
		    if (monthNode != null) {
			mthStr = monthNode.getTextContent();
		    }
		    Node dayNode = (Node) xPath.compile("Day")
			    .evaluate(dateNode, XPathConstants.NODE);
		    if (dayNode != null) {
			day = Integer.valueOf(dayNode.getTextContent());
		    }
		    YMD ymd = new YMD(year, mthStr, day);
		    date = ymd.getDate();
		}

		// title
		String articleTitle = null;
		Node articleTitleNode = (Node) xPath.compile("ArticleTitle")
			.evaluate(articleNode, XPathConstants.NODE);
		if (articleTitleNode != null
			&& articleTitleNode.getTextContent() != null) {
		    articleTitle = articleTitleNode.getTextContent().strip();
		}

		// abstract text
		String abstrText = null;
		Node abstrNode = (Node) xPath.compile("Abstract")
			.evaluate(articleNode, XPathConstants.NODE);
		if (abstrNode != null) {
		    NodeList abstrTextNodes = (NodeList) xPath
			    .compile("AbstractText")
			    .evaluate(abstrNode, XPathConstants.NODESET);
		    for (int j = 0; j < abstrTextNodes.getLength(); j++) {
			Node abstrTextNode = abstrTextNodes.item(j);
			NamedNodeMap nodeMap = abstrTextNode.getAttributes();
			if (nodeMap.getLength() > 0) {
			    Node node = nodeMap.getNamedItem("Label");
			    if (node != null) {
				String labelValue = node.getNodeValue().strip()
					+ ": ";
				if (abstrText == null) {
				    abstrText = labelValue;
				} else {
				    abstrText = abstrText.concat(labelValue);
				}
			    }
			}
			if (abstrTextNode.getTextContent() != null) {
			    if (abstrText == null) {
				abstrText = abstrTextNode.getTextContent()
					.strip() + " ";
			    } else {
				abstrText = abstrText.concat(
					abstrTextNode.getTextContent().strip()
						+ " ");
			    }
			}
		    }
		}

		boolean isJournalTitleMissing = journalTitle == null
			|| journalTitle.isBlank() || journalTitle.isEmpty();
		if (isJournalTitleMissing) {
		    result.nbrJournalTitleMissing++;
		}
		boolean isArticleTitleMissing = articleTitle == null
			|| articleTitle.isBlank() || articleTitle.isEmpty();
		if (isArticleTitleMissing) {
		    result.nbrArticleTitleMissing++;
		}
		boolean isAbstractTextMissing = abstrText == null
			|| abstrText.isBlank() || abstrText.isEmpty();
		if (isAbstractTextMissing) {
		    result.nbrAbstrTextMissing++;
		}
		if (date == null) {
		    result.nbrDatesMissing++;
		}

		if (!isJournalTitleMissing && !isArticleTitleMissing
			&& !isAbstractTextMissing) {
		    NlmAbstract abstr = instantiateAbstract(pmid, journalTitle,
			    articleTitle, abstrText, date, result);
		    if (abstr != null) {
			abstracts.add(abstr);
			if (isPersist && abstracts.size() == batchSize) {
			    abstrDao.persistBatch(abstracts, persistedPmids,
				    connPool);
			    result.nbrAbstrPersisted += abstracts.size();
			    abstracts.clear();
			}
		    }
		}

		result.nbrAbstrParsed++;
		if (result.nbrAbstrParsed % rptInterval == 0) {
		    System.out.printf(
			    "File %1$s: Parsed %2$,d articles and persisted %3$,d "
				    + "(%4$,d are missing abstract text, "
				    + "%5$,d are missing article title, "
				    + "and %6$,d are missing journal title). "
				    + "%7$,d are missing complete date information %n ",
			    result.fileName, result.nbrAbstrParsed,
			    result.nbrAbstrPersisted,
			    result.nbrAbstrTextMissing,
			    result.nbrArticleTitleMissing,
			    result.nbrJournalTitleMissing,
			    result.nbrDatesMissing);
		}
	    }
	}

	if (isPersist && abstracts.size() > 0) {
	    abstrDao.persistBatch(abstracts, persistedPmids, connPool);
	    result.nbrAbstrPersisted += abstracts.size();
	    abstracts.clear();
	}

	System.out.printf(
		"FINISHED File %1$s: Parsed %2$,d articles and persisted %3$,d "
			+ "(%4$,d are missing abstract text, "
			+ "%5$,d are missing article title, "
			+ "and %6$,d are missing journal title). "
			+ "%7$,d are missing complete date information. %n ",
		result.fileName, result.nbrAbstrParsed,
		result.nbrAbstrPersisted, result.nbrAbstrTextMissing,
		result.nbrArticleTitleMissing, result.nbrJournalTitleMissing,
		result.nbrDatesMissing);

	return result;
    }

    public class NlmAbstractParserResult {
	public String fileName = null;
	public int rptInterval = 500;
	public int nbrAbstrNodesParsed = 0;
	public int nbrAbstrParsed = 0;
	public int nbrAbstrTextMissing = 0;
	public int nbrArticleTitleMissing = 0;
	public int nbrJournalTitleMissing = 0;
	public int nbrDatesMissing = 0;
	public int nbrAbstrPersisted = 0;
    }

    /**
     * Use SAX to parse abstracts
     * 
     * @throws IOException
     * @throws XMLStreamException
     * @throws SQLException
     * 
     */
    public NlmAbstractParserResult parseSAX()
	    throws IOException, XMLStreamException, SQLException {

	NlmAbstractParserResult result = new NlmAbstractParserResult();
	result.fileName = gzipFileName;

	String pubMedArticleTag = "PubmedArticle";
	boolean isBeginPubMedArticle = false;
	String medlineCitationTag = "MedlineCitation";
	boolean isBeginMedlineCitation = false;
	String pmidTag = "PMID";
	boolean isBeginPmid = false;
	String articleTag = "Article";
	boolean isBeginArticle = false;
	String journalTag = "Journal";
	boolean isBeginJournal = false;
	String journalIssueTag = "JournalIssue";
	boolean isBeginJournalIssue = false;
	String pubDateTag = "PubDate";
	boolean isBeginPubDate = false;
	String pubDateYearTag = "Year";
	boolean isBeginPubDateYear = false;
	String pubDateMonthTag = "Month";
	boolean isBeginPubDateMonth = false;
	String pubDateDayTag = "Day";
	boolean isBeginPubDateDay = false;
	String journalTitleTag = "Title";
	boolean isBeginJournalTitle = false;
	String articleTitleTag = "ArticleTitle";
	boolean isBeginArticleTitle = false;
	String abstrTag = "Abstract";
	boolean isBeginAbstr = false;
	String abstrTxtTag = "AbstractText";
	boolean isBeginAbstrText = false;
	String abstrLabelAttr = "Label";
	String correctionsTag = "CommentsCorrections";
	boolean isBeginCorrections = false;

	int pmid = 0;
	String journalTitle = null;
	String articleTitle = null;
	String abstrText = null;
	int year = 0;
	String month = null;
	int day = 0;
	Date date = null;

	GZIPInputStream in = new GZIPInputStream(new FileInputStream(gzipFile));

	try (Reader decoder = new InputStreamReader(in)) {
	    try (BufferedReader br = new BufferedReader(decoder)) {
		XMLInputFactory factory = XMLInputFactory.newInstance();
		XMLEventReader eventReader = factory.createXMLEventReader(br);
		while (eventReader.hasNext()) {
		    XMLEvent event = eventReader.nextEvent();
		    switch (event.getEventType()) {
		    case XMLStreamConstants.START_ELEMENT:
			StartElement startElement = event.asStartElement();
			String qName = startElement.getName().getLocalPart();
			if (qName.equalsIgnoreCase(pubMedArticleTag)) {
			    isBeginPubMedArticle = true;
			} else if (qName.equalsIgnoreCase(medlineCitationTag)) {
			    isBeginMedlineCitation = true;
			} else if (qName.equalsIgnoreCase(pmidTag)) {
			    isBeginPmid = true;
			} else if (qName.equalsIgnoreCase(articleTag)) {
			    isBeginArticle = true;
			} else if (qName.equalsIgnoreCase(journalTag)) {
			    isBeginJournal = true;
			} else if (qName.equalsIgnoreCase(journalIssueTag)) {
			    isBeginJournalIssue = true;
			} else if (qName.equalsIgnoreCase(pubDateTag)) {
			    isBeginPubDate = true;
			} else if (qName.equalsIgnoreCase(pubDateYearTag)) {
			    isBeginPubDateYear = true;
			} else if (qName.equalsIgnoreCase(pubDateMonthTag)) {
			    isBeginPubDateMonth = true;
			} else if (qName.equalsIgnoreCase(pubDateDayTag)) {
			    isBeginPubDateDay = true;
			} else if (qName.equalsIgnoreCase(journalTitleTag)) {
			    isBeginJournalTitle = true;
			} else if (qName.equals(articleTitleTag)) {
			    isBeginArticleTitle = true;
			} else if (qName.equalsIgnoreCase(abstrTag)) {
			    isBeginAbstr = true;
			    abstrText = null;
			} else if (qName.equalsIgnoreCase(abstrTxtTag)) {
			    isBeginAbstrText = true;
			    Iterator<Attribute> attrIter = event
				    .asStartElement().getAttributes();
			    while (attrIter.hasNext()) {
				Attribute attribute = attrIter.next();
				String name = attribute.getName().toString();
				if (name.equalsIgnoreCase(abstrLabelAttr)) {
				    String labelValue = attribute.getValue()
					    .strip() + ": ";
				    if (abstrText == null) {
					abstrText = labelValue;
				    } else {
					abstrText = abstrText
						.concat(labelValue);
				    }
				}
			    }
			} else if (qName.equalsIgnoreCase(correctionsTag)) {
			    isBeginCorrections = true;
			}
			break;
		    case XMLStreamConstants.CHARACTERS:
			Characters characters = event.asCharacters();
			if (isBeginPubMedArticle) {
			    if (isBeginMedlineCitation) {
				if (isBeginPmid) {
				    if (!isBeginCorrections) {
					pmid = Integer
						.valueOf(characters.getData());
				    }
				}
				if (isBeginArticle) {
				    if (isBeginJournal) {
					if (isBeginJournalIssue) {
					    if (isBeginPubDate) {
						if (isBeginPubDateYear) {
						    year = Integer
							    .valueOf(characters
								    .getData());
						}
						if (isBeginPubDateMonth) {
						    month = characters
							    .getData();
						}
						if (isBeginPubDateDay) {
						    day = Integer
							    .valueOf(characters
								    .getData());
						}
					    }
					}
					if (isBeginJournalTitle) {
					    journalTitle = characters.getData()
						    .strip();
					    if (journalTitle
						    .length() > maxLenJournalTitle) {
						journalTitle = journalTitle
							.substring(0,
								maxLenJournalTitle
									+ 1);
					    }
					}
				    }
				    if (isBeginArticleTitle) {
					articleTitle = characters.getData()
						.strip();
				    }
				}
				if (isBeginAbstr) {
				    if (isBeginAbstrText) {
					if (abstrText == null) {
					    abstrText = characters.getData()
						    .strip() + " ";
					} else {
					    abstrText = abstrText.concat(
						    characters.getData().strip()
							    + " ");
					}
				    }
				}
			    }
			}
			break;
		    case XMLStreamConstants.END_ELEMENT:
			EndElement endElement = event.asEndElement();
			String endName = endElement.getName().getLocalPart();
			if (endName.equalsIgnoreCase(pubMedArticleTag)) {
			    isBeginPubMedArticle = false;
			    result.nbrAbstrNodesParsed++;
			    YMD ymd = new YMD(year, month, day);
			    if (ymd.isCompleteDate()
				    || ymd.isYearAndMonthOnly()) {
				date = ymd.getDate();
			    }
			    NlmAbstract abstr = instantiateAbstract(pmid,
				    journalTitle, articleTitle, abstrText, date,
				    result);
			    if (abstr != null) {
				abstracts.add(abstr);
				nbrInBatch++;
				if (isPersist && nbrInBatch % batchSize == 0) {
				    result.nbrAbstrPersisted += abstrDao
					    .persistBatch(abstracts,
						    persistedPmids, connPool);
				    abstracts.clear();
				    nbrInBatch = 0;
				}
			    }
			    result.nbrAbstrParsed++;
			    if (result.nbrAbstrParsed % rptInterval == 0) {
				System.out.printf(
					"File %1$s: Parsed %2$,d articles and persisted %3$,d "
						+ "(%4$,d are missing abstract text, "
						+ "%5$,d are missing article title, "
						+ "and %6$,d are missing journal title). "
						+ "%7$,d are missing complete date information %n",
					result.fileName, result.nbrAbstrParsed,
					result.nbrAbstrPersisted,
					result.nbrAbstrTextMissing,
					result.nbrArticleTitleMissing,
					result.nbrJournalTitleMissing,
					result.nbrDatesMissing);
			    }
			    pmid = 0;
			    journalTitle = null;
			    articleTitle = null;
			    abstrText = null;
			    year = 0;
			    month = null;
			    day = 0;
			    date = null;
			} else if (endName
				.equalsIgnoreCase(medlineCitationTag)) {
			    isBeginMedlineCitation = false;
			} else if (endName.equalsIgnoreCase(pmidTag)) {
			    isBeginPmid = false;
			} else if (endName.equalsIgnoreCase(articleTag)) {
			    isBeginArticle = false;
			} else if (endName.equalsIgnoreCase(journalTag)) {
			    isBeginJournal = false;
			} else if (endName.equalsIgnoreCase(journalIssueTag)) {
			    isBeginJournalIssue = false;
			} else if (endName.equalsIgnoreCase(pubDateTag)) {
			    isBeginPubDate = false;
			} else if (endName.equalsIgnoreCase(pubDateYearTag)) {
			    isBeginPubDateYear = false;
			} else if (endName.equalsIgnoreCase(pubDateMonthTag)) {
			    isBeginPubDateMonth = false;
			} else if (endName.equalsIgnoreCase(pubDateDayTag)) {
			    isBeginPubDateDay = false;
			} else if (endName.equalsIgnoreCase(journalTitleTag)) {
			    isBeginJournalTitle = false;
			} else if (endName.equalsIgnoreCase(articleTitleTag)) {
			    isBeginArticleTitle = false;
			} else if (endName.equalsIgnoreCase(abstrTag)) {
			    isBeginAbstr = false;
			} else if (endName.equalsIgnoreCase(abstrTxtTag)) {
			    isBeginAbstrText = false;
			} else if (endName.equalsIgnoreCase(correctionsTag)) {
			    isBeginCorrections = false;
			}
			break;
		    }

		}
	    }
	}

	if (isPersist && abstracts.size() > 0) {
	    abstrDao.persistBatch(abstracts, persistedPmids, connPool);
	    result.nbrAbstrPersisted += abstracts.size();
	    abstracts.clear();
	}

	System.out.printf(
		"FINISHED File %1$s: Parsed %2$,d articles and persisted %3$,d "
			+ "(%4$,d are missing abstract text, "
			+ "%5$,d are missing article title, "
			+ "and %6$,d are missing journal title). "
			+ "%7$,d are missing complete date information. %n",
		result.fileName, result.nbrAbstrParsed,
		result.nbrAbstrPersisted, result.nbrAbstrTextMissing,
		result.nbrArticleTitleMissing, result.nbrJournalTitleMissing,
		result.nbrDatesMissing);

	return result;

    }

    private NlmAbstract instantiateAbstract(int pmid, String journalTitle,
	    String articleTitle, String abstrText, Date date,
	    NlmAbstractParserResult result) throws IllegalArgumentException {

	boolean isJournalTitleMissing = journalTitle == null
		|| journalTitle.isBlank() || journalTitle.isEmpty();
	if (isJournalTitleMissing) {
	    result.nbrJournalTitleMissing++;
	}
	boolean isArticleTitleMissing = articleTitle == null
		|| articleTitle.isBlank() || articleTitle.isEmpty();
	if (isArticleTitleMissing) {
	    result.nbrArticleTitleMissing++;
	}
	boolean isAbstractTextMissing = abstrText == null || abstrText.isBlank()
		|| abstrText.isEmpty();
	if (isAbstractTextMissing) {
	    result.nbrAbstrTextMissing++;
	}
	if (date == null) {
	    result.nbrDatesMissing++;
	}

	if (isJournalTitleMissing || isArticleTitleMissing
		|| isAbstractTextMissing) {
	    return null;
	}

	Date dateToPersist = date == null ? null
		: Date.valueOf(date.toString());
	NlmAbstract abstr = new NlmAbstract(pmid,
		String.valueOf(journalTitle).strip(),
		String.valueOf(articleTitle).strip(),
		String.valueOf(abstrText).strip(), dateToPersist);
	abstr.setNegPmid(-1*pmid);
	return abstr;
    }

    public SortedSet<NlmAbstract> getAbstracts() {
	return abstracts;
    }

    public int getNbrMissingData() {
	return nbrMissingData;
    }

    public boolean isPersist() {
	return isPersist;
    }

    public void setIsPersist(boolean isPersist) {
	this.isPersist = isPersist;
    }

    public int getBatchSize() {
	return batchSize;
    }

    public void setBatchSize(int batchSize) {
	this.batchSize = batchSize;
    }

    public int getNbrAbstrParsed() {
	return nbrAbstrParsed;
    }

    public ConcurrentSkipListSet<Integer> getPersistedPmids() {
	return persistedPmids;
    }

    public void setPersistedPmids(
	    ConcurrentSkipListSet<Integer> persistedPmids) {
	this.persistedPmids = persistedPmids;
    }

}

package edu.mst.db.nlm.corpora.parse;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import cern.colt.list.IntArrayList;
import edu.mst.db.lexicon.StopWords;
import edu.mst.db.nlm.corpora.NlmAbstractWordDao;
import edu.mst.db.util.ConnDbName;
import edu.mst.db.util.JdbcConnectionPool;

public class NlmAbstractWordsWorker implements AutoCloseable {

    private JdbcConnectionPool connPool;

    private IntArrayList pmids = new IntArrayList();
    private int nbrPmids = 0;

    private int nbrOfAbstrToProcess = 1000000;
    private int threadBatchSize = 100;
    private int nbrAbstrRptInterval = 10000;
    private int nbrAbstractsProcessed = 0;
    private int nbrWordsPersisted = 0;
    private int currentPos = 0;
    private int maxPos = 0;
    private int startPos = 0;

    private static int nbrOfProcessors;
    private static ExecutorService pool;
    private CompletionService<NlmAbstractWordsResult> svc;

    private StopWords stopWords = null;

    public NlmAbstractWordsWorker(JdbcConnectionPool connPool)
	    throws Exception {
	this.connPool = connPool;
	stopWords = new StopWords(connPool);
	nbrOfProcessors = Runtime.getRuntime().availableProcessors();
	pool = Executors.newFixedThreadPool(nbrOfProcessors *= 3);
	svc = new ExecutorCompletionService<NlmAbstractWordsResult>(pool);
    }

    public void run() throws Exception {

	// marshal PMIDs
	System.out.println("Marshaling PMIDs to process");
	int lastPmidParsed = NlmAbstractWordDao.lastPmidParsed(connPool);
	System.out.printf("PMID of last abstract parsed = %1$d %n",
		lastPmidParsed);
	try (Connection conn = connPool.getConnection()) {
	    String getPmidsSql = null;
	    if (lastPmidParsed == 0) {
		getPmidsSql = String.format(
			"select pmid from conceptrecogn.nlmabstracts order by negPmid limit %1$d ",
			nbrOfAbstrToProcess);
	    } else {
		getPmidsSql = String.format(
			"select pmid from conceptrecogn.nlmabstracts where pmid < ? "
				+ "order by negPmid limit %1$d ",
			nbrOfAbstrToProcess);
	    }
	    System.out.printf("SQL query for retrieving PMIDs: %1$s %n",
		    getPmidsSql);
	    try (PreparedStatement getPmidsStmt = conn
		    .prepareStatement(getPmidsSql)) {
		if (lastPmidParsed > 0) {
		    getPmidsStmt.setInt(1, lastPmidParsed);
		}
		try (ResultSet rs = getPmidsStmt.executeQuery()) {
		    while (rs.next()) {
			pmids.add(rs.getInt(1));
		    }
		}
	    }
	}
	nbrPmids = pmids.size();
	System.out.printf("Marshaled %1$,d PMIDs %n", nbrPmids);

	System.out.println("Parsing sentences and words from abstracts");
	while (currentPos < nbrPmids) {

	    maxPos = getNextBatchMaxPos();
	    startPos = currentPos;
	    for (int pos = startPos; pos <= maxPos; pos++) {
		int pmid = pmids.get(pos);
		NlmAbstractWordsCallable callable = new NlmAbstractWordsCallable(
			pmid, stopWords, connPool);
		svc.submit(callable);
	    }

	    for (int pos = startPos; pos <= maxPos; pos++) {
		Future<NlmAbstractWordsResult> f = svc.take();
		NlmAbstractWordsResult result = f.get();
		nbrWordsPersisted += result.nbrWordsPersisted;
		nbrAbstractsProcessed++;
		if (nbrAbstractsProcessed % nbrAbstrRptInterval == 0) {
		    System.out.printf(
			    "Processed %1$,d abstracts and "
				    + "persisted %2$,d lexicon words %n",
			    nbrAbstractsProcessed, nbrWordsPersisted);
		}
	    }

	    currentPos = maxPos + 1;

	}

	System.out.printf(
		"Processed grand total of %1$,d abstracts and "
			+ "persisted %2$,d lexicon words %n",
		nbrAbstractsProcessed, nbrWordsPersisted);

    }

    private int getNextBatchMaxPos() throws Exception {
	// subtract 1 from max pos since positions are inclusive
	int maxPos = currentPos + threadBatchSize - 1;
	if (maxPos >= nbrPmids - 1)
	    maxPos = nbrPmids - 1;
	return maxPos;
    }

    protected void shutdownAndAwaitTermination(ExecutorService pool) {
	// pool.shutdown(); // Disable new tasks from being submitted
	pool.shutdownNow();
	try {
	    // Wait a while for existing tasks to terminate
	    if (!pool.awaitTermination(5, TimeUnit.SECONDS)) {
		pool.shutdownNow(); // Cancel currently executing tasks
		// Wait a while for tasks to respond to being cancelled
		if (!pool.awaitTermination(15, TimeUnit.SECONDS))
		    System.err.println("Pool did not terminate");
	    }
	} catch (InterruptedException ie) {
	    // (Re-)Cancel if current thread also interrupted
	    pool.shutdownNow();
	    // Preserve interrupt status
	    Thread.currentThread().interrupt();
	}
    }

    @Override
    public void close() throws Exception {
	this.shutdownAndAwaitTermination(pool);
	System.out.println("Parallel processor closed");
    }

    public static void main(String[] args) {

	try {

	    JdbcConnectionPool connPool = new JdbcConnectionPool(
		    ConnDbName.CONCEPT_RECOGN);

	    try (NlmAbstractWordsWorker worker = new NlmAbstractWordsWorker(
		    connPool)) {
		worker.run();
	    }

	} catch (Exception ex) {
	    ex.printStackTrace();
	}

    }

}

package edu.mst.db.nlm.corpora;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;

import edu.mst.db.util.JdbcConnectionPool;

public class NlmAbstractWordDao {

    public static final String deleteAllSql = "truncate table conceptrecogn.nlmabstractwords";
    public static final String deleteForPmidSql = "delete from conceptrecogn.nlmabstractwords where pmid=?";
    public static final String allFields = "pmid, sentNbr, wordNbr, token, lexWordId, beginOffset, endOffset, "
	    + "isStopWord, isPunctOrDelimChar, cumLnAbstrCogency, cumLnSentCogency";
    public static final String persistSql = "INSERT INTO conceptrecogn.nlmabstractwords "
	    + "(" + allFields + ")  VALUES(?,?,?,?,?,?,?,?,?,?,?)";
    public static final String marshalForPmidSql = "SELECT " + allFields
	    + " FROM conceptrecogn.nlmabstractwords where pmid=?";
    public static final String lastPmidParsedSql = "select min(pmid) from conceptrecogn.nlmabstractwords";
    public static final String updateLexWordIdSql = "update conceptrecogn.nlmabstractwords "
	    + "set lexWordId = ? where pmid=? and sentNbr=? and wordNbr=?";
    public static final String updateLexWordIdByTokenSql = "update conceptrecogn.nlmabstractwords "
	    + "set lexWordId = ? where token=?";
    public static final String updateCogencySql = "update conceptrecogn.nlmabstractwords "
	    + "set cumLnSentCogency=?, cumLnAbstrCogency=? where pmid=? and sentNbr=? and wordNbr=?";
    public static final String countPmidsSql = "select count(*) from (select distinct pmid from conceptrecogn.nlmabstractwords) p";
    public static final String getAllPmidsSql = "select pmid from conceptrecogn.nlmabstractwords_pmids";
    public static final String getSentCogenciesSql = "select wordNbr, lexWordId, cumLnAbstrCogency, cumLnSentCogency "
	    + "from conceptrecogn.nlmabstractwords where pmid=? and sentNbr=?";

    public static void deleteAll(JdbcConnectionPool connPool)
	    throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement deleteStmt = conn
		    .prepareStatement(deleteAllSql)) {
		deleteStmt.executeUpdate();
	    }
	}
    }

    public static int[] getPmids(JdbcConnectionPool connPool)
	    throws SQLException {
	SortedSet<Integer> pmidsSet = new TreeSet<Integer>();
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement getPmidsStmt = conn
		    .prepareStatement(getAllPmidsSql)) {
		try (ResultSet rs = getPmidsStmt.executeQuery()) {
		    while (rs.next()) {
			pmidsSet.add(rs.getInt(1));
		    }
		}
	    }
	}

	int[] pmids = new int[pmidsSet.size()];
	Iterator<Integer> pmidsIter = pmidsSet.iterator();
	int pos = 0;
	while(pmidsIter.hasNext()) {
	    Integer pmid = pmidsIter.next();
	    pmids[pos] = pmid;
	    pos++;
	}

	return pmids;
    }

    public void persist(NlmAbstractWord word, JdbcConnectionPool connPool)
	    throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement persistStmt = conn
		    .prepareStatement(persistSql)) {
		setValues(word, persistStmt);
		persistStmt.executeUpdate();
	    }
	}
    }

    public void persist(Collection<NlmAbstractWord> words,
	    JdbcConnectionPool connPool) throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement persistStmt = conn
		    .prepareStatement(persistSql)) {
		for (NlmAbstractWord word : words) {
		    setValues(word, persistStmt);
		    persistStmt.addBatch();
		}
		persistStmt.executeBatch();
	    }
	}
    }

    private void setValues(NlmAbstractWord word, PreparedStatement persistStmt)
	    throws SQLException {
	persistStmt.setInt(1, word.getPmid());
	persistStmt.setInt(2, word.getSentNbr());
	persistStmt.setInt(3, word.getSentWordNbr());
	persistStmt.setString(4, word.getToken());
	persistStmt.setInt(5, word.getLexWordId());
	persistStmt.setInt(6, word.getBeginOffset());
	persistStmt.setInt(7, word.getEndOffset());
	persistStmt.setBoolean(8, word.isStopWord());
	persistStmt.setBoolean(9, word.isPunctOrDelimChar());
	persistStmt.setDouble(10, word.getCumLnAbstrCogency());
	persistStmt.setDouble(11, word.getCumLnSentCogency());
    }

    public SortedSet<NlmAbstractWord> marshalWords(int pmid,
	    JdbcConnectionPool connPool) throws SQLException {
	SortedSet<NlmAbstractWord> words = new TreeSet<NlmAbstractWord>();
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement marshalStmt = conn
		    .prepareStatement(marshalForPmidSql)) {
		marshalStmt.setInt(1, pmid);
		try (ResultSet rs = marshalStmt.executeQuery()) {
		    while (rs.next()) {
			NlmAbstractWord word = instantiateFromResultSet(rs);
			words.add(word);
		    }
		}
	    }
	}
	return words;
    }

    public SortedSet<NlmAbstractSentence> marshalSentences(int pmid,
	    JdbcConnectionPool connPool) throws SQLException {
	SortedSet<NlmAbstractSentence> sentences = new TreeSet<NlmAbstractSentence>();
	SortedSet<NlmAbstractWord> sortedWordSet = marshalWords(pmid, connPool);
	boolean isNewSent = true;
	int lastSentNbr = 0;
	NlmAbstractSentence currentSent = null;
	Iterator<NlmAbstractWord> wordSetIter = sortedWordSet.iterator();
	while (wordSetIter.hasNext()) {
	    NlmAbstractWord word = wordSetIter.next();
	    isNewSent = word.getSentNbr() > lastSentNbr || currentSent == null
		    ? true
		    : false;
	    if (isNewSent) {
		currentSent = new NlmAbstractSentence();
		currentSent.setPmid(pmid);
		currentSent.setSentNbr(word.getSentNbr());
		sentences.add(currentSent);
	    }
	    currentSent.getWords().add(word);
	    lastSentNbr = word.getSentNbr();
	}
	return sentences;
    }

    private NlmAbstractWord instantiateFromResultSet(ResultSet rs)
	    throws SQLException {
	NlmAbstractWord abstrWord = new NlmAbstractWord();
	/*
	 * pmid, sentNbr, wordNbr, token, lexWordId, beginOffset, endOffset,
	 * isStopWord, isPunctOrDelimChar, cumAbstrCogency, cumSentCogency
	 */
	abstrWord.setPmid(rs.getInt(1));
	abstrWord.setSentNbr(rs.getInt(2));
	abstrWord.setSentWordNbr(rs.getInt(3));
	abstrWord.setToken(rs.getString(4));
	abstrWord.setLexWordId(rs.getInt(5));
	abstrWord.setBeginOffset(rs.getInt(6));
	abstrWord.setEndOffset(rs.getInt(7));
	abstrWord.setIsStopWord(rs.getBoolean(8));
	abstrWord.setIsPunctOrDelimChar(rs.getBoolean(9));
	abstrWord.setCumLnAbstrCogency(rs.getDouble(10));
	abstrWord.setCumLnSentCogency(rs.getDouble(11));
	return abstrWord;
    }

    public void updateCogencyBatch(Collection<NlmAbstractSentence> sentences,
	    JdbcConnectionPool connPool) throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement updateCogencyStmt = conn
		    .prepareStatement(updateCogencySql)) {
		for (NlmAbstractSentence sent : sentences) {
		    for (NlmAbstractWord word : sent.getWords()) {
			updateCogencyStmt.setDouble(1,
				word.getCumLnSentCogency());
			updateCogencyStmt.setDouble(2,
				word.getCumLnAbstrCogency());
			updateCogencyStmt.setInt(3, word.getPmid());
			updateCogencyStmt.setInt(4, word.getSentNbr());
			updateCogencyStmt.setInt(5, word.getSentWordNbr());
			updateCogencyStmt.addBatch();
		    }
		}
		updateCogencyStmt.executeBatch();
	    }
	}
    }

    public SortedSet<Cogency> getSentCogencies(int pmid, int sentNbr,
	    JdbcConnectionPool connPool) throws SQLException {
	SortedSet<Cogency> cogencies = new TreeSet<Cogency>();
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement getSentCogenciesStmt = conn
		    .prepareStatement(getSentCogenciesSql)) {
		getSentCogenciesStmt.setInt(1, pmid);
		getSentCogenciesStmt.setInt(2, sentNbr);
		try (ResultSet rs = getSentCogenciesStmt.executeQuery()) {
		    while (rs.next()) {
			Cogency cogency = new Cogency();
			cogency.setPmid(pmid);
			cogency.setSentNbr(sentNbr);
			cogency.setWordNbr(rs.getInt(1));
			cogency.setLexWordId(rs.getInt(2));
			cogency.setCumLnAbstractCogency(rs.getDouble(3));
			cogency.setCumLnSentCogency(rs.getDouble(4));
			cogencies.add(cogency);
		    }
		}
	    }
	}
	return cogencies;
    }

    /**
     * Retrieves minimum PMID from the nlmabstractwords table.
     * <p>
     * The word parsing process is performed in descending PMID order, so the
     * minimum PMID corresponds to the last PMID parsed.
     * 
     * @param connPool
     * @return
     * @throws SQLException
     */
    public static int lastPmidParsed(JdbcConnectionPool connPool)
	    throws SQLException {
	int lastPmid = 0;
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement lastPmidStmt = conn
		    .prepareStatement(lastPmidParsedSql)) {
		try (ResultSet rs = lastPmidStmt.executeQuery()) {
		    if (rs.next()) {
			lastPmid = rs.getInt(1);
		    }
		}
	    }
	}
	return lastPmid;
    }

    public void deleteWordsForPmid(int pmid, JdbcConnectionPool connPool)
	    throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement deleteStmt = conn
		    .prepareStatement(deleteForPmidSql)) {
		deleteStmt.setInt(1, pmid);
		deleteStmt.executeUpdate();
	    }
	}
    }

}

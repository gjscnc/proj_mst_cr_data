/**
 * 
 */
package edu.mst.db.nlm.corpora.cogency;

import java.util.concurrent.Future;

import edu.mst.concurrent.BaseWorker;
import edu.mst.db.nlm.corpora.NlmAbstractWordDao;
import edu.mst.db.util.ConnDbName;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * Compute cogency values for sentences and abstracts in the NLM corpora.
 * 
 * @author gjs
 *
 */
public class PersistNlmCogencyWorker extends BaseWorker<Integer> {

    private int nbrPmids = 0;
    private int[] pmids = null;

    private int nbrPmidsProcessed = 0;
    private int nbrSentProcessed = 0;
    private int rptInterval = 5000;

    public PersistNlmCogencyWorker(JdbcConnectionPool connPool) {
	super(connPool);
    }

    @Override
    public void run() throws Exception {

	System.out.println("Retrieving PMIDs for NLM abstracts");
	pmids = NlmAbstractWordDao.getPmids(connPool);
	nbrPmids = pmids.length;
	System.out.printf("Marshaled %1$,d PMIDs into memory %n", nbrPmids);

	System.out.println("Now computing cogency values for each abstract");

	while (currentPos < nbrPmids) {

	    maxPos = getNextBatchMaxPos();
	    startPos = currentPos;

	    for (int i = startPos; i <= maxPos; i++) {
		int pmid = pmids[i];
		PersistNlmCogencyCallable callable = new PersistNlmCogencyCallable(
			pmid, connPool);
		svc.submit(callable);
	    }

	    for (int i = startPos; i <= maxPos; i++) {
		Future<Integer> future = svc.take();
		nbrSentProcessed += future.get();
		nbrPmidsProcessed++;
		if (nbrPmidsProcessed % rptInterval == 0) {
		    System.out.printf(
			    "Processed %1$,d abstracts, encompassing %2$,d sentences. %n",
			    nbrPmidsProcessed, nbrSentProcessed);
		}
	    }

	    currentPos = maxPos + 1;

	}

	System.out.printf(
		"Processed total of %1$,d abstracts, encompassing %2$,d sentences. %n",
		nbrPmidsProcessed, nbrSentProcessed);
    }

    @Override
    protected int getNextBatchMaxPos() throws Exception {
	// subtract 1 from max pos since positions are inclusive
	int maxPos = currentPos + threadCallablesBatchSize - 1;
	if (maxPos >= nbrPmids - 1)
	    maxPos = nbrPmids - 1;
	return maxPos;
    }

    public static void main(String[] args) {
	
	try {
	    
	    JdbcConnectionPool connPool = new JdbcConnectionPool(ConnDbName.CONCEPT_RECOGN);
	    
	    try(PersistNlmCogencyWorker worker = new PersistNlmCogencyWorker(connPool)){
		worker.run();
	    }
	    
	} catch(Exception ex) {
	    ex.printStackTrace();
	}
	
    }

}

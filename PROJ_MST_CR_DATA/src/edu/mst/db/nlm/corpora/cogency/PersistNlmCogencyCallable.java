/**
 * 
 */
package edu.mst.db.nlm.corpora.cogency;

import java.util.Iterator;
import java.util.SortedSet;
import java.util.concurrent.Callable;

import edu.mst.db.nlm.corpora.NlmAbstractSentence;
import edu.mst.db.nlm.corpora.NlmAbstractWord;
import edu.mst.db.nlm.corpora.NlmAbstractWordDao;
import edu.mst.db.nlm.corpora.condprob.CorporaCondWordProbDao;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * @author gjs
 *
 */
public class PersistNlmCogencyCallable implements Callable<Integer> {

    private int pmid;
    private JdbcConnectionPool connPool;

    private NlmAbstractWordDao abstrWordDao = new NlmAbstractWordDao();
    private CorporaCondWordProbDao wordProbDao = new CorporaCondWordProbDao();

    public PersistNlmCogencyCallable(int pmid, JdbcConnectionPool connPool) {
	this.pmid = pmid;
	this.connPool = connPool;
    }

    @Override
    public Integer call() throws Exception {

	SortedSet<NlmAbstractSentence> sentences = abstrWordDao
		.marshalSentences(pmid, connPool);
	Iterator<NlmAbstractSentence> sentIter = sentences.iterator();
	double cumLnAbstrCogency = 0.0d;
	NlmAbstractWord assumedFactWord = null;
	while (sentIter.hasNext()) {
	    NlmAbstractSentence sent = sentIter.next();
	    double cumLnSentCogency = 0.0d;
	    Iterator<NlmAbstractWord> wordIter = sent.getWords().iterator();
	    while (wordIter.hasNext()) {
		NlmAbstractWord predWord = wordIter.next();
		predWord.setCumLnAbstrCogency(cumLnAbstrCogency);
		predWord.setCumLnSentCogency(cumLnSentCogency);
		if (assumedFactWord == null) {
		    assumedFactWord = predWord;
		    continue;
		}
		double lnCondProb = wordProbDao.marshalLnCondProbForPredFact(
			predWord.getLexWordId(), assumedFactWord.getLexWordId(),
			connPool);
		cumLnAbstrCogency += lnCondProb;
		// cumulative for abstract
		predWord.setCumLnAbstrCogency(cumLnAbstrCogency);
		// cumulative for sentence
		if (predWord.getSentWordNbr() > 0) {
		    cumLnSentCogency += lnCondProb;
		    predWord.setCumLnSentCogency(cumLnSentCogency);
		}
		assumedFactWord = predWord;
	    }
	}

	abstrWordDao.updateCogencyBatch(sentences, connPool);

	return sentences.size();
    }

}

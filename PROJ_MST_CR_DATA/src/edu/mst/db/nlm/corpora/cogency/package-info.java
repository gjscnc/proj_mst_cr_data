/**
 * 
 */
/**
 * Components to compute the 'standard' Hecht-Nielsen cogency values for
 * sentences and abstracts in the NLM corpora
 * 
 * @author gjs
 *
 */
package edu.mst.db.nlm.corpora.cogency;
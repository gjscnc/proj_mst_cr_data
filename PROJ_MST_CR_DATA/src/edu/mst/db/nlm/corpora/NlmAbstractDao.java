package edu.mst.db.nlm.corpora;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentSkipListSet;

import cern.colt.list.IntArrayList;

import edu.mst.db.util.JdbcConnectionPool;

public class NlmAbstractDao {

    public static final String deleteAllSql = "truncate table conceptrecogn.nlmabstracts";
    public static final String getAbstrSql = "SELECT journal, title, abstract, abstrLen, date, isWordsCounted, negPmid "
	    + "FROM conceptrecogn.nlmabstracts WHERE pmid=?";
    public static final String persistSql = "INSERT INTO conceptrecogn.nlmabstracts "
	    + "(pmid, journal, title, abstract, abstrLen, date, isWordsCounted, isLexParsed, negPmid) VALUES(?,?,?,?,?,?,?,?,?)";
    public static final String nbrAbstrSql = "select count(*) from conceptrecogn.nlmabstracts";
    public static final String nbrAbstrWordsNotCountedSql = "select count(*) from conceptrecogn.nlmabstracts where isWordsCounted = 0";
    public static final String getLastPmidWordsCountedSql = "select max(pmid) from conceptrecogn.nlmabstracts where isWordsCounted = 1";
    public static final String getMaxPmidWordsNotCountedSql = "select max(pmid) from conceptrecogn.nlmabstracts where isWordsCounted = 0";
    public static final String setIsWordsCountedSql = "update conceptrecogn.nlmabstractwords_pmids "
	    + "set isWordsCounted = 1 where pmid = ?";
    public static final String resetIsWordsCountedSql = "update conceptrecogn.nlmabstractwords_pmids "
	    + "set isWordsCounted = 0 where isWordsCounted = 1";
    public static final String nbrAbstrNotLexParsedSql = "select count(*) from conceptrecogn.nlmabstracts where isLexParsed = 0";
    public static final String getLastPmidLexParsedSql = "select max(pmid) from conceptrecogn.nlmabstracts where isLexParsed = 1";
    public static final String getMaxPmidNotLexParsedSql = "select max(pmid) from conceptrecogn.nlmabstracts where isLexParsed = 0";
    public static final String setIsLexParsedSql = "update conceptrecogn.nlmabstracts set isLexParsed = 1 where pmid = ?";
    public static final String resetIsLexParsedSql = "update conceptrecogn.nlmabstracts  set isLexParsed = 0 where isLexParsed = 1";
    public static final String countIsLexParsedSql = "select count(*) from conceptrecogn.nlmabstracts where isLexParsed  = 1";

    public static void deleteAll(JdbcConnectionPool connPool)
	    throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement deleteStmt = conn
		    .prepareStatement(deleteAllSql)) {
		deleteStmt.executeUpdate();
	    }
	}
    }

    public static int nbrOfAbstracts(JdbcConnectionPool connPool)
	    throws SQLException {
	int nbrAbstr = 0;
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement countStmt = conn
		    .prepareStatement(nbrAbstrSql)) {
		try (ResultSet rs = countStmt.executeQuery()) {
		    if (rs.next()) {
			nbrAbstr = rs.getInt(1);
		    }
		}
	    }
	}
	return nbrAbstr;
    }

    public static int nbrAbstrWordsNotCounted(JdbcConnectionPool connPool)
	    throws SQLException {
	int nbrAbstr = 0;
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement countStmt = conn
		    .prepareStatement(nbrAbstrWordsNotCountedSql)) {
		try (ResultSet rs = countStmt.executeQuery()) {
		    if (rs.next()) {
			nbrAbstr = rs.getInt(1);
		    }
		}
	    }
	}
	return nbrAbstr;
    }

    public static int nbrAbstrNotLexParsed(JdbcConnectionPool connPool)
	    throws SQLException {
	int nbrAbstr = 0;
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement countStmt = conn
		    .prepareStatement(nbrAbstrNotLexParsedSql)) {
		try (ResultSet rs = countStmt.executeQuery()) {
		    if (rs.next()) {
			nbrAbstr = rs.getInt(1);
		    }
		}
	    }
	}
	return nbrAbstr;
    }

    public static int minPmid(JdbcConnectionPool connPool) throws SQLException {
	int minPmid = 0;
	try (Connection conn = connPool.getConnection()) {
	    String getMinPmidSql = "select min(pmid) from conceptrecogn.nlmabstracts";
	    try (PreparedStatement getMinPmidStmt = conn
		    .prepareStatement(getMinPmidSql)) {
		try (ResultSet rs = getMinPmidStmt.executeQuery()) {
		    if (rs.next()) {
			minPmid = rs.getInt(1);
		    }
		}
	    }
	}
	return minPmid;
    }

    public static int getLastPmidWordsCounted(JdbcConnectionPool connPool)
	    throws SQLException {
	int lastPmid = 0;
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement getLastPmidCountedStmt = conn
		    .prepareStatement(getLastPmidWordsCountedSql)) {
		try (ResultSet rs = getLastPmidCountedStmt.executeQuery()) {
		    if (rs.next()) {
			lastPmid = rs.getInt(1);
		    }
		}
	    }
	}
	return lastPmid;
    }

    public static int getLastPmidLexParsed(JdbcConnectionPool connPool)
	    throws SQLException {
	int lastPmid = 0;
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement getLastPmidCountedStmt = conn
		    .prepareStatement(getLastPmidLexParsedSql)) {
		try (ResultSet rs = getLastPmidCountedStmt.executeQuery()) {
		    if (rs.next()) {
			lastPmid = rs.getInt(1);
		    }
		}
	    }
	}
	return lastPmid;
    }

    public static int getMaxPmidWordsNotCounted(JdbcConnectionPool connPool)
	    throws SQLException {
	int lastPmid = 0;
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement getLastPmidCountedStmt = conn
		    .prepareStatement(getMaxPmidWordsNotCountedSql)) {
		try (ResultSet rs = getLastPmidCountedStmt.executeQuery()) {
		    if (rs.next()) {
			lastPmid = rs.getInt(1);
		    }
		}
	    }
	}
	return lastPmid;
    }

    public static int getMaxPmidNotLexParsed(JdbcConnectionPool connPool)
	    throws SQLException {
	int lastPmid = 0;
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement getLastPmidCountedStmt = conn
		    .prepareStatement(getMaxPmidNotLexParsedSql)) {
		try (ResultSet rs = getLastPmidCountedStmt.executeQuery()) {
		    if (rs.next()) {
			lastPmid = rs.getInt(1);
		    }
		}
	    }
	}
	return lastPmid;
    }

    public static void setIsWordsCountedBatch(
	    Collection<Integer> pmidsProcessed, JdbcConnectionPool connPool)
	    throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement updateStmt = conn
		    .prepareStatement(setIsWordsCountedSql)) {
		int updateBatchSize = 500;
		int nbrInBatch = 0;
		Iterator<Integer> pmidsIter = pmidsProcessed.iterator();
		while (pmidsIter.hasNext()) {
		    int pmid = pmidsIter.next();
		    updateStmt.setInt(1, pmid);
		    updateStmt.addBatch();
		    nbrInBatch++;
		    if (nbrInBatch % updateBatchSize == 0) {
			updateStmt.executeBatch();
			nbrInBatch = 0;
		    }
		}
		// finish up stragglers
		updateStmt.executeBatch();
	    }
	}
    }

    public static void setIsWordsCountedBatch(IntArrayList pmidsProcessed,
	    JdbcConnectionPool connPool) throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement updateStmt = conn
		    .prepareStatement(setIsWordsCountedSql)) {
		int updateBatchSize = 500;
		int nbrInBatch = 0;
		for (int i = 0; i < pmidsProcessed.size(); i++) {
		    int pmid = pmidsProcessed.get(i);
		    updateStmt.setInt(1, pmid);
		    updateStmt.addBatch();
		    nbrInBatch++;
		    if (nbrInBatch % updateBatchSize == 0) {
			updateStmt.executeBatch();
			nbrInBatch = 0;
		    }
		}
		// finish up stragglers
		updateStmt.executeBatch();
	    }
	}
    }

    public static void setIsLexParsedBatch(Collection<Integer> pmidsProcessed,
	    JdbcConnectionPool connPool) throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement updateStmt = conn
		    .prepareStatement(setIsLexParsedSql)) {
		int updateBatchSize = 500;
		int nbrInBatch = 0;
		Iterator<Integer> pmidIter = pmidsProcessed.iterator();
		while (pmidIter.hasNext()) {
		    int pmid = pmidIter.next();
		    updateStmt.setInt(1, pmid);
		    updateStmt.addBatch();
		    nbrInBatch++;
		    if (nbrInBatch % updateBatchSize == 0) {
			updateStmt.executeBatch();
			conn.commit();
			nbrInBatch = 0;
		    }
		}
		// finish up stragglers
		updateStmt.executeBatch();
	    }
	}
    }

    public static void setIsLexParsedBatch(IntArrayList pmidsProcessed,
	    int updateBatchSize, JdbcConnectionPool connPool)
	    throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement updateStmt = conn
		    .prepareStatement(setIsLexParsedSql);
		    PreparedStatement countTotalStmt = conn
			    .prepareStatement(countIsLexParsedSql)) {
		int nbrInBatch = 0;
		for (int i = 0; i < pmidsProcessed.size(); i++) {
		    int pmid = pmidsProcessed.get(i);
		    updateStmt.setInt(1, pmid);
		    updateStmt.addBatch();
		    nbrInBatch++;
		    if (nbrInBatch % updateBatchSize == 0) {
			updateStmt.executeBatch();
			nbrInBatch = 0;
		    }
		}
		// finish up stragglers
		updateStmt.executeBatch();
		try (ResultSet rs = countTotalStmt.executeQuery()) {
		    if (rs.next()) {
			System.out.printf(
				"%nTotal of %1$,d NLM abstracts parsed to date %n",
				rs.getInt(1));
		    }
		}
	    }
	}
    }

    public static void resetIsWordsCounted(JdbcConnectionPool connPool)
	    throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement resetStmt = conn
		    .prepareStatement(resetIsWordsCountedSql)) {
		resetStmt.executeUpdate();
	    }
	}
    }

    public static void resetIsLexParsed(JdbcConnectionPool connPool)
	    throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement resetStmt = conn
		    .prepareStatement(resetIsLexParsedSql)) {
		resetStmt.executeUpdate();
	    }
	}
    }

    public void persist(NlmAbstract nlmAbstract, JdbcConnectionPool connPool)
	    throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement persistStmt = conn
		    .prepareStatement(persistSql)) {
		persistStmt.setInt(1, nlmAbstract.getPmid());
		persistStmt.setString(2, nlmAbstract.getJournal());
		persistStmt.setString(3, nlmAbstract.getTitle());
		persistStmt.setString(4, nlmAbstract.getAbstrText());
		persistStmt.setInt(5, nlmAbstract.getAbstrLen());
		persistStmt.setDate(6, nlmAbstract.getDate());
		persistStmt.setBoolean(7, nlmAbstract.isWordsCounted());
		persistStmt.setBoolean(8, nlmAbstract.isLexParsed());
		persistStmt.setInt(9, nlmAbstract.getNegPmid());
		persistStmt.executeUpdate();
	    }
	}
    }

    public int persistBatch(Collection<NlmAbstract> abstractBatch,
	    ConcurrentSkipListSet<Integer> persistedPmids,
	    JdbcConnectionPool connPool) throws SQLException {
	int nbrPersisted = 0;
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement persistStmt = conn
		    .prepareStatement(NlmAbstractDao.persistSql)) {
		Iterator<NlmAbstract> abstrIter = abstractBatch.iterator();
		while (abstrIter.hasNext()) {
		    NlmAbstract nlmAbstract = abstrIter.next();
		    if (persistedPmids != null
			    && persistedPmids.contains(nlmAbstract.getPmid())) {
			continue;
		    } else {
			if (persistedPmids != null) {
			    persistedPmids.add(nlmAbstract.getPmid());
			}
			persistStmt.setInt(1, nlmAbstract.getPmid());
			persistStmt.setString(2, nlmAbstract.getJournal());
			persistStmt.setString(3, nlmAbstract.getTitle());
			persistStmt.setString(4, nlmAbstract.getAbstrText());
			persistStmt.setInt(5, nlmAbstract.getAbstrLen());
			persistStmt.setDate(6, nlmAbstract.getDate());
			persistStmt.setBoolean(7, nlmAbstract.isWordsCounted());
			persistStmt.setBoolean(8, nlmAbstract.isLexParsed());
			persistStmt.setInt(9, nlmAbstract.getNegPmid());
			persistStmt.addBatch();
			nbrPersisted++;
		    }
		}
		persistStmt.executeBatch();
	    }
	}
	return nbrPersisted;
    }

    public NlmAbstract marshal(int pmid, JdbcConnectionPool connPool)
	    throws Exception {
	NlmAbstract nlmAbstr = null;
	try (Connection conn = connPool.getConnection()) {
	    nlmAbstr = marshal(pmid, conn);
	}
	return nlmAbstr;
    }

    public NlmAbstract marshal(int pmid, Connection conn) throws SQLException {
	NlmAbstract nlmAbstr = null;
	try (PreparedStatement marshalStmt = conn
		.prepareStatement(getAbstrSql)) {
	    marshalStmt.setInt(1, pmid);
	    try (ResultSet rs = marshalStmt.executeQuery()) {
		if (rs.next()) {
		    nlmAbstr = new NlmAbstract(pmid, rs.getString(1),
			    rs.getString(2), rs.getString(3), rs.getInt(4),
			    rs.getDate(5));
		    nlmAbstr.setIsWordsCounted(rs.getBoolean(6));
		    nlmAbstr.setNegPmid(rs.getInt(7));
		}
	    }
	}
	return nlmAbstr;
    }

    /**
     * Get array of PMIDs from corpora in descending order.
     * 
     * @param connPool
     * @return
     * @throws SQLException
     */
    public static int[] getPmidsDesc(JdbcConnectionPool connPool)
	    throws SQLException {
	int nbrPmids = 0;
	int[] pmids = null;
	try (Connection conn = connPool.getConnection()) {
	    String countSql = "select count(*) from conceptrecogn.nlmabstracts";
	    String pmidsSql = "select pmid from conceptrecogn.nlmabstracts order by pmid desc";
	    try (PreparedStatement countStmt = conn.prepareStatement(countSql);
		    PreparedStatement pmidsStmt = conn
			    .prepareStatement(pmidsSql)) {
		try (ResultSet rs = countStmt.executeQuery()) {
		    if (rs.next()) {
			nbrPmids = rs.getInt(1);
		    } else {
			return pmids;
		    }
		}
		pmids = new int[nbrPmids];
		try (ResultSet rs = pmidsStmt.executeQuery()) {
		    int i = 0;
		    while (rs.next()) {
			pmids[i] = rs.getInt(1);
			i++;
		    }
		}
	    }
	}
	return pmids;
    }

    /**
     * Get array of PMIDs from corpora in descending order.
     * 
     * @param connPool
     * @return
     * @throws SQLException
     */
    public static int[] getPmidsAsc(JdbcConnectionPool connPool)
	    throws SQLException {
	SortedSet<Integer> pmidSet = new TreeSet<Integer>();
	try (Connection conn = connPool.getConnection()) {
	    String pmidsSql = "select pmid from conceptrecogn.nlmabstracts";
	    try (PreparedStatement pmidsStmt = conn
		    .prepareStatement(pmidsSql)) {
		try (ResultSet rs = pmidsStmt.executeQuery()) {
		    while (rs.next()) {
			pmidSet.add(rs.getInt(1));
		    }
		}
	    }
	}
	int[] pmids = new int[pmidSet.size()];
	int pos = 0;
	Iterator<Integer> pmidIter = pmidSet.iterator();
	while (pmidIter.hasNext()) {
	    Integer pmid = pmidIter.next();
	    pmids[pos] = pmid;
	    pos++;
	}
	return pmids;
    }
}

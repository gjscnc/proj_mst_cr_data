/**
 * 
 */
package edu.mst.db.nlm.corpora;

/**
 * Enumerates the test data sets obtained from the NLM for training and
 * evaluation of the MLP.
 * <p>
 * TODO modify to account for different platforms, i.e., different root
 * directories. Currently configured for Linux.
 * </p>
 * 
 * @author gjs
 *
 */
public enum Corpus {

    BIO_CITS_TC_CORPUS("Bio_Cits_TC"), CLIN_CITS_TC_CORPUS(
	    "Clin_Cits_TC"), NCBI_DISEASE_CORPUS(
		    "NCBI_Disease_Corpus_Training_TC"), ALL("All");

    public static final String defaultRootResultsDirLinux = "/home/gjs/umls/TestCollections/MetaMapResults/";
    public static final String defaultRootMetaMapInputDirLinux = "/home/gjs/umls/TestCollections/MetaMapInput/";
    public static final String defaultRootMetaMapResultsDirWindow = "C:\\UMLS\\IOC_Data\\MetaMapBatchOutput\\";
    public static final String defeaultRootResultDirWindows = "C:\\UMLS\\IOC_Data\\MetaMapBatchInput\\";
    public static final String xmlFileExtension = ".xml";

    private String corpusDirName;

    Corpus(String corpusDirName) {
	this.corpusDirName = corpusDirName;
    }

    public String getCorpusDirName() {
	return corpusDirName;
    }

    public static Corpus getCorpusForDirName(String dirName) {

	Corpus corpus = null;

	switch (dirName) {
	case "Bio_Cits_TC":
	    corpus = Corpus.BIO_CITS_TC_CORPUS;
	    break;
	case "Clin_Cits_TC":
	    corpus = Corpus.CLIN_CITS_TC_CORPUS;
	    break;
	case "NCBI_Disease_Corpus_Training_TC":
	    corpus = Corpus.NCBI_DISEASE_CORPUS;
	    break;
	default:
	    corpus = Corpus.ALL;
	    break;
	}

	return corpus;
    }
    

}

/**
 * 
 */
package edu.mst.db.nlm.corpora;

/**
 * POJO for cogency values for a {@link NlmAbstractWord}.
 * <p>
 * Used to reduce memory requirements during processing.
 * 
 * @author gjs
 *
 */
public class Cogency implements Comparable<Cogency> {
    
    private int pmid = 0;
    private int sentNbr = 0;
    private int wordNbr = 0;
    private int lexWordId = 0;
    private double cumLnAbstractCogency = 0.0d;
    private double cumLnSentCogency = 0.0d;

    @Override
    public int compareTo(Cogency cogency) {
	int result = Integer.compare(pmid, cogency.pmid);
	if (result != 0) {
	    return result;
	}
	result = Integer.compare(sentNbr, cogency.sentNbr);
	if (result != 0) {
	    return result;
	}
	return Integer.compare(wordNbr, cogency.wordNbr);
    }

    @Override
    public boolean equals(Object obj) {
	if (obj instanceof Cogency) {
	    Cogency cogency = (Cogency) obj;
	    return pmid == cogency.pmid && sentNbr == cogency.sentNbr
		    && wordNbr == cogency.wordNbr;
	}
	return false;
    }

    @Override
    public int hashCode() {
	return Integer.hashCode(pmid) + 7 * Integer.hashCode(sentNbr)
		+ 31 * Integer.hashCode(wordNbr);
    }

    public int getPmid() {
        return pmid;
    }

    public void setPmid(int pmid) {
        this.pmid = pmid;
    }

    public int getSentNbr() {
        return sentNbr;
    }

    public void setSentNbr(int sentNbr) {
        this.sentNbr = sentNbr;
    }

    public int getWordNbr() {
        return wordNbr;
    }

    public void setWordNbr(int wordNbr) {
        this.wordNbr = wordNbr;
    }

    public int getLexWordId() {
        return lexWordId;
    }

    public void setLexWordId(int lexWordId) {
        this.lexWordId = lexWordId;
    }

    public double getCumLnAbstractCogency() {
        return cumLnAbstractCogency;
    }

    public void setCumLnAbstractCogency(double cumLnAbstractCogency) {
        this.cumLnAbstractCogency = cumLnAbstractCogency;
    }

    public double getCumLnSentCogency() {
        return cumLnSentCogency;
    }

    public void setCumLnSentCogency(double cumLnSentCogency) {
        this.cumLnSentCogency = cumLnSentCogency;
    }
}

package edu.mst.db.nlm.corpora;

import java.sql.Date;

public class NlmDocument implements Comparable<NlmDocument> {

    private int pmid = 0;
    private String journal = null;
    private String title = null;
    private String abstr = null;
    private int abstrLen = 0;
    private String body = null;
    private int bodyLen = 0;
    private Date date = null;

    public NlmDocument(int pmid, String journal, String title, String abstr,
	    String body, Date date) throws IllegalArgumentException {
	this.pmid = pmid;
	this.journal = journal;
	this.title = title;
	this.abstr = abstr;
	this.body = body;
	isMissingData();
    }

    public NlmDocument(int pmid, String journal, String title, String abstr,
	    int abstrLen, String body, int bodyLen, Date date)
	    throws IllegalArgumentException {
	this.pmid = pmid;
	this.journal = journal;
	this.title = title;
	this.abstr = abstr;
	this.abstrLen = abstrLen;
	this.body = body;
	this.bodyLen = bodyLen;
	isMissingData();
    }

    private boolean isMissingData() throws IllegalArgumentException {
	if (journal == null || journal.isBlank() || journal.isEmpty()) {
	    throw new IllegalArgumentException("Missing journal");
	} else if (title == null || title.isBlank() || title.isEmpty()) {
	    throw new IllegalArgumentException("Missing title");
	} else if (abstr == null || abstr.isBlank() || abstr.isEmpty()) {
	    throw new IllegalArgumentException("Missing abstract");
	} else if (body == null || body.isBlank() || body.isEmpty()) {
	    throw new IllegalArgumentException("Missing body");
	}
	return false;
    }

    @Override
    public int compareTo(NlmDocument doc) {
	return Integer.compare(pmid, doc.pmid);
    }

    @Override
    public boolean equals(Object obj) {
	if (obj instanceof NlmDocument) {
	    return pmid == ((NlmDocument) obj).pmid;
	}
	return false;
    }

    @Override
    public int hashCode() {
	return Integer.hashCode(pmid);
    }

    public int getPmid() {
        return pmid;
    }

    public String getJournal() {
        return journal;
    }

    public String getTitle() {
        return title;
    }

    public String getAbstr() {
        return abstr;
    }

    public int getAbstrLen() {
        return abstrLen;
    }

    public String getBody() {
        return body;
    }

    public int getBodyLen() {
        return bodyLen;
    }

    public Date getDate() {
        return date;
    }

}

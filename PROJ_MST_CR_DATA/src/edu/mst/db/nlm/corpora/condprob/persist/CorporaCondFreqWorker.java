/**
 * 
 */
package edu.mst.db.nlm.corpora.condprob.persist;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import cern.colt.list.IntArrayList;
import edu.mst.db.nlm.corpora.NlmAbstractDao;
import edu.mst.db.nlm.corpora.condprob.CorporaCondWordProbDao;
import edu.mst.db.util.ConnDbName;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * Worker class for parsing and computing word co-occurence frequencies in NLM
 * corpora.
 * 
 * @author gjs
 *
 */
public class CorporaCondFreqWorker implements AutoCloseable {

    private int threadPoolBatchSize = 0;
    private int nbrPmidsPerCallable = 0;
    private int persistFreqBatchSize = 0;
    private int batchAccumulatorSize = 0;
    private int nbrOfAbstrToProcess = 0;
    private JdbcConnectionPool connPool = null;

    private static int nbrOfProcessors;
    private static ExecutorService pool;
    private CompletionService<CorporaCondFreqResult> svc;

    private int rptIntervalAbstrProcessed = 0;
    private int rptIntervalFreqPersisted = 0;
    private int maxPmid = 0;
    private int minPmid = 0;
    private int abstrCount = 0;
    private int nbrPmidsProcessed = 0;
    private long nbrCondFreqToPersist = 0;
    private int nbrCondFreqPersisted = 0;

    private IntArrayList batchPmids = new IntArrayList();
    private IntArrayList pmidsProcessed = new IntArrayList();
    private List<IntArrayList> threadBatchPmids = new ArrayList<IntArrayList>();

    private Map<Integer, Map<Integer, Long>> freqByPredFact = new HashMap<Integer, Map<Integer, Long>>();

    public CorporaCondFreqWorker(int threadsBatchSize, int nbrPmidsPerCallable,
	    int persistFreqBatchSize, int nbrOfAbstrToProcess,
	    int rptIntervalAbstrProcessed, int rptIntervalFreqPersisted,
	    JdbcConnectionPool connPool) {
	this.threadPoolBatchSize = threadsBatchSize;
	this.nbrPmidsPerCallable = nbrPmidsPerCallable;
	this.persistFreqBatchSize = persistFreqBatchSize;
	this.nbrOfAbstrToProcess = nbrOfAbstrToProcess;
	this.rptIntervalAbstrProcessed = rptIntervalAbstrProcessed;
	this.rptIntervalFreqPersisted = rptIntervalFreqPersisted;
	this.connPool = connPool;
	nbrOfProcessors = Runtime.getRuntime().availableProcessors();
	pool = Executors.newFixedThreadPool(nbrOfProcessors *= 3);
	svc = new ExecutorCompletionService<CorporaCondFreqResult>(pool);
    }

    public void run() throws Exception {

	batchAccumulatorSize = threadPoolBatchSize * nbrPmidsPerCallable;

	try (Connection conn = connPool.getConnection()) {
	    String getPmidsSql = null;
	    if (nbrOfAbstrToProcess == -1) {
		getPmidsSql = "select pmid from conceptrecogn.nlmabstractwords_pmids "
			+ "where isWordsCounted = 0 order by pmid";

	    } else {
		getPmidsSql = String.format(
			"select pmid from conceptrecogn.nlmabstractwords_pmids "
				+ "where isWordsCounted = 0 order by pmid limit %1$d ",
			nbrOfAbstrToProcess);

	    }
	    String getMaxPmidSql = "select max(pmid), min(pmid) from conceptrecogn.nlmabstractwords_pmids "
		    + " where isWordsCounted = 0";
	    try (PreparedStatement getMaxPmidStmt = conn
		    .prepareStatement(getMaxPmidSql);
		    PreparedStatement getPmidsStmt = conn
			    .prepareStatement(getPmidsSql)) {
		try (ResultSet rs = getMaxPmidStmt.executeQuery()) {
		    if (rs.next()) {
			maxPmid = rs.getInt(1);
			minPmid = rs.getInt(2);
		    }
		}
		System.out.printf("Min PMID = %1$d. %n%n", minPmid);
		System.out.printf("SQL to marshal PMIDs = %1$s %n%n",
			getPmidsSql);
		try (ResultSet rs = getPmidsStmt.executeQuery()) {

		    boolean isLastPmid = false;
		    while (rs.next() && !isLastPmid) {

			int pmid = rs.getInt(1);
			abstrCount++;
			isLastPmid = abstrCount == nbrOfAbstrToProcess
				|| pmid == maxPmid;

			if (batchPmids.size() < batchAccumulatorSize) {
			    batchPmids.add(pmid);
			}

			int currentBatchSize = batchPmids.size();
			if (currentBatchSize == batchAccumulatorSize
				|| isLastPmid) {

			    chunkThreadBatchPmids();
			    for (int i = 0; i < threadBatchPmids.size(); i++) {
				IntArrayList threadPmidBatch = threadBatchPmids
					.get(i);
				CorporaCondFreqCallable callable = new CorporaCondFreqCallable(
					threadPmidBatch, connPool);
				svc.submit(callable);
			    }

			    batchPmids = new IntArrayList();

			    for (int i = 0; i < threadBatchPmids.size(); i++) {
				Future<CorporaCondFreqResult> future = svc
					.take();
				CorporaCondFreqResult result = future.get();

				for (Integer predWordId : result.freqByPredFact
					.keySet()) {
				    if (!freqByPredFact
					    .containsKey(predWordId)) {
					freqByPredFact.put(predWordId,
						new HashMap<Integer, Long>());
				    }
				    for (Integer factWordId : result.freqByPredFact
					    .get(predWordId).keySet()) {
					if (predWordId == factWordId) {
					    continue;
					}
					Long deltaFreq = result.freqByPredFact
						.get(predWordId)
						.get(factWordId);
					if (freqByPredFact.get(predWordId)
						.containsKey(factWordId)) {
					    Long newFreq = freqByPredFact
						    .get(predWordId)
						    .get(factWordId)
						    + deltaFreq;
					    freqByPredFact.get(predWordId)
						    .put(factWordId, newFreq);
					} else {
					    freqByPredFact.get(predWordId)
						    .put(factWordId, deltaFreq);
					    nbrCondFreqToPersist++;
					}
				    }
				}

				pmidsProcessed.addAllOf(result.threadPmidBatch);
				nbrPmidsProcessed = pmidsProcessed.size();
				if (nbrPmidsProcessed
					% rptIntervalAbstrProcessed == 0) {
				    System.out.printf(
					    "Processed %1$,d PMIDs. %n",
					    nbrPmidsProcessed);
				}
			    }
			}
		    }
		}
	    }
	}

	/*
	 * Persist frequencies into conditional probabilities table
	 * (corporacondprob)
	 */
	System.out.printf("Now persisting %1$,d conditional frequencies. %n",
		nbrCondFreqToPersist);

	try (Connection conn = connPool.getConnection()) {

	    String marshalFreqSql = "SELECT freq FROM conceptrecogn.corporacondwordprob "
		    + "WHERE predicateWordId = ? AND assumedFactWordId = ?";
	    String insertFreqSql = "INSERT INTO conceptrecogn.corporacondwordprob "
		    + "(predicateWordId, assumedFactWordId, freq) VALUES(?,?,?)";
	    String updateFreqSql = "UPDATE conceptrecogn.corporacondwordprob "
		    + "SET freq = ? WHERE predicateWordId = ? AND assumedFactWordId = ?";

	    try (PreparedStatement marshalFreqStmt = conn
		    .prepareStatement(marshalFreqSql);
		    PreparedStatement insertFreqStmt = conn
			    .prepareStatement(insertFreqSql);
		    PreparedStatement updateFreqStmt = conn
			    .prepareStatement(updateFreqSql)) {

		int nbrInUpdateBatch = 0;
		int nbrInPersistBatch = 0;

		for (Integer predWordId : freqByPredFact.keySet()) {
		    for (Integer factWordId : freqByPredFact.get(predWordId)
			    .keySet()) {

			if (predWordId == factWordId) {
			    continue;
			}

			// see if frequency value already in database
			marshalFreqStmt.setInt(1, predWordId);
			marshalFreqStmt.setInt(2, factWordId);
			long priorFreq = 0;
			boolean isUpdate = false;
			try (ResultSet rs = marshalFreqStmt.executeQuery()) {
			    if (rs.next()) {
				priorFreq = rs.getLong(1);
				isUpdate = true;
			    }
			}

			long freq = freqByPredFact.get(predWordId)
				.get(factWordId);

			if (isUpdate) {
			    updateFreqStmt.setLong(1, priorFreq + freq);
			    updateFreqStmt.setInt(2, predWordId);
			    updateFreqStmt.setInt(3, factWordId);
			    updateFreqStmt.addBatch();
			    nbrInUpdateBatch++;
			    if (nbrInUpdateBatch % persistFreqBatchSize == 0) {
				updateFreqStmt.executeBatch();
				nbrInUpdateBatch = 0;
			    }
			} else {
			    insertFreqStmt.setInt(1, predWordId);
			    insertFreqStmt.setInt(2, factWordId);
			    insertFreqStmt.setLong(3, freq);
			    insertFreqStmt.addBatch();
			    nbrInPersistBatch++;
			    if (nbrInPersistBatch % persistFreqBatchSize == 0) {
				insertFreqStmt.executeBatch();
				nbrInPersistBatch = 0;
			    }
			}
			nbrCondFreqPersisted++;
			if (nbrCondFreqPersisted
				% rptIntervalFreqPersisted == 0) {
			    System.out.printf(
				    "Persisted or updated %1$,d conditional frequencies %n",
				    nbrCondFreqPersisted);
			}
		    }
		    // clean up stragglers
		    updateFreqStmt.executeBatch();
		    insertFreqStmt.executeBatch();

		}
	    }
	}

	System.out.printf(
		"%n%nFINISHED - Processed grand total of %1$,d PMIDs. %n",
		nbrPmidsProcessed);

	System.out.printf(
		"%nPersisted or updated total of %1$,d co-occurrence frequencies.%n",
		nbrCondFreqPersisted);

	System.out.printf(
		"%nNow updating flag in corpora indicating document words were counted.%n");
	try (Connection conn = connPool.getConnection()) {
	    String updateFlagSql = "update conceptrecogn.nlmabstractwords_pmids "
		    + "set isWordsCounted = 1 where pmid = ?";
	    try (PreparedStatement updateFlagStmt = conn
		    .prepareStatement(updateFlagSql)) {
		for (int i = 0; i < pmidsProcessed.size(); i++) {
		    int processedPmid = pmidsProcessed.get(i);
		    updateFlagStmt.setInt(1, processedPmid);
		    updateFlagStmt.executeUpdate();
		}
	    }
	}
	System.out.printf(
		"Set flag 'isWordsCounted' to 'true' for %1$,d abstracts.%n",
		pmidsProcessed.size());

    }

    private void chunkThreadBatchPmids() {
	threadBatchPmids.clear();
	IntArrayList currentThreadBatch = new IntArrayList();
	for (int i = 0; i < batchPmids.size(); i++) {
	    Integer pmid = batchPmids.get(i);
	    currentThreadBatch.add(pmid);
	    if (currentThreadBatch.size() == nbrPmidsPerCallable
		    || i + 1 == batchPmids.size()) {
		IntArrayList pmids = new IntArrayList(
			currentThreadBatch.size());
		pmids.addAllOf(currentThreadBatch);
		threadBatchPmids.add(pmids);
		currentThreadBatch = new IntArrayList();
	    }
	}
    }

    protected void shutdownAndAwaitTermination(ExecutorService pool) {
	// pool.shutdown(); // Disable new tasks from being submitted
	pool.shutdownNow();
	try {
	    // Wait a while for existing tasks to terminate
	    if (!pool.awaitTermination(5, TimeUnit.SECONDS)) {
		pool.shutdownNow(); // Cancel currently executing tasks
		// Wait a while for tasks to respond to being cancelled
		if (!pool.awaitTermination(15, TimeUnit.SECONDS))
		    System.err.println("Pool did not terminate");
	    }
	} catch (InterruptedException ie) {
	    // (Re-)Cancel if current thread also interrupted
	    pool.shutdownNow();
	    // Preserve interrupt status
	    Thread.currentThread().interrupt();
	}
    }

    @Override
    public void close() throws Exception {
	this.shutdownAndAwaitTermination(pool);
	System.out.println("Parallel processor closed");
    }

    public static int nbrRemainingPmids(JdbcConnectionPool connPool)
	    throws SQLException {
	System.out.printf("%nCounting remaining PMIDs to process%n");
	int nbrPmidsCounted = 0;
	int nbrRemainingPmids = 0;
	try (Connection conn = connPool.getConnection()) {
	    String nbrPmidsCountedSql = "select count(*) as nbrPmidsCounted from conceptrecogn.nlmabstractwords_pmids where isWordsCounted = 1";
	    String nbrPmidsNotCountedSql = "select count(*) as nbrPmidsCounted from conceptrecogn.nlmabstractwords_pmids where isWordsCounted = 0";
	    try (PreparedStatement nbrRemainingStmt = conn
		    .prepareStatement(nbrPmidsCountedSql);
		    PreparedStatement nbrNotCountedStmt = conn
			    .prepareStatement(nbrPmidsNotCountedSql)) {
		try (ResultSet rs = nbrRemainingStmt.executeQuery()) {
		    if (rs.next()) {
			nbrPmidsCounted = rs.getInt(1);
		    }
		}
		try (ResultSet rs = nbrNotCountedStmt.executeQuery()) {
		    if (rs.next()) {
			nbrRemainingPmids = rs.getInt(1);
		    }
		}
	    }
	}
	System.out.printf("Nbr PMIDs counted = %1$,d%n", nbrPmidsCounted);
	return nbrRemainingPmids;
    }

    public static void main(String[] args) {

	try {

	    JdbcConnectionPool connPool = new JdbcConnectionPool(
		    ConnDbName.CONCEPT_RECOGN);
	    int threadsPoolBatchSize = 24;
	    int nbrPmidsPerCallable = 200;
	    int persistFreqBatchSize = 200000;
	    int nbrOfAbstrToProcess = 200000;
	    int rptIntervalAbstrProcessed = 20000;
	    int rptIntervalFreqPersisted = 200000;

	    boolean isDeleteExisting = true;

	    if (isDeleteExisting) {
		System.out.println(
			"Deleting existing corpora conditional probabilities.");
		CorporaCondWordProbDao.deleteAll(connPool);
		NlmAbstractDao.resetIsWordsCounted(connPool);
		System.out.println(
			"Existing corpora conditional probabilities deleted");
		System.out.println();
	    }

	    int nbrRemainingPmids = CorporaCondFreqWorker
		    .nbrRemainingPmids(connPool);
	    while (nbrRemainingPmids > 0) {
		System.out.printf("Number of remaining PMIDs = %1$,d. %n",
			nbrRemainingPmids);
		System.out.printf(
			"%nProcessing next batch of %1$,d PMIDs. %n%n",
			nbrOfAbstrToProcess);
		try (CorporaCondFreqWorker worker = new CorporaCondFreqWorker(
			threadsPoolBatchSize, nbrPmidsPerCallable,
			persistFreqBatchSize, nbrOfAbstrToProcess,
			rptIntervalAbstrProcessed, rptIntervalFreqPersisted,
			connPool)) {
		    worker.run();
		}
		nbrRemainingPmids = CorporaCondFreqWorker
			.nbrRemainingPmids(connPool);
	    }

	} catch (Exception ex) {
	    ex.printStackTrace();
	}

    }

}

package edu.mst.db.nlm.corpora.condprob.persist;

import java.util.HashMap;
import java.util.Map;

import cern.colt.list.IntArrayList;

public class CorporaCondFreqResult {

    public Map<Integer, Map<Integer, Long>> freqByPredFact = new HashMap<Integer, Map<Integer, Long>>();
    public IntArrayList threadPmidBatch = null;

}

package edu.mst.db.nlm.corpora.condprob.persist;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Set;
import java.util.concurrent.Callable;

import edu.mst.db.condprob.CondWordProb;
import edu.mst.db.condprob.CondWordProbDao;
import edu.mst.db.condprob.CondWordProbType;
import edu.mst.db.lexicon.LexWordVariantDao;
import edu.mst.db.lexicon.StopWords;
import edu.mst.db.util.JdbcConnectionPool;

public class CorporaCondWordProbVariantsCallable implements Callable<Integer> {

    private CondWordProb condProb = null;
    private CondWordProbType type = null;
    private boolean isPersist = false;
    private int persistBatchSize = 0;
    private StopWords stopWords = null;
    private JdbcConnectionPool connPool = null;

    private LexWordVariantDao lexVariantDao = new LexWordVariantDao();
    private CondWordProbDao condProbDao = new CondWordProbDao();

    private int nbrNewCondProbs = 0;

    @Deprecated
    public CorporaCondWordProbVariantsCallable(CondWordProb condProb,
	    CondWordProbType type, boolean isPersist, int persistBatchSize,
	    StopWords stopWords, JdbcConnectionPool connPool) {
	this.condProb = condProb;
	this.type = type;
	this.isPersist = isPersist;
	this.persistBatchSize = persistBatchSize;
	this.stopWords = stopWords;
	this.connPool = connPool;
    }

    @Override
    public Integer call() throws Exception {

	int originalPredWordId = condProb.getOrigPredWordId();
	int originalAssumedFactId = condProb.getOrigAssumedFactWordId();

	int[] predVariants = null;
	{
	    Set<Integer> variantsSet = lexVariantDao
		    .marshalVariantIdsForLexWord(originalPredWordId, connPool);
	    variantsSet.add(originalPredWordId);
	    predVariants = new int[variantsSet.size()];
	    int pos = 0;
	    for (int predVariantId : variantsSet) {
		predVariants[pos] = predVariantId;
		pos++;
	    }
	}
	int[] assumedFactVariants = null;
	{
	    Set<Integer> variantsSet = lexVariantDao
		    .marshalVariantIdsForLexWord(originalAssumedFactId,
			    connPool);
	    variantsSet.add(originalAssumedFactId);
	    assumedFactVariants = new int[variantsSet.size()];
	    int pos = 0;
	    for (int variantId : variantsSet) {
		assumedFactVariants[pos] = variantId;
		pos++;
	    }
	}

	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement persistStmt = conn
		    .prepareStatement(condProbDao.getPersistSql(type))) {
		for (int i = 0; i < predVariants.length; i++) {
		    int predVariant = predVariants[i];
		    if (stopWords.getLexWordIds().contains(predVariant)) {
			continue;
		    }
		    for (int j = 0; j < assumedFactVariants.length; j++) {
			int assumedFactVariant = assumedFactVariants[j];
			/*
			 * Conditions: is not stop word, assumed fact and
			 * predicate are not the same word, and variant words
			 * must not be the same as the originals
			 */
			if (stopWords.getLexWordIds()
				.contains(assumedFactVariant)
				|| assumedFactVariant == predVariant
				|| (predVariant == originalPredWordId
					&& assumedFactVariant == originalAssumedFactId)) {
			    continue;
			}
			/*
			 * 1-origPredWordId, 2-origAssumedFactWordId,
			 * 3-variantPredWordId, 4-variantAssumedFactWordId,
			 * 5-freq, 6-condProb, 7-lnCondProb
			 */
			persistStmt.setInt(1, originalPredWordId);
			persistStmt.setInt(2, originalAssumedFactId);
			persistStmt.setInt(3, predVariant);
			persistStmt.setInt(4, assumedFactVariant);
			persistStmt.setInt(5, condProb.getFreq());
			persistStmt.setDouble(6, condProb.getCondProb());
			persistStmt.setDouble(7, condProb.getLnCondProb());

			persistStmt.addBatch();
			nbrNewCondProbs++;
			if (isPersist
				&& nbrNewCondProbs % persistBatchSize == 0) {
			    persistStmt.executeBatch();
			}
		    }
		}
		// pick up any stragglers
		persistStmt.executeBatch();
	    }
	}

	return nbrNewCondProbs;
    }

}

/**
 * 
 */
package edu.mst.db.nlm.corpora.condprob;

/**
 * Conditional probability for two words existing in same sentence in NLM
 * articles.
 * <p>
 * NOT the same as conditional probabilities for concepts in the ontology.
 * 
 * @author gjs
 *
 */
public class CorporaCondWordProb
	implements Comparable<CorporaCondWordProb> {

    private int predicateWordId = 0;
    private int assumedFactWordId = 0;
    private long freq = 0;
    private double condProb = 0.0d;
    private double lnCondProb = 0.0d;

    @Override
    public int compareTo(CorporaCondWordProb condProb) {
	Integer result = Integer.compare(predicateWordId,
		condProb.predicateWordId);
	if (result != 0) {
	    return result;
	}
	return Integer.compare(assumedFactWordId, condProb.assumedFactWordId);
    }

    @Override
    public boolean equals(Object obj) {
	if (obj instanceof CorporaCondWordProb) {
	    CorporaCondWordProb condProb = (CorporaCondWordProb) obj;
	    return predicateWordId == condProb.predicateWordId
		    && assumedFactWordId == condProb.assumedFactWordId;
	}
	return false;
    }

    @Override
    public int hashCode() {
	return Integer.hashCode(predicateWordId)
		+ 7 * Integer.hashCode(assumedFactWordId);
    }
    
    public void addFreq(int additionalFreq) {
	this.freq += additionalFreq;
    }

    public int getPredicateWordId() {
        return predicateWordId;
    }

    public void setPredicateWordId(int predicateWordId) {
        this.predicateWordId = predicateWordId;
    }

    public int getAssumedFactWordId() {
        return assumedFactWordId;
    }

    public void setAssumedFactWordId(int assumedFactWordId) {
        this.assumedFactWordId = assumedFactWordId;
    }

    public long getFreq() {
        return freq;
    }

    public void setFreq(long freq) {
        this.freq = freq;
    }

    public double getCondProb() {
        return condProb;
    }

    public void setCondProb(double condProb) {
        this.condProb = condProb;
    }

    public double getLnCondProb() {
        return lnCondProb;
    }

    public void setLnCondProb(double lnCondProb) {
        this.lnCondProb = lnCondProb;
    }

}

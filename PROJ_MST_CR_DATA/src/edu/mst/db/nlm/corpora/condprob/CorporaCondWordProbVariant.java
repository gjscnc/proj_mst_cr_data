/**
 * 
 */
package edu.mst.db.nlm.corpora.condprob;

import java.util.Comparator;

/**
 * @author gjs
 *
 */
public class CorporaCondWordProbVariant implements Comparable<CorporaCondWordProbVariant>{

    private int origPredWordId = 0;
    private int origAssumedFactWordId = 0;
    private int variantPredWordId = 0;
    private int variantAssumedFactWordId = 0;
    private int freq = 0;
    private double condProb = 0.0d;
    private double lnCondProb = 0.0d;

    public static class Comparators {
	public static final Comparator<CorporaCondWordProbVariant> byOrigAssumedFactWord = (
		CorporaCondWordProbVariant condProb1,
		CorporaCondWordProbVariant condProb2) -> Integer.compare(
			condProb1.origAssumedFactWordId,
			condProb2.origAssumedFactWordId);
	public static final Comparator<CorporaCondWordProbVariant> byVariantAssumedFactWord = (
		CorporaCondWordProbVariant condProb1,
		CorporaCondWordProbVariant condProb2) -> Integer.compare(
			condProb1.variantAssumedFactWordId,
			condProb2.variantAssumedFactWordId);
	public static final Comparator<CorporaCondWordProbVariant> byOrigPredWord = (
		CorporaCondWordProbVariant condProb1,
		CorporaCondWordProbVariant condProb2) -> Integer.compare(
			condProb1.origPredWordId, condProb2.origPredWordId);
	public static final Comparator<CorporaCondWordProbVariant> byVariantPredWord = (
		CorporaCondWordProbVariant condProb1,
		CorporaCondWordProbVariant condProb2) -> Integer.compare(
			condProb1.variantPredWordId,
			condProb2.variantPredWordId);
	/**
	 * This is the primary key
	 */
	public static final Comparator<CorporaCondWordProbVariant> byOrigPred_OrigAssumedFact_VarPred_VarAssumedFact = (
		CorporaCondWordProbVariant condProb1,
		CorporaCondWordProbVariant condProb2) -> byOrigPredWord
			.thenComparing(byOrigAssumedFactWord)
			.thenComparing(byVariantPredWord)
			.thenComparing(byVariantAssumedFactWord)
			.compare(condProb1, condProb2);
	public static final Comparator<CorporaCondWordProbVariant> ByCondProbDesc = (
		CorporaCondWordProbVariant condProb1,
		CorporaCondWordProbVariant condProb2) -> Double
			.compare(condProb2.condProb, condProb1.condProb);
	public static final Comparator<CorporaCondWordProbVariant> ByLnCondProbDesc = (
		CorporaCondWordProbVariant condProb1,
		CorporaCondWordProbVariant condProb2) -> Double
			.compare(condProb2.lnCondProb, condProb1.lnCondProb);
    }
    
    @Override
    public boolean equals(Object obj) {
	if (obj instanceof CorporaCondWordProbVariant) {
	    CorporaCondWordProbVariant condProb = (CorporaCondWordProbVariant) obj;
	    return origPredWordId == condProb.origPredWordId
		    && origAssumedFactWordId == condProb.origAssumedFactWordId
		    && variantPredWordId == condProb.variantPredWordId
		    && variantAssumedFactWordId == condProb.variantAssumedFactWordId;
	}
	return false;
    }

    @Override
    public int compareTo(CorporaCondWordProbVariant condProb) {
	return Comparators.byOrigPred_OrigAssumedFact_VarPred_VarAssumedFact
		.compare(this, condProb);
    }

    @Override
    public int hashCode() {
	return Integer.hashCode(origPredWordId)
		+ 7 * Integer.hashCode(origAssumedFactWordId)
		+ 31 * Integer.hashCode(variantPredWordId)
		+ 47 * Integer.hashCode(variantAssumedFactWordId);
    }

    public int getOrigPredWordId() {
        return origPredWordId;
    }

    public void setOrigPredWordId(int origPredWordId) {
        this.origPredWordId = origPredWordId;
    }

    public int getOrigAssumedFactWordId() {
        return origAssumedFactWordId;
    }

    public void setOrigAssumedFactWordId(int origAssumedFactWordId) {
        this.origAssumedFactWordId = origAssumedFactWordId;
    }

    public int getVariantPredWordId() {
        return variantPredWordId;
    }

    public void setVariantPredWordId(int variantPredWordId) {
        this.variantPredWordId = variantPredWordId;
    }

    public int getVariantAssumedFactWordId() {
        return variantAssumedFactWordId;
    }

    public void setVariantAssumedFactWordId(int variantAssumedFactWordId) {
        this.variantAssumedFactWordId = variantAssumedFactWordId;
    }

    public int getFreq() {
        return freq;
    }

    public void setFreq(int freq) {
        this.freq = freq;
    }

    public double getCondProb() {
        return condProb;
    }

    public void setCondProb(double condProb) {
        this.condProb = condProb;
    }

    public double getLnCondProb() {
        return lnCondProb;
    }

    public void setLnCondProb(double lnCondProb) {
        this.lnCondProb = lnCondProb;
    }



}

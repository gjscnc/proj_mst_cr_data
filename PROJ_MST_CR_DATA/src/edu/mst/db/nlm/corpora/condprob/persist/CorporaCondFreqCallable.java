package edu.mst.db.nlm.corpora.condprob.persist;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.SortedSet;
import java.util.concurrent.Callable;

import cern.colt.list.IntArrayList;
import edu.mst.db.nlm.corpora.NlmAbstractSentence;
import edu.mst.db.nlm.corpora.NlmAbstractWord;
import edu.mst.db.nlm.corpora.NlmAbstractWordDao;
import edu.mst.db.util.JdbcConnectionPool;

public class CorporaCondFreqCallable
	implements Callable<CorporaCondFreqResult> {

    private IntArrayList threadPmidBatch = null;
    private JdbcConnectionPool connPool = null;
    private NlmAbstractWordDao wordDao = new NlmAbstractWordDao();

    public CorporaCondFreqCallable(IntArrayList threadPmidBatch,
	    JdbcConnectionPool connPool) {
	this.threadPmidBatch = threadPmidBatch;
	this.connPool = connPool;
    }

    @Override
    public CorporaCondFreqResult call() throws Exception {

	CorporaCondFreqResult result = new CorporaCondFreqResult();
	result.threadPmidBatch = threadPmidBatch;

	for (int pmidPos = 0; pmidPos < threadPmidBatch.size(); pmidPos++) {
	    int pmid = threadPmidBatch.get(pmidPos);
	    SortedSet<NlmAbstractSentence> sentences = wordDao.marshalSentences(pmid,
		    connPool);
	    for (NlmAbstractSentence sent : sentences) {

		SortedSet<NlmAbstractWord> sortedWordSet = sent.getWords();
		List<NlmAbstractWord> words = new ArrayList<NlmAbstractWord>();
		Iterator<NlmAbstractWord> wordSetIter = sortedWordSet.iterator();
		while(wordSetIter.hasNext()) {
		    words.add(wordSetIter.next());
		}

		for (int predWordPos = words.size()
			- 1; predWordPos >= 1; predWordPos--) {

		    NlmAbstractWord predWord = words.get(predWordPos);
		    Integer predWordId = predWord.getLexWordId();

		    for (int factWordPos = predWordPos
			    - 1; factWordPos >= 0; factWordPos--) {

			NlmAbstractWord factWord = words.get(factWordPos);
			Integer factWordId = factWord.getLexWordId();
			if (factWordId == predWordId) {
			    continue;
			}
			if (!result.freqByPredFact.containsKey(predWordId)) {
			    result.freqByPredFact.put(predWordId,
				    new HashMap<Integer, Long>());
			}
			if (!result.freqByPredFact.get(predWordId)
				.containsKey(factWordId)) {
			    result.freqByPredFact.get(predWordId)
				    .put(factWordId, 1L);
			} else {
			    Long currentFreq = result.freqByPredFact
				    .get(predWordId).get(factWordId);
			    currentFreq += 1;
			    result.freqByPredFact.get(predWordId)
				    .put(factWordId, currentFreq);
			}
		    }
		}
	    }
	}

	return result;
    }

}

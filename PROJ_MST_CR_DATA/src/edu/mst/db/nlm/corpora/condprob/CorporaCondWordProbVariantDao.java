/**
 * 
 */
package edu.mst.db.nlm.corpora.condprob;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;

import edu.mst.db.util.JdbcConnectionPool;

/**
 * @author gjs
 *
 */
public class CorporaCondWordProbVariantDao {

    public static final String allFields = "origPredWordId, origAssumedFactWordId, variantPredWordId, variantAssumedFactWordId, "
	    + "freq, condProb, lnCondProb";

    public static final String tableName = "conceptrecogn.corporacondwordprob_variants";

    public static final String deleteExistingSql = "truncate table "
	    + tableName;

    public static final String persistSql = "insert into " + tableName + "("
	    + allFields + ") values(?,?,?,?,?,?,?)";

    public static final String marshalSql = "select " + allFields + " from "
	    + tableName + " where origPredWordId=? and origAssumedFactWordId=? "
	    + "and variantPredWordId=? and variantAssumedFactWordId=?";

    /*
     * 1-origPredWordId, 2-origAssumedFactWordId, 3-variantPredWordId,
     * 4-variantAssumedFactWordId, 5-freq, 6-condProb, 7-lnCondProb
     */

    public static void deleteExisting(JdbcConnectionPool connPool)
	    throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement deleteStmt = conn
		    .prepareStatement(deleteExistingSql)) {
		deleteStmt.executeUpdate();
	    }
	}
    }

    public void persist(CorporaCondWordProbVariant condProb,
	    JdbcConnectionPool connPool) throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement persistStmt = conn
		    .prepareStatement(persistSql)) {
		setPersistValues(condProb, persistStmt);
		persistStmt.executeUpdate();
	    }
	}
    }

    public void persistBatch(Collection<CorporaCondWordProbVariant> condProbs,
	    JdbcConnectionPool connPool) throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement persistStmt = conn
		    .prepareStatement(persistSql)) {
		for (CorporaCondWordProbVariant condProb : condProbs) {
		    setPersistValues(condProb, persistStmt);
		    persistStmt.addBatch();
		}
		persistStmt.executeBatch();
	    }
	}
    }

    public void setPersistValues(CorporaCondWordProbVariant condProb,
	    PreparedStatement persistStmt) throws SQLException {
	/*
	 * 1-origPredWordId, 2-origAssumedFactWordId, 3-variantPredWordId,
	 * 4-variantAssumedFactWordId, 5-freq, 6-condProb, 7-lnCondProb
	 */
	persistStmt.setInt(1, condProb.getOrigPredWordId());
	persistStmt.setInt(2, condProb.getOrigAssumedFactWordId());
	persistStmt.setInt(3, condProb.getVariantPredWordId());
	persistStmt.setInt(4, condProb.getVariantAssumedFactWordId());
	persistStmt.setInt(5, condProb.getFreq());
	persistStmt.setDouble(6, condProb.getCondProb());
	persistStmt.setDouble(7, condProb.getLnCondProb());
    }

    public CorporaCondWordProbVariant marshal(int origPredWordId,
	    int origAssumedFactWordId, int variantPredWordId,
	    int variantAssumedFactWordId, JdbcConnectionPool connPool)
	    throws SQLException {
	CorporaCondWordProbVariant condProb = null;
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement marshalStmt = conn
		    .prepareStatement(marshalSql)) {
		marshalStmt.setInt(1, origPredWordId);
		marshalStmt.setInt(2, origAssumedFactWordId);
		marshalStmt.setInt(3, variantPredWordId);
		marshalStmt.setInt(4, variantAssumedFactWordId);
		try (ResultSet rs = marshalStmt.executeQuery()) {
		    if (rs.next()) {
			condProb = instantiateFromResultSet(rs);
		    }
		}
	    }
	}
	return condProb;
    }

    private CorporaCondWordProbVariant instantiateFromResultSet(ResultSet rs)
	    throws SQLException {
	/*
	 * 1-origPredWordId, 2-origAssumedFactWordId, 3-variantPredWordId,
	 * 4-variantAssumedFactWordId, 5-freq, 6-condProb, 7-lnCondProb
	 */
	CorporaCondWordProbVariant condProb = new CorporaCondWordProbVariant();
	condProb.setOrigPredWordId(rs.getInt(1));
	condProb.setOrigAssumedFactWordId(rs.getInt(2));
	condProb.setVariantPredWordId(rs.getInt(3));
	condProb.setVariantAssumedFactWordId(rs.getInt(4));
	condProb.setFreq(rs.getInt(5));
	condProb.setCondProb(rs.getDouble(6));
	condProb.setLnCondProb(rs.getDouble(7));
	return condProb;
    }

}

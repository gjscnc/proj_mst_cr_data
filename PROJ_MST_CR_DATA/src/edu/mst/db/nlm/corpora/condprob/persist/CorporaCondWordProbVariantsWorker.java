/**
 * 
 */
package edu.mst.db.nlm.corpora.condprob.persist;

import java.util.Properties;

import edu.mst.concurrent.BaseWorker;
import edu.mst.db.condprob.CondWordProb;
import edu.mst.db.condprob.CondWordProbDao;
import edu.mst.db.condprob.CondWordProbType;
import edu.mst.db.lexicon.StopWords;
import edu.mst.db.util.CondProbProperties;
import edu.mst.db.util.ConfigPropCondProbNames;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * @author gjs
 *
 */
public class CorporaCondWordProbVariantsWorker extends BaseWorker<Integer> {

    private int maxNbrToProcess = 0;
    private boolean isPersist = false;
    private int persistBatchSize = 0;
    private boolean isDeleteExisting = false;
    
    private StopWords stopWords = null;
    private Properties condProbProps = null;

    private CondWordProbType type = CondWordProbType.CORPORA_VARIANT;
    private CondWordProb[] condProbs = null;
    private int nbrCondProbs = 0;
    private int minPredWordId = 0;
    private int maxPredWordIdToProcess = 0;

    private int nbrCondProbsProcessed = 0;
    private int nbrNewCondProbs = 0;
    private int rptInterval = 10000;

    @Deprecated
    public CorporaCondWordProbVariantsWorker(int maxNbrToProcess,
	    boolean isPersist, int persistBatchSize, boolean isDeleteExisting,
	    JdbcConnectionPool connPool) {
	super(connPool);
	this.maxNbrToProcess = maxNbrToProcess;
	this.isPersist = isPersist;
	this.persistBatchSize = persistBatchSize;
	this.isDeleteExisting = isDeleteExisting;
    }

    @Override
    public void run() throws Exception {

	if(isDeleteExisting) {
	    System.out.println("Deleting existing conditional probabilities");
	    CondWordProbDao condProbDao = new CondWordProbDao();
	    condProbDao.deleteExisting(type, connPool);
	    minPredWordId = 1;
	} else {
	    condProbProps = CondProbProperties.getProperties();
	    String lastPredWordIdStr = condProbProps.getProperty(
		    ConfigPropCondProbNames.lastCorporaCondWordProbPredId
			    .toString());
	    minPredWordId = Integer.valueOf(lastPredWordIdStr).intValue() + 1;
	}

	{
	    System.out.println("Marshaling conditional probabilities");
	    CondWordProbDao condProbDao = new CondWordProbDao();
	    condProbs = condProbDao.marshalSubsetAsArray(type, minPredWordId,
		    maxNbrToProcess, connPool);
	    nbrCondProbs = condProbs.length;
	    maxPredWordIdToProcess = condProbs[nbrCondProbs - 1]
		    .getOrigPredWordId();
	    System.out.printf("Marshaled %1$,d conditional probabilities. %n",
		    nbrCondProbs);
	}

	System.out.println("Now generating conditional probability variants");
	
	stopWords = new StopWords(connPool);

	while (currentPos < nbrCondProbs) {

	    maxPos = getNextBatchMaxPos();
	    startPos = currentPos;

	    for (int i = startPos; i <= maxPos; i++) {
		CondWordProb condProb = condProbs[i];
		CorporaCondWordProbVariantsCallable callable = new CorporaCondWordProbVariantsCallable(
			condProb, type, isPersist, persistBatchSize, stopWords, connPool);
		svc.submit(callable);
	    }

	    for (int i = startPos; i <= maxPos; i++) {
		nbrNewCondProbs += svc.take().get();
		nbrCondProbsProcessed++;
		if (nbrCondProbsProcessed % rptInterval == 0) {
		    System.out.printf(
			    "Processed %1$,d conditional probabilities, "
				    + "generating %2$,d new conditional probabilities for word variants. %n",
			    nbrCondProbsProcessed, nbrNewCondProbs);
		}
	    }

	    currentPos = maxPos + 1;

	}

	condProbProps
		.setProperty(
			ConfigPropCondProbNames.lastCorporaCondWordProbPredId
				.toString(),
			String.valueOf(maxPredWordIdToProcess));
	CondProbProperties.updateProperties(condProbProps);

	System.out.printf("Processed total of %1$,d conditional probabilities, "
		+ "generating %2$,d new conditional probabilities for word variants. %n",
		nbrCondProbsProcessed, nbrNewCondProbs);
    }

    @Override
    protected int getNextBatchMaxPos() throws Exception {
	int maxPos = currentPos + threadCallablesBatchSize - 1;
	if (maxPos >= nbrCondProbs - 1) {
	    maxPos = nbrCondProbs - 1;
	}
	return maxPos;
    }

}

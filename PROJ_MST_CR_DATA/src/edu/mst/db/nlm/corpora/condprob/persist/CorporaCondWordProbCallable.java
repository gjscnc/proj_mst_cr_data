package edu.mst.db.nlm.corpora.condprob.persist;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.concurrent.Callable;

import edu.mst.db.nlm.corpora.condprob.CorporaCondWordPredFreq;
import edu.mst.db.nlm.corpora.condprob.CorporaCondWordPredFreqDao;
import edu.mst.db.nlm.corpora.condprob.CorporaCondWordProbDao;
import edu.mst.db.util.JdbcConnectionPool;

public class CorporaCondWordProbCallable implements Callable<Integer> {

    private int predicateWordId = 0;
    private boolean isPersist = false;
    private int persistBatchSize = 0;
    private JdbcConnectionPool connPool = null;
    private CorporaCondWordPredFreqDao predFreqDao = new CorporaCondWordPredFreqDao();
    private int nbrInBatch = 0;

    public CorporaCondWordProbCallable(int predicateWordId, boolean isPersist,
	    int persistBatchSize, JdbcConnectionPool connPool) {
	this.predicateWordId = predicateWordId;
	this.isPersist = isPersist;
	this.persistBatchSize = persistBatchSize;
	this.connPool = connPool;
    }

    @Override
    public Integer call() throws Exception {

	Integer condProbCount = 0;

	CorporaCondWordPredFreq predFreq = predFreqDao.marshal(predicateWordId, connPool);
	if(predFreq == null) {
	    return 0;
	}
	
	long sumFreqForPred = predFreq.getTotalFreq();
	
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement marshalStmt = conn
		    .prepareStatement(CorporaCondWordProbDao.marshalForPredSql);
		    PreparedStatement updateStmt = conn.prepareStatement(
			    CorporaCondWordProbDao.updateCondProbSql)) {
		marshalStmt.setInt(1, predicateWordId);
		try (ResultSet rs = marshalStmt.executeQuery()) {
		    while (rs.next()) {
			int assumedFactWordId = rs.getInt(1);
			int freq = rs.getInt(2);
			double condProb = (double) freq
				/ (double) sumFreqForPred;
			double lnCondProb = Math.log(condProb);
			condProbCount++;
			if (isPersist) {
			    updateStmt.setDouble(1, condProb);
			    updateStmt.setDouble(2, lnCondProb);
			    updateStmt.setInt(3, predicateWordId);
			    updateStmt.setInt(4, assumedFactWordId);
			    updateStmt.addBatch();
			    nbrInBatch++;
			    if (nbrInBatch % persistBatchSize == 0) {
				updateStmt.executeBatch();
				nbrInBatch = 0;
			    }
			}
		    }
		}
		// stragglers
		if (isPersist) {
		    updateStmt.executeBatch();
		}
	    }
	}

	return condProbCount;
    }

}

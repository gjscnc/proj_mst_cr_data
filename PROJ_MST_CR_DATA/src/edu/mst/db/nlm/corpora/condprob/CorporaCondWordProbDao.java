package edu.mst.db.nlm.corpora.condprob;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import cern.colt.list.IntArrayList;
import cern.colt.list.DoubleArrayList;
import cern.colt.map.OpenIntIntHashMap;
import cern.colt.matrix.DoubleMatrix2D;
import edu.mst.db.util.JdbcConnectionPool;

public class CorporaCondWordProbDao {

    public static final String allFields = "predicateWordId, assumedFactWordId, freq, condProb, lnCondProb";
    public static final String tableName = "conceptrecogn.corporacondwordprob";
    public static final String deleteAllSql = "truncate table " + tableName;
    public static final String persistSql = "INSERT INTO " + tableName + " ("
	    + allFields + ") VALUES(?,?,?,?,?)";
    public static final String marshalSql = "SELECT freq, condProb, lnCondProb FROM conceptrecogn.corporacondwordprob "
	    + "WHERE predicateWordId = ? AND assumedFactWordId = ?";
    public static final String marshalFreqSql = "SELECT freq FROM conceptrecogn.corporacondwordprob "
	    + "WHERE predicateWordId = ? AND assumedFactWordId = ?";
    public static final String updateFreqSql = "UPDATE conceptrecogn.corporacondwordprob "
	    + "SET freq = ? WHERE predicateWordId = ? AND assumedFactWordId = ?";
    public static final String updateCondProbSql = "UPDATE conceptrecogn.corporacondwordprob "
	    + "SET condProb = ?, lnCondProb = ? WHERE predicateWordId = ? AND assumedFactWordId = ?";
    public static final String countUniquePredSql = "select count(*) from "
	    + "(select distinct predicateWordId from conceptrecogn.corporacondwordprob) as c";
    public static final String getUniquePredWordIdsSql = "select distinct predicateWordId "
	    + "from conceptrecogn.corporacondwordprob order by predicateWordId";
    public static final String marshalForPredSql = "SELECT assumedFactWordId, freq, condProb, lnCondProb "
	    + "FROM conceptrecogn.corporacondwordprob WHERE predicateWordId = ?";
    public static final String marshalLnCondProbForPredFactSql = "SELECT lnCondProb "
	    + "FROM conceptrecogn.corporacondwordprob WHERE predicateWordId = ? and assumedFactWordId = ?";
    public static final String sumFreqForPredSql = "select sum(freq) from conceptrecogn.corporacondwordprob "
	    + "where predicateWordId = ?";

    public static void deleteAll(JdbcConnectionPool connPool)
	    throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement deleteStmt = conn
		    .prepareStatement(deleteAllSql)) {
		deleteStmt.executeUpdate();
	    }
	}
    }

    public static int countUniquePredWords(JdbcConnectionPool connPool)
	    throws SQLException {
	int count = 0;
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement countStmt = conn
		    .prepareStatement(countUniquePredSql)) {
		try (ResultSet rs = countStmt.executeQuery()) {
		    if (rs.next()) {
			count = rs.getInt(1);
		    }
		}
	    }
	}
	return count;
    }

    public static int[] getUniquePredWords(JdbcConnectionPool connPool) throws SQLException {
	int nbrUniquePredWords = countUniquePredWords(connPool);
	int[] predWords = new int[nbrUniquePredWords];
	try(Connection conn = connPool.getConnection()) {
	    try(PreparedStatement getPredIdsStmt = conn.prepareStatement(getUniquePredWordIdsSql)){
		try(ResultSet rs = getPredIdsStmt.executeQuery()){
		    int pos = 0;
		    while(rs.next()) {
			predWords[pos] = rs.getInt(1);
			pos++;
		    }
		}
	    }
	}
	return predWords;
    }

    public void persist(CorporaCondWordProb condProb,
	    JdbcConnectionPool connPool) throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement persistStmt = conn
		    .prepareStatement(persistSql)) {
		setPersistValues(condProb, persistStmt);
		persistStmt.executeUpdate();
	    }
	}
    }

    public void persistBatch(Collection<CorporaCondWordProb> condProbs,
	    JdbcConnectionPool connPool) throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement persistStmt = conn
		    .prepareStatement(persistSql)) {
		for (CorporaCondWordProb condProb : condProbs) {
		    setPersistValues(condProb, persistStmt);
		    persistStmt.addBatch();
		}
		persistStmt.executeBatch();
	    }
	}
    }

    private void setPersistValues(CorporaCondWordProb condProb,
	    PreparedStatement persistStmt) throws SQLException {
	persistStmt.setInt(1, condProb.getPredicateWordId());
	persistStmt.setInt(2, condProb.getAssumedFactWordId());
	persistStmt.setLong(3, condProb.getFreq());
	persistStmt.setDouble(4, condProb.getCondProb());
	persistStmt.setDouble(5, condProb.getLnCondProb());
    }

    /**
     * Used when counting and persisting word co-occurrence frequencies.
     * Computing conditional probabilities occurs afterwards when all
     * frequencies are persisted.
     * <p>
     * This method meant for batch parsing NLM abstracts and counting word
     * co-occurrences in abstracts, rather than doing all abstracts at once.
     * 
     * @param newFrequencies
     * @param connPool
     * @throws SQLException
     */
    public void persistOrUpdateFreqBatch(
	    Collection<CorporaCondWordProb> newFrequencies,
	    JdbcConnectionPool connPool) throws SQLException {
	// compile map on which records already exist
	Set<CorporaCondWordProb> existingCondProbs = new HashSet<CorporaCondWordProb>();
	try (Connection conn = connPool.getConnection()) {
	    String existsSql = "SELECT freq FROM conceptrecogn.corporacondwordprob "
		    + "WHERE predicateWordId = ? AND assumedFactWordId = ?";
	    try (PreparedStatement existsStmt = conn
		    .prepareStatement(existsSql)) {
		Iterator<CorporaCondWordProb> condProbIter = newFrequencies
			.iterator();
		while (condProbIter.hasNext()) {
		    CorporaCondWordProb newFreqCondProb = condProbIter.next();
		    existsStmt.setInt(1, newFreqCondProb.getPredicateWordId());
		    existsStmt.setInt(2,
			    newFreqCondProb.getAssumedFactWordId());
		    try (ResultSet rs = existsStmt.executeQuery()) {
			if (rs.next()) {
			    long newFreq = rs.getLong(1)
				    + newFreqCondProb.getFreq();
			    newFreqCondProb.setFreq(newFreq);
			    existingCondProbs.add(newFreqCondProb);
			}
		    }
		}
	    }
	}
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement persistStmt = conn
		    .prepareStatement(persistSql);
		    PreparedStatement updateStmt = conn
			    .prepareStatement(updateFreqSql)) {
		Iterator<CorporaCondWordProb> condProbIter = newFrequencies
			.iterator();
		while (condProbIter.hasNext()) {
		    CorporaCondWordProb newFreqCondProb = condProbIter.next();
		    if (existingCondProbs.contains(newFreqCondProb)) {
			updateStmt.setLong(1, newFreqCondProb.getFreq());
			updateStmt.setInt(2,
				newFreqCondProb.getPredicateWordId());
			updateStmt.setInt(3,
				newFreqCondProb.getAssumedFactWordId());
			updateStmt.addBatch();
		    } else {
			setPersistValues(newFreqCondProb, persistStmt);
			persistStmt.addBatch();
		    }
		}
		persistStmt.executeBatch();
		updateStmt.executeBatch();
	    }
	}
    }

    /**
     * Persist changes to word co-occurrence frequencies in batch mode.
     * 
     * @param freqMatrix
     * @param wordIdByPos
     * @param persistBatchSize
     * @param rptInterval
     * @param connPool
     * @throws SQLException
     */
    public void persistOrUpdateFreqBatch(DoubleMatrix2D freqMatrix,
	    OpenIntIntHashMap wordIdByPos, int persistBatchSize,
	    int rptInterval, JdbcConnectionPool connPool) throws SQLException {
	IntArrayList predPositions = new IntArrayList();
	IntArrayList factPositions = new IntArrayList();
	DoubleArrayList freqList = new DoubleArrayList();
	freqMatrix.getNonZeros(predPositions, factPositions, freqList);
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement marshalFreqStmt = conn
		    .prepareStatement(marshalFreqSql);
		    PreparedStatement persistStmt = conn
			    .prepareStatement(persistSql);
		    PreparedStatement updateStmt = conn
			    .prepareStatement(updateFreqSql)) {
		int nbrUpdated = 0;
		int nbrInUpdateBatch = 0;
		int nbrInPersistBatch = 0;
		for (int i = 0; i < predPositions.size(); i++) {
		    int predPos = predPositions.get(i);
		    int predWordId = wordIdByPos.get(predPos);
		    int factPos = factPositions.get(i);
		    int factWordId = wordIdByPos.get(factPos);
		    long incrFreq = (long) freqList.get(i);
		    marshalFreqStmt.setInt(1, predWordId);
		    marshalFreqStmt.setInt(2, factWordId);
		    try (ResultSet rs = marshalFreqStmt.executeQuery()) {
			if (rs.next()) {
			    long newFreq = rs.getLong(1) + incrFreq;
			    updateStmt.setLong(1, newFreq);
			    updateStmt.setInt(2, predWordId);
			    updateStmt.setInt(3, factWordId);
			    updateStmt.addBatch();
			    nbrInUpdateBatch++;
			    if (nbrInUpdateBatch == persistBatchSize) {
				updateStmt.executeBatch();
				nbrInUpdateBatch = 0;
			    }
			} else {
			    persistStmt.setInt(1, predWordId);
			    persistStmt.setInt(2, factWordId);
			    persistStmt.setLong(3, incrFreq);
			    persistStmt.addBatch();
			    nbrInPersistBatch++;
			    if (nbrInPersistBatch == persistBatchSize) {
				persistStmt.executeBatch();
				nbrInPersistBatch = 0;
			    }
			}
			nbrUpdated++;
			if (nbrUpdated % rptInterval == 0) {
			    System.out.printf(
				    "Persisted %1$,d predicate/assumed fact co-occurrence frequencies%n",
				    nbrUpdated);
			}
		    }
		}
		// clean up stragglers
		updateStmt.executeBatch();
		persistStmt.executeBatch();
		System.out.printf(
			"Persisted grand total of %1$,d predicate/assumed fact co-occurrence frequencies%n",
			nbrUpdated);
	    }
	}
    }

    public CorporaCondWordProb marshal(int predicateWordId,
	    int assumedFactWordId, JdbcConnectionPool connPool)
	    throws SQLException {
	CorporaCondWordProb condProb = null;
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement marshalStmt = conn
		    .prepareStatement(marshalSql)) {
		condProb = marshal(predicateWordId, assumedFactWordId,
			marshalStmt);
	    }
	}
	return condProb;
    }

    /**
     * Utility facilitating batch selection of objects
     * 
     * @param predicateWordId
     * @param assumedFactWordId
     * @param marshalStmt
     * @return
     * @throws SQLException
     */
    private CorporaCondWordProb marshal(int predicateWordId,
	    int assumedFactWordId, PreparedStatement marshalStmt)
	    throws SQLException {
	CorporaCondWordProb condProb = null;
	marshalStmt.setInt(1, predicateWordId);
	marshalStmt.setInt(2, assumedFactWordId);
	try (ResultSet rs = marshalStmt.executeQuery()) {
	    if (rs.next()) {
		condProb = new CorporaCondWordProb();
		condProb.setPredicateWordId(predicateWordId);
		condProb.setAssumedFactWordId(assumedFactWordId);
		condProb.setFreq(rs.getInt(1));
		condProb.setCondProb(rs.getDouble(2));
		condProb.setLnCondProb(rs.getDouble(3));
	    }
	}
	return condProb;
    }

    /**
     * Marshal all conditional probabilities for predicate word.
     * 
     * @param predicateWordId
     * @param connPool
     * @return
     * @throws SQLException
     */
    public SortedSet<CorporaCondWordProb> marshal(int predicateWordId,
	    JdbcConnectionPool connPool) throws SQLException {
	SortedSet<CorporaCondWordProb> results = new TreeSet<CorporaCondWordProb>();
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement marshalStmt = conn
		    .prepareStatement(marshalForPredSql)) {
		marshalStmt.setInt(1, predicateWordId);
		try (ResultSet rs = marshalStmt.executeQuery()) {
		    while (rs.next()) {
			CorporaCondWordProb condProb = new CorporaCondWordProb();
			condProb.setPredicateWordId(predicateWordId);
			condProb.setAssumedFactWordId(rs.getInt(1));
			condProb.setFreq(rs.getInt(2));
			condProb.setCondProb(rs.getDouble(3));
			condProb.setLnCondProb(rs.getDouble(4));
			results.add(condProb);
		    }
		}
	    }
	}
	return results;
    }

    public double marshalLnCondProbForPredFact(int predicateWordId,
	    int assumedFactWordId, JdbcConnectionPool connPool)
	    throws SQLException {
	double lnCondProb = 0.0;
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement getLnCondProbStmt = conn
		    .prepareStatement(marshalLnCondProbForPredFactSql)) {
		getLnCondProbStmt.setInt(1, predicateWordId);
		getLnCondProbStmt.setInt(2, assumedFactWordId);
		try (ResultSet rs = getLnCondProbStmt.executeQuery()) {
		    if (rs.next()) {
			lnCondProb = rs.getDouble(1);
		    }
		}
	    }
	}
	return lnCondProb;
    }

    public long sumFreqForPred(int predicateWordId, JdbcConnectionPool connPool)
	    throws SQLException {
	long freqSum = 0;
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement sumStmt = conn
		    .prepareStatement(sumFreqForPredSql)) {
		sumStmt.setInt(1, predicateWordId);
		try (ResultSet rs = sumStmt.executeQuery()) {
		    if (rs.next()) {
			freqSum = rs.getLong(1);
		    }
		}
	    }
	}
	return freqSum;
    }

    /**
     * Updates conditional probability condProb and natural log of conditional
     * probability lnCondProb fields.
     * 
     * @param condProbs
     * @param connPool
     * @throws SQLException
     */
    public void updateCondProbBatch(Collection<CorporaCondWordProb> condProbs,
	    JdbcConnectionPool connPool) throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement updateCondProbStmt = conn
		    .prepareStatement(updateCondProbSql)) {
		Iterator<CorporaCondWordProb> condProbIter = condProbs
			.iterator();
		while (condProbIter.hasNext()) {
		    CorporaCondWordProb condProb = condProbIter.next();
		    updateCondProbStmt.setDouble(1, condProb.getCondProb());
		    updateCondProbStmt.setDouble(2, condProb.getLnCondProb());
		    updateCondProbStmt.setInt(3, condProb.getPredicateWordId());
		    updateCondProbStmt.setInt(4,
			    condProb.getAssumedFactWordId());
		    updateCondProbStmt.addBatch();
		}
		updateCondProbStmt.executeBatch();
	    }
	}
    }

}

/**
 * 
 */
/**
 * Components for computing word co-occurrence probability for corpora, used for
 * cogency calculations.
 * 
 * @author gjs
 *
 */
package edu.mst.db.nlm.corpora.condprob;
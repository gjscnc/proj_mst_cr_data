/**
 * 
 */
package edu.mst.db.nlm.corpora.condprob;

/**
 * Total frequency for the predicate word in the corpora.
 * <p>
 * Pre-processed total frequency for predicate word in corpora sentences.
 * 
 * @author gjs
 *
 */
public class CorporaCondWordPredFreq implements Comparable<CorporaCondWordPredFreq>{
    
    private int predWordId = 0;
    private long totalFreq = 0L;
    
    public CorporaCondWordPredFreq(int predWordId, long totalFreq) {
	this.predWordId = predWordId;
	this.totalFreq = totalFreq;
    }

    @Override
    public int compareTo(CorporaCondWordPredFreq predFreq) {
	return Integer.compare(predWordId, predFreq.predWordId);
    }
    
    @Override
    public boolean equals(Object obj) {
	if(obj instanceof CorporaCondWordPredFreq) {
	    CorporaCondWordPredFreq predFreq = (CorporaCondWordPredFreq)obj;
	    return predWordId == predFreq.predWordId;
	}
	return false;
    }
    
    @Override
    public int hashCode() {
	return Integer.hashCode(predWordId);
    }

    public int getPredWordId() {
        return predWordId;
    }

    public void setPredWordId(int predWordId) {
        this.predWordId = predWordId;
    }

    public long getTotalFreq() {
        return totalFreq;
    }

    public void setTotalFreq(long totalFreq) {
        this.totalFreq = totalFreq;
    }

}

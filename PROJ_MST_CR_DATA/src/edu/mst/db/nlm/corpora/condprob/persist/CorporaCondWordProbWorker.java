/**
 * 
 */
package edu.mst.db.nlm.corpora.condprob.persist;

import edu.mst.concurrent.BaseWorker;
import edu.mst.db.nlm.corpora.condprob.CorporaCondWordPredFreqDao;
import edu.mst.db.util.ConnDbName;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * Worker for multi-threaded process to compute conditional probabilities for
 * corpora words.
 * <p>
 * Execute this worker after computing word co-occurrence frequencies using
 * {@link CorporaCondFreqWorker}.
 * 
 * @author gjs
 *
 */
public class CorporaCondWordProbWorker extends BaseWorker<Integer> {

    private boolean isPersist = false;
    private int persistBatchSize = 0;

    private int[] predWordIds = null;
    private int nbrPredWords = 0;

    private int nbrPredWordsProcessed = 0;
    private int nbrCondProbsUpdated = 0;
    private int rptInterval = 5000;

    public CorporaCondWordProbWorker(boolean isPersist, int persistBatchSize,
	    int threadCallablesBatchSize, JdbcConnectionPool connPool) {
	super(threadCallablesBatchSize, connPool);
	this.isPersist = isPersist;
	this.persistBatchSize = persistBatchSize;
    }

    @Override
    public void run() throws Exception {

	System.out.println("Marshaling lexicon word IDs for predicate words");
	predWordIds = CorporaCondWordPredFreqDao.marshalPredIds(connPool);
	nbrPredWords = predWordIds.length;
	System.out.printf("Marshaled %1$,d predicate word IDs %n",
		nbrPredWords);

	System.out
		.println("Now computing conditional probabilities for corpora");

	while (currentPos < nbrPredWords) {

	    maxPos = getNextBatchMaxPos();
	    startPos = currentPos;

	    for (int i = startPos; i <= maxPos; i++) {
		int predWordId = predWordIds[i];
		CorporaCondWordProbCallable callable = new CorporaCondWordProbCallable(
			predWordId, isPersist, persistBatchSize, connPool);
		svc.submit(callable);
	    }

	    for (int i = startPos; i <= maxPos; i++) {
		nbrCondProbsUpdated += svc.take().get();
		nbrPredWordsProcessed++;
		if (nbrPredWordsProcessed % rptInterval == 0) {
		    System.out.printf(
			    "Processed %1$,d predicate lexicon words, "
				    + "updating %2$,d conditional probabilities. %n",
			    nbrPredWordsProcessed, nbrCondProbsUpdated);
		}
	    }

	    currentPos = maxPos + 1;

	}

	System.out.printf(
		"Processed total of %1$,d predicate lexicon words, "
			+ "updating %2$,d conditional probabilities. %n",
		nbrPredWordsProcessed, nbrCondProbsUpdated);
    }

    @Override
    protected int getNextBatchMaxPos() throws Exception {
	int maxPos = currentPos + threadCallablesBatchSize - 1;
	if (maxPos >= nbrPredWords - 1) {
	    maxPos = nbrPredWords - 1;
	}
	return maxPos;
    }

    public static void main(String[] args) {

	try {

	    JdbcConnectionPool connPool = new JdbcConnectionPool(
		    ConnDbName.CONCEPT_RECOGN);
	    boolean isPersist = true;
	    int persistBatchSize = 20000;
	    int threadCallablesBatchSize = 50;

	    try (CorporaCondWordProbWorker worker = new CorporaCondWordProbWorker(
		    isPersist, persistBatchSize, threadCallablesBatchSize,
		    connPool)) {
		worker.run();
	    }

	} catch (Exception ex) {
	    ex.printStackTrace();
	}
    }

}

/**
 * 
 */
package edu.mst.db.nlm.corpora.condprob.persist;

import java.util.concurrent.Callable;

import edu.mst.db.nlm.corpora.condprob.CorporaCondWordPredFreq;
import edu.mst.db.nlm.corpora.condprob.CorporaCondWordPredFreqDao;
import edu.mst.db.nlm.corpora.condprob.CorporaCondWordProbDao;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * @author gjs
 *
 */
public class CorporaPredTotalFreqCallable implements Callable<Boolean> {

    private int predWordId = 0;
    private boolean isPersist = false;
    private JdbcConnectionPool connPool = null;

    private CorporaCondWordProbDao condProbDao = new CorporaCondWordProbDao();
    private CorporaCondWordPredFreqDao predFreqDao = new CorporaCondWordPredFreqDao();

    public CorporaPredTotalFreqCallable(int predWordId, boolean isPersist,
	    JdbcConnectionPool connPool) {
	this.predWordId = predWordId;
	this.isPersist = isPersist;
	this.connPool = connPool;
    }

    @Override
    public Boolean call() throws Exception {

	long sumFreqForPred = condProbDao.sumFreqForPred(predWordId, connPool);

	CorporaCondWordPredFreq predFreq = new CorporaCondWordPredFreq(
		predWordId, sumFreqForPred);

	if (isPersist) {
	    predFreqDao.persist(predFreq, connPool);
	}

	return true;
    }

}

/**
 * 
 */
package edu.mst.db.nlm.corpora.condprob;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.SortedSet;
import java.util.TreeSet;

import edu.mst.db.util.JdbcConnectionPool;

/**
 * @author gjs
 *
 */
public class CorporaCondWordPredFreqDao {

    public static final String allFields = "predWordId, totalFreq";

    public static final String tableName = "conceptrecogn.corporacondword_predtotalfreq";

    public static final String deleteExistingSql = "truncate table "
	    + tableName;

    public static final String persistSql = "insert into " + tableName + "("
	    + allFields + ") values(?,?)";

    public static final String marshalSql = "select " + allFields + " from "
	    + tableName + " where predWordId=?";

    public static final String marshalAllPredIdsSql = "select " + allFields
	    + " from " + tableName + " order by predWordId";

    public static void deleteExisting(JdbcConnectionPool connPool)
	    throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement deleteStmt = conn
		    .prepareStatement(deleteExistingSql)) {
		deleteStmt.executeUpdate();
	    }
	}
    }

    public void persist(CorporaCondWordPredFreq predFreq,
	    JdbcConnectionPool connPool) throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement persistStmt = conn
		    .prepareStatement(persistSql)) {
		setPersistValues(predFreq, persistStmt);
		persistStmt.executeUpdate();
	    }
	}
    }

    public void persistBatch(Collection<CorporaCondWordPredFreq> predFreqs,
	    JdbcConnectionPool connPool) throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement persistStmt = conn
		    .prepareStatement(persistSql)) {
		for (CorporaCondWordPredFreq predFreq : predFreqs) {
		    setPersistValues(predFreq, persistStmt);
		    persistStmt.addBatch();
		}
		persistStmt.executeBatch();
	    }
	}
    }

    private void setPersistValues(CorporaCondWordPredFreq predFreq,
	    PreparedStatement persistStmt) throws SQLException {
	persistStmt.setInt(1, predFreq.getPredWordId());
	persistStmt.setLong(2, predFreq.getTotalFreq());
    }

    public static int[] marshalPredIds(JdbcConnectionPool connPool)
	    throws SQLException {
	SortedSet<Integer> predIdsSet = new TreeSet<Integer>();
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement marshalStmt = conn
		    .prepareStatement(marshalAllPredIdsSql)) {
		try (ResultSet rs = marshalStmt.executeQuery()) {
		    while (rs.next()) {
			predIdsSet.add(rs.getInt(1));
		    }
		}
	    }
	}
	int[] predIds = new int[predIdsSet.size()];
	int pos = 0;
	for (int predId : predIdsSet) {
	    predIds[pos] = predId;
	    pos++;
	}
	return predIds;
    }

    public CorporaCondWordPredFreq marshal(int predWordId,
	    JdbcConnectionPool connPool) throws SQLException {
	CorporaCondWordPredFreq predFreq = null;
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement marshalStmt = conn
		    .prepareStatement(marshalSql)) {
		marshalStmt.setInt(1, predWordId);
		try (ResultSet rs = marshalStmt.executeQuery()) {
		    while (rs.next()) {
			predFreq = instantiateFromResultSet(rs);
		    }
		}
	    }
	}
	return predFreq;
    }

    private CorporaCondWordPredFreq instantiateFromResultSet(ResultSet rs)
	    throws SQLException {
	int lexWordId = rs.getInt(1);
	long totalFreq = rs.getLong(2);
	CorporaCondWordPredFreq predFreq = new CorporaCondWordPredFreq(
		lexWordId, totalFreq);
	return predFreq;
    }

}

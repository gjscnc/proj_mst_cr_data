package edu.mst.db.nlm.corpora.condprob.persist;

import edu.mst.concurrent.BaseWorker;
import edu.mst.db.nlm.corpora.condprob.CorporaCondWordPredFreqDao;
import edu.mst.db.nlm.corpora.condprob.CorporaCondWordProbDao;
import edu.mst.db.util.ConnDbName;
import edu.mst.db.util.JdbcConnectionPool;

public class CorporaPredTotalFreqWorker extends BaseWorker<Boolean> {

    private boolean isPersist = false;
    private boolean isDeleteExisting = false;

    private int[] predWordIds = null;
    private int nbrPredWords = 0;

    private int nbrPredWordsProcessed = 0;
    private int rptInterval = 5000;

    public CorporaPredTotalFreqWorker(boolean isPersist,
	    boolean isDeleteExisting, JdbcConnectionPool connPool) {
	super(connPool);
	this.isPersist = isPersist;
	this.isDeleteExisting = isDeleteExisting;
    }

    @Override
    public void run() throws Exception {

	if (isDeleteExisting) {
	    System.out.println(
		    "Deleting existing total frequency for predicate words in corpora");
	    CorporaCondWordPredFreqDao.deleteExisting(connPool);
	}

	System.out.println("Marshaling lexicon word IDs for predicate words");
	predWordIds = CorporaCondWordProbDao.getUniquePredWords(connPool);
	nbrPredWords = predWordIds.length;
	System.out.printf("Marshaled %1$,d predicate word IDs %n",
		nbrPredWords);

	System.out.println("Now computing total frequencies");

	while (currentPos < nbrPredWords) {

	    maxPos = getNextBatchMaxPos();
	    startPos = currentPos;

	    for (int i = startPos; i <= maxPos; i++) {
		int predWordId = predWordIds[i];
		CorporaPredTotalFreqCallable callable = new CorporaPredTotalFreqCallable(
			predWordId, isPersist, connPool);
		svc.submit(callable);
	    }

	    for (int i = startPos; i <= maxPos; i++) {
		boolean isPersisted = svc.take().get();
		if (isPersisted) {
		    nbrPredWordsProcessed++;
		    if (nbrPredWordsProcessed % rptInterval == 0) {
			System.out.printf("Processed %1$,d predicate words. %n",
				nbrPredWordsProcessed);
		    }
		}
	    }

	    currentPos = maxPos + 1;

	}

	System.out.printf("Processed total of %1$,d predicate words. %n",
		nbrPredWordsProcessed);
    }

    @Override
    protected int getNextBatchMaxPos() throws Exception {
	int maxPos = currentPos + threadCallablesBatchSize - 1;
	if (maxPos >= nbrPredWords - 1) {
	    maxPos = nbrPredWords - 1;
	}
	return maxPos;
    }

    public static void main(String[] args) {

	try {

	    JdbcConnectionPool connPool = new JdbcConnectionPool(
		    ConnDbName.CONCEPT_RECOGN);
	    boolean isPersist = true;
	    boolean isDeleteExisting = true;

	    try (CorporaPredTotalFreqWorker worker = new CorporaPredTotalFreqWorker(
		    isPersist, isDeleteExisting, connPool)) {
		worker.run();
	    }

	} catch (Exception ex) {
	    ex.printStackTrace();
	}

    }

}

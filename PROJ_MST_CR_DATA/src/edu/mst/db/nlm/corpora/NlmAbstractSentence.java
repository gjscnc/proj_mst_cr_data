package edu.mst.db.nlm.corpora;

import java.util.SortedSet;
import java.util.TreeSet;

public class NlmAbstractSentence implements Comparable<NlmAbstractSentence> {

    private int pmid = 0;
    private int sentNbr = 0;
    private SortedSet<NlmAbstractWord> words = new TreeSet<NlmAbstractWord>();

    @Override
    public int compareTo(NlmAbstractSentence sent) {
	int result = Integer.compare(pmid, sent.pmid);
	if (result != 0) {
	    return result;
	}
	return Integer.compare(sentNbr, sent.sentNbr);
    }

    public boolean equals(Object obj) {
	if (obj instanceof NlmAbstractSentence) {
	    NlmAbstractSentence sent = (NlmAbstractSentence) obj;
	    return pmid == sent.pmid && sentNbr == sent.sentNbr;
	}
	return false;
    }

    @Override
    public int hashCode() {
	return Integer.hashCode(pmid) + 31 * Integer.hashCode(sentNbr);
    }

    public int getPmid() {
        return pmid;
    }

    public void setPmid(int pmid) {
        this.pmid = pmid;
    }

    public int getSentNbr() {
        return sentNbr;
    }

    public void setSentNbr(int sentNbr) {
        this.sentNbr = sentNbr;
    }

    public SortedSet<NlmAbstractWord> getWords() {
        return words;
    }

    public void setWords(SortedSet<NlmAbstractWord> words) {
        this.words = words;
    }

}

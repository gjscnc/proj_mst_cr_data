/**
 * 
 */
package edu.mst.db.nlm.goldstd.corpora.cogency;

import java.util.Iterator;
import java.util.List;
import java.util.SortedSet;
import java.util.concurrent.Callable;

import edu.mst.db.lexicon.StopWords;
import edu.mst.db.nlm.corpora.condprob.CorporaCondWordProbDao;
import edu.mst.db.nlm.goldstd.corpora.GoldStdSentWord;
import edu.mst.db.nlm.goldstd.corpora.GoldStdSentWordDao;
import edu.mst.db.nlm.goldstd.corpora.GoldStdSentence;
import edu.mst.db.nlm.goldstd.corpora.GoldStdSentenceDao;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * @author gjs
 *
 */
public class PersistGoldStdCogencyCallable implements Callable<Integer> {

    private int goldStdPmid;
    private StopWords stopWords;
    private JdbcConnectionPool connPool;
    private GoldStdSentenceDao sentDao = new GoldStdSentenceDao();
    private GoldStdSentWordDao sentWordDao = new GoldStdSentWordDao();
    private CorporaCondWordProbDao wordProbDao = new CorporaCondWordProbDao();

    public PersistGoldStdCogencyCallable(int goldStdPmid, StopWords stopWords,
	    JdbcConnectionPool connPool) {
	this.goldStdPmid = goldStdPmid;
	this.stopWords = stopWords;
	this.connPool = connPool;
    }

    @Override
    public Integer call() throws Exception {

	SortedSet<GoldStdSentence> sentences = sentDao
		.marshalAllSentencesForPmid(goldStdPmid, connPool);
	Iterator<GoldStdSentence> sentIter = sentences.iterator();
	double cumLnAbstrCogency = 0.0d;
	GoldStdSentWord assumedFact = null;
	while (sentIter.hasNext()) {
	    GoldStdSentence sent = sentIter.next();
	    double cumLnSentCogency = 0.0d;
	    List<GoldStdSentWord> words = sent.getWords();
	    words.sort(GoldStdSentWord.Comparators.ByPmidSentNbrWordNbr);
	    Iterator<GoldStdSentWord> wordIter = words.iterator();
	    while (wordIter.hasNext()) {
		GoldStdSentWord predicate = wordIter.next();
		predicate.setCumLnAbstrCogency(cumLnAbstrCogency);
		predicate.setCumLnSentCogency(cumLnSentCogency);
		if (stopWords.getLexWordIds().contains(predicate.getLexWordId())
			|| predicate.isPunct()) {
		    continue;
		}
		if (assumedFact == null) {
		    assumedFact = predicate;
		    continue;
		}
		double lnCondProb = wordProbDao.marshalLnCondProbForPredFact(
			predicate.getLexWordId(), assumedFact.getLexWordId(),
			connPool);
		cumLnAbstrCogency += lnCondProb;
		predicate.setCumLnAbstrCogency(cumLnAbstrCogency);
		if (predicate.getSentWordNbr() > 0) {
		    cumLnSentCogency += lnCondProb;
		    predicate.setCumLnSentCogency(cumLnSentCogency);
		}
		assumedFact = predicate;
	    }
	}
	
	sentWordDao.updateCogencies(sentences, connPool);

	return sentences.size();
    }

}

/**
 * 
 */
package edu.mst.db.nlm.goldstd.corpora;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.SortedSet;
import java.util.TreeSet;

import edu.mst.db.util.JdbcConnectionPool;

/**
 * @author gjs
 *
 */
public class GoldStdAnnotWordDao {

    public static final String[] autoPkField = new String[] { "id" };

    public static final String persistFields = "annotId, pmid, sentNbr, sentWordNbr, sentPhraseNbr, phraseWordNbr, "
	    + "lexWordId, sourceCorpus";

    public static final String allFields = "id, " + persistFields;

    public static final String persistWordSql = "INSERT INTO conceptrecogn.goldstd_annotwords "
	    + "(" + persistFields + ") VALUES (?,?,?,?,?,?,?,?)";

    public static final String marshalWordsSql = "SELECT " + allFields
	    + " FROM conceptrecogn.goldstd_annotwords WHERE annotId=? ";

    /**
     * Persists this object to database
     * 
     * @param word
     * @param connPool
     * @throws Exception
     */
    public void persist(GoldStdAnnotWord word, JdbcConnectionPool connPool)
	    throws Exception {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement insertWordStmt = conn
		    .prepareStatement(persistWordSql, autoPkField)) {
		insertWordStmt.setInt(1, word.getAnnotDbId());
		insertWordStmt.setInt(2, word.getPmid());
		insertWordStmt.setInt(3, word.getSentNbr());
		insertWordStmt.setInt(4, word.getSentWordNbr());
		insertWordStmt.setInt(5, word.getSentPhraseNbr());
		insertWordStmt.setInt(6, word.getPhraseWordNbr());
		insertWordStmt.setInt(7, word.getLexWordId());
		insertWordStmt.setString(8, word.getSourceCorpus());
		insertWordStmt.executeUpdate();

		try (ResultSet keyRs = insertWordStmt.getGeneratedKeys()) {
		    if (keyRs.next()) {
			word.setId(keyRs.getInt(1));
		    } else {
			throw new Exception(
				"Auto-incremented PK value not found for gold std annotation word");
		    }
		}
	    }
	}
    }

    /**
     * This method marshals all gold standard words associated with the
     * annotation database ID.
     * 
     * @param annotId
     * @param connPool
     * @return
     * @throws Exception
     */
    public SortedSet<GoldStdAnnotWord> marshalWordsForAnnot(int annotId,
	    JdbcConnectionPool connPool) throws Exception {

	SortedSet<GoldStdAnnotWord> words = new TreeSet<GoldStdAnnotWord>();

	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement marshalWordsStmt = conn
		    .prepareStatement(marshalWordsSql)) {
		marshalWordsStmt.setInt(1, annotId);
		try (ResultSet rs = marshalWordsStmt.executeQuery()) {
		    while (rs.next()) {
			GoldStdAnnotWord word = new GoldStdAnnotWord();
			word.setId(rs.getInt(1));
			word.setAnnotDbId(rs.getInt(2));
			word.setPmid(rs.getInt(3));
			word.setSentNbr(rs.getInt(4));
			word.setSentWordNbr(rs.getInt(5));
			word.setSentPhraseNbr(rs.getInt(6));
			word.setPhraseWordNbr(rs.getInt(7));
			word.setLexWordId(rs.getInt(8));
			word.setSourceCorpus(rs.getString(9));
			words.add(word);
		    }
		}
	    }
	}

	return words;
    }

}

/**
 * 
 */
package edu.mst.db.nlm.goldstd.corpora.annot;

import java.util.concurrent.Future;

import edu.mst.concurrent.BaseWorker;
import edu.mst.db.util.ConnDbName;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * @author gjs
 *
 */
public class PersistGoldStdTagWordsWorker extends BaseWorker<Integer> {

    private boolean isPersist = false;
    private boolean isDeleteExisting = false;

    private int nbrPmids = 0;
    private int[] pmids = null;

    private GoldStdTagSpanDao tagDao = new GoldStdTagSpanDao();

    private int rptInterval = 50;
    private int nbrPmidsProcessed = 0;
    private int nbrTagWordsPersisted = 0;

    public PersistGoldStdTagWordsWorker(boolean isPersist, boolean isDeleteExisting, 
	    JdbcConnectionPool connPool) {
	super(connPool);
	this.isPersist = isPersist;
	this.isDeleteExisting = isDeleteExisting;
    }

    @Override
    public void run() throws Exception {

	if(isDeleteExisting){
	    System.out.println("Deleteing existing tag words");
	    GoldStdTagWordDao.deleteAll(connPool);
	}
	
	{
	    System.out.println("Marshaling tag PMIDs");
	    pmids = tagDao.marshalPmids(connPool);
	    nbrPmids = pmids.length;
	    System.out.printf("Marshaled %1$,d PMIDs. %n", nbrPmids);
	}

	System.out.println("Now persisting tag words");

	while (currentPos < nbrPmids) {

	    maxPos = getNextBatchMaxPos();
	    startPos = currentPos;

	    for (int i = startPos; i <= maxPos; i++) {
		int pmid = pmids[i];
		PersistGoldStdTagWordsCallable callable = new PersistGoldStdTagWordsCallable(
			pmid, isPersist, connPool);
		svc.submit(callable);
	    }

	    for (int i = startPos; i <= maxPos; i++) {
		Future<Integer> future = svc.take();
		nbrTagWordsPersisted += future.get();
		nbrPmidsProcessed++;
		if (nbrPmidsProcessed % rptInterval == 0) {
		    System.out.printf(
			    "Processed %1$,d PMIDs, persisting %2$,d gold standard tag words. %n",
			    nbrPmidsProcessed, nbrTagWordsPersisted);
		}
	    }

	    currentPos = maxPos + 1;

	}

	System.out.printf(
		"Processed %1$,d PMIDs, persisting %2$,d gold standard tag words. %n",
		nbrPmidsProcessed, nbrTagWordsPersisted);

    }

    @Override
    protected int getNextBatchMaxPos() throws Exception {
	int maxPos = currentPos + threadCallablesBatchSize - 1;
	if (maxPos >= nbrPmids - 1) {
	    maxPos = nbrPmids - 1;
	}
	return maxPos;
    }

    public static void main(String[] args) {

	try {

	    boolean isPersist = true;
	    boolean isDeleteExisting = true;
	    JdbcConnectionPool connPool = new JdbcConnectionPool(
		    ConnDbName.CONCEPT_RECOGN);

	    try (PersistGoldStdTagWordsWorker worker = new PersistGoldStdTagWordsWorker(
		    isPersist, isDeleteExisting, connPool)) {
		worker.run();
	    }

	} catch (Exception ex) {
	    ex.printStackTrace();
	}
    }

}

/**
 * 
 */
package edu.mst.db.nlm.goldstd.corpora;

import java.util.SortedSet;
import java.util.concurrent.Future;

import edu.mst.concurrent.BaseWorker;
import edu.mst.db.nlm.goldstd.corpora.annot.GoldStdTag;
import edu.mst.db.nlm.goldstd.corpora.annot.GoldStdTagDao;
import edu.mst.db.nlm.goldstd.corpora.annot.GoldStdTagNameMatchDao;
import edu.mst.db.util.ConnDbName;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * @author gjs
 *
 */
public class MatchTagToConceptNamesWorker
	extends BaseWorker<MatchTagToConceptNameResult> {

    private boolean isPersist = false;
    private boolean isDeleteExisting = false;

    private GoldStdTag[] tags;
    private int nbrTags = 0;
    private int nbrTagsProcessed = 0;

    private int rptInterval = 50;
    private int nbrNamesMatched = 0;
    private int nbrTagsMissingCui = 0;

    public MatchTagToConceptNamesWorker(boolean isPersist, boolean isDeleteExisting, 
	    JdbcConnectionPool connPool) {
	super(connPool);
	this.isPersist = isPersist;
	this.isDeleteExisting = isDeleteExisting;
    }

    @Override
    public void run() throws Exception {

	if(isDeleteExisting){
	    System.out.println("Deleting existing tag-name matches");
	    GoldStdTagNameMatchDao.deleteAll(connPool);
	}
	
	{
	    System.out.println("Marshaling gold standard annotation tags");
	    GoldStdTagDao tagDao = new GoldStdTagDao();
	    SortedSet<GoldStdTag> tagSet = tagDao.marshalAll(connPool);
	    nbrTags = tagSet.size();
	    tags = new GoldStdTag[nbrTags];
	    int pos = 0;
	    for (GoldStdTag tag : tagSet) {
		tags[pos] = tag;
		pos++;
	    }
	    System.out.printf(
		    "Marshaled %1$,d gold standard annotation tags. %n",
		    nbrTags);
	}

	System.out.println(
		"Now identifying matching concept name for each annotation");

	while (currentPos < nbrTags) {

	    maxPos = getNextBatchMaxPos();
	    startPos = currentPos;

	    for (int i = startPos; i <= maxPos; i++) {
		GoldStdTag tag = tags[i];
		MatchTagToConceptNamesCallable callable = new MatchTagToConceptNamesCallable(
			tag, isPersist, connPool);
		svc.submit(callable);
	    }

	    for (int i = startPos; i <= maxPos; i++) {
		Future<MatchTagToConceptNameResult> future = svc.take();
		MatchTagToConceptNameResult result = future.get();
		nbrNamesMatched += result.nbrNamesMatched;
		if (!result.isCuiInNames) {
		    nbrTagsMissingCui++;
		}
		nbrTagsProcessed++;
		if (nbrTagsProcessed % rptInterval == 0) {
		    System.out.printf(
			    "Processed %1$,d tags, matched %2$,d names, "
				    + "%3$,d names missing CUI.%n",
			    nbrTagsProcessed, nbrNamesMatched,
			    nbrTagsMissingCui);
		}
	    }

	    currentPos = maxPos + 1;

	}
	System.out.printf(
		"Processed total of %1$,d tags, %2$,d names matched, "
			+ "%3$,d tags missing CUI.%n",
		nbrTagsProcessed, nbrNamesMatched, nbrTagsMissingCui);
    }

    @Override
    protected int getNextBatchMaxPos() throws Exception {
	int maxPos = currentPos + threadCallablesBatchSize - 1;
	if (maxPos >= nbrTags - 1)
	    maxPos = nbrTags - 1;
	return maxPos;
    }

    public static void main(String[] args) {

	try {

	    JdbcConnectionPool connPool = new JdbcConnectionPool(
		    ConnDbName.CONCEPT_RECOGN);
	    boolean isPersist = true;
	    boolean isDeleteExisting = true;

	    try (MatchTagToConceptNamesWorker worker = new MatchTagToConceptNamesWorker(
		    isPersist, isDeleteExisting, connPool)) {
		worker.run();
	    }

	} catch (Exception ex) {
	    ex.printStackTrace();
	}
    }

}

package edu.mst.db.nlm.goldstd.corpora.parse;

import java.util.ArrayList;
import java.util.List;

import gov.nih.nlm.nls.metamap.Ev;
import gov.nih.nlm.nls.metamap.Mapping;
import gov.nih.nlm.nls.metamap.MetaMapApi;
import gov.nih.nlm.nls.metamap.MetaMapApiImpl;
import gov.nih.nlm.nls.metamap.PCM;
import gov.nih.nlm.nls.metamap.Result;
import gov.nih.nlm.nls.metamap.Utterance;

public class NLMConcept {
    
    private static MetaMapApi api = null;

    private String cui = null;
    private String conceptName = null;
    
    public NLMConcept(String cui, String conceptName){
	this.cui = cui;
	this.conceptName = conceptName;
    }
    
    public static NLMConcept getConcept(String text) throws Exception{
	
	if(api == null)
	    startMetaMap();

	String cui = null;
	String conceptName = null;

	List<Result> results = api.processCitationsFromString(text);

	int bestScore = 0;

	for (Result result : results) {
	    for (Utterance utterance : result.getUtteranceList()) {
		for (PCM pcm : utterance.getPCMList()) {
		    for (Mapping map : pcm.getMappingList()) {
			for (Ev ev : map.getEvList()) {
			    int score = Math.abs(ev.getScore());
			    if (score > bestScore) {
				bestScore = score;
				cui = ev.getConceptId();
				conceptName = ev.getConceptName();
			    }
			}
		    }
		}
	    }
	}

	return new NLMConcept(cui, conceptName);
    }
    
    private static void startMetaMap(){
	
	api = new MetaMapApiImpl();

	List<String> theOptions = new ArrayList<String>();
	// theOptions.add("--allow_concept_gaps");
	// theOptions.add("--restrict_to_sources ");
	// theOptions.add("-R SNOMEDCT_US");
	// theOptions.add("-V USAbase");
	// theOptions.add("-A");
	// theOptions.add("--ignore_stop_phrases");
	if (theOptions.size() > 0) {
	    api.setOptions(theOptions);
	}
    }

    public static MetaMapApi getApi() {
        return api;
    }

    public String getCui() {
        return cui;
    }

    public String getConceptName() {
        return conceptName;
    }

}

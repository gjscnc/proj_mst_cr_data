package edu.mst.db.nlm.goldstd.corpora.annot;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import edu.mst.db.nlm.goldstd.corpora.GoldStdAnnotSpan;
import edu.mst.db.nlm.goldstd.corpora.parse.TextConceptTagType;

/**
 * This is one NLM annotation extracted from the NLM test data. It consists of
 * either a Gold Standard manual annotation for UMLS concepts, or results from
 * annotation provided by MetaMap. This is indicated by the attributes
 * NLMAnnotType ("annotType) and NLMAnnotSubType ("annotSubType").
 * 
 * @author George
 *
 */
public class NlmAnnot implements Comparable<NlmAnnot> {

    public static final String[] autoIncrPkName = new String[] { "id" };

    private int id;
    private int pmid;
    /**
     * This attribute exists for downstream analysis. It is not stored in the
     * database at this time. TODO store this attribute in the database.
     */
    private int sentenceNbr;
    private int docAnnotId;
    private TextConceptTagType annotType;
    private NLMAnnotSubType annotSubType;
    private List<GoldStdAnnotSpan> annotSpans = new ArrayList<GoldStdAnnotSpan>();
    private String text;
    private String cui;
    private String conceptName;
    private int beginDocPos;

    /**
     * Default constructor
     */
    public NlmAnnot() {

    }

    /**
     * Constructor for instantiating object from database record.
     * 
     * @param id
     * @param pmid
     * @param docAnnotId
     * @param annotType
     * @param annotSubType
     * @param text
     * @param cui
     * @param conceptName
     */
    public NlmAnnot(int id, int pmid, int docAnnotId, TextConceptTagType annotType,
	    NLMAnnotSubType annotSubType, String text, String cui,
	    String conceptName) {
	this.id = id;
	this.pmid = pmid;
	this.docAnnotId = docAnnotId;
	this.annotType = annotType;
	this.annotSubType = annotSubType;
	this.text = text;
	this.cui = cui;
	this.conceptName = conceptName;
    }
    
    @Override
    public int compareTo(NlmAnnot a2) {
	return Comparators.UniqueId.compare(this, a2);
    }

    public static class Comparators {
	public static final Comparator<NlmAnnot> UniqueId = (NlmAnnot a1,
		NlmAnnot a2) -> Integer.valueOf(a1.id).compareTo(
		Integer.valueOf(a2.id));
	public static final Comparator<NlmAnnot> TagId = (NlmAnnot a1,
		NlmAnnot a2) -> {
	    int i = Integer.valueOf(a1.pmid)
		    .compareTo(Integer.valueOf(a2.pmid));
	    if (i != 0)
		return i;
	    i = Integer.valueOf(a1.docAnnotId).compareTo(
		    Integer.valueOf(a2.docAnnotId));
	    if (i != 0)
		return i;
	    i = a1.annotType.toString().compareTo(a2.annotType.toString());
	    if (i != 0)
		return i;
	    return a2.annotSubType.toString().compareTo(
		    a2.annotSubType.toString());
	};
    }

    @Override
    public boolean equals(Object obj) {
	if (obj instanceof NlmAnnot) {
	    NlmAnnot annot = (NlmAnnot) obj;
	    return this.id == annot.id;
	}
	return false;
    }

    @Override
    public int hashCode() {
	return Integer.valueOf(id).hashCode();
    }

    public int getId() {
	return id;
    }

    public void setId(int id) {
	this.id = id;
    }

    public int getPmid() {
	return pmid;
    }

    public void setPmid(int pmid) {
	this.pmid = pmid;
    }

    public int getSentenceNbr() {
        return sentenceNbr;
    }

    public void setSentenceNbr(int sentenceNbr) {
        this.sentenceNbr = sentenceNbr;
    }

    public int getDocAnnotId() {
	return docAnnotId;
    }

    public void setDocAnnotId(int docAnnotId) {
	this.docAnnotId = docAnnotId;
    }

    public TextConceptTagType getAnnotType() {
	return annotType;
    }

    public void setAnnotType(TextConceptTagType annotType) {
	this.annotType = annotType;
    }

    public NLMAnnotSubType getAnnotSubType() {
	return annotSubType;
    }

    public void setAnnotSubType(NLMAnnotSubType annotSubType) {
	this.annotSubType = annotSubType;
    }

    public List<GoldStdAnnotSpan> getAnnotSpans() {
	return annotSpans;
    }

    public void setAnnotSpans(List<GoldStdAnnotSpan> annotSpans) {
	this.annotSpans = annotSpans;
    }

    public String getText() {
	return text;
    }

    public void setText(String text) {
	this.text = text;
    }

    public String getCui() {
	return cui;
    }

    public void setCui(String cui) {
	this.cui = cui;
    }

    public String getConceptName() {
	return conceptName;
    }

    public void setConceptName(String conceptName) {
	this.conceptName = conceptName;
    }

    public int getBeginDocPos() {
        return beginDocPos;
    }

    public void setBeginDocPos(int beginDocPos) {
        this.beginDocPos = beginDocPos;
    }

};

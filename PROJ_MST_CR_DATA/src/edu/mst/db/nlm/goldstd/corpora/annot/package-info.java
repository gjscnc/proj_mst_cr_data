/**
 * 
 */
/**
 * Annotation components for NLM gold standard annotated abstracts.
 * 
 * @author gjs
 *
 */
package edu.mst.db.nlm.goldstd.corpora.annot;
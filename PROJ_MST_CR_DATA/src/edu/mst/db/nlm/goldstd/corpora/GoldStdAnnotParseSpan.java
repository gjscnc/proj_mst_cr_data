/**
 * 
 */
package edu.mst.db.nlm.goldstd.corpora;

/**
 * Span consists of two simple attributes - begin position in document and end
 * position in document. This class created to enable sorting by position in
 * document.
 * 
 * @author George
 *
 */
public class GoldStdAnnotParseSpan implements Comparable<GoldStdAnnotParseSpan> {

    private int beginDocPos;
    private int endDocPos;

    @Override
    public int compareTo(GoldStdAnnotParseSpan span) {
	int i = Integer.compare(beginDocPos, span.beginDocPos);
	if (i != 0)
	    return i;
	return Integer.compare(endDocPos, span.endDocPos);
    };

    @Override
    public boolean equals(Object obj) {
	if (obj instanceof GoldStdAnnotParseSpan) {
	    GoldStdAnnotParseSpan span = (GoldStdAnnotParseSpan) obj;
	    return beginDocPos == span.beginDocPos
		    && endDocPos == span.endDocPos;
	}
	return false;
    }

    @Override
    public int hashCode() {
	return Integer.hashCode(beginDocPos) + 31 * Integer.hashCode(endDocPos);
    }

    public int getBeginDocPos() {
        return beginDocPos;
    }

    public void setBeginDocPos(int beginDocPos) {
        this.beginDocPos = beginDocPos;
    }

    public int getEndDocPos() {
        return endDocPos;
    }

    public void setEndDocPos(int endDocPos) {
        this.endDocPos = endDocPos;
    }

}

/**
 * 
 */
package edu.mst.db.nlm.goldstd.corpora;

import java.util.Comparator;

/**
 * Class to use as a map key and for sorting phrase words.
 * <p>
 * Assumes that the sentence word number has been determined.
 * </p>
 * 
 * @author gjs
 *
 */
public class GoldStdPhraseWordKey implements Comparable<GoldStdPhraseWordKey> {

    public int pmid = 0;
    public int sentNbr = 0;
    public int phraseNbr = 0;
    public int docBeginPos = 0;

    public static class Comparators {
	public static final Comparator<GoldStdPhraseWordKey> Position = (
		GoldStdPhraseWordKey k1, GoldStdPhraseWordKey k2) -> {
	    int i = Integer.compare(k1.pmid, k2.pmid);
	    if (i != 0) {
		return i;
	    }
	    i = Integer.compare(k1.sentNbr, k2.sentNbr);
	    if (i != 0) {
		return i;
	    }
	    i = Integer.compare(k1.phraseNbr, k2.phraseNbr);
	    if (i != 0) {
		return i;
	    }
	    return Integer.compare(k1.docBeginPos, k2.docBeginPos);
	};
    }

    @Override
    public int compareTo(GoldStdPhraseWordKey key) {
	return Comparators.Position.compare(this, key);
    }

    @Override
    public boolean equals(Object obj) {
	if (obj instanceof GoldStdPhraseWordKey) {
	    GoldStdPhraseWordKey key = (GoldStdPhraseWordKey) obj;
	    return pmid == key.pmid && sentNbr == key.sentNbr
		    && phraseNbr == key.phraseNbr
		    && docBeginPos == key.docBeginPos;
	}
	return false;
    }

    @Override
    public int hashCode() {
	int prime = 31;
	long result = 1L;
	result = prime * Integer.hashCode(pmid) + result;
	result = prime * Integer.hashCode(sentNbr) + result;
	result = prime * Integer.hashCode(phraseNbr) + result;
	result = prime * Integer.hashCode(docBeginPos) + result;
	return Long.hashCode(result);
    }

}

/**
 * 
 */
/**
 * Components for importing and parsing gold standard data from the NLM.
 * <p>
 * These data are used to train and test the concept recognition neural network.
 * 
 * @author gjs
 *
 */
package edu.mst.db.nlm.goldstd.corpora;
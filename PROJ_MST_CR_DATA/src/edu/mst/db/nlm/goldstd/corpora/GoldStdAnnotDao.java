/**
 * 
 */
package edu.mst.db.nlm.goldstd.corpora;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import edu.mst.db.nlm.corpora.Corpus;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * Data access object for gold standard annotations and related objects.
 * 
 * @author George
 *
 */
public class GoldStdAnnotDao {

    public static final String[] autoPkField = new String[] { "id" };

    public static final String persistFields = "annotId, annotType, pmid, beginDocPos, sentenceNbr, sentFirstWordNbr, "
	    + "sentPhraseNbr, phraseFirstWordNbr, text, cui, cuiName, bestMatchAui, existsInSnomed, sourceCorpus, "
	    + "bestMatchSnomedConceptUid, bestMatchSnomedNameUid, bestMatchSnomedNameText, isCuiInNames";

    public static final String allFields = "id, " + persistFields;

    public static final String persistAnnotSql = "INSERT INTO conceptrecogn.goldstd_annot "
	    + "(" + persistFields
	    + ") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

    public static final String marshalAnnotByIdSql = "SELECT " + allFields
	    + " FROM conceptrecogn.goldstd_annot WHERE id = ?";

    public static final String marshalAllAnnotSql = "SELECT " + allFields
	    + " FROM conceptrecogn.goldstd_annot order by id";

    public static final String countAnnotSql = "select count(*) from conceptrecogn.goldstd_annot";

    public static final String marshalAnnotIdSql = "select id from conceptrecogn.goldstd_annot order by id";

    public static final String updateNameInfoSql = "update conceptrecogn.goldstd_annot "
	    + "set bestMatchAui = ?, bestMatchSnomedNameUid=?, bestMatchSnomedNameText=?, isCuiInNames=? where id=?";

    public static final String setIsCuiInNamesSql = "update conceptrecogn.goldstd_annot "
	    + "set isCuiInNames=? where id=?";

    public static int[] marshalIds(JdbcConnectionPool connPool)
	    throws SQLException {

	int nbrAnnotIds = 0;
	int[] annotIds = null;
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement countStmt = conn
		    .prepareStatement(countAnnotSql);
		    PreparedStatement marshalStmt = conn
			    .prepareStatement(marshalAnnotIdSql)) {
		try (ResultSet rs = countStmt.executeQuery()) {
		    if (rs.next()) {
			nbrAnnotIds = rs.getInt(1);
		    }
		}
		annotIds = new int[nbrAnnotIds];
		int pos = 0;
		try (ResultSet rs = marshalStmt.executeQuery()) {
		    while (rs.next()) {
			annotIds[pos] = rs.getInt(1);
			pos++;
		    }
		}
	    }
	}
	return annotIds;
    }

    /**
     * Persists this object along with all annotation words
     * 
     * @param annot
     * @param connPool
     * @throws Exception
     */
    public void persist(GoldStdAnnot annot, JdbcConnectionPool connPool)
	    throws Exception {

	/*
	 * 1-annotId, 2-annotType, 3-pmid, 4-beginDocPos, 5-sentenceNbr,
	 * 6-sentFirstWordNbr, 7-sentPhraseNbr, 8-phraseFirstWordNbr, 9-text,
	 * 10-cui, 11-cuiName, 12-bestMatchAui, 13-existsInSnomed,
	 * 14-sourceCorpus, 15-bestMatchSnomedUid, 16-bestMatchSnomedNameUid,
	 * 17-bestMatchSnomedNameText, 18-isCuiInNames
	 */
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement persistGoldStdAnnotStmt = conn
		    .prepareStatement(persistAnnotSql, autoPkField)) {
		persistGoldStdAnnotStmt.setString(1, annot.getAnnotId());
		persistGoldStdAnnotStmt.setString(2, annot.getAnnotType());
		persistGoldStdAnnotStmt.setInt(3, annot.getPmid());
		persistGoldStdAnnotStmt.setInt(4, annot.getBeginDocPos());
		persistGoldStdAnnotStmt.setInt(5, annot.getSentenceNbr());
		persistGoldStdAnnotStmt.setInt(6, annot.getSentFirstWordNbr());
		persistGoldStdAnnotStmt.setInt(7, annot.getSentPhraseNbr());
		persistGoldStdAnnotStmt.setInt(8,
			annot.getPhraseFirstWordNbr());
		persistGoldStdAnnotStmt.setString(9, annot.getText());
		persistGoldStdAnnotStmt.setString(10, annot.getCui());
		persistGoldStdAnnotStmt.setString(11, annot.getCuiName());
		persistGoldStdAnnotStmt.setString(12, annot.getBestMatchAui());
		persistGoldStdAnnotStmt.setBoolean(13, annot.isExistsInOntol());
		persistGoldStdAnnotStmt.setString(14,
			annot.getCorpus().toString());
		persistGoldStdAnnotStmt.setLong(15,
			annot.getBestMatchSnomedConceptUid());
		persistGoldStdAnnotStmt.setLong(16,
			annot.getBestMatchSnomedNameUid());
		persistGoldStdAnnotStmt.setString(17,
			annot.getBestMatchSnomedNameText());
		persistGoldStdAnnotStmt.setBoolean(18, annot.isCuiInNames());
		try {
		    persistGoldStdAnnotStmt.executeUpdate();
		} catch (Exception ex) {
		    String errMsg = String.format(
			    "Error saving gold std annotations for "
				    + "PMID = %1$d, annotation ID = %2$s, CUI = %3$s, CUI name = %4$s",
			    annot.getPmid(), annot.getAnnotId(), annot.getCui(),
			    annot.getCuiName());
		    throw new Exception(errMsg, ex);
		}

		try (ResultSet keyRs = persistGoldStdAnnotStmt
			.getGeneratedKeys()) {
		    if (keyRs.next()) {
			annot.setId(keyRs.getInt(1));
		    } else {
			throw new Exception(
				"Auto-incremented PK value not found for gold std annotation");
		    }
		}
	    }
	}
	GoldStdAnnotWordDao wordDao = new GoldStdAnnotWordDao();
	for (GoldStdAnnotWord word : annot.getWords()) {
	    word.setAnnotDbId(annot.getId());
	    wordDao.persist(word, connPool);
	}
    }

    /**
     * Marshal gold standard annotation based upon its database ID.
     * <p>
     * This method marshals all words associated with the annotation.
     * </p>
     * 
     * @param annotId
     * @param connPool
     * @return
     * @throws Exception
     */
    public GoldStdAnnot marshal(int annotId, JdbcConnectionPool connPool)
	    throws Exception {

	GoldStdAnnot annot = null;

	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement marshalAnnotStmt = conn
		    .prepareStatement(marshalAnnotByIdSql)) {
		marshalAnnotStmt.setInt(1, annotId);
		try (ResultSet rs = marshalAnnotStmt.executeQuery()) {
		    if (rs.next()) {
			annot = instantiateFromResultSet(rs);
		    }
		}
	    }
	}

	if (annot != null) {
	    GoldStdAnnotWordDao wordDao = new GoldStdAnnotWordDao();
	    annot.getWords()
		    .addAll(wordDao.marshalWordsForAnnot(annotId, connPool));
	}

	return annot;
    }

    public List<GoldStdAnnot> marshalAll(JdbcConnectionPool connPool)
	    throws SQLException {

	List<GoldStdAnnot> annotations = new ArrayList<GoldStdAnnot>();

	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement marshalAnnotStmt = conn
		    .prepareStatement(marshalAllAnnotSql)) {
		try (ResultSet rs = marshalAnnotStmt.executeQuery()) {
		    while (rs.next()) {
			GoldStdAnnot annot = instantiateFromResultSet(rs);
			annotations.add(annot);
		    }
		}
	    }
	}

	return annotations;
    }

    private GoldStdAnnot instantiateFromResultSet(ResultSet rs)
	    throws SQLException {
	/*
	 * 1-id, 2-annotId, 3-annotType, 4-pmid, 5-beginDocPos, 6-sentenceNbr,
	 * 7-sentFirstWordNbr, 8-sentPhraseNbr, 9-phraseFirstWordNbr, 10-text,
	 * 11-cui, 12-cuiName, 13-bestMatchAui, 14-existsInSnomed,
	 * 15-sourceCorpus, 16-bestMatchSnomedConceptUid,
	 * 17-bestMatchSnomedNameUid, 18-bestMatchSnomedNameText,
	 * 19-isCuiInNames
	 */
	GoldStdAnnot annot = new GoldStdAnnot();
	annot.setId(rs.getInt(1));
	annot.setAnnotId(rs.getString(2));
	annot.setAnnotType(rs.getString(3));
	annot.setPmid(rs.getInt(4));
	annot.setBeginDocPos(rs.getInt(5));
	annot.setSentenceNbr(rs.getInt(6));
	annot.setSentFirstWordNbr(rs.getInt(7));
	annot.setSentPhraseNbr(rs.getInt(8));
	annot.setPhraseFirstWordNbr(rs.getInt(9));
	annot.setText(rs.getString(10));
	annot.setCui(rs.getString(11));
	annot.setCuiName(rs.getString(12));
	annot.setBestMatchAui(rs.getString(13));
	annot.setExistsInOntol(rs.getBoolean(14));
	annot.setCorpus(Corpus.valueOf(rs.getString(15)));
	annot.setBestMatchSnomedConceptUid(rs.getLong(16));
	annot.setBestMatchSnomedNameUid(rs.getLong(17));
	annot.setBestMatchSnomedNameText(rs.getString(18));
	annot.setIsCuiInNames(rs.getBoolean(19));

	return annot;
    }

    public void updateNameInfo(String aui, long bestMatchSnomedNameUid,
	    String bestMatchSnomedNameText, boolean isCuiInNames, int annotId, 
	    JdbcConnectionPool connPool) throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement updateStmt = conn
		    .prepareStatement(updateNameInfoSql)) {
		updateStmt.setString(1, aui);
		updateStmt.setLong(2, bestMatchSnomedNameUid);
		updateStmt.setString(3, bestMatchSnomedNameText);
		updateStmt.setInt(4, annotId);
		updateStmt.setBoolean(5, isCuiInNames);
		updateStmt.executeUpdate();
	    }
	}
    }
    
    public void setIsCuiInNames(boolean isCuiInNames, int annotId, JdbcConnectionPool connPool) throws SQLException{
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement updateStmt = conn
		    .prepareStatement(setIsCuiInNamesSql)) {
		updateStmt.setBoolean(1, isCuiInNames);
		updateStmt.setInt(2, annotId);
		updateStmt.executeUpdate();
	    }
	}	
    }

}

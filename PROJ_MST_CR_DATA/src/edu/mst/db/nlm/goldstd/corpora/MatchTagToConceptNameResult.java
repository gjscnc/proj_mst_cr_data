/**
 * 
 */
package edu.mst.db.nlm.goldstd.corpora;

/**
 * @author gjs
 *
 */
public class MatchTagToConceptNameResult {

    public int pmid = 0;
    public String tagId = null;
    public String cui = null;
    public boolean isCuiInNames = false;
    public int nbrNamesMatched = 0;
    public int nbrSingleWordsNotMatched = 0;
    public int nbrMultiWordsNotMatched = 0;
    public boolean isSingleWordMatched = false;
    public boolean isMultiWordMatched = false;
    public boolean isExactMatchExists = false;
    
}

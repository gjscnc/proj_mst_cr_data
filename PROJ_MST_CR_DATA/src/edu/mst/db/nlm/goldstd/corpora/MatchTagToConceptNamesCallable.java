package edu.mst.db.nlm.goldstd.corpora;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.Callable;

import edu.mst.db.lexicon.LexWordVariantDao;
import edu.mst.db.nlm.goldstd.corpora.annot.GoldStdTag;
import edu.mst.db.nlm.goldstd.corpora.annot.GoldStdTagDao;
import edu.mst.db.nlm.goldstd.corpora.annot.GoldStdTagNameMatch;
import edu.mst.db.nlm.goldstd.corpora.annot.GoldStdTagNameMatchDao;
import edu.mst.db.nlm.goldstd.corpora.annot.GoldStdTagWord;
import edu.mst.db.nlm.goldstd.corpora.annot.GoldStdTagWordDao;
import edu.mst.db.ontol.names.ConceptName;
import edu.mst.db.ontol.names.ConceptNameDao;
import edu.mst.db.ontol.names.Mrconso;
import edu.mst.db.ontol.names.MrconsoDao;
import edu.mst.db.ontol.words.ConceptNameWord;
import edu.mst.db.ontol.words.ConceptNameWordDao;
import edu.mst.db.util.JdbcConnectionPool;

public class MatchTagToConceptNamesCallable
	implements Callable<MatchTagToConceptNameResult> {

    private GoldStdTag tag = null;
    private boolean isPersist = false;
    private JdbcConnectionPool connPool;

    private GoldStdTagDao tagDao = new GoldStdTagDao();
    private GoldStdTagWordDao tagWordDao = new GoldStdTagWordDao();
    private GoldStdTagNameMatchDao matchDao = new GoldStdTagNameMatchDao();
    private MrconsoDao mrconsoDao = new MrconsoDao();
    private ConceptNameDao nameDao = new ConceptNameDao();
    private ConceptNameWordDao nameWordDao = new ConceptNameWordDao();
    private LexWordVariantDao variantDao = new LexWordVariantDao();

    private SortedSet<GoldStdTagNameMatch> matches = new TreeSet<GoldStdTagNameMatch>();

    private MatchTagToConceptNameResult result = new MatchTagToConceptNameResult();

    public MatchTagToConceptNamesCallable(GoldStdTag tag, boolean isPersist,
	    JdbcConnectionPool connPool) {
	this.tag = tag;
	this.isPersist = isPersist;
	this.connPool = connPool;
    }

    @Override
    public MatchTagToConceptNameResult call() throws Exception {

	result.pmid = tag.getPmid();
	result.tagId = tag.getTagId();
	result.cui = tag.getCui();
	// if no cui matched, then not usable for training
	if (result.cui == null || result.cui.isBlank()
		|| result.cui.isEmpty()) {
	    return result;
	}

	GoldStdTagWord[] tagWords = null;
	Map<Integer, Set<Integer>> variantsByLexWord = null;
	{
	    SortedSet<GoldStdTagWord> tagWordsSet = tagWordDao
		    .marshal(result.pmid, result.tagId, connPool);
	    tagWords = tagWordsSet
		    .toArray(new GoldStdTagWord[tagWordsSet.size()]);
	    Set<Integer> lexWordIds = new HashSet<Integer>();
	    for (GoldStdTagWord tagWord : tagWords) {
		lexWordIds.add(tagWord.getLexWordId());
	    }
	    variantsByLexWord = variantDao
		    .marshalVariantsByLexWordId(lexWordIds, connPool);
	}

	int nbrTagWordsNotStopPunct = 0;
	for (GoldStdTagWord tagWord : tagWords) {
	    if (tagWord.isPunctOrDelimChar() || tagWord.isStopWord()) {
		continue;
	    }
	    nbrTagWordsNotStopPunct++;
	}

	// see if any SNOMED names have this CUI
	List<Mrconso> nlmNames = mrconsoDao.marshal(tag.getCui(),
		MrconsoDao.snomedIdentifier, connPool);
	// if no SNOMED names matching CUI, then return;
	if (nlmNames.size() == 0) {
	    result.isCuiInNames = false;
	    tagDao.setIsCuiInNames(false, tag.getPmid(), tag.getTagId(),
		    connPool);
	    return result;
	}
	result.isCuiInNames = true;
	tagDao.setIsCuiInNames(true, tag.getPmid(), tag.getTagId(), connPool);

	for (Mrconso mrconso : nlmNames) {
	    String saui = mrconso.getSAUI();
	    if (saui == null || saui.isBlank() || saui.isEmpty()) {
		continue;
	    }
	    String scui = mrconso.getSCUI();
	    if (scui == null || scui.isBlank() || scui.isEmpty()) {
		continue;
	    }
	    long candidateNameUid = Long.valueOf(mrconso.getSAUI());
	    long snomedConceptUid = Long.valueOf(mrconso.getSCUI());
	    String aui = mrconso.getAUI();
	    String auiText = mrconso.getSTR();
	    /*
	     * see if single word, may be single-word concept or
	     * acronym/abbreviation
	     */
	    if (tagWords.length == 1) {
		int lexWordId = tagWords[0].getLexWordId();
		// see if acr/abbrev
		ConceptNameWord acrAbbrevNameWord = nameWordDao
			.getAcrAbbrevWord(lexWordId, snomedConceptUid,
				connPool);
		boolean isAcrAbbrev = acrAbbrevNameWord != null;
		if (isAcrAbbrev) {
		    addMatch(candidateNameUid, aui, auiText);
		    result.isSingleWordMatched = true;
		} else {
		    // else, look for single word concept that is not acr/abbrev
		    ConceptNameWord singleWordNotAcrAbbrev = nameWordDao
			    .getSingleWordNameNotAcrAbbrev(lexWordId,
				    snomedConceptUid, connPool);
		    if (singleWordNotAcrAbbrev != null && singleWordNotAcrAbbrev
			    .getConceptNameUid() != candidateNameUid) {
			continue;
		    }
		    boolean isSingleWordNotAcrAbbrev = singleWordNotAcrAbbrev != null;
		    if (isSingleWordNotAcrAbbrev) {
			addMatch(candidateNameUid, aui, auiText);
			result.isSingleWordMatched = true;
		    }
		}
	    } else {
		/*
		 * Since multiple words, need to iterate over words in
		 * annotation to find best-fit concept name
		 */

		List<ConceptNameWord> nameWordsList = nameWordDao
			.marshalWordsForName(candidateNameUid, connPool);
		nameWordsList
			.sort(ConceptNameWord.Comparators.byNameUid_WordNbr);
		ConceptNameWord[] conceptNameWords = nameWordsList
			.toArray(new ConceptNameWord[nameWordsList.size()]);
		int nbrNameWordsNotStopPunct = 0;
		for (ConceptNameWord nameWord : conceptNameWords) {
		    if (nameWord.isPunct() || nameWord.isStopWord()) {
			continue;
		    }
		    nbrNameWordsNotStopPunct++;
		}
		boolean[] exactMatchAnnotByPos = new boolean[tagWords.length];
		boolean[] exactMatchNameByPos = new boolean[conceptNameWords.length];
		Arrays.fill(exactMatchAnnotByPos, false);
		Arrays.fill(exactMatchNameByPos, false);
		for (int i = 0; i < tagWords.length; i++) {
		    GoldStdTagWord tagWord = tagWords[i];
		    if (tagWord.isPunctOrDelimChar() || tagWord.isStopWord()) {
			continue;
		    }
		    int annotWordId = tagWord.getLexWordId();
		    if (i == conceptNameWords.length) {
			continue;
		    }
		    for (int j = i; j < conceptNameWords.length; j++) {
			ConceptNameWord nameWord = conceptNameWords[i];
			if (nameWord.isPunct() || nameWord.isStopWord()) {
			    continue;
			}
			int nameWordId = nameWord.getLexWordId();
			if (annotWordId == nameWordId) {
			    exactMatchAnnotByPos[i] = true;
			    if (exactMatchNameByPos[j] == false) {
				exactMatchNameByPos[j] = true;
			    }
			} else {
			    // if words don't match, see if variant does
			    if (variantsByLexWord.get(annotWordId)
				    .contains(nameWordId)) {
				exactMatchAnnotByPos[i] = true;
				if (exactMatchNameByPos[j] == false) {
				    exactMatchNameByPos[j] = true;
				}
			    }
			}
		    }
		}
		int nbrTagWordsMatched = 0;
		for (boolean isWordMatched : exactMatchAnnotByPos) {
		    if (isWordMatched) {
			nbrTagWordsMatched++;
		    }
		}
		int nbrNameWordsMatched = 0;
		for (boolean isWordMatched : exactMatchNameByPos) {
		    if (isWordMatched) {
			nbrNameWordsMatched++;
		    }
		}
		if (nbrTagWordsMatched == nbrTagWordsNotStopPunct
			&& nbrNameWordsMatched == nbrNameWordsNotStopPunct) {
		    addMatch(candidateNameUid, aui, auiText);
		    result.isMultiWordMatched = true;
		}

	    }
	}

	result.nbrNamesMatched = matches.size();
	result.isCuiInNames = result.nbrNamesMatched > 0;

	if (isPersist) {
	    matchDao.persist(matches, connPool);
	}

	return result;
    }

    private void addMatch(long conceptNameUid, String aui, String auiText)
	    throws SQLException {
	ConceptName name = nameDao.marshal(conceptNameUid, connPool);
	if (name == null) {
	    // name is in UMLS, but not in SNOMED
	    return;
	}
	GoldStdTagNameMatch match = new GoldStdTagNameMatch();
	match.setPmid(tag.getPmid());
	match.setTagId(tag.getTagId());
	match.setConceptNameUid(conceptNameUid);
	match.setConceptUid(name.getConceptUid());
	match.setConceptNameText(name.getNameText());
	match.setCui(tag.getCui());
	match.setAui(aui);
	match.setTaggedText(tag.getTaggedText());
	match.setAuiText(auiText);
	matches.add(match);
    }

}

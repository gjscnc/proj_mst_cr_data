/**
 * 
 */
package edu.mst.db.nlm.goldstd.corpora;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import edu.mst.db.nlm.corpora.NlmAbstract;
import edu.mst.db.nlm.corpora.NlmAbstractDao;
import edu.mst.db.nlm.metamap.MincoManPhrase;
import edu.mst.db.nlm.metamap.MincoManSentence;
import edu.mst.db.nlm.metamap.MincoManWord;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * @author gjs
 *
 */
public class GoldStdSentenceDao {

    // public static final String getBaseForTokenSql = "select id, token from
    // conceptrecogn.lexwords where token = ?";
    public static final String marshalSentForPmidSentNbrSql = "SELECT begin_doc_pos, end_doc_pos, text "
	    + "FROM conceptrecogn.goldstd_sentence WHERE pmid=? AND sentence_nbr=?";
    public static final String getMaxNbrSentForPmid = "SELECT MAX(sentence_nbr) "
	    + "FROM conceptrecogn.goldstd_sentence WHERE pmid=?";
    public static final String delAllSentencesSql = "DELETE FROM conceptrecogn.goldstd_sentence";
    public static final String delAllSentforPmidSql = "DELETE FROM conceptrecogn.goldstd_sentence WHERE pmid = ?";
    public static final String persistSentSql = "INSERT INTO conceptrecogn.goldstd_sentence "
	    + "(pmid, sentence_nbr, begin_doc_pos, end_doc_pos, text) "
	    + "VALUES (?,?,?,?,?)";

    /**
     * Persist this object to database. Includes persisting all words associated
     * with sentence.
     * 
     * @param sent
     * @param connPool
     * @throws Exception
     */
    public void persistSentAndWords(GoldStdSentence sent,
	    JdbcConnectionPool connPool) throws Exception {
	try (Connection conn = connPool.getConnection()) {
	    persistSentAndWords(sent, conn);
	}
    }

    /**
     * Persist this object to database. Includes persisting all words associated
     * with sentence.
     * 
     * @param sent
     * @param conn
     * @throws Exception
     */
    public void persistSentAndWords(GoldStdSentence sent, Connection conn)
	    throws Exception {

	try (PreparedStatement persistSentStmt = conn
		.prepareStatement(persistSentSql)) {
	    persistSentStmt.setInt(1, sent.getPmid());
	    persistSentStmt.setInt(2, sent.getSentenceNbr());
	    persistSentStmt.setInt(3, sent.getBeginDocPos());
	    persistSentStmt.setInt(4, sent.getEndDocPos());
	    persistSentStmt.setString(5, sent.getSentText());

	    if (persistSentStmt.executeUpdate() != 1) {
		throw new Exception("Unable to persist sentence.");
	    }

	    GoldStdSentWordDao wordDao = new GoldStdSentWordDao();
	    for (GoldStdSentWord word : sent.getWords()) {
		wordDao.persist(word, conn);
	    }
	}
    }

    /**
     * Persists sentence and persists words associated with sentence
     * 
     * @param mincoSent
     * @param isPersistWords
     * @param connPool
     * @throws Exception
     */
    public void persist(MincoManSentence mincoSent, JdbcConnectionPool connPool)
	    throws Exception {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement persistSentStmt = conn
		    .prepareStatement(persistSentSql)) {
		/*
		 * pmid, sentence_nbr, begin_doc_pos, end_doc_pos, text
		 */
		persistSentStmt.setInt(1, mincoSent.mincoDoc.pmid);
		persistSentStmt.setInt(2, mincoSent.sentNbr);
		persistSentStmt.setInt(3, mincoSent.beginDocPos);
		persistSentStmt.setInt(4, mincoSent.endDocPos);
		persistSentStmt.setString(5, mincoSent.text);
		persistSentStmt.executeUpdate();
	    }
	}
	GoldStdSentWordDao wordDao = new GoldStdSentWordDao();
	for (MincoManPhrase mincoPhrase : mincoSent.mincoPhrases) {
	    for (MincoManWord mincoWord : mincoPhrase.mincoWords) {
		wordDao.persist(mincoWord, connPool);
	    }
	}

    }

    /**
     * Marshals sentence from database.
     * <p>
     * Include marshaling sentence words.
     * 
     * @param pmid
     * @param sentenceNbr
     * @param conn
     * @return
     * @throws Exception
     */
    public GoldStdSentence marshal(int pmid, int sentenceNbr, Connection conn)
	    throws SQLException {

	GoldStdSentence sent = null;
	try (PreparedStatement getSentForPmidSentNbrQry = conn
		.prepareStatement(marshalSentForPmidSentNbrSql)) {
	    // marshal sentence
	    getSentForPmidSentNbrQry.setInt(1, pmid);
	    getSentForPmidSentNbrQry.setInt(2, sentenceNbr);
	    try (ResultSet rs = getSentForPmidSentNbrQry.executeQuery()) {
		if (rs.next()) {
		    int sentBeginDocPos = rs.getInt(1);
		    int sentEndDocPos = rs.getInt(2);
		    String sentText = rs.getString(3);
		    NlmAbstractDao abstrDao = new NlmAbstractDao();
		    GoldStdSentWordDao sentWordDao = new GoldStdSentWordDao();
		    NlmAbstract nlmAbstr = abstrDao.marshal(pmid, conn);
		    sent = new GoldStdSentence(pmid, sentenceNbr,
			    sentBeginDocPos, sentEndDocPos, sentText,
			    nlmAbstr.getAbstrText());
		    // marshal words for this sentence
		    sent.setWords(sentWordDao.marshalWordsForSent(pmid,
			    sentenceNbr, conn));
		}
	    }
	}
	return sent;
    }

    /**
     * Marshals sentence from database.
     * <p>
     * Include marshaling sentence words.
     * 
     * @param pmid
     * @param sentenceNbr
     * @param connPool
     * @return
     * @throws Exception
     */
    public GoldStdSentence marshal(int pmid, int sentenceNbr,
	    JdbcConnectionPool connPool) throws SQLException {

	try (Connection conn = connPool.getConnection()) {
	    return marshal(pmid, sentenceNbr, conn);
	}

    }

    /**
     * Marshals all sentences associated with a PMID.
     * <p>
     * Includes marshaling sentence words.
     * 
     * @param pmid
     * @param conn
     * @return
     * @throws SQLException
     */
    public SortedSet<GoldStdSentence> marshalAllSentencesForPmid(int pmid,
	    JdbcConnectionPool connPool) throws SQLException {
	SortedSet<GoldStdSentence> sentences = new TreeSet<GoldStdSentence>();
	int maxSentNbr = -1;
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement getMaxSentNbrForPmidQry = conn
		    .prepareStatement(getMaxNbrSentForPmid)) {
		getMaxSentNbrForPmidQry.setInt(1, pmid);
		try (ResultSet rs = getMaxSentNbrForPmidQry.executeQuery()) {
		    if (rs.next())
			maxSentNbr = rs.getInt(1);
		}
	    }
	}
	GoldStdSentenceDao sentDao = new GoldStdSentenceDao();
	for (int i = 0; i <= maxSentNbr; i++) {
	    sentences.add(sentDao.marshal(pmid, i, connPool));
	}
	return sentences;
    }

    /**
     * Delete all sentences associated with a corpus.
     * 
     * @param pmidsToDelete
     *                          Set collection of PMIDs for sentences to delete
     * @param connPool
     * @return Nbr of Sentences deleted.
     * @throws SQLException
     * @throws Exception
     */
    public static int delAllSentences(Set<Integer> pmidsToDelete,
	    JdbcConnectionPool connPool) throws SQLException, Exception {

	int nbrDel = 0;
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement delAllSentForPmidStmt = conn
		    .prepareStatement(delAllSentforPmidSql)) {
		Iterator<Integer> pmidsIter = pmidsToDelete.iterator();
		while (pmidsIter.hasNext()) {
		    int pmid = pmidsIter.next();
		    delAllSentForPmidStmt.setInt(1, pmid);
		    nbrDel += delAllSentForPmidStmt.executeUpdate();
		}
	    }
	}

	return nbrDel;
    }

    /**
     * Delete all sentences in the database.
     * 
     * @param connPool
     * @throws Exception
     */
    public static int delAllSentences(JdbcConnectionPool connPool)
	    throws Exception {
	int nbrDel = 0;
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement delAllSentStmt = conn
		    .prepareStatement(delAllSentencesSql)) {
		nbrDel = delAllSentStmt.executeUpdate();
	    }
	}
	return nbrDel;
    }

}

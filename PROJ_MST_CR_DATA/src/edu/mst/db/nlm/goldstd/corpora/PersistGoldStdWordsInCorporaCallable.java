package edu.mst.db.nlm.goldstd.corpora;

import java.util.Iterator;
import java.util.SortedSet;
import java.util.concurrent.Callable;

import edu.mst.db.nlm.corpora.condprob.CorporaCondWordProb;
import edu.mst.db.nlm.corpora.condprob.CorporaCondWordProbDao;
import edu.mst.db.util.JdbcConnectionPool;

public class PersistGoldStdWordsInCorporaCallable implements Callable<Integer> {

    private int predWordId;
    private JdbcConnectionPool connPool;
    private CorporaCondWordProbDao condProbDao = new CorporaCondWordProbDao();

    public PersistGoldStdWordsInCorporaCallable(int predWordId,
	    JdbcConnectionPool connPool) {
	this.predWordId = predWordId;
	this.connPool = connPool;
    }

    @Override
    public Integer call() throws Exception {
	
	int condProbCount = 0;
	
	long sumFreqForPred = condProbDao.sumFreqForPred(predWordId, connPool);

	SortedSet<CorporaCondWordProb> condProbs = condProbDao
		.marshal(predWordId, connPool);
	Iterator<CorporaCondWordProb> condProbIter = condProbs.iterator();
	while (condProbIter.hasNext()) {
	    CorporaCondWordProb corporaCondProb = condProbIter.next();
	    if (corporaCondProb.getFreq() > 0) {
		double condProb = (double) corporaCondProb.getFreq()
			/ (double) sumFreqForPred;
		double lnCondProb = Math.log(condProb);
		corporaCondProb.setCondProb(condProb);
		corporaCondProb.setLnCondProb(lnCondProb);
	    }
	    condProbCount++;
	}

	condProbDao.updateCondProbBatch(condProbs, connPool);

	return condProbCount;
    }

}

package edu.mst.db.nlm.goldstd.corpora.annot;

public enum GoldStdType {

    Disorder,
    GeneralDisorder,
    OverlyGeneralDisorder,
    Disease,
    MMI,
    MMLite,
    AnnotatorNotes
}

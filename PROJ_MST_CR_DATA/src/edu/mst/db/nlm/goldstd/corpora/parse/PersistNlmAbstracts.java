/**
 * 
 */
package edu.mst.db.nlm.goldstd.corpora.parse;

import java.io.BufferedReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import edu.mst.db.nlm.corpora.Corpus;
import edu.mst.db.nlm.goldstd.corpora.GoldStdAbstractDao;
import edu.mst.db.util.ConnDbName;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * @author George
 *
 */
public class PersistNlmAbstracts {

    private static final int MSG_INTERVAL = 25;

    private Corpus corpus = null;
    private JdbcConnectionPool connPool = null;
    private NlmTestFileNames fileNames = null;

    private boolean persistTestData = false;
    private boolean deleteExisting = false;

    public PersistNlmAbstracts(Corpus corpus, Platform platform)
	    throws Exception {
	this.corpus = corpus;
	fileNames = new NlmTestFileNames(platform, corpus);
	connPool = new JdbcConnectionPool(ConnDbName.CONCEPT_RECOGN);
    }

    protected void persistAbstracts() throws Exception {

	// extract file names and sort to help with debugging
	List<String> sortedFileNames = new ArrayList<String>();
	if (fileNames.getAbstractFullNamesByShortName() == null
		|| fileNames.getAbstractFullNamesByShortName().size() == 0)
	    fileNames.compileNlmFileNames();
	sortedFileNames
		.addAll(fileNames.getAbstractFullNamesByShortName().keySet());
	Collections.sort(sortedFileNames);

	if (deleteExisting) {
	    deleteExisting();
	}

	int nbrAbstractsPersisted = 0;
	String currentFileName = null;
	int currentPmid = 0;

	try (Connection conn = connPool.getConnection()) {

	    try (PreparedStatement checkForAbstrStmt = conn
		    .prepareStatement(GoldStdAbstractDao.checkForAbstrSql);
		    PreparedStatement checkForAbstrCorpusStmt = conn
			    .prepareStatement(
				    GoldStdAbstractDao.checkForAbstrCorpusSql);
		    PreparedStatement insertAbstrStmt = conn.prepareStatement(
			    GoldStdAbstractDao.insertAbstrSql);
		    PreparedStatement insertAbstrCorpusStmt = conn
			    .prepareStatement(
				    GoldStdAbstractDao.insertAbstrCorpusSql)) {

		checkForAbstrCorpusStmt.setString(2,
			fileNames.getCorpus().toString());
		insertAbstrCorpusStmt.setString(2,
			fileNames.getCorpus().toString());

		for (String fileName : sortedFileNames) {

		    currentFileName = fileName;

		    currentPmid = Integer.valueOf(currentFileName.substring(5));

		    // see if abstract and abstract-corpus record exists in
		    // database
		    checkForAbstrStmt.setInt(1, currentPmid);
		    checkForAbstrCorpusStmt.setInt(1, currentPmid);

		    boolean abstrExists = false;
		    boolean abstrCorpusExists = false;
		    try (ResultSet rs = checkForAbstrStmt.executeQuery()) {
			if (rs.next()) {
			    abstrExists = rs.getInt(1) == 1;
			}
		    }
		    try (ResultSet rs = checkForAbstrCorpusStmt
			    .executeQuery()) {
			if (rs.next()) {
			    abstrCorpusExists = rs.getInt(1) == 1;
			}
		    }

		    if (abstrExists && abstrCorpusExists) {
			continue;
		    }

		    if (!abstrExists) {

			String fullFileName = fileNames.getFilesDir()
				.concat(String.valueOf(fileNames.getDirDelim()))
				.concat(fileName).concat(".")
				.concat(NlmTestFileNames.ABSTRACT_EXT);
			Path file = Paths.get(fullFileName);

			String abstractText = null;
			String title = null;
			int lineCount = 0;

			try (BufferedReader reader = Files
				.newBufferedReader(file)) {
			    String line = null;
			    while ((line = reader.readLine()) != null) {
				lineCount++;
				/*
				 * Remove [ and ] at beginning and end of title
				 * respectively.
				 */
				if (lineCount == 1) {
				    title = (new String(line)).trim();
				    if (title.charAt(0) == '\0')
					title = title.substring(1);
				    if (title.charAt(0) == '[')
					title = title.substring(1);
				    if (title.charAt(title.length() - 1) == ']')
					title = title.substring(0,
						title.length() - 2);
				}
				if (lineCount == 2) {
				    abstractText = (new String(line)).trim();
				    if (abstractText.charAt(0) == '\0')
					abstractText = abstractText
						.substring(1);
				}
			    }
			}

			// add period to end of title if none exist
			title.trim(); // remove spaces
			if (title.charAt(title.length() - 1) != '.') {
			    title = title.concat(".");
			}
			abstractText = title.concat(" ").concat(abstractText);
			insertAbstrStmt.setInt(1, currentPmid);
			insertAbstrStmt.setString(2, title);
			insertAbstrStmt.setString(3, abstractText);

			int rowsInserted = insertAbstrStmt.executeUpdate();

			if (rowsInserted == 0)
			    throw new Exception(String.format(
				    "Record not created for PMID %1$d %n",
				    currentPmid));

			nbrAbstractsPersisted++;
		    }

		    if (!abstrCorpusExists) {

			insertAbstrCorpusStmt.setInt(1, currentPmid);

			int rowsInserted = insertAbstrCorpusStmt
				.executeUpdate();

			if (rowsInserted == 0) {
			    throw new Exception(String.format(
				    "Record not created for PMID-Corpus table, "
					    + "PMID = %1$d, corpus = %2$s %n",
				    currentPmid,
				    fileNames.getCorpus().toString()));
			}
		    }

		    if (nbrAbstractsPersisted % MSG_INTERVAL == 0) {
			System.out.printf(
				"Total of %1$,d abstracts persisted in database %n",
				nbrAbstractsPersisted);
		    }

		    currentFileName = null;
		    currentPmid = 0;
		}
	    }

	    System.out.printf(
		    "%nOut of %1$,d abstracts in corpus, grand total of %2$,d new abstracts "
			    + "persisted in database %n",
		    sortedFileNames.size(), nbrAbstractsPersisted);

	} catch (Exception ex) {
	    String msg = String.format("Error for file %1$s", currentFileName);
	    System.out.println(msg);
	    throw ex;
	}

    }

    private void deleteExisting() throws Exception {
	int nbrDeleted = GoldStdAbstractDao.deleteAbstrForCorpus(corpus,
		connPool);
	System.out.printf("Deleted %1$,d existing abstracts. %n", nbrDeleted);
    }

    public boolean isPersistTestData() {
	return persistTestData;
    }

    public void setPersistTestData(boolean persistTestData) {
	this.persistTestData = persistTestData;
    }

    public boolean isDeleteExisting() {
	return deleteExisting;
    }

    public void setDeleteExisting(boolean deleteExisting) {
	this.deleteExisting = deleteExisting;
    }

    /**
     * @param args
     */
    public static void main(String[] args) {

	try {

	    boolean deleteExisting = true;
	    Corpus corpus = Corpus.CLIN_CITS_TC_CORPUS;
	    Platform platform = Platform.LINUX;
	    PersistNlmAbstracts load = new PersistNlmAbstracts(corpus,
		    platform);
	    load.setPersistTestData(true);
	    load.setDeleteExisting(deleteExisting);
	    load.fileNames.compileNlmFileNames();
	    load.persistAbstracts();
	    System.out.println("Finished");

	} catch (Exception ex) {
	    ex.printStackTrace();
	}

    }

}

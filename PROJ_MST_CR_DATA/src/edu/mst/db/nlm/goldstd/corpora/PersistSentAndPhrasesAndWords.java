/**
 * 
 */
package edu.mst.db.nlm.goldstd.corpora;

import java.io.StringReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import edu.mst.db.lexicon.LexWord;
import edu.mst.db.lexicon.LexWordDao;
import edu.mst.db.lexicon.LexWordVariantDao;
import edu.mst.db.lexicon.StopWords;
import edu.mst.db.lexicon.acrabbrev.AcrAbbrev;
import edu.mst.db.lexicon.acrabbrev.AcrAbbrevDao;
import edu.mst.db.nlm.corpora.Corpus;
import edu.mst.db.nlm.metamap.MetaMapUtil;
import edu.mst.db.nlm.metamap.MincoManDoc;
import edu.mst.db.nlm.metamap.MincoManParseUtil;
import edu.mst.db.nlm.metamap.MincoManPhrase;
import edu.mst.db.nlm.metamap.MincoManSentence;
import edu.mst.db.nlm.metamap.MincoManWord;
import edu.mst.db.text.util.LexVariantsSynType;
import edu.mst.db.text.util.LexVariantsUtil;
import edu.mst.db.text.util.NlpUtil;
import edu.mst.db.text.util.LexVariantsUtil.Variants;
import edu.mst.db.text.util.StanfordParser;
import edu.mst.db.util.ConnDbName;
import edu.mst.db.util.JdbcConnectionPool;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.process.DocumentPreprocessor;
import edu.stanford.nlp.process.PTBTokenizer;
import edu.stanford.nlp.process.TokenizerFactory;

/**
 * This class contains one public method that parses then persists sentences,
 * phrases, and words from the NLM test abstracts.
 * <p>
 * It uses the MetaMap tagger for sentence, word, and phrase parsing. This
 * supersedes methods that use the Stanford POS tagger. The Stanford tagger
 * provided part-of-speech tags inconsistent with the gold standard phrase tags
 * provided by the NLM (which are MetaMap based).
 * </p>
 * <p>
 * The abstracts are categorized by corpus. Although this hasn't occurred yet,
 * the same abstract may exist in more than one corpus. A database table exists
 * to identify which PMIDs exist in each corpus. The parsing logic will only
 * parse abstracts that have not been previously parsed.
 * </p>
 * 
 * @apiNote Note that since MetaMap is single-threaded this class has to be
 *          single threaded. Future versions will either not use MetaMap or a
 *          wrapper is necessary for calling MetaMap services that offer the
 *          ability to synchronize the service for use by multiple threads.
 *          (Obviously not a good design but only options available with
 *          MetaMap).
 * 
 * @author George
 *
 */
public class PersistSentAndPhrasesAndWords {

    private Corpus corpus = null;
    private boolean isPrintStackTrace = false;
    private boolean isDeleteExisting = false;
    private boolean isPreviouslyParsed = false;
    private boolean isPersist = false;
    private JdbcConnectionPool connPool;
    private SortedSet<Integer> goldStdPmids = new TreeSet<Integer>();
    private int nbrPmids = 0;
    private int rptProgressInterval = 5;
    private int nbrPmidsParsed = 0;
    private int nbrSentPersisted = 0;
    private int nbrPhrasesPersisted = 0;
    private MincoManParseUtil mincoManParseUtil;
    private MetaMapUtil mmUtil;
    private SortedMap<String, AcrAbbrev> acrAbbrevByToken = new TreeMap<String, AcrAbbrev>();
    private SortedMap<String, LexWord> lexWordByToken = null;
    // private MincoSentParser sentParser = null;
    private StanfordParser stanfordParser = null;
    private LexVariantsUtil variantsUtil = null;
    private LexWordDao lexWordDao = new LexWordDao();
    private LexWordVariantDao varWordDao = new LexWordVariantDao();
    private AcrAbbrevDao acrAbbrevDao = new AcrAbbrevDao();
    private GoldStdAbstractDao abstrDao = new GoldStdAbstractDao();
    private GoldStdSentenceDao sentDao = new GoldStdSentenceDao();
    private GoldStdPhraseDao phraseDao = new GoldStdPhraseDao();

    private StopWords stopWords = new StopWords();
    private NlpUtil nlpUtil = null;
    private List<Exception> persistErrors = new ArrayList<Exception>();

    /**
     * Constructor - no default constructor.
     * 
     * @param connPool
     */
    public PersistSentAndPhrasesAndWords(Corpus corpus,
	    boolean isPrintStackTrace, boolean deleteExisting,
	    boolean previouslyParsed, boolean isPersist,
	    boolean isMutateVariants, JdbcConnectionPool connPool)
	    throws Exception {
	this.corpus = corpus;
	this.isPrintStackTrace = isPrintStackTrace;
	this.isDeleteExisting = deleteExisting;
	this.isPreviouslyParsed = previouslyParsed;
	this.isPersist = isPersist;
	this.connPool = connPool;
	variantsUtil = new LexVariantsUtil(isMutateVariants, stopWords);
	nlpUtil = new NlpUtil();
	init();
    }

    private void init() throws Exception {
	// sentParser = new MincoSentParser(connPool);
	stanfordParser = new StanfordParser(connPool);
	mmUtil = new MetaMapUtil(connPool, false);
	mincoManParseUtil = new MincoManParseUtil(mmUtil, stanfordParser);
	System.out.println("Marshaling lexicon");
	lexWordByToken = LexWordDao.marshalAllWordsByToken(connPool);
	System.out.printf("Marshaled %1$,d lexicon words %n",
		lexWordByToken.size());
    }

    /**
     * This method persists all sentences, phrases, and words from each
     * abstract. All parsing is done by MetaMap.
     * 
     * @apiNote This method uses the MetaMap service to parse the document into
     *          sentences, and each sentence into phrases, and then each phrase
     *          into words. Then the
     *          {@link edu.mst.sps.util.SentPhraseWordPosParser#parseWordTokensFromPost()}
     *          method is invoked to extract words from the phrase. Words from
     *          all phrases are combined and associated with the sentence.
     *          Finally the sentence and phrases are saved including saving the
     *          maps between each phrase and words and each sentence and its
     *          words.
     * 
     * @implNote Don't forget to start the MetaMap server before invoking this
     *           method. This does not have to include starting the word sense
     *           disambiguation server.
     * 
     * @throws Exception
     */
    public void persistAll() throws Exception {

	retrievePmids();

	/*
	 * Need to adjust delete process to remove only those records associated
	 * with the specified corpus. Until then, delete records for each corpus
	 * manually via sql
	 */

	if (isDeleteExisting) {
	    // sent
	    System.out.printf("Deleted %1$,d sentences for corpus %2$s %n",
		    GoldStdSentenceDao.delAllSentences(goldStdPmids, connPool),
		    corpus.toString());
	    // sent words
	    System.out.printf("Deleted %1$,d sentence words for corpus %2$s%n",
		    GoldStdSentWordDao.delAllWords(goldStdPmids, connPool),
		    corpus.toString());
	    // phrases
	    System.out.printf("Deleted %1$,d phrases for corpus %2$s %n",
		    GoldStdPhraseDao.delAllPhrases(goldStdPmids, connPool),
		    corpus.toString());
	    // phrase words
	    System.out.printf("Deleted %1$,d phrase words for corpus %2$s %n",
		    GoldStdPhraseWordDao.delAllWords(goldStdPmids, connPool),
		    corpus.toString());

	}

	/*
	 * Retrieve acronyms and abbreviations for use when parsing words
	 */
	for (AcrAbbrev acrAbbrev : acrAbbrevDao.marshalAll(connPool)) {
	    acrAbbrevByToken.put(acrAbbrev.getToken().toLowerCase(), acrAbbrev);
	}
	System.out.printf("Marshaled %1$,d acronym/abbreviations %n",
		acrAbbrevByToken.size());

	/*
	 * now iterate over documents, extract sentences, phrases, and words,
	 * and persist these to database
	 */
	nbrPmidsParsed = 0;
	System.out.println("Now parsing sentences, phrases, and words");
	for (int pmid : goldStdPmids) {

	    MincoManDoc mincoDoc = mincoManParseUtil.parseGoldStdAbstr(pmid,
		    connPool);

	    /*
	     * Parse each sentence into phrases and words
	     */
	    for (MincoManSentence mincoSent : mincoDoc.mincoSentences) {

		mincoManParseUtil.parseMincomanPhrases(mincoSent);

		int sentWordNbr = 0;
		for (MincoManPhrase mincoPhrase : mincoSent.mincoPhrases) {

		    StringReader reader = new StringReader(mincoPhrase.text);
		    DocumentPreprocessor tokenizer = new DocumentPreprocessor(
			    reader);
		    TokenizerFactory<? extends HasWord> factory = PTBTokenizer
			    .coreLabelFactory();
		    factory.setOptions(
			    "untokenizable=noneDelete,splitHyphenated=true,ptb3Escaping=false");
		    tokenizer.setTokenizerFactory(factory);
		    Iterator<List<HasWord>> phraseIter = tokenizer.iterator();
		    while (phraseIter.hasNext()) {
			List<HasWord> stanfSent = phraseIter.next();
			int phraseWordNbr = 0;
			for (HasWord hasWord : stanfSent) {
			    CoreLabel coreLabel = (CoreLabel) hasWord;
			    String token = coreLabel.value();
			    if (token.length() > LexWord.maxTokenLen) {
				token = new String(token.substring(0,
					LexWord.maxTokenLen));
			    }
			    MincoManWord mincoWord = new MincoManWord();
			    mincoWord.text = token;
			    if (mincoDoc.acrAbbrevByToken.containsKey(token)) {
				mincoWord.isAcrAbbrev = true;
				mincoWord.acrAbbrev = mincoDoc.acrAbbrevByToken
					.get(token);
			    }
			    mincoWord.isPunct = nlpUtil
				    .isPunctOrDelimChar(token);
			    /*
			     * Make sure lexicon contains this token and its
			     * variants, including acronyms/abbreviations
			     */
			    updateLexicon(mincoWord);
			    /*
			     * Get acronym/abbreviation ID *after* updating
			     * lexicon, to ensure any new ones are marshaled
			     * into memory
			     */
			    if (mincoWord.isAcrAbbrev) {
				mincoWord.acrAbbrevId = acrAbbrevByToken
					.get(token.toLowerCase()).getId();
			    }
			    String wordTokenLc = token.toLowerCase();

			    mincoWord.beginPhrasePos = coreLabel
				    .beginPosition();
			    mincoWord.beginDocPos = mincoPhrase.beginDocPos
				    + coreLabel.beginPosition();
			    mincoWord.endPhrasePos = coreLabel.endPosition();
			    mincoWord.endDocPos = mincoPhrase.beginDocPos
				    + coreLabel.endPosition();
			    mincoWord.phraseWordNbr = phraseWordNbr;
			    mincoWord.sentWordNbr = sentWordNbr;
			    if (mincoWord.phraseWordNbr == 0) {
				mincoPhrase.sentFirstWordNbr = sentWordNbr;
			    }
			    mincoPhrase.sentLastWordNbr = sentWordNbr;
			    mincoWord.mincoPhrase = mincoPhrase;
			    if (!lexWordByToken.containsKey(wordTokenLc)) {
				String errMsg = String.format(
					"Word token '%1$s' not found in lexicon",
					wordTokenLc);
				throw new Exception(errMsg);
			    }
			    mincoWord.lexWordId = lexWordByToken
				    .get(wordTokenLc).getId();
			    mincoPhrase.mincoWords.add(mincoWord);
			    phraseWordNbr++;
			    sentWordNbr++;
			}
		    }
		}

	    }

	    /*
	     * Instantiate gold standard objects
	     */
	    List<GoldStdSentence> sentences = new ArrayList<GoldStdSentence>();
	    for (MincoManSentence mincoSent : mincoDoc.mincoSentences) {
		GoldStdSentence sent = instantiateSent(mincoSent);
		sentences.add(sent);
	    }

	    /*
	     * Persist results
	     */
	    if (isPersist) {
		try (Connection conn = connPool.getConnection()) {
		    for (GoldStdSentence sent : sentences) {
			sentDao.persistSentAndWords(sent, conn);
			nbrSentPersisted++;
			for (GoldStdPhrase phrase : sent.getPhrases()) {
			    try {
				phraseDao.persistPhraseAndWords(phrase, conn);
				nbrPhrasesPersisted++;
			    } catch (Exception ex) {
				persistErrors.add(ex);
				System.out.printf("Persist error: %1$s %n",
					ex.getLocalizedMessage());
				if (isPrintStackTrace) {
				    ex.printStackTrace();
				}
			    }
			}
		    }
		}
	    }

	    nbrPmidsParsed++;
	    if (nbrPmidsParsed % rptProgressInterval == 0) {
		System.out.printf("Parsed %1$,d PMIDs and "
			+ "persisted %2$,d sentences and %3$,d phrases, %4$,d persist errors. %n",
			nbrPmidsParsed, nbrSentPersisted, nbrPhrasesPersisted,
			persistErrors.size());
	    }
	}

	System.out.printf("Parsed grand total of %1$,d PMIDs and "
		+ "persisted %2$,d sentences and %3$,d phrases, %4$,d persist errors. %n",
		nbrPmidsParsed, nbrSentPersisted, nbrPhrasesPersisted,
		persistErrors.size());

	System.out.println("Persist errors");
	for (Exception ex : persistErrors) {
	    System.out.printf("%1$s %n", ex.getLocalizedMessage());
	}

    }

    /**
     * Make sure lexicon, including acronyms/abbreviations, contain this token.
     * 
     * @param mincoWord
     * @throws Exception
     */
    private void updateLexicon(MincoManWord mincoWord) throws Exception {

	// if (mincoWord.text.equalsIgnoreCase("cflipfl/fl-k")) {
	// System.out.printf("Test case for token %1$s %n", mincoWord.text);
	// }

	/*
	 * Get all variants
	 */
	Variants variants = variantsUtil.getVariants(
		mincoWord.text.toLowerCase(),
		LexVariantsSynType.SYNONYMS_NOT_RECURSIVE);

	/*
	 * Ensure all variants are in lexicon
	 */
	SortedMap<LexWord, SortedSet<LexWord>> lexWordsAndVariants = new TreeMap<LexWord, SortedSet<LexWord>>();

	/*
	 * See if all words are in the lexicon, if not persist new instances
	 */

	// collect all word tokens into single set collection
	Set<String> wordTokens = new HashSet<String>();
	Iterator<String> wordTokenIter = variants.variantTokens.iterator();
	while (wordTokenIter.hasNext()) {
	    String wordToken = wordTokenIter.next();
	    wordTokens.add(wordToken);
	    wordTokens.addAll(variants.variantTokens);
	}

	// persist new lexicon tokens
	Map<String, LexWord> updatesLexWordByToken = new HashMap<String, LexWord>();
	for (String wordToken : wordTokens) {
	    // add to lexicon if required
	    LexWord lexWord = lexWordDao.marshalByToken(wordToken, connPool);
	    if (lexWord == null) {
		lexWord = new LexWord();
		lexWord.setToken(wordToken);
		lexWord.setIsStopWord(
			stopWords.getLexWordsByToken().containsKey(wordToken));
		lexWord.setIsPunct(nlpUtil.isPunctOrDelimChar(wordToken));
		lexWord.setIsAcrAbbrev(mincoWord.isAcrAbbrev);
		lexWordDao.persist(lexWord, connPool);
	    }
	    // make sure in-memory lexicon collection contains this word
	    if (!lexWordByToken.containsKey(wordToken)) {
		lexWordByToken.put(wordToken, lexWord);
	    }
	    updatesLexWordByToken.put(wordToken, lexWord);
	    // add to acronyms/abbreviations if required
	    if (mincoWord.isAcrAbbrev) {
		if (!acrAbbrevByToken.containsKey(wordToken)) {
		    AcrAbbrev acrAbbrev = new AcrAbbrev();
		    acrAbbrev.setToken(wordToken);
		    acrAbbrev.setExpanded(mincoWord.acrAbbrev.expandedForm);
		    acrAbbrev.setLexWordId(lexWord.getId());
		    acrAbbrevDao.persist(acrAbbrev, connPool);
		    acrAbbrevByToken.put(wordToken, acrAbbrev);
		}
	    }
	}

	// compile variant lexicon words
	for (String wordToken : wordTokens) {
	    LexWord lexWord = updatesLexWordByToken.get(wordToken);
	    if (!lexWordsAndVariants.containsKey(lexWord)) {
		lexWordsAndVariants.put(lexWord, new TreeSet<LexWord>());
	    }
	    if (variants.variantTokens.contains(wordToken)) {
		for (String variantToken : variants.variantTokens) {
		    LexWord lexWordVariant = updatesLexWordByToken
			    .get(variantToken);
		    lexWordsAndVariants.get(lexWord).add(lexWordVariant);
		}
	    }
	}

	// persist variants if required
	for (LexWord baseLexWord : lexWordsAndVariants.keySet()) {
	    Set<Integer> variantIds = varWordDao
		    .marshalVariantIdsForLexWord(baseLexWord.getId(), connPool);
	    for (LexWord variantLexWord : lexWordsAndVariants
		    .get(baseLexWord)) {
		if (!variantIds.contains(variantLexWord.getId())) {
		    varWordDao.persist(baseLexWord.getId(),
			    variantLexWord.getId(), connPool);
		}
	    }
	}

    }

    /**
     * Ignore word numbers parsed from each phrase since the Stanford parser
     * used to parse MetaMap phrases into words is breaking MetaMap phrases into
     * sub-phrases, hence the word numbers are incorrect for phrases.
     * 
     * @param mincoSent
     * @return
     * @throws Exception
     */
    private GoldStdSentence instantiateSent(MincoManSentence mincoSent)
	    throws Exception {

	if (mincoSent.mincoDoc.pmid == 116187 && mincoSent.sentNbr == 1) {
	    System.out.printf("Debug sentence = %1$s %n", mincoSent.text);
	}
	/*
	 * Instantiate sentence object
	 */
	String docText = abstrDao
		.marshalAbstract(mincoSent.mincoDoc.pmid, connPool)
		.getTextToIndex();
	GoldStdSentence sent = new GoldStdSentence(mincoSent.mincoDoc.pmid,
		mincoSent.sentNbr, mincoSent.beginDocPos, mincoSent.endDocPos,
		mincoSent.text, docText);

	int sentWordNbr = 0;
	/*
	 * Instantiate phrase and words for each MincoMan phrases
	 */
	for (MincoManPhrase mincoPhrase : mincoSent.mincoPhrases) {
	    GoldStdPhrase phrase = new GoldStdPhrase();
	    phrase.setPmid(mincoPhrase.mincoSent.mincoDoc.pmid);
	    phrase.setSentNbr(mincoSent.sentNbr);
	    phrase.setBeginDocPos(mincoPhrase.beginDocPos);
	    phrase.setEndDocPos(mincoPhrase.endDocPos);
	    phrase.setText(mincoPhrase.text);
	    phrase.setCuis(mincoPhrase.metaMapCuis);
	    phrase.setPosTag(mincoPhrase.phraseMincoManStr);
	    phrase.setSentPhraseNbr(mincoPhrase.phraseNbr);
	    phrase.setBeginSentWordNbr(mincoPhrase.sentFirstWordNbr);
	    phrase.setEndSentWordNbr(mincoPhrase.sentLastWordNbr);
	    int phraseWordNbr = 0;
	    for (MincoManWord mincoWord : mincoPhrase.mincoWords) {
		/*
		 * Instantiate phrase word
		 */
		GoldStdPhraseWord phraseWord = new GoldStdPhraseWord();
		phraseWord.setText(mincoWord.text);
		phraseWord.setBeginPhrasePos(mincoWord.beginPhrasePos);
		phraseWord.setBeginDocPos(mincoWord.beginDocPos);
		phraseWord.setEndPhrasePos(mincoWord.endPhrasePos);
		phraseWord.setEndDocPos(mincoWord.endDocPos);
		phraseWord.setIsAcrAbbrev(mincoWord.isAcrAbbrev);
		if (mincoWord.isAcrAbbrev) {
		    phraseWord.setAcrAbbrevId(mincoWord.acrAbbrevId);
		}
		phraseWord.setLexWordId(mincoWord.lexWordId);
		phraseWord
			.setPmid(mincoWord.mincoPhrase.mincoSent.mincoDoc.pmid);
		phraseWord.setSentPhraseNbr(mincoPhrase.phraseNbr);
		phraseWord.setPhraseWordNbr(phraseWordNbr);
		phraseWord.setSentNbr(mincoSent.sentNbr);
		phraseWord.setSentWordNbr(sentWordNbr);
		phraseWord.setPosTag(mincoWord.post);
		phraseWord.setIsPunct(mincoWord.isPunct);
		phrase.getWords().add(phraseWord);
		/*
		 * Instantiate sentence word
		 */
		GoldStdSentWord sentWord = GoldStdSentWord
			.fromPhraseWord(phraseWord);
		sent.addWord(sentWord);
		phraseWordNbr++;
		sentWordNbr++;
	    }
	    phrase.setNbrWords(phrase.getWords().size());
	    sent.getPhrases().add(phrase);
	}

	return sent;
    }

    private void retrievePmids() throws Exception {

	Set<Integer> newPmids = new HashSet<Integer>();

	if (isPreviouslyParsed) {
	    Set<Integer> prevParsedPmids = new HashSet<Integer>();
	    try (Connection conn = connPool.getConnection()) {
		// pmids previously parsed
		String prevParsedPmidSql = "SELECT DISTINCT pmid FROM conceptrecogn.goldstd_sentences";
		try (PreparedStatement prevParsedPmidStmt = conn
			.prepareStatement(prevParsedPmidSql)) {
		    try (ResultSet rs = prevParsedPmidStmt.executeQuery()) {
			while (rs.next()) {
			    prevParsedPmids.add(rs.getInt(1));
			}
		    }
		}
		String getPmidsSql = "SELECT pmid FROM conceptrecogn.goldstd_abstrcorpus where sourceCorpus=?";
		try (PreparedStatement getPmidsStmt = conn
			.prepareStatement(getPmidsSql)) {
		    getPmidsStmt.setString(1, corpus.toString());
		    try (ResultSet rs = getPmidsStmt.executeQuery()) {
			while (rs.next()) {
			    Integer pmid = rs.getInt(1);
			    if (prevParsedPmids.contains(pmid)) {
				continue;
			    }
			    newPmids.add(pmid);
			}
		    }
		}
	    }
	    nbrPmids = newPmids.size();
	    if (nbrPmids == 0) {
		throw new Exception("Could not retrieve PMIDs from database.");
	    }
	    goldStdPmids.addAll(newPmids);

	} else {
	    try (Connection conn = connPool.getConnection()) {
		String getPmidsSql = "select c.pmid, a.title, a.abstract from "
			+ "(select pmid, title, abstract from conceptrecogn.goldstd_abstracts) as a, "
			+ "(select pmid from conceptrecogn.goldstd_abstrcorpus where sourceCorpus = ?) as c "
			+ "where a.pmid = c.pmid order by c.pmid";
		try (PreparedStatement getPmidsStmt = conn
			.prepareStatement(getPmidsSql)) {
		    getPmidsStmt.setString(1, corpus.toString());
		    try (ResultSet rs = getPmidsStmt.executeQuery()) {
			while (rs.next()) {
			    goldStdPmids.add(rs.getInt(1));
			}
		    }
		}
	    }
	}

	nbrPmids = goldStdPmids.size();
	System.out.printf("Retrieved total of %1$,d PMIDs from database.%n",
		nbrPmids);
    }

    /**
     * @param args
     */
    public static void main(String[] args) {

	@SuppressWarnings("unused")
	int debugPmid = 23402;
	@SuppressWarnings("unused")
	int debugSentNbr = 0;

	try {
	    JdbcConnectionPool connPool = new JdbcConnectionPool(
		    ConnDbName.CONCEPT_RECOGN);
	    // instantiating class when persisting data
	    // note - delete existing not used, delete existing by hand (sql)
	    boolean isPrintStackTrace = true;
	    boolean isDeleteExisting = true;
	    boolean isPreviouslyParsed = false;
	    boolean isPersist = false;
	    boolean isMutateVariants = true;
	    Corpus corpus = Corpus.CLIN_CITS_TC_CORPUS;
	    PersistSentAndPhrasesAndWords persist = new PersistSentAndPhrasesAndWords(
		    corpus, isPrintStackTrace, isDeleteExisting,
		    isPreviouslyParsed, isPersist, isMutateVariants, connPool);
	    persist.persistAll();
	} catch (Exception ex) {
	    ex.printStackTrace();
	}

    }

}

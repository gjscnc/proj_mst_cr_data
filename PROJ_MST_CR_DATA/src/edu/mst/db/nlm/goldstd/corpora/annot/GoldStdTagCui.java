/**
 * 
 */
package edu.mst.db.nlm.goldstd.corpora.annot;

import java.util.Comparator;

import edu.mst.db.nlm.corpora.Corpus;

/**
 * NLM CUI associated with each gold standard tag
 * <p>
 * That is, each 'Nxx" entry found in the *.annot files that associates a NLM
 * concept ID ('CUI') with each annotation tag ('Txx').
 * 
 * @author gjs
 *
 */
public class GoldStdTagCui implements Comparable<GoldStdTagCui> {

    private int pmid = 0;
    private String referenceId = null;
    private String tagId = null;
    private String cui = null;
    private String cuiName = null;
    private Corpus corpus = null;

    public static class Comparators {
	public static Comparator<GoldStdTagCui> byPmid = (GoldStdTagCui tagCui1,
		GoldStdTagCui tagCui2) -> Integer.compare(tagCui1.pmid,
			tagCui2.pmid);
	public static Comparator<GoldStdTagCui> byRefId = (
		GoldStdTagCui tagCui1,
		GoldStdTagCui tagCui2) -> tagCui1.referenceId
			.compareTo(tagCui2.referenceId);
	public static Comparator<GoldStdTagCui> byTagId = (
		GoldStdTagCui tagCui1, GoldStdTagCui tagCui2) -> tagCui1.tagId
			.compareTo(tagCui2.tagId);
	public static Comparator<GoldStdTagCui> byPmid_RefId_TagId = (
		GoldStdTagCui tagCui1, GoldStdTagCui tagCui2) -> byPmid
			.thenComparing(byRefId).thenComparing(byTagId)
			.compare(tagCui1, tagCui2);
    }

    @Override
    public int compareTo(GoldStdTagCui tagCui) {
	return Comparators.byPmid_RefId_TagId.compare(this, tagCui);
    }

    @Override
    public boolean equals(Object obj) {
	if (obj instanceof GoldStdTagCui) {
	    GoldStdTagCui tagCui = (GoldStdTagCui) obj;
	    return pmid == tagCui.pmid && referenceId.equals(tagCui.referenceId)
		    && tagId.equals(tagCui.tagId);
	}
	return false;
    }

    @Override
    public int hashCode() {
	return Integer.hashCode(pmid) + 7 * referenceId.hashCode()
		+ 31 * tagId.hashCode();
    }

    public int getPmid() {
	return pmid;
    }

    public void setPmid(int pmid) {
	this.pmid = pmid;
    }

    public String getReferenceId() {
	return referenceId;
    }

    public void setReferenceId(String referenceId) {
	this.referenceId = referenceId;
    }

    public String getTagId() {
	return tagId;
    }

    public void setTagId(String tagId) {
	this.tagId = tagId;
    }

    public String getCui() {
	return cui;
    }

    public void setCui(String cui) {
	this.cui = cui;
    }

    public String getCuiName() {
	return cuiName;
    }

    public void setCuiName(String cuiName) {
	this.cuiName = cuiName;
    }

    public Corpus getCorpus() {
        return corpus;
    }

    public void setCorpus(Corpus corpus) {
        this.corpus = corpus;
    }

}

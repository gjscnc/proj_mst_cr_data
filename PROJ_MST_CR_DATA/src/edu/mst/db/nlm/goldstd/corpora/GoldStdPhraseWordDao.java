/**
 * 
 */
package edu.mst.db.nlm.goldstd.corpora;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.Set;

import edu.mst.db.util.JdbcConnectionPool;

/**
 * @author gjs
 *
 */
public class GoldStdPhraseWordDao {

    public static final String deleteAllPhraseWordsSql = "DELETE FROM conceptrecogn.goldstd_phraseword ";
    public static final String delAllPhraseWordsForPmidSql = "DELETE FROM conceptrecogn.goldstd_phraseword WHERE pmid = ?";
    public static final String resetPhraseWordAutoIdSql = "ALTER TABLE conceptrecogn.goldstd_phraseword AUTO_INCREMENT = 1";
    public static final String allFields = "pmid, sentNbr, sentPhraseNbr, phraseWordNbr, sentWordNbr, lexWordId, "
	    + "text, beginDocPos, endDocPos, posTag, isAcrAbbrev, acrAbbrevId, isPunct, iocTag ";
    public static final String persistPhraseWordSql = "INSERT INTO conceptrecogn.goldstd_phraseword "
	    + "(" + allFields + ") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

    public static final String getPhraseWordsSql = "SELECT " + allFields
	    + " FROM conceptrecogn.goldstd_phraseword "
	    + "WHERE pmid=? and sentNbr = ? and sentPhraseNbr = ? ORDER BY phraseWordNbr";

    public static final String updateIocTagSql = "UPDATE conceptrecogn.goldstd_phraseword "
	    + "SET iocTag=? WHERE pmid=? AND sentNbr=? AND sentWordNbr=?";

    public static final String getLexWordIdSql = "SELECT id from conceptrecogn.lexwords WHERE token = ?";

    /**
     * Persists new phrase word.
     * 
     * @param word
     * @param connPool
     * @throws Exception
     */
    public void persist(GoldStdPhraseWord word, JdbcConnectionPool connPool)
	    throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    persist(word, conn);
	}
    }

    /**
     * Persists phrase word.
     * 
     * @param word
     * @param conn
     * @throws Exception
     */
    public void persist(GoldStdPhraseWord word, Connection conn)
	    throws SQLException {

	try (PreparedStatement persistPhraseWordStmt = conn
		.prepareStatement(persistPhraseWordSql)) {
	    /*
	     * pmid, sentenceNbr, sentPhraseNbr, phraseWordNbr, sentWordNbr,
	     * lexBaseWordId, text, beginDocPos, endDocPos, posTag, isAcrAbbrev,
	     * acrAbbrevId
	     */
	    persistPhraseWordStmt.setInt(1, word.getPmid());
	    persistPhraseWordStmt.setInt(2, word.getSentNbr());
	    persistPhraseWordStmt.setInt(3, word.getSentPhraseNbr());
	    persistPhraseWordStmt.setInt(4, word.getPhraseWordNbr());
	    persistPhraseWordStmt.setInt(5, word.getSentWordNbr());
	    persistPhraseWordStmt.setInt(6, word.getLexWordId());
	    persistPhraseWordStmt.setString(7, word.getText());
	    persistPhraseWordStmt.setInt(8, word.getBeginDocPos());
	    persistPhraseWordStmt.setInt(9, word.getEndDocPos());
	    persistPhraseWordStmt.setString(10, word.getPosTag());
	    persistPhraseWordStmt.setBoolean(11, word.isAcrAbbrev());
	    persistPhraseWordStmt.setInt(12, word.getAcrAbbrevId());
	    persistPhraseWordStmt.setBoolean(13, word.isPunct());
	    persistPhraseWordStmt.setLong(14, word.getIocTag());

	    persistPhraseWordStmt.executeUpdate();

	} catch (Exception ex) {
	    String errMsg = String.format("Error persisting phrase word. "
		    + "PMID = %1$d, sent nbr = %2$d, sent phrase nbr = %3$d, "
		    + "sent word nbr = %4$d, phrase word nbr = %5$d, text = '%6$s' "
		    + "begin doc pos = %7$,d, end doc pos = %8$,d, lex base word ID = %9$d",
		    word.getPmid(), word.getSentNbr(), word.getSentPhraseNbr(),
		    word.getSentWordNbr(), word.getPhraseWordNbr(),
		    word.getText(), word.getBeginDocPos(), word.getEndDocPos(),
		    word.getLexWordId());
	    throw new SQLException(errMsg, ex);
	}

    }

    /**
     * Deletes all word objects in database
     * 
     * @param connPool
     * @return
     * @throws Exception
     */
    public static int delAllWords(JdbcConnectionPool connPool)
	    throws SQLException {
	int nbrDel = 0;
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement delAllWordsStmt = conn
		    .prepareStatement(deleteAllPhraseWordsSql)) {
		nbrDel = delAllWordsStmt.executeUpdate();
	    }
	}
	return nbrDel;
    }

    /**
     * 
     * @param pmidsToDelete
     *                          Set collection of PMIDs for sentences to delete
     * @param connPool
     * @return
     * @throws SQLException
     */
    public static int delAllWords(Set<Integer> pmidsToDelete,
	    JdbcConnectionPool connPool) throws SQLException {
	int nbrDel = 0;
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement delAllPhraseWordsForCorpusStmt = conn
		    .prepareStatement(delAllPhraseWordsForPmidSql)) {
		Iterator<Integer> pmidsIter = pmidsToDelete.iterator();
		while (pmidsIter.hasNext()) {
		    int pmid = pmidsIter.next();
		    delAllPhraseWordsForCorpusStmt.setInt(1, pmid);
		    nbrDel += delAllPhraseWordsForCorpusStmt.executeUpdate();
		}
	    }
	}
	return nbrDel;
    }
    
    public void marshalWordsForPhrase(GoldStdPhrase phrase, JdbcConnectionPool connPool)
	    throws SQLException {

	try(Connection conn = connPool.getConnection()){
	    marshalWordsForPhrase(phrase, conn);
	}
    }

    public void marshalWordsForPhrase(GoldStdPhrase phrase, Connection conn)
	    throws SQLException {

	try (PreparedStatement getPhraseWordsStmt = conn
		.prepareStatement(getPhraseWordsSql)) {
	    getPhraseWordsStmt.setInt(1, phrase.getPmid());
	    getPhraseWordsStmt.setInt(2, phrase.getSentNbr());
	    getPhraseWordsStmt.setInt(3, phrase.getSentPhraseNbr());
	    try (ResultSet wordsRs = getPhraseWordsStmt.executeQuery()) {
		while (wordsRs.next()) {
		    GoldStdPhraseWord word = instantiateFromResultSet(wordsRs);
		    phrase.getWords().add(word);
		}
	    }
	}
    }

    private GoldStdPhraseWord instantiateFromResultSet(ResultSet rs)
	    throws SQLException {
	/*
	 * 1-pmid, 2-sentNbr, 3-sentPhraseNbr, 4-phraseWordNbr, 5-sentWordNbr,
	 * 6-lexWordId, 7-text, 8-beginDocPos, 9-endDocPos, 10-posTag,
	 * 11-isAcrAbbrev, 12-acrAbbrevId, 13-isPunct, 14-iocTag
	 */
	GoldStdPhraseWord word = new GoldStdPhraseWord();
	word.setPmid(rs.getInt(1));
	word.setSentNbr(rs.getInt(2));
	word.setSentPhraseNbr(rs.getInt(3));
	word.setPhraseWordNbr(rs.getInt(4));
	word.setSentWordNbr(rs.getInt(5));
	word.setLexWordId(rs.getInt(6));
	word.setText(rs.getString(7));
	word.setBeginDocPos(rs.getInt(8));
	word.setEndDocPos(rs.getInt(9));
	word.setPosTag(rs.getString(10));
	word.setIsAcrAbbrev(rs.getBoolean(11));
	word.setAcrAbbrevId(rs.getInt(12));
	word.setIsPunct(rs.getBoolean(13));
	Long wordIocTag = rs.getLong(14);
	if (wordIocTag != null) {
	    word.setIocTag(wordIocTag);
	}

	return word;
    }

    /**
     * Persist the Inverse Ontology Cogency (IOC) tag for this word.
     * 
     * @param pmid
     * @param sentenceNbr
     * @param sentWordNbr
     * @param iocTag
     * @param connPool
     * @throws SQLException
     */
    public void updateIocTag(int pmid, int sentenceNbr, int sentWordNbr,
	    long iocTag, JdbcConnectionPool connPool) throws SQLException {
	/*
	 * SET iocTag=? WHERE pmid=? AND sentenceNbr=? AND sentWordNbr=?
	 */
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement updateIocTagStmt = conn
		    .prepareStatement(updateIocTagSql)) {
		updateIocTagStmt.setLong(1, iocTag);
		updateIocTagStmt.setInt(2, pmid);
		updateIocTagStmt.setInt(3, sentenceNbr);
		updateIocTagStmt.setInt(4, sentWordNbr);
		int nbrUpdated = 0;
		try {
		    nbrUpdated = updateIocTagStmt.executeUpdate();
		} catch (Exception ex) {
		    System.out.printf(
			    "Updating IOC tag (PMID = %1$d, sentence nbr = %2$d,"
				    + "sentence word nbr = %3$d, IOC tag = %4$2.f) failed.",
			    pmid, sentenceNbr, sentWordNbr, iocTag);
		    throw ex;
		}
		if (nbrUpdated != 1) {
		    String errMsg = String.format(
			    "Updating IOC tag (PMID = %1$d, sentence nbr = %2$d, "
				    + "sentence word nbr = %3$d, IOC tag = %4$2.f) failed.",
			    pmid, sentenceNbr, sentWordNbr, iocTag);
		    throw new SQLException(errMsg);
		}
	    }
	}
    }

}

/**
 * 
 */
package edu.mst.db.nlm.goldstd.corpora.annot;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import edu.mst.db.nlm.corpora.Corpus;
import edu.mst.db.nlm.goldstd.corpora.parse.NlmTestFileNames;
import edu.mst.db.nlm.goldstd.corpora.parse.Platform;
import edu.mst.db.util.ConnDbName;
import edu.mst.db.util.Constants;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * This class contains methods for persisting the gold standard annotations from
 * each annotation file provided by the NLM.
 * 
 * @author gjscnc
 *
 */
public class PersistNlmGoldStdAnnotWorker implements AutoCloseable {

    private boolean persistAnnot = false;
    private boolean isDebug = false;
    private boolean delExisting = false;
    private int debugPmid = Constants.uninitializedIntVal;
    private String debugAnnotId = null;
    private boolean debugMsg = false;
    private Corpus corpus = null;
    private Platform platform = null;
    private JdbcConnectionPool connPool;
    private char dirDelimChar = ' ';

    protected static int processors;
    protected static int nbrThreadsPerProcessor = 3;
    protected static ExecutorService pool;
    private CompletionService<PersistNlmGoldStdAnnotCallableResult> svc;

    private String annotFilesDir = null;
    private Map<Integer, String> annotFilesByPmid;
    private int[] pmids;
    private int nbrPmids;
    private int batchSize = 24;
    private int beginPos = 0;
    private int endPos = 0;
    private int msgInterval = 5;

    /**
     * Constructor for testing one abstract.
     * 
     * @param annotFilesDir
     * @param persistAnnot
     * @param isDebug
     * @param debugPmid
     * @param debugAnnotId
     * @param corpus
     * @param connPool
     */
    public PersistNlmGoldStdAnnotWorker(String annotFilesDir,
	    boolean persistAnnot, boolean isDebug, int debugPmid,
	    String debugAnnotId, Platform platform, Corpus corpus,
	    JdbcConnectionPool connPool) {
	this.annotFilesDir = annotFilesDir;
	this.persistAnnot = persistAnnot;
	this.isDebug = isDebug;
	this.debugPmid = debugPmid;
	this.debugAnnotId = debugAnnotId;
	this.debugMsg = true;
	this.platform = platform;
	this.corpus = corpus;
	this.connPool = connPool;
	init();
    }

    /**
     * Constructor for parsing annotations for all PMIDs.
     * 
     * @param annotFilesDir
     * @param persistAnnot
     * @param delExisting
     * @param isDebug
     * @param platform
     * @param corpus
     * @param connPool
     */
    public PersistNlmGoldStdAnnotWorker(String annotFilesDir,
	    boolean persistAnnot, boolean delExisting, boolean isDebug,
	    Platform platform, Corpus corpus, JdbcConnectionPool connPool) {
	this.annotFilesDir = annotFilesDir;
	this.persistAnnot = persistAnnot;
	this.delExisting = delExisting;
	this.isDebug = isDebug;
	this.debugMsg = false;
	this.platform = platform;
	this.corpus = corpus;
	this.connPool = connPool;
	init();
    }

    private void init() {
	processors = Runtime.getRuntime().availableProcessors();
	processors *= nbrThreadsPerProcessor;
	pool = Executors.newFixedThreadPool(processors);
	svc = new ExecutorCompletionService<PersistNlmGoldStdAnnotCallableResult>(
		pool);
	switch (platform) {
	case WINDOWS_DESKTOP:
	    dirDelimChar = NlmTestFileNames.DIR_DELIM_WINDOWS;
	    break;
	case WINDOWS_LAPTOP:
	    dirDelimChar = NlmTestFileNames.DIR_DELIM_WINDOWS;
	    break;
	case LINUX:
	    dirDelimChar = NlmTestFileNames.DIR_DELIM_LINUX;
	    break;
	}
    }

    public void persistGoldStdAnnot() throws Exception {

	/*
	 * Deleting existing deletes ALL annotations, regardless of corpora
	 */
	if (delExisting) {
	    GoldStdAnnotUtil.deleteAllAnnot(connPool);
	}

	if (isDebug && debugPmid != Constants.uninitializedIntVal) {
	    String fullFileName = String.format("%1$s%2$sPMID-%3$d.%4$s",
		    annotFilesDir, String.valueOf(dirDelimChar), debugPmid,
		    NlmTestFileNames.ANNOTATION_EXT);
	    annotFilesByPmid = new HashMap<Integer, String>();
	    annotFilesByPmid.put(debugPmid, fullFileName);
	} else {
	    NlmTestFileNames fileNames = new NlmTestFileNames(platform, corpus);
	    fileNames.setFilesDir(annotFilesDir);
	    fileNames.compileNlmFileNames();
	    annotFilesByPmid = fileNames.getAnnotFullNamesByPmid();
	}

	nbrPmids = annotFilesByPmid.keySet().size();
	pmids = new int[nbrPmids];
	int pos = 0;
	for (int pmid : annotFilesByPmid.keySet()) {
	    pmids[pos] = pmid;
	    pos++;
	}
	Arrays.sort(pmids);
	System.out.printf("Total of %1$,d abstracts will be parsed. %n",
		pmids.length);

	int nbrAbstractsParsed = 0;
	int nbrAnnotParsed = 0;
	int nbrAnnotAfterTrim = 0;
	int nbrAnnotPersisted = 0;
	while (beginPos < nbrPmids) {

	    endPos = getNextBatchMaxPos();

	    for (int i = beginPos; i <= endPos; i++) {
		int pmid = pmids[i];
		String annotFullFileName = annotFilesByPmid.get(pmid);
		PersistNlmGoldStdAnnotCallable callable;
		if (isDebug) {
		    callable = new PersistNlmGoldStdAnnotCallable(pmid,
			    annotFullFileName, persistAnnot, isDebug,
			    debugAnnotId, debugMsg, corpus, connPool);
		} else {
		    callable = new PersistNlmGoldStdAnnotCallable(pmid,
			    annotFullFileName, persistAnnot, corpus, connPool);
		}
		svc.submit(callable);
	    }

	    for (int i = beginPos; i <= endPos; i++) {
		Future<PersistNlmGoldStdAnnotCallableResult> future = svc
			.take();
		PersistNlmGoldStdAnnotCallableResult result = future.get();
		if (isDebug && debugPmid != Constants.uninitializedIntVal) {
		    GoldStdAnnotUtil.printResult(result.annotations,
			    connPool);
		}
		nbrAbstractsParsed++;
		nbrAnnotParsed += result.nbrGoldStdAnnotIdentified;
		nbrAnnotAfterTrim += result.annotations.size();
		nbrAnnotPersisted += result.nbrGoldStdAnnotPersisted;
		if (nbrAbstractsParsed % msgInterval == 0) {
		    System.out.printf("Total of %1$,d abstracts parsed, "
			    + "%2$,d annotations parsed from abstracts, "
			    + "%3$,d annotations remain after trimming and removing duplicates, "
			    + "%4$,d annotations persisted. %n",
			    nbrAbstractsParsed, nbrAnnotParsed,
			    nbrAnnotAfterTrim, nbrAnnotPersisted);
		}
	    }

	    beginPos = endPos + 1;
	}

	System.out.printf("Grand total of %1$,d abstracts parsed, "
		+ "%2$,d annotations parsed from abstracts, "
		+ "%3$,d annotations remain after trimming and removing duplicates, "
		+ "%4$,d annotations persisted. %n", nbrAbstractsParsed,
		nbrAnnotParsed, nbrAnnotAfterTrim, nbrAnnotPersisted);

    }

    private int getNextBatchMaxPos() {
	// subtract 1 from max pos since positions are inclusive
	int maxPos = beginPos + batchSize - 1;
	if (maxPos >= nbrPmids - 1)
	    maxPos = nbrPmids - 1;
	return maxPos;
    }

    protected void shutdownAndAwaitTermination(ExecutorService pool) {
	// pool.shutdown(); // Disable new tasks from being submitted
	pool.shutdownNow();
	try {
	    // Wait a while for existing tasks to terminate
	    if (!pool.awaitTermination(5, TimeUnit.SECONDS)) {
		pool.shutdownNow(); // Cancel currently executing tasks
		// Wait a while for tasks to respond to being cancelled
		if (!pool.awaitTermination(15, TimeUnit.SECONDS))
		    System.err.println("Pool did not terminate");
	    }
	} catch (InterruptedException ie) {
	    // (Re-)Cancel if current thread also interrupted
	    pool.shutdownNow();
	    // Preserve interrupt status
	    Thread.currentThread().interrupt();
	}
    }

    @Override
    public void close() throws Exception {
	this.shutdownAndAwaitTermination(pool);
	System.out.println("Parallel processor closed");
    }

    /**
     * @param args
     */
    public static void main(String[] args) {

	boolean persistAnnot = true;
	boolean delExisting = false;
	boolean isDebug = false;
	int debugPmid = 1577763;
	String debugAnnotId = "T1";
	Platform platform = Platform.LINUX;
	Corpus corpus = Corpus.NCBI_DISEASE_CORPUS;

	try {

	    JdbcConnectionPool connPool = new JdbcConnectionPool(
		    ConnDbName.CONCEPT_RECOGN);
	    NlmTestFileNames fileName = new NlmTestFileNames(platform, corpus);
	    String filesDir = fileName.getFilesDir();
	    
	    if (isDebug) {
		try (PersistNlmGoldStdAnnotWorker worker = new PersistNlmGoldStdAnnotWorker(
			filesDir, persistAnnot, isDebug, debugPmid,
			debugAnnotId, platform, corpus, connPool)) {
		    worker.persistGoldStdAnnot();
		}
	    } else {
		try (PersistNlmGoldStdAnnotWorker worker = new PersistNlmGoldStdAnnotWorker(
			filesDir, persistAnnot, delExisting, isDebug, platform,
			corpus, connPool)) {
		    worker.persistGoldStdAnnot();
		}
	    }
	} catch (Exception ex) {
	    ex.printStackTrace();
	}

    }

}

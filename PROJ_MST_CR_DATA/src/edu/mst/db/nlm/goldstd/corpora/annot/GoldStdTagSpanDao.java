/**
 * 
 */
package edu.mst.db.nlm.goldstd.corpora.annot;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.SortedSet;
import java.util.TreeSet;

import edu.mst.db.nlm.corpora.Corpus;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * @author gjs
 *
 */
public class GoldStdTagSpanDao {

    public static final String allFields = "pmid, tagId, spanNbr, category, beginPos, endPos, text, corpus";

    public static final String deleteAllSql = "truncate table conceptrecogn.goldstd_tagspans";

    public static final String persistSql = "insert into conceptrecogn.goldstd_tagspans ("
	    + allFields + ") VALUES(?,?,?,?,?,?,?,?) ";

    public static final String marshalForPmidSql = "select " + allFields
	    + " from conceptrecogn.goldstd_tagspans where pmid=?";

    public static final String marshalForPmidTagSql = "select " + allFields
	    + " from conceptrecogn.goldstd_tagspans where pmid=? and tagId=?";

    public static final String marshalPmidsSql = "select distinct pmid from conceptrecogn.goldstd_tagspans";

    public static void deleteAll(JdbcConnectionPool connPool)
	    throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement deleteStmt = conn
		    .prepareStatement(deleteAllSql)) {
		deleteStmt.executeUpdate();
	    }
	}
    }

    public void persist(GoldStdTagSpan tag, JdbcConnectionPool connPool)
	    throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement persistStmt = conn
		    .prepareStatement(persistSql)) {
		setPersistValues(tag, persistStmt);
		persistStmt.executeUpdate();
	    }
	}
    }

    public void persistBatch(Collection<GoldStdTagSpan> tags,
	    JdbcConnectionPool connPool) throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement persistStmt = conn
		    .prepareStatement(persistSql)) {
		for (GoldStdTagSpan tag : tags) {
		    setPersistValues(tag, persistStmt);
		    persistStmt.addBatch();
		}
		persistStmt.executeBatch();
	    }
	}
    }

    private void setPersistValues(GoldStdTagSpan tag,
	    PreparedStatement persistStmt) throws SQLException {
	/*
	 * 1-pmid, 2-tagId, 3-spanNbr, 4-category, 5-beginPos, 6-endPos, 7-text,
	 * 8-corpus
	 */
	persistStmt.setInt(1, tag.getPmid());
	persistStmt.setString(2, tag.getTagId());
	persistStmt.setInt(3, tag.getSpanNbr());
	persistStmt.setString(4, tag.getCategory());
	persistStmt.setInt(5, tag.getBeginPos());
	persistStmt.setInt(6, tag.getEndPos());
	persistStmt.setString(7, tag.getText());
	persistStmt.setString(8, tag.getCorpus().toString());
    }

    public SortedSet<GoldStdTagSpan> marshal(int pmid,
	    JdbcConnectionPool connPool) throws SQLException {
	SortedSet<GoldStdTagSpan> tags = new TreeSet<GoldStdTagSpan>();
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement marshalStmt = conn
		    .prepareStatement(marshalForPmidSql)) {
		marshalStmt.setInt(1, pmid);
		try (ResultSet rs = marshalStmt.executeQuery()) {
		    while (rs.next()) {
			GoldStdTagSpan tag = instantiateFromResultSet(rs);
			tags.add(tag);
		    }
		}
	    }
	}
	return tags;
    }

    public SortedSet<GoldStdTagSpan> marshal(int pmid, String tagId,
	    JdbcConnectionPool connPool) throws SQLException {
	SortedSet<GoldStdTagSpan> tags = new TreeSet<GoldStdTagSpan>();
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement marshalStmt = conn
		    .prepareStatement(marshalForPmidTagSql)) {
		marshalStmt.setInt(1, pmid);
		marshalStmt.setString(2, tagId);
		try (ResultSet rs = marshalStmt.executeQuery()) {
		    while (rs.next()) {
			GoldStdTagSpan tag = instantiateFromResultSet(rs);
			tags.add(tag);
		    }
		}
	    }
	}
	return tags;
    }

    private GoldStdTagSpan instantiateFromResultSet(ResultSet rs)
	    throws SQLException {
	/*
	 * 1-pmid, 2-tagId, 3-spanNbr, 4-category, 5-beginPos, 6-endPos, 7-text,
	 * 8-corpus
	 */
	GoldStdTagSpan tag = new GoldStdTagSpan();
	tag.setPmid(rs.getInt(1));
	tag.setTagId(rs.getString(2));
	tag.setSpanNbr(rs.getInt(3));
	tag.setCategory(rs.getString(4));
	tag.setBeginPos(rs.getInt(5));
	tag.setEndPos(rs.getInt(6));
	tag.setText(rs.getString(7));
	tag.setCorpus(Corpus.valueOf(rs.getString(8)));
	return tag;
    }

    public int[] marshalPmids(JdbcConnectionPool connPool) throws SQLException {
	SortedSet<Integer> pmidSet = new TreeSet<Integer>();
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement marshalStmt = conn
		    .prepareStatement(marshalPmidsSql)) {
		try (ResultSet rs = marshalStmt.executeQuery()) {
		    while (rs.next()) {
			pmidSet.add(rs.getInt(1));
		    }
		}
	    }
	}
	int nbrPmids = pmidSet.size();
	int[] pmids = new int[nbrPmids];
	int pos = 0;
	for (int pmid : pmidSet) {
	    pmids[pos] = pmid;
	    pos++;
	}
	return pmids;
    }

}

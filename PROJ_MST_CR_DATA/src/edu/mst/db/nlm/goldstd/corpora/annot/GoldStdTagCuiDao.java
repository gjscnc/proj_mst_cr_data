/**
 * 
 */
package edu.mst.db.nlm.goldstd.corpora.annot;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.SortedSet;
import java.util.TreeSet;

import edu.mst.db.nlm.corpora.Corpus;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * @author gjs
 *
 */
public class GoldStdTagCuiDao {

    public static final String allFields = "pmid, referenceId, tagId, cui, cuiName, corpus";

    public static final String deleteAllSql = "truncate table conceptrecogn.goldstd_tagcuis";

    public static final String persistSql = "insert into conceptrecogn.goldstd_tagcuis ("
	    + allFields + ") VALUES(?,?,?,?,?,?) ";

    public static final String marshalForPmidSql = "select " + allFields
	    + " from conceptrecogn.goldstd_tagcuis where pmid=?";

    public static final String marshalForPmidAndTagSql = "select " + allFields
	    + " from conceptrecogn.goldstd_tagcuis where pmid=? and tagId=?";

    public static void deleteAll(JdbcConnectionPool connPool)
	    throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement deleteStmt = conn
		    .prepareStatement(deleteAllSql)) {
		deleteStmt.executeUpdate();
	    }
	}
    }

    public void persist(GoldStdTagCui tagCui, JdbcConnectionPool connPool)
	    throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement persistStmt = conn
		    .prepareStatement(persistSql)) {
		setPersistValues(tagCui, persistStmt);
		persistStmt.executeUpdate();
	    }
	}
    }

    public void persistBatch(Collection<GoldStdTagCui> tagCuis,
	    JdbcConnectionPool connPool) throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement persistStmt = conn
		    .prepareStatement(persistSql)) {
		for (GoldStdTagCui tagCui : tagCuis) {
		    setPersistValues(tagCui, persistStmt);
		    persistStmt.addBatch();
		}
		persistStmt.executeBatch();
	    }
	}
    }

    private void setPersistValues(GoldStdTagCui tagCui,
	    PreparedStatement persistStmt) throws SQLException {
	persistStmt.setInt(1, tagCui.getPmid());
	persistStmt.setString(2, tagCui.getReferenceId());
	persistStmt.setString(3, tagCui.getTagId());
	persistStmt.setString(4, tagCui.getCui());
	persistStmt.setString(5, tagCui.getCuiName());
	persistStmt.setString(6, tagCui.getCorpus().toString());
    }

    public SortedSet<GoldStdTagCui> marshal(int pmid,
	    JdbcConnectionPool connPool) throws SQLException {
	SortedSet<GoldStdTagCui> tagCuis = new TreeSet<GoldStdTagCui>();
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement marshalStmt = conn
		    .prepareStatement(marshalForPmidSql)) {
		marshalStmt.setInt(1, pmid);
		try (ResultSet rs = marshalStmt.executeQuery()) {
		    while (rs.next()) {
			GoldStdTagCui tagCui = instantiateFromResultSet(rs);
			tagCuis.add(tagCui);
		    }
		}
	    }
	}
	return tagCuis;
    }

    public SortedSet<GoldStdTagCui> marshal(int pmid, String tagId,
	    JdbcConnectionPool connPool) throws SQLException {
	SortedSet<GoldStdTagCui> tagCuis = new TreeSet<GoldStdTagCui>();
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement marshalStmt = conn
		    .prepareStatement(marshalForPmidAndTagSql)) {
		marshalStmt.setInt(1, pmid);
		marshalStmt.setString(2, tagId);
		try (ResultSet rs = marshalStmt.executeQuery()) {
		    while (rs.next()) {
			GoldStdTagCui tagCui = instantiateFromResultSet(rs);
			tagCuis.add(tagCui);
		    }
		}
	    }
	}
	return tagCuis;
    }

    private GoldStdTagCui instantiateFromResultSet(ResultSet rs)
	    throws SQLException {
	/*
	 * pmid, referenceId, tagId, cui, cuiName
	 */
	GoldStdTagCui tagCui = new GoldStdTagCui();
	tagCui.setPmid(rs.getInt(1));
	tagCui.setReferenceId(rs.getString(2));
	tagCui.setTagId(rs.getString(3));
	tagCui.setCui(rs.getString(4));
	tagCui.setCuiName(rs.getString(5));
	tagCui.setCorpus(Corpus.valueOf(rs.getString(6)));
	return tagCui;
    }

}

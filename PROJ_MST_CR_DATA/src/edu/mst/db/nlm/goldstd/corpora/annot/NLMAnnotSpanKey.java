package edu.mst.db.nlm.goldstd.corpora.annot;

public class NLMAnnotSpanKey implements Comparable<NLMAnnotSpanKey> {

    private int pmid;
    private int annotId;
    private int annotType;
    private int annotSubType;
    private int beginPos;

    public NLMAnnotSpanKey(int pmid, int annotId, int annotType,
	    int annotSubType, int beginPos) {
	this.pmid = pmid;
	this.annotId = annotId;
	this.annotType = annotType;
	this.annotSubType = annotSubType;
	this.beginPos = beginPos;
    }

    @Override
    public int compareTo(NLMAnnotSpanKey annotKey) {
	int i = Integer.valueOf(pmid).compareTo(Integer.valueOf(annotKey.pmid));
	if (i != 0)
	    return i;
	i = Integer.valueOf(annotId).compareTo(
		Integer.valueOf(annotKey.annotId));
	if (i != 0)
	    return i;
	i = Integer.valueOf(annotType).compareTo(
		Integer.valueOf(annotKey.annotType));
	if (i != 0)
	    return i;
	i = Integer.valueOf(annotSubType).compareTo(
		Integer.valueOf(annotKey.annotSubType));
	if (i != 0)
	    return i;
	return Integer.valueOf(beginPos).compareTo(
		Integer.valueOf(annotKey.beginPos));
    }

    @Override
    public boolean equals(Object obj) {
	if (obj instanceof NLMAnnotSpanKey) {
	    NLMAnnotSpanKey annotKey = (NLMAnnotSpanKey) obj;
	    return pmid == annotKey.pmid && annotId == annotKey.annotId
		    && annotType == annotKey.annotType
		    && annotSubType == annotKey.annotSubType
		    && beginPos == annotKey.beginPos;
	}
	return false;
    }

    @Override
    public int hashCode() {
	return Integer.valueOf(pmid).hashCode() + 7
		* Integer.valueOf(annotId).hashCode() + 13
		* Integer.valueOf(annotType).hashCode() + 17
		* Integer.valueOf(annotSubType).hashCode() + 23
		* Integer.valueOf(beginPos).hashCode();
    }

    public int getPmid() {
	return pmid;
    }

    public int getAnnotId() {
	return annotId;
    }

    public int getAnnotType() {
	return annotType;
    }

    public int getAnnotSubType() {
	return annotSubType;
    }

    public int getBeginPos() {
        return beginPos;
    }

}

package edu.mst.db.nlm.goldstd.corpora.parse;

public class Normalization {

    public int pmid;
    public String normID;
    public int annotationID;
    public String cui;
    public String conceptName;
    
}

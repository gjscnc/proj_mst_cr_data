/**
 * 
 */
package edu.mst.db.nlm.goldstd.corpora.annot;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.SortedSet;
import java.util.TreeSet;

import edu.mst.db.util.JdbcConnectionPool;

/**
 * @author gjs
 *
 */
public class GoldStdTagNameMatchDao {

    public static final String allFields = "pmid, tagId, conceptNameUid, conceptUid, conceptNameText, cui, aui, taggedText, auiText";

    public static final String deleteAllSql = "truncate table conceptrecogn.goldstd_tag_namematches";

    public static final String persistSql = "insert into conceptrecogn.goldstd_tag_namematches ("
	    + allFields + ") values (?,?,?,?,?,?,?,?,?)";

    public static final String marshalAllSql = "select " + allFields
	    + " from conceptrecogn.goldstd_tag_namematches";

    public static final String marshalForPmidSql = "select " + allFields
	    + " from conceptrecogn.goldstd_tag_namematches where pmid=?";

    public static final String marshalForPmidTagSql = "select " + allFields
	    + " from conceptrecogn.goldstd_tag_namematches where pmid=? and tagId=?";

    public static final String marshalForNameUidSql = "select " + allFields
	    + " from conceptrecogn.goldstd_tag_namematches where conceptNameUid=?";

    public static void deleteAll(JdbcConnectionPool connPool)
	    throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement deleteStmt = conn
		    .prepareStatement(deleteAllSql)) {
		deleteStmt.executeUpdate();
	    }
	}
    }

    public void persist(GoldStdTagNameMatch match, JdbcConnectionPool connPool)
	    throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement persistStmt = conn
		    .prepareStatement(persistSql)) {
		setPersistValues(match, persistStmt);
		persistStmt.executeUpdate();
	    }
	}
    }

    public void persist(Collection<GoldStdTagNameMatch> matches,
	    JdbcConnectionPool connPool) throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement persistStmt = conn
		    .prepareStatement(persistSql)) {
		for (GoldStdTagNameMatch match : matches) {
		    setPersistValues(match, persistStmt);
		    persistStmt.addBatch();
		}
		persistStmt.executeBatch();
	    }
	}
    }

    private void setPersistValues(GoldStdTagNameMatch match,
	    PreparedStatement persistStmt) throws SQLException {
	/*
	 * 1-pmid, 2-tagId, 3-conceptNameUid, 4-conceptUid, 5-conceptNameText,
	 * 6-cui, 7-aui, 8-taggedText, 9-auiText
	 */
	persistStmt.setInt(1, match.getPmid());
	persistStmt.setString(2, match.getTagId());
	persistStmt.setLong(3, match.getConceptNameUid());
	persistStmt.setLong(4, match.getConceptUid());
	persistStmt.setString(5, match.getConceptNameText());
	persistStmt.setString(6, match.getCui());
	persistStmt.setString(7, match.getAui());
	persistStmt.setString(8, match.getTaggedText());
	persistStmt.setString(9, match.getAuiText());
    }

    public SortedSet<GoldStdTagNameMatch> marshalAll(
	    JdbcConnectionPool connPool) throws SQLException {
	SortedSet<GoldStdTagNameMatch> matches = new TreeSet<GoldStdTagNameMatch>();
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement marshalStmt = conn
		    .prepareStatement(marshalAllSql)) {
		try (ResultSet rs = marshalStmt.executeQuery()) {
		    while (rs.next()) {
			GoldStdTagNameMatch match = instantiateFromResultSet(
				rs);
			matches.add(match);
		    }
		}
	    }
	}
	return matches;
    }

    public SortedSet<GoldStdTagNameMatch> marshal(int pmid,
	    JdbcConnectionPool connPool) throws SQLException {
	SortedSet<GoldStdTagNameMatch> matches = new TreeSet<GoldStdTagNameMatch>();
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement marshalStmt = conn
		    .prepareStatement(marshalForPmidSql)) {
		marshalStmt.setInt(1, pmid);
		try (ResultSet rs = marshalStmt.executeQuery()) {
		    while (rs.next()) {
			GoldStdTagNameMatch match = instantiateFromResultSet(
				rs);
			matches.add(match);
		    }
		}
	    }
	}
	return matches;
    }

    public SortedSet<GoldStdTagNameMatch> marshal(int pmid, String tagId,
	    JdbcConnectionPool connPool) throws SQLException {
	SortedSet<GoldStdTagNameMatch> matches = new TreeSet<GoldStdTagNameMatch>();
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement marshalStmt = conn
		    .prepareStatement(marshalForPmidTagSql)) {
		marshalStmt.setInt(1, pmid);
		marshalStmt.setString(2, tagId);
		try (ResultSet rs = marshalStmt.executeQuery()) {
		    while (rs.next()) {
			GoldStdTagNameMatch match = instantiateFromResultSet(
				rs);
			matches.add(match);
		    }
		}
	    }
	}
	return matches;
    }

    public SortedSet<GoldStdTagNameMatch> marshal(long conceptNameUid,
	    JdbcConnectionPool connPool) throws SQLException {
	SortedSet<GoldStdTagNameMatch> matches = new TreeSet<GoldStdTagNameMatch>();
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement marshalStmt = conn
		    .prepareStatement(marshalForNameUidSql)) {
		marshalStmt.setLong(1, conceptNameUid);
		try (ResultSet rs = marshalStmt.executeQuery()) {
		    while (rs.next()) {
			GoldStdTagNameMatch match = instantiateFromResultSet(
				rs);
			matches.add(match);
		    }
		}
	    }
	}
	return matches;
    }

    private GoldStdTagNameMatch instantiateFromResultSet(ResultSet rs)
	    throws SQLException {
	/*
	 * 1-pmid, 2-tagId, 3-conceptNameUid, 4-conceptUid, 5-conceptNameText,
	 * 6-cui, 7-aui, 8-taggedText, 9-auiText
	 */
	GoldStdTagNameMatch match = new GoldStdTagNameMatch();
	match.setPmid(rs.getInt(1));
	match.setTagId(rs.getString(2));
	match.setConceptNameUid(rs.getLong(3));
	match.setConceptUid(rs.getLong(4));
	match.setConceptNameText(rs.getString(5));
	match.setCui(rs.getString(6));
	match.setAui(rs.getString(7));
	match.setTaggedText(rs.getString(8));
	match.setAuiText(rs.getString(9));
	return match;
    }

    /*
     * SELECT pmid, tagId, conceptNameUid, conceptUid, conceptNameText, cui FROM
     * conceptrecogn.goldstd_tag_namematches;
     */
}

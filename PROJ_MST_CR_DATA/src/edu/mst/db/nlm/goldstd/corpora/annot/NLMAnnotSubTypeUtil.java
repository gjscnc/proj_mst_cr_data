package edu.mst.db.nlm.goldstd.corpora.annot;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class NLMAnnotSubTypeUtil {

    private static Set<NLMAnnotSubType> allSubTypes;
    private static Set<Integer> allIds;
    private static Set<String> allSubTypeText;
    private static Map<String, NLMAnnotSubType> subTypeByText;
    private static Map<Integer, NLMAnnotSubType> subTypeById;

    private static void loadCollections(){
	if(allSubTypes == null){
	    allSubTypes = new HashSet<NLMAnnotSubType>();
	    allSubTypes.add(NLMAnnotSubType.DISORDER);
	    allSubTypes.add(NLMAnnotSubType.GENERAL_DISORDER);
	    allSubTypes.add(NLMAnnotSubType.OVERLY_GENERAL_DISORDER);
	    allSubTypes.add(NLMAnnotSubType.METAMAP_FULL);
	    allSubTypes.add(NLMAnnotSubType.METAMAP_LITE);
	    allSubTypes.add(NLMAnnotSubType.OIC);
	}
	if(allIds == null){
	    allIds = new HashSet<Integer>();
	    for(NLMAnnotSubType subType : allSubTypes){
		allIds.add(subType.getAnnotSubTypeId());
	    }
	}
	if(allSubTypeText == null){
	    allSubTypeText = new HashSet<String>();
	    for(NLMAnnotSubType subType : allSubTypes){
		allSubTypeText.add(subType.getAnnotText());
	    }
	}
	if(subTypeByText == null){
	    subTypeByText = new HashMap<String, NLMAnnotSubType>();
	    for(NLMAnnotSubType subType : allSubTypes){
		subTypeByText.put(subType.getAnnotText(), subType);
	    }
	}
	if(subTypeById == null){
	    subTypeById = new HashMap<Integer, NLMAnnotSubType>();
	    for(NLMAnnotSubType subType : allSubTypes){
		subTypeById.put(subType.getAnnotSubTypeId(), subType);
	    }
	}
    }

    public static NLMAnnotSubType getSubTypeForText(String text) {
	if(subTypeByText == null)
	    loadCollections();
	return subTypeByText.get(text);
    }
    
    public static NLMAnnotSubType getSubTypeForId(int subTypeId){
	if(subTypeById == null)
	    loadCollections();
	return subTypeById.get(subTypeId);
    }
    
    public static Set<NLMAnnotSubType> getAllSubTypes() {
	if(allSubTypes == null)
	    loadCollections();
	return allSubTypes;
    }

    public static Set<Integer> getAllIds() {
	if(allIds == null)
	    loadCollections();
	return allIds;
    }

    public static Set<String> getAllSubTypeText() {
	if(allSubTypeText == null)
	    loadCollections();
	return allSubTypeText;
    }

}

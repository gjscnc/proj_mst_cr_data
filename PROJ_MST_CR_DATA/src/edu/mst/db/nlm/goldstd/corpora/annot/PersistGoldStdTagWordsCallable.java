/**
 * 
 */
package edu.mst.db.nlm.goldstd.corpora.annot;

import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.Callable;

import edu.mst.db.nlm.corpora.NlmAbstractWord;
import edu.mst.db.nlm.corpora.NlmAbstractWordDao;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * @author gjs
 *
 */
public class PersistGoldStdTagWordsCallable implements Callable<Integer> {

    private int pmid = 0;
    private boolean isPersist = false;
    private JdbcConnectionPool connPool = null;

    private SortedMap<Integer, NlmAbstractWord> abstrWordByBeginPos = new TreeMap<Integer, NlmAbstractWord>();
    private SortedMap<Integer, NlmAbstractWord> abstrWordByEndPos = new TreeMap<Integer, NlmAbstractWord>();
    private NlmAbstractWordDao abstrWordDao = new NlmAbstractWordDao();

    private GoldStdTagSpanDao tagDao = new GoldStdTagSpanDao();
    private GoldStdTagWordDao tagWordDao = new GoldStdTagWordDao();
    private GoldStdTagCuiDao tagCuiDao = new GoldStdTagCuiDao();

    private SortedMap<String, SortedSet<GoldStdTagSpan>> tagSpansByTagId = new TreeMap<String, SortedSet<GoldStdTagSpan>>();

    private SortedSet<GoldStdTagWord> newTagWords = new TreeSet<GoldStdTagWord>();

    public PersistGoldStdTagWordsCallable(int pmid, boolean isPersist,
	    JdbcConnectionPool connPool) {
	this.pmid = pmid;
	this.isPersist = isPersist;
	this.connPool = connPool;
    }

    @Override
    public Integer call() throws Exception {

	// retrieve abstract words by position
	{
	    SortedSet<NlmAbstractWord> abstrWords = abstrWordDao
		    .marshalWords(pmid, connPool);
	    for (NlmAbstractWord abstrWord : abstrWords) {
		abstrWordByBeginPos.put(abstrWord.getBeginOffset(), abstrWord);
		abstrWordByEndPos.put(abstrWord.getEndOffset(), abstrWord);
	    }
	}

	{
	    SortedSet<GoldStdTagSpan> tagSpans = tagDao.marshal(pmid, connPool);
	    //SortedSet<GoldStdTagSpan> tags = tagDao.marshal(pmid, testTagId, connPool);
	    for (GoldStdTagSpan tagSpan : tagSpans) {
		String tagId = tagSpan.getTagId();
		if (!tagSpansByTagId.containsKey(tagId)) {
		    tagSpansByTagId.put(tagId, new TreeSet<GoldStdTagSpan>());
		}
		tagSpansByTagId.get(tagId).add(tagSpan);
	    }
	}

	for (String tagId : tagSpansByTagId.keySet()) {
	    SortedSet<GoldStdTagCui> cuis = tagCuiDao.marshal(pmid, tagId,
		    connPool);
	    if (cuis.size() == 0) {
		continue; // no CUI provided in NLM tags
	    }
	    String cui = cuis.first().getCui();
	    SortedSet<GoldStdTagSpan> tagSpans = tagSpansByTagId.get(tagId);
	    for (GoldStdTagSpan tagSpan : tagSpans) {
		int tagBeginPos = tagSpan.getBeginPos();
		int tagEndPos = tagSpan.getEndPos();
		SortedMap<Integer, NlmAbstractWord> headMap = null;
		if (tagBeginPos > 0) {
		    headMap = abstrWordByBeginPos.tailMap(tagBeginPos);
		} else {
		    headMap = abstrWordByBeginPos;
		}
		Integer firstKey = headMap.firstKey();
		NlmAbstractWord firstWord = headMap.get(firstKey);
		SortedSet<NlmAbstractWord> matchedWords = new TreeSet<NlmAbstractWord>();
		matchedWords.add(firstWord);
		for (Integer keyPos : headMap.keySet()) {
		    NlmAbstractWord nextWord = headMap.get(keyPos);
		    if (nextWord.equals(firstWord)) {
			continue;
		    }
		    if (nextWord.getBeginOffset() >= tagEndPos) {
			break;
		    }
		    matchedWords.add(nextWord);
		}
		for (NlmAbstractWord matchedAbstrWord : matchedWords) {
		    GoldStdTagWord tagWord = new GoldStdTagWord();
		    tagWord.setPmid(pmid);
		    tagWord.setTagId(tagId);
		    tagWord.setSentNbr(matchedAbstrWord.getSentNbr());
		    tagWord.setSentWordNbr(matchedAbstrWord.getSentWordNbr());
		    tagWord.setLexWordId(matchedAbstrWord.getLexWordId());
		    tagWord.setToken(matchedAbstrWord.getToken());
		    tagWord.setBeginPos(matchedAbstrWord.getBeginOffset());
		    tagWord.setEndPos(matchedAbstrWord.getEndOffset());
		    tagWord.setCui(cui);
		    tagWord.setStopWord(matchedAbstrWord.isStopWord());
		    tagWord.setPunctOrDelimChar(matchedAbstrWord.isPunctOrDelimChar());
		    newTagWords.add(tagWord);
		}
	    }
	}

	if (isPersist) {
	    tagWordDao.persist(newTagWords, connPool);
	}

	return newTagWords.size();
    }

}

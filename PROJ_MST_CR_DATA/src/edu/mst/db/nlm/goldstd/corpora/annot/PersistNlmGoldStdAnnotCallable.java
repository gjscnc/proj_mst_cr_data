package edu.mst.db.nlm.goldstd.corpora.annot;

import java.io.BufferedReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import edu.mst.db.nlm.corpora.Corpus;
import edu.mst.db.nlm.goldstd.corpora.GoldStdAbstract;
import edu.mst.db.nlm.goldstd.corpora.GoldStdAbstractDao;
import edu.mst.db.nlm.goldstd.corpora.GoldStdAnnot;
import edu.mst.db.nlm.goldstd.corpora.GoldStdAnnotDao;
import edu.mst.db.nlm.goldstd.corpora.GoldStdAnnotParseSpan;
import edu.mst.db.nlm.goldstd.corpora.GoldStdAnnotWord;
import edu.mst.db.nlm.goldstd.corpora.GoldStdPhrase;
import edu.mst.db.nlm.goldstd.corpora.GoldStdPhraseDao;
import edu.mst.db.nlm.goldstd.corpora.GoldStdPhraseWord;
import edu.mst.db.nlm.goldstd.corpora.parse.NlmTestFileNames;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * Class for concurrent process to persist NLM gold standard annotations
 * including gold standard words.
 * 
 * @author George
 *
 */
public class PersistNlmGoldStdAnnotCallable
	implements Callable<PersistNlmGoldStdAnnotCallableResult> {

    private int pmid;
    private String annotFullFileName;
    private boolean isDebug = false;
    private boolean persistAnnot = true;
    private String debugAnnotId;
    private boolean debugMsg = false;
    private Corpus corpus = null;
    private GoldStdAnnotUtil persistAnnotUtil;
    private JdbcConnectionPool connPool;

    private Map<String, GoldStdAnnot> annotations = new HashMap<String, GoldStdAnnot>();

    /**
     * Constructor for normal use. Note that must provide full file name of the
     * annotation file.
     * 
     * @param pmid
     * @param annotFullFileName
     * @param persistAnnot
     * @param corpus
     * @param connPool
     */
    public PersistNlmGoldStdAnnotCallable(int pmid, String annotFullFileName,
	    boolean persistAnnot, Corpus corpus, JdbcConnectionPool connPool) {
	this.pmid = pmid;
	this.annotFullFileName = annotFullFileName;
	this.connPool = connPool;
	this.persistAnnot = persistAnnot;
	this.corpus = corpus;
	this.connPool = connPool;
	persistAnnotUtil = new GoldStdAnnotUtil(pmid, false, null, false,
		connPool);
    }

    /**
     * Constructor for debugging. Same as constructor for normal use only
     * includes debug parameters.
     * 
     * @param pmid
     * @param annotFullFileName
     * @param persistAnnot
     * @param isDebug
     * @param debugAnnotId
     * @param debugMsg
     * @param corpus
     * @param connPool
     */
    public PersistNlmGoldStdAnnotCallable(int pmid, String annotFullFileName,
	    boolean persistAnnot, boolean isDebug, String debugAnnotId,
	    boolean debugMsg, Corpus corpus, JdbcConnectionPool connPool) {
	this.pmid = pmid;
	this.annotFullFileName = annotFullFileName;
	this.connPool = connPool;
	this.persistAnnot = persistAnnot;
	this.isDebug = isDebug;
	this.debugAnnotId = debugAnnotId;
	this.debugMsg = debugMsg;
	this.corpus = corpus;
	persistAnnotUtil = new GoldStdAnnotUtil(pmid, isDebug, debugAnnotId,
		debugMsg, connPool);
    }

    @Override
    public PersistNlmGoldStdAnnotCallableResult call() throws Exception {

	PersistNlmGoldStdAnnotCallableResult result = new PersistNlmGoldStdAnnotCallableResult();
	result.pmid = pmid;
	result.isDebug = isDebug;

	Path file = Paths.get(annotFullFileName);
	
	GoldStdPhraseDao phraseDao = new GoldStdPhraseDao();
	GoldStdAbstractDao abstrDao = new GoldStdAbstractDao();

	try (BufferedReader reader = Files.newBufferedReader(file,
		NlmTestFileNames.CHARACTER_SET)) {

	    String text = null;
	    while ((text = reader.readLine()) != null) {
		/*
		 * First step is to marshal each line in the annotation file
		 * into two categories: 1) gold standard tags, and 2) notes with
		 * map to gold standard CUI.
		 */
		String annotId = persistAnnotUtil.parseAnnotId(text);
		if (isDebug && debugMsg && annotId.equals(debugAnnotId)) {
		    System.out.printf("%1$s unit test case %n", debugAnnotId);
		}
		GoldStdAnnot annot = null;
		GoldStdPhrase phrase = null;
		char firstChar = annotId.charAt(0);
		switch (firstChar) {
		// if annotation ID begins with a 'T' it is a "tag"
		case 'T':

		    int beginDocPos = persistAnnotUtil.parseFirstDocPos(text);
		    phrase = phraseDao.retrievePhraseAtDocPos(pmid, beginDocPos,
			    connPool);
		    if (phrase == null) {
			// String errMsg = String.format("Phrase not found. "
			// + "PMID = %1$d, begin doc position = %2$d, "
			// + "phrase database ID = %3$d", pmid,
			// beginDocPos, phraseDbId);
			// throw new Exception(errMsg);
			continue;
		    }

		    annot = new GoldStdAnnot();
		    annot.setAnnotId(annotId);
		    annot.setAnnotType(
			    persistAnnotUtil.parseAnnotType(text).toString());
		    annot.setPmid(pmid);
		    annot.setSentenceNbr(phrase.getSentNbr());
		    annot.setBeginDocPos(phrase.getBeginDocPos());
		    annot.setSentPhraseNbr(phrase.getSentPhraseNbr());
		    annot.setCorpus(corpus);
		    annot.setText(persistAnnotUtil.parseAnnotText(text));
		    annot.setSentFirstWordNbr(phrase.getBeginSentWordNbr());

		    List<GoldStdAnnotParseSpan> spans = null;
		    try {
			spans = persistAnnotUtil.parseAnnotSpans(text);
		    } catch (Exception ex) {
			String errMsg = String.format(
				"For PMID = %1$d, annotation ID = %2$s, "
					+ "error parsing text '%3$s'",
				pmid, annotId, text);
			throw new Exception(errMsg, ex);
		    }
		    for (GoldStdPhraseWord phraseWord : phrase.getWords()) {
			for (GoldStdAnnotParseSpan span : spans) {
			    int spanBegin = span.getBeginDocPos();
			    int spanEnd = span.getEndDocPos();
			    int wordBegin = phraseWord.getBeginDocPos();
			    int wordEnd = phraseWord.getEndDocPos();
			    /*
			     * if word ending position == -1 then it is set to
			     * last position of abstract, need to get length of
			     * abstract to obtain position of last char for
			     * logic below
			     */
			    if (wordEnd == -1) {
				GoldStdAbstract nlmAbstract = abstrDao
					.marshalAbstract(pmid, connPool);
				wordEnd = nlmAbstract.getText().length();
			    }
			    // instantiate annotation word
			    if (spanBegin <= wordBegin && wordEnd <= spanEnd) {
				GoldStdAnnotWord annotWord = new GoldStdAnnotWord();
				annotWord.setPmid(pmid);
				annotWord.setLexWordId(
					phraseWord.getLexWordId());
				annotWord
					.setSentNbr(annot.getSentenceNbr());
				annotWord.setSentWordNbr(
					phraseWord.getSentWordNbr());
				annotWord.setBeginDocPos(beginDocPos);
				annotWord.setSentPhraseNbr(
					phrase.getSentPhraseNbr());
				annotWord.setPhraseWordNbr(
					phraseWord.getPhraseWordNbr());
				annotWord.setSourceCorpus(corpus.toString());
				annot.getWords().add(annotWord);
			    }
			}
		    }
		    // set phrase first word number
		    if (annot.getWords() != null
			    && annot.getWords().size() > 0) {
			GoldStdAnnotWord firstWord = annot.getWords().first();
			annot.setPhraseFirstWordNbr(
				firstWord.getPhraseWordNbr());
		    }

		    annotations.put(annotId, annot);

		    break;
		case 'N':
		    if (persistAnnotUtil.textContainsGoldStdCui(text)) {
			String refAnnotId = persistAnnotUtil.getRefAnnotId(text,
				false);
			if (isDebug && debugMsg && annotId.equals(debugAnnotId))
			    System.out.printf("%1$s unit test case %n",
				    debugAnnotId);
			String cui = persistAnnotUtil.parseGoldStdCui(text);
			String cuiName = persistAnnotUtil.parseRefCuiName(text);
			boolean cuiExistsInOntol = false;
			if (cui != null && !cui.isEmpty())
			    cuiExistsInOntol = persistAnnotUtil.existsInSnomed(cui);
			if (!annotations.containsKey(refAnnotId)) {
			    String errMsg = String.format(
				    "Annotation ID = %1$s not found "
					    + "for PMID = %2$d, CUI = %3$s",
				    refAnnotId, pmid, cui);
			    throw new Exception(errMsg);
			}
			annot = annotations.get(refAnnotId);
			annot.setCui(cui);
			annot.setCuiName(cuiName);
			annot.setExistsInOntol(cuiExistsInOntol);
			long bestMatchSnomedUid = persistAnnotUtil
				.getBestMatchSnomedUid(cui);
			annot.setBestMatchSnomedConceptUid(bestMatchSnomedUid);
			result.nbrGoldStdAnnotIdentified++;
		    }
		    break;
		case '#':
		    break;
		default:
		    continue;
		}

	    }
	}

	/*
	 * trim then remove duplicates...trimming removes annotations of a type
	 * not used for precision calculations and removing duplicates removes
	 * those annotations applied to the same word set (keeping the MMI type
	 * if the duplicate is a MMLite type).
	 */
	trimAndRemoveDups();
	/*
	 * persist
	 */
	if (persistAnnot) {
	    if (annotations.size() == 0) {
		System.out.printf("No annotations found for pmid %1$d %n",
			pmid);
	    } else {
		GoldStdAnnotDao annotDao = new GoldStdAnnotDao();
		for (String annotId : annotations.keySet()) {
		    GoldStdAnnot annot = annotations.get(annotId);
		    annotDao.persist(annot, connPool);
		    result.nbrGoldStdAnnotPersisted++;
		}
	    }
	}

	result.annotations = annotations;

	return result;
    }

    /**
     * Removes annotations that are unrelated to the gold standard annotations
     * desired for estimating IOC precision. This limits annotations to types
     * Disorder and Disease. All others are deleted.
     * <p>
     * In addition any annotation that does not have a matching CUI is deleted.
     * 
     * @throws Exception
     */
    public void trimAnnots() throws Exception {
	List<String> annotIdsToDelete = new ArrayList<String>();
	for (String annotId : annotations.keySet()) {
	    GoldStdAnnot annot = annotations.get(annotId);
	    GoldStdType type = GoldStdType.valueOf(annot.getAnnotType());
	    switch (type) {
	    case Disorder:
	    case Disease:
		break;
	    default:
		annotIdsToDelete.add(annotId);
	    }
	    // delete annotations without matching CUI
	    String cui = annot.getCui();
	    if (cui == null || cui.isEmpty())
		annotIdsToDelete.add(annotId);
	}
	for (String annotId : annotIdsToDelete) {
	    annotations.remove(annotId);
	}
    }

    /**
     * This method performs two functions. It trims annotations that are
     * unrelated to the gold standard annotations desired for estimating IOC
     * precision. Then it removes duplicate annotations. Each word in a sentence
     * can only have one CUI concept annotation. But the NLM annotation files
     * have duplicates where the words are tagged with a different concept CUI.
     * Removal uses the order of preference of MMI preferred over MMLite.
     * <p>
     * Note that this method is for use with NLM annotations for the same
     * abstract (i.e., the same PMID).
     * 
     * @throws Exception
     */
    public void trimAndRemoveDups() throws Exception {

	// first, trim annotations
	trimAnnots();

	// get list of annotations for each sentence
	Map<Integer, List<GoldStdAnnot>> annotBySentNbr = new HashMap<Integer, List<GoldStdAnnot>>();
	for (GoldStdAnnot annot : annotations.values()) {
	    if (!annotBySentNbr.containsKey(annot.getSentenceNbr()))
		annotBySentNbr.put(annot.getSentenceNbr(),
			new ArrayList<GoldStdAnnot>());
	    annotBySentNbr.get(annot.getSentenceNbr()).add(annot);
	}

	// for each sentence remove duplicate
	for (Integer sentNbr : annotBySentNbr.keySet()) {
	    List<GoldStdAnnot> sentAnnots = annotBySentNbr.get(sentNbr);
	    sentAnnots.sort(GoldStdAnnot.Comparators.byPosition);
	    List<String> duplAnnotIds = new ArrayList<String>();
	    int nbrSentAnnot = sentAnnots.size();
	    for (int i = 0; i < nbrSentAnnot - 1; i++) {
		GoldStdAnnot annotA = sentAnnots.get(i);
		if (isDebug && debugMsg
			&& annotA.getAnnotId().equals(debugAnnotId))
		    System.out.printf("%1$s unit test case %n", debugAnnotId);
		for (int j = i + 1; j < nbrSentAnnot; j++) {
		    GoldStdAnnot annotB = sentAnnots.get(j);
		    if (isDebug && debugMsg
			    && annotA.getAnnotId().equals(debugAnnotId))
			System.out.printf("%1$s unit test case %n",
				debugAnnotId);

		    if (annotA == null || annotB == null) {
			String errMsg = String.format(
				"%1$d annotations for sentence %2$d, "
					+ "PMID %3$d, i=%4$d, j=%5$d %n",
				nbrSentAnnot, sentNbr, pmid, i, j);
			throw new Exception(errMsg);
		    }

		    boolean pmidEqual = annotA.getPmid() == annotB.getPmid();
		    boolean sentNbrEqual = annotA.getSentenceNbr() == annotB
			    .getSentenceNbr();
		    // boolean cuiEqual =
		    // annotA.getCui().equals(annotB.getCui());
		    boolean nbrWordsEqual = annotA.getWords().size() == annotB
			    .getWords().size();
		    boolean allWordsEqual = annotA.getWords().equals(annotB.getWords());
		    /*
		     * If words are equal then remove the annotation having the
		     * more abstract NLM annotation type. Random sampling of NLM
		     * annotation files found that duplicates occur when one of
		     * the annotations is of type MMI and the other is type
		     * MMLite. Preference is given to the MMI annotation since,
		     * based upon random samples, the MMI type appear to be more
		     * accurate. Code is verbose to help ensure the logic is
		     * explicit and can be debugged easier.
		     */
		    if (pmidEqual && sentNbrEqual && nbrWordsEqual
			    && allWordsEqual) {
			GoldStdAnnot keeper = null;
			GoldStdType annotAType = GoldStdType
				.valueOf(annotA.getAnnotType());
			GoldStdType annotBType = GoldStdType
				.valueOf(annotB.getAnnotType());
			// apply selection rules
			if (annotAType == GoldStdType.Disorder
				&& annotBType != GoldStdType.Disorder) {
			    keeper = annotA;
			}
			if (annotAType != GoldStdType.Disorder
				&& annotBType == GoldStdType.Disorder) {
			    keeper = annotB;
			}
			if (annotAType == GoldStdType.MMI
				&& annotBType != GoldStdType.MMI) {
			    keeper = annotA;
			}
			if (annotAType != GoldStdType.MMI
				&& annotBType == GoldStdType.MMI) {
			    keeper = annotB;
			}
			if (keeper == null)
			    keeper = annotA;
			GoldStdAnnot annotToDel = keeper.equals(annotA)
				? annotB
				: annotA;

			if (isDebug && debugMsg
				&& annotA.getAnnotId().equals(debugAnnotId))
			    System.out.printf("%1$s unit test case %n",
				    debugAnnotId);

			annotations.remove(annotToDel.getAnnotId());
		    }
		}
		for (String duplAnnotId : duplAnnotIds) {
		    if (isDebug && debugMsg
			    && annotA.getAnnotId().equals(debugAnnotId))
			System.out.printf(
				"%1$s unit test case in PersistNlmGoldStdAnnotUtil.trimAndRemoveDups() %n",
				debugAnnotId);
		    annotations.remove(duplAnnotId);
		}
	    }
	}

    }

}

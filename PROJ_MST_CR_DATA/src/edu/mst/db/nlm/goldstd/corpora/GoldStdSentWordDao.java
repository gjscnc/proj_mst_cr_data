/**
 * 
 */
package edu.mst.db.nlm.goldstd.corpora;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import edu.mst.db.nlm.metamap.MincoManWord;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * @author gjs
 *
 */
public class GoldStdSentWordDao {

    public static final String allFields = "pmid, sentNbr, sentWordNbr, beginDocPos, endDocPos, text, lexWordId, "
	    + "isAcrAbbrev, acrAbbrevId, isPunct, cumLnAbstrCogency, cumLnSentCogency ";
    public static final String persistWordSql = "INSERT INTO conceptrecogn.goldstd_sentword "
	    + "(" + allFields + ") VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
    public static final String marshalWordSql = "SELECT " + allFields
	    + " from conceptrecogn.goldstd_sentword where pmid = ? and sentNbr = ? and sentWordNbr = ? ";
    public static final String marshalWordsForSentSql = "SELECT " + allFields
	    + " from conceptrecogn.goldstd_sentword where pmid = ? and sentNbr = ? ";
    private static final String delAllWordsSql = "DELETE FROM conceptrecogn.goldstd_sentword";
    private static final String delAllWordsForPmidSql = "DELETE FROM conceptrecogn.goldstd_sentword WHERE pmid = ?";
    public static final String updateCogencySql = "update conceptrecogn.goldstd_sentword "
	    + "set cumLnAbstrCogency=?, cumLnSentCogency=? where pmid=? and sentNbr=? and sentWordNbr=?";

    /**
     * Persists word
     * 
     * @param mincoWord
     * @param connPool
     * @throws Exception
     */
    public void persist(MincoManWord mincoWord, JdbcConnectionPool connPool)
	    throws Exception {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement persistStmt = conn
		    .prepareStatement(persistWordSql)) {
		setPersistParams(mincoWord, persistStmt);
		try {
		    persistStmt.executeUpdate();
		} catch (Exception ex) {
		    String isAcrAbbrevStr = Boolean
			    .toString(mincoWord.isAcrAbbrev);
		    String errMsg = String.format(
			    "Error persisting sentence word. | PMID = %1$d | sent nbr = %2$d | "
				    + "sent word nbr = %3$d | begin doc pos = %4$,d | end doc pos = %5$,d | "
				    + "text = '%6$s' | lex base word ID = %7$d | "
				    + "is acr/abbrev = %8$s | acr/abbrev ID = %9$d ",
			    mincoWord.mincoPhrase.mincoSent.mincoDoc.pmid,
			    mincoWord.mincoPhrase.mincoSent.sentNbr,
			    mincoWord.sentWordNbr, mincoWord.beginDocPos,
			    mincoWord.endDocPos, mincoWord.text,
			    mincoWord.lexWordId, isAcrAbbrevStr,
			    mincoWord.acrAbbrevId);
		    throw new Exception(errMsg, ex);
		}
	    }
	}
    }

    public GoldStdSentWord marshal(int pmid, int sentNbr, int wordNbr,
	    JdbcConnectionPool connPool) throws SQLException {

	GoldStdSentWord word = null;

	/*
	 * begin_doc_pos, end_doc_pos, text, lexWordId, isAcrAbbrev,
	 * acrAbbrevId, isPunct
	 */
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement marshalStmt = conn
		    .prepareStatement(marshalWordSql)) {
		marshalStmt.setInt(1, pmid);
		marshalStmt.setInt(2, sentNbr);
		marshalStmt.setInt(3, wordNbr);
		try (ResultSet rs = marshalStmt.executeQuery()) {
		    if (rs.next()) {
			word = instantiateFromResultSet(rs);
		    }
		}
	    }
	}
	return word;
    }

    public List<GoldStdSentWord> marshalWordsForSent(int pmid, int sentNbr,
	    JdbcConnectionPool connPool) throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    return marshalWordsForSent(pmid, sentNbr, conn);
	}
    }

    public List<GoldStdSentWord> marshalWordsForSent(int pmid, int sentNbr,
	    Connection conn) throws SQLException {
	List<GoldStdSentWord> words = new ArrayList<GoldStdSentWord>();
	try (PreparedStatement marshalWordsStmt = conn
		.prepareStatement(marshalWordsForSentSql)) {
	    marshalWordsStmt.setInt(1, pmid);
	    marshalWordsStmt.setInt(2, sentNbr);
	    try (ResultSet rs = marshalWordsStmt.executeQuery()) {
		while (rs.next()) {
		    GoldStdSentWord word = instantiateFromResultSet(rs);
		    words.add(word);
		}
	    }
	}
	words.sort(GoldStdSentWord.Comparators.ByPmidSentNbrWordNbr);
	return words;
    }

    private GoldStdSentWord instantiateFromResultSet(ResultSet rs)
	    throws SQLException {
	GoldStdSentWord word = new GoldStdSentWord();
	/*
	 * 1-pmid, 2-sentNbr, 3-sentWordNbr, 4-beginDocPos, 5-endDocPos, 6-text,
	 * 7-lexWordId, 8-isAcrAbbrev, 9-acrAbbrevId, 10-isPunct,
	 * 11-cumLnAbstrCogency, 12-cumLnSentCogency
	 */
	word.setPmid(rs.getInt(1));
	word.setSentNbr(rs.getInt(2));
	word.setSentWordNbr(rs.getInt(3));
	word.setBeginDocPos(rs.getInt(4));
	word.setEndDocPos(rs.getInt(5));
	word.setText(rs.getString(6));
	word.setLexWordId(rs.getInt(7));
	word.setIsAcrAbbrev(rs.getBoolean(8));
	word.setAcrAbbrevId(rs.getInt(9));
	word.setIsPunct(rs.getBoolean(10));
	word.setCumLnAbstrCogency(rs.getDouble(11));
	word.setCumLnSentCogency(rs.getDouble(12));
	return word;
    }

    /**
     * Persist word object.
     * 
     * @param word
     * @param connPool
     * @throws Exception
     */
    public void persist(GoldStdSentWord word, JdbcConnectionPool connPool)
	    throws Exception {
	try (Connection conn = connPool.getConnection()) {
	    persist(word, conn);
	}
    }

    /**
     * Persist word object.
     * 
     * @param word
     * @param conn
     * @throws Exception
     */
    public void persist(GoldStdSentWord word, Connection conn)
	    throws Exception {

	try (PreparedStatement persistWordStmt = conn
		.prepareStatement(persistWordSql)) {
	    setPersistParams(word, persistWordStmt);
	    if (persistWordStmt.executeUpdate() != 1) {
		throw new Exception("Error persisting object to database.");
	    }
	} catch (Exception ex) {
	    String errMsg = String.format("Error persisting word for "
		    + "PMID = %1$d, sent nbr = %2$d, sent word nbr = %3$d, "
		    + "acronym/abbreviation ID = %4$d, text = '%5$s'",
		    word.getPmid(), word.getSentNbr(), word.getSentWordNbr(),
		    word.getAcrAbbrevId(), word.getText());
	    throw new Exception(errMsg, ex);
	}
    }

    private void setPersistParams(Object obj, PreparedStatement persistStmt)
	    throws SQLException {
	/*
	 * 1-pmid, 2-sentNbr, 3-sentWordNbr, 4-beginDocPos, 5-endDocPos, 6-text,
	 * 7-lexWordId, 8-isAcrAbbrev, 9-acrAbbrevId, 10-isPunct,
	 * 11-cumLnAbstrCogency, 12-cumLnSentCogency
	 */
	if (obj instanceof GoldStdSentWord) {
	    GoldStdSentWord word = (GoldStdSentWord) obj;
	    persistStmt.setInt(1, word.getPmid());
	    persistStmt.setInt(2, word.getSentNbr());
	    persistStmt.setInt(3, word.getSentWordNbr());
	    persistStmt.setInt(4, word.getBeginDocPos());
	    persistStmt.setInt(5, word.getEndDocPos());
	    persistStmt.setString(6, word.getText());
	    persistStmt.setInt(7, word.getLexWordId());
	    persistStmt.setBoolean(8, word.isAcrAbbrev());
	    persistStmt.setInt(9, word.getAcrAbbrevId());
	    persistStmt.setBoolean(10, word.isPunct());
	    persistStmt.setDouble(11, word.getCumLnAbstrCogency());
	    persistStmt.setDouble(12, word.getCumLnSentCogency());
	} else if (obj instanceof MincoManWord) {
	    MincoManWord mincoWord = (MincoManWord) obj;
	    persistStmt.setInt(1,
		    mincoWord.mincoPhrase.mincoSent.mincoDoc.pmid);
	    persistStmt.setInt(2, mincoWord.mincoPhrase.mincoSent.sentNbr);
	    persistStmt.setInt(3, mincoWord.sentWordNbr);
	    persistStmt.setInt(4, mincoWord.beginDocPos);
	    persistStmt.setInt(5, mincoWord.endDocPos);
	    persistStmt.setString(6, mincoWord.text);
	    persistStmt.setInt(7, mincoWord.lexWordId);
	    persistStmt.setBoolean(8, mincoWord.isAcrAbbrev);
	    persistStmt.setInt(9, mincoWord.acrAbbrevId);
	    persistStmt.setBoolean(10, mincoWord.isPunct);
	    persistStmt.setDouble(11, 0.0d);
	    persistStmt.setDouble(12, 0.0d);

	} else {
	    throw new SQLException("Incorrect object type");
	}
    }

    public void updateCogencies(Collection<GoldStdSentence> sentences,
	    JdbcConnectionPool connPool) throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement updateStmt = conn
		    .prepareStatement(updateCogencySql)) {
		for (GoldStdSentence sent : sentences) {
		    for (GoldStdSentWord word : sent.getWords()) {
			updateStmt.setDouble(1, word.getCumLnAbstrCogency());
			updateStmt.setDouble(2, word.getCumLnSentCogency());
			updateStmt.setInt(3, word.getPmid());
			updateStmt.setInt(4, word.getSentNbr());
			updateStmt.setInt(5, word.getSentWordNbr());
			updateStmt.addBatch();
		    }
		}
		updateStmt.executeBatch();
	    }
	}
    }

    /**
     * Delete all maps between sentence and word objects in database.
     * 
     * @param connPool
     * @return
     * @throws Exception
     */
    public static int delAllWords(JdbcConnectionPool connPool)
	    throws SQLException, Exception {
	int nbrDel = 0;
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement delAllWordsStmt = conn
		    .prepareStatement(delAllWordsSql)) {
		nbrDel = delAllWordsStmt.executeUpdate();
	    }
	}
	return nbrDel;
    }

    /**
     * 
     * @param pmidsToDelete
     *                          Set collection of PMIDs for sentences to delete
     * @param connPool
     * @return
     * @throws SQLException
     * @throws Exception
     */
    public static int delAllWords(Set<Integer> pmidsToDelete,
	    JdbcConnectionPool connPool) throws SQLException, Exception {
	int nbrDel = 0;
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement delAllWordsForCorpusStmt = conn
		    .prepareStatement(delAllWordsForPmidSql)) {
		Iterator<Integer> pmidsIter = pmidsToDelete.iterator();
		while (pmidsIter.hasNext()) {
		    int pmid = pmidsIter.next();
		    delAllWordsForCorpusStmt.setInt(1, pmid);
		    nbrDel += delAllWordsForCorpusStmt.executeUpdate();
		}
	    }
	}
	return nbrDel;
    }

}

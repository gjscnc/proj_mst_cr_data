package edu.mst.db.nlm.goldstd.corpora.parse;

import java.nio.charset.Charset;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import edu.mst.db.nlm.corpora.Corpus;
import edu.mst.db.nlm.goldstd.corpora.annot.NLMAnnotatedFileType;

/**
 * File names of all NLM abstracts and abstract annotations for each abstract
 * that is stored in the "gold standard" annotations directory.
 * 
 * @author gjscnc
 *
 */
public class NlmTestFileNames {

    public static final Charset CHARACTER_SET = Charset.forName("UTF-8");

    public static final char DIR_DELIM_WINDOWS = '\\';
    public static final char DIR_DELIM_LINUX = '/';
    public static final String BASE_FILES_DIR_WINDOWS = "\\UMLS\\";
    public static final String BASE_FILES_DIR_LINUX = "/home/gjs/umls/TestCollections";
    public static final String DRIVE_WINDOWS_DESKTOP = "G:";
    public static final String DRIVE_WINDOWS_LAPTOP = "C:";
    public static final String ABSTRACT_EXT = "txt";
    public static final String ANNOTATION_EXT = "ann";

    public static final String DEBUG_FILES_DIRECTORY = "G:\\DebugFiles";
    public static final String DEBUG_FILE_NAME = "DebugResults.txt";

    public static final int maxUnitTestPmid = 100;

    private Map<String, String> abstractFullNamesByShortName = new HashMap<String, String>();
    private Map<Integer, String> abstractFullNamesByPmid = new HashMap<Integer, String>();
    private Map<String, String> annotFullNamesByShortName = new HashMap<String, String>();
    private Map<Integer, String> annotFullNamesByPmid = new HashMap<Integer, String>();

    private char dirDelim = DIR_DELIM_WINDOWS; // default
    private String filesDir = null;

    private Platform platform = null;
    private Corpus corpus = null;

    public NlmTestFileNames(Platform platform, Corpus corpus) throws Exception {
	this.platform = platform;
	this.corpus = corpus;
	setDir();
    }

    private void setDir() throws Exception {
	switch (platform) {
	case WINDOWS_DESKTOP:
	    dirDelim = DIR_DELIM_WINDOWS;
	    filesDir = DRIVE_WINDOWS_DESKTOP.concat(BASE_FILES_DIR_WINDOWS);
	    break;
	case WINDOWS_LAPTOP:
	    dirDelim = DIR_DELIM_WINDOWS;
	    filesDir = DRIVE_WINDOWS_LAPTOP.concat(BASE_FILES_DIR_WINDOWS);
	    break;
	case LINUX:
	    dirDelim = DIR_DELIM_LINUX;
	    filesDir = BASE_FILES_DIR_LINUX;
	    break;
	default:
	    throw new Exception(
		    String.format("Invalid platform type %1$s provided",
			    platform.toString()));
	}
	filesDir = filesDir.concat(String.valueOf(dirDelim))
		.concat(corpus.getCorpusDirName());
    }

    /**
     * This method retrieves file names of all NLM files, both abstracts and
     * annotation files, stored in the directory containing NLM test data. It
     * does NOT retrieve unit test files. File names for abstracts and
     * annotations are stored in separate containers.
     * 
     * @throws Exception
     */
    public void compileNlmFileNames() throws Exception {

	if (filesDir == null || filesDir.isEmpty()) {
	    setDir();
	}
	Path dir = Paths.get(filesDir);

	// get list of files in the directory
	try (DirectoryStream<Path> stream = Files.newDirectoryStream(dir)) {

	    for (Path file : stream) {

		String fullFileName = file.toString();
		int fileNameLen = fullFileName.length();
		String suffix = fullFileName.substring(fileNameLen - 3);

		NLMAnnotatedFileType fileType;
		if (suffix.equals(NlmTestFileNames.ABSTRACT_EXT)) {
		    fileType = NLMAnnotatedFileType.TEXT;
		} else if (suffix.equals(NlmTestFileNames.ANNOTATION_EXT)) {
		    fileType = NLMAnnotatedFileType.ANNOTATION;
		} else {
		    continue;
		}

		int lastDirDelimPos = fullFileName.lastIndexOf(dirDelim);
		String fileName = fullFileName.substring(lastDirDelimPos + 1,
			fileNameLen - 4);

		Integer pmid = parsePmid(fullFileName, true, dirDelim);

		// ignore unit test files
		if (pmid <= maxUnitTestPmid) {
		    continue;
		}

		switch (fileType) {
		case TEXT:
		    abstractFullNamesByShortName.put(fileName, fullFileName);
		    abstractFullNamesByPmid.put(pmid, fullFileName);
		    break;
		case ANNOTATION:
		    annotFullNamesByShortName.put(fileName, fullFileName);
		    annotFullNamesByPmid.put(pmid, fullFileName);
		    break;
		}
	    }

	}

	// make sure equal number of annotations and pub abstract files
	// if (abstractFilesByName.size() != annFilesByName.size() + 2)
	// throw new Exception(
	// "Number of abstracts is not equal to number of annotations");

	System.out.printf("Extracted %1$,d file names %n",
		abstractFullNamesByShortName.size());

    }

    /**
     * Extracts the integer portion from the file name for NLM abstract or
     * annotation files.
     * 
     * @param fileName
     * @param isFullName
     * @return
     */
    public static int parsePmid(String fileName, boolean isFullName,
	    char dirDelim) {

	int pmid = 0;

	String shortName = null;
	if (isFullName) {
	    int dirDelimPos = fileName.lastIndexOf(dirDelim);
	    int nameDelimPos = fileName.lastIndexOf('.');
	    shortName = fileName.substring(dirDelimPos + 1, nameDelimPos);
	} else {
	    shortName = fileName;
	}
	String pmidStr = shortName.substring(5);
	pmid = Integer.valueOf(pmidStr);

	return pmid;
    }

    public String getFilesDir() {
	return filesDir;
    }

    public void setFilesDir(String filesDir) {
	this.filesDir = filesDir;
    }

    /**
     * Collection of full file name for NLM abstracts suitable for use in a Java
     * Path object, indexed by the short name of the file (i.e., without
     * directory path).
     * 
     * @return
     */
    public Map<String, String> getAbstractFullNamesByShortName() {
	return abstractFullNamesByShortName;
    }

    /**
     * Collection of full file names for NLM annotations of an abstract,
     * suitable for use in a Java Path object, indexed by the short name of the
     * annotation file.
     * 
     * @return
     */
    public Map<String, String> getAnnotFullNamesByShortName() {
	return annotFullNamesByShortName;
    }

    /**
     * Collection of full file name for NLM abstracts suitable for use in a Java
     * Path object, indexed by the PMID of the abstract.
     * 
     * @return
     */
    public Map<Integer, String> getAbstractFullNamesByPmid() {
	return abstractFullNamesByPmid;
    }

    /**
     * Collection of full file name for annotations of NLM abstract, indexed by
     * the PMID of the abstract, where annotation full file name is suitable for
     * use in a Java Path object.
     * 
     * @return
     */
    public Map<Integer, String> getAnnotFullNamesByPmid() {
	return annotFullNamesByPmid;
    }

    public char getDirDelim() {
	return dirDelim;
    }

    public void setDirDelim(char dirDelim) {
	this.dirDelim = dirDelim;
    }

    public Platform getPlatform() {
	return platform;
    }

    public void setPlatform(Platform platform) {
	this.platform = platform;
    }

    public Corpus getCorpus() {
	return corpus;
    }

    public void setCorpus(Corpus corpus) {
	this.corpus = corpus;
    }

}

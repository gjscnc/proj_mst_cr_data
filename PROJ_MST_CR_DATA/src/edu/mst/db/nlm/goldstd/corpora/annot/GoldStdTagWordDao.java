/**
 * 
 */
package edu.mst.db.nlm.goldstd.corpora.annot;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.SortedSet;
import java.util.TreeSet;

import edu.mst.db.util.JdbcConnectionPool;

/**
 * @author gjs
 *
 */
public class GoldStdTagWordDao {

    public static final String allFields = "pmid, tagId, sentNbr, sentWordNbr, lexWordId, token, beginPos, endPos, cui, isStopWord, isPunctOrDelimChar";

    public static final String deleteAllSql = "truncate table conceptrecogn.goldstd_tagwords";

    public static final String persistSql = "insert into conceptrecogn.goldstd_tagwords ("
	    + allFields + ") values(?,?,?,?,?,?,?,?,?,?,?)";

    public static final String marshalForPmidSql = "select " + allFields
	    + " from conceptrecogn.goldstd_tagwords where pmid=?";

    public static final String marshalForPmidAndTagSql = "select " + allFields
	    + " from conceptrecogn.goldstd_tagwords where pmid=? and tagId=?";

    public static final String marshalForPmidAndSentNbrSql = "select " + allFields
	    + " from conceptrecogn.goldstd_tagwords where pmid=? and sentNbr=?";

    public static void deleteAll(JdbcConnectionPool connPool)
	    throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement deleteAllStmt = conn
		    .prepareStatement(deleteAllSql)) {
		deleteAllStmt.executeUpdate();
	    }
	}
    }

    public void persist(GoldStdTagWord word, JdbcConnectionPool connPool)
	    throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement persistStmt = conn
		    .prepareStatement(persistSql)) {
		setPersistValues(word, persistStmt);
		persistStmt.executeUpdate();
	    }
	}
    }

    public void persist(Collection<GoldStdTagWord> words,
	    JdbcConnectionPool connPool) throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement persistStmt = conn
		    .prepareStatement(persistSql)) {
		for (GoldStdTagWord word : words) {
		    setPersistValues(word, persistStmt);
		    persistStmt.addBatch();
		}
		persistStmt.executeBatch();
	    }
	}
    }

    private void setPersistValues(GoldStdTagWord word,
	    PreparedStatement persistStmt) throws SQLException {
	/*
	 * 1-pmid, 2-tagId, 3-sentNbr, 4-sentWordNbr, 5-lexWordId, 6-token,
	 * 7-beginPos, 8-endPos, 9-cui, 10-isStopWord, 11, isPunct
	 */
	persistStmt.setInt(1, word.getPmid());
	persistStmt.setString(2, word.getTagId());
	persistStmt.setInt(3, word.getSentNbr());
	persistStmt.setInt(4, word.getSentWordNbr());
	persistStmt.setInt(5, word.getLexWordId());
	persistStmt.setString(6, word.getToken());
	persistStmt.setInt(7, word.getBeginPos());
	persistStmt.setInt(8, word.getEndPos());
	persistStmt.setString(9, word.getCui());
	persistStmt.setBoolean(10, word.isStopWord());
	persistStmt.setBoolean(11, word.isPunctOrDelimChar());
    }

    public SortedSet<GoldStdTagWord> marshal(int pmid,
	    JdbcConnectionPool connPool) throws SQLException {
	SortedSet<GoldStdTagWord> words = new TreeSet<GoldStdTagWord>();
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement marshalStmt = conn
		    .prepareStatement(marshalForPmidSql)) {
		marshalStmt.setInt(1, pmid);
		try (ResultSet rs = marshalStmt.executeQuery()) {
		    while (rs.next()) {
			GoldStdTagWord word = instantiateFromResultSet(rs);
			words.add(word);
		    }
		}
	    }
	}
	return words;
    }

    public SortedSet<GoldStdTagWord> marshal(int pmid, String tagId,
	    JdbcConnectionPool connPool) throws SQLException {
	SortedSet<GoldStdTagWord> words = new TreeSet<GoldStdTagWord>();
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement marshalStmt = conn
		    .prepareStatement(marshalForPmidAndTagSql)) {
		marshalStmt.setInt(1, pmid);
		marshalStmt.setString(2, tagId);
		try (ResultSet rs = marshalStmt.executeQuery()) {
		    while (rs.next()) {
			GoldStdTagWord word = instantiateFromResultSet(rs);
			words.add(word);
		    }
		}
	    }
	}
	return words;
    }

    public SortedSet<GoldStdTagWord> marshal(int pmid, int sentNbr,
	    JdbcConnectionPool connPool) throws SQLException {
	SortedSet<GoldStdTagWord> words = new TreeSet<GoldStdTagWord>();
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement marshalStmt = conn
		    .prepareStatement(marshalForPmidAndSentNbrSql)) {
		marshalStmt.setInt(1, pmid);
		marshalStmt.setInt(2, sentNbr);
		try (ResultSet rs = marshalStmt.executeQuery()) {
		    while (rs.next()) {
			GoldStdTagWord word = instantiateFromResultSet(rs);
			words.add(word);
		    }
		}
	    }
	}
	return words;
    }

    private GoldStdTagWord instantiateFromResultSet(ResultSet rs)
	    throws SQLException {
	/*
	 * 1-pmid, 2-tagId, 3-sentNbr, 4-sentWordNbr, 5-lexWordId, 6-token,
	 * 7-beginPos, 8-endPos, 9-cui, 10-isStopWord, 11, isPunct
	 */
	GoldStdTagWord word = new GoldStdTagWord();
	word.setPmid(rs.getInt(1));
	word.setTagId(rs.getString(2));
	word.setSentNbr(rs.getInt(3));
	word.setSentWordNbr(rs.getInt(4));
	word.setLexWordId(rs.getInt(5));
	word.setToken(rs.getString(6));
	word.setBeginPos(rs.getInt(7));
	word.setEndPos(rs.getInt(8));
	word.setCui(rs.getString(9));
	word.setStopWord(rs.getBoolean(10));
	word.setPunctOrDelimChar(rs.getBoolean(11));
	return word;
    }

}

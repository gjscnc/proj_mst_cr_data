package edu.mst.db.nlm.goldstd.corpora.annot;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.mst.db.nlm.goldstd.corpora.GoldStdAnnot;
import edu.mst.db.nlm.goldstd.corpora.GoldStdAnnotParseSpan;
import edu.mst.db.nlm.goldstd.corpora.GoldStdAnnotWord;
import edu.mst.db.nlm.goldstd.corpora.GoldStdSentWord;
import edu.mst.db.nlm.goldstd.corpora.GoldStdSentence;
import edu.mst.db.nlm.goldstd.corpora.GoldStdSentenceDao;
import edu.mst.db.util.Constants;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * Utility class providing methods to parse and persist NLM gold standard
 * annotation data. To be used for extending callable class that is parsing and
 * persisting gold standard annotations in a parallel process.
 * <p>
 * Methods are not static as a strategy to force either instantiating or
 * extending another class that is instantiated in each thread for concurrent
 * processing. This is done to simplify the coding; concurrency issues do not
 * have to be addressed, and throughput choke points are avoided.
 * <p>
 * This class includes methods and data associated with ONE PMID only since it
 * is designed for use in a parallel process where each thread parses the
 * annotation file for one abstract (i.e., one PMID).
 * 
 * @author George
 *
 */
public class GoldStdAnnotUtil {

    public static final String[] goldStdAutoIncrPkName = new String[] { "id" };

    private static final String deleteAllGoldStdAnnotSql = "DELETE FROM conceptrecogn.goldstd_annot";
    private static final String resetAutoIdAnnotSql = "ALTER TABLE conceptrecogn.goldstd_annot AUTO_INCREMENT = 1";
    private static final String deleteAllGoldStdAnnotWordSql = "DELETE FROM conceptrecogn.goldstd_annotwords";
    private static final String resetAutoIdAnnotWordSql = "ALTER TABLE conceptrecogn.goldstd_annotwords AUTO_INCREMENT = 1";
    private static final String cuiExistsInOntolSql = "SELECT count(*) FROM conceptrecogn.concepts WHERE cui=?";
    private static final String countAnnotWordsSql = "SELECT count(*) FROM conceptrecogn.goldstd_annotwords "
	    + "WHERE pmid=? AND sentenceNbr=? and wordNbr>=? AND wordNbr<=?";
    private static final String getSnomedUidsSql = "SELECT uid FROM conceptrecogn.concepts  "
	    + "WHERE cui = ? ORDER BY uid";

    public static final char discontinuousTagChar = ';';
    public static final char spanDelimChar = ';';
    public static final String cuiIdentifier = "Concept";
    public static final char cuiDelimiter = ':';
    public static final char cuiBeginDelimAnnotNote = '[';
    public static final char cuiEndDelimAnnotNote = ']';

    public static int maxNbrSpanIter = 20;

    private int pmid;
    private boolean isDebug;
    private String debugAnnotId;
    private boolean debugMsg;
    private JdbcConnectionPool connPool;

    private Map<String, GoldStdAnnot> annotations = new HashMap<String, GoldStdAnnot>();

    /**
     * Constructor for production use, i.e., no debugging.
     * 
     * @param connPool
     */
    public GoldStdAnnotUtil(int pmid, JdbcConnectionPool connPool) {
	this.pmid = pmid;
	this.connPool = connPool;
    }

    /**
     * Constructor when debugging.
     * 
     * @param pmid
     * @param isDebug
     * @param debugAnnotId
     * @param debugMsg
     * @param connPool
     */
    public GoldStdAnnotUtil(int pmid, boolean isDebug,
	    String debugAnnotId, boolean debugMsg,
	    JdbcConnectionPool connPool) {
	this.pmid = pmid;
	this.isDebug = isDebug;
	this.debugAnnotId = debugAnnotId;
	this.debugMsg = debugMsg;
	this.connPool = connPool;
    }

    /**
     * Parses annotation ID which is the value of the first field in each line
     * of the annotation file. The delimiter between this first field and the
     * remaining values is a tab.
     * 
     * @param text
     * @return
     */
    public String parseAnnotId(String text) throws Exception {
	String annotId = null;
	int endOfAnnotIdPos = text.indexOf(Constants.tabChar);
	annotId = text.substring(0, endOfAnnotIdPos);
	return annotId;
    }

    /**
     * Parses the annotation row to obtain the first document position for this
     * annotation.
     * <p>
     * Only applies to 'T' type annotations.
     * </p>
     * 
     * @param text
     * @return
     * @throws Exception
     */
    public int parseFirstDocPos(String text) throws Exception {

	int endOfAnnotIdPos = text.indexOf(Constants.tabChar);
	int endOfAnnotTypePos = text.indexOf(Constants.spaceChar,
		endOfAnnotIdPos + 1);
	int endOfFirstDocPos = text.indexOf(Constants.spaceChar,
		endOfAnnotTypePos + 1);
	String firstDocPosStr = text.substring(endOfAnnotTypePos + 1,
		endOfFirstDocPos);

	return Integer.valueOf(firstDocPosStr);
    }

    /**
     * Parses the string in the annotation record that indicates the type of
     * annotation. It returns an enumeration constant matching the text. If the
     * text does not correspond to an enumeration constant with matching name
     * then it throws an Exception.
     * 
     * @param text
     * @return GoldStdTypeEnum annotation type constant
     * @throws Exception
     */
    public GoldStdType parseAnnotType(String text) throws Exception {
	int firstPos = text.indexOf(Constants.tabChar) + 1;
	int lastPos = text.indexOf(Constants.spaceChar, firstPos);
	String type = text.substring(firstPos, lastPos);
	return GoldStdType.valueOf(type);
    }

    /**
     * This method parses the begin and end positions from the annotation text
     * and then retrieves the sentence number in that range. The text provided
     * must be associated with an annotation type of "T" only.
     * 
     * @param text
     *                 Text must be from type 'T' annotation ID
     * @return Sentence number
     * @throws Exception
     */
    public int parseSentNbr(String text) throws Exception {

	int sentNbr = Constants.uninitializedIntVal;
	int beginParsePos = Constants.uninitializedIntVal;
	int endParsePos = Constants.uninitializedIntVal;
	String intText = null;
	int beginDocPos = Constants.uninitializedIntVal;
	int endDocPos = Constants.uninitializedIntVal;

	try {
	    // first space is one position before first doc pos
	    beginParsePos = text.indexOf(Constants.spaceChar) + 1;
	    // next space is between first and last doc positions
	    endParsePos = text.indexOf(Constants.spaceChar, beginParsePos + 1);
	    intText = text.substring(beginParsePos, endParsePos);
	    beginDocPos = Integer.valueOf(intText);

	    beginParsePos = endParsePos + 1;
	    endParsePos = text.indexOf(Constants.tabChar, beginParsePos);
	    intText = text.substring(beginParsePos, endParsePos);
	    if (intText.contains(Constants.semiColonStr)) {
		endParsePos = intText.indexOf(Constants.semiColonChar);
		intText = intText.substring(0, endParsePos);
	    }
	    endDocPos = Integer.valueOf(intText);
	    sentNbr = retrieveSentenceNbr(beginDocPos, endDocPos);
	} catch (Exception ex) {
	    String errMsg = String.format(
		    "Sentence number not found for "
			    + "PMID = %1$d and annotation text = %2$s, "
			    + "error msg = %3$s %n",
		    pmid, text, ex.getMessage());
	    System.out.println(errMsg);
	    throw new Exception(errMsg, ex);
	}

	return sentNbr;
    }

    /**
     * This method parses the gold standard CUI from the record. If it does not
     * contain a CUI it returns null.
     * 
     * @param text
     * @return
     */
    public boolean textContainsGoldStdCui(String text) throws Exception {
	return text.contains(cuiIdentifier);
    }

    /**
     * This method parses the text to retrieve the annotation ID identified by a
     * reference record in the annotation record. This applies to annotation
     * records whose ID begins with an 'N' and contains the text 'Reference'.
     * 
     * @param text
     * @param isAnnotatorNote
     * @return
     * @throws Exception
     */
    public String getRefAnnotId(String text, boolean isAnnotatorNote)
	    throws Exception {
	int beginPosRefAnnotId = text.indexOf(Constants.spaceChar) + 1;
	char endChar;
	if (isAnnotatorNote)
	    endChar = Constants.tabChar;
	else
	    endChar = Constants.spaceChar;
	int endPosRefAnnotId = text.indexOf(endChar, beginPosRefAnnotId);
	return text.substring(beginPosRefAnnotId, endPosRefAnnotId);
    }

    /**
     * This method parses the UMLS concept unique identifier, or CUI, from a
     * reference record in the annotation file.
     * 
     * @param text
     * @return
     */
    public String parseGoldStdCui(String text) throws Exception {
	int beginPosCui = text.indexOf(cuiDelimiter) + 1;
	int endPosCui = text.indexOf(Constants.tabChar, beginPosCui);
	return text.substring(beginPosCui, endPosCui);
    }

    /**
     * Essentially same method as parsing gold standard CUI, only it does this
     * for the annotator's notes records occurring at end of annotation file.
     * 
     * @param text
     * @return
     * @throws Exception
     */
    public String parseAnnotNotesCui(String text) throws Exception {
	try {
	    int beginPosCui = text.indexOf(cuiBeginDelimAnnotNote) + 1;
	    int endPosCui = text.indexOf(cuiEndDelimAnnotNote);
	    if (beginPosCui == -1 || endPosCui == -1) {
		if (isDebug) {
		    System.out.printf(
			    "Cannot parse CUI from annotator notes "
				    + "for pmid %1$d, text = %2$s %n",
			    pmid, text);
		}
		return null;
	    }
	    return text.substring(beginPosCui, endPosCui);
	} catch (Exception ex) {
	    String errMsg = String
		    .format("Error parsing CUI from annotator notes "
			    + "for pmid %1$d, text = %2$s", pmid, text);
	    throw new Exception(errMsg, ex);
	}
    }

    /**
     * Determine whether or not this NLM concept (with unique identifier CUI)
     * exists in the ontology used by the tagger.
     * <p>
     * The tagger currently uses the SNOMED ontology.
     * 
     * @param cui
     * @return
     * @throws Exception
     */
    public boolean existsInSnomed(String cui) throws Exception {
	boolean existsInSnomed = false;
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement cuiExistsInOntolStmt = conn
		    .prepareStatement(cuiExistsInOntolSql)) {
		cuiExistsInOntolStmt.setString(1, cui);
		try (ResultSet rs = cuiExistsInOntolStmt.executeQuery()) {
		    if (rs.next())
			existsInSnomed = rs.getInt(1) > 0;
		}
	    }
	}
	return existsInSnomed;
    }

    public long getBestMatchSnomedUid(String cui) throws Exception {

	long conceptUid = 0L;

	if (cui == null || cui.isEmpty())
	    return conceptUid;

	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement getSnomedUidsStmt = conn
		    .prepareStatement(getSnomedUidsSql)) {
		getSnomedUidsStmt.setString(1, cui);
		try (ResultSet rs = getSnomedUidsStmt.executeQuery()) {
		    if (rs.next()) {
			conceptUid = rs.getLong(1);
		    }
		}
	    }
	}

	return conceptUid;
    }

    /**
     * This method extracts the text associated with an annotation, i.e., whose
     * NLM annotation ID begins with a 'T'.
     * 
     * @param text
     * @return
     * @throws Exception
     */
    public String parseAnnotText(String text) throws Exception {
	int beginPosCuiName = text.lastIndexOf(Constants.tabChar) + 1;
	return text.substring(beginPosCuiName);
    }

    /**
     * This method extracts the CUI name associated with an annotation
     * reference.
     * <p>
     * This method only applies to references (those whose NLM annotation ID
     * begins with a 'N', i.e., a reference annotation).
     * 
     * @param text
     * @return
     */
    public String parseRefCuiName(String text) throws Exception {
	int beginPosCuiName = text.lastIndexOf(Constants.tabChar) + 1;
	return text.substring(beginPosCuiName);
    }

    /**
     * This method extracts the CUI name associated with a note made by the
     * annotator for this annotation.
     * <p>
     * This method only applies to annotation notes (those whose NLM annotation
     * ID begins with a '#', i.e., a reference annotation).
     * 
     * @param text
     * @return
     * @throws Exception
     */
    public String parseAnnotNoteCuiName(String text) throws Exception {
	try {
	    int beginPosCuiName = text.indexOf(cuiEndDelimAnnotNote);
	    if (beginPosCuiName == -1 || beginPosCuiName == text.length() - 1) {
		if (isDebug) {
		    System.out.printf(
			    "Cannot parse CUI name from annotator notes "
				    + "for pmid %1$d, text = %2$s %n",
			    pmid, text);
		}
		return null;
	    }
	    return text.substring(beginPosCuiName + 2);
	} catch (Exception ex) {
	    String errMsg = String
		    .format("Error parsing CUI name from annotator notes "
			    + "for pmid %1$d, text = %2$s", pmid, text);
	    throw new Exception(errMsg, ex);
	}
    }

    /**
     * This method uses one of the spans for an annotation to determine which
     * sentence it pertains to in the abstract document.
     * <p>
     * If multiple spans exist for an annotation it should only be required that
     * one span segment be used since each NLM annotation only pertains to one
     * particular sentence.
     * 
     * @param pmid
     * @param beginDocPos
     * @param endDocPos
     * @return
     * @throws Exception
     */
    public int retrieveSentenceNbr(int beginDocPos, int endDocPos)
	    throws Exception {
	int sentenceNbr = Constants.uninitializedIntVal;
	String getSentNbrInSpanSql = "SELECT sentence_nbr FROM conceptrecogn.goldstd_sentence "
		+ "WHERE pmid=? AND begin_doc_pos <= ? AND "
		+ "(end_doc_pos >= ? OR end_doc_pos = -1) "
		+ "ORDER BY sentence_nbr";
	try (Connection conn = connPool.getConnection()) {

	    try (PreparedStatement getSentNbrInSpanStmt = conn
		    .prepareStatement(getSentNbrInSpanSql)) {
		getSentNbrInSpanStmt.setInt(1, pmid);
		getSentNbrInSpanStmt.setInt(2, beginDocPos);
		getSentNbrInSpanStmt.setInt(3, beginDocPos);
		try (ResultSet rs = getSentNbrInSpanStmt.executeQuery()) {
		    if (rs.next())
			sentenceNbr = rs.getInt(1);
		}
	    }
	}
	if (sentenceNbr == Constants.uninitializedIntVal) {
	    String errMsg = String.format(
		    "%nUnable to retrieve sentence number for "
			    + "PMID = %1$d, begin doc position = %2$d, end doc position = %3$d %n",
		    pmid, beginDocPos, endDocPos);
	    throw new Exception(errMsg);
	}
	return sentenceNbr;
    }

    public List<GoldStdAnnotParseSpan> parseAnnotSpans(String text)
	    throws Exception {

	List<GoldStdAnnotParseSpan> spans = new ArrayList<GoldStdAnnotParseSpan>();

	// go to one character past end of annotation ID for this abstract
	int cursorPos = text.indexOf(Constants.tabChar) + 1;
	// go to one character past end of annotation type text
	cursorPos = text.indexOf(Constants.spaceChar, cursorPos) + 1;
	// get index of last char in text containing positions data
	int indexLastPositionsData = text.indexOf(Constants.tabChar, cursorPos);
	int firstMultiSpanPos = text.indexOf(Constants.semiColonChar);
	boolean isMultiSpan = firstMultiSpanPos == -1
		|| firstMultiSpanPos < indexLastPositionsData;

	boolean isLastSpan = false;
	do {
	    /*
	     * parse first document position
	     */
	    int beginCharPos = cursorPos;
	    int endCharPos = text.indexOf(Constants.spaceChar, beginCharPos);
	    String firstDocPosStr = text.substring(beginCharPos, endCharPos);
	    int beginDocPos = Integer.valueOf(firstDocPosStr);

	    /*
	     * If text contains multiple spans then need to determine if the
	     * next delimiter character is ';' or space.
	     */
	    char delimChar = Constants.tabChar;
	    beginCharPos = endCharPos + 1; // start of next doc position
	    if (isMultiSpan) {
		int nextSemiCharPos = text.indexOf(Constants.semiColonChar,
			beginCharPos);
		if (nextSemiCharPos > 0
			&& nextSemiCharPos < indexLastPositionsData) {
		    delimChar = Constants.semiColonChar;
		} else {
		    isLastSpan = true;
		}
	    } else {
		isLastSpan = true;
	    }
	    endCharPos = text.indexOf(delimChar, beginCharPos);
	    String endDocPosStr = text.substring(beginCharPos, endCharPos);
	    int endDocPos = Integer.valueOf(endDocPosStr);

	    GoldStdAnnotParseSpan span = new GoldStdAnnotParseSpan();
	    span.setBeginDocPos(beginDocPos);
	    span.setEndDocPos(endDocPos);
	    spans.add(span);

	    cursorPos = endCharPos + 1;

	} while (!isLastSpan);

	Collections.sort(spans);
	return spans;
    }

    /**
     * This method sets the begin document position for each annotation. The
     * begin document position is set to the begin document position for the
     * first word in the list of annotation words previously retrieved.
     * 
     * @Api Must execute method parseAnnotWords prior to executing this method.
     */
    public void setBeginDocPos() {
	for (String annotId : annotations.keySet()) {
	    GoldStdAnnot annot = annotations.get(annotId);
	    GoldStdAnnotWord firstWord = annot.getWords().first();
	    int beginDocPos = firstWord.getBeginDocPos();
	    annot.setBeginDocPos(beginDocPos);
	}
    }

    /**
     * This method deletes all NLM Gold Standard annotations from the database,
     * including associated word positions.
     * 
     * @param connPool
     * @throws Exception
     */
    public static void deleteAllAnnot(JdbcConnectionPool connPool)
	    throws Exception {

	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement deleteAllGoldStdAnnotStmt = conn
		    .prepareStatement(deleteAllGoldStdAnnotSql)) {
		int nbrDeleted = deleteAllGoldStdAnnotStmt.executeUpdate();
		System.out.printf(
			"Deleted total of %1$,d NLM Gold Standard annotations. %n",
			nbrDeleted);
	    }
	    try (PreparedStatement resetAutoIdAnnotStmt = conn
		    .prepareStatement(resetAutoIdAnnotSql)) {
		resetAutoIdAnnotStmt.executeUpdate();
	    }
	    try (PreparedStatement deleteAllGoldStdAnnotWordStmt = conn
		    .prepareStatement(deleteAllGoldStdAnnotWordSql)) {
		int nbrDeleted = deleteAllGoldStdAnnotWordStmt.executeUpdate();
		System.out.printf("Deleted total of %1$,d NLM Gold Standard "
			+ "annotation word positions. %n", nbrDeleted);
	    }
	    try (PreparedStatement resetAutoIdAnnotWordStmt = conn
		    .prepareStatement(resetAutoIdAnnotWordSql)) {
		resetAutoIdAnnotWordStmt.executeUpdate();
	    }
	}

    }

    /**
     * Method determines whether or not at least one of the words in the list
     * associated with an NLM Gold Std annotation already exist in the database.
     * 
     * @param words
     * @param conn
     * @return
     * @throws Exception
     */
    public boolean wordsExistsInDb(List<GoldStdAnnotWord> words,
	    Connection conn) throws Exception {

	int minWordNbr = Integer.MAX_VALUE;
	int maxWordNbr = -1;
	for (GoldStdAnnotWord word : words) {
	    if (word.getSentWordNbr() < minWordNbr)
		minWordNbr = word.getSentWordNbr();
	    if (word.getSentWordNbr() > maxWordNbr)
		maxWordNbr = word.getSentWordNbr();
	}
	boolean inDb = false;
	int sentNbr = words.get(0).getSentNbr();
	try (PreparedStatement countAnnotWordsStmt = conn
		.prepareStatement(countAnnotWordsSql)) {
	    countAnnotWordsStmt.setInt(1, pmid);
	    countAnnotWordsStmt.setInt(2, sentNbr);
	    countAnnotWordsStmt.setInt(3, minWordNbr);
	    countAnnotWordsStmt.setInt(4, maxWordNbr);
	    try (ResultSet rs = countAnnotWordsStmt.executeQuery()) {
		if (rs.next()) {
		    if (rs.getInt(1) > 0)
			inDb = true;
		}
	    }
	}
	return inDb;
    }

    public static void printResult(Map<String, GoldStdAnnot> annotations,
	    JdbcConnectionPool connPool) throws SQLException {

	GoldStdSentenceDao sentDao = new GoldStdSentenceDao();
	
	    List<GoldStdAnnot> sortedAnnot = new ArrayList<GoldStdAnnot>();
	    sortedAnnot.addAll(annotations.values());
	    sortedAnnot.sort(GoldStdAnnot.Comparators.byPosition);

	    int currentPmid = Constants.uninitializedIntVal;
	    int currentSentNbr = Constants.uninitializedIntVal;
	    GoldStdSentence currentSent = null;
	    for (GoldStdAnnot annot : sortedAnnot) {
		int annotPmid = annot.getPmid();
		if (annotPmid != currentPmid) {
		    currentPmid = annotPmid;
		    System.out.printf("%nAnnotations for PMID %1$d %n",
			    currentPmid);
		}
		int annotSentNbr = annot.getSentenceNbr();
		if (annotSentNbr != currentSentNbr) {
		    currentSentNbr = annotSentNbr;
		    currentSent = sentDao.marshal(currentPmid, currentSentNbr, connPool);
		    String sentText = currentSent.getSentText();
		    System.out.printf("%nSentence %1$d - %2$s %n", currentSentNbr,
			    sentText);
		}

		System.out.printf("Annotation ID %1$s | CUI %2$s %3$s %n",
			annot.getAnnotId(), annot.getCui(), annot.getCuiName());
		for (GoldStdAnnotWord word : annot.getWords()) {
		    int wordNbr = word.getSentWordNbr();
		    GoldStdSentWord sentWord = currentSent.getWordBySentPos().get(wordNbr);
		    System.out.printf("Word nbr %1$d, token = %2$s %n",
			    wordNbr, sentWord.getText());
		}
	    }


    }

    public static char getDiscontinuoustagchar() {
	return discontinuousTagChar;
    }

    public static char getSpandelimchar() {
	return spanDelimChar;
    }

    public static String getCuiidentifier() {
	return cuiIdentifier;
    }

    public static char getCuidelimiter() {
	return cuiDelimiter;
    }

    public static char getCuibegindelimannotnote() {
	return cuiBeginDelimAnnotNote;
    }

    public static char getCuienddelimannotnote() {
	return cuiEndDelimAnnotNote;
    }

    public static int getMaxNbrSpanIter() {
	return maxNbrSpanIter;
    }

    public int getPmid() {
	return pmid;
    }

    public boolean isDebug() {
	return isDebug;
    }

    public String getDebugAnnotId() {
	return debugAnnotId;
    }

    public boolean isDebugMsg() {
	return debugMsg;
    }

    public Map<String, GoldStdAnnot> getAnnotations() {
	return annotations;
    }

}

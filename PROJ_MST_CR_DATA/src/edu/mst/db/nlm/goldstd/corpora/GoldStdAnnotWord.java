package edu.mst.db.nlm.goldstd.corpora;

import java.util.Comparator;

public class GoldStdAnnotWord implements Comparable<GoldStdAnnotWord> {

    private int id;
    private int annotDbId;
    private int pmid;
    private int sentNbr;
    private int sentWordNbr;
    private int sentPhraseNbr;
    private int phraseWordNbr;
    private int beginDocPos;
    private int lexWordId;
    private String sourceCorpus;

    public static class Comparators {
	private static final Comparator<GoldStdAnnotWord> byPmid = (
		GoldStdAnnotWord word1,
		GoldStdAnnotWord word2) -> Long.compare(word1.pmid, word2.pmid);
	private static final Comparator<GoldStdAnnotWord> bySentNbr = (
		GoldStdAnnotWord word1, GoldStdAnnotWord word2) -> Long
			.compare(word1.sentNbr, word2.sentNbr);
	private static final Comparator<GoldStdAnnotWord> bySentWordNbr = (
		GoldStdAnnotWord word1, GoldStdAnnotWord word2) -> Long
			.compare(word1.sentWordNbr, word2.sentWordNbr);
	public static final Comparator<GoldStdAnnotWord> byPmid_SentNbr_WordNbr = (
		GoldStdAnnotWord word1, GoldStdAnnotWord word2) -> byPmid
			.thenComparing(bySentNbr).thenComparing(bySentWordNbr)
			.compare(word1, word2);
	public static final Comparator<GoldStdAnnotWord> byDbId = (
		GoldStdAnnotWord word1,
		GoldStdAnnotWord word2) -> Long.compare(word1.id, word2.id);
    }

    @Override
    public int compareTo(GoldStdAnnotWord word) {
	return Comparators.byDbId.compare(this, word);
    }

    @Override
    public boolean equals(Object obj) {
	if (obj instanceof GoldStdAnnotWord) {
	    GoldStdAnnotWord word = (GoldStdAnnotWord) obj;
	    return id == word.id;
	}
	return false;
    }

    @Override
    public int hashCode() {
	return Integer.hashCode(id);
    }

    public int getId() {
	return id;
    }

    public void setId(int id) {
	this.id = id;
    }

    public int getAnnotDbId() {
	return annotDbId;
    }

    public void setAnnotDbId(int annotId) {
	this.annotDbId = annotId;
    }

    public int getPmid() {
	return pmid;
    }

    public void setPmid(int pmid) {
	this.pmid = pmid;
    }

    public int getSentNbr() {
	return sentNbr;
    }

    public void setSentNbr(int sentNbr) {
	this.sentNbr = sentNbr;
    }

    public int getSentWordNbr() {
	return sentWordNbr;
    }

    public void setSentWordNbr(int sentWordNbr) {
	this.sentWordNbr = sentWordNbr;
    }

    public int getSentPhraseNbr() {
	return sentPhraseNbr;
    }

    public void setSentPhraseNbr(int sentPhraseNbr) {
	this.sentPhraseNbr = sentPhraseNbr;
    }

    public int getPhraseWordNbr() {
	return phraseWordNbr;
    }

    public void setPhraseWordNbr(int phraseWordNbr) {
	this.phraseWordNbr = phraseWordNbr;
    }

    public int getBeginDocPos() {
	return beginDocPos;
    }

    public void setBeginDocPos(int beginDocPos) {
	this.beginDocPos = beginDocPos;
    }

    public int getLexWordId() {
	return lexWordId;
    }

    public void setLexWordId(int lexWordId) {
	this.lexWordId = lexWordId;
    }

    public String getSourceCorpus() {
	return sourceCorpus;
    }

    public void setSourceCorpus(String sourceCorpus) {
	this.sourceCorpus = sourceCorpus;
    }

}

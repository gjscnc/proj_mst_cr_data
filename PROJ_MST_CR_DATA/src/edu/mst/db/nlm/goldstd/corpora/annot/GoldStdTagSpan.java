/**
 * 
 */
package edu.mst.db.nlm.goldstd.corpora.annot;

import java.util.Comparator;

import edu.mst.db.nlm.corpora.Corpus;

/**
 * This class is for each 'tag' found in the gold standard annotations.
 * <p>
 * That is, each 'Txx" tag entry found in the *.annot files
 * 
 * @author gjs
 *
 */
public class GoldStdTagSpan implements Comparable<GoldStdTagSpan> {

    private int pmid = 0;
    private String tagId = null;
    private int spanNbr = 0;
    private String category = null;
    private int beginPos = 0;
    private int endPos = 0;
    private String text = null;
    private Corpus corpus = null;

    public static class Comparators {
	public static Comparator<GoldStdTagSpan> byPmid = (GoldStdTagSpan tag1,
		GoldStdTagSpan tag2) -> Integer.compare(tag1.pmid, tag2.pmid);
	public static Comparator<GoldStdTagSpan> byTagId = (GoldStdTagSpan tag1,
		GoldStdTagSpan tag2) -> tag1.tagId.compareTo(tag2.tagId);
	public static Comparator<GoldStdTagSpan> bySpanNbr = (GoldStdTagSpan tag1,
		GoldStdTagSpan tag2) -> Integer.compare(tag1.spanNbr, tag2.spanNbr);
	public static Comparator<GoldStdTagSpan> byPmid_TagId_SpanNbr = (
		GoldStdTagSpan tag1, GoldStdTagSpan tag2) -> byPmid
			.thenComparing(byTagId).thenComparing(bySpanNbr)
			.compare(tag1, tag2);
	public static Comparator<GoldStdTagSpan> byBeginPos = (GoldStdTagSpan tag1,
		GoldStdTagSpan tag2) -> Integer.compare(tag1.beginPos,
			tag2.beginPos);
	public static Comparator<GoldStdTagSpan> byPmid_BeginPos = (GoldStdTagSpan tag1,
		GoldStdTagSpan tag2) -> byPmid.thenComparing(byBeginPos)
			.compare(tag1, tag2);
    }

    @Override
    public int compareTo(GoldStdTagSpan tag) {
	return Comparators.byPmid_TagId_SpanNbr.compare(this, tag);
    }

    @Override
    public boolean equals(Object obj) {
	if (obj instanceof GoldStdTagSpan) {
	    GoldStdTagSpan tag = (GoldStdTagSpan) obj;
	    return pmid == tag.pmid && tagId.equals(tag.tagId)
		    && spanNbr == tag.spanNbr;
	}
	return false;
    }

    @Override
    public int hashCode() {
	return Integer.hashCode(pmid) + 7 * tagId.hashCode()
		+ 31 * Integer.hashCode(spanNbr);
    }

    public int getPmid() {
	return pmid;
    }

    public void setPmid(int pmid) {
	this.pmid = pmid;
    }

    public String getTagId() {
	return tagId;
    }

    public void setTagId(String tagId) {
	this.tagId = tagId;
    }

    public int getSpanNbr() {
	return spanNbr;
    }

    public void setSpanNbr(int spanNbr) {
	this.spanNbr = spanNbr;
    }

    public String getCategory() {
	return category;
    }

    public void setCategory(String category) {
	this.category = category;
    }

    public int getBeginPos() {
	return beginPos;
    }

    public void setBeginPos(int beginPos) {
	this.beginPos = beginPos;
    }

    public int getEndPos() {
	return endPos;
    }

    public void setEndPos(int endPos) {
	this.endPos = endPos;
    }

    public String getText() {
	return text;
    }

    public void setText(String text) {
	this.text = text;
    }

    public Corpus getCorpus() {
        return corpus;
    }

    public void setCorpus(Corpus corpus) {
        this.corpus = corpus;
    }

}

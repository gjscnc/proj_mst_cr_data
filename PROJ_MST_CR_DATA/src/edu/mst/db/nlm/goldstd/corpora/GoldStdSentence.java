package edu.mst.db.nlm.goldstd.corpora;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Class to store results of parsing document into sentences. Includes begin and
 * end position of the sentence in the document. Begin and end document
 * positions, along with sentence text, are required for instantiation to ensure
 * proper adjustments are made for white spaces.
 * 
 * @author George
 *
 */
public class GoldStdSentence implements Comparable<GoldStdSentence> {

    private int pmid = 0;
    private int sentenceNbr = 0;
    private int beginDocPos = 0;
    private int endDocPos = 0;
    private String sentText;
    private String docText;
    /**
     * List of base words associated with this sentence. Position in this list
     * corresponds to the position in the sentence.
     */
    private List<GoldStdSentWord> words;
    private SortedMap<Integer, GoldStdSentWord> wordBySentPos;
    /**
     * This map is used to ensure that words can be retrieved by the sentence
     * position of the word's first char in the text. Map keys are automatically
     * sorted to enhance ease of insertion and retrieval.
     */
    private SortedMap<Integer, GoldStdSentWord> wordsByBeginCharPos;
    /**
     * Array contains base word ID for each word in sentence, where position in
     * the array corresponds to position in the sentence. Value is set to -1
     * initially, and any position in array whose value is -1 indicates that a
     * base word ID was not available for this word.
     */
    private int[] sentBaseWordIdByPos;

    private List<GoldStdPhrase> phrases;

    /**
     * Default constructor.
     * 
     * @param pmid
     * @param sentenceNbr
     * @param beginDocPos
     * @param endDocPos
     * @param sentenceText
     * @param docText
     */
    public GoldStdSentence(int pmid, int sentenceNbr, int beginDocPos,
	    int endDocPos, String sentenceText, String docText) {
	this.pmid = pmid;
	this.sentenceNbr = sentenceNbr;
	this.beginDocPos = beginDocPos;
	this.endDocPos = endDocPos;
	this.sentText = sentenceText;
	this.docText = docText;
	words = new ArrayList<GoldStdSentWord>();
	wordBySentPos = new TreeMap<Integer, GoldStdSentWord>();
	wordsByBeginCharPos = new TreeMap<Integer, GoldStdSentWord>();
	phrases = new ArrayList<GoldStdPhrase>();
    }

    public void addWord(GoldStdSentWord newWord) throws Exception {

	words.add(newWord);

	words.sort(GoldStdSentWord.Comparators.ByPmidSentNbrWordNbr);
	wordBySentPos.put(newWord.getSentWordNbr(), newWord);
	wordsByBeginCharPos.put(newWord.getBeginDocPos(), newWord);
	sentBaseWordIdByPos = new int[words.size()];
	for (int i = 0; i < words.size(); i++) {
	    GoldStdSentWord sentWord = words.get(i);
	    sentWord.setSentWordNbr(i);
	    sentBaseWordIdByPos[i] = sentWord.getLexWordId();
	}
    }

    /**
     * Sets list of words to equal the list provided, sorts, and extracts all
     * base word IDs
     * 
     * @param newWords
     */
    public void setWords(List<GoldStdSentWord> newWords) {
	words = newWords;
	wordsByBeginCharPos = new TreeMap<Integer, GoldStdSentWord>();
	sentBaseWordIdByPos = new int[words.size()];
	for (GoldStdSentWord word : words) {
	    wordBySentPos.put(word.getSentWordNbr(), word);
	    sentBaseWordIdByPos[word.getSentWordNbr()] = word.getLexWordId();
	    wordsByBeginCharPos.put(word.getBeginDocPos(), word);
	}
    }

    public List<GoldStdSentWord> getWords() {
	return words;
    }

    /**
     * Array contains base word ID for each word in sentence, where position in
     * the array corresponds to position in the sentence. Value is set to -1
     * initially, and any position in array whose value is -1 indicates that a
     * base word ID was not available for this word.
     */
    public int[] getBaseWordIdByPos() {
	return sentBaseWordIdByPos;
    }

    public SortedMap<Integer, GoldStdSentWord> getWordsByBeginCharPos() {
	return wordsByBeginCharPos;
    }

    public static class Comparators {
 	private static final Comparator<GoldStdSentence> ByPmid = (
 		GoldStdSentence s1,
 		GoldStdSentence s2) -> Integer.compare(s1.pmid, s2.pmid);
 	private static final Comparator<GoldStdSentence> BySentNbr = (
 		GoldStdSentence s1, GoldStdSentence s2) -> Integer
 			.compare(s1.sentenceNbr, s2.sentenceNbr);
 	public static final Comparator<GoldStdSentence> ByPmid_SentNbr = (
 		GoldStdSentence s1, GoldStdSentence s2) -> ByPmid
 			.thenComparing(BySentNbr).compare(s1, s2);
     }

   @Override
    public int compareTo(GoldStdSentence s2) {
	return Comparators.ByPmid_SentNbr.compare(this, s2);
    }

     @Override
    public boolean equals(Object obj) {
	if (obj instanceof GoldStdSentence) {
	    GoldStdSentence sentence = (GoldStdSentence) obj;
	    return this.pmid == sentence.pmid
		    && this.sentenceNbr == sentence.sentenceNbr;
	}
	return false;
    }

    @Override
    public int hashCode() {
	return Integer.hashCode(pmid) + 37 * Integer.hashCode(sentenceNbr);
    }

    @Override
    public String toString() {
	return String.format("PMID %1$d, sentence nbr %2$,d, text '%3$s'", pmid,
		sentenceNbr, sentText);
    }

    public String getSentText() {
	return sentText;
    }

    public void setSentText(String sentText) {
	this.sentText = sentText;
    }

    public void setPmid(int pmid) {
	this.pmid = pmid;
    }

    public void setSentenceNbr(int sentenceNbr) {
	this.sentenceNbr = sentenceNbr;
    }

    public void setBeginDocPos(int beginDocPos) {
	this.beginDocPos = beginDocPos;
    }

    public void setEndDocPos(int endDocPos) {
	this.endDocPos = endDocPos;
    }

    public int getPmid() {
	return pmid;
    }

    /**
     * 0-based index of position of sentence in document, e.g., 1st, 2nd, 3rd,
     * etc., which number sentence is this in the document.
     * 
     * @return
     */
    public int getSentenceNbr() {
	return sentenceNbr;
    }

    /**
     * 0-based position of first character of sentence in the document.
     * 
     * @return
     */
    public int getBeginDocPos() {
	return beginDocPos;
    }

    /**
     * 0-based, exclusive, position of last character of sentence in the
     * document. It is one position to the right of the last character since
     * Java String.substring(begin, end) function uses exclusive end.
     * 
     * @return
     */
    public int getEndDocPos() {
	return endDocPos;
    }

    public String getDocText() {
	return docText;
    }

    public void setDocText(String docText) {
	this.docText = docText;
    }

    public SortedMap<Integer, GoldStdSentWord> getWordBySentPos() {
        return wordBySentPos;
    }

    public int[] getSentBaseWordIdByPos() {
	return sentBaseWordIdByPos;
    }

    public List<GoldStdPhrase> getPhrases() {
	return phrases;
    }

    public void setPhrases(List<GoldStdPhrase> phrases) {
	this.phrases = phrases;
    }

}

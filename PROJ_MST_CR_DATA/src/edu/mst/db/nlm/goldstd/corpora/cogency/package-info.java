/**
 * 
 */
/**
 * Components to compute the Hecht-Nielsen cogency values for abstracts in the
 * gold standard corpora.
 * <p>
 * Includes computing persisting cogency values for sentence words for each
 * abstract, and persisting the matching cogency values in the gold standard
 * words.
 * 
 * @author gjs
 *
 */
package edu.mst.db.nlm.goldstd.corpora.cogency;
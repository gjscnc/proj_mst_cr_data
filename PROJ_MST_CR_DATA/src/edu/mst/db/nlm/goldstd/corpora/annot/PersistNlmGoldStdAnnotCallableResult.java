package edu.mst.db.nlm.goldstd.corpora.annot;

import java.util.Map;

import edu.mst.db.nlm.goldstd.corpora.GoldStdAnnot;

public class PersistNlmGoldStdAnnotCallableResult {
    
    public int pmid = -1;
    public boolean isDebug;
    public int nbrGoldStdAnnotIdentified = 0;
    public int nbrGoldStdAnnotPersisted = 0;
    public Map<String, GoldStdAnnot> annotations = null;
    public Exception ex = null;

}

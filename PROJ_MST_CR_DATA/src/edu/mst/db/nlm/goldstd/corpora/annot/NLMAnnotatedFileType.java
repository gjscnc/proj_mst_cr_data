package edu.mst.db.nlm.goldstd.corpora.annot;

public enum NLMAnnotatedFileType {
    TEXT,
    ANNOTATION
}

package edu.mst.db.nlm.goldstd.corpora;

import java.util.Comparator;
import java.util.SortedSet;
import java.util.TreeSet;

import edu.mst.db.nlm.corpora.Corpus;

/**
 * Annotations parsed from the NLM annotation database.
 * 
 * @author gjs
 *
 */
public class GoldStdAnnot implements Comparable<GoldStdAnnot> {

    private int id;
    private int pmid;
    private String annotId;
    private String annotType;
    private int sentenceNbr;
    private String cui;
    private String text;
    private String cuiName;
    private String bestMatchAui;
    private boolean existsInOntol;
    private int sentFirstWordNbr;
    private int beginDocPos;
    private int sentPhraseNbr;
    private int phraseFirstWordNbr;
    private long bestMatchSnomedConceptUid;
    private SortedSet<GoldStdAnnotWord> words = new TreeSet<GoldStdAnnotWord>();
    private Corpus corpus = null;
    private long bestMatchSnomedNameUid;
    private String bestMatchSnomedNameText;
    private boolean isCuiInNames = false;

    public static class Comparators {
	public static final Comparator<GoldStdAnnot> byDbId = (
		GoldStdAnnot annot1,
		GoldStdAnnot annot2) -> Integer.compare(annot1.id, annot2.id);
	private static final Comparator<GoldStdAnnot> byPmid = (
		GoldStdAnnot annot1, GoldStdAnnot annot2) -> Integer
			.compare(annot1.pmid, annot2.pmid);
	private static final Comparator<GoldStdAnnot> bySentNbr = (
		GoldStdAnnot annot1, GoldStdAnnot annot2) -> Integer
			.compare(annot1.sentenceNbr, annot2.sentenceNbr);
	private static final Comparator<GoldStdAnnot> bySentFirstWordId = (
		GoldStdAnnot annot1, GoldStdAnnot annot2) -> Integer.compare(
			annot1.sentFirstWordNbr, annot2.sentFirstWordNbr);
	public static final Comparator<GoldStdAnnot> byPosition = (
		GoldStdAnnot annot1, GoldStdAnnot annot2) -> byPmid
			.thenComparing(bySentNbr)
			.thenComparing(bySentFirstWordId)
			.compare(annot1, annot2);
    }

    @Override
    public int compareTo(GoldStdAnnot annot) {
	return Comparators.byDbId.compare(this, annot);
    }

    @Override
    public boolean equals(Object obj) {
	if (obj instanceof GoldStdAnnot) {
	    GoldStdAnnot annot = (GoldStdAnnot) obj;
	    return id == annot.id;
	}
	return false;
    }

    @Override
    public int hashCode() {
	return Integer.hashCode(id);
    }

    public int getId() {
	return id;
    }

    public void setId(int id) {
	this.id = id;
    }

    public int getPmid() {
	return pmid;
    }

    public void setPmid(int pmid) {
	this.pmid = pmid;
    }

    public String getAnnotId() {
	return annotId;
    }

    public void setAnnotId(String annotId) {
	this.annotId = annotId;
    }

    public String getAnnotType() {
	return annotType;
    }

    public void setAnnotType(String annotType) {
	this.annotType = annotType;
    }

    public int getSentenceNbr() {
	return sentenceNbr;
    }

    public void setSentenceNbr(int sentenceNbr) {
	this.sentenceNbr = sentenceNbr;
    }

    public String getCui() {
	return cui;
    }

    public void setCui(String cui) {
	this.cui = cui;
    }

    public String getText() {
	return text;
    }

    public void setText(String text) {
	this.text = text;
    }

    public String getCuiName() {
	return cuiName;
    }

    public void setCuiName(String cuiName) {
	this.cuiName = cuiName;
    }

    public String getBestMatchAui() {
        return bestMatchAui;
    }

    public void setBestMatchAui(String bestMatchAui) {
        this.bestMatchAui = bestMatchAui;
    }

    public boolean isExistsInOntol() {
	return existsInOntol;
    }

    public void setExistsInOntol(boolean existsInOntol) {
	this.existsInOntol = existsInOntol;
    }

    public int getSentFirstWordNbr() {
	return sentFirstWordNbr;
    }

    public void setSentFirstWordNbr(int sentFirstWordNbr) {
	this.sentFirstWordNbr = sentFirstWordNbr;
    }

    public int getBeginDocPos() {
	return beginDocPos;
    }

    public void setBeginDocPos(int beginDocPos) {
	this.beginDocPos = beginDocPos;
    }

    public int getSentPhraseNbr() {
	return sentPhraseNbr;
    }

    public void setSentPhraseNbr(int sentPhraseNbr) {
	this.sentPhraseNbr = sentPhraseNbr;
    }

    public int getPhraseFirstWordNbr() {
	return phraseFirstWordNbr;
    }

    public void setPhraseFirstWordNbr(int phraseFirstWordNbr) {
	this.phraseFirstWordNbr = phraseFirstWordNbr;
    }

    public long getBestMatchSnomedConceptUid() {
	return bestMatchSnomedConceptUid;
    }

    public void setBestMatchSnomedConceptUid(long bestMatchSnomedConceptUid) {
	this.bestMatchSnomedConceptUid = bestMatchSnomedConceptUid;
    }

    public SortedSet<GoldStdAnnotWord> getWords() {
	return words;
    }

    public void setWords(SortedSet<GoldStdAnnotWord> words) {
	this.words = words;
    }

    public Corpus getCorpus() {
	return corpus;
    }

    public void setCorpus(Corpus corpus) {
	this.corpus = corpus;
    }

    public long getBestMatchSnomedNameUid() {
        return bestMatchSnomedNameUid;
    }

    public void setBestMatchSnomedNameUid(long bestMatchSnomedNameUid) {
        this.bestMatchSnomedNameUid = bestMatchSnomedNameUid;
    }

    public String getBestMatchSnomedNameText() {
        return bestMatchSnomedNameText;
    }

    public void setBestMatchSnomedNameText(String bestMatchSnomedNameText) {
        this.bestMatchSnomedNameText = bestMatchSnomedNameText;
    }

    public boolean isCuiInNames() {
        return isCuiInNames;
    }

    public void setIsCuiInNames(boolean isCuiInNames) {
        this.isCuiInNames = isCuiInNames;
    }

}

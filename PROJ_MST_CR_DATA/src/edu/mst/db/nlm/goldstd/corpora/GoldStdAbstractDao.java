/**
 * 
 */
package edu.mst.db.nlm.goldstd.corpora;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import edu.mst.db.nlm.corpora.Corpus;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * @author gjs
 *
 */
public class GoldStdAbstractDao {

    public static final String getAbstrSql = "SELECT title, abstract FROM conceptrecogn.goldstd_abstracts WHERE pmid=?";
    public static final String delAbstractForPmidSql = "DELETE FROM conceptrecogn.goldstd_abstracts where pmid = ?";
    public static final String checkForAbstrSql = "SELECT count(*) FROM conceptrecogn.goldstd_abstracts WHERE pmid=?";
    public static final String checkForAbstrCorpusSql = "SELECT count(*) FROM conceptrecogn.goldstd_abstrcorpus "
	    + "WHERE pmid = ? and sourceCorpus = ?";
    public static final String insertAbstrSql = "INSERT INTO conceptrecogn.goldstd_abstracts (pmid, title, abstract) "
	    + "VALUES (?,?,?)";
    public static final String insertAbstrCorpusSql = "INSERT INTO conceptrecogn.goldstd_abstrcorpus (pmid, sourceCorpus) values(?,?)";
    public static final String getPmidsForCorpusSql = "SELECT pmid FROM conceptrecogn.goldstd_abstrcorpus WHERE sourceCorpus = ?";
    public static final String getAllPmidsSql = "SELECT pmid FROM conceptrecogn.goldstd_abstrcorpus order by pmid";

    public GoldStdAbstract marshalAbstract(int pmid,
	    JdbcConnectionPool connPool) throws Exception {
	GoldStdAbstract abstr = null;
	try (Connection conn = connPool.getConnection()) {
	    abstr = marshalAbstract(pmid, conn);
	}
	return abstr;
    }

    /**
     * Marshal all abstracts associated with a corpus.
     * 
     * @param corpus
     * @param conn
     * @return
     * @throws Exception
     */
    public List<GoldStdAbstract> marshalAbstractsForCorpus(Corpus corpus,
	    Connection conn) throws Exception {

	List<GoldStdAbstract> abstracts = new ArrayList<GoldStdAbstract>();

	String getAbstrForCorpusSql = "select c.pmid, a.title, a.abstract from "
		+ "(select pmid, sourceCorpus from conceptrecogn.goldstd_abstrcorpus where sourceCorpus=?) as c, "
		+ "(select pmid, title, abstract from conceptrecogn.goldstd_abstracts) as a "
		+ "where c.pmid = a.pmid order by c.pmid";
	try (PreparedStatement getAbstrForCorpusStmt = conn
		.prepareStatement(getAbstrForCorpusSql)) {
	    getAbstrForCorpusStmt.setString(1, corpus.toString());
	    try (ResultSet rs = getAbstrForCorpusStmt.executeQuery()) {
		while (rs.next()) {
		    int pmid = rs.getInt(1);
		    String title = rs.getString(2);
		    String text = rs.getString(3);
		    GoldStdAbstract abstr = new GoldStdAbstract(pmid, title,
			    text);
		    abstracts.add(abstr);
		}
	    }
	}

	return abstracts;
    }

    public SortedSet<Integer> getPmidsForCorpus(Corpus corpus,
	    JdbcConnectionPool connPool) throws SQLException {
	SortedSet<Integer> pmids = new TreeSet<Integer>();
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement getPmidsStmt = conn
		    .prepareStatement(getPmidsForCorpusSql)) {
		getPmidsStmt.setString(1, corpus.toString());
		try (ResultSet rs = getPmidsStmt.executeQuery()) {
		    while (rs.next()) {
			pmids.add(rs.getInt(1));
		    }
		}
	    }
	}
	return pmids;
    }

    public static int[] getAllPmids(JdbcConnectionPool connPool)
	    throws SQLException {

	int[] pmids = null;
	List<Integer> pmidsList = new ArrayList<Integer>();
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement getAllStmt = conn
		    .prepareStatement(getAllPmidsSql)) {
		try (ResultSet rs = getAllStmt.executeQuery()) {
		    while (rs.next()) {
			pmidsList.add(rs.getInt(1));
		    }
		}
	    }
	}
	if (pmidsList.size() > 0) {
	    Collections.sort(pmidsList);
	    pmids = new int[pmidsList.size()];
	    for (int i = 0; i < pmidsList.size(); i++) {
		pmids[i] = pmidsList.get(i);
	    }
	}
	return pmids;
    }

    public GoldStdAbstract marshalAbstract(int pmid, Connection conn)
	    throws Exception {
	GoldStdAbstract abstr = null;
	try (PreparedStatement getAbstrStmt = conn
		.prepareStatement(getAbstrSql)) {
	    getAbstrStmt.setInt(1, pmid);
	    try (ResultSet rs = getAbstrStmt.executeQuery()) {
		if (rs.next()) {
		    abstr = new GoldStdAbstract(pmid, rs.getString(1),
			    rs.getString(2));
		}
	    }
	}
	return abstr;
    }

    public static int deleteAbstrForCorpus(Corpus corpus,
	    JdbcConnectionPool connPool) throws SQLException {
	int nbrDeleted = 0;
	SortedSet<Integer> pmids = new TreeSet<Integer>();
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement getPmidsForCorpusStmt = conn
		    .prepareStatement(getPmidsForCorpusSql);
		    PreparedStatement delAbstrForPmidStmt = conn
			    .prepareStatement(delAbstractForPmidSql)) {
		getPmidsForCorpusStmt.setString(1, corpus.toString());
		try (ResultSet rs = getPmidsForCorpusStmt.executeQuery()) {
		    while (rs.next()) {
			pmids.add(rs.getInt(1));
		    }
		}
		Iterator<Integer> pmidsIter = pmids.iterator();
		while (pmidsIter.hasNext()) {
		    int pmid = pmidsIter.next();
		    delAbstrForPmidStmt.setInt(1, pmid);
		    nbrDeleted += delAbstrForPmidStmt.executeUpdate();
		}
	    }
	}
	return nbrDeleted;
    }

}

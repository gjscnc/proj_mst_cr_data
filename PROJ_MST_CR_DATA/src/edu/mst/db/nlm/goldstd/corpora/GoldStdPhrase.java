/**
 * 
 */
package edu.mst.db.nlm.goldstd.corpora;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import edu.mst.db.util.Constants;

/**
 * This class serves as the tool to marshal phrase objects from the database and
 * to persist phrase objects into the database.
 * 
 * @author George
 *
 */
public class GoldStdPhrase implements Comparable<GoldStdPhrase> {

    // private int id = Constants.uninitializedIntVal;
    private int pmid = Constants.uninitializedIntVal;
    private int sentNbr = Constants.uninitializedIntVal;
    private int sentPhraseNbr = Constants.uninitializedIntVal;
    private int beginSentWordNbr = Constants.uninitializedIntVal;
    private int endSentWordNbr = Constants.uninitializedIntVal;
    private int nbrWords = Constants.uninitializedIntVal;
    private int beginDocPos = Constants.uninitializedIntVal;
    private int endDocPos = Constants.uninitializedIntVal;
    private String text = null;
    private String textLc = null;
    private List<GoldStdPhraseWord> words = new ArrayList<GoldStdPhraseWord>();
    private String posTag = null;
    private Set<String> cuis = new HashSet<String>();

    @Override
    public int compareTo(GoldStdPhrase phrase) {
	int i = Integer.compare(pmid, phrase.pmid);
	if (i != 0) {
	    return i;
	}
	i = Integer.compare(sentNbr, phrase.sentNbr);
	if (i != 0) {
	    return i;
	}
	return Integer.compare(sentPhraseNbr, phrase.sentPhraseNbr);
    }

    @Override
    public boolean equals(Object obj) {
	if (obj instanceof GoldStdPhrase) {
	    GoldStdPhrase phrase = (GoldStdPhrase) obj;
	    return pmid == phrase.pmid && sentNbr == phrase.sentNbr
		    && sentPhraseNbr == phrase.sentPhraseNbr;
	}
	return false;
    }

    @Override
    public int hashCode() {
	return Integer.hashCode(pmid) + 7 * Integer.hashCode(sentNbr)
		+ 37 * Integer.hashCode(sentPhraseNbr);
    }

    public static class Comparators {
	public static final Comparator<GoldStdPhrase> byPmid = (
		GoldStdPhrase phrase1, GoldStdPhrase phrase2) -> Double
			.compare(phrase1.pmid, phrase2.pmid);
	public static final Comparator<GoldStdPhrase> bySentNbr = (
		GoldStdPhrase phrase1, GoldStdPhrase phrase2) -> Double
			.compare(phrase1.sentNbr, phrase2.sentNbr);
	public static final Comparator<GoldStdPhrase> byBeginSentWordNbr = (
		GoldStdPhrase phrase1, GoldStdPhrase phrase2) -> Double.compare(
			phrase1.beginSentWordNbr, phrase2.beginSentWordNbr);
	public static final Comparator<GoldStdPhrase> byDocBeginPos = (
		GoldStdPhrase phrase1, GoldStdPhrase phrase2) -> Double
			.compare(phrase1.beginDocPos, phrase2.beginDocPos);
	public static final Comparator<GoldStdPhrase> byPmid_SentNbr_BeginSentWordNbr = (
		GoldStdPhrase phrase1, GoldStdPhrase phrase2) -> byPmid
			.thenComparing(bySentNbr)
			.thenComparing(byBeginSentWordNbr)
			.compare(phrase1, phrase2);
	public static final Comparator<GoldStdPhrase> byPmid_SentNbr_DocBeginPosition = (
		GoldStdPhrase phrase1, GoldStdPhrase phrase2) -> byPmid
			.thenComparing(bySentNbr).thenComparing(byDocBeginPos)
			.compare(phrase1, phrase2);
    };

    public String toString() {
	return String.format(
		"PMID %1$d, sentence nbr %2$,d, phrase nbr %3$,d, text '%4$s'",
		pmid, sentNbr, sentPhraseNbr, text);
    }

    public int getPmid() {
	return pmid;
    }

    public void setPmid(int pmid) {
	this.pmid = pmid;
    }

    public int getSentNbr() {
	return sentNbr;
    }

    public void setSentNbr(int sentNbr) {
	this.sentNbr = sentNbr;
    }

    public int getBeginSentWordNbr() {
	return beginSentWordNbr;
    }

    public void setBeginSentWordNbr(int beginSentWordNbr) {
	this.beginSentWordNbr = beginSentWordNbr;
    }

    public int getEndSentWordNbr() {
	return endSentWordNbr;
    }

    public void setEndSentWordNbr(int endSentWordNbr) {
	this.endSentWordNbr = endSentWordNbr;
    }

    public int getNbrWords() {
	return nbrWords;
    }

    public void setNbrWords(int nbrWords) {
	this.nbrWords = nbrWords;
    }

    public int getBeginDocPos() {
	return beginDocPos;
    }

    public void setBeginDocPos(int beginDocPos) {
	this.beginDocPos = beginDocPos;
    }

    public int getEndDocPos() {
	return endDocPos;
    }

    public void setEndDocPos(int endDocPos) {
	this.endDocPos = endDocPos;
    }

    public String getText() {
	return text;
    }

    public void setText(String text) {
	this.text = text;
	this.textLc = text.toLowerCase();
    }

    public String getTextLc() {
	return textLc;
    }

    public List<GoldStdPhraseWord> getWords() {
	return words;
    }

    public void setWords(List<GoldStdPhraseWord> phraseWords) {
	this.words = phraseWords;
    }

    public String getPosTag() {
	return posTag;
    }

    public void setPosTag(String posTag) {
	this.posTag = posTag;
    }

    public Set<String> getCuis() {
	return cuis;
    }

    public void setCuis(Set<String> cuis) {
	this.cuis = cuis;
    }

    public int getSentPhraseNbr() {
	return sentPhraseNbr;
    }

    public void setSentPhraseNbr(int sentPhraseNbr) {
	this.sentPhraseNbr = sentPhraseNbr;
    }

}

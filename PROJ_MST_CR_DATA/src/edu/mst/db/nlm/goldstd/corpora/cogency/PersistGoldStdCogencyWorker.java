/**
 * 
 */
package edu.mst.db.nlm.goldstd.corpora.cogency;

import java.util.concurrent.Future;

import edu.mst.concurrent.BaseWorker;
import edu.mst.db.lexicon.StopWords;
import edu.mst.db.nlm.goldstd.corpora.GoldStdAbstractDao;
import edu.mst.db.util.ConnDbName;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * @author gjs
 *
 */
public class PersistGoldStdCogencyWorker extends BaseWorker<Integer> {

    private StopWords stopWords;
    
    private int nbrGoldStdPmids = 0;
    private int[] goldStdPmids = null;

    private int nbrPmidsProcessed = 0;
    private int nbrSentProcessed = 0;
    private int rptInterval = 50;

    public PersistGoldStdCogencyWorker(JdbcConnectionPool connPool) {
	super(connPool);
    }

    @Override
    public void run() throws Exception {

	System.out.println("Retrieving PMIDs from the gold standard corpora");
	goldStdPmids = GoldStdAbstractDao.getAllPmids(connPool);
	nbrGoldStdPmids = goldStdPmids.length;
	System.out.printf("Retrieved %1$,d PMIDs %n", nbrGoldStdPmids);

	System.out.println("Now computing cogency values");

	stopWords = new StopWords(connPool);

	while (currentPos < nbrGoldStdPmids) {

	    maxPos = getNextBatchMaxPos();
	    startPos = currentPos;

	    for (int i = startPos; i <= maxPos; i++) {
		int goldStdPmid = goldStdPmids[i];
		PersistGoldStdCogencyCallable callable = new PersistGoldStdCogencyCallable(
			goldStdPmid, stopWords, connPool);
		svc.submit(callable);
	    }

	    for (int i = startPos; i <= maxPos; i++) {
		Future<Integer> future = svc.take();
		nbrSentProcessed += future.get();
		nbrPmidsProcessed++;
		if (nbrPmidsProcessed % rptInterval == 0) {
		    System.out.printf(
			    "Processed %1$,d abstracts, encompassing %2$,d sentences. %n",
			    nbrPmidsProcessed, nbrSentProcessed);
		}
	    }

	    currentPos = maxPos + 1;

	}

	System.out.printf(
		"Processed total of %1$,d abstracts, encompassing %2$,d sentences. %n",
		nbrPmidsProcessed, nbrSentProcessed);

    }

    @Override
    protected int getNextBatchMaxPos() throws Exception {
	// subtract 1 from max pos since positions are inclusive
	int maxPos = currentPos + threadCallablesBatchSize - 1;
	if (maxPos >= nbrGoldStdPmids - 1)
	    maxPos = nbrGoldStdPmids - 1;
	return maxPos;
    }
    
    public static void main(String[] args) {
	
	try {
	    
	    JdbcConnectionPool connPool = new JdbcConnectionPool(ConnDbName.CONCEPT_RECOGN);
	    try(PersistGoldStdCogencyWorker worker = new PersistGoldStdCogencyWorker(connPool)){
		worker.run();
	    }
	    
	} catch(Exception ex) {
	    ex.printStackTrace();
	}
    }

}

package edu.mst.db.nlm.goldstd.corpora.parse;

public enum TextConceptTagType {
    
    GOLD_STD (1, "GoldStandard"),
    META_MAP (2, "MetaMap"),
    OIC (3, "OIC");

    private int tagTypeId;
    private String tagText;
    
    public int getAnnotTypeId(){
	return tagTypeId;
    }
    
    public String getAnnotText(){
	return tagText;
    }
    
    TextConceptTagType(int tagTypeId, String tagText) {
	this.tagTypeId = tagTypeId;
	this.tagText = tagText;
    }
    
    
}

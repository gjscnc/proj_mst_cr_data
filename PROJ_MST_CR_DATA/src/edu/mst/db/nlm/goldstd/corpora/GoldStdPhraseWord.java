/**
 * 
 */
package edu.mst.db.nlm.goldstd.corpora;

import java.util.Comparator;

/**
 * This class serves as the tool to marshal phrase word objects from the
 * database and persist phrase word objects to the database.
 * <p>
 * This class is instantiated from the MincoMan output from MetaMap. The class
 * {@link GoldStdSentWord} is instantiated from the MetaMap results first (along
 * with the MincoMan parser results) and the PhraseWord is instantiated from the
 * {@link GoldStdSentWord}, along with other input.
 * </p>
 * 
 * @author George
 *
 */
public class GoldStdPhraseWord implements Comparable<GoldStdPhraseWord> {

    private int pmid = 0;
    private int sentNbr = 0;
    private int sentPhraseNbr = 0;
    private int sentWordNbr = 0;
    private int phraseWordNbr = 0;
    private int lexWordId = 0;
    private String text = null;
    private int beginPhrasePos = 0;
    private int endPhrasePos = 0;
    private int beginDocPos = 0;
    private int endDocPos = 0;
    private String posTag = null;
    private long iocTag = 0;
    private boolean isAcrAbbrev = false;
    private int acrAbbrevId = 0;
    private boolean isPunct = false;

    @Override
    public int compareTo(GoldStdPhraseWord word) {
	return Comparators.byPmid_SentNbr_SentPhraseNbr_PhraseWordNbr
		.compare(this, word);
    }

    @Override
    public boolean equals(Object obj) {
	if (obj instanceof GoldStdPhraseWord) {
	    GoldStdPhraseWord word = (GoldStdPhraseWord) obj;
	    return pmid == word.pmid && sentNbr == word.sentNbr
		    && sentPhraseNbr == word.sentPhraseNbr
		    && phraseWordNbr == word.phraseWordNbr;
	}
	return false;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * pmid + result;
	result = prime * sentNbr + result;
	result = prime * sentPhraseNbr + result;
	result = prime * phraseWordNbr + result;
	return Long.valueOf(result).hashCode();
    }

    public static class Comparators {
	public static final Comparator<GoldStdPhraseWord> byPmid = (
		GoldStdPhraseWord word1, GoldStdPhraseWord word2) -> Integer
			.compare(word1.pmid, word2.pmid);
	public static final Comparator<GoldStdPhraseWord> bySentNbr = (
		GoldStdPhraseWord word1, GoldStdPhraseWord word2) -> Integer
			.compare(word1.sentNbr, word2.sentNbr);
	public static final Comparator<GoldStdPhraseWord> bySentWordNbr = (
		GoldStdPhraseWord word1, GoldStdPhraseWord word2) -> Integer
			.compare(word1.sentWordNbr, word2.sentWordNbr);
	public static final Comparator<GoldStdPhraseWord> byPmid_SentNbr_SentWordNbr = (
		GoldStdPhraseWord word1, GoldStdPhraseWord word2) -> byPmid
			.thenComparing(bySentNbr).thenComparing(bySentWordNbr)
			.compare(word1, word2);
	public static final Comparator<GoldStdPhraseWord> bySentPhraseNbr = (
		GoldStdPhraseWord word1, GoldStdPhraseWord word2) -> Integer
			.compare(word1.sentPhraseNbr, word2.sentPhraseNbr);
	public static final Comparator<GoldStdPhraseWord> byPhraseWordNbr = (
		GoldStdPhraseWord word1, GoldStdPhraseWord word2) -> Integer
			.compare(word1.phraseWordNbr, word2.phraseWordNbr);
	public static final Comparator<GoldStdPhraseWord> byPmid_SentNbr_SentPhraseNbr_PhraseWordNbr = (
		GoldStdPhraseWord word1, GoldStdPhraseWord word2) -> byPmid
			.thenComparing(bySentNbr).thenComparing(bySentPhraseNbr)
			.thenComparing(byPhraseWordNbr).compare(word1, word2);
    }

    public String toString() {
	return String.format("PMID %1$d, sentence nbr %2$,d, phrase nbr %3$,d, "
		+ "sentence word nbr %4$,d, phrase word nbr %5,d, text '%6$s'",
		pmid, sentNbr, sentPhraseNbr, sentWordNbr, phraseWordNbr, text);
    }

    public int getPmid() {
	return pmid;
    }

    public void setPmid(int pmid) {
	this.pmid = pmid;
    }

    public int getSentNbr() {
	return sentNbr;
    }

    public void setSentNbr(int sentNbr) {
	this.sentNbr = sentNbr;
    }

    public int getSentPhraseNbr() {
	return sentPhraseNbr;
    }

    public void setSentPhraseNbr(int sentPhraseNbr) {
	this.sentPhraseNbr = sentPhraseNbr;
    }

    public int getSentWordNbr() {
	return sentWordNbr;
    }

    public void setSentWordNbr(int phraseWordNbr) {
	this.sentWordNbr = phraseWordNbr;
    }

    public int getPhraseWordNbr() {
	return phraseWordNbr;
    }

    public void setPhraseWordNbr(int phraseWordNbr) {
	this.phraseWordNbr = phraseWordNbr;
    }

    public String getText() {
	return text;
    }

    public void setText(String text) {
	this.text = text;
    }

    public int getBeginPhrasePos() {
	return beginPhrasePos;
    }

    public void setBeginPhrasePos(int beginPhrasePos) {
	this.beginPhrasePos = beginPhrasePos;
    }

    public int getEndPhrasePos() {
	return endPhrasePos;
    }

    public void setEndPhrasePos(int endPhrasePos) {
	this.endPhrasePos = endPhrasePos;
    }

    public int getLexWordId() {
	return lexWordId;
    }

    public void setLexWordId(int lexWordId) {
	this.lexWordId = lexWordId;
    }

    public int getBeginDocPos() {
	return beginDocPos;
    }

    public void setBeginDocPos(int beginDocPos) {
	this.beginDocPos = beginDocPos;
    }

    public int getEndDocPos() {
	return endDocPos;
    }

    public void setEndDocPos(int endDocPos) {
	this.endDocPos = endDocPos;
    }

    public String getPosTag() {
	return posTag;
    }

    public void setPosTag(String posTag) {
	this.posTag = posTag;
    }

    public long getIocTag() {
	return iocTag;
    }

    public void setIocTag(long iocTag) {
	this.iocTag = iocTag;
    }

    public boolean isAcrAbbrev() {
	return isAcrAbbrev;
    }

    public void setIsAcrAbbrev(boolean isAcrAbbrev) {
	this.isAcrAbbrev = isAcrAbbrev;
    }

    public int getAcrAbbrevId() {
	return acrAbbrevId;
    }

    public void setAcrAbbrevId(int acrAbbrevId) {
	this.acrAbbrevId = acrAbbrevId;
    }

    public boolean isPunct() {
	return isPunct;
    }

    public void setIsPunct(boolean isPunct) {
	this.isPunct = isPunct;
    }

}

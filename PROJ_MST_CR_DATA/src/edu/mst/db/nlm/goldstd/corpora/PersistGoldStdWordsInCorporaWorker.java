/**
 * 
 */
package edu.mst.db.nlm.goldstd.corpora;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.Future;

import edu.mst.concurrent.BaseWorker;
import edu.mst.db.lexicon.StopWords;
import edu.mst.db.nlm.corpora.NlmAbstractSentence;
import edu.mst.db.nlm.corpora.NlmAbstractWord;
import edu.mst.db.nlm.corpora.NlmAbstractWordDao;
import edu.mst.db.nlm.corpora.cogency.PersistNlmCogencyCallable;
import edu.mst.db.nlm.corpora.condprob.CorporaCondWordProb;
import edu.mst.db.nlm.corpora.condprob.CorporaCondWordProbDao;
import edu.mst.db.util.ConnDbName;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * Perform functions necessary to add the gold standard word sequences to the
 * corpora.
 * <p>
 * <ul>
 * <li>Add word sequences from the gold standard annotations to word sequences
 * for the NLM corpora</li>
 * <li>Update conditional word frequencies and and conditional word
 * probabilities for new sentences added to corpora</li>
 * </ul>
 * 
 * @author gjs
 *
 */
public class PersistGoldStdWordsInCorporaWorker extends BaseWorker<Integer> {

    private int[] goldStdPmids = null;
    private int nbrGoldStdPmids = 0;
    private StopWords stopWords;
    private NlmAbstractWordDao abstrWordDao = new NlmAbstractWordDao();
    private GoldStdSentenceDao goldStdSentDao = new GoldStdSentenceDao();
    private CorporaCondWordProbDao condProbDao = new CorporaCondWordProbDao();

    private SortedSet<Integer> newWordPmids = new TreeSet<Integer>();
    private static final String pmidsFileDir = "/home/gjs/";
    private static final String pmidsFileName = "newPmids.txt";
    private Map<Integer, Map<Integer, CorporaCondWordProb>> condProbByPred = new HashMap<Integer, Map<Integer, CorporaCondWordProb>>();
    private List<CorporaCondWordProb> allProbs = new ArrayList<CorporaCondWordProb>();

    private int nbrPmidsProcessed = 0;
    private int rptInterval = 50;

    // for computing and persisting new conditional probabilities
    private int nbrPredWordIds = 0;
    private int[] predWordIds = null;
    private int nbrPredWordsProcess = 0;

    private boolean isProcessPmids = false;
    private boolean isProcessPredWords = false;

    public PersistGoldStdWordsInCorporaWorker(JdbcConnectionPool connPool)
	    throws Exception {
	super(connPool);
	stopWords = new StopWords(connPool);
	System.out.println(
		"Retrieving abstract PMIDs for gold standard annotation abstracts");
	goldStdPmids = GoldStdAbstractDao.getAllPmids(connPool);
	nbrGoldStdPmids = goldStdPmids.length;
	System.out.printf("Retrieved %1$,d gold standard abstract PMIDs %n",
		nbrGoldStdPmids);
    }

    public void persistNewWordSeq() throws Exception {

	System.out.println(
		"Now adding words sequences to NLM abstracts database");
	List<NlmAbstractWord> newAbstrWords = new ArrayList<NlmAbstractWord>();
	for (int i = 0; i < nbrGoldStdPmids; i++) {
	    int goldStdPmid = goldStdPmids[i];
	    // if word sequence already exists, continue to next PMID
	    SortedSet<NlmAbstractWord> abstrWords = abstrWordDao
		    .marshalWords(goldStdPmid, connPool);
	    if (abstrWords.size() > 0) {
		continue;
	    }
	    SortedSet<GoldStdSentence> goldStdSents = goldStdSentDao
		    .marshalAllSentencesForPmid(goldStdPmid, connPool);
	    Iterator<GoldStdSentence> goldStdSentIter = goldStdSents.iterator();
	    while (goldStdSentIter.hasNext()) {
		GoldStdSentence goldStdSent = goldStdSentIter.next();
		Iterator<GoldStdSentWord> goldStdSentWordIter = goldStdSent
			.getWords().iterator();
		while (goldStdSentWordIter.hasNext()) {
		    GoldStdSentWord goldStdWord = goldStdSentWordIter.next();
		    NlmAbstractWord abstrWord = new NlmAbstractWord();
		    abstrWord.setPmid(goldStdWord.getPmid());
		    abstrWord.setSentNbr(goldStdWord.getSentNbr());
		    abstrWord.setSentWordNbr(goldStdWord.getSentWordNbr());
		    abstrWord.setToken(goldStdWord.getText().toLowerCase());
		    abstrWord.setLexWordId(goldStdWord.getLexWordId());
		    abstrWord.setBeginOffset(goldStdWord.getBeginDocPos());
		    abstrWord.setEndOffset(goldStdWord.getEndDocPos());
		    abstrWord.setIsPunctOrDelimChar(goldStdWord.isPunct());
		    abstrWord.setIsStopWord(stopWords.getLexWordIds()
			    .contains(goldStdWord.getLexWordId()));
		    newAbstrWords.add(abstrWord);
		}
	    }
	    nbrPmidsProcessed++;
	    if (nbrPmidsProcessed % rptInterval == 0) {
		System.out.printf(
			"Processed %1$,d PMIDs, extracting %2$,d abstract words %n",
			nbrPmidsProcessed, newAbstrWords.size());
	    }
	    newWordPmids.add(goldStdPmid);
	}
	System.out.printf(
		"Processed total of %1$,d PMIDs, extracting %2$,d abstract words %n",
		nbrPmidsProcessed, newAbstrWords.size());
	System.out.println("Now persisting words to database.");
	abstrWordDao.persist(newAbstrWords, connPool);
	saveNewPmidsToFile();
	System.out.printf(
		"Processed total of %1$,d PMIDS, "
			+ "persisting %2$,d abstract words.%n",
		nbrPmidsProcessed, newAbstrWords.size());
    }

    /**
     * Panic button if problems occur
     * 
     * @throws SQLException
     */
    public void deleteNewWordSeq() throws SQLException {
	for (int i = 0; i < nbrGoldStdPmids; i++) {
	    int goldStdPmid = goldStdPmids[i];
	    abstrWordDao.deleteWordsForPmid(goldStdPmid, connPool);
	}
    }

    private void saveNewPmidsToFile() throws IOException {
	Path file = Paths.get(pmidsFileDir + pmidsFileName);
	List<String> newWordPmidsStr = new ArrayList<String>();
	for (long pmid : newWordPmids) {
	    newWordPmidsStr.add(String.valueOf(pmid));
	}
	Files.write(file, newWordPmidsStr, StandardCharsets.UTF_8);
	System.out.printf("Saved %1$,d new word PMIDs to file.%n",
		newWordPmids.size());
    }

    private void marshalNewPmidsFromFile() throws IOException {
	newWordPmids = new TreeSet<Integer>();
	try (FileReader reader = new FileReader(pmidsFileDir + pmidsFileName);
		BufferedReader bufferedReader = new BufferedReader(reader)) {
	    String line;
	    while ((line = bufferedReader.readLine()) != null) {
		Integer pmid = Integer.valueOf(line);
		newWordPmids.add(pmid);
	    }
	}
	System.out.printf("Read %1$,d new word PMIDs from file.%n",
		newWordPmids.size());
    }

    public void computeFreqDeltas() throws SQLException, IOException {

	System.out.println(
		"Now computing conditional frequencies for words in the gold standard abstracts");

	marshalNewPmidsFromFile();
	nbrPmidsProcessed = 0;
	Iterator<Integer> newWordPmidsIter = newWordPmids.iterator();
	while (newWordPmidsIter.hasNext()) {
	    int newWordsPmid = newWordPmidsIter.next();
	    SortedSet<NlmAbstractSentence> abstrSents = abstrWordDao
		    .marshalSentences(newWordsPmid, connPool);
	    Iterator<NlmAbstractSentence> abstrSentIter = abstrSents.iterator();
	    while (abstrSentIter.hasNext()) {
		NlmAbstractSentence abstrSent = abstrSentIter.next();
		NlmAbstractWord[] words = new NlmAbstractWord[abstrSent
			.getWords().size()];
		int pos = 0;
		Iterator<NlmAbstractWord> wordsIter = abstrSent.getWords()
			.iterator();
		while (wordsIter.hasNext()) {
		    NlmAbstractWord word = wordsIter.next();
		    words[pos] = word;
		    pos++;
		}
		int nbrWords = words.length;
		for (int i = nbrWords - 1; i >= 1; i--) {
		    NlmAbstractWord predicate = words[i];
		    if (predicate.isStopWord()
			    || predicate.isPunctOrDelimChar()) {
			continue;
		    }
		    Integer predWordId = predicate.getLexWordId();
		    if (!condProbByPred.keySet().contains(predWordId)) {
			condProbByPred.put(predWordId,
				new HashMap<Integer, CorporaCondWordProb>());
		    }
		    Map<Integer, CorporaCondWordProb> condProbsByFact = condProbByPred
			    .get(predWordId);
		    for (int j = i - 1; j >= 0; j--) {
			NlmAbstractWord fact = words[j];
			if (fact.isStopWord() || fact.isPunctOrDelimChar()) {
			    continue;
			}
			Integer factWordId = fact.getLexWordId();
			if (factWordId == predWordId) {
			    continue;
			}
			if (condProbsByFact.containsKey(factWordId)) {
			    CorporaCondWordProb condProb = condProbsByFact
				    .get(factWordId);
			    long freq = condProb.getFreq();
			    freq++;
			    condProb.setFreq(freq);
			} else {
			    CorporaCondWordProb condProb = new CorporaCondWordProb();
			    condProb.setPredicateWordId(predWordId);
			    condProb.setAssumedFactWordId(factWordId);
			    condProb.setFreq(1L);
			    condProbsByFact.put(factWordId, condProb);
			}
		    }
		}
	    }
	    nbrPmidsProcessed++;
	    if (nbrPmidsProcessed % rptInterval == 0) {
		System.out.printf(
			"Computed conditional frequencies for %1$,d PMIDs %n",
			nbrPmidsProcessed);
	    }
	}
	System.out.printf(
		"Computed conditional frequencies for total of %1$,d PMIDs %n",
		nbrPmidsProcessed);

    }

    public void persistCondFreq() throws Exception {
	if (condProbByPred == null || condProbByPred.size() == 0) {
	    throw new Exception(
		    "Run method for computing conditional frequences "
			    + "before this method can run");
	}
	System.out.println("Now persisting conditional frequencies");
	System.out.println("Compiling list of delta frequencies to persist");
	allProbs = new ArrayList<CorporaCondWordProb>();
	for (Integer predWordId : condProbByPred.keySet()) {
	    for (Integer factWordId : condProbByPred.get(predWordId).keySet()) {
		allProbs.add(condProbByPred.get(predWordId).get(factWordId));
	    }
	}
	System.out.println("Persisting delta conditional frequencies");
	condProbDao.persistOrUpdateFreqBatch(allProbs, connPool);
	System.out.printf("Persisted %1$,d new conditional frequencies %n",
		allProbs.size());
    }

    public void persistCondProb() throws Exception {

	if (condProbByPred == null || condProbByPred.size() == 0) {
	    System.out.println(
		    "Computing conditional frequencies to extract predicate word IDs");
	    computeFreqDeltas();
	}

	/*
	 * to reduce memory requirement, extract set of predicate IDs, then
	 * delete prior collections of conditional probabilities
	 */

	List<Integer> predIds = new ArrayList<Integer>();
	predIds.addAll(condProbByPred.keySet());
	condProbByPred.clear();
	allProbs.clear();
	predWordIds = new int[predIds.size()];
	nbrPredWordIds = predWordIds.length;
	for (int i = 0; i < nbrPredWordIds; i++) {
	    predWordIds[i] = predIds.get(i);
	}
	predIds.clear();

	System.out.printf(
		"Now computing and persisting conditional probabilities "
			+ "for %1$,d predicate words. %n",
		nbrPredWordIds);
	isProcessPredWords = true;
	int nbrCondProbs = 0;
	while (currentPos < nbrGoldStdPmids) {

	    maxPos = getNextBatchMaxPos();
	    startPos = currentPos;

	    for (int i = startPos; i <= maxPos; i++) {
		int predWordId = predWordIds[i];
		PersistGoldStdWordsInCorporaCallable callable = new PersistGoldStdWordsInCorporaCallable(
			predWordId, connPool);
		svc.submit(callable);
	    }

	    for (int i = startPos; i <= maxPos; i++) {
		Future<Integer> future = svc.take();
		nbrCondProbs += future.get();
		nbrPredWordsProcess++;
		if (nbrPredWordsProcess % rptInterval == 0) {
		    System.out.printf(
			    "Persisted updates for %1$,d predicate words, "
				    + "updating %2$,d conditional probabilities %n",
			    nbrPredWordsProcess, nbrCondProbs);
		}
	    }

	    currentPos = maxPos + 1;

	}
	System.out.printf(
		"Persisted updates for total of %1$,d predicate words, "
			+ "updating %2$,d conditional probabilities %n",
		nbrPredWordsProcess, nbrCondProbs);
    }

    public void persistCumCogencies() throws Exception {

	isProcessPmids = true;
	marshalNewPmidsFromFile();
	nbrGoldStdPmids = goldStdPmids.length;

	System.out.printf("Computing corpora word cogencies for %1$,d PMIDs %n",
		nbrGoldStdPmids);

	int nbrPmidsProcessed = 0;
	int nbrSentProcessed = 0;

	while (currentPos < nbrGoldStdPmids) {

	    maxPos = getNextBatchMaxPos();
	    startPos = currentPos;

	    for (int i = startPos; i <= maxPos; i++) {
		int pmid = goldStdPmids[i];
		PersistNlmCogencyCallable callable = new PersistNlmCogencyCallable(
			pmid, connPool);
		svc.submit(callable);
	    }

	    for (int i = startPos; i <= maxPos; i++) {
		Future<Integer> future = svc.take();
		nbrSentProcessed += future.get();
		nbrPmidsProcessed++;
		if (nbrPmidsProcessed % rptInterval == 0) {
		    System.out.printf(
			    "Processed %1$,d abstracts, encompassing %2$,d sentences. %n",
			    nbrPmidsProcessed, nbrSentProcessed);
		}
	    }

	    currentPos = maxPos + 1;

	}
	System.out.printf(
		"Processed total of %1$,d abstracts, encompassing %2$,d sentences. %n",
		nbrPmidsProcessed, nbrSentProcessed);

    }

    @Override
    public void run() throws Exception {
	// persist.deleteNewWordSeq();
	// persist.persistNewWordSeq();
	// computeFreqDeltas();
	// persist.persistCondFreq();
	// persistCondProb();
	persistCumCogencies();
    }

    @Override
    protected int getNextBatchMaxPos() throws Exception {
	// subtract 1 from max pos since positions are inclusive
	int nbrIds = 0;
	if (isProcessPmids) {
	    nbrIds = nbrGoldStdPmids;
	}
	if (isProcessPredWords) {
	    nbrIds = nbrPredWordIds;
	}
	int maxPos = currentPos + threadCallablesBatchSize - 1;
	if (maxPos >= nbrIds - 1)
	    maxPos = nbrIds - 1;
	return maxPos;
    }

    public static void main(String[] args) {

	try {

	    JdbcConnectionPool connPool = new JdbcConnectionPool(
		    ConnDbName.CONCEPT_RECOGN);

	    try (PersistGoldStdWordsInCorporaWorker worker = new PersistGoldStdWordsInCorporaWorker(
		    connPool)) {
		worker.run();
	    }

	} catch (Exception ex) {
	    ex.printStackTrace();
	}

    }

}

/**
 * 
 */
package edu.mst.db.nlm.goldstd.corpora;

import java.util.ArrayList;
import java.util.List;

import edu.mst.db.util.Constants;
import edu.mst.conceptrecogn.tag.TextTagSet;

/**
 * This class contains the properties of a POS phrase in text.
 * <p>
 * "Text" typically refers to a sentence, although that is not required.
 * <p>
 * The POS word list contains the ordered words in the POS phrase. A static
 * method is provided to extract POS phrases from the text POS parse tree.
 * <p>
 * This class is used for in-memory part-of-speech phrases only when processing
 * NLM abstracts and then performing POS tagging using MetaMap.
 * 
 * @author George
 *
 */
public class GoldStdPosPhrase implements Comparable<GoldStdPosPhrase> {

    private String phraseText = null;
    private int docBeginPos = 0;
    private int docEndPos = 0;
    private int beginSentWordNbr = Constants.uninitializedIntVal;
    private int endSentWordNbr = Constants.uninitializedIntVal;
    private int nbrWords = Constants.uninitializedIntVal;
    private GoldStdSentence sentence = null;
    private int sentPhraseNbr = Constants.uninitializedIntVal;
    private List<GoldStdSentWord> phraseWords = new ArrayList<GoldStdSentWord>();
    private String posTag = null;
    private TextTagSet tagSet = null;

    public GoldStdPosPhrase(GoldStdSentence sentence) {
	this.sentence = sentence;
    }

    public GoldStdPosPhrase() {

    }

    @Override
    public int compareTo(GoldStdPosPhrase posPhrase) {
	int i = Integer.compare(sentence.getPmid(),
		posPhrase.sentence.getPmid());
	if (i != 0)
	    return i;
	i = Integer.compare(sentence.getSentenceNbr(),
		posPhrase.sentence.getSentenceNbr());
	if (i != 0)
	    return i;
	return Integer.compare(docBeginPos, posPhrase.docBeginPos);
    }

    @Override
    public boolean equals(Object obj) {
	if (obj instanceof GoldStdPosPhrase) {
	    GoldStdPosPhrase posPhrase = (GoldStdPosPhrase) obj;
	    return sentence.getPmid() == posPhrase.getSentence().getPmid()
		    && sentence.getSentenceNbr() == posPhrase.getSentence()
			    .getSentenceNbr()
		    && docBeginPos == posPhrase.docBeginPos;
	}
	return false;
    }

    @Override
    public int hashCode() {
	return Integer.hashCode(sentence.getPmid())
		+ 7 * Integer.hashCode(sentence.getSentenceNbr())
		+ 31 * Integer.hashCode(docBeginPos);
    }

    public String getPhraseText() {
	return phraseText;
    }

    public void setPhraseText(String phraseText) {
	this.phraseText = phraseText;
    }

    public int getDocBeginPos() {
	return docBeginPos;
    }

    public void setDocBeginPos(int docBeginPos) {
	this.docBeginPos = docBeginPos;
    }

    public int getDocEndPos() {
	return docEndPos;
    }

    public void setDocEndPos(int docEndPos) {
	this.docEndPos = docEndPos;
    }

    public int getBeginSentWordNbr() {
	return beginSentWordNbr;
    }

    public void setBeginSentWordNbr(int beginSentWordNbr) {
	this.beginSentWordNbr = beginSentWordNbr;
    }

    public int getEndSentWordNbr() {
	return endSentWordNbr;
    }

    public void setEndSentWordNbr(int endSentWordNbr) {
	this.endSentWordNbr = endSentWordNbr;
    }

    public int getNbrWords() {
	return nbrWords;
    }

    public void setNbrWords(int nbrWords) {
	this.nbrWords = nbrWords;
    }

    public GoldStdSentence getSentence() {
	return sentence;
    }

    public List<GoldStdSentWord> getPhraseWords() {
	return phraseWords;
    }

    public void setPhraseWords(List<GoldStdSentWord> phraseWords) {
	this.phraseWords = phraseWords;
    }

    public String getPosTag() {
	return posTag;
    }

    public void setPosTag(String posTag) {
	this.posTag = posTag;
    }

    public TextTagSet getTagSet() {
	return tagSet;
    }

    public int getSentPhraseNbr() {
	return sentPhraseNbr;
    }

    public void setSentPhraseNbr(int sentPhraseNbr) {
	this.sentPhraseNbr = sentPhraseNbr;
    }

}

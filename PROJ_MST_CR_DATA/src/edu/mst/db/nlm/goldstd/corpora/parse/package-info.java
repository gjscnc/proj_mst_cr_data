/**
 * 
 */
/**
 * Components for importing and parsing gold standard data from the NLM.
 * 
 * @author gjs
 *
 */
package edu.mst.db.nlm.goldstd.corpora.parse;
package edu.mst.db.nlm.goldstd.corpora.annot;

public enum NLMAnnotSubType {

    DISORDER(1, "Disorder"), 
    GENERAL_DISORDER(2, "GeneralDisorder"), 
    OVERLY_GENERAL_DISORDER(3, "OverlyGeneralDisorder"),
    METAMAP_FULL(4, "MMI"),
    METAMAP_LITE(5, "MMLite"),
    OIC(5, "OIC");

    private int annotSubTypeId;
    private String annotText;
    
    public int getAnnotSubTypeId() {
	return annotSubTypeId;
   }
    
    public String getAnnotText(){
	return annotText;
    }
    
    NLMAnnotSubType(int annotSubTypeId, String annotText) {
	this.annotSubTypeId = annotSubTypeId;
	this.annotText = annotText;
    }
    
}

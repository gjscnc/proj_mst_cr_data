/**
 * 
 */
package edu.mst.db.nlm.goldstd.corpora.annot;

import java.util.Comparator;

/**
 * @author gjs
 *
 */
public class GoldStdTagNameMatch implements Comparable<GoldStdTagNameMatch> {

    private int pmid = 0;
    private String tagId = null;
    private long conceptNameUid = 0L;
    private long conceptUid = 0L;
    private String conceptNameText = null;
    private String cui = null;
    private String aui = null;
    private String taggedText = null;
    private String auiText = null;

    public static class Comparators {
	public static Comparator<GoldStdTagNameMatch> byPmid = (
		GoldStdTagNameMatch match1,
		GoldStdTagNameMatch match2) -> Integer.compare(match1.pmid,
			match2.pmid);
	public static Comparator<GoldStdTagNameMatch> byTagId = (
		GoldStdTagNameMatch match1,
		GoldStdTagNameMatch match2) -> match1.tagId
			.compareTo(match2.tagId);
	public static Comparator<GoldStdTagNameMatch> byConceptNameUid = (
		GoldStdTagNameMatch match1, GoldStdTagNameMatch match2) -> Long
			.compare(match1.conceptNameUid, match2.conceptNameUid);
	public static Comparator<GoldStdTagNameMatch> byConceptUid = (
		GoldStdTagNameMatch match1, GoldStdTagNameMatch match2) -> Long
			.compare(match1.conceptUid, match2.conceptUid);
	public static Comparator<GoldStdTagNameMatch> byPmid_TagId_NameUid = (
		GoldStdTagNameMatch match1,
		GoldStdTagNameMatch match2) -> byPmid.thenComparing(byTagId)
			.thenComparing(byConceptNameUid)
			.compare(match1, match2);
    }

    @Override
    public int compareTo(GoldStdTagNameMatch match) {
	return Comparators.byPmid_TagId_NameUid.compare(this, match);
    }

    @Override
    public boolean equals(Object obj) {
	if (obj instanceof GoldStdTagNameMatch) {
	    GoldStdTagNameMatch match = (GoldStdTagNameMatch) obj;
	    return pmid == match.pmid && tagId.equals(match.tagId)
		    && conceptNameUid == match.conceptNameUid;
	}
	return false;
    }

    @Override
    public int hashCode() {
	return Integer.hashCode(pmid) + 7 * tagId.hashCode()
		+ 31 * Long.hashCode(conceptNameUid);
    }

    public int getPmid() {
        return pmid;
    }

    public void setPmid(int pmid) {
        this.pmid = pmid;
    }

    public String getTagId() {
        return tagId;
    }

    public void setTagId(String tagId) {
        this.tagId = tagId;
    }

    public long getConceptNameUid() {
        return conceptNameUid;
    }

    public void setConceptNameUid(long conceptNameUid) {
        this.conceptNameUid = conceptNameUid;
    }

    public long getConceptUid() {
        return conceptUid;
    }

    public void setConceptUid(long conceptUid) {
        this.conceptUid = conceptUid;
    }

    public String getConceptNameText() {
        return conceptNameText;
    }

    public void setConceptNameText(String conceptNameText) {
        this.conceptNameText = conceptNameText;
    }

    public String getCui() {
        return cui;
    }

    public void setCui(String cui) {
        this.cui = cui;
    }

    public String getAui() {
        return aui;
    }

    public void setAui(String aui) {
        this.aui = aui;
    }

    public String getTaggedText() {
        return taggedText;
    }

    public void setTaggedText(String taggedText) {
        this.taggedText = taggedText;
    }

    public String getAuiText() {
        return auiText;
    }

    public void setAuiText(String auiText) {
        this.auiText = auiText;
    }

}

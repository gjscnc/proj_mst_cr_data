package edu.mst.db.nlm.goldstd.corpora;

import java.util.Comparator;

/**
 * This class instantiates a sentence word from the database or is used to
 * instantiate a word for persisting in the database. Formatting may occur
 * depending upon the method for instantiating. Always use this class for
 * instantiating words.
 * 
 * @author George J. Shannon
 *
 */
public class GoldStdSentWord implements Comparable<GoldStdSentWord> {

    private int pmid = 0;
    private int sentNbr = 0;
    private int sentWordNbr = 0;
    private int beginDocPos = 0;
    private int endDocPos = 0;
    private String text;
    private int lexWordId = 0;
    private String post;
    private boolean isAcrAbbrev = false;
    private int acrAbbrevId = 0;
    private boolean isPunct = false;
    private double cumLnAbstrCogency = 0.0d;
    private double cumLnSentCogency = 0.0d;

    /**
     * Default constructor
     */
    public GoldStdSentWord() {
    }

    /**
     * Constructor for new word not yet persisted.
     * 
     * @param pmid
     * @param sentenceNbr
     * @param sentWordNbr
     * @param beginDocPos
     * @param endDocPos
     * @param text
     */
    public GoldStdSentWord(int pmid, int sentenceNbr, int sentWordNbr, int beginDocPos,
	    int endDocPos, String text) {
	this.pmid = pmid;
	this.sentNbr = sentenceNbr;
	this.sentWordNbr = sentWordNbr;
	this.beginDocPos = beginDocPos;
	this.endDocPos = endDocPos;
	this.text = text;
    }

    /**
     * Constructor for instantiating word previously persisted.
     * 
     * @param pmid
     * @param sentenceNbr
     * @param sentWordNbr
     * @param beginDocPos
     * @param endDocPos
     * @param text
     * @param lexWordId
     */
    public GoldStdSentWord(int pmid, int sentenceNbr, int sentWordNbr, int beginDocPos,
	    int endDocPos, String text, int lexWordId) {
	this.pmid = pmid;
	this.sentNbr = sentenceNbr;
	this.sentWordNbr = sentWordNbr;
	this.beginDocPos = beginDocPos;
	this.endDocPos = endDocPos;
	this.text = text;
	this.lexWordId = lexWordId;
    }

    /**
     * Instantiates sentence word from phrase word.
     * 
     * @param phraseWord
     * @return
     */
    public static GoldStdSentWord fromPhraseWord(GoldStdPhraseWord phraseWord) {
	GoldStdSentWord sentWord = new GoldStdSentWord();
	sentWord.setPmid(phraseWord.getPmid());
	sentWord.setSentNbr(phraseWord.getSentNbr());
	sentWord.setSentWordNbr(phraseWord.getSentWordNbr());
	sentWord.setText(phraseWord.getText());
	sentWord.setIsAcrAbbrev(phraseWord.isAcrAbbrev());
	sentWord.setAcrAbbrevId(phraseWord.getAcrAbbrevId());
	sentWord.setIsPunct(phraseWord.isPunct());
	sentWord.setBeginDocPos(phraseWord.getBeginDocPos());
	sentWord.setEndDocPos(phraseWord.getEndDocPos());
	sentWord.setPost(phraseWord.getPosTag());
	sentWord.setLexWordId(phraseWord.getLexWordId());
	return sentWord;
    }

    @Override
    public int compareTo(GoldStdSentWord word) {
	return Comparators.ByPmidSentNbrWordNbr.compare(this, word);
    }

    public static class Comparators {
	public static final Comparator<GoldStdSentWord> ByPmid = (
		GoldStdSentWord w1,
		GoldStdSentWord w2) -> Integer.compare(w1.pmid, w2.pmid);
	public static final Comparator<GoldStdSentWord> BySentNbr = (
		GoldStdSentWord w1,
		GoldStdSentWord w2) -> Integer.compare(w1.sentNbr, w2.sentNbr);
	public static final Comparator<GoldStdSentWord> ByWordNbr = (
		GoldStdSentWord w1, GoldStdSentWord w2) -> Integer
			.compare(w1.sentWordNbr, w2.sentWordNbr);
	public static final Comparator<GoldStdSentWord> ByPmidSentNbrWordNbr = (
		GoldStdSentWord w1, GoldStdSentWord w2) -> ByPmid
			.thenComparing(BySentNbr).thenComparing(ByWordNbr)
			.compare(w1, w2);
    }

    @Override
    public boolean equals(Object obj) {
	if (obj instanceof GoldStdSentWord) {
	    GoldStdSentWord word = (GoldStdSentWord) obj;
	    return pmid == word.pmid && sentNbr == word.sentNbr
		    && sentWordNbr == word.sentWordNbr;
	} else
	    return false;
    }

    @Override
    public int hashCode() {
	return Integer.hashCode(pmid) + 7 * Integer.hashCode(sentNbr)
		+ 31 * Integer.hashCode(sentWordNbr);
    }

    public int getPmid() {
	return pmid;
    }

    public void setPmid(int pmid) {
	this.pmid = pmid;
    }

    /**
     * Character position of the beginning of the word in the document.
     * 
     * @return
     */
    public int getBeginDocPos() {
	return beginDocPos;
    }

    /**
     * Character position of the end of the word in the document.
     * 
     * @return
     */
    public int getEndDocPos() {
	return endDocPos;
    }

    public String getText() {
	return text;
    }

    public int getSentNbr() {
	return sentNbr;
    }

    public int getSentWordNbr() {
	return sentWordNbr;
    }

    public int getLexWordId() {
	return lexWordId;
    }

    public void setLexWordId(int lexWordId) {
	this.lexWordId = lexWordId;
    }

    public String getPost() {
	return post;
    }

    public void setPost(String post) {
	this.post = post;
    }

    public boolean isAcrAbbrev() {
	return isAcrAbbrev;
    }

    public void setIsAcrAbbrev(boolean isAcrAbbrev) {
	this.isAcrAbbrev = isAcrAbbrev;
    }

    public int getAcrAbbrevId() {
	return acrAbbrevId;
    }

    public void setAcrAbbrevId(int acrAbbrevId) {
	this.acrAbbrevId = acrAbbrevId;
    }

    public void setSentNbr(int sentenceNbr) {
	this.sentNbr = sentenceNbr;
    }

    public void setSentWordNbr(int sentWordNbr) {
	this.sentWordNbr = sentWordNbr;
    }

    public void setBeginDocPos(int beginDocPos) {
	this.beginDocPos = beginDocPos;
    }

    public void setEndDocPos(int endDocPos) {
	this.endDocPos = endDocPos;
    }

    public void setText(String text) {
	this.text = text;
    }

    public boolean isPunct() {
        return isPunct;
    }

    public void setIsPunct(boolean isPunct) {
        this.isPunct = isPunct;
    }

    public double getCumLnAbstrCogency() {
        return cumLnAbstrCogency;
    }

    public void setCumLnAbstrCogency(double cumLnAbstrCogency) {
        this.cumLnAbstrCogency = cumLnAbstrCogency;
    }

    public double getCumLnSentCogency() {
        return cumLnSentCogency;
    }

    public void setCumLnSentCogency(double cumLnSentCogency) {
        this.cumLnSentCogency = cumLnSentCogency;
    }

}

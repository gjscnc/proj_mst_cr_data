/**
 * 
 */
package edu.mst.db.nlm.goldstd.corpora.annot;

import java.util.Comparator;

import edu.mst.db.nlm.corpora.Corpus;

/**
 * @author gjs
 *
 */
public class GoldStdTag implements Comparable<GoldStdTag> {

    private int pmid = 0;
    private String tagId = null;
    private String category = null;
    private String spansAsText = null;
    private String taggedText = null;
    private String cui = null;
    private String cuiName = null;
    private Corpus corpus = null;
    private boolean isCuiInNames = false;

    public static class Comparators {
	public static Comparator<GoldStdTag> byPmid = (GoldStdTag tag1,
		GoldStdTag tag2) -> Integer.compare(tag1.pmid, tag2.pmid);
	public static Comparator<GoldStdTag> byTagId = (GoldStdTag tag1,
		GoldStdTag tag2) -> tag1.tagId.compareTo(tag2.tagId);
	public static Comparator<GoldStdTag> byPmid_TagId = (GoldStdTag tag1,
		GoldStdTag tag2) -> byPmid.thenComparing(byTagId).compare(tag1,
			tag2);
    }

    @Override
    public int compareTo(GoldStdTag tag) {
	return Comparators.byPmid_TagId.compare(this, tag);
    }

    @Override
    public boolean equals(Object obj) {
	if (obj instanceof GoldStdTag) {
	    GoldStdTag tag = (GoldStdTag) obj;
	    return pmid == tag.pmid && tagId.equals(tag.tagId);
	}
	return false;
    }

    @Override
    public int hashCode() {
	return Integer.hashCode(pmid) + 7 * tagId.hashCode();
    }
    
    public int getPmid() {
        return pmid;
    }

    public void setPmid(int pmid) {
        this.pmid = pmid;
    }

    public String getTagId() {
        return tagId;
    }

    public void setTagId(String tagId) {
        this.tagId = tagId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSpansAsText() {
        return spansAsText;
    }

    public void setSpansAsText(String spansAsText) {
        this.spansAsText = spansAsText;
    }

    public String getTaggedText() {
        return taggedText;
    }

    public void setTaggedText(String taggedText) {
        this.taggedText = taggedText;
    }

    public String getCui() {
        return cui;
    }

    public void setCui(String cui) {
        this.cui = cui;
    }

    public String getCuiName() {
        return cuiName;
    }

    public void setCuiName(String cuiName) {
        this.cuiName = cuiName;
    }

    public Corpus getCorpus() {
        return corpus;
    }

    public void setCorpus(Corpus corpus) {
        this.corpus = corpus;
    }

    public boolean isCuiInNames() {
        return isCuiInNames;
    }

    public void setCuiInNames(boolean isCuiInNames) {
        this.isCuiInNames = isCuiInNames;
    }

 
}

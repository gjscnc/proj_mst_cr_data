/**
 * 
 */
package edu.mst.db.nlm.goldstd.corpora.annot;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.SortedSet;
import java.util.TreeSet;

import edu.mst.db.nlm.corpora.Corpus;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * @author gjs
 *
 */
public class GoldStdTagDao {

    public static final String[] autoPkField = new String[] { "id" };

    public static final String allFields = "pmid, tagId, category, taggedText, spansAsText, cui, cuiName, corpus, isCuiInNames";

    public static final String deleteAllSql = "truncate table conceptrecogn.goldstd_tags";

    public static final String persistSql = "insert into conceptrecogn.goldstd_tags ("
	    + allFields + ") values(?,?,?,?,?,?,?,?,?,?,?)";

    public static final String setCuiIsInNamesSql = "update conceptrecogn.goldstd_tags set isCuiInNames = ? "
	    + "where pmid = ? and tagId = ?";

    public static final String marshalAllSql = "select " + allFields
	    + " from conceptrecogn.goldstd_tags";
    
    public static final String marshalCuiNotNullSql = "select " + allFields
	    + " from conceptrecogn.goldstd_tags where cui is not null ";

    public static final String marshalByIdSql = "select " + allFields
	    + " from conceptrecogn.goldstd_tags where id=?";

    public static final String marshalForPmidSql = "select " + allFields
	    + " from conceptrecogn.goldstd_tags where pmid=?";

    public static final String marshalForPmidTagIdSql = "select " + allFields
	    + " from conceptrecogn.goldstd_tags where pmid=? and tagId=?";

    public static final String marshalPmidsSql = "select distinct pmid from conceptrecogn.goldstd_tags";
    
    public static final String marshalIdsSql = "select id from conceptrecogn.goldstd_tags";

    public static void deleteAll(JdbcConnectionPool connPool)
	    throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement deleteStmt = conn
		    .prepareStatement(deleteAllSql)) {
		deleteStmt.executeUpdate();
	    }
	}
    }
    
    public static int[] getIds(JdbcConnectionPool connPool) throws SQLException {
	SortedSet<Integer> idSet = new TreeSet<Integer>();
	try(Connection conn = connPool.getConnection()){
	    try(PreparedStatement getIdsStmt = conn.prepareStatement(marshalIdsSql)){
		try(ResultSet rs = getIdsStmt.executeQuery()){
		    while(rs.next()) {
			idSet.add(rs.getInt(1));
		    }
		}
	    }
	}
	int[] ids = new int[idSet.size()];
	int pos = 0;
	for(int id : idSet) {
	    ids[pos] = id;
	    pos++;
	}
	return ids;
    }

    public void persist(GoldStdTag tag, JdbcConnectionPool connPool)
	    throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement persistStmt = conn
		    .prepareStatement(persistSql)) {
		setPersistValues(tag, persistStmt);
		persistStmt.executeUpdate();
	    }
	}
    }

    public void persistBatch(Collection<GoldStdTag> tags,
	    JdbcConnectionPool connPool) throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement persistStmt = conn
		    .prepareStatement(persistSql)) {
		for (GoldStdTag tag : tags) {
		    setPersistValues(tag, persistStmt);
		    persistStmt.addBatch();
		}
		persistStmt.executeBatch();
	    }
	}
    }

    public void setIsCuiInNames(boolean isCuiInNames, int pmid, String tagId,
	    JdbcConnectionPool connPool) throws SQLException {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement updateStmt = conn
		    .prepareStatement(setCuiIsInNamesSql)) {
		updateStmt.setBoolean(1, isCuiInNames);
		updateStmt.setInt(2, pmid);
		updateStmt.setString(3, tagId);
		updateStmt.executeUpdate();
	    }
	}
    }

    public static int[] marshalPmids(JdbcConnectionPool connPool)
	    throws SQLException {
	SortedSet<Integer> pmidSet = new TreeSet<Integer>();
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement marshalStmt = conn
		    .prepareStatement(marshalPmidsSql)) {
		try (ResultSet rs = marshalStmt.executeQuery()) {
		    while (rs.next()) {
			pmidSet.add(rs.getInt(1));
		    }
		}
	    }
	}
	int nbrPmids = pmidSet.size();
	int[] pmids = new int[nbrPmids];
	int pos = 0;
	for (int pmid : pmidSet) {
	    pmids[pos] = pmid;
	    pos++;
	}
	return pmids;
    }

    private void setPersistValues(GoldStdTag tag, PreparedStatement persistStmt)
	    throws SQLException {
	/*
	 * 1-pmid, 2-tagId, 3-category, 4-taggedText, 5-spansAsText, 6-cui,
	 * 7-cuiName, 8-corpus, 9-isCuiInNames
	 */
	persistStmt.setInt(1, tag.getPmid());
	persistStmt.setString(2, tag.getTagId());
	persistStmt.setString(3, tag.getCategory());
	persistStmt.setString(4, tag.getTaggedText());
	persistStmt.setString(5, tag.getSpansAsText());
	persistStmt.setString(6, tag.getCui());
	persistStmt.setString(7, tag.getCuiName());
	persistStmt.setString(8, tag.getCorpus().toString());
	persistStmt.setBoolean(9, tag.isCuiInNames());
    }

    public SortedSet<GoldStdTag> marshalAll(JdbcConnectionPool connPool)
	    throws SQLException {
	SortedSet<GoldStdTag> tags = new TreeSet<GoldStdTag>();
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement marshalStmt = conn
		    .prepareStatement(marshalAllSql)) {
		try (ResultSet rs = marshalStmt.executeQuery()) {
		    while (rs.next()) {
			GoldStdTag tag = instantiateFromResultSet(rs);
			tags.add(tag);
		    }
		}
	    }
	}
	return tags;
    }
    
    public SortedSet<GoldStdTag> marshalCuiNotNull(JdbcConnectionPool connPool)
	    throws SQLException {
	SortedSet<GoldStdTag> tags = new TreeSet<GoldStdTag>();
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement marshalStmt = conn
		    .prepareStatement(marshalCuiNotNullSql)) {
		try (ResultSet rs = marshalStmt.executeQuery()) {
		    while (rs.next()) {
			GoldStdTag tag = instantiateFromResultSet(rs);
			tags.add(tag);
		    }
		}
	    }
	}
	return tags;
    }
    
    
    public SortedSet<GoldStdTag> marshal(int pmid, JdbcConnectionPool connPool)
	    throws SQLException {
	SortedSet<GoldStdTag> tags = new TreeSet<GoldStdTag>();
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement marshalStmt = conn
		    .prepareStatement(marshalForPmidSql)) {
		marshalStmt.setInt(1, pmid);
		try (ResultSet rs = marshalStmt.executeQuery()) {
		    while (rs.next()) {
			GoldStdTag tag = instantiateFromResultSet(rs);
			tags.add(tag);
		    }
		}
	    }
	}
	return tags;
    }

    public GoldStdTag marshal(int pmid, String tagId,
	    JdbcConnectionPool connPool) throws SQLException {
	GoldStdTag tag = null;
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement marshalStmt = conn
		    .prepareStatement(marshalForPmidTagIdSql)) {
		marshalStmt.setInt(1, pmid);
		marshalStmt.setString(2, tagId);
		try (ResultSet rs = marshalStmt.executeQuery()) {
		    if (rs.next()) {
			tag = instantiateFromResultSet(rs);
		    }
		}
	    }
	}
	return tag;
    }

    private GoldStdTag instantiateFromResultSet(ResultSet rs)
	    throws SQLException {
	/*
	 * 1-pmid, 2-tagId, 3-category, 4-taggedText, 5-spansAsText, 6-cui,
	 * 7-cuiName, 8-corpus, 9-isCuiInNames
	 */
	GoldStdTag tag = new GoldStdTag();
	tag.setPmid(rs.getInt(1));
	tag.setTagId(rs.getString(2));
	tag.setCategory(rs.getString(3));
	tag.setTaggedText(rs.getString(4));
	tag.setSpansAsText(rs.getString(5));
	tag.setCui(rs.getString(6));
	tag.setCuiName(rs.getString(7));
	tag.setCorpus(Corpus.valueOf(rs.getString(8)));
	tag.setCuiInNames(rs.getBoolean(9));
	return tag;
    }

}

package edu.mst.db.nlm.goldstd.corpora.annot;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import edu.mst.db.nlm.goldstd.corpora.parse.TextConceptTagType;

public class NLMAnnotTypeUtil {
    
    private static Set<TextConceptTagType> annotTypes;
    private static Map<Integer, TextConceptTagType> annotTypeById;
    private static Map<String, TextConceptTagType> annotTypeByText;
    
    private static void loadCollections(){
	if(annotTypes == null){
	    annotTypes = new HashSet<TextConceptTagType>();
	    annotTypes.add(TextConceptTagType.GOLD_STD);
	    annotTypes.add(TextConceptTagType.META_MAP);
	    annotTypes.add(TextConceptTagType.OIC);
	}
	if(annotTypeById == null){
	    annotTypeById = new HashMap<Integer, TextConceptTagType>();
	    for(TextConceptTagType type : annotTypes)
		annotTypeById.put(type.getAnnotTypeId(), type);
	}
	if(annotTypeByText == null){
	    annotTypeByText = new HashMap<String, TextConceptTagType>();
	    for(TextConceptTagType type : annotTypes)
		annotTypeByText.put(type.getAnnotText(), type);
	}
    }
    
    public static Set<TextConceptTagType> getAllAnnotTypes(){
	if(annotTypes == null)
	    loadCollections();
	return annotTypes;
    }
    
    public static TextConceptTagType getAnnotTypeForId(int typeId){
	if(annotTypeById == null)
	    loadCollections();
	return annotTypeById.get(typeId);
    }
    
    public static TextConceptTagType getAnnotTypeForText(String annotText){
	if(annotTypeByText == null)
	    loadCollections();
	return annotTypeByText.get(annotText);
    }

}

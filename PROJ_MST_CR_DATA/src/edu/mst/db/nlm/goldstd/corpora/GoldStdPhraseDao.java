/**
 * 
 */
package edu.mst.db.nlm.goldstd.corpora;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.SortedSet;

import edu.mst.db.util.JdbcConnectionPool;

/**
 * @author gjs
 *
 */
public class GoldStdPhraseDao {

    public static final String deleteAllPhrasesSql = "truncate conceptrecogn.goldstd_phrase ";

    public static final String deleteAllCuisSql = "truncate conceptrecogn.goldstd_cuimap";

    public static final String delAllPhrasesForPmidSql = "DELETE FROM conceptrecogn.goldstd_phrase WHERE pmid = ?";

    public static final String delAllCuisForPmidSql = "DELETE FROM conceptrecogn.goldstd_cuimap WHERE pmid = ?";

    public static final String persistPhraseSql = "INSERT INTO conceptrecogn.goldstd_phrase "
	    + "(pmid, sentNbr, sentPhraseNbr, beginSentWord, endSentWord, "
	    + "nbrWords, beginDocPos, endDocPos, text, posTag) "
	    + "VALUES (?,?,?,?,?,?,?,?,?,?)";

    public static final String persistCuisSql = "INSERT INTO conceptrecogn.goldstd_cuimap "
	    + "(pmid, sentNbr, phraseNbr, cui) VALUES(?,?,?,?)";

    public static final String getPhrasesSql = "SELECT sentPhraseNbr, beginSentWord, "
	    + "endSentWord, nbrWords, beginDocPos, endDocPos, text, posTag "
	    + "FROM conceptrecogn.goldstd_phrase WHERE pmid=? AND sentNbr=? order by sentPhraseNbr";

    public static final String getPhraseSql = "SELECT beginSentWord, endSentWord, nbrWords, "
	    + "beginDocPos, endDocPos, text, posTag FROM conceptrecogn.goldstd_phrase "
	    + "WHERE pmid=? AND sentNbr=? AND sentPhraseNbr=?";

    public static final String getCuisSql = "SELECT cui FROM conceptrecogn.goldstd_cuimap "
	    + "where pmid=? and sentNbr=? and phraseNbr=?";

    public static final String getPhraseAtDocPosSql = "SELECT sentNbr, sentPhraseNbr, "
	    + "beginSentWord, endSentWord, nbrWords, endDocPos, text, posTag FROM conceptrecogn.goldstd_phrase "
	    + "WHERE pmid=? AND beginDocPos<=? and (endDocPos>=? or endDocPos=-1)";

    public static final String getPhraseAtSentWordPosSql = "SELECT pmid, sentNbr, sentPhraseNbr, "
	    + "beginSentWord, endSentWord, nbrWords, beginDocPos, endDocPos, text, posTag FROM conceptrecogn.goldstd_phrase "
	    + "WHERE pmid=? AND sentNbr=? and beginSentWord <= ? and endSentWord >= ?";

    public static final String getLexWordIdSql = "SELECT baseWordId from conceptrecogn.lexwords WHERE token = ?";

    /**
     * Persist object to database.
     * <p>
     * This method persists the phrase plus all phrase words and MetaMap CUIs
     * associated with the phrase.
     * </p>
     * 
     * @param phrase
     * @param conn
     * @throws Exception
     */
    public void persistPhraseAndWords(GoldStdPhrase phrase, Connection conn)
	    throws Exception {

	try (PreparedStatement persistPhraseStmt = conn
		.prepareStatement(persistPhraseSql);
		PreparedStatement persistCuisStmt = conn
			.prepareStatement(persistCuisSql)) {
	    /*
	     * pmid, sentNbr, sentPhraseNbr, beginSentWord, endSentWord,
	     * nbrWords, beginDocPos, endDocPos, text, posTag
	     */
	    persistPhraseStmt.setInt(1, phrase.getPmid());
	    persistPhraseStmt.setInt(2, phrase.getSentNbr());
	    persistPhraseStmt.setInt(3, phrase.getSentPhraseNbr());
	    persistPhraseStmt.setInt(4,
		    phrase.getBeginSentWordNbr() >= 0
			    ? phrase.getBeginSentWordNbr()
			    : 0);
	    persistPhraseStmt.setInt(5,
		    phrase.getEndSentWordNbr() >= 0 ? phrase.getEndSentWordNbr()
			    : 0);
	    persistPhraseStmt.setInt(6,
		    phrase.getNbrWords() >= 0 ? phrase.getNbrWords() : 0);
	    persistPhraseStmt.setInt(7, phrase.getBeginDocPos());
	    persistPhraseStmt.setInt(8, phrase.getEndDocPos());
	    persistPhraseStmt.setString(9, phrase.getText());
	    persistPhraseStmt.setString(10, phrase.getPosTag());

	    try {
		if (persistPhraseStmt.executeUpdate() != 1)
		    throw new Exception("Persisting phrase not successful");
	    } catch (Exception ex) {
		String errMsg = String.format("Error persisting phrase - "
			+ "PMID = %1$d, sent nbr = %2$,d, sent phrase nbr = %3$,d, "
			+ "text = %4$s, part-of-speech = %5$s",
			phrase.getPmid(), phrase.getSentNbr(),
			phrase.getSentPhraseNbr(), phrase.getText(),
			phrase.getPosTag());
		throw new Exception(errMsg, ex);
	    }

	    // persist words
	    GoldStdPhraseWordDao phraseWordDao = new GoldStdPhraseWordDao();
	    for (GoldStdPhraseWord word : phrase.getWords()) {
		word.setSentPhraseNbr(phrase.getSentPhraseNbr());
		phraseWordDao.persist(word, conn);
	    }

	    // persist CUIs
	    for (String cui : phrase.getCuis()) {
		persistCuisStmt.setInt(1, phrase.getPmid());
		persistCuisStmt.setInt(2, phrase.getSentNbr());
		persistCuisStmt.setInt(3, phrase.getSentPhraseNbr());
		persistCuisStmt.setString(4, cui);
		if (persistCuisStmt.executeUpdate() != 1) {
		    throw new Exception("Persisting CUIs not successful");
		}

	    }
	}
    }

    /**
     * Delete all phrases in database.
     * 
     * @param connPool
     * @return
     * @throws Exception
     */
    public static void delAllPhrases(JdbcConnectionPool connPool)
	    throws SQLException, Exception {
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement delAllPhrasesStmt = conn
		    .prepareStatement(deleteAllPhrasesSql);
		    PreparedStatement delAllCuisStmt = conn
			    .prepareStatement(delAllCuisForPmidSql)) {
		delAllPhrasesStmt.executeUpdate();
		delAllCuisStmt.executeUpdate();
	    }
	}
    }

    /**
     * 
     * @param pmidsToDelete
     *                          Set collection of PMIDs for sentences to delete
     * @param connPool
     * @return
     * @throws SQLException
     * @throws Exception
     */
    public static int delAllPhrases(SortedSet<Integer> pmidsToDelete,
	    JdbcConnectionPool connPool) throws SQLException, Exception {
	int nbrDel = 0;
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement delAllPhrasesForCorpusStmt = conn
		    .prepareStatement(delAllPhrasesForPmidSql);
		    PreparedStatement delAllCuisForPmidStmt = conn
			    .prepareStatement(delAllCuisForPmidSql)) {
		Iterator<Integer> pmidsIter = pmidsToDelete.iterator();
		while (pmidsIter.hasNext()) {
		    int pmid = pmidsIter.next();
		    delAllPhrasesForCorpusStmt.setInt(1, pmid);
		    delAllCuisForPmidStmt.setInt(1, pmid);
		    nbrDel += delAllPhrasesForCorpusStmt.executeUpdate();
		    delAllCuisForPmidStmt.executeUpdate();
		}
	    }
	}
	return nbrDel;
    }

    /**
     * Retrieve phrase for document position in abstract.
     * 
     * @param pmid
     * @param docPosition
     *                        Offset in document for beginning of phrase
     * @param conn
     * @return
     * @throws Exception
     */
    public GoldStdPhrase retrievePhraseAtDocPos(int pmid, int docPosition,
	    Connection conn) throws Exception {

	GoldStdPhrase phrase = null;

	try (PreparedStatement getPhraseDbAtPosStmt = conn
		.prepareStatement(getPhraseAtDocPosSql);
		PreparedStatement getCuisStmt = conn
			.prepareStatement(getCuisSql)) {
	    /*
	     * sentNbr, sentPhraseNbr, beginSentWord, endSentWord, nbrWords,
	     * endDocPos, text, posTag, metaMapCui
	     */
	    getPhraseDbAtPosStmt.setInt(1, pmid);
	    getPhraseDbAtPosStmt.setInt(2, docPosition);
	    getPhraseDbAtPosStmt.setInt(3, docPosition);
	    try (ResultSet rs = getPhraseDbAtPosStmt.executeQuery()) {
		if (rs.next()) {
		    phrase = new GoldStdPhrase();
		    phrase.setPmid(pmid);
		    phrase.setSentNbr(rs.getInt(1));
		    phrase.setSentPhraseNbr(rs.getInt(2));
		    phrase.setBeginSentWordNbr(rs.getInt(3));
		    phrase.setEndSentWordNbr(rs.getInt(4));
		    phrase.setNbrWords(rs.getInt(5));
		    phrase.setBeginDocPos(docPosition);
		    phrase.setEndDocPos(rs.getInt(6));
		    phrase.setText(rs.getString(7));
		    phrase.setPosTag(rs.getString(8));
		}
	    }
	    if (phrase == null) {
		String errMsg = String.format(
			"Phrase not found for PMID %1$d "
				+ "at document position %2$,d ",
			pmid, docPosition);
		throw new Exception(errMsg);
	    }
	    getCuisStmt.setInt(1, pmid);
	    getCuisStmt.setInt(2, phrase.getSentNbr());
	    getCuisStmt.setInt(3, phrase.getSentPhraseNbr());
	    try (ResultSet rs = getCuisStmt.executeQuery()) {
		while (rs.next()) {
		    phrase.getCuis().add(rs.getString(1));
		}
	    }
	}

	GoldStdPhraseWordDao phraseWordDao = new GoldStdPhraseWordDao();
	phraseWordDao.marshalWordsForPhrase(phrase, conn);

	return phrase;
    }

    /**
     * Retrieve phrase for document position in abstract.
     * 
     * @param pmid
     * @param docPosition
     *                        Offset in document for beginning of phrase
     * @param connPool
     * @return
     * @throws Exception
     */
    public GoldStdPhrase retrievePhraseAtDocPos(int pmid, int docPosition,
	    JdbcConnectionPool connPool) throws Exception {

	GoldStdPhrase phrase = null;

	try (Connection conn = connPool.getConnection()) {
	    phrase = retrievePhraseAtDocPos(pmid, docPosition, conn);
	}

	return phrase;
    }

    public GoldStdPhrase retrievePhraseAtSentWordPos(int pmid, int sentNbr,
	    int sentWordNbr, JdbcConnectionPool connPool) throws SQLException {

	GoldStdPhrase phrase = null;

	/*
	 * 1-pmid, 2-sentNbr, 3-sentPhraseNbr, 4-beginSentWord, 5-endSentWord,
	 * 6-nbrWords, 7-beginDocPos, 8-endDocPos, 9-text, 10-posTag
	 */
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement marshalStmt = conn
		    .prepareStatement(getPhraseAtSentWordPosSql)) {
		marshalStmt.setInt(1, pmid);
		marshalStmt.setInt(2, sentNbr);
		marshalStmt.setInt(3, sentWordNbr);
		marshalStmt.setInt(4, sentWordNbr);
		try (ResultSet rs = marshalStmt.executeQuery()) {
		    if (rs.next()) {
			phrase = new GoldStdPhrase();
			phrase.setPmid(rs.getInt(1));
			phrase.setSentNbr(rs.getInt(2));
			phrase.setSentPhraseNbr(rs.getInt(3));
			phrase.setBeginSentWordNbr(rs.getInt(4));
			phrase.setEndSentWordNbr(rs.getInt(5));
			phrase.setNbrWords(rs.getInt(6));
			phrase.setBeginDocPos(rs.getInt(7));
			phrase.setEndDocPos(rs.getInt(8));
			phrase.setText(rs.getString(9));
			phrase.setPosTag(rs.getString(10));
		    }
		}

	    }
	}

	return phrase;
    }

    /**
     * Retrieve phrase based upon PMID, sentence number, and sentence phrase
     * number. Also retrieves all words associated with the phrase
     * 
     * @param pmid
     * @param sentNbr
     * @param sentPhraseNbr
     * @param conn
     * @return
     * @throws Exception
     */
    public GoldStdPhrase marshal(int pmid, int sentNbr, int sentPhraseNbr,
	    Connection conn) throws Exception {

	GoldStdPhrase phrase = null;

	try (PreparedStatement getPhraseStmt = conn
		.prepareStatement(getPhraseSql);
		PreparedStatement getCuisStmt = conn
			.prepareStatement(getCuisSql)) {
	    getPhraseStmt.setInt(1, pmid);
	    getPhraseStmt.setInt(2, sentNbr);
	    getPhraseStmt.setInt(3, sentPhraseNbr);
	    try (ResultSet rs = getPhraseStmt.executeQuery()) {
		if (rs.next()) {
		    /*
		     * beginSentWord, endSentWord, nbrWords, beginDocPos,
		     * endDocPos, text, posTag, metaMapCui
		     */
		    phrase = new GoldStdPhrase();
		    phrase.setPmid(pmid);
		    phrase.setSentNbr(sentNbr);
		    phrase.setSentPhraseNbr(sentPhraseNbr);
		    phrase.setBeginSentWordNbr(rs.getInt(1));
		    phrase.setEndSentWordNbr(rs.getInt(2));
		    phrase.setNbrWords(rs.getInt(3));
		    phrase.setBeginDocPos(rs.getInt(4));
		    phrase.setEndDocPos(rs.getInt(5));
		    phrase.setText(rs.getString(6));
		    phrase.setPosTag(rs.getString(7));

		    getCuisStmt.setInt(1, pmid);
		    getCuisStmt.setInt(2, phrase.getSentNbr());
		    getCuisStmt.setInt(3, phrase.getSentPhraseNbr());
		    try (ResultSet cuiRs = getCuisStmt.executeQuery()) {
			while (cuiRs.next()) {
			    phrase.getCuis().add(cuiRs.getString(1));
			}
		    }

		}
	    }
	}

	GoldStdPhraseWordDao phraseWordDao = new GoldStdPhraseWordDao();
	phraseWordDao.marshalWordsForPhrase(phrase, conn);

	return phrase;
    }

    /**
     * Retrieve phrase based upon PMID, sentence number, and sentence phrase
     * number. Also retrieves all words associated with the phrase
     * 
     * @param pmid
     * @param sentNbr
     * @param sentPhraseNbr
     * @param connPool
     * @return
     * @throws Exception
     */
    public GoldStdPhrase marshal(int pmid, int sentNbr, int sentPhraseNbr,
	    JdbcConnectionPool connPool) throws Exception {

	GoldStdPhrase phrase = null;

	try (Connection conn = connPool.getConnection()) {
	    phrase = marshal(pmid, sentNbr, sentPhraseNbr, conn);
	}

	return phrase;
    }

    /**
     * Method for marshaling phrase from the database.
     * 
     * @param pmid
     * @param sentNbr
     * @param conn
     * @return
     * @throws Exception
     */
    public List<GoldStdPhrase> marshalAllPhrases(int pmid, int sentNbr,
	    Connection conn) throws SQLException {

	List<GoldStdPhrase> phrases = new ArrayList<GoldStdPhrase>();

	try (PreparedStatement getPhrasesStmt = conn
		.prepareStatement(getPhrasesSql);
		PreparedStatement getCuisStmt = conn
			.prepareStatement(getCuisSql)) {

	    getPhrasesStmt.setInt(1, pmid);
	    getPhrasesStmt.setInt(2, sentNbr);

	    try (ResultSet phrasesRs = getPhrasesStmt.executeQuery()) {

		GoldStdPhraseWordDao phraseWordDao = new GoldStdPhraseWordDao();
		while (phrasesRs.next()) {
		    /*
		     * sentPhraseNbr, beginSentWord, endSentWord, nbrWords,
		     * beginDocPos, endDocPos, text, posTag, metaMapCui
		     */
		    GoldStdPhrase phrase = new GoldStdPhrase();
		    phrase.setPmid(pmid);
		    phrase.setSentNbr(sentNbr);
		    phrase.setSentPhraseNbr(phrasesRs.getInt(1));
		    phrase.setBeginSentWordNbr(phrasesRs.getInt(2));
		    phrase.setEndSentWordNbr(phrasesRs.getInt(3));
		    phrase.setNbrWords(phrasesRs.getInt(4));
		    phrase.setBeginDocPos(phrasesRs.getInt(5));
		    phrase.setEndDocPos(phrasesRs.getInt(6));
		    phrase.setText(phrasesRs.getString(7));
		    phrase.setPosTag(phrasesRs.getString(8));

		    getCuisStmt.setInt(1, pmid);
		    getCuisStmt.setInt(2, phrase.getSentNbr());
		    getCuisStmt.setInt(3, phrase.getSentPhraseNbr());
		    try (ResultSet cuiRs = getCuisStmt.executeQuery()) {
			while (cuiRs.next()) {
			    phrase.getCuis().add(cuiRs.getString(1));
			}
		    }

		    // instantiate each word
		    phraseWordDao.marshalWordsForPhrase(phrase, conn);

		    phrases.add(phrase);
		}
		phrases.sort(
			GoldStdPhrase.Comparators.byPmid_SentNbr_DocBeginPosition);
	    }
	}

	return phrases;
    }

    /**
     * Method for marshaling phrases from the database for sentence.
     * 
     * @param pmid
     * @param sentNbr
     * @param connPool
     * @return
     * @throws SQLException
     */
    public List<GoldStdPhrase> marshalAllPhrases(int pmid, int sentNbr,
	    JdbcConnectionPool connPool) throws SQLException {

	List<GoldStdPhrase> phrases = null;

	try (Connection conn = connPool.getConnection()) {
	    phrases = marshalAllPhrases(pmid, sentNbr, conn);
	}

	return phrases;
    }

    public static int deleteAllPhrases(JdbcConnectionPool connPool)
	    throws Exception {
	int nbrPhrasesDeleted = 0;
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement deleteAllPhrasesStmt = conn
		    .prepareStatement(deleteAllPhrasesSql)) {
		nbrPhrasesDeleted = deleteAllPhrasesStmt.executeUpdate();
	    }
	}
	return nbrPhrasesDeleted;
    }

}

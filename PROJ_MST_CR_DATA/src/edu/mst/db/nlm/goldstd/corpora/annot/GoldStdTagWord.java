/**
 * 
 */
package edu.mst.db.nlm.goldstd.corpora.annot;

import java.util.Comparator;

/**
 * Word in a NLM abstract that matches a word identified in a gold standard NLM
 * annotation tag ('Txx').
 * 
 * @author gjs
 *
 */
public class GoldStdTagWord implements Comparable<GoldStdTagWord> {

    private int pmid = 0;
    private String tagId = null;
    private int sentNbr = 0;
    private int sentWordNbr = 0;
    private int lexWordId = 0;
    private String token = null;
    private int beginPos = 0;
    private int endPos = 0;
    private String cui = null;
    private boolean isStopWord = false;
    private boolean isPunctOrDelimChar = false;

    public static class Comparables {
	public static Comparator<GoldStdTagWord> byPmid = (GoldStdTagWord word1,
		GoldStdTagWord word2) -> Integer.compare(word1.pmid,
			word2.pmid);
	public static Comparator<GoldStdTagWord> byTagId = (
		GoldStdTagWord word1,
		GoldStdTagWord word2) -> word1.tagId.compareTo(word2.tagId);
	public static Comparator<GoldStdTagWord> bySentNbr = (
		GoldStdTagWord word1, GoldStdTagWord word2) -> Integer
			.compare(word1.sentNbr, word2.sentNbr);
	public static Comparator<GoldStdTagWord> byWordNbr = (
		GoldStdTagWord word1, GoldStdTagWord word2) -> Integer
			.compare(word1.sentWordNbr, word2.sentWordNbr);
	public static Comparator<GoldStdTagWord> byPmid_tagId_SentNbr_WordNbr = (
		GoldStdTagWord word1, GoldStdTagWord word2) -> byPmid
			.thenComparing(byTagId).thenComparing(bySentNbr)
			.thenComparing(byWordNbr).compare(word1, word2);
    }

    @Override
    public int compareTo(GoldStdTagWord word) {
	return Comparables.byPmid_tagId_SentNbr_WordNbr.compare(this, word);
    }

    @Override
    public boolean equals(Object obj) {
	if (obj instanceof GoldStdTagWord) {
	    GoldStdTagWord word = (GoldStdTagWord) obj;
	    return pmid == word.pmid && tagId.equals(word.tagId)
		    && sentNbr == word.sentNbr && sentWordNbr == word.sentWordNbr;
	}
	return false;
    }

    @Override
    public int hashCode() {
	return Integer.hashCode(pmid) + 7 * tagId.hashCode()
		+ 31 * Integer.hashCode(sentNbr)
		+ 47 * Integer.hashCode(sentWordNbr);
    }

    public int getPmid() {
        return pmid;
    }

    public void setPmid(int pmid) {
        this.pmid = pmid;
    }

    public String getTagId() {
        return tagId;
    }

    public void setTagId(String tagId) {
        this.tagId = tagId;
    }

    public int getSentNbr() {
        return sentNbr;
    }

    public void setSentNbr(int sentNbr) {
        this.sentNbr = sentNbr;
    }

    public int getSentWordNbr() {
        return sentWordNbr;
    }

    public void setSentWordNbr(int sentWordNbr) {
        this.sentWordNbr = sentWordNbr;
    }

    public int getLexWordId() {
        return lexWordId;
    }

    public void setLexWordId(int lexWordId) {
        this.lexWordId = lexWordId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getBeginPos() {
        return beginPos;
    }

    public void setBeginPos(int beginPos) {
        this.beginPos = beginPos;
    }

    public int getEndPos() {
        return endPos;
    }

    public void setEndPos(int endPos) {
        this.endPos = endPos;
    }

    public String getCui() {
        return cui;
    }

    public void setCui(String cui) {
        this.cui = cui;
    }

    public boolean isStopWord() {
        return isStopWord;
    }

    public void setStopWord(boolean isStopWord) {
        this.isStopWord = isStopWord;
    }

    public boolean isPunctOrDelimChar() {
        return isPunctOrDelimChar;
    }

    public void setPunctOrDelimChar(boolean isPunct) {
        this.isPunctOrDelimChar = isPunct;
    }

}

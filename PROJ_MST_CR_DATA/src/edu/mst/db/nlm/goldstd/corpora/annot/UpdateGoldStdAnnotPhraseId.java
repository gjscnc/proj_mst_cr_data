/**
 * 
 */
package edu.mst.db.nlm.goldstd.corpora.annot;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.mst.db.nlm.goldstd.corpora.GoldStdAnnot;
import edu.mst.db.util.ConnDbName;
import edu.mst.db.util.JdbcConnectionPool;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Contains method to set the phrase ID for each NLM gold standard annotation.
 * <p>
 * This class was created after discovering that values were not set properly
 * when executing the concurrent process for persisting gold standard annotations, i.e., 
 * {@link PersistNlmGoldStdAnnotCallable#call()}
 * .
 * @deprecated This class no longer used
 * @author George
 *
 */
public class UpdateGoldStdAnnotPhraseId {

    private JdbcConnectionPool connPool;

    private List<GoldStdAnnot> annotations = new ArrayList<GoldStdAnnot>();
    private Map<Integer, Integer> phraseIdByGoldStdAnnotId = new HashMap<Integer, Integer>();

    public UpdateGoldStdAnnotPhraseId(JdbcConnectionPool connPool) {
	this.connPool = connPool;
    }

    public void updateIds() throws Exception {

	int nbrGoldStdAnnot = 0;
	int nbrPhraseIds = 0;
	int nbrGoldStdAnnotUpdated = 0;  
	int rptInterval = 100;

	try (Connection conn = connPool.getConnection()) {

	    // get gold std annotation IDs
	    String getGoldStdIdsSql = "SELECT id, pmid, sentenceNbr, firstWordNbr "
		    + "FROM conceptrecogn.goldstd_annot";
	    try (PreparedStatement getGoldStdIdsStmt = conn
		    .prepareStatement(getGoldStdIdsSql)) {
		try (ResultSet rs = getGoldStdIdsStmt.executeQuery()) {
		    while (rs.next()) {
			int goldStdDbId = rs.getInt(1);
			int pmid = rs.getInt(2);
			int sentNbr = rs.getInt(3);
			int firstWordNbr = rs.getInt(4);
			GoldStdAnnot annot = new GoldStdAnnot();
			annot.setId(goldStdDbId);
			annot.setPmid(pmid);
			annot.setSentenceNbr(sentNbr);
			annot.setSentFirstWordNbr(firstWordNbr);
			annotations.add(annot);
			nbrGoldStdAnnot++;
			if (nbrGoldStdAnnot % rptInterval == 0) {
			    System.out
				    .printf("Retrieved %1$d NLM gold standard annotations.%n",
					    nbrGoldStdAnnot);
			}
		    }
		}
	    }
	    annotations.sort(GoldStdAnnot.Comparators.byPosition);
	    System.out.printf(
		    "Retrieved total of %1$d NLM gold standard annotations.%n",
		    nbrGoldStdAnnot);

	    String getPhraseIdSql = "SELECT id FROM sps.testphrase "
		    + "WHERE pmid = ? AND sentenceNbr = ? AND "
		    + "beginSentWord <= ? AND endSentWord >= ?";
	    try (PreparedStatement getPhraseIdStmt = conn
		    .prepareStatement(getPhraseIdSql)) {
		for (GoldStdAnnot annot : annotations) {
		    getPhraseIdStmt.setInt(1, annot.getPmid());
		    getPhraseIdStmt.setInt(2, annot.getSentenceNbr());
		    getPhraseIdStmt.setInt(3, annot.getSentFirstWordNbr());
		    getPhraseIdStmt.setInt(4, annot.getSentFirstWordNbr());
		    try (ResultSet rs = getPhraseIdStmt.executeQuery()) {
			if (rs.next()) {
			    int phraseId = rs.getInt(1);
			    int goldStdDbId = annot.getId();
			    phraseIdByGoldStdAnnotId.put(goldStdDbId, phraseId);
			    nbrPhraseIds++;
			    if (nbrPhraseIds % rptInterval == 0) {
				System.out.printf(
					"Retrieved %1$d phrase IDs.%n",
					nbrPhraseIds);
			    }
			} else {
			    String errMsg = String
				    .format("Phrase ID not found for "
					    + "PMID = %1$d, sentence nbr = %2$d, first word nbr = %3$d",
					    annot.getPmid(),
					    annot.getSentenceNbr(),
					    annot.getSentFirstWordNbr());
			    throw new Exception(errMsg);
			}
		    }
		}
	    }

	    System.out.printf("Retrieved total of %1$d phrase IDs.%n",
		    nbrPhraseIds);

	    // update NLM gold std annotations with phrase id
	    // sort nlm gold std annot ids to facilitate debugging
	    List<Integer> annotIds = new ArrayList<Integer>();
	    annotIds.addAll(phraseIdByGoldStdAnnotId.keySet());
	    Collections.sort(annotIds);

	    String updatePhraseIdSql = "UPDATE conceptrecogn.goldstd_annot SET phraseDbId = ? WHERE id = ?";
	    try (PreparedStatement updatePhraseIdStmt = conn
		    .prepareStatement(updatePhraseIdSql)) {
		for (Integer annotId : annotIds) {
		    int phraseDbId = phraseIdByGoldStdAnnotId.get(annotId);
		    updatePhraseIdStmt.setInt(1, phraseDbId);
		    updatePhraseIdStmt.setInt(2, annotId);
		    if (updatePhraseIdStmt.executeUpdate() == 0) {
			String errMsg = String.format(
				"Unable to update NLM gold std "
					+ "annotation with ID = %1$d "
					+ "with phrase DB ID = %2$d", annotId,
				phraseDbId);
			throw new Exception(errMsg);
		    }
		    nbrGoldStdAnnotUpdated++;
		    if (nbrGoldStdAnnotUpdated % rptInterval == 0) {
			System.out.printf(
				"Updated %1$d NLM gold std annotations.%n",
				nbrGoldStdAnnotUpdated);
		    }
		}
	    }
	}
	System.out.printf("Updated total of %1$d NLM gold std annotations.%n",
		nbrGoldStdAnnotUpdated);

    }

    /**
     * @param args
     */
    public static void main(String[] args) {
	try {
	    JdbcConnectionPool connPool = new JdbcConnectionPool(ConnDbName.CONCEPT_RECOGN);
	    UpdateGoldStdAnnotPhraseId updater = new UpdateGoldStdAnnotPhraseId(
		    connPool);
	    updater.updateIds();
	} catch (Exception ex) {
	    ex.printStackTrace();
	}
    }

}

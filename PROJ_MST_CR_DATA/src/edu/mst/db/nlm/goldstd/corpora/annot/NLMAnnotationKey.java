package edu.mst.db.nlm.goldstd.corpora.annot;

import java.util.Comparator;
import java.util.UUID;

public class NLMAnnotationKey implements Comparable<NLMAnnotationKey> {

    private UUID uuid;
    private int pmid;
    private int annotId;
    private int annotType;
    private int annotSubType;

    public NLMAnnotationKey(int pmid, int annotId, int annotType, int annotSubType) {
	this.pmid = pmid;
	this.annotId = annotId;
	this.annotType = annotType;
	this.annotSubType = annotSubType;
    }

    @Override
    public int compareTo(NLMAnnotationKey k2) {
	return Comparators.UniqueId.compare(this, k2);
    }

    public static class Comparators {
	public static final Comparator<NLMAnnotationKey> UniqueId = (
		NLMAnnotationKey k1, NLMAnnotationKey k2) -> k1.uuid
		.compareTo(k2.uuid);
	public static final Comparator<NLMAnnotationKey> TagId = (
		NLMAnnotationKey k1, NLMAnnotationKey k2) -> {
	    int i = Integer.valueOf(k1.pmid)
		    .compareTo(Integer.valueOf(k2.pmid));
	    if (i != 0)
		return i;
	    i = Integer.valueOf(k1.annotId).compareTo(
		    Integer.valueOf(k2.annotId));
	    if (i != 0)
		return i;
	    i = Integer.valueOf(k1.annotType).compareTo(
		    Integer.valueOf(k2.annotType));
	    if (i != 0)
		return i;
	    return Integer.valueOf(k1.annotSubType).compareTo(
		    Integer.valueOf(k2.annotSubType));
	};
    }

    @Override
    public boolean equals(Object obj) {
	if (obj instanceof NLMAnnotationKey) {
	    NLMAnnotationKey annotKey = (NLMAnnotationKey) obj;
	    return this.uuid.equals(annotKey.uuid);
	}
	return false;
    }
    
    public boolean tagIdsEqual(Object obj){
	if (obj instanceof NLMAnnotationKey) {
	    NLMAnnotationKey annotKey = (NLMAnnotationKey) obj;
	    return pmid == annotKey.pmid && annotId == annotKey.annotId
		    && annotType == annotKey.annotType
		    && annotSubType == annotKey.annotSubType;
	}
	return false;
    }

    @Override
    public int hashCode() {
	return uuid.hashCode();
    }

    public UUID getUuid() {
        return uuid;
    }

    public int getPmid() {
	return pmid;
    }

    public int getAnnotId() {
	return annotId;
    }

    public int getAnnotType() {
	return annotType;
    }

    public int getAnnotSubType() {
	return annotSubType;
    }

}

package edu.mst.db.nlm.goldstd.corpora.annot;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.javatuples.Pair;

import edu.mst.db.nlm.goldstd.corpora.GoldStdAbstract;
import edu.mst.db.nlm.goldstd.corpora.GoldStdAnnotSpan;
import edu.mst.db.nlm.goldstd.corpora.parse.Normalization;
import edu.mst.db.nlm.goldstd.corpora.parse.TextConceptTagType;
import edu.mst.db.util.JdbcConnectionPool;

public class ParseNlmDataUtil {

    private static final char tabChar = '\t';
    private static final char spaceChar = ' ';
    private static final char discontinuousTagChar = ';';

    private static final String getAbstractSql = "SELECT title, abstract FROM conceptrecogn.goldstd_abstracts WHERE pmid=?";
    private static final String getSentNbrSql = "SELECT sentence_nbr FROM conceptrecogn.goldstd_sentence "
	    + "WHERE pmid=? AND begin_doc_pos <= ? AND (end_doc_pos >= ? OR end_doc_pos = -1)";
    private static final String getWordNbrsSql = "SELECT sentence_word_nbr, begin_doc_pos, end_doc_pos "
	    + "FROM conceptrecogn.goldstd_sentword WHERE pmid=? and sentence_nbr = ?";

    private static int docLen = 0;

    public static NlmAnnot parseAnnot(int pmid, String line,
	    JdbcConnectionPool connPool) throws Exception {

	// get abstract
	GoldStdAbstract doc = null;

	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement getAbstractQry = conn
		    .prepareStatement(getAbstractSql)) {
		getAbstractQry.setInt(1, pmid);
		try (ResultSet rs = getAbstractQry.executeQuery()) {
		    if (rs.next()) {
			String title = rs.getString(1).trim();
			String body = rs.getString(2);
			doc = new GoldStdAbstract(pmid, title, body);
			docLen = doc.getText().length();
		    }
		}
	    }
	}

	NlmAnnot annot = new NlmAnnot();
	annot.setPmid(pmid);

	// annotation ID
	int beginParsePos = line.indexOf(tabChar);
	annot.setDocAnnotId(Integer.valueOf(line.substring(1, beginParsePos)));

	// type
	beginParsePos++;
	int endParsePos = line.indexOf(spaceChar, beginParsePos);
	String type = line.substring(beginParsePos, endParsePos);
	annot.setAnnotSubType(NLMAnnotSubTypeUtil.getSubTypeForText(type));
	switch (annot.getAnnotSubType()) {
	case DISORDER:
	    annot.setAnnotType(TextConceptTagType.GOLD_STD);
	    break;
	case GENERAL_DISORDER:
	    annot.setAnnotType(TextConceptTagType.GOLD_STD);
	    break;
	case OVERLY_GENERAL_DISORDER:
	    annot.setAnnotType(TextConceptTagType.GOLD_STD);
	    break;
	case METAMAP_FULL:
	    annot.setAnnotType(TextConceptTagType.META_MAP);
	    break;
	case METAMAP_LITE:
	    annot.setAnnotType(TextConceptTagType.META_MAP);
	    break;
	default:
	    return null;
	}

	// span positions
	beginParsePos = endParsePos + 1;

	boolean hasDiscontinousTagSpan = line.indexOf(discontinuousTagChar,
		beginParsePos) != -1;
	boolean lastTagSpan = false;
	int beginDocPos = -1;
	while (!lastTagSpan) {

	    // get first tag position
	    endParsePos = line.indexOf(spaceChar, beginParsePos);
	    int firstTagPos = Integer
		    .valueOf(line.substring(beginParsePos, endParsePos));

	    // get second tag position
	    beginParsePos = endParsePos + 1;
	    if (hasDiscontinousTagSpan) {
		endParsePos = line.indexOf(discontinuousTagChar, beginParsePos);
		if (endParsePos == -1) {
		    endParsePos = line.indexOf(tabChar, beginParsePos);
		    lastTagSpan = true;
		}
	    } else {
		endParsePos = line.indexOf(tabChar, beginParsePos);
		lastTagSpan = true;
	    }
	    int secondTagPos = Integer
		    .valueOf(line.substring(beginParsePos, endParsePos));

	    // span text
	    String text = null;
	    if (secondTagPos == -1 || secondTagPos == docLen - 1)
		text = doc.getText().substring(firstTagPos);
	    else
		text = doc.getText().substring(firstTagPos, secondTagPos);
	    if (beginDocPos == -1) {
		beginDocPos = firstTagPos;
		annot.setBeginDocPos(beginDocPos);
	    }

	    // add tag span to list
	    Pair<Integer, Integer> spanSentWordNbrs = getWordNbrSpan(
		    annot.getPmid(), firstTagPos, secondTagPos, connPool);
	    GoldStdAnnotSpan tagSpan = new GoldStdAnnotSpan(annot.getPmid(),
		    annot.getDocAnnotId(), annot.getAnnotType(),
		    annot.getAnnotSubType(), firstTagPos, secondTagPos,
		    spanSentWordNbrs.getValue0(), spanSentWordNbrs.getValue1(),
		    text);
	    annot.getAnnotSpans().add(tagSpan);

	    // reset parse positions
	    beginParsePos = endParsePos + 1;
	    // if(hasDiscontinousTagSpan && !lastTagSpan){
	    // beginParsePos = endParsePos + 2;
	    // } else {
	    // beginParsePos = endParsePos + 1;
	    // }
	}

	// tagged concept line, not necessarily the concept name
	annot.setText(line.substring(beginParsePos));

	return annot;

    }

    /**
     * Method to map span begin and ending document positions to sentence
     * positions, i.e., begin and end sentence word numbers.
     * 
     * @param pmid
     * @param sentNbr
     * @param spanBeginDocPos
     * @param spanEndDocPos
     * @return Pair<Integer, Integer> First integer is beginSentWordNbr, second
     *         is endSentWordNbr
     * @throws Exception
     */
    public static Pair<Integer, Integer> getWordNbrSpan(int pmid,
	    int spanBeginDocPos, int spanEndDocPos, JdbcConnectionPool connPool)
	    throws Exception {

	int sentNbr = -1;
	int beginSentWordNbr = -1;
	int endSentWordNbr = -1;

	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement getSentNbrStmt = conn
		    .prepareStatement(getSentNbrSql)) {
		getSentNbrStmt.setInt(1, pmid);
		getSentNbrStmt.setInt(2, spanBeginDocPos);
		getSentNbrStmt.setInt(3, spanEndDocPos);
		try (ResultSet rs = getSentNbrStmt.executeQuery()) {
		    if (rs.next()) {
			sentNbr = rs.getInt(1);
		    } else {
			String errMsg = String.format(
				"Sentence number not found for "
					+ "PMID = %1$d between document positions "
					+ "begin = %2$d and end = %3$d.",
				pmid, spanBeginDocPos, spanEndDocPos);
			throw new Exception(errMsg);
		    }
		}
	    }
	    try (PreparedStatement getWordNbrsStmt = conn
		    .prepareStatement(getWordNbrsSql)) {
		getWordNbrsStmt.setInt(1, pmid);
		getWordNbrsStmt.setInt(2, sentNbr);
		boolean isSpanBeginWordFound = false;
		boolean isSpanEndWordFound = false;
		try (ResultSet rs = getWordNbrsStmt.executeQuery()) {
		    while (rs.next()) {
			int sentWordNbr = rs.getInt(1);
			int wordBeginDocPos = rs.getInt(2);
			int wordEndDocPos = rs.getInt(3);
			// TODO: validate this logic - unit test
			if (!isSpanBeginWordFound
				&& spanBeginDocPos <= wordBeginDocPos
				&& (spanEndDocPos >= wordEndDocPos
					|| wordEndDocPos == -1)) {
			    beginSentWordNbr = sentWordNbr;
			    isSpanBeginWordFound = true;
			}
			if (isSpanBeginWordFound && !isSpanEndWordFound
				&& (spanEndDocPos <= wordEndDocPos
					|| wordEndDocPos == -1)) {
			    endSentWordNbr = sentWordNbr;
			    isSpanEndWordFound = true;
			}
			if (isSpanBeginWordFound && isSpanEndWordFound)
			    break;
		    }
		}
	    }
	}

	return new Pair<Integer, Integer>(beginSentWordNbr, endSentWordNbr);
    }

    public static Normalization parseNorm(int pmid, String line)
	    throws Exception {

	Normalization norm = new Normalization();

	norm.pmid = pmid;

	// annotation ID
	int beginParsePos = line.indexOf(tabChar);
	norm.normID = line.substring(0, beginParsePos);

	// find end of "Reference" string in line
	beginParsePos++;
	int endParsePos = line.indexOf(spaceChar, beginParsePos);

	// get reference to text annotation
	beginParsePos = endParsePos + 1;
	endParsePos = line.indexOf(spaceChar, beginParsePos);
	norm.annotationID = Integer
		.valueOf(line.substring(beginParsePos + 1, endParsePos));

	// get cui
	beginParsePos = line.indexOf(':', endParsePos);
	if (beginParsePos == -1)
	    throw new Exception(
		    "Concept CUI not found since ':' character not found");

	beginParsePos++;
	endParsePos = line.indexOf(tabChar, beginParsePos);
	norm.cui = line.substring(beginParsePos, endParsePos);

	beginParsePos = endParsePos + 1;
	norm.conceptName = line.substring(beginParsePos);

	return norm;

    }

}

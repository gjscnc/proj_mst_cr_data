package edu.mst.db.nlm.goldstd.corpora;

import java.util.Comparator;

import edu.mst.db.nlm.goldstd.corpora.annot.NLMAnnotSpanKey;
import edu.mst.db.nlm.goldstd.corpora.annot.NLMAnnotSubType;
import edu.mst.db.nlm.goldstd.corpora.parse.TextConceptTagType;

/**
 * Gold standard annotation span parsed from the NLM annotation database.
 * 
 * @author gjs
 *
 */
public class GoldStdAnnotSpan implements Comparable<GoldStdAnnotSpan> {

    private int id;
    private int annotId;
    private int pmid;
    private int docAnnotId;
    private TextConceptTagType annotType;
    private NLMAnnotSubType annotSubType;
    private int beginDocPos;
    private int endDocPos;
    private int beginSentWordNbr;
    private int endSentWordNbr;
    private String text;

    /*
     * Constructor for instantiating annotation span objects from NLM annotation
     * files. The database id and parent annotation id are auto-increment
     * database fields, and since this object is not yet persisted these values
     * are not yet available.
     */
    public GoldStdAnnotSpan(int pmid, int docAnnotId,
	    TextConceptTagType annotType, NLMAnnotSubType annotSubType,
	    int beginPos, int endPos, int beginSentWordNbr, int endSentWordNbr,
	    String text) {
	this.pmid = pmid;
	this.docAnnotId = docAnnotId;
	this.annotType = annotType;
	this.annotSubType = annotSubType;
	this.beginDocPos = beginPos;
	this.beginSentWordNbr = beginSentWordNbr;
	this.endSentWordNbr = endSentWordNbr;
	this.endDocPos = endPos;
	this.text = text;
    }

    /**
     * Constructor for instantiating annotation span from database.
     * 
     * @param id
     * @param annotId
     * @param pmid
     * @param annotationId
     * @param annotType
     * @param annotSubType
     * @param beginDocPos
     * @param endDocPos
     * @param text
     */
    public GoldStdAnnotSpan(int id, int annotId, int pmid, int annotationId,
	    TextConceptTagType annotType, NLMAnnotSubType annotSubType,
	    int beginDocPos, int endDocPos, int beginSentWordNbr,
	    int endSentWordNbr, String text) {
	this.id = id;
	this.annotId = annotId;
	this.pmid = pmid;
	this.docAnnotId = annotationId;
	this.annotType = annotType;
	this.annotSubType = annotSubType;
	this.beginDocPos = beginDocPos;
	this.endDocPos = endDocPos;
	this.beginSentWordNbr = beginSentWordNbr;
	this.endSentWordNbr = endSentWordNbr;
	this.text = text;
    }

    public NLMAnnotSpanKey getKey() {
	NLMAnnotSpanKey spanKey = new NLMAnnotSpanKey(pmid, docAnnotId,
		annotType.getAnnotTypeId(), annotSubType.getAnnotSubTypeId(),
		beginDocPos);
	return spanKey;
    }

    @Override
    public int compareTo(GoldStdAnnotSpan s2) {
	return Comparators.DbUniqueId.compare(this, s2);
    }

    public static class Comparators {
	public static final Comparator<GoldStdAnnotSpan> DbUniqueId = (
		GoldStdAnnotSpan s1, GoldStdAnnotSpan s2) -> Integer
			.valueOf(s1.id).compareTo(Integer.valueOf(s2.id));
	public static final Comparator<GoldStdAnnotSpan> AnnotId = (
		GoldStdAnnotSpan s1, GoldStdAnnotSpan s2) -> {
	    int i = Integer.valueOf(s1.pmid)
		    .compareTo(Integer.valueOf(s2.pmid));
	    if (i != 0)
		return i;
	    i = Integer.valueOf(s1.docAnnotId)
		    .compareTo(Integer.valueOf(s2.docAnnotId));
	    if (i != 0)
		return i;
	    i = s1.annotType.toString().compareTo(s2.annotType.toString());
	    if (i != 0)
		return i;
	    i = s2.annotSubType.toString()
		    .compareTo(s2.annotSubType.toString());
	    if (i != 0)
		return i;
	    i = Integer.valueOf(s1.beginDocPos)
		    .compareTo(Integer.valueOf(s2.beginDocPos));
	    if (i != 0)
		return i;
	    return Integer.valueOf(s1.endDocPos)
		    .compareTo(Integer.valueOf(s2.endDocPos));
	};
    }

    @Override
    public boolean equals(Object obj) {
	if (obj instanceof GoldStdAnnotSpan) {
	    GoldStdAnnotSpan span = (GoldStdAnnotSpan) obj;
	    return this.id == span.id;
	}
	return false;
    }

    @Override
    public int hashCode() {
	return Integer.valueOf(id).hashCode();
    }

    public int getId() {
	return id;
    }

    public void setId(int id) {
	this.id = id;
    }

    public int getAnnotId() {
	return annotId;
    }

    public void setAnnotId(int annotId) {
	this.annotId = annotId;
    }

    public int getPmid() {
	return pmid;
    }

    public void setPmid(int pmid) {
	this.pmid = pmid;
    }

    public int getDocAnnotId() {
	return docAnnotId;
    }

    public void setDocAnnotId(int annotationId) {
	this.docAnnotId = annotationId;
    }

    public TextConceptTagType getAnnotType() {
	return annotType;
    }

    public void setAnnotType(TextConceptTagType annotType) {
	this.annotType = annotType;
    }

    public NLMAnnotSubType getAnnotSubType() {
	return annotSubType;
    }

    public void setAnnotSubType(NLMAnnotSubType annotSubType) {
	this.annotSubType = annotSubType;
    }

    public int getBeginPos() {
	return beginDocPos;
    }

    public void setBeginPos(int beginPos) {
	this.beginDocPos = beginPos;
    }

    public int getEndPos() {
	return endDocPos;
    }

    public void setEndPos(int endPos) {
	this.endDocPos = endPos;
    }

    public int getBeginSentWordNbr() {
	return beginSentWordNbr;
    }

    public void setBeginSentWordNbr(int beginSentWordNbr) {
	this.beginSentWordNbr = beginSentWordNbr;
    }

    public int getEndSentWordNbr() {
	return endSentWordNbr;
    }

    public void setEndSentWordNbr(int endSentWordNbr) {
	this.endSentWordNbr = endSentWordNbr;
    }

    public String getText() {
	return text;
    }

    public void setText(String text) {
	this.text = text;
    }

}

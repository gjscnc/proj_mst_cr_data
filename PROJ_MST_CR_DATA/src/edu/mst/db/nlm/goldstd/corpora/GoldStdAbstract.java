package edu.mst.db.nlm.goldstd.corpora;

/**
 * This class instantiates gold standard abstract from database and formats the
 * text of the abstract to be suitable for parsing.
 * 
 * @author George
 *
 */
public class GoldStdAbstract implements Comparable<GoldStdAbstract> {

    private int pmid = 0;
    private String title = null;
    private String text = null;
    private int abstrLen = 0;

    public GoldStdAbstract(int pmid, String title, String text)
	    throws Exception {
	if (title == null || title.isEmpty() || text == null
		|| text.isEmpty()) {
	    String msg = String.format(
		    "Missing title or missing text for abstract PMID %1$d ",
		    pmid);
	    throw new Exception(msg);
	}
	this.pmid = pmid;
	this.title = title;
	this.text = text;

	// if no period or question mark at end of title then add period
	/*
	 * char titleLastChar = title.charAt(title.length() - 1); if
	 * (titleLastChar == Constants.periodChar || titleLastChar ==
	 * Constants.questionChar) { text = title.concat(" ").concat(body); }
	 * else { text = title.concat(". ").concat(body); }
	 */
	abstrLen = text.length();
    }

    @Override
    public int compareTo(GoldStdAbstract abstr) {
	return Integer.compare(pmid, abstr.pmid);
    }

    @Override
    public boolean equals(Object obj) {
	if (obj instanceof GoldStdAbstract) {
	    GoldStdAbstract abstr = (GoldStdAbstract) obj;
	    return pmid == abstr.pmid;
	}
	return false;
    }

    @Override
    public int hashCode() {
	return Integer.hashCode(pmid);
    }

    public String getTextToIndex() {
	if (text.startsWith(title)) {
	    return text;
	} else {
	    return title.concat("  ").concat(text);
	}
    }

    public int getPmid() {
	return pmid;
    }

    public String getTitle() {
	return title;
    }

    /**
     * The text contains the abstract, i.e., title concatenated with the body of
     * the abstract.
     * 
     * @return
     */
    public String getText() {
	return text;
    }

    public int getAbstrLen() {
	return abstrLen;
    }

}

/**
 * 
 */
package edu.mst.db.nlm.goldstd.corpora.annot;

import java.io.BufferedReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.Callable;

import org.javatuples.Pair;

import edu.mst.db.nlm.corpora.Corpus;
import edu.mst.db.nlm.goldstd.corpora.parse.NlmTestFileNames;
import edu.mst.db.util.Constants;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * @author gjs
 *
 */
public class PersistGoldStdTagsCallable
	implements Callable<Pair<Integer, Integer>> {

    private int pmid = 0;
    private String annotFullFileName = null;
    private Corpus corpus = null;
    private boolean isPersist = false;
    private JdbcConnectionPool connPool = null;
    private GoldStdTagDao tagDao = new GoldStdTagDao();
    private GoldStdTagSpanDao tagSpanDao = new GoldStdTagSpanDao();
    private GoldStdTagCuiDao tagCuiDao = new GoldStdTagCuiDao();
    private Map<String, GoldStdTag> tagByTagId = new HashMap<String, GoldStdTag>();
    private Map<String, String> cuiByTagId = new HashMap<String, String>();
    private Map<String, String> cuiNameByTagId = new HashMap<String, String>();
    private SortedSet<GoldStdTagSpan> tagSpans = new TreeSet<GoldStdTagSpan>();
    private SortedSet<GoldStdTagCui> tagCuis = new TreeSet<GoldStdTagCui>();

    public PersistGoldStdTagsCallable(int pmid, String annotFullFileName,
	    Corpus corpus, boolean isPersist, JdbcConnectionPool connPool) {
	this.pmid = pmid;
	this.annotFullFileName = annotFullFileName;
	this.corpus = corpus;
	this.isPersist = isPersist;
	this.connPool = connPool;
    }

    @Override
    public Pair<Integer, Integer> call() throws Exception {

	Path file = Paths.get(annotFullFileName);

	try (BufferedReader reader = Files.newBufferedReader(file,
		NlmTestFileNames.CHARACTER_SET)) {

	    String text = null;
	    while ((text = reader.readLine()) != null) {

		// all lines that begin with a 'T' are tags
		if (text.startsWith("T")) {
		    extractTagSpans(text);
		}

		// all lines that begin with a 'N' are cuis
		if (text.startsWith("N")) {
		    extractCui(text);
		}

	    }
	}
	
	for(String tagId : tagByTagId.keySet()) {
	    String cui = cuiByTagId.get(tagId);
	    String cuiName = cuiNameByTagId.get(tagId);
	    GoldStdTag tag = tagByTagId.get(tagId);
	    tag.setCui(cui);
	    tag.setCuiName(cuiName);
	}

	if (isPersist) {
	    tagDao.persistBatch(tagByTagId.values(), connPool);
	    tagSpanDao.persistBatch(tagSpans, connPool);
	    tagCuiDao.persistBatch(tagCuis, connPool);
	}

	Pair<Integer, Integer> results = new Pair<Integer, Integer>(
		tagSpans.size(), tagCuis.size());

	return results;
    }

    private void extractTagSpans(String text) {

	String[] tabSplits = text.split(Constants.tabStr, 0);

	String tagId = tabSplits[0];

	String taggedText = tabSplits[2].trim();

	List<Pair<Integer, Integer>> spans = new ArrayList<Pair<Integer, Integer>>();

	int firstSpanPos = tabSplits[1].indexOf(Constants.spaceChar) + 1;

	String category = tabSplits[1].substring(0, firstSpanPos).trim();

	String[] spansText = tabSplits[1].substring(firstSpanPos)
		.split(Constants.semiColonStr);
	for (String spanText : spansText) {
	    int firstBlankPos = spanText.indexOf(Constants.spaceChar);
	    Integer beginPos = Integer
		    .valueOf(spanText.substring(0, firstBlankPos));
	    Integer endPos = Integer
		    .valueOf(spanText.substring(firstBlankPos + 1));
	    Pair<Integer, Integer> span = new Pair<Integer, Integer>(beginPos,
		    endPos);
	    spans.add(span);
	}

	int spanNbr = 0;
	for (Pair<Integer, Integer> span : spans) {
	    GoldStdTagSpan tagSpan = new GoldStdTagSpan();
	    tagSpan.setPmid(pmid);
	    tagSpan.setTagId(tagId);
	    tagSpan.setSpanNbr(spanNbr);
	    tagSpan.setCategory(category);
	    tagSpan.setBeginPos(span.getValue0());
	    tagSpan.setEndPos(span.getValue1());
	    tagSpan.setText(taggedText);
	    tagSpan.setCorpus(corpus);
	    tagSpans.add(tagSpan);
	    spanNbr++;
	}

	String spansAsText = tabSplits[1].substring(firstSpanPos);
	GoldStdTag tag = new GoldStdTag();
	tag.setPmid(pmid);
	tag.setTagId(tagId);
	tag.setTaggedText(taggedText);
	tag.setSpansAsText(spansAsText);
	tag.setCategory(category);
	tag.setCorpus(corpus);
	tagByTagId.put(tagId, tag);

    }

    /**
     * If multiple CUIs are associated with the same tag, then the last one
     * specified is used.
     * 
     * @param text
     */
    private void extractCui(String text) {

	if (!text.contains("ConceptId:")) {
	    return;
	}

	String[] tabSplits = text.split(Constants.tabStr, 0);

	String referenceId = tabSplits[0];

	int tagIdBegin = tabSplits[1].indexOf(Constants.spaceChar) + 1;
	int tagIdEnd = tabSplits[1].indexOf(Constants.spaceChar, tagIdBegin);
	String tagId = tabSplits[1].substring(tagIdBegin, tagIdEnd).trim();

	int cuiBegin = tabSplits[1].indexOf(Constants.colonChar) + 1;
	String cui = tabSplits[1].substring(cuiBegin).trim();
	cuiByTagId.put(tagId, cui);

	String cuiName = tabSplits[2].trim();
	cuiNameByTagId.put(tagId, cuiName);

	GoldStdTagCui tagCui = new GoldStdTagCui();
	tagCui.setPmid(pmid);
	tagCui.setReferenceId(referenceId);
	tagCui.setTagId(tagId);
	tagCui.setCui(cui);
	tagCui.setCuiName(cuiName);
	tagCui.setCorpus(corpus);

	tagCuis.add(tagCui);
    }

}

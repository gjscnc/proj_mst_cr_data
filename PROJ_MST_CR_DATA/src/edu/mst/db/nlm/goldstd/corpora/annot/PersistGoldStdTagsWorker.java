/**
 * 
 */
package edu.mst.db.nlm.goldstd.corpora.annot;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Future;

import org.javatuples.Pair;

import edu.mst.concurrent.BaseWorker;
import edu.mst.db.nlm.corpora.Corpus;
import edu.mst.db.nlm.goldstd.corpora.parse.NlmTestFileNames;
import edu.mst.db.nlm.goldstd.corpora.parse.Platform;
import edu.mst.db.util.ConnDbName;
import edu.mst.db.util.Constants;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * Persists annotation file content in the database.
 * <p>
 * Specific entries found in each *.annot file that are persisted in the database include:
 * <ul>
 * <li>Each 'Txx' tag entry</li>
 * <li>Each 'Nxx' entry that specifies the CUI concept ID for the 'Txx' tag</li>
 * </ul>

 * @author gjs
 *
 */
public class PersistGoldStdTagsWorker
	extends BaseWorker<Pair<Integer, Integer>> {

    private String annotFilesDir = null;
    private boolean isDeleteExisting = false;
    private boolean isPersist = false;
    private boolean isDebug = false;
    private int debugPmid = 0;
    private Corpus corpus = null;
    private Platform platform = null;
    private char dirDelimChar = ' ';

    private Map<Integer, String> annotFilesByPmid;
    private int[] pmids;
    private int nbrPmids;
    private int nbrPmidsProcessed = 0;
    private int nbrTagsPersisted = 0;
    private int nbrTagCuisPersisted = 0;
    private int rptInterval = 50;

    public PersistGoldStdTagsWorker(String annotFilesDir,
	    boolean isDeleteExisting, boolean isPersist, boolean isDebug,
	    int debugPmid, Platform platform, Corpus corpus,
	    JdbcConnectionPool connPool) {
	super(connPool);
	this.annotFilesDir = annotFilesDir;
	this.isDeleteExisting = isDeleteExisting;
	this.isPersist = isPersist;
	this.isDebug = isDebug;
	this.debugPmid = debugPmid;
	this.platform = platform;
	this.corpus = corpus;
	init();
    }

    private void init() {
	switch (platform) {
	case WINDOWS_DESKTOP:
	    dirDelimChar = NlmTestFileNames.DIR_DELIM_WINDOWS;
	    break;
	case WINDOWS_LAPTOP:
	    dirDelimChar = NlmTestFileNames.DIR_DELIM_WINDOWS;
	    break;
	case LINUX:
	    dirDelimChar = NlmTestFileNames.DIR_DELIM_LINUX;
	    break;
	}
    }

    @Override
    public void run() throws Exception {

	if (isDeleteExisting) {
	    System.out
		    .println("Deleting existing gold standard annotation tags");
	    GoldStdTagDao.deleteAll(connPool);
	    GoldStdTagSpanDao.deleteAll(connPool);
	    GoldStdTagCuiDao.deleteAll(connPool);
	}

	System.out.println("Retrieving annotation file names");
	{
	    if (isDebug && debugPmid != Constants.uninitializedIntVal) {
		String fullFileName = String.format("%1$s%2$spmid-%3$d.%4$s",
			annotFilesDir, String.valueOf(dirDelimChar), debugPmid,
			NlmTestFileNames.ANNOTATION_EXT);
		annotFilesByPmid = new HashMap<Integer, String>();
		annotFilesByPmid.put(debugPmid, fullFileName);
	    } else {
		NlmTestFileNames fileNames = new NlmTestFileNames(platform,
			corpus);
		fileNames.setFilesDir(annotFilesDir);
		fileNames.compileNlmFileNames();
		annotFilesByPmid = fileNames.getAnnotFullNamesByPmid();
	    }
	    nbrPmids = annotFilesByPmid.size();
	    pmids = new int[nbrPmids];
	    int pos = 0;
	    for (int pmid : annotFilesByPmid.keySet()) {
		pmids[pos] = pmid;
		pos++;
	    }
	    Arrays.sort(pmids);
	}
	System.out.printf("Retrieved %1$,d annotation files.%n", nbrPmids);

	while (currentPos < nbrPmids) {

	    maxPos = getNextBatchMaxPos();
	    startPos = currentPos;

	    for (int i = startPos; i <= maxPos; i++) {
		int pmid = pmids[i];
		String annotFullFileName = annotFilesByPmid.get(pmid);
		PersistGoldStdTagsCallable callable = new PersistGoldStdTagsCallable(
			pmid, annotFullFileName, corpus, isPersist, connPool);
		svc.submit(callable);
	    }

	    for (int i = startPos; i <= maxPos; i++) {
		Future<Pair<Integer, Integer>> future = svc.take();
		Pair<Integer, Integer> results = future.get();
		nbrTagsPersisted += results.getValue0().intValue();
		nbrTagCuisPersisted += results.getValue1().intValue();
		nbrPmidsProcessed++;
		if (nbrPmidsProcessed % rptInterval == 0) {
		    System.out.printf(
			    "Processed %1$,d PMIDs, persisting %2$,d tags and %3$,d tag CUIs. %n",
			    nbrPmidsProcessed, nbrTagsPersisted,
			    nbrTagCuisPersisted);
		}
	    }

	    currentPos = maxPos + 1;

	}

	System.out.printf(
		"Processed total of %1$,d PMIDs, persisting %2$,d tags and %3$,d tag CUIs. %n",
		nbrPmidsProcessed, nbrTagsPersisted, nbrTagCuisPersisted);
    }

    @Override
    protected int getNextBatchMaxPos() throws Exception {
	int maxPos = currentPos + threadCallablesBatchSize - 1;
	if (maxPos >= nbrPmids - 1)
	    maxPos = nbrPmids - 1;
	return maxPos;
    }

    public static void main(String[] args) {

	try {

	    JdbcConnectionPool connPool = new JdbcConnectionPool(
		    ConnDbName.CONCEPT_RECOGN);
	    Platform platform = Platform.LINUX;
	    Corpus corpus = Corpus.NCBI_DISEASE_CORPUS;
	    NlmTestFileNames fileName = new NlmTestFileNames(platform, corpus);
	    String filesDir = fileName.getFilesDir();
	    boolean isPersistAnnot = true;
	    boolean isDelExisting = false;
	    boolean isDebug = false;
	    int debugPmid = 24090791;

	    try (PersistGoldStdTagsWorker worker = new PersistGoldStdTagsWorker(
		    filesDir, isDelExisting, isPersistAnnot, isDebug, debugPmid,
		    platform, corpus, connPool)) {
		worker.run();
	    }

	} catch (Exception ex) {
	    ex.printStackTrace();
	}

    }
}

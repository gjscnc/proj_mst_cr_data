package edu.mst.db.nlm.goldstd.corpora.parse;

public enum Platform {
    
    LINUX, WINDOWS_DESKTOP, WINDOWS_LAPTOP

}

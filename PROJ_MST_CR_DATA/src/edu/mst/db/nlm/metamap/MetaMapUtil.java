package edu.mst.db.nlm.metamap;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.mst.db.nlm.goldstd.corpora.GoldStdPosPhrase;
import edu.mst.db.nlm.goldstd.corpora.GoldStdSentWord;
import edu.mst.db.nlm.goldstd.corpora.GoldStdSentence;
import edu.mst.db.text.util.TextInterval;
import edu.mst.db.text.util.TextIntervalUtil;
import edu.mst.db.text.util.WordUtil;
import edu.mst.db.util.JdbcConnectionPool;
import gov.nih.nlm.nls.metamap.MetaMapApi;
import gov.nih.nlm.nls.metamap.MetaMapApiImpl;
import gov.nih.nlm.nls.metamap.PCM;
import gov.nih.nlm.nls.metamap.Phrase;
import gov.nih.nlm.nls.metamap.Position;
import gov.nih.nlm.nls.metamap.Result;
import gov.nih.nlm.nls.metamap.Utterance;

public class MetaMapUtil {

    public static final String getAbstrSql = "SELECT abstract "
	    + "FROM sps.testabstract WHERE pmid=?";

    private JdbcConnectionPool connPool;
    private MetaMapApi mmApi;
    private List<Result> resultList;
    private WordUtil wordUtil;
    private List<GoldStdSentence> sentences;
    private Map<Integer, List<GoldStdPosPhrase>> phrasesBySentNbr;
    private Map<Integer, List<GoldStdSentWord>> wordsBySentNbr;
    private int pmid;
    private String docText = null;
    private long docEndPos = -1L;
    private List<TextInterval> sentenceIntervals;
    private TextIntervalUtil intervalUtil;

    /**
     * 
     * @param connPool
     * @param useWSD
     */
    public MetaMapUtil(JdbcConnectionPool connPool, boolean useWSD) {
	this.connPool = connPool;
	mmApi = new MetaMapApiImpl();
	/*
	 * Options: MetaMap options available in the api are below. USE OF ANY
	 * OPTIONS NOT LISTED RESULTS IN CRASHING THE SERVER!
	 * 
	 * -@ --WSD <hostname> : Which WSD server to use. -8
	 * --dynamic_variant_generation : dynamic variant generation -A
	 * --strict_model : use strict model -C --relaxed_model : use relaxed
	 * model -D --all_derivational_variants : all derivational variants -J
	 * --restrict_to_sts <semtypelist> : restrict to semantic types -K
	 * --ignore_stop_phrases : ignore stop phrases. -R --restrict_to_sources
	 * <sourcelist> : restrict to sources -S --tagger <sourcelist> : Which
	 * tagger to use. -V --mm_data_version <name> : version of MetaMap data
	 * to use. -X --truncate_candidates_mappings : truncate candidates
	 * mapping -Y --prefer_multiple_concepts : prefer multiple concepts -Z
	 * --mm_data_year <name> : year of MetaMap data to use. -a
	 * --all_acros_abbrs : allow Acronym/Abbreviation variants -b
	 * --compute_all_mappings : compute/display all mappings -d
	 * --no_derivational_variants : no derivational variants -e
	 * --exclude_sources <sourcelist> : exclude sources -g
	 * --allow_concept_gaps : allow concept gaps -i --ignore_word_order :
	 * ignore word order -k --exclude_sts <semtypelist> : exclude semantic
	 * types -l --allow_large_n : allow Large N -o --allow_overmatches :
	 * allow overmatches -r --threshold <integer> : Threshold for displaying
	 * candidates. -y --word_sense_disambiguation : use WSD -z
	 * --term_processing : use term processing
	 * 
	 */

	String options = "-A";
	//options = "-Aio";
	/*
	 * if (useWSD) { options = "-AIay+f -R SNOMEDCT_US -V USAbase"; } else {
	 * options = "-AIa+f -R SNOMEDCT_US -V USAbase"; }
	 */
	mmApi.setOptions(options);
	wordUtil = new WordUtil();
	intervalUtil = new TextIntervalUtil();
	// String options = "-y -R SNOMEDCT_US";
	// mmApi.setOptions(options);
	/*
	 * List<String> theOptions = new ArrayList<String>();
	 * theOptions.add("-y"); // turn on Word Sense Disambiguation
	 * theOptions.add("--allow_concept_gaps");
	 * theOptions.add("--restrict_to_sources ");
	 * theOptions.add("-R SNOMEDCT"); theOptions.add("-V USAbase");
	 * theOptions.add("-A"); theOptions.add("--ignore_stop_phrases");
	 * 
	 * theOptions.add("--all_acros_abbrs"); if (theOptions.size() > 0) {
	 * mmApi.setOptions(theOptions); }
	 */
    }

    /**
     * This method parses text into sentences, phrases, and finally into words
     * using the NLM MetaMap tool. It uses the MetaMap API to access the MetaMap
     * services hosted locally. When called overwrites previous results.
     * 
     * @param docText
     * @throws Exception
     */
    public void parseText(String docText) throws Exception {
	this.docText = docText;
	parse();
    }

    /**
     * This method parses one PMID into sentences, phrases, and finally into
     * words using the NLM MetaMap tool. It uses the MetaMap API to access the
     * MetaMap services hosted locally. When called overwrites previous results.
     * 
     * @param pmid
     */
    public void parsePmid(int pmid) throws Exception {

	/*
	 * retrieve abstract text
	 */
	this.pmid = pmid;
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement getAbstrStmt = conn
		    .prepareStatement(getAbstrSql)) {
		getAbstrStmt.setInt(1, pmid);
		try (ResultSet rs = getAbstrStmt.executeQuery()) {
		    if (rs.next()) {
			docText = rs.getString(1);
			docEndPos = docText.length() - 1;
		    }
		}
	    }
	}

	if (docText == null || docText.isEmpty()) {
	    String errMsg = String
		    .format("Abstract text not found for PMID %1$d", pmid);
	    throw new Exception(errMsg);
	}

	parse();

    }

    private void parse() throws Exception {

	/*
	 * initialize containers
	 */
	resultList = null;
	sentences = new ArrayList<GoldStdSentence>();
	phrasesBySentNbr = new HashMap<Integer, List<GoldStdPosPhrase>>();
	wordsBySentNbr = new HashMap<Integer, List<GoldStdSentWord>>();
	sentenceIntervals = new ArrayList<TextInterval>();

	// parse document using MetaMap
	resultList = mmApi.processCitationsFromString(docText);

	/*
	 * Parse sentence/words and then phrases
	 */
	parseSentences();
	parsePhrases();

    }

    /**
     * Parse list of sentences.
     * 
     * @param sentBySentNbr
     * @throws Exception
     */
    public Map<Integer, List<GoldStdPosPhrase>> parseSentences(
	    List<GoldStdSentence> sentences) throws Exception {

	// initialize containers
	resultList = null;
	this.sentences = sentences;
	phrasesBySentNbr = new HashMap<Integer, List<GoldStdPosPhrase>>();
	wordsBySentNbr = null;
	sentenceIntervals = null;

	for (GoldStdSentence sentence : sentences) {
	    resultList = mmApi
		    .processCitationsFromString(sentence.getSentText());
	    parsePhrases();
	}

	return phrasesBySentNbr;
    }

    /**
     * Extract and marshal sentences
     * 
     * @throws Exception
     */
    private void parseSentences() throws Exception {

	long end = Long.MIN_VALUE;
	for (Result result : resultList) {
	    List<Utterance> utteranceList = result.getUtteranceList();
	    for (Utterance utterance : utteranceList) {
		Position sentencePos = utterance.getPosition();
		if (sentencePos.getY() <= sentencePos.getX()) {
		    String errMsg = String.format(
			    "Invalid sentence begin and end positions. "
				    + "Begin = %1$,d, end=%2$,d.",
			    sentencePos.getX(), end);
		    throw new Exception(errMsg);
		}
		end = sentencePos.getY();
		if (end >= docEndPos)
		    end = -1L;
		sentenceIntervals
			.add(new TextInterval(pmid, sentencePos.getX(), end));
	    }
	}
	// adjust sentence intervals for odd formats
	// TextInterval interval = new TextInterval(pmid, begin, -1L);
	// sentenceIntervals.add(interval);
	intervalUtil.adjustSentenceIntervals(pmid, docText, sentenceIntervals);
	sentenceIntervals = intervalUtil.getIntervals();
	// compile sentences
	int sentenceNbr = 0;
	for (TextInterval textInterval : sentenceIntervals) {
	    String sentenceText;
	    int sentBegin = (int) textInterval.getBegin();
	    int sentEnd = (int) textInterval.getEnd();
	    try {
		if (sentEnd == -1L)
		    sentenceText = docText.substring(sentBegin);
		else
		    sentenceText = docText.substring(sentBegin, sentEnd);
	    } catch (Exception ex) {
		String errMsg = String.format(
			"Invalid sentence begin and end positions. "
				+ "Begin = %1$,d, end = %2$,d.",
			sentBegin, sentEnd);
		throw new Exception(errMsg, ex);
	    }
	    GoldStdSentence sent = new GoldStdSentence(pmid, sentenceNbr,
		    sentBegin, sentEnd, sentenceText, docText);
	    sentences.add(sent);
	    wordUtil.parseSentenceIntoWords(sent);
	    sent.getWords().sort(GoldStdSentWord.Comparators.ByPmidSentNbrWordNbr);
	    wordsBySentNbr.put(sentenceNbr, sent.getWords());
	    sentenceNbr++;
	}
	// sort sentences for convenience
	sentences.sort(GoldStdSentence.Comparators.ByPmid_SentNbr);

    }

    /**
     * Extract and marshal phrases.
     * <p>
     * Programming Note: both sentences and phrases assign a document end
     * position equal to -1 when either is the last sentence or phrase in the
     * document. The logic used in this method accounts for this.
     * 
     * @throws Exception
     */
    private void parsePhrases() throws Exception {
	for (Result result : resultList) {
	    List<Utterance> utteranceList = result.getUtteranceList();
	    for (Utterance utterance : utteranceList) {
		for (PCM pcm : utterance.getPCMList()) {
		    Phrase phrase = pcm.getPhrase();
		    Position phrasePos = phrase.getPosition();
		    long phraseBeginDocPos = phrasePos.getX();
		    long phraseEndDocPos = phrasePos.getY();
		    if (phraseEndDocPos >= docEndPos)
			phraseEndDocPos = -1L;
		    GoldStdSentence phraseSent = null;
		    for (GoldStdSentence sentence : sentences) {
			int sentEndPos = sentence.getEndDocPos();
			if (sentEndPos < 0)
			    sentEndPos = sentence.getDocText().length();
			if (phraseBeginDocPos >= sentence.getBeginDocPos()
				&& phraseEndDocPos <= sentEndPos) {
			    phraseSent = sentence;
			    break;
			}
		    }
		    // new get words
		    List<GoldStdSentWord> phraseWords = new ArrayList<GoldStdSentWord>();
		    boolean firstWordFound = false;
		    boolean lastWordFound = false;
		    for (GoldStdSentWord phraseWord : phraseSent.getWords()) {
			// stop when past end of phrase
			if (firstWordFound && lastWordFound)
			    break;
			firstWordFound = phraseBeginDocPos <= phraseWord
				.getBeginDocPos();
			if (firstWordFound)
			    phraseWords.add(phraseWord);
			int wordEndPos = phraseWord.getEndDocPos();
			if (wordEndPos < 0)
			    wordEndPos = phraseSent.getDocText().length();
			lastWordFound = wordEndPos >= phraseEndDocPos;
		    }
		    GoldStdPosPhrase posPhrase = new GoldStdPosPhrase(phraseSent);
		    posPhrase.setPhraseWords(phraseWords);
		    posPhrase.setPosTag(phrase.getMincoManAsString());
		    if (!phrasesBySentNbr
			    .containsKey(phraseSent.getSentenceNbr()))
			phrasesBySentNbr.put(phraseSent.getSentenceNbr(),
				new ArrayList<GoldStdPosPhrase>());
		    phrasesBySentNbr.get(phraseSent.getSentenceNbr())
			    .add(posPhrase);
		}
	    }
	}
    }

    public List<GoldStdSentence> getSentences() {
	return sentences;
    }

    public Map<Integer, List<GoldStdSentWord>> getWordsBySentNbr() {
	return wordsBySentNbr;
    }

    public int getPmid() {
	return pmid;
    }

    public String getDocText() {
	return docText;
    }

    public List<TextInterval> getSentenceIntervals() {
	return sentenceIntervals;
    }

    public MetaMapApi getMmApi() {
	return mmApi;
    }

    public Map<Integer, List<GoldStdPosPhrase>> getPhrasesBySentNbr() {
	return phrasesBySentNbr;
    }

    public List<Result> getResultList() {
	return resultList;
    }

}

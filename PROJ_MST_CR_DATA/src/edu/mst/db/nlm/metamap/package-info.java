/**
 * MetaMap and related utilities for parsing text into sentences, phrases, and words. 
 * When parsing text into words, provides utilities for identifying acronyms and
 * abbreviations.
 */
/**
 * @author gjs
 *
 */
package edu.mst.db.nlm.metamap;
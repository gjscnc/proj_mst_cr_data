package edu.mst.db.nlm.metamap;

/**
 * Abstract class containing attributes common to all MINCOMAN objects.
 * 
 * @author George
 *
 */
public abstract class MincoManObj {

    /**
     * Extracted text for this object.
     */
    public String text;
    /**
     * Beginning character position of object in document
     */
    public int beginDocPos = 0;
    /**
     * End character position of object in document
     * <p>
     * Is one position past end of object, unless it is the last character in
     * document. If last object in document, and the last character is at the
     * end of the document, then end position of object set to -1;
     * </p>
     */
    public int endDocPos = 0;

}

package edu.mst.db.nlm.metamap;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Phrase in the sentence
 * 
 * @author George
 *
 */
public class MincoManPhrase extends MincoManObj implements
	Comparable<MincoManPhrase> {
    /**
     * Sentence where this phrase is found.
     */
    public MincoManSentence mincoSent;
    /**
     * Phrase number in the sentence. This is not phrase number in the document,
     * it is the sequential number of the phrase in the sentence.
     */
    public int phraseNbr;
    /**
     * First sentence word sequence number in this phrase. Used for calculating
     * the sentence word nbr for each word.
     */
    public int sentFirstWordNbr;
    /**
     * Last sentence word sequence number in this phrase. Used for calculating
     * the word numbers in the next phrase in the sentence.
     */
    public int sentLastWordNbr;
    /**
     * Part-of-speech tagging results from MINCOMAN parser (minimal commitment
     * parser) used by MetaMap.
     * <p>
     * The MincoMan string for each phrase is stored in the database for
     * traceability and debugging.
     * </p>
     */
    public String phraseMincoManStr;
    /**
     * Words for this phrase, parsed from the MincoMan formatted output.
     */
    public List<MincoManWord> mincoWords = new ArrayList<MincoManWord>();
    /**
     * List of CUIs provided by MetaMap, i.e., results of entity extraction.
     */
    public Set<String> metaMapCuis = new HashSet<String>();

    @Override
    public int compareTo(MincoManPhrase phrase) {
	int i = mincoSent.compareTo(phrase.mincoSent);
	if (i != 0)
	    return i;
	return Integer.compare(phraseNbr, phrase.phraseNbr);
    }

    @Override
    public boolean equals(Object obj) {
	if (obj instanceof MincoManPhrase) {
	    MincoManPhrase phrase = (MincoManPhrase) obj;
	    return mincoSent.equals(phrase.mincoSent)
		    && phraseNbr == phrase.phraseNbr;
	}
	return false;
    }

    @Override
    public int hashCode() {
	return mincoSent.hashCode() + 23 * Integer.hashCode(phraseNbr);
    }

}

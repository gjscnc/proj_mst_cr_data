package edu.mst.db.nlm.metamap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Document to be parsed by MetaMap
 * 
 * @author George
 *
 */
public class MincoManDoc extends MincoManObj implements Comparable<MincoManDoc> {
    /**
     * Document ID for sentence.
     */
    public int pmid;
    /**
     * List of sentences associated with this document.
     */
    public List<MincoManSentence> mincoSentences = new ArrayList<MincoManSentence>();
    /**
     * List of acronyms and abbreviations found in this document.
     * <p>
     * The MetaMap application provides acronyms and abbreviations at the
     * document level only.
     * </p>
     */
    public Map<String, MincoManAcrAbbrev> acrAbbrevByToken = new HashMap<String, MincoManAcrAbbrev>();

    @Override
    public int compareTo(MincoManDoc doc) {
	return Integer.compare(pmid, doc.pmid);
    }

    @Override
    public boolean equals(Object obj) {
	if (obj instanceof MincoManDoc) {
	    MincoManDoc doc = (MincoManDoc) obj;
	    return pmid == doc.pmid;
	}
	return false;
    }

    @Override
    public int hashCode() {
	return Integer.hashCode(pmid);
    }
}

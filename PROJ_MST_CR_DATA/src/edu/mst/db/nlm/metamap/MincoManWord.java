package edu.mst.db.nlm.metamap;

/**
 * Class holding the results of parsing words and part-of-speech from MINCOMAN
 * text.
 * 
 * @author George
 *
 */
public class MincoManWord extends MincoManObj implements
	Comparable<MincoManWord> {
    /**
     * Phrase that this word belongs to;
     */
    public MincoManPhrase mincoPhrase;
    /**
     * Word sequence number of the word within the phrase.
     */
    public int phraseWordNbr;
    /**
     * Word sequence number in the sentence.
     */
    public int sentWordNbr;
    /**
     * Part of speech tag for this word
     */
    public String post;
    /**
     * Lexicon word database ID for this word token
     */
    public int lexWordId;
    /**
     * Whether or not this token is an acronym or abbreviation
     */
    public boolean isAcrAbbrev = false;
    /**
     * Acronym/abbreviation object provided by MincoMan containing both the
     * acronym and its expanded form
     */
    public MincoManAcrAbbrev acrAbbrev = null;
    /**
     * Is this word a punctuation character
     */
    public boolean isPunct = false;
    /**
     * Database ID for the persisted acronym/abbreviation
     */
    public int acrAbbrevId = 0;
    /**
     * Begin position within the phrase
     */
    public int beginPhrasePos = 0;
    /**
     * End position within the phrase
     */
    public int endPhrasePos = 0;

    @Override
    public int compareTo(MincoManWord word) {
	int i = mincoPhrase.compareTo(word.mincoPhrase);
	if (i != 0)
	    return i;
	return Integer.compare(phraseWordNbr, word.phraseWordNbr);
    }

    @Override
    public boolean equals(Object obj) {
	if (obj instanceof MincoManWord) {
	    MincoManWord word = (MincoManWord) obj;
	    return mincoPhrase.equals(word.mincoPhrase)
		    && phraseWordNbr == word.phraseWordNbr;
	}
	return false;
    }

    @Override
    public int hashCode() {
	return mincoPhrase.hashCode() + 31 * Integer.hashCode(phraseWordNbr);
    }
}

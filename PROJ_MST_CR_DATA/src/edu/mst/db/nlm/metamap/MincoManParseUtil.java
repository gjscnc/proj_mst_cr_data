/**
 * 
 */
package edu.mst.db.nlm.metamap;

import edu.mst.db.nlm.corpora.NlmAbstract;
import edu.mst.db.nlm.corpora.NlmAbstractDao;
import edu.mst.db.nlm.goldstd.corpora.GoldStdAbstract;
import edu.mst.db.nlm.goldstd.corpora.GoldStdAbstractDao;
import edu.mst.db.text.util.StanfordParser;
import edu.mst.db.text.util.Utf8Util;
import edu.mst.db.text.util.WordUtil;
import edu.mst.db.util.JdbcConnectionPool;
import gov.nih.nlm.nls.metamap.AcronymsAbbrevs;
import gov.nih.nlm.nls.metamap.Ev;
import gov.nih.nlm.nls.metamap.Mapping;
import gov.nih.nlm.nls.metamap.MetaMapApi;
import gov.nih.nlm.nls.metamap.PCM;
import gov.nih.nlm.nls.metamap.Phrase;
import gov.nih.nlm.nls.metamap.Position;
import gov.nih.nlm.nls.metamap.Result;
import gov.nih.nlm.nls.metamap.Utterance;

import java.util.ArrayList;
import java.util.List;

/**
 * This class contains methods for using the MetaMap tool for:
 * <ul>
 * <li>parsing text into sentences
 * <li>parsing each sentence into phrases
 * <li>parsing each phrase into words
 * </ul>
 * This includes extracting MINCOMAN output from MetaMap for each phrase and
 * extracting words along with part-of-speech associated with each word.
 * <p>
 * In addition it extracts numerous other positional values for sentences,
 * phrases, and words.
 * </p>
 * <p>
 * Acronyms and abbreviations are identified also. This includes the expansion
 * of each of these into the normal form.
 * </p>
 * <p>
 * Note: use class
 * {@link edu.mst.db.nlm.goldstd.corpora.PersistConceptWords.ontology.cogency.tagger.nlm.abstr.PersistSentAndPhrasesAndWords}
 * to persist sentences, phrases, and words, along with acronyms and
 * abbreviations.
 * </p>
 * 
 * @author George
 *
 */
@SuppressWarnings("unused")
public class MincoManParseUtil {

    public static final String punctDelim = "punc([";
    public static final String punctTokenDelim = "inputmatch([";
    public static final int punctTokenDelimLen = punctTokenDelim.length();
    public static final String endOfPostDelim = "])])";
    public static final int endOfPostDelimLen = endOfPostDelim.length();
    public static final String tokensDelim = "tokens([";
    public static final int tokensDelimLen = tokensDelim.length();
    public static final String lexMatchDelim = "lexmatch([";
    public static final int lexMatchDelimLen = lexMatchDelim.length();
    public static final String shapesDelim = "shapes([";
    public static final int shapesDelimLen = shapesDelim.length();
    public static final String indivLexMatchTokenDelimStr = "\\s";
    public static final char indivLexMatchTokenDelimChar = '\u0020';
    public static final String postAcronDelim = "tag(";
    public static final int postAcronDelimLen = 4;
    public static final char fieldEndPostAcronDelim = ')';
    public static final char fieldBeginDelim = '[';
    public static final char fieldEndDelim = ']';
    public static final char postDelim = ',';
    public static final char indivTokenDelim = ',';

    private MetaMapUtil mmUtil = null;
    private StanfordParser stanfordParser = null;
    private WordUtil wordUtil = null;

    public MincoManParseUtil(MetaMapUtil mmUtil, StanfordParser stanfordParser)
	    throws Exception {
	this.mmUtil = mmUtil;
	this.stanfordParser = stanfordParser;
	wordUtil = new WordUtil();
    }

    /**
     * Obtain acronym/abbreviation for this text. These are provided in the
     * acronym/abbreviation set for the returned MincoManDoc.
     * 
     * @param text
     * @return edu.mst.db.text.metamap.MincoManDoc
     * @throws Exception
     */
    public MincoManDoc getAcrAbbrev(String text) throws Exception {

	MetaMapApi mmApi = mmUtil.getMmApi();

	MincoManDoc mincoDoc = new MincoManDoc();
	mincoDoc.pmid = 0;
	mincoDoc.text = text;
	mincoDoc.beginDocPos = 0;
	mincoDoc.endDocPos = text.length() - 1;

	List<Result> results = mmApi.processCitationsFromString(text);
	if (results == null || results.size() == 0) {
	    return mincoDoc;
	}

	Result result = results.get(0);
	/*
	 * Retrieve list of all acronyms and abbreviations for this text. This
	 * is done since acronyms and abbreviations are only available at the
	 * document level with MetaMap.
	 */
	List<AcronymsAbbrevs> acrAbbrs = result.getAcronymsAbbrevs();
	for (AcronymsAbbrevs acrAbbr : acrAbbrs) {
	    MincoManAcrAbbrev mincoAcrAbbr = new MincoManAcrAbbrev();
	    mincoAcrAbbr.metaMapAcrAbbrUtf8CharStr = acrAbbr.getAcronym();
	    mincoAcrAbbr.text = acrAbbr.getAcronym();
	    mincoAcrAbbr.metaMapExpandedUtf8CharStr = acrAbbr.getExpansion();
	    mincoAcrAbbr.expandedForm = acrAbbr.getExpansion();

	    mincoAcrAbbr.cuis = new ArrayList<String>();
	    for (String cui : acrAbbr.getCUIList()) {
		mincoAcrAbbr.cuis.add(cui);
	    }
	    mincoDoc.acrAbbrevByToken.put(mincoAcrAbbr.text, mincoAcrAbbr);
	}

	return mincoDoc;

    }

    /**
     * This method uses MetaMap to identify acronyms and abbreviations in
     * concept names.
     * <p>
     * Note that this method does not automatically extract phrases from the
     * sentence. This is done to avoid automatic text substitution for acronyms
     * and abbreviations into the parsing results; this is automatically done by
     * MetaMap.
     * </p>
     * 
     * @param text
     * @return
     * @throws Exception
     */
    public MincoManDoc parseConceptName(String text) throws Exception {

	MetaMapApi mmApi = mmUtil.getMmApi();

	List<Result> results = mmApi.processCitationsFromString(text);

	MincoManDoc mincoDoc = new MincoManDoc();
	mincoDoc.pmid = 0;
	mincoDoc.text = text;
	mincoDoc.beginDocPos = 0;
	mincoDoc.endDocPos = text.length() - 1;

	/*
	 * When processing the entire document, the results array contains only
	 * one result which is for the entire document. Utterance is for each
	 * sentence.
	 */
	Result result = results.get(0);
	/*
	 * Retrieve list of all acronyms and abbreviations used in this
	 * sentence. Note that MetaMap capabilities are such that acronyms and
	 * abbreviations are only available at the document level.
	 */
	List<AcronymsAbbrevs> acrAbbrs = result.getAcronymsAbbrevs();
	for (AcronymsAbbrevs acrAbbr : acrAbbrs) {
	    MincoManAcrAbbrev mincoAcrAbbr = new MincoManAcrAbbrev();
	    mincoAcrAbbr.metaMapAcrAbbrUtf8CharStr = acrAbbr.getAcronym();
	    mincoAcrAbbr.text = acrAbbr.getAcronym();
	    mincoAcrAbbr.metaMapExpandedUtf8CharStr = acrAbbr.getExpansion();
	    mincoAcrAbbr.expandedForm = acrAbbr.getExpansion();

	    mincoAcrAbbr.cuis = new ArrayList<String>();
	    for (String cui : acrAbbr.getCUIList()) {
		mincoAcrAbbr.cuis.add(cui);
	    }
	    mincoDoc.acrAbbrevByToken.put(mincoAcrAbbr.text, mincoAcrAbbr);
	}

	return mincoDoc;
    }

    /**
     * This method uses MetaMap to parse sentences from document text.
     * <p>
     * Note that this method does not automatically extract phrases from the
     * sentence. This is done to avoid text substitution for acronyms and
     * abbreviations into the parsing results. This substitution is done
     * automatically by MetaMap.
     * </p>
     * 
     * @param pmid
     * @param connPool
     * @return
     * @throws Exception
     */
    public MincoManDoc parseGoldStdAbstr(int pmid, JdbcConnectionPool connPool)
	    throws Exception {

	// debugging
	if (pmid == 2303408L) {
	    System.out.printf("Debugging PMID = %1$d %n", pmid);
	}

	GoldStdAbstractDao abstrDao = new GoldStdAbstractDao();
	GoldStdAbstract nlmAbstract = abstrDao.marshalAbstract(pmid, connPool);
	String text = nlmAbstract.getTextToIndex();

	MetaMapApi mmApi = mmUtil.getMmApi();

	List<Result> results = mmApi.processCitationsFromString(text);

	MincoManDoc mincoDoc = new MincoManDoc();
	mincoDoc.pmid = pmid;
	mincoDoc.text = text;
	mincoDoc.beginDocPos = 0;
	mincoDoc.endDocPos = text.length() - 1;
	/*
	 * When processing the entire document, the results array contains only
	 * one result which is for the entire document. Utterance is for each
	 * sentence.
	 */
	Result result = results.get(0);
	/*
	 * Retrieve list of all acronyms and abbreviations used in this
	 * sentence. Note that MetaMap capabilities are such that acronyms and
	 * abbreviations are only available at the document level.
	 */
	List<AcronymsAbbrevs> acrAbbrs = result.getAcronymsAbbrevs();
	for (AcronymsAbbrevs acrAbbr : acrAbbrs) {
	    MincoManAcrAbbrev mincoAcrAbbr = new MincoManAcrAbbrev();
	    /*
	     * Previous version MetaMap output provided an array of integer
	     * values, where each integer is the UTF8 code for a character.
	     * These had to be converted to a character array. The MetaMap
	     * output of providing the array of integer values no longer seems
	     * to be in use, at least on the Linux platform.
	     */
	    // mincoAcrAbbr.metaMapAcrAbbrUtf8CharStr = acrAbbr.getAcronym();
	    // mincoAcrAbbr.metaMapExpandedUtf8CharStr = acrAbbr.getExpansion();
	    // mincoAcrAbbr.utf8ValuesToken = Utf8Util.metaMapUTF8ArrayToDec(
	    // mincoAcrAbbr.metaMapAcrAbbrUtf8CharStr);
	    // mincoAcrAbbr.utf8ValuesExpandedForm = Utf8Util
	    // .metaMapUTF8ArrayToDec(
	    // mincoAcrAbbr.metaMapExpandedUtf8CharStr);
	    // mincoAcrAbbr.text = Utf8Util
	    // .utf8ToString(mincoAcrAbbr.utf8ValuesToken);
	    // mincoAcrAbbr.expandedForm = Utf8Util
	    // .utf8ToString(mincoAcrAbbr.utf8ValuesExpandedForm);

	    mincoAcrAbbr.metaMapAcrAbbrUtf8CharStr = acrAbbr.getAcronym();
	    mincoAcrAbbr.text = acrAbbr.getAcronym();
	    mincoAcrAbbr.metaMapExpandedUtf8CharStr = acrAbbr.getExpansion();
	    mincoAcrAbbr.expandedForm = acrAbbr.getExpansion();

	    mincoAcrAbbr.cuis = new ArrayList<String>();
	    for (String cui : acrAbbr.getCUIList()) {
		mincoAcrAbbr.cuis.add(cui);
	    }
	    mincoDoc.acrAbbrevByToken.put(mincoAcrAbbr.text, mincoAcrAbbr);
	}
	int sentNbr = 0;
	for (Utterance utterance : result.getUtteranceList()) {
	    int beginDocPos = utterance.getPosition().getX();
	    int endDocPos = beginDocPos + utterance.getPosition().getY() - 1;
	    if (endDocPos >= mincoDoc.endDocPos) {
		endDocPos = -1;
	    }
	    MincoManSentence mincoSent = new MincoManSentence();
	    mincoSent.text = utterance.getString();
	    mincoSent.beginDocPos = beginDocPos;
	    mincoSent.endDocPos = endDocPos;
	    mincoSent.sentNbr = sentNbr;
	    mincoSent.mincoDoc = mincoDoc;
	    mincoDoc.mincoSentences.add(mincoSent);
	    sentNbr++;
	}

	return mincoDoc;
    }

    /**
     * Method to parse MincoManPhrases from sentence. It parses phrases and
     * words associated with phrases.
     * <p>
     * Note that this method only parses phrases. This includes adjusting text
     * in each phrase to account for MetaMap deleting acronyms and abbreviations
     * from text (in some instances).
     * 
     * @param mincoSent
     * @throws Exception
     */
    public void parseMincomanPhrases(MincoManSentence mincoSent)
	    throws Exception {

	MetaMapApi mmApi = mmUtil.getMmApi();

	List<Result> results = mmApi.processCitationsFromString(mincoSent.text);

	// instantiate phrases and words for each phrase
	for (Result result : results) {
	    for (Utterance utterance : result.getUtteranceList()) {
		int phraseNbr = 0; // use zero-based numbering for phrases
		int nbrPhrases = utterance.getPCMList().size();
		for (PCM pcm : utterance.getPCMList()) {
		    Phrase mmPhrase = pcm.getPhrase();
		    Position position = mmPhrase.getPosition();
		    int beginDocPos = position.getX() + mincoSent.beginDocPos;
		    if (beginDocPos < 0) {
			beginDocPos = 0;
		    }
		    int endDocPos = beginDocPos + position.getY();
		    if (endDocPos >= mincoSent.mincoDoc.endDocPos) {
			endDocPos = -1;
		    }
		    /*
		     * adjust begin and end positions for prior phrase, and for
		     * the last phrase, to account for the fact that MetaMap
		     * sometimes ignores abbreviations, acronyms in text.
		     */
		    if (phraseNbr > 0) {
			MincoManPhrase priorPhrase = mincoSent.mincoPhrases
				.get(phraseNbr - 1);
			if (beginDocPos - priorPhrase.endDocPos > 1) {
			    priorPhrase.endDocPos = beginDocPos - 1;
			    priorPhrase.text = mincoSent.mincoDoc.text
				    .substring(priorPhrase.beginDocPos,
					    priorPhrase.endDocPos);
			}
		    }
		    // adjust end position for last phrase to account for
		    // MetaMap removing text
		    if (phraseNbr == nbrPhrases - 1) {
			if (endDocPos < mincoSent.endDocPos
				|| (mincoSent.endDocPos == -1
					&& endDocPos != -1)) {
			    endDocPos = mincoSent.endDocPos;
			}
		    }
		    MincoManPhrase mincoPhrase = new MincoManPhrase();
		    if (endDocPos == -1) {
			mincoPhrase.text = mincoSent.mincoDoc.text
				.substring(beginDocPos);
		    } else {
			mincoPhrase.text = mincoSent.mincoDoc.text
				.substring(beginDocPos, endDocPos);
		    }
		    mincoPhrase.beginDocPos = beginDocPos;
		    mincoPhrase.endDocPos = endDocPos;
		    mincoPhrase.phraseMincoManStr = mmPhrase
			    .getMincoManAsString();
		    mincoPhrase.phraseNbr = phraseNbr;
		    mincoPhrase.mincoSent = mincoSent;
		    mincoSent.mincoPhrases.add(mincoPhrase);
		    for (Mapping mapping : pcm.getMappingList()) {
			for (Ev ev : mapping.getEvList()) {
			    String cui = ev.getConceptId();
			    mincoPhrase.metaMapCuis.add(cui);
			}
		    }
		    phraseNbr++;
		}
	    }
	}

    }

    /**
     * Use this method for extracting words from a phrase.
     * <p>
     * Note that this method is automatically invoked when parsing a sentence,
     * so use of this method is not required after parsing a sentence
     * {@link #parseMincomanPhrases(MincoManSentence)}.
     * </p>
     * 
     * @param mincoPhrase
     * @param docTextLastIndexPos
     * @return
     * @throws Exception
     */
    public void parseMincoManPhraseWords(MincoManPhrase mincoPhrase)
	    throws Exception {

	// determine if this is punctuation only. If so, no words extracted.
	// boolean isPunct =
	// mincoPhrase.phraseMincoManStr.startsWith(punctDelim);

	// [shapes([inputmatch([twenty,-,eight]),features([word_numeral]),tokens([twenty,eight])])]

	int beginCursorPos = 0;
	int endCursorPos = mincoPhrase.phraseMincoManStr
		.indexOf(MincoManParseUtil.endOfPostDelim, beginCursorPos)
		+ MincoManParseUtil.endOfPostDelimLen;

	int endOfStrPos = mincoPhrase.phraseMincoManStr.length() - 1;
	int phraseWordNbr = 0;
	while (endCursorPos <= endOfStrPos && endCursorPos != -1) {
	    // extract tag string
	    String tagSubstr = mincoPhrase.phraseMincoManStr
		    .substring(beginCursorPos, endCursorPos);
	    // account for comma between post entries in MincoMan string
	    if (tagSubstr.charAt(0) == ',' || tagSubstr.charAt(0) == '[') {
		tagSubstr = tagSubstr.substring(1);
		beginCursorPos++;
	    }
	    // find beginning of words element
	    List<String> tokens = null;
	    if (tagSubstr.startsWith(punctDelim)) {
		int beginCursor = tagSubstr.indexOf(punctTokenDelim)
			+ punctTokenDelimLen;
		int endCursor = beginCursor + 1;
		String punctToken = tagSubstr.substring(beginCursor, endCursor);
		tokens = new ArrayList<String>();
		tokens.add(punctToken);
	    } else if (tagSubstr.startsWith(shapesDelim)) {
		int beginCursor = tagSubstr.indexOf(tokensDelim)
			+ tokensDelimLen;
		int endCursor = beginCursor + 1;
		String shapesToken = tagSubstr.substring(beginCursor,
			endCursor);
		tokens = new ArrayList<String>();
		tokens.add(shapesToken);
	    } else {
		int tokensCursorBeginPos = tagSubstr
			.indexOf(MincoManParseUtil.lexMatchDelim)
			+ MincoManParseUtil.lexMatchDelimLen;
		int tokensCursorEndPos = tagSubstr.indexOf(
			MincoManParseUtil.fieldEndDelim, tokensCursorBeginPos);
		// parse out word tokens, if a punctuation then do nothing
		tokens = parseWordTokens(tagSubstr
			.substring(tokensCursorBeginPos, tokensCursorEndPos));
	    }
	    /*
	     * if (tagSubstr.contains(MincoManParseUtil.punctDelim)) { int
	     * beginCursor = tagSubstr.indexOf(punctTokenDelim) +
	     * punctTokenDelimLen + 1; int endCursor = beginCursor + 1; String
	     * punctToken = tagSubstr.substring(beginCursor, endCursor);
	     * 
	     * beginCursorPos = endCursorPos +
	     * MincoManParseUtil.endOfPostDelimLen; endCursorPos =
	     * mincoPhrase.phraseMincoManStr.indexOf(
	     * MincoManParseUtil.endOfPostDelim, beginCursorPos); continue; }
	     * else { tokens =
	     * parseWordTokens(tagSubstr.substring(tokensCursorPos,
	     * tokensCursorEndPos)); }
	     */
	    // get part-of-speech tag and instantiate word
	    String postForToken = parsePostName(tagSubstr);
	    for (String token : tokens) {
		MincoManWord mincoWord = new MincoManWord();
		mincoWord.text = token;
		mincoWord.post = postForToken;
		mincoWord.phraseWordNbr = phraseWordNbr;
		// mincoWord.isPunct = isPunct;
		mincoPhrase.mincoWords.add(mincoWord);
		mincoWord.mincoPhrase = mincoPhrase;
		// see if this is an acronym or abbreviation
		if (mincoWord.mincoPhrase.mincoSent.mincoDoc.acrAbbrevByToken
			.size() > 0) {
		    for (String acrAbrText : mincoWord.mincoPhrase.mincoSent.mincoDoc.acrAbbrevByToken
			    .keySet()) {
			if (mincoWord.text.equals(acrAbrText)) {
			    MincoManAcrAbbrev acrAbbrev = mincoWord.mincoPhrase.mincoSent.mincoDoc.acrAbbrevByToken
				    .get(acrAbrText);
			    mincoWord.isAcrAbbrev = true;
			    mincoWord.acrAbbrev = acrAbbrev;
			    break;
			}
		    }
		}
		phraseWordNbr++;
	    }
	    // update cursor positions for next set of tokens
	    beginCursorPos = endCursorPos;
	    endCursorPos = mincoPhrase.phraseMincoManStr
		    .indexOf(MincoManParseUtil.endOfPostDelim, beginCursorPos);
	    if (endCursorPos == -1)
		break;
	    endCursorPos += MincoManParseUtil.endOfPostDelimLen;
	}

	// update word position numbers in phrase
	mincoPhrase.sentFirstWordNbr = mincoPhrase.mincoWords
		.get(0).sentWordNbr;
	mincoPhrase.sentLastWordNbr = mincoPhrase.mincoWords
		.get(mincoPhrase.mincoWords.size() - 1).sentWordNbr;

	// extract position for each token
	String phraseText = mincoPhrase.text.toLowerCase();
	beginCursorPos = 0;
	endCursorPos = 0;
	int docTextLastIndexPos = mincoPhrase.mincoSent.mincoDoc.endDocPos;
	// int parseEndPos = mincoPhrase.endDocPos > -1 ? mincoPhrase.endDocPos
	// : docTextLastIndexPos;
	for (MincoManWord mincoWord : mincoPhrase.mincoWords) {
	    endCursorPos = phraseText.indexOf(mincoWord.text, beginCursorPos);
	    int beginDocPos = mincoPhrase.beginDocPos + endCursorPos;
	    if (beginDocPos < 0)
		beginDocPos = 0;
	    mincoWord.beginDocPos = beginDocPos;
	    int endDocPos = beginDocPos + mincoWord.text.length();
	    if (endDocPos >= docTextLastIndexPos)
		endDocPos = -1;
	    mincoWord.endDocPos = endDocPos;
	    beginCursorPos = endCursorPos;
	}

    }

    /**
     * Method for extracting word tokens from that portion of the MetaMap POS
     * tag that is labeled "tokens".
     * 
     * @param wordTokensStr
     * @return
     */
    public List<String> parseWordTokens(String wordTokensStr) {
	List<String> wordTokens = new ArrayList<String>();
	if (wordTokensStr == null || wordTokensStr.isEmpty())
	    return wordTokens;
	if (wordTokensStr.indexOf(indivLexMatchTokenDelimChar) == -1) {
	    wordTokens.add(wordTokensStr);
	} else {
	    String strToParse = new String(wordTokensStr);
	    // replace spaces with tab to facilitate parsing
	    strToParse.replace(' ', '\t');
	    String[] tokens = wordTokensStr.split(indivLexMatchTokenDelimStr);
	    for (int i = 0; i < tokens.length; i++)
		wordTokens.add(tokens[i]);
	}
	return wordTokens;
    }

    public String parsePunctToken(String mincoStr) {
	String punctToken = null;
	if (mincoStr == null || mincoStr.isEmpty())
	    return punctToken;
	int beginCursor = mincoStr.indexOf(punctTokenDelim) + punctTokenDelimLen
		+ 1;
	int endCursor = beginCursor + 1;
	punctToken = mincoStr.substring(beginCursor, endCursor);
	return punctToken;
    }

    /**
     * Method for extracting part-of-speech tag assigned by MetaMap.
     * 
     * @param tagSubstr
     * @return
     */
    public String parsePostName(String tagSubstr) {
	int beginPostAcronPos = tagSubstr.indexOf(postAcronDelim);
	// if post delimiter not found, return MincoMan post name
	if (beginPostAcronPos < 0) {
	    int lastPosOfMincoPost = tagSubstr.indexOf('(');
	    return tagSubstr.substring(0, lastPosOfMincoPost);
	}
	beginPostAcronPos = beginPostAcronPos + postAcronDelimLen;
	int endPostAcronPos = tagSubstr.indexOf(fieldEndPostAcronDelim,
		beginPostAcronPos);
	return tagSubstr.substring(beginPostAcronPos, endPostAcronPos);
    }

}

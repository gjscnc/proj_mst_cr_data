package edu.mst.db.nlm.metamap;

import java.util.List;

/**
 * Acronym or abbreviation that MetaMap extracted from document. Acronyms and
 * abbreviations are extracted at the document level only.
 * 
 * @author George
 *
 */
public class MincoManAcrAbbrev implements Comparable<MincoManAcrAbbrev> {
    /**
     * Array of UTF-8 codes, in decimal format, for the acronym/abbreviation
     * output from MetaMap
     */
    public int[] utf8ValuesToken;
    /**
     * MetaMap output, in the form of an array of UTF-8 values, but all in text
     * form, for the acronym or abbreviation
     * <p>
     * For example, the MetaMap output for the acronym G6PD is "[71,54,80,68]",
     * i.e., a string in array format containing the sequence of UTF-8 character
     * decimal codes.
     * </p>
     */
    public String metaMapAcrAbbrUtf8CharStr;
    /**
     * Acronym/abbreviation after conversion from UTF-8 codes
     */
    public String text;
    /**
     * Array of UTF-8 codes, in decimal format, for the expanded form of the
     * acronym/abbreviation
     */
    public int[] utf8ValuesExpandedForm;
    /**
     * Same as attribute metaMapAcrAbbrUtf8CharStr, only for the expanded form
     * of the acronym.
     * <p>
     * For example, the expanded form of the acronym "G6PD" is
     * "glucose-6-phosphate dehydrogenase".
     * </p>
     */
    public String metaMapExpandedUtf8CharStr;
    /**
     * Expanded form of the acronym/abbreviation after conversion from the
     * MetaMap UTF-8 codes
     */
    public String expandedForm;
    /**
     * List of IDs for possible UMLS concepts associated with
     * acronym/abbreviation.
     */
    public List<String> cuis;

    @Override
    public int compareTo(MincoManAcrAbbrev acrAbbrev) {
	int i = text.compareTo(acrAbbrev.text);
	if (i != 0)
	    return expandedForm.compareTo(acrAbbrev.expandedForm);
	return 0;
    }

    @Override
    public boolean equals(Object obj) {
	if (obj instanceof MincoManAcrAbbrev) {
	    MincoManAcrAbbrev acrAbbrev = (MincoManAcrAbbrev) obj;
	    if (text.equals(acrAbbrev.text)) {
		if (expandedForm.equals(acrAbbrev.expandedForm))
		    return true;
	    }
	}
	return false;
    }

    @Override
    public int hashCode() {
	return text.hashCode() + 31 * expandedForm.hashCode();
    }

}

package edu.mst.db.nlm.metamap;

import java.util.ArrayList;
import java.util.List;

/**
 * Sentence in the document.
 * 
 * @author George
 *
 */
public class MincoManSentence extends MincoManObj implements
	    Comparable<MincoManSentence> {
	/**
	 * Document this sentence belongs to
	 */
	public MincoManDoc mincoDoc;
	/**
	 * Sentence nbr in document
	 */
	public int sentNbr;
	/**
	 * MincoMan phrases parsed from this sentence
	 */
	public List<MincoManPhrase> mincoPhrases = new ArrayList<MincoManPhrase>();

	@Override
	public int compareTo(MincoManSentence sentence) {
	    int i = mincoDoc.compareTo(sentence.mincoDoc);
	    if (i != 0)
		return i;
	    return Integer.compare(sentNbr, sentence.sentNbr);
	}

	@Override
	public boolean equals(Object obj) {
	    if (obj instanceof MincoManSentence) {
		MincoManSentence sent = (MincoManSentence) obj;
		return mincoDoc.equals(sent.mincoDoc)
			&& sentNbr == sent.sentNbr;
	    }
	    return false;
	}

	@Override
	public int hashCode() {
	    return mincoDoc.hashCode() + 7 * Integer.hashCode(sentNbr);
	}

}

/**
 * 
 */
package edu.mst.db.text.util;

/**
 * This enum used for determining the type of variants desired, primarily
 * whether or not synonym recursion is used.
 * 
 * @author gjs
 *
 */
public enum LexVariantsSynType {
    RECURSIVE_SYNONYMS, SYNONYMS_NOT_RECURSIVE;
}

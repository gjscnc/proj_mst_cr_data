package edu.mst.db.text.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.SortedMap;
import java.util.concurrent.ConcurrentSkipListMap;

import edu.mst.db.nlm.goldstd.corpora.GoldStdSentWord;
import edu.mst.db.nlm.goldstd.corpora.GoldStdSentence;
import edu.mst.db.util.JdbcConnectionPool;

public class WordUtil {

    public static final String getBaseForTokenSql = "select id, token from sps.lexbaseword where token = ?";
    public static final String getBaseFromVarForTokenSql = "Select baseWordId, token "
	    + "from sps.lexvariantword where token = ?";
    public static final String getVariantIdForTokenSql = "Select id, token from sps.lexvariantword WHERE token=?";

    private NlpUtil nlpUtil;
    private Set<Character> wordDelimChars;
    private Set<Character> sentDelimChars;
    // private char[] sentenceChars;
    private GoldStdSentence sentence;
    private String documentText;
    private String sentenceText;
    private int pmid;
    private int sentenceNbr;
    /**
     * This map is used to ensure that words can be retrieved by the sentence
     * position of the word's first char in the text. Map keys are automatically
     * sorted to enhance ease of insertion and retrieval.
     */
    private SortedMap<Integer, GoldStdSentWord> wordsByBeginCharPos;
    private List<TextInterval> wordIntervals;
    private TextIntervalUtil intervalUtil;

    public WordUtil() {
	nlpUtil = new NlpUtil();
	wordDelimChars = nlpUtil.getSentence2WordsDelimChars();
	sentDelimChars = nlpUtil.getSentenceDelimChars();
	intervalUtil = new TextIntervalUtil();
    }

    /**
     * Method for simple parsing of a string into word tokens. For use when
     * complete Sentence object not available.
     * <p>
     * This is essentially the same as method
     * {@link #parseSentenceIntoWordIntervals()}, but modified for use with text
     * instead of a {@link GoldStdSentence} object.
     * </p>
     * <p>
     * Synchronized for use in parallel processing.
     * </p>
     * 
     * @param text
     *            String to parse into words
     * @return List collection of word tokens
     * @throws Exception
     */
    public synchronized List<String> parseStringIntoTokens(String text)
	    throws Exception {

	/*
	 * Parse string into word intervals.
	 */
	wordIntervals = parseWordIntervals(text);

	/*
	 * Extract token list using token intervals.
	 */
	List<String> words = new ArrayList<String>();

	/*
	 * If intervals empty or null, return same
	 */
	if (wordIntervals.size() == 0) {
	    return words;
	}

	for (TextInterval interval : wordIntervals) {
	    long beginInterval = interval.getBegin();
	    long endInterval = interval.getEnd();
	    String wordText;
	    if (endInterval == -1) {
		wordText = text.substring((int) beginInterval);
	    } else {
		wordText = text.substring((int) beginInterval,
			(int) endInterval);
	    }
	    if (wordText.length() == 1) {
		char wordChar = wordText.charAt(0);
		if (wordDelimChars.contains(wordChar)
			|| wordChar == NlpUtil.EndOfSentenceDelim
			|| wordChar == NlpUtil.SemiColon_MetaMapFalseEosDelim)
		    continue;
	    }
	    words.add(wordText);
	}

	return words;
    }

    /**
     * Extracts word intervals for text. Used to parse words from free-form
     * text.
     * 
     * @param text
     * @return
     * @throws Exception
     */
    public synchronized List<TextInterval> parseWordIntervals(String text)
	    throws Exception {

	/*
	 * Parse string into word intervals.
	 */
	wordIntervals = new ArrayList<TextInterval>();

	/*
	 * If single character, determine if it is a punctuation character. If
	 * so, ignore and return empty collection. Otherwise, provide one
	 * TextInterval.
	 */
	if (nlpUtil.isPunctOrDelimChar(text)) {
	    return wordIntervals;
	}

	int wordBeginTextPos = 0;
	int wordEndTextPos = text.length() - 1;
	// find last character position in text
	int lastCharPos = text.length() - 1;
	char lastChar = text.charAt(lastCharPos);
	try {
	    if (wordDelimChars.contains(lastChar)
		    || lastChar == NlpUtil.EndOfSentenceDelim) {
		do {
		    lastCharPos--;
		    /*
		     * if iterations results in a postion prior to start of the
		     * text, then no word exist in text, all are word delimiter
		     * characters.
		     */
		    if (lastCharPos < 0) {
			return wordIntervals;
		    }
		    lastChar = text.charAt(lastCharPos);
		} while ((wordDelimChars.contains(lastChar)
			|| lastChar == NlpUtil.EndOfSentenceDelim)
			&& lastChar >= 0);
	    }
	} catch (Exception ex) {
	    String errMsg = String
		    .format("Error parsing word for string '%1$s' ", text);
	    Exception errEx = new Exception(errMsg, ex);
	    errEx.printStackTrace();
	    throw errEx;
	}
	boolean noDelimAtEndOfSent = lastCharPos == (text.length() - 1);
	try {

	    do {

		char currentChar;

		// find begin pos of word (ignore spaces, etc)
		do {
		    currentChar = text.charAt(wordBeginTextPos);
		    if (wordDelimChars.contains(currentChar)) {
			wordBeginTextPos++;
		    } else {
			// if beginning char is not a delimiter, then done
			break;
		    }
		} while (wordBeginTextPos < lastCharPos);

		/*
		 * Scenario of two-character token and one of the characters is
		 * a word delimiter.
		 */
		if (wordBeginTextPos == lastCharPos) {
		    int endPos = lastCharPos == text.length() - 1 ? -1
			    : wordBeginTextPos + 1;
		    TextInterval interval = new TextInterval(0, lastCharPos,
			    endPos);
		    wordIntervals.add(interval);
		    return wordIntervals;
		}

		// find end position of word
		wordEndTextPos = wordBeginTextPos + 1;
		currentChar = text.charAt(wordEndTextPos);
		while (!wordDelimChars.contains(currentChar)
			&& wordEndTextPos < lastCharPos + 1) {
		    wordEndTextPos++;
		    if (noDelimAtEndOfSent
			    && wordEndTextPos == lastCharPos + 1) {
			wordEndTextPos = -1;
			break;
		    }
		    currentChar = text.charAt(wordEndTextPos);
		}

		// if (wordEndDocPos == (lastCharPos - 1)
		// && sentence.getEndDocPos() == -1)
		// wordEndDocPos = -1;
		TextInterval interval = new TextInterval(0, wordBeginTextPos,
			wordEndTextPos);
		wordIntervals.add(interval);

		if (wordEndTextPos == -1)
		    break;

		wordBeginTextPos = wordEndTextPos + 1;
		wordEndTextPos = wordBeginTextPos + 1;

	    } while (wordBeginTextPos < lastCharPos
		    && wordEndTextPos <= lastCharPos);

	} catch (Exception ex) {
	    String errMsg = String
		    .format("Error parsing word for string '%1$s' ", text);
	    Exception errEx = new Exception(errMsg, ex);
	    errEx.printStackTrace();
	    throw errEx;
	}

	return wordIntervals;

    }

    /**
     * Utility method for parsing sentence into words. Uses method similar to
     * parseTextToWords except it creates a list of {@link GoldStdSentWord} that
     * include document position data.
     * <p>
     * Note that this method must be run before words and other sentence
     * information is populated.
     * <p/>
     * <p>
     * Method includes identifying the lexical word associated with each parsed
     * word token, and adding these to the database if not found.
     * </p>
     * 
     * @param sentence
     * @throws Exception
     */
    public synchronized void parseSentenceIntoWords(GoldStdSentence sentence)
	    throws Exception {

	documentText = sentence.getDocText();
	this.sentence = sentence;
	sentenceText = sentence.getSentText();
	// this.sentenceChars = sentenceText.toCharArray();
	pmid = sentence.getPmid();
	List<GoldStdSentWord> words = new ArrayList<GoldStdSentWord>();
	wordsByBeginCharPos = new ConcurrentSkipListMap<Integer, GoldStdSentWord>();
	wordIntervals = null;

	// edge condition - single character word
	if (sentenceText.length() == 1) {
	    Character txtChar = sentenceText.charAt(0);
	    if (wordDelimChars.contains(txtChar)
		    || sentDelimChars.contains(txtChar)
		    || Character.isWhitespace(txtChar))
		return;
	    GoldStdSentWord word = new GoldStdSentWord(pmid, sentenceNbr, 0,
		    sentence.getBeginDocPos(), sentence.getEndDocPos(),
		    sentenceText);
	    words.add(word);
	    wordsByBeginCharPos.put(sentence.getBeginDocPos(), word);
	    return;
	}

	parseSentenceIntoWordIntervals();

	intervalUtil.adjustWordIntervals(sentence, documentText, wordIntervals);
	wordIntervals = intervalUtil.getIntervals();

	// loop through word intervals and extract words
	int wordNbr = 0;
	for (TextInterval interval : wordIntervals) {
	    // debugging
	    // if (sentence.getPmid() == 1380672 && sentence.getSentenceNbr() ==
	    // 2
	    // && wordNbr == 12) {
	    // System.out.println("Error condition");
	    // }
	    int beginDocPos = (int) interval.getBegin();
	    int endDocPos = (int) interval.getEnd();
	    String wordText;
	    if (endDocPos == -1)
		wordText = documentText.substring(beginDocPos);
	    else
		wordText = documentText.substring(beginDocPos, endDocPos);
	    wordText = removeWhiteSpaces(wordText);
	    if (isAcronym(wordText))
		wordText = removePeriods(wordText);
	    if (wordText == null || wordText.isEmpty())
		continue;
	    if (wordText.length() == 1) {
		char wordChar = wordText.charAt(0);
		if (wordDelimChars.contains(wordChar)
			|| wordChar == NlpUtil.EndOfSentenceDelim
			|| wordChar == NlpUtil.SemiColon_MetaMapFalseEosDelim)
		    continue;
	    }
	    GoldStdSentWord word = new GoldStdSentWord(pmid, sentence.getSentenceNbr(),
		    wordNbr, beginDocPos, endDocPos, wordText);
	    words.add(word);
	    wordsByBeginCharPos.put(beginDocPos, word);
	    wordNbr++;
	}

	words.sort(GoldStdSentWord.Comparators.ByPmidSentNbrWordNbr);
	sentence.setWords(words);

    }

    /**
     * 
     * Due to anomaly in the lexicon, get base word id first based upon matching
     * token param base word token. If base not found, then find variant (and
     * hence base word id) by matching token param with word variant token.
     * 
     * @param token
     * @param connPool
     * @return
     * @throws Exception
     */
    public static int getBaseWordId(String token, JdbcConnectionPool connPool)
	    throws Exception {

	int baseWordId = 0;

	try (Connection conn = connPool.getConnection()) {
	    baseWordId = getBaseWordId(token, conn);
	}

	return baseWordId;
    }

    /**
     * 
     * Due to anomaly in the lexicon, get base word id first based upon matching
     * token param base word token. If base not found, then find variant (and
     * hence base word id) by matching token param with word variant token.
     * 
     * @param token
     * @param conn
     * @return
     * @throws Exception
     */
    public static int getBaseWordId(String token, Connection conn)
	    throws Exception {

	int baseWordId = 0;

	try (PreparedStatement getBaseForTokenQry = conn
		.prepareStatement(getBaseForTokenSql)) {
	    try (PreparedStatement getBaseFromVarForTokenQry = conn
		    .prepareStatement(getBaseFromVarForTokenSql)) {
		getBaseForTokenQry.setString(1, token);
		try (ResultSet rs = getBaseForTokenQry.executeQuery()) {
		    if (rs.next()) {
			baseWordId = rs.getInt(1);
		    }
		}
		if (baseWordId == 0) {
		    getBaseFromVarForTokenQry.setString(1, token);
		    try (ResultSet rs = getBaseFromVarForTokenQry
			    .executeQuery()) {
			if (rs.next()) {
			    baseWordId = rs.getInt(1);
			}
		    }
		}
	    }
	}

	return baseWordId;
    }

    /**
     * Retrieve variant record ID with matching token.
     * 
     * @param token
     * @param conn
     * @return
     * @throws Exception
     */
    public static int getVariantWordId(String token, Connection conn)
	    throws Exception {

	int variantWordId = 0;

	try (PreparedStatement getVariantIdForTokenQry = conn
		.prepareStatement(getVariantIdForTokenSql)) {
	    getVariantIdForTokenQry.setString(1, token);
	    try (ResultSet rs = getVariantIdForTokenQry.executeQuery()) {
		if (rs.next()) {
		    variantWordId = rs.getInt(1);
		}
	    }
	}

	return variantWordId;
    }

    /**
     * Private method to parse the text from a {@link GoldStdSentence} object into
     * intervals corresponding to words in the text.
     * 
     * @throws Exception
     */
    private void parseSentenceIntoWordIntervals() throws Exception {

	wordIntervals = new ArrayList<TextInterval>();

	int wordBeginDocPos = sentence.getBeginDocPos();
	int wordEndDocPos = sentence.getEndDocPos();
	// find last character position in sentence
	int lastCharPos = sentence.getEndDocPos();
	if (lastCharPos == -1)
	    lastCharPos = documentText.length() - 1;
	char lastChar = documentText.charAt(lastCharPos);
	if (wordDelimChars.contains(lastChar)
		|| lastChar == NlpUtil.EndOfSentenceDelim) {
	    do {
		lastCharPos--;
		lastChar = documentText.charAt(lastCharPos);
	    } while (wordDelimChars.contains(lastChar)
		    || lastChar == NlpUtil.EndOfSentenceDelim);
	}
	boolean noDelimAtEndOfSent = lastCharPos == (documentText.length() - 1);
	try {

	    do {

		char currentChar = documentText.charAt(wordBeginDocPos);

		// find begin pos of word (ignore spaces, etc)
		while (wordDelimChars.contains(currentChar)
			&& wordBeginDocPos <= lastCharPos) {
		    wordBeginDocPos++;
		    currentChar = documentText.charAt(wordBeginDocPos);
		}

		// find end pos of word
		wordEndDocPos = wordBeginDocPos + 1;
		currentChar = documentText.charAt(wordEndDocPos);
		while (!wordDelimChars.contains(currentChar)
			&& wordEndDocPos < lastCharPos + 1) {
		    wordEndDocPos++;
		    if (noDelimAtEndOfSent
			    && wordEndDocPos == lastCharPos + 1) {
			wordEndDocPos = -1;
			break;
		    }
		    currentChar = documentText.charAt(wordEndDocPos);
		}

		// if (wordEndDocPos == (lastCharPos - 1)
		// && sentence.getEndDocPos() == -1)
		// wordEndDocPos = -1;
		TextInterval interval = new TextInterval(pmid, wordBeginDocPos,
			wordEndDocPos);
		wordIntervals.add(interval);

		if (wordEndDocPos == -1)
		    break;

		wordBeginDocPos = wordEndDocPos + 1;
		wordEndDocPos = wordBeginDocPos + 1;

	    } while (wordBeginDocPos < lastCharPos
		    && wordEndDocPos <= lastCharPos);

	} catch (Exception ex) {
	    String errMsg = String.format(
		    "Error parsing word for pmid %1$d | sentence nbr %2$d "
			    + "| begin doc pos %3$,d | end doc pos %4$d | text len %5$,d "
			    + "| text %6$s ",
		    sentence.getPmid(), sentence.getSentenceNbr(),
		    sentence.getBeginDocPos(), sentence.getEndDocPos(),
		    documentText.length(), sentence.getSentText());
	    Exception errEx = new Exception(errMsg, ex);
	    errEx.printStackTrace();
	    throw errEx;
	}
    }

    private String removeWhiteSpaces(String text) {
	char[] originalText = text.toCharArray();
	StringBuilder stringBuilder = new StringBuilder();
	for (int i = 0; i < originalText.length; i++) {
	    if (Character.isWhitespace(originalText[i]))
		continue;
	    stringBuilder.append(originalText[i]);
	}
	return stringBuilder.toString();
    }

    private boolean isAcronym(String wordText) {
	if (wordText == null || wordText.isEmpty())
	    return false;
	// minimum length that enables a two character acronym is four
	if (wordText.length() < 4)
	    return false;
	/*
	 * Iterate through the word text to see if it follows the pattern of a
	 * formatted acronym, where a decimal then space is inserted between
	 * each letter in the acronym.
	 */
	// find first non-blank char
	int currentCharPos = 0;
	int lastCharPos = wordText.length() - 1;
	char currentChar = Character.UNASSIGNED;
	while (currentCharPos <= lastCharPos) {
	    currentChar = wordText.charAt(currentCharPos);
	    if (Character.isWhitespace(currentChar)) {
		currentCharPos++;
		continue;
	    }
	    break;
	}
	// need at least four characters for an acronym
	if ((wordText.length() - currentCharPos) < 4)
	    return false;
	/*
	 * Iterate looking for repeating upper case letter/decimal/space
	 * pattern. The above search positioned cursor on first non-blank
	 * character, so the cycle must begin with an upper case character. Must
	 * get thru at least one pattern cycle before acronym can end (otherwise
	 * will extract just one letter)
	 */
	int nbrPatternCycles = 1;
	do {
	    currentChar = wordText.charAt(currentCharPos);
	    // first char in cycle is letter; must be upper case
	    if (!Character.isUpperCase(currentChar))
		return false;
	    // if last char in text, and at least one pattern cycle, then is end
	    // of acronym
	    if (currentCharPos == lastCharPos && nbrPatternCycles > 1)
		return true;
	    currentCharPos++;
	    currentChar = wordText.charAt(currentCharPos);
	    // acronym can also end also with a blank char, but nothing else
	    if (currentCharPos == lastCharPos && nbrPatternCycles > 1) {
		if (Character.isWhitespace(currentChar))
		    return true;
		else
		    return false;
	    }
	    currentCharPos++;
	    if (currentCharPos > currentCharPos)
		return false;
	    currentChar = wordText.charAt(currentCharPos);
	    // since not at end, next position must be a period
	    if (currentChar != '.')
		return false;
	    currentCharPos++;
	    if (currentCharPos > currentCharPos)
		return false;
	    currentChar = wordText.charAt(currentCharPos);
	    // character after period must be a blank
	    if (!Character.isWhitespace(currentChar))
		return false;
	    currentCharPos++;
	    nbrPatternCycles++;
	} while (currentCharPos < wordText.length());
	return false;
    }

    private String removePeriods(String text) {
	char[] originalText = text.toCharArray();
	StringBuilder stringBuilder = new StringBuilder();
	for (int i = 0; i < originalText.length - 1; i++) {
	    if (originalText[i] == '.')
		continue;
	    stringBuilder.append(originalText[i]);
	}
	return stringBuilder.toString();
    }

    /**
     * This map is used to ensure that words can be retrieved by the sentence
     * position of the word's first char in the text. Map keys are automatically
     * sorted to enhance ease of insertion and retrieval.
     * 
     * @return SortedMap: key is Integer, value is Word
     */
    public SortedMap<Integer, GoldStdSentWord> getWordsByBeginCharPos() {
	return wordsByBeginCharPos;
    }

    public List<TextInterval> getWordIntervals() {
	return wordIntervals;
    }

    public Set<Character> getWordDelimChars() {
	return wordDelimChars;
    }

    public Set<Character> getSentDelimChars() {
	return sentDelimChars;
    }

    public NlpUtil getNlpUtil() {
	return nlpUtil;
    }

}

package edu.mst.db.text.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import edu.mst.db.nlm.goldstd.corpora.GoldStdSentence;

public class TextIntervalUtil {

    private List<TextInterval> intervals;

    private int pmid;
    private String documentText;
    private NlpUtil nlpUtil = new NlpUtil();
    private int lastIntervalPos;

    /**
     * Modifies word intervals to account for odd formats using the period
     * symbol, such as NCBI decimals, initials, acronyms, etc.
     * 
     * @param sentence
     * @param intervals
     */
    public void adjustWordIntervals(GoldStdSentence sentence, String documentText,
	    List<TextInterval> intervals) {
	this.pmid = sentence.getPmid();
	this.documentText = documentText;
	this.intervals = intervals;
	this.lastIntervalPos = intervals.size() - 1;

	adjustForNcbiFormat();
	adjustForAcrAbbrev();
	adjustForMetaMapFalseEOS();
	cleanupWordBeginEndPos();
    }

    private void cleanupWordBeginEndPos() {

	Set<Character> wordDelim = nlpUtil.getSentence2WordsDelimChars();

	List<TextInterval> revisedIntervals = new ArrayList<TextInterval>();

	boolean intervalRevised = false;
	for (TextInterval interval : intervals) {

	    // find best beginning of interval
	    long revisedBeginDocPos = interval.getBegin();
	    char currentChar = documentText.charAt((int) revisedBeginDocPos);
	    while (wordDelim.contains(currentChar)
		    && revisedBeginDocPos < interval.getEnd()) {
		revisedBeginDocPos++;
		currentChar = documentText.charAt((int) revisedBeginDocPos);
	    }

	    // find best ending of interval
	    if (interval.getEnd() == -1L) {
		if (revisedBeginDocPos != interval.getBegin()) {
		    TextInterval newInterval = new TextInterval(pmid,
			    revisedBeginDocPos, -1L);
		    revisedIntervals.add(newInterval);
		    intervalRevised = true;
		} else
		    revisedIntervals.add(interval);
		break;
	    }
	    long revisedEndDocPos = interval.getEnd();
	    currentChar = documentText.charAt((int) revisedEndDocPos - 1);
	    while (wordDelim.contains(currentChar)
		    && revisedEndDocPos > revisedBeginDocPos) {
		revisedEndDocPos--;
		currentChar = documentText.charAt((int) revisedEndDocPos - 1);
	    }

	    if (revisedBeginDocPos != interval.getBegin()
		    || revisedEndDocPos != interval.getEnd()) {
		TextInterval newInterval = new TextInterval(pmid,
			revisedBeginDocPos, revisedEndDocPos);
		revisedIntervals.add(newInterval);
		intervalRevised = true;
	    } else
		revisedIntervals.add(interval);

	}

	if (intervalRevised)
	    intervals = revisedIntervals;

    }

    /**
     * Modifies sentence intervals to account for odd formats using the period
     * symbol, such as NCBI decimals, initials, acronyms, etc.
     * 
     * @param pmid
     * @param documentText
     * @param intervals
     */
    public void adjustSentenceIntervals(int pmid, String documentText,
	    List<TextInterval> intervals) {
	this.pmid = pmid;
	this.documentText = documentText;
	this.intervals = intervals;
	this.lastIntervalPos = intervals.size() - 1;

	adjustForNcbiFormat();
	adjustForAcrAbbrev();
	adjustForMetaMapFalseEOS();
	adjustLastSentence();
    }

    /**
     * Combine text intervals when the NCBI decimal format is mistaken for an
     * end-of-sentence causing that sentence to be split
     */
    private void adjustForNcbiFormat() {

	List<TextInterval> adjustedIntervals = new ArrayList<TextInterval>();
	int beginComboInterval = -1;
	int endComboInterval = -1;
	for (int i = 0; i <= lastIntervalPos; i++) {

	    TextInterval thisInterval = intervals.get(i);
	    int nextIntervalPos = i + 1;
	    int thisBeginPos = (int) thisInterval.getBegin();
	    int thisEndPos = (int) thisInterval.getEnd();
	    boolean isIntervalComboStarted = beginComboInterval != -1;

	    boolean isCurrentIntervalBeginOfDec = isPossibleNCBIBegin(i);
	    if (isCurrentIntervalBeginOfDec && i != lastIntervalPos) {
		boolean isNextIntervalEndOfDec = isPossibleNCBIEnd(nextIntervalPos);
		if (isNextIntervalEndOfDec) {
		    if (isIntervalComboStarted) {
			// extend interval combo
			endComboInterval = (int) intervals.get(nextIntervalPos)
				.getEnd();
			continue;
		    } else {
			// create interval combo
			beginComboInterval = thisBeginPos;
			endComboInterval = (int) intervals.get(nextIntervalPos)
				.getEnd();
			continue;
		    }
		} else {
		    /*
		     * Current interval indicates beginning of decimal, but,
		     * next interval does not indicate end of decimal. Assume a
		     * false positive for beginning of decimal.
		     */
		    if (isIntervalComboStarted) {
			// save combo interval
			endComboInterval = thisEndPos;
			TextInterval comboInterval = new TextInterval(pmid,
				beginComboInterval, endComboInterval);
			adjustedIntervals.add(comboInterval);
			beginComboInterval = -1;
			endComboInterval = -1;
			continue;
		    } else {
			// No NCBI decimal found, save current interval as
			// sentence
			adjustedIntervals.add(thisInterval);
			continue;
		    }
		}
	    } else {
		if (isIntervalComboStarted) {
		    // save combo interval
		    endComboInterval = thisEndPos;
		    TextInterval comboInterval = new TextInterval(pmid,
			    beginComboInterval, endComboInterval);
		    adjustedIntervals.add(comboInterval);
		    beginComboInterval = -1;
		    endComboInterval = -1;
		    continue;
		} else {
		    // No NCBI decimal found, save current interval as sentence
		    adjustedIntervals.add(thisInterval);
		    continue;
		}
	    }
	}
	intervals = adjustedIntervals;
	lastIntervalPos = intervals.size() - 1;
    }

    /**
     * Determine whether or not this text interval has the beginning of an NCBI
     * decimal at the end of the interval
     * 
     * @param intervalNbr
     * @return
     */
    private boolean isPossibleNCBIBegin(int intervalNbr) {
	TextInterval interval = intervals.get(intervalNbr);
	// need length at least 2 characters long
	if (interval.getEnd() - interval.getBegin() < 2L)
	    return false;
	int lastCharPos = (int) interval.getEnd() - 1;
	// can't be begin if last sentence interval in doc
	if (lastCharPos < 0)
	    return false;
	char lastChar = documentText.charAt(lastCharPos);
	// if white space then iterate backwards to find position of
	// first non-white char
	if (Character.isWhitespace(lastChar)) {
	    while (Character.isWhitespace(lastChar)) {
		lastCharPos -= 1;
		lastChar = documentText.charAt(lastCharPos);
	    }
	}
	if (lastCharPos == 0)
	    System.out.println("Error condition");
	if (lastChar == '.') {
	    int nextToLastCharPos = lastCharPos - 1;
	    if (nextToLastCharPos >= 0) {
		char nextToLastChar = documentText.charAt(lastCharPos - 1);
		if (Character.isDigit(nextToLastChar))
		    return true;
	    }
	}
	return false;
    }

    private boolean isPossibleNCBIEnd(int intervalNbr) {
	TextInterval interval = intervals.get(intervalNbr);
	int sentenceBeginDocPos = (int) interval.getBegin();
	char sentenceChar = documentText.charAt(sentenceBeginDocPos);
	boolean isDigit = Character.isDigit(sentenceChar);
	if (!isDigit)
	    return false;
	// check to see if this is an integer
	int pointer = sentenceBeginDocPos;
	while (isDigit) {
	    pointer--;
	    sentenceChar = documentText.charAt(pointer);
	    isDigit = Character.isDigit(sentenceChar);
	}
	if (Character.isWhitespace(sentenceChar))
	    return true;
	return false;
    }

    private void adjustForAcrAbbrev() {

	List<TextInterval> adjustedIntervals = new ArrayList<TextInterval>();
	int beginComboInterval = -1;
	int endComboInterval = -1;
	for (int i = 0; i <= lastIntervalPos; i++) {

	    TextInterval thisInterval = intervals.get(i);
	    int nextIntervalPos = i + 1;
	    int thisBeginPos = (int) thisInterval.getBegin();
	    int thisEndPos = (int) thisInterval.getEnd();
	    boolean isIntervalComboStarted = beginComboInterval != -1;

	    boolean isCurrentIntervalBeginOfAcronym = isPossibleAcronymBegin(i);
	    if (isCurrentIntervalBeginOfAcronym && i != lastIntervalPos) {
		boolean isNextIntervalEndOfAcronym = isPossibleAcronymEnd(nextIntervalPos);
		if (isNextIntervalEndOfAcronym) {
		    if (isIntervalComboStarted) {
			// extend interval combo
			endComboInterval = (int) intervals.get(nextIntervalPos)
				.getEnd();
			continue;
		    } else {
			// create interval combo
			beginComboInterval = thisBeginPos;
			endComboInterval = (int) intervals.get(nextIntervalPos)
				.getEnd();
			continue;
		    }
		} else {
		    /*
		     * Current interval indicates beginning of decimal, but,
		     * next interval does not indicate end of decimal. Assume a
		     * false positive for beginning of decimal.
		     */
		    if (isIntervalComboStarted) {
			// save combo interval
			endComboInterval = thisEndPos;
			TextInterval comboInterval = new TextInterval(pmid,
				beginComboInterval, endComboInterval);
			adjustedIntervals.add(comboInterval);
			beginComboInterval = -1;
			endComboInterval = -1;
			continue;
		    } else {
			// No NCBI decimal found, save current interval as
			// sentence
			adjustedIntervals.add(thisInterval);
			continue;
		    }
		}
	    } else {
		if (isIntervalComboStarted) {
		    // save combo interval
		    endComboInterval = thisEndPos;
		    TextInterval comboInterval = new TextInterval(pmid,
			    beginComboInterval, endComboInterval);
		    adjustedIntervals.add(comboInterval);
		    beginComboInterval = -1;
		    endComboInterval = -1;
		    continue;
		} else {
		    // No NCBI decimal found, save current interval as sentence
		    adjustedIntervals.add(thisInterval);
		    continue;
		}
	    }
	}
	intervals = adjustedIntervals;
	lastIntervalPos = intervals.size() - 1;
    }

    private boolean isPossibleAcronymBegin(int intervalNbr) {
	// can't be a beginning of acronym if last sentence interval in doc
	if (intervalNbr == (intervals.size() - 1))
	    return false;
	TextInterval interval = intervals.get(intervalNbr);
	// need length at least 3 characters long
	if (interval.getEnd() - interval.getBegin() < 3L)
	    return false;
	int lastCharPos = (int) interval.getEnd() - 1;
	char lastChar = documentText.charAt(lastCharPos);
	// if white space then iterate backwards to find position of
	// first non-white char
	if (Character.isWhitespace(lastChar)) {
	    while (Character.isWhitespace(lastChar)) {
		lastCharPos -= 1;
		lastChar = documentText.charAt(lastCharPos);
	    }
	}
	/*
	 * The beginning of an acronym is identified by a space followed by a
	 * single upper case character followed by a period. Make sure positions
	 * for extracting these characters does not throw an error.
	 */
	if (lastChar != '.' || (lastCharPos - 2) < 0)
	    return false;
	char upperCaseChar = documentText.charAt(lastCharPos - 1);
	char spaceChar = documentText.charAt(lastCharPos - 2);
	if (Character.isUpperCase(upperCaseChar)
		&& Character.isWhitespace(spaceChar))
	    return true;
	return false;
    }

    private boolean isPossibleAcronymEnd(int intervalNbr) {
	try {
	    TextInterval interval = intervals.get(intervalNbr);
	    int sentenceBeginDocPos = (int) interval.getBegin();
	    /*
	     * The ending of an acronym is identified by an upper case alpha
	     * followed by either a period or a space. Make sure positions for
	     * extracting these characters does not throw an error.
	     */
	    // check to see first char is upper case letter followed by a period
	    // and
	    // space
	    char firstChar = documentText.charAt(sentenceBeginDocPos);
	    if (!Character.isUpperCase(firstChar))
		return false;
	    char secondChar = documentText.charAt(sentenceBeginDocPos + 1);
	    if (secondChar != '.' || Character.isWhitespace(secondChar))
		return false;
	    if (secondChar == '.') {
		char spaceChar = documentText.charAt(sentenceBeginDocPos + 2);
		if (!Character.isWhitespace(spaceChar))
		    return false;
	    }
	    return false;
	} catch (Exception ex) {
	    ex.printStackTrace();
	    throw ex;
	}
    }

    private void adjustForMetaMapFalseEOS() {

	List<TextInterval> adjustedIntervals = new ArrayList<TextInterval>();
	int beginComboInterval = -1;
	// int endComboInterval = -1;
	for (int i = 0; i <= lastIntervalPos; i++) {

	    TextInterval thisInterval = intervals.get(i);
	    int thisBeginPos = (int) thisInterval.getBegin();
	    int thisEndPos = (int) thisInterval.getEnd();
	    boolean isIntervalComboStarted = beginComboInterval != -1;

	    int lastCharPos;
	    if (thisEndPos == -1)
		lastCharPos = documentText.length() - 1;
	    else
		lastCharPos = thisEndPos - 1;
	    boolean currentIntervalHasFalseEOS = documentText
		    .charAt(lastCharPos) == NlpUtil.SemiColon_MetaMapFalseEosDelim;

	    if (currentIntervalHasFalseEOS) {
		if (isIntervalComboStarted) {
		    continue;
		} else {
		    beginComboInterval = thisBeginPos;
		    continue;
		}
	    } else {
		if (isIntervalComboStarted) {
		    TextInterval combinedInterval = new TextInterval(pmid,
			    beginComboInterval, thisEndPos);
		    adjustedIntervals.add(combinedInterval);
		    beginComboInterval = -1;
		    continue;
		} else {
		    adjustedIntervals.add(thisInterval);
		    continue;
		}
	    }

	}
	intervals = adjustedIntervals;
	lastIntervalPos = intervals.size() - 1;
    }

    /**
     * Note - this must the last adjustment to the sentences. Combining
     * sentences for NCBI formatted decimals, etc. will combine sentences
     * produced by MetaMap, and this method only applies to the last sentence in
     * the document.
     */
    private void adjustLastSentence() {
	TextInterval lastInterval = intervals.get(lastIntervalPos);
	int lastCharPos = documentText.length() - 1;
	int currentCharPos = lastCharPos;
	char lastChar = documentText.charAt(currentCharPos);
	do {
	    currentCharPos--;
	    lastChar = documentText.charAt(currentCharPos);
	} while (lastChar == NlpUtil.EndOfSentenceDelim
		|| lastChar == NlpUtil.SemiColon_MetaMapFalseEosDelim
		|| Character.isWhitespace(lastChar));
	// determine if multiple end of sentence chars exist; if so, adjust end
	// position for last sentence in document
	if (currentCharPos < lastCharPos - 1) {
	    // create replacement
	    TextInterval replacement = new TextInterval(pmid,
		    lastInterval.getBegin(), currentCharPos + 2);
	    // delete then replace last sentence
	    intervals.remove(lastIntervalPos);
	    intervals.add(replacement);
	}
    }

    public List<TextInterval> getIntervals() {
	return intervals;
    }

}

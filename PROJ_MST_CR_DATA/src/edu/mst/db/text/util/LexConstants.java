package edu.mst.db.text.util;

public abstract class LexConstants {

    public static final String lvgPropertiesFileName = "lvg.properties";
    public static final String lvgPropertiesDirLinux = "/home/gjs/umls/lvg2017/data/config/";
    public static final char fieldDelim = '|';


}

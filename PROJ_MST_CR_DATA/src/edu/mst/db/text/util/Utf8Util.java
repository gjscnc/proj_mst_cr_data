/**
 * 
 */
package edu.mst.db.text.util;

import java.util.ArrayList;
import java.util.List;

/**
 * Utility methods for conversion of UTF-8 to text
 * 
 * @author George
 *
 */
public class Utf8Util {

    public static final char separatorChar = ',';

    /**
     * Converts sequence of UTF-8 codes to a string
     * 
     * @param utf8Sequence
     * @return
     */
    public String utf8ToString(int[] utf8Sequence) {

	StringBuilder builder = new StringBuilder();

	for (int i = 0; i < utf8Sequence.length; i++) {
	    char c = (char) utf8Sequence[i];
	    builder.append(c);
	}

	return builder.toString();
    }

    /**
     * This version of method takes a MetaMap UTF-8 array of strings, in string
     * form, and extracts a Java String object from it.
     * 
     * @param utf8ArrayAsStr
     * @return
     */
    public String utf8ToString(String utf8ArrayAsStr) {
	int[] utf8Codes = metaMapUTF8ArrayToInt(utf8ArrayAsStr);
	return utf8ToString(utf8Codes);
    }

    /**
     * Converts the array of UTF-8 values from string to integer values. MetaMap
     * API provides acronym and abbreviation strings as a character string. Need
     * to parse the array and convert each char to an integer.
     * 
     * @param utf8ArrayAsStr
     * @return
     */
    public int[] metaMapUTF8ArrayToInt(String utf8ArrayAsStr) {

	int[] utf8Codes = null;
	String trimStr = utf8ArrayAsStr.trim();
	// remove first char '[', and last char ']'
	trimStr = trimStr.substring(1);
	trimStr = trimStr.substring(0, trimStr.length() - 1);
	int cursorPos = 0;
	int nextPos = 0;
	List<Integer> utf8CodeList = new ArrayList<Integer>();
	while (nextPos > -1) {
	    nextPos = trimStr.indexOf(separatorChar, cursorPos);
	    String utf8CodeStr = null;
	    if (nextPos == -1)
		utf8CodeStr = trimStr.substring(cursorPos);
	    else
		utf8CodeStr = trimStr.substring(cursorPos, nextPos);
	    int utf8code = Integer.valueOf(utf8CodeStr);
	    utf8CodeList.add(utf8code);
	    cursorPos = nextPos + 1;
	}
	int arraySize = utf8CodeList.size();
	utf8Codes = new int[arraySize];
	for (int i = 0; i < arraySize; i++) {
	    utf8Codes[i] = utf8CodeList.get(i);
	}
	return utf8Codes;
    }

}

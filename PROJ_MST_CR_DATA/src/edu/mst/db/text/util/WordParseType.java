/**
 * Enumerates the two applications of word parsing: 
 *    parsing concept names into words, or parsing document sentences into words
 */
package edu.mst.db.text.util;

/**
 * @author gjs
 *
 */
public enum WordParseType {

    CONCEPT_NAME, SENTENCE;

}

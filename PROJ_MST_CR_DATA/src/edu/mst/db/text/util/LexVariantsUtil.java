/**
 * 
 */
package edu.mst.db.text.util;

import java.util.HashSet;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.Vector;

import edu.mst.db.lexicon.StopWords;
import edu.mst.db.lexicon.acrabbrev.AcrAbbrev;
import gov.nih.nlm.nls.lvg.Api.LvgApi;
import gov.nih.nlm.nls.lvg.Api.NormApi;
import gov.nih.nlm.nls.lvg.Flows.ToAntiNorm;
import gov.nih.nlm.nls.lvg.Flows.ToExpansions;
import gov.nih.nlm.nls.lvg.Flows.ToFruitfulVariantsDb;
import gov.nih.nlm.nls.lvg.Flows.ToRecursiveSynonyms;
//import gov.nih.nlm.nls.lvg.Flows.ToRecursiveSynonyms;
import gov.nih.nlm.nls.lvg.Flows.ToSpellingVariants;
import gov.nih.nlm.nls.lvg.Flows.ToSynonyms;
import gov.nih.nlm.nls.lvg.Lib.LexItem;

/**
 * Utilities for generating lexical variants, e.g., spelling variants, synonyms,
 * and for identifying acronyms/abbreviations.
 * 
 * @author gjs
 *
 */
public class LexVariantsUtil implements AutoCloseable {

    private static final String lvgPropsFilePath = LexConstants.lvgPropertiesDirLinux
	    .concat(LexConstants.lvgPropertiesFileName);
    private LvgApi lvgApi = new LvgApi(lvgPropsFilePath);
    private NormApi normApi = new NormApi(lvgPropsFilePath);
    private NlpUtil nlpUtil = new NlpUtil();
    private StopWords stopWords = null;
    /**
     * Flag for performing mutations when extracting variants. This flag is set
     * in the constructor.
     */
    private boolean isMutateVariants = false;

    public LexVariantsUtil(boolean isMutateVariants, StopWords stopWords) {
	this.isMutateVariants = isMutateVariants;
	this.stopWords = stopWords;
    }

    public SortedSet<String> getNonFruitfulVariants(String token,
	    LexVariantsSynType synVariantsType) throws Exception {

	SortedSet<String> variantTokens = new TreeSet<String>();

	LexItem lexItemInput = new LexItem(token);

	/**
	 * Spelling variants
	 */
	Vector<LexItem> spellVariants = ToSpellingVariants.Mutate(lexItemInput,
		lvgApi.GetConnection(), false, isMutateVariants);
	for (LexItem spellVariant : spellVariants) {
	    if (nlpUtil.isParsable(spellVariant.GetTargetTerm())) {
		continue;
	    }
	    variantTokens.add(spellVariant.GetTargetTerm().toLowerCase());
	}

	/**
	 * Normalization variants
	 */
	Vector<String> norms = normApi.Mutate(token);
	for (String norm : norms) {
	    if (nlpUtil.isParsable(norm)) {
		continue;
	    }
	    variantTokens.add(norm);
	}

	/**
	 * Anti-normalization variants
	 */
	Vector<LexItem> antiNorms = ToAntiNorm.Mutate(lexItemInput,
		lvgApi.GetMaxTerm(), lvgApi.GetStopWords(),
		lvgApi.GetConnection(), lvgApi.GetInflectionTrie(),
		lvgApi.GetSymbolMap(), lvgApi.GetUnicodeMap(),
		lvgApi.GetLigatureMap(), lvgApi.GetDiacriticMap(),
		lvgApi.GetNonStripMap(), lvgApi.GetRemoveSTree(), false,
		isMutateVariants);
	for (LexItem antiNorm : antiNorms) {
	    if (nlpUtil.isParsable(antiNorm.GetTargetTerm())) {
		continue;
	    }
	    variantTokens.add(antiNorm.GetTargetTerm());
	}

	/**
	 * Synonym variants
	 */
	/**
	 * Synonym variants
	 */
	switch (synVariantsType) {
	case RECURSIVE_SYNONYMS:
	    Vector<LexItem> recursiveSynonyms = ToRecursiveSynonyms.Mutate(
		    lexItemInput, lvgApi.GetConnection(), false,
		    isMutateVariants, false);
	    for (LexItem synonym : recursiveSynonyms) {
		if (nlpUtil.isParsable(synonym.GetTargetTerm())) {
		    continue;
		}
		variantTokens.add(synonym.GetTargetTerm());
	    }
	    break;
	case SYNONYMS_NOT_RECURSIVE:
	    Vector<LexItem> synonyms = ToSynonyms.Mutate(lexItemInput,
		    lvgApi.GetConnection(), false, isMutateVariants);
	    for (LexItem synonym : synonyms) {
		if (nlpUtil.isParsable(synonym.GetTargetTerm())) {
		    continue;
		}
		variantTokens.add(synonym.GetTargetTerm());
	    }
	    break;
	}

	return variantTokens;
    }

    /**
     * One-pass extraction of variants associated with the provided tokens.
     * <p>
     * 
     * @apiNote No recursion - i.e., no variants of variants.
     * 
     * @param tokens
     * @param synVariantsType
     * @return
     * @throws Exception
     */
    public SortedSet<Variants> getVariants(SortedSet<String> tokens,
	    LexVariantsSynType synVariantsType) throws Exception {
	SortedSet<Variants> allVariants = new TreeSet<Variants>();
	Variants results = new Variants();
	for (String token : tokens) {
	    if (token == null || token.isBlank() || token.isEmpty()) {
		continue;
	    }
	    if (nlpUtil.isParsable(token)) {
		results.parsableTokens.add(token);
		continue;
	    }
	    Variants resultsForToken = getVariants(token,
		    synVariantsType);
	    allVariants.add(resultsForToken);
	}
	return allVariants;
    }

    /**
     * Retrieves variants for token.
     * <p>
     * Includes the LVG method for extracting 'fruitful' variants, a more
     * comprehensive set of variants.
     * <p>
     * If the attribute {@link #isMutateVariants} set to true, then performs
     * mutations for identifying variants for all variant types, <b>except
     * for</b> synonyms.
     * 
     * @param token
     * @param synVariantsType
     * @return
     * @throws Exception
     */
    public Variants getVariants(String token,
	    LexVariantsSynType synVariantsType) throws Exception {

	Variants result = new Variants();
	result.token = token;

	SortedSet<String> variants = new TreeSet<String>();

	if (nlpUtil.isParsable(token)) {
	    result.parsableTokens.add(token);
	    // return result;
	}
	if (stopWords.getStopWordTokens().contains(token)) {
	    result.stopWords.add(token);
	    // return result;
	}
	if (token.length() == 1) {
	    if (nlpUtil.isLetterOrNumber(token.charAt(0))) {
		result.lettersAndNbrs.add(token);
		// return result;
	    }
	    if (nlpUtil.isPunctOrDelimChar(token)) {
		result.punctations.add(token);
		// return result;
	    }
	}

	addVariant(token, variants);

	LexItem lexItemInput = new LexItem(token);

	/**
	 * Spelling variants
	 */
	Vector<LexItem> spellVariants = ToSpellingVariants.Mutate(lexItemInput,
		lvgApi.GetConnection(), false, isMutateVariants);
	for (LexItem spellVariant : spellVariants) {
	    if (nlpUtil.isParsable(spellVariant.GetTargetTerm())) {
		result.parsableTokens.add(spellVariant.GetTargetTerm());
		continue;
	    }
	    addVariant(spellVariant.GetTargetTerm(), variants);
	}

	/**
	 * Normalization variants
	 */
	Vector<String> norms = normApi.Mutate(token);
	for (String norm : norms) {
	    if (nlpUtil.isParsable(norm)) {
		result.parsableTokens.add(norm);
		continue;
	    }
	    addVariant(norm, variants);
	}

	/**
	 * Anti-normalization variants
	 */
	Vector<LexItem> antiNorms = ToAntiNorm.Mutate(lexItemInput,
		lvgApi.GetMaxTerm(), lvgApi.GetStopWords(),
		lvgApi.GetConnection(), lvgApi.GetInflectionTrie(),
		lvgApi.GetSymbolMap(), lvgApi.GetUnicodeMap(),
		lvgApi.GetLigatureMap(), lvgApi.GetDiacriticMap(),
		lvgApi.GetNonStripMap(), lvgApi.GetRemoveSTree(), false,
		isMutateVariants);
	for (LexItem antiNorm : antiNorms) {
	    if (nlpUtil.isParsable(antiNorm.GetTargetTerm())) {
		result.parsableTokens.add(antiNorm.GetTargetTerm());
		continue;
	    }
	    addVariant(antiNorm.GetTargetTerm(), variants);
	}

	/**
	 * Synonym variants
	 */
	switch (synVariantsType) {
	case RECURSIVE_SYNONYMS:
	    Vector<LexItem> recursiveSynonyms = ToRecursiveSynonyms.Mutate(
		    lexItemInput, lvgApi.GetConnection(), false,
		    isMutateVariants, false);
	    for (LexItem synonym : recursiveSynonyms) {
		if (nlpUtil.isParsable(synonym.GetTargetTerm())) {
		    result.parsableTokens.add(synonym.GetTargetTerm());
		    continue;
		}
		addVariant(synonym.GetTargetTerm(), variants);
	    }
	    break;
	case SYNONYMS_NOT_RECURSIVE:
	    Vector<LexItem> synonyms = ToSynonyms.Mutate(lexItemInput,
		    lvgApi.GetConnection(), false, isMutateVariants);
	    for (LexItem synonym : synonyms) {
		if (nlpUtil.isParsable(synonym.GetTargetTerm())) {
		    result.parsableTokens.add(synonym.GetTargetTerm());
		    continue;
		}
		addVariant(synonym.GetTargetTerm(), variants);
	    }
	    break;
	}

	/**
	 * Fruitful variants - the most available it appears from LVG
	 */
	Vector<LexItem> fruitfulVariants = ToFruitfulVariantsDb.Mutate(
		lexItemInput, lvgApi.GetConnection(), false, isMutateVariants);
	for (LexItem fruitfulVariant : fruitfulVariants) {
	    if (nlpUtil.isParsable(fruitfulVariant.GetTargetTerm())) {
		result.parsableTokens.add(fruitfulVariant.GetTargetTerm());
		continue;
	    }
	    addVariant(fruitfulVariant.GetTargetTerm(), variants);
	}

	result.variantTokens.addAll(variants);
	result.nbrVariantsExtracted = variants.size();

	AcrAbbrev acrAbbrev = getAcrAbbrev(token);
	if (acrAbbrev != null) {
	    result.acrAbbrevByToken.put(acrAbbrev.getToken(), acrAbbrev);
	}

	return result;
    }

    private void addVariant(String targetTerm, SortedSet<String> variants) {
	if (targetTerm == null || targetTerm.isBlank()
		|| targetTerm.isEmpty()) {
	    return;
	}
	variants.add(targetTerm.toLowerCase());
    }

    /**
     * Determines if token is an acronym/abbreviation, and if so returns an
     * {@link AcrAbbrev} object that contains the expanded form.
     * <p>
     * 
     * @apiNote If multiple acronyms/abbreviations found, only the first one in
     *          the list is returned.
     * @param token
     * @return
     */
    public AcrAbbrev getAcrAbbrev(String token) {
	AcrAbbrev acrAbbrev = null;
	Vector<LexItem> outs = ToExpansions.Mutate(new LexItem(token),
		lvgApi.GetConnection(), false, true);
	if (outs == null || outs.size() == 0) {
	    return acrAbbrev;
	}
	acrAbbrev = new AcrAbbrev();
	acrAbbrev.setToken(token);
	acrAbbrev.setExpanded(outs.elementAt(0).GetTargetTerm());
	acrAbbrev.setLexWordId(0);
	return acrAbbrev;
    }

    @Override
    public void close() throws Exception {
	lvgApi.CleanUp();
	lvgApi.CloseConnection(lvgApi.GetConnection());
	normApi.CleanUp();
    }

    public class Variants implements Comparable<Variants>{
	public String token = null;
	public SortedSet<String> variantTokens = new TreeSet<String>();
	public int nbrVariantsExtracted = 0;
	public SortedMap<String, AcrAbbrev> acrAbbrevByToken = new TreeMap<String, AcrAbbrev>();
	public Set<String> stopWords = new HashSet<String>();
	public Set<String> punctations = new HashSet<String>();
	public Set<String> lettersAndNbrs = new HashSet<String>();
	public Set<String> parsableTokens = new HashSet<String>();
	
	@Override
	public int compareTo(Variants variants) {
	    return token.compareTo(variants.token);
	}
	
	@Override
	public boolean equals(Object obj) {
	    if(obj instanceof Variants) {
		Variants variants = (Variants) obj;
		return token.equals(variants.token);
	    }
	    return false;
	}
	
	@Override
	public int hashCode() {
	    return token.hashCode();
	}
    }

}

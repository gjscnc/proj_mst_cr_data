package edu.mst.db.text.util;

import java.util.*;
import gov.nih.nlm.nls.lvg.Api.*;
import gov.nih.nlm.nls.lvg.Flows.ToAntiNorm;
import gov.nih.nlm.nls.lvg.Flows.ToExpansions;
import gov.nih.nlm.nls.lvg.Flows.ToFruitfulVariantsDb;
import gov.nih.nlm.nls.lvg.Flows.ToRecursiveSynonyms;
import gov.nih.nlm.nls.lvg.Flows.ToSpellingVariants;
import gov.nih.nlm.nls.lvg.Flows.ToSynonyms;
import gov.nih.nlm.nls.lvg.Lib.LexItem;

public class TestLvg {
    // test driver
    public static void main(String[] args) {
	// instantiate a LvgApi object by config file
	String lvgConfigFile = "/home/gjs/umls/lvg2017/data/config/lvg.properties";
	NormApi normApi = new NormApi(lvgConfigFile);
	LvgApi lvgApi = new LvgApi(lvgConfigFile);
	SortedMap<String, SortedSet<String>> variantsByToken = new TreeMap<String, SortedSet<String>>();
	NlpUtil nlpUtil = new NlpUtil();

	// Process the inflectional variants mutation
	try {
	    String[] inputTokens = new String[] { "left", "leave", "spinal",
		    "spine", "vertebrae", "vertebra", "Neoplasm", "COPD", "copd" };

	    System.out.printf("%nSpelling variants%n");
	    for (String inputToken : inputTokens) {
		System.out.printf("%n----Input token = %1$s---- %n",
			inputToken);
		LexItem in = new LexItem(inputToken);
		Vector<LexItem> outs = ToSpellingVariants.Mutate(in,
			lvgApi.GetConnection(), false, true);
		// PrintOut the Result
		if (!variantsByToken.containsKey(inputToken)) {
		    variantsByToken.put(inputToken, new TreeSet<String>());
		}
		for (LexItem out : outs) {
		    System.out.println(out.ToString());
		    String targetTerm = out.GetTargetTerm();
		    if (nlpUtil.isParsable(targetTerm)) {
			continue;
		    }
		    if (!variantsByToken.containsKey(inputToken)) {
			variantsByToken.put(inputToken, new TreeSet<String>());
		    }
		    variantsByToken.get(inputToken).add(targetTerm);
		}
	    }

	    System.out.printf("%nNormalization results%n");
	    for (String inputToken : inputTokens) {
		System.out.printf("%n----Input token = %1$s---- %n",
			inputToken);
		Vector<String> outs = normApi.Mutate(inputToken);
		// PrintOut the Result
		if (!variantsByToken.containsKey(inputToken)) {
		    variantsByToken.put(inputToken, new TreeSet<String>());
		}
		for (String out : outs) {
		    if (nlpUtil.isParsable(out)) {
			continue;
		    }
		    System.out.println(inputToken + "|" + out);
		    variantsByToken.get(inputToken).add(out);
		}
	    }

	    System.out.printf("%nAnti-normalization results%n");
	    for (String inputToken : inputTokens) {
		System.out.printf("%n----Input token = %1$s---- %n",
			inputToken);
		LexItem in = new LexItem(inputToken);
		Vector<LexItem> outs = ToAntiNorm.Mutate(in,
			lvgApi.GetMaxTerm(), lvgApi.GetStopWords(),
			lvgApi.GetConnection(), lvgApi.GetInflectionTrie(),
			lvgApi.GetSymbolMap(), lvgApi.GetUnicodeMap(),
			lvgApi.GetLigatureMap(), lvgApi.GetDiacriticMap(),
			lvgApi.GetNonStripMap(), lvgApi.GetRemoveSTree(), false,
			true);

		// PrintOut the Result
		for (LexItem out : outs) {
		    System.out.println(out.ToString());
		    String targetTerm = out.GetTargetTerm();
		    if (nlpUtil.isParsable(targetTerm)) {
			continue;
		    }
		    if (!variantsByToken.containsKey(inputToken)) {
			variantsByToken.put(inputToken, new TreeSet<String>());
		    }
		    variantsByToken.get(inputToken).add(targetTerm);
		}
	    }
	    System.out.printf("%nTo synonyms results%n");
	    for (String inputToken : inputTokens) {
		System.out.printf("%n----Input token = %1$s---- %n",
			inputToken);
		LexItem in = new LexItem(inputToken);
		Vector<LexItem> outs = ToSynonyms.Mutate(in,
			lvgApi.GetConnection(), false, true);
		for (LexItem out : outs) {
		    System.out.println(out.ToString());
		    String targetTerm = out.GetTargetTerm();
		    if (nlpUtil.isParsable(targetTerm)) {
			continue;
		    }
		    if (!variantsByToken.containsKey(inputToken)) {
			variantsByToken.put(inputToken, new TreeSet<String>());
		    }
		    variantsByToken.get(inputToken).add(targetTerm);
		}
	    }
	    System.out.printf("%nTo recursive synonyms%n");
	    for (String inputToken : inputTokens) {
		System.out.printf("%n----Input token = %1$s---- %n",
			inputToken);
		LexItem in = new LexItem(inputToken);
		Vector<LexItem> outs = ToRecursiveSynonyms.Mutate(in,
			lvgApi.GetConnection(), false, true, false);
		for (LexItem out : outs) {
		    System.out.println(out.ToString());
		    String targetTerm = out.GetTargetTerm();
		    if (nlpUtil.isParsable(targetTerm)) {
			continue;
		    }
		    if (!variantsByToken.containsKey(inputToken)) {
			variantsByToken.put(inputToken, new TreeSet<String>());
		    }
		    variantsByToken.get(inputToken).add(targetTerm);
		}
	    }
	    System.out.printf("%nFruitful variants%n");
	    for (String inputToken : inputTokens) {
		System.out.printf("%n----Input token = %1$s---- %n",
			inputToken);
		LexItem in = new LexItem(inputToken);
		Vector<LexItem> outs = ToFruitfulVariantsDb.Mutate(in,
			lvgApi.GetConnection(), false, true);
		for (LexItem out : outs) {
		    System.out.println(out.ToString());
		    String targetTerm = out.GetTargetTerm();
		    if (nlpUtil.isParsable(targetTerm)) {
			continue;
		    }
		    if (!variantsByToken.containsKey(inputToken)) {
			variantsByToken.put(inputToken, new TreeSet<String>());
		    }
		    variantsByToken.get(inputToken).add(targetTerm);
		}
	    }

	    System.out.printf("%nCompiled results for variants %n");
	    for (String inputToken : variantsByToken.keySet()) {
		System.out.printf("Input token: %1$s %n", inputToken);
		for (String outputToken : variantsByToken.get(inputToken)) {
		    System.out.printf("   %1$s %n", outputToken);
		}
	    }

	    System.out.printf("%nChecking for acronyms & abbreviations%n");
	    for (String inputToken : inputTokens) {
		System.out.printf("%n----Input token = %1$s---- %n",
			inputToken);
		LexItem in = new LexItem(inputToken);
		Vector<LexItem> outs = ToExpansions.Mutate(in,
			lvgApi.GetConnection(), false, true);
		if (outs == null || outs.size() == 0) {
		    System.out.printf("Token %1$s is not an acronym or abbreviation", inputToken);
		    continue;
		}
		for (LexItem out : outs) {
		    System.out.println(out.ToString());
		}
	    }

	    // clean up
	    normApi.CleanUp();
	    lvgApi.CleanUp();
	} catch (Exception e) {
	    System.err.println("** ERR: " + e.toString());
	    e.printStackTrace();
	}
    }
}

/*
=================================================================================
Copyright (c) 2010 RAI 
All rights reserved. 
=================================================================================
 */
package edu.mst.db.text.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Collections;
//import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;

import edu.mst.db.util.Constants;
import edu.mst.db.util.JdbcConnectionPool;
import edu.mst.db.util.ConnDbName;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.process.CoreLabelTokenFactory;
import edu.stanford.nlp.process.PTBTokenizer;

/**
 * @author RAI
 * 
 */
public class NlpUtil {

    public static String wordParseRegEx = "[.-]";

    public static final char EndOfSentenceDelim = '.';
    public static final char SemiColon_MetaMapFalseEosDelim = ';';

    private Set<Character> concept2WordsDelimChars;
    private Set<Character> sentence2WordsDelimChars;
    private Set<Character> sentenceRemoveChars;
    private Set<Character> sentenceDelimChars;
    private Set<Character> punctuationChars;

    public static final String CONCEPT_CATEGORY_DELIM = ",";

    public NlpUtil() {
	this.loadConcept2WordsDelimChars();
	this.loadSentence2WordsDelimChars();
	this.loadSentenceRemoveChars();
	this.loadSentenceDelimChars();
	this.loadPunctuationChars();
    }

    private void loadConcept2WordsDelimChars() {
	concept2WordsDelimChars = new HashSet<Character>();
	concept2WordsDelimChars.add('/');
	concept2WordsDelimChars.add('-');
	concept2WordsDelimChars.add(',');
	concept2WordsDelimChars.add(' ');
	concept2WordsDelimChars.add('(');
	concept2WordsDelimChars.add(')');
	concept2WordsDelimChars.add('[');
	concept2WordsDelimChars.add(']');
	concept2WordsDelimChars.add('{');
	concept2WordsDelimChars.add('}');
	concept2WordsDelimChars.add('\\');
	concept2WordsDelimChars.add(':');
	concept2WordsDelimChars.add(';');
	concept2WordsDelimChars.add('^');
	concept2WordsDelimChars.add('#');
	concept2WordsDelimChars.add('*');
	concept2WordsDelimChars.add('.');
	concept2WordsDelimChars.add('+');
	concept2WordsDelimChars.add('\'');
	concept2WordsDelimChars.add('\"');
	concept2WordsDelimChars.add('!');
	concept2WordsDelimChars.add('<');
	concept2WordsDelimChars.add('>');
    }

    private void loadSentence2WordsDelimChars() {
	sentence2WordsDelimChars = new HashSet<Character>();
	sentence2WordsDelimChars.addAll(concept2WordsDelimChars);
	sentence2WordsDelimChars.add('\n');
	sentence2WordsDelimChars.add('\t');
	sentence2WordsDelimChars.add('\r');
    }

    private void loadSentenceRemoveChars() {
	sentenceRemoveChars = new HashSet<Character>();
	sentenceRemoveChars.add('(');
	sentenceRemoveChars.add(')');
    }

    private void loadSentenceDelimChars() {
	sentenceDelimChars = new HashSet<Character>();
	// sentenceDelimChars.add(';');
	sentenceDelimChars.add('.');
    }

    private void loadPunctuationChars() {
	punctuationChars = new HashSet<Character>();
	punctuationChars.add('.');
	punctuationChars.add(',');
	punctuationChars.add('!');
	punctuationChars.add('?');
	punctuationChars.add(':');
	punctuationChars.add(';');
	punctuationChars.add('\'');
	punctuationChars.add('-');
	punctuationChars.add('"');
	punctuationChars.add('(');
	punctuationChars.add(')');
	punctuationChars.add('[');
	punctuationChars.add(']');
    }

    /**
     * Use Stanford NLP tool to parse concept name into word tokens.
     * <p>
     * Use this method when non-parsable characters are found in the concept
     * names. The Stanford parser in this method is setup to remove all
     * untokenizable characters.
     * </p>
     * 
     * @param text
     * @return
     */
    public List<String> stanfordWordParse(String text) {
	List<String> words = new ArrayList<String>();
	PTBTokenizer<CoreLabel> ptbt = new PTBTokenizer<CoreLabel>(
		new StringReader(text), new CoreLabelTokenFactory(),
		"untokenizable=allDelete,normalizeParentheses=false");
	while (ptbt.hasNext()) {
	    CoreLabel word = ptbt.next();
	    words.add(word.word());
	}
	return words;
    }

    /**
     * Return a token that, when necessary, is truncated to the maximum allowed
     * length for any token associated with the lexicon. It is also converted to
     * lower-case.
     * <p>
     * The length of any token parsed from the sentence must be no greater than
     * the maximum allowed in the lexicon. This method truncates the token to
     * this maximum length.
     * </p>
     * <p>
     * Thread safe.
     * </p>
     * 
     * @see {@link Constants#maxLengthLexiconToken}.
     * @param token
     * @return
     */
    public synchronized String adjustToken(String token) {
	if (token == null || token.isEmpty()) {
	    return token;
	}
	String adjToken = token.length() > Constants.maxLexiconTokenLength
		? token.substring(0, Constants.maxLexiconTokenLength)
			.toLowerCase()
		: token.toLowerCase();
	return adjToken;
    }

    public synchronized String replaceNonBreakingSpace(String token) {
	if (token == null || token.isEmpty()) {
	    return token;
	}
	return token.replace('\u00A0', ' ');
    }

    public synchronized boolean isPunctOrDelimChar(String text) {
	if (text == null || text.isEmpty() || text.length() > 1) {
	    return false;
	}
	boolean isAllPunctDelim = true;
	for (int i = 0; i < text.length(); i++) {
	    Character textChar = text.charAt(i);
	    if (!concept2WordsDelimChars.contains(textChar)
		    && !sentence2WordsDelimChars.contains(textChar)
		    && !sentenceRemoveChars.contains(textChar)
		    && !sentenceDelimChars.contains(textChar)
		    && !punctuationChars.contains(textChar)) {
		isAllPunctDelim = false;
		break;
	    }
	}
	return isAllPunctDelim;
    }

    /**
     * Determines if the text consists of white-space characters only.
     * 
     * @param text
     * @return
     */
    public synchronized boolean isWhiteSpaceOnly(String text) {

	boolean containsNonBlankChar = false;
	for (int i = 0; i < text.length(); i++) {
	    char charAtPos = text.charAt(i);
	    if (!Character.isWhitespace(charAtPos)) {
		containsNonBlankChar = true;
		break;
	    }
	}
	return !containsNonBlankChar;
    }

    public synchronized String removeChar(String txt, String removeChar) {

	if (txt == null || txt.length() == 0)
	    return txt;

	String newTxt = txt;
	if (txt.contains(removeChar)) {
	    int pos = txt.indexOf(removeChar);
	    if (pos > 0) {
		newTxt = txt.substring(0, pos);
	    } else {
		newTxt = txt.substring(1);
	    }
	}
	return newTxt;
    }

    public synchronized String removeAllSentenceChars(String text) {
	String newText = text;
	for (Character removeChar : sentenceRemoveChars) {
	    String removeStr = String.valueOf(removeChar);
	    newText = removeChar(newText, removeStr);
	}
	return newText;
    }

    /**
     * Splits text into tokens using specified split char
     * 
     * @param text
     * @param splitChar
     * @return
     */
    public synchronized List<String> split(String text, char splitChar) {

	List<String> splitList = new ArrayList<String>();

	int currentPos = 0;
	int nextPos = 0;
	do {
	    nextPos = text.indexOf(splitChar, currentPos);
	    String splitSeq = null;
	    if (nextPos == -1) {
		splitSeq = text.substring(currentPos);
	    } else {
		splitSeq = text.substring(currentPos, nextPos);
	    }
	    splitList.add(splitSeq);
	    currentPos = nextPos + 1;
	} while (nextPos != -1);

	return splitList;
    }

    public static int adjustBeginPosForWhiteSpace(int beginSentDocPos,
	    int sentDelimPos, String text) {
	int adjustedBeginSentDocPos = beginSentDocPos;
	if (Character.isWhitespace(text.charAt(beginSentDocPos))) {
	    for (int newBeginDocPos = beginSentDocPos
		    + 1; newBeginDocPos < sentDelimPos; newBeginDocPos++) {
		if (!Character.isWhitespace(text.charAt(newBeginDocPos))) {
		    adjustedBeginSentDocPos = newBeginDocPos;
		    break;
		}
	    }
	}

	return adjustedBeginSentDocPos;
    }

    /**
     * Utility method for parsing text into words. Required since regex throwing
     * error when splitting text using blank or whitespace character. Unclear
     * why this happens, so using brute force approach to save time.
     * 
     * @param text
     * @return
     * @throws UnsupportedEncodingException
     */
    public synchronized List<String> parseTextIntoWords(String text,
	    ParseType type) {

	List<String> tokens = new ArrayList<String>();

	if (text == null || text.length() == 0 || text == " "
		|| text.isEmpty()) {
	    return tokens;
	}

	Set<Character> parseChars = null;
	switch (type) {
	case CONCEPT_TO_WORDS:
	    parseChars = concept2WordsDelimChars;
	    break;
	case SENTENCE_TO_WORDS:
	    parseChars = sentence2WordsDelimChars;
	    break;
	default:
	    return null;
	}

	if (text.length() == 1) {
	    Character txtChar = text.charAt(0);
	    if (parseChars.contains(txtChar)) {
		return tokens;
	    }

	    tokens.add(text);
	    return tokens;
	}

	/**
	 * Loops thru string to extract positions to split text
	 */
	char[] chars = text.toCharArray();
	if (chars[0] == '-') {
	    System.out.printf("Debug instance%n");
	}
	int lastBeginPosIncl = 0;
	for (int pos = 0; pos < text.length(); pos++) {
	    if (pos == 0 && chars[0] == '-') {
		lastBeginPosIncl = 1;
		continue;
	    }
	    if (parseChars.contains(chars[pos])
		    || Character.isWhitespace(chars[pos])) {
		if (lastBeginPosIncl == pos) {
		    lastBeginPosIncl = pos + 1;
		    continue;
		}
		tokens.add(text.substring(lastBeginPosIncl, pos));
		lastBeginPosIncl = pos + 1;
		continue;
	    }
	    if (pos + 1 == text.length() && lastBeginPosIncl <= pos)
		tokens.add(text.substring(lastBeginPosIncl));

	}

	if (tokens.size() == 0) {
	    return tokens;
	}

	if (tokens.get(tokens.size() - 1).endsWith(".")) {
	    String token = tokens.get(tokens.size() - 1);
	    token = token.substring(0, token.length() - 1);
	    tokens.set(tokens.size() - 1, token);
	}

	return tokens;
    }

    public synchronized String closeGapsInDecimalNumbers(int pmid,
	    String text) {

	List<TextInterval> keepIntervals = new ArrayList<TextInterval>();

	char decimalChar = '.';
	int beginPos = 0;
	int endPos = 0;
	for (int i = 0; i < text.length() - 4; i++)
	    if (text.charAt(i) == decimalChar)
		if (Character.isDigit(text.charAt(i - 1)))
		    if (Character.isWhitespace(text.charAt(i + 1)))
			if (Character.isDigit(text.charAt(i + 2))) {
			    endPos = i + 1;
			    TextInterval keepInterval = new TextInterval(pmid,
				    beginPos, endPos);
			    keepIntervals.add(keepInterval);
			    beginPos = endPos + 1;
			}
	if (keepIntervals.size() > 0) {
	    TextInterval lastInterval = keepIntervals
		    .get(keepIntervals.size() - 1);
	    if (lastInterval.getEnd() < text.length() - 1) {
		TextInterval keepInterval = new TextInterval(pmid, beginPos,
			-1);
		keepIntervals.add(keepInterval);
	    }
	}

	Collections.sort(keepIntervals);

	StringBuilder stringBuilder = new StringBuilder();

	if (keepIntervals.size() != 0) {
	    for (TextInterval keepInterval : keepIntervals) {
		if (keepInterval.getEnd() == -1
			|| keepInterval.getEnd() == text.length() - 1)
		    stringBuilder.append(
			    text.substring((int) keepInterval.getBegin()));
		else
		    stringBuilder.append(
			    text.substring((int) keepInterval.getBegin(),
				    (int) keepInterval.getEnd()));
	    }
	}

	return stringBuilder.toString();
    }

    /**
     * Determines if a string can be parsed into multiple tokens. Used when
     * performing natural language processing of word tokens in sentences or
     * concept names.
     * 
     * @param text
     * @return
     */
    public synchronized boolean isParsable(String text) {
	boolean isParsable = false;
	char[] baseWordChars = text.toCharArray();
	for (int i = 0; i < baseWordChars.length; i++) {
	    if (this.getConceptParseExpressions().contains(baseWordChars[i])) {
		isParsable = true;
		break;
	    }
	}
	return isParsable;
    }

    public synchronized String cleanText(String text) {
	String txtToRemove = "\"";
	if (text.contains(txtToRemove)) {
	    String newText = text;
	    do {
		int pos = newText.indexOf(txtToRemove);
		if (pos == -1)
		    break;
		if (pos == 0) {
		    newText = text.substring(1);
		    continue;
		} else if (pos == text.length() - 1) {
		    newText = text.substring(1, text.length() - 1);
		    continue;
		}
		String tmpText = newText.substring(0, pos);
		newText = tmpText + newText.substring(pos + 1);
	    } while (newText.contains(txtToRemove));
	    return newText;
	} else
	    return text;
    }

    public synchronized String cleanName(String name) {
	if (name == null || name.length() == 0 || name.isEmpty())
	    return name;
	String clean = name.trim();
	// remove leading '[X]' if it exists
	if (clean.startsWith("[X]"))
	    clean = clean.substring(3).trim();

	// remove root concept in parenthesis
	// int pos = clean.lastIndexOf(")");
	// if (pos >= clean.length() - 2) {
	// int beginParen = clean.lastIndexOf("(");
	// clean = clean.substring(0, beginParen);
	// }

	// remove quote character
	char quoteChar = '"';
	int quotePos = clean.indexOf(quoteChar);
	int lastCleanCharPos = clean.length() - 1;
	while (quotePos >= 0) {
	    if (quotePos == 0)
		clean = clean.substring(quotePos + 1);
	    else if (quotePos > 0 && quotePos <= lastCleanCharPos) {
		if (quotePos == lastCleanCharPos) {
		    clean = clean.substring(0, quotePos);
		} else {
		    String firstSubstr = clean.substring(0, quotePos);
		    String secondSubstr = clean.substring(quotePos + 1);
		    clean = firstSubstr + secondSubstr;
		}
	    }
	    quotePos = clean.indexOf(quoteChar);
	    lastCleanCharPos = clean.length() - 1;
	}

	return clean;
    }

    public synchronized Set<String> getCategoriesFromConceptName(String name) {

	String text = name.trim();

	Set<String> categories = new HashSet<String>();

	String[] categoryArray = null;

	char lastChar = text.charAt(text.length() - 1);
	if (lastChar == ')') {
	    int posFirstParen = text.lastIndexOf('(');
	    if (posFirstParen > 0) {
		String categoryString = text.substring(posFirstParen + 1,
			text.length() - 1);
		categoryArray = categoryString.split(CONCEPT_CATEGORY_DELIM);
		if (categoryArray != null) {
		    for (int i = 0; i < categoryArray.length; i++)
			categories.add(categoryArray[i].trim());
		}
	    }
	}

	return categories;
    }

    public synchronized Set<String> getCategoriesFromAllConceptNames() {

	JdbcConnectionPool connPool = null;

	Set<String> categoriesAllNames = new HashSet<String>();

	try {

	    connPool = new JdbcConnectionPool(ConnDbName.CONCEPT_RECOGN);

	    categoriesAllNames = getCategoriesFromFullyQualifiedNames(connPool);

	} catch (Exception ex) {
	    ex.printStackTrace();
	}

	return categoriesAllNames;

    }

    /**
     * TODO refactor - this code is very old
     * 
     * @param connPool
     * @return
     * @throws Exception
     */
    public synchronized Set<String> getCategoriesFromFullyQualifiedNames(
	    JdbcConnectionPool connPool) throws Exception {

	Set<String> categoriesAllNames = new HashSet<String>();

	Connection conn = null;
	PreparedStatement getQualifiedNameQry = null;
	ResultSet rs = null;

	try {

	    conn = connPool.getConnection();
	    conn.setAutoCommit(false);

	    String getSourceNameSql = "SELECT fullyQualifiedName FROM sps.concepts";
	    getQualifiedNameQry = conn.prepareStatement(getSourceNameSql);
	    rs = getQualifiedNameQry.executeQuery();

	    while (rs.next()) {
		String qualifiedName = rs.getString(1);
		int posFirstParen = qualifiedName.lastIndexOf('(');
		if (posFirstParen == -1)
		    continue;
		int posLastParen = qualifiedName.lastIndexOf(')');
		if (posLastParen == -1 || posLastParen <= posFirstParen)
		    continue;
		String category = qualifiedName.substring(posFirstParen + 1,
			posLastParen);
		categoriesAllNames.add(category);
	    }

	} catch (Exception ex) {

	} finally {
	    if (rs != null)
		rs.close();
	    if (getQualifiedNameQry != null)
		getQualifiedNameQry.close();
	    if (conn != null) {
		conn.commit();
		conn.close();
	    }
	}
	return categoriesAllNames;

    }

    public synchronized String removeCategoriesFromName(String rawText) {

	String cleanText = rawText.trim();

	int posFirstParen = cleanText.lastIndexOf('(');
	if (posFirstParen == -1)
	    return cleanText;
	cleanText = cleanText.substring(0, posFirstParen);
	cleanText.trim();

	return cleanText;
    }

    public synchronized void testNameCleaning() {
	String name = "[X]Diacetate measurement (procedure)";
	name = cleanName(name);
	System.out.println(name);

	name = "\"Text With Parenthesis\"";
	name = cleanName(name);
	System.out.println(name);
    }

    public synchronized static BufferedReader getUTF8Reader(File file)
	    throws UnsupportedEncodingException, FileNotFoundException {
	BufferedReader bufferedReader = new BufferedReader(
		new InputStreamReader(new FileInputStream(file), "UTF-8"));
	return bufferedReader;
    }

    public synchronized static BufferedReader getUTF16Reader(File file)
	    throws UnsupportedEncodingException, FileNotFoundException {
	BufferedReader bufferedReader = new BufferedReader(
		new InputStreamReader(new FileInputStream(file), "UTF-16"));
	return bufferedReader;
    }

    public synchronized static BufferedReader getIso8859Reader(File file)
	    throws UnsupportedEncodingException, FileNotFoundException {
	BufferedReader bufferedReader = new BufferedReader(
		new InputStreamReader(new FileInputStream(file), "ISO-8859-1"));
	return bufferedReader;
    }

    public synchronized static BufferedReader getDefaultReader(File file)
	    throws UnsupportedEncodingException, FileNotFoundException {
	FileReader fileReader = new FileReader(file);
	BufferedReader bufferedReader = new BufferedReader(fileReader);
	return bufferedReader;
    }

    public synchronized static BufferedWriter getUTF8Writer(File file)
	    throws UnsupportedEncodingException, FileNotFoundException {
	BufferedWriter bufferedWriter = new BufferedWriter(
		new OutputStreamWriter(new FileOutputStream(file), "UTF-8"));
	return bufferedWriter;
    }

    public synchronized static BufferedWriter getUTF16Writer(File file)
	    throws UnsupportedEncodingException, FileNotFoundException {
	BufferedWriter bufferedWriter = new BufferedWriter(
		new OutputStreamWriter(new FileOutputStream(file), "UTF-16"));
	return bufferedWriter;
    }

    public synchronized static BufferedWriter getUTFIso8859Writer(File file)
	    throws UnsupportedEncodingException, FileNotFoundException {
	BufferedWriter bufferedWriter = new BufferedWriter(
		new OutputStreamWriter(new FileOutputStream(file),
			"ISO-8859-1"));
	return bufferedWriter;
    }

    public synchronized static BufferedWriter getDefaultWriter(File file)
	    throws UnsupportedEncodingException, FileNotFoundException {
	BufferedWriter bufferedWriter = new BufferedWriter(
		new OutputStreamWriter(new FileOutputStream(file)));
	return bufferedWriter;
    }

    public synchronized boolean hasUpperCaseChar(String str) {
	char[] chars = str.toCharArray();
	for (int i = 0; i < chars.length; i++) {
	    if (Character.isUpperCase(chars[i]))
		return true;
	}
	return false;
    }

    public boolean isLetterOrNumber(char c) {
	return Character.isLetter(c) || Character.isDigit(c);
    }

    public Set<Character> getConceptParseExpressions() {
	return concept2WordsDelimChars;
    }

    public Set<Character> getSentenceParseExpressions() {
	return sentence2WordsDelimChars;
    }

    public Set<Character> getSentenceRemoveChars() {
	return sentenceRemoveChars;
    }

    public Set<Character> getConcept2WordsDelimChars() {
	return concept2WordsDelimChars;
    }

    public Set<Character> getSentence2WordsDelimChars() {
	return sentence2WordsDelimChars;
    }

    public Set<Character> getSentenceDelimChars() {
	return sentenceDelimChars;
    }

    public Set<Character> getPunctuationChars() {
	return punctuationChars;
    }

    /**
     * @param args
     * @throws IllegalAccessException
     * @throws InstantiationException
     */
    public static void main(String[] args)
	    throws InstantiationException, IllegalAccessException {

	try {
	    NlpUtil textUtil = new NlpUtil();
	    textUtil.testNameCleaning();
	    // printSampleConceptNames();
	} catch (Exception ex) {
	    ex.printStackTrace();
	}
    }

}

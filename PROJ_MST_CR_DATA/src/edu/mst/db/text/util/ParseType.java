package edu.mst.db.text.util;

public enum ParseType {

    CONCEPT_TO_WORDS,
    SENTENCE_TO_WORDS
}

package edu.mst.db.text.util;

/**
 * Defines intervals in the text to keep when removing gaps in the abstract due
 * to the odd formatting of decimal numbers. The format leaves a space between
 * the decimal symbol and the first number to it's right.
 * 
 * @author George
 *
 */
public class TextInterval implements Comparable<TextInterval> {

    private int pmid;
    private long begin;
    private long end;
    
    public TextInterval(int pmid, long begin, long end){
	this.pmid = pmid;
	this.begin = begin;
	this.end = end;
    }

    @Override
    public int compareTo(TextInterval interval) {
	int i = Integer.valueOf(pmid).compareTo(Integer.valueOf(interval.pmid));
	if (i != 0)
	    return i;
	i = Long.valueOf(begin).compareTo(Long.valueOf(interval.begin));
	if (i != 0)
	    return i;
	return Long.valueOf(end).compareTo(Long.valueOf(interval.end));
    }

    @Override
    public boolean equals(Object obj) {
	if (obj instanceof TextInterval) {
	    TextInterval gap = (TextInterval) obj;
	    if (this.pmid == gap.pmid && this.begin == gap.begin
		    && this.end == gap.end)
		return true;
	}
	return false;
    }

    @Override
    public int hashCode() {
	return 17 * pmid + 19 * (int)begin + 31 * (int)end;
    }

    public int getPmid() {
        return pmid;
    }

    public long getBegin() {
        return begin;
    }

    public long getEnd() {
        return end;
    }

}

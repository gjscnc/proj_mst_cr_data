/**
 * 
 */
package edu.mst.db.text.util;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;

import edu.mst.db.lexicon.LexWord;
import edu.mst.db.lexicon.LexWordDao;
import edu.mst.db.lexicon.StopWords;
import edu.mst.db.lexicon.acrabbrev.AcrAbbrev;
import edu.mst.db.nlm.corpora.NlmAbstractWord;
import edu.mst.db.nlm.goldstd.corpora.GoldStdPhrase;
import edu.mst.db.nlm.goldstd.corpora.GoldStdPhraseWord;
import edu.mst.db.nlm.goldstd.corpora.GoldStdSentWord;
import edu.mst.db.nlm.goldstd.corpora.GoldStdSentence;
import edu.mst.db.nlm.metamap.MincoManPhrase;
import edu.mst.db.ontol.words.ConceptNameWord;
import edu.mst.db.util.JdbcConnectionPool;
import edu.stanford.nlp.ling.BasicDocument;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.ling.Word;
import edu.stanford.nlp.process.DocumentPreprocessor;
import edu.stanford.nlp.process.PTBTokenizer;
import edu.stanford.nlp.process.TokenizerFactory;

/**
 * Uses the Stanford tokenizer to parse sentence text and phrase text into
 * words. Includes removing all word and sentence delimiters from text to
 * facilitate the IOC tagging process.
 * 
 * @author gjs
 *
 */
public class StanfordParser {

    public static final String stanfordLeftParenToken = "-LRB-";
    public static final String stanfordRightParenToken = "-RRB-";

    private Map<String, AcrAbbrev> acrAbbrevByToken = null;
    private NlpUtil nlpUtil = null;
    private JdbcConnectionPool connPool = null;
    private LexWordDao lexDao = new LexWordDao();

    public StanfordParser(JdbcConnectionPool connPool) {
	nlpUtil = new NlpUtil();
	this.connPool = connPool;
    }

    /**
     * Method to parse word tokens from concept name
     * 
     * @param text
     * @param conceptNameUid
     * @param conceptUid
     * @return
     */
    public List<ConceptNameWord> parseNameWords(String text,
	    long conceptNameUid, long conceptUid) {

	List<ConceptNameWord> nameWords = new ArrayList<ConceptNameWord>();

	if (text == null || text.isBlank() || text.isEmpty()) {
	    return nameWords;
	}

	// replace any word delimiter chars with space
	char[] strCharArray = replaceWordDelims(text,
		WordParseType.CONCEPT_NAME);
	// text to parse, sans delimiters
	String textToParse = String.valueOf(strCharArray);
	BasicDocument<Word> doc = BasicDocument.init(textToParse);
	int nbrOfWords = doc.size();
	for (int i = 0; i < nbrOfWords; i++) {
	    Word word = doc.get(i);
	    String wordToken = word.word();
	    int beginSentPos = word.beginPosition();
	    int endSentPos = word.endPosition();
	    ConceptNameWord nameWord = new ConceptNameWord(conceptNameUid, i,
		    beginSentPos, endSentPos, conceptUid, wordToken);
	    nameWords.add(nameWord);
	}

	return nameWords;
    }

    public List<List<String>> parseSentencesAndWords(String text) {

	List<List<String>> sentences = new ArrayList<List<String>>();
	char[] strCharArray = replaceWordDelims(text, WordParseType.SENTENCE);
	String textToParse = String.valueOf(strCharArray);

	// edu.stanford.nlp.process.CoreLabelTokenFactory

	StringReader reader = new StringReader(textToParse);
	DocumentPreprocessor tokenizer = new DocumentPreprocessor(reader);
	TokenizerFactory<? extends HasWord> factory = PTBTokenizer
		.coreLabelFactory();
	factory.setOptions("untokenizable=noneDelete");
	tokenizer.setTokenizerFactory(factory);
	Iterator<List<HasWord>> stanfSentIter = tokenizer.iterator();
	while (stanfSentIter.hasNext()) {
	    List<HasWord> stanfSent = stanfSentIter.next();
	    List<String> tokenSent = new ArrayList<String>();
	    for (HasWord hasWord : stanfSent) {
		CoreLabel coreLabel = (CoreLabel) hasWord;
		String token = coreLabel.value();
		tokenSent.add(token);
	    }
	    sentences.add(tokenSent);
	}
	return sentences;
    }

    public List<List<String>> parseSentencesAndWords(String text,
	    int maxTokenLen) {

	List<List<String>> sentences = new ArrayList<List<String>>();
	char[] strCharArray = replaceWordDelims(text, WordParseType.SENTENCE);
	String textToParse = String.valueOf(strCharArray);

	// edu.stanford.nlp.process.CoreLabelTokenFactory

	StringReader reader = new StringReader(textToParse);
	DocumentPreprocessor tokenizer = new DocumentPreprocessor(reader);
	TokenizerFactory<? extends HasWord> factory = PTBTokenizer
		.coreLabelFactory();
	factory.setOptions("untokenizable=noneDelete");
	tokenizer.setTokenizerFactory(factory);
	Iterator<List<HasWord>> stanfSentIter = tokenizer.iterator();
	while (stanfSentIter.hasNext()) {
	    List<HasWord> stanfSent = stanfSentIter.next();
	    List<String> tokenSent = new ArrayList<String>();
	    for (HasWord hasWord : stanfSent) {
		CoreLabel coreLabel = (CoreLabel) hasWord;
		String token = coreLabel.value();
		if (token.length() > LexWord.maxTokenLen) {
		    token = new String(token.substring(0, maxTokenLen));
		}
		tokenSent.add(token);
	    }
	    sentences.add(tokenSent);
	}
	return sentences;
    }

    /**
     * Extract words for abstract when lexicon word IDs are *NOT* available.
     * 
     * @param pmid
     * @param text
     * @param maxTokenLen
     * @param stopWords
     * @return
     */
    public List<List<NlmAbstractWord>> parseAbstractWords(int pmid, String text,
	    int maxTokenLen, StopWords stopWords) {

	List<List<NlmAbstractWord>> sentences = new ArrayList<List<NlmAbstractWord>>();
	// char[] strCharArray = replaceWordDelims(text,
	// WordParseType.SENTENCE);
	// String textToParse = String.valueOf(strCharArray);

	// edu.stanford.nlp.process.CoreLabelTokenFactory

	StringReader reader = new StringReader(text);
	DocumentPreprocessor tokenizer = new DocumentPreprocessor(reader);
	TokenizerFactory<? extends HasWord> factory = PTBTokenizer
		.coreLabelFactory();
	factory.setOptions(
		"untokenizable=noneDelete,splitHyphenated=true,ptb3Escaping=false");
	tokenizer.setTokenizerFactory(factory);
	Iterator<List<HasWord>> stanfSentIter = tokenizer.iterator();
	int sentNbr = 0;
	while (stanfSentIter.hasNext()) {
	    List<HasWord> stanfSent = stanfSentIter.next();
	    List<NlmAbstractWord> sent = new ArrayList<NlmAbstractWord>();
	    int wordNbr = 0;
	    for (HasWord hasWord : stanfSent) {
		CoreLabel coreLabel = (CoreLabel) hasWord;
		String token = coreLabel.value().toLowerCase();
		if (token.length() > LexWord.maxTokenLen) {
		    token = new String(token.substring(0, maxTokenLen));
		}
		int beginOffset = coreLabel.beginPosition();
		int endOffset = coreLabel.endPosition();
		boolean isStopWord = stopWords.getStopWordTokens()
			.contains(token);
		boolean isPunctOrDelimChar = nlpUtil.isPunctOrDelimChar(token);
		NlmAbstractWord word = new NlmAbstractWord(pmid, sentNbr,
			wordNbr, token, isStopWord, isPunctOrDelimChar,
			beginOffset, endOffset);
		sent.add(word);
		wordNbr += 1;
	    }
	    sentences.add(sent);
	    sentNbr += 1;
	}
	return sentences;
    }

    /**
     * Extract words for abstract when lexicon word IDs are available.
     * 
     * @param pmid
     * @param text
     * @param maxTokenLen
     * @param lexWordIdByToken
     * @param stopWords
     * @return
     */
    public List<List<NlmAbstractWord>> parseAbstractWords(int pmid, String text,
	    int maxTokenLen, SortedMap<String, Integer> lexWordIdByToken,
	    StopWords stopWords) {

	List<List<NlmAbstractWord>> sentences = new ArrayList<List<NlmAbstractWord>>();
	char[] strCharArray = replaceWordDelims(text, WordParseType.SENTENCE);
	String textToParse = String.valueOf(strCharArray);

	// edu.stanford.nlp.process.CoreLabelTokenFactory

	StringReader reader = new StringReader(textToParse);
	DocumentPreprocessor tokenizer = new DocumentPreprocessor(reader);
	TokenizerFactory<? extends HasWord> factory = PTBTokenizer
		.coreLabelFactory();
	factory.setOptions("untokenizable=noneDelete");
	tokenizer.setTokenizerFactory(factory);
	Iterator<List<HasWord>> stanfSentIter = tokenizer.iterator();
	int sentNbr = 0;
	while (stanfSentIter.hasNext()) {
	    List<HasWord> stanfSent = stanfSentIter.next();
	    List<NlmAbstractWord> sent = new ArrayList<NlmAbstractWord>();
	    int wordNbr = 0;
	    for (HasWord hasWord : stanfSent) {
		CoreLabel coreLabel = (CoreLabel) hasWord;
		String token = coreLabel.value().toLowerCase();
		if (token.length() > LexWord.maxTokenLen) {
		    token = new String(token.substring(0, maxTokenLen));
		}
		if (stopWords.getStopWordTokens().contains(token)
			|| nlpUtil.isPunctOrDelimChar(token)) {
		    continue;
		}
		int lexWordId = lexWordIdByToken.get(token);
		int beginOffset = coreLabel.beginPosition();
		int endOffset = coreLabel.endPosition();
		boolean isStopWord = stopWords.getStopWordTokens()
			.contains(token);
		boolean isPunctOrDelimChar = nlpUtil.isPunctOrDelimChar(token);
		NlmAbstractWord word = new NlmAbstractWord(pmid, sentNbr,
			wordNbr, token, lexWordId, isStopWord,
			isPunctOrDelimChar, beginOffset, endOffset);
		sent.add(word);
		wordNbr += 1;
	    }
	    sentences.add(sent);
	    sentNbr += 1;
	}
	return sentences;
    }

    private char[] replaceWordDelims(String text, WordParseType type) {
	Set<Character> delimChars = null;
	switch (type) {
	case CONCEPT_NAME:
	    delimChars = nlpUtil.getConcept2WordsDelimChars();
	    break;
	case SENTENCE:
	    delimChars = nlpUtil.getSentence2WordsDelimChars();
	    break;
	}
	// replace any word delimiter chars with space
	char[] strCharArray = text.toCharArray();
	for (char delimChar : delimChars) {
	    for (int i = 0; i < strCharArray.length; i++) {
		if (strCharArray[i] == delimChar || strCharArray[i] == '.') {
		    strCharArray[i] = ' ';
		}
	    }
	}
	return strCharArray;
    }

    /**
     * Assumes all {@link GoldStdPhrase} objects are already instantiated for
     * the sentence.
     * 
     * @param mincoPhrase
     * @param sent
     */
    public void parsePhraseWords(MincoManPhrase mincoPhrase,
	    GoldStdSentence sent) {

	GoldStdPhrase phrase = sent.getPhrases().get(mincoPhrase.phraseNbr);

	List<Integer> wordBeginPositions = new ArrayList<Integer>();
	wordBeginPositions.addAll(sent.getWordsByBeginCharPos().keySet());
	Collections.sort(wordBeginPositions);
	int phraseBeginDocPos = mincoPhrase.beginDocPos;
	int phraseEndDocPos = mincoPhrase.endDocPos;
	List<GoldStdSentWord> sentWordsInPhrase = new ArrayList<GoldStdSentWord>();
	for (int wordBegin : wordBeginPositions) {
	    if (wordBegin >= phraseBeginDocPos && wordBegin < phraseEndDocPos) {
		GoldStdSentWord sentWord = sent.getWordsByBeginCharPos()
			.get(wordBegin);
		sentWordsInPhrase.add(sentWord);
	    }
	}
	for (int i = 0; i < sentWordsInPhrase.size(); i++) {
	    GoldStdSentWord sentWord = sentWordsInPhrase.get(i);
	    GoldStdPhraseWord phraseWord = new GoldStdPhraseWord();
	    phraseWord.setPmid(sent.getPmid());
	    phraseWord.setSentNbr(sent.getSentenceNbr());
	    phraseWord.setSentPhraseNbr(mincoPhrase.phraseNbr);
	    phraseWord.setPhraseWordNbr(i);
	    phraseWord.setSentWordNbr(sentWord.getSentWordNbr());
	    phraseWord.setText(sentWord.getText());
	    phraseWord.setBeginDocPos(sentWord.getBeginDocPos());
	    phraseWord.setEndDocPos(sentWord.getEndDocPos());
	    phraseWord.setIsAcrAbbrev(sentWord.isAcrAbbrev());
	    phraseWord.setAcrAbbrevId(sentWord.getAcrAbbrevId());
	    phraseWord.setLexWordId(sentWord.getLexWordId());
	    int beginInPhrasePos = sentWord.getBeginDocPos()
		    - phrase.getBeginDocPos();
	    phraseWord.setBeginPhrasePos(beginInPhrasePos);
	    int endInPhrasePos = beginInPhrasePos + sentWord.getText().length();
	    phraseWord.setEndPhrasePos(endInPhrasePos);

	    phrase.getWords().add(phraseWord);
	    if (i == 0) {
		phrase.setBeginSentWordNbr(sentWord.getSentWordNbr());
	    } else if (i == sentWordsInPhrase.size() - 1) {
		phrase.setEndSentWordNbr(sentWord.getSentWordNbr());
	    }
	}

	phrase.setNbrWords(phrase.getWords().size());

    }

    public List<GoldStdSentWord> parseSentWords(GoldStdSentence sent)
	    throws Exception {

	List<GoldStdSentWord> sentWords = new ArrayList<GoldStdSentWord>();

	// replace any word delimiter chars with space
	char[] strCharArray = sent.getSentText().toCharArray();
	for (char delimChar : nlpUtil.getSentence2WordsDelimChars()) {
	    for (int i = 0; i < strCharArray.length; i++) {
		if (strCharArray[i] == delimChar || strCharArray[i] == '.') {
		    strCharArray[i] = ' ';
		}
	    }
	}
	// text to parse, sans delimiters
	String textToParse = String.valueOf(strCharArray);
	BasicDocument<Word> doc = BasicDocument.init(textToParse);
	int nbrOfWords = doc.size();
	for (int i = 0; i < nbrOfWords; i++) {
	    Word word = doc.get(i);
	    String wordToken = word.word();
	    int wordBeginSentPos = word.beginPosition();
	    int wordEndSentPos = word.endPosition();
	    int wordBeginDocPos = sent.getBeginDocPos() + wordBeginSentPos;
	    int wordEndDocPos = sent.getBeginDocPos() + wordEndSentPos;
	    GoldStdSentWord sentWord = new GoldStdSentWord();
	    sentWord.setSentWordNbr(i);
	    sentWord.setText(wordToken);
	    sentWord.setPmid(sent.getPmid());
	    sentWord.setSentNbr(sent.getSentenceNbr());
	    sentWord.setBeginDocPos(wordBeginDocPos);
	    sentWord.setEndDocPos(wordEndDocPos);
	    if (acrAbbrevByToken.containsKey(wordToken.toLowerCase())) {
		sentWord.setIsAcrAbbrev(true);
		sentWord.setAcrAbbrevId(
			acrAbbrevByToken.get(wordToken.toLowerCase()).getId());
	    }
	    LexWord lexWord = lexDao.marshalByToken(wordToken, connPool);
	    if (lexWord == null) {
		String errMsg = String.format(
			"Lexicon word not found for token %1$s", wordToken);
		throw new Exception(errMsg);
	    }
	    sentWord.setLexWordId(lexWord.getId());

	    sent.getWords().add(sentWord);
	    sent.getWordsByBeginCharPos().put(sentWord.getBeginDocPos(),
		    sentWord);
	}

	return sentWords;

    }

    public Map<String, AcrAbbrev> getAcrAbbrevByToken() {
	return acrAbbrevByToken;
    }

    public void setAcrAbbrevByToken(Map<String, AcrAbbrev> acrAbbrevByToken) {
	this.acrAbbrevByToken = acrAbbrevByToken;
    }

}

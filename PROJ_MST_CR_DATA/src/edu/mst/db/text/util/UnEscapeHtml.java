package edu.mst.db.text.util;

import java.io.IOException;
import java.io.StringWriter;

import org.apache.commons.text.translate.NumericEntityUnescaper;

public class UnEscapeHtml {

    private NumericEntityUnescaper unescaper = new NumericEntityUnescaper(
	    NumericEntityUnescaper.OPTION.semiColonRequired);

    public String unescapeString(String text) throws IOException {
	StringWriter strWriter = new StringWriter();
	int currentPos = 0;
	int lastPos = 0;
	while (currentPos < text.length()) {
	    currentPos = text.indexOf("&#x", currentPos);
	    if (currentPos < 0) {
		strWriter.append(text.substring(lastPos));
		break;
	    }
	    strWriter.append(text.subSequence(lastPos, currentPos - 1));
	    unescaper.translate(text, currentPos, strWriter);
	    lastPos = text.indexOf(";", currentPos) + 1;
	    currentPos = lastPos;
	}
	return strWriter.toString();
    }

}

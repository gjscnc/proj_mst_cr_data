/**
 * Copyright Missouri University of Science and Technology
 * Authoer: George J. Shannon
 */
package edu.mst.conceptrecogn.tag;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Arrays;
import java.util.Comparator;

import edu.mst.db.lexicon.StopWords;
import edu.mst.db.util.CondProbFreqType;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * This class contains properties of each concept used to match concept with
 * specific words in text.
 * 
 * @author George
 *
 */
public class ConceptTextTag implements Comparable<ConceptTextTag> {

    public static final long CONCEPTUID_SPINAL_FUSION_HARRINGTON_ROD = 78217005L;
    public static final long CONCEPTUID_HARRINGTON_ROD = 14548009L;
    public static final long CONCEPTUID_SPINAL_SCOLIOSIS = 428548000L;
    public static final long CONCEPTUID_WITH_PATTERNS = 103372001L;
    public static final long CONCEPTUID_ERROR = 40885006L;
    public static final long CONCEPTUID_USE = 419385000L;
    public static final int VALID_BASEWORDID_FUSION = 547252;
    public static final int INVALID_BASEWORDID_FUSION = 547271;

    public static final int VALID_BASEWORDID_PATIENT = 601063;
    public static final int INVALID_BASEWORDID_PATIENT = 601061;

    public static final String getConceptIocSql = "SELECT lnOcNoStopWords "
	    + "FROM sps.conceptontolcogency "
	    + "WHERE conceptUid=? AND predicateBaseWordUid=? AND cogencyType=?";
    public static final String getWordProbSql = "SELECT lnProbNoStopWords "
	    + "FROM sps.wordprobability " + "WHERE baseWordId=?";
    public static final String maxCogencySql = "SELECT lnOcNoStopWords "
	    + "FROM sps.ConceptOntolCogency "
	    + "WHERE conceptUid = ? AND predicateBaseWordUid = ? AND cogencyType = ?";
    public static final String wordIdsSql = "SELECT baseWordId, baseWordPos "
	    + "FROM sps.conceptbasewordbypos WHERE " + "conceptUid=? "
	    + "ORDER BY baseWordPos";
    public static final String getConceptWordCountSql = "SELECT count(*) FROM sps.ConceptBaseWordByPos "
	    + "WHERE conceptUid = ?";
    public static final String getConceptNameSql = "SELECT c.cui, concname.nameText "
	    + "FROM sps.conceptcleanname as concname, sps.concepts as c "
	    + "WHERE  c.uid = ? AND c.cleanNameUid = concname.id";

    private ConceptPosKey key;
    private JdbcConnectionPool connPool;
    private StopWords stopWords;
    private long conceptUid = 0L;
    private int taggingBeginTextPos = -1;
    private int taggingEndTextPos = -1;
    private boolean useStopWords = false;
    private int textPredicateWordId = 0;
    private int conceptPredicateWordId = 0;
    private int conceptPredWordPos = -1;
    private int currentTaggerPos = -1;
    /**
     * Array contains base word ID for each word in text, where position in the
     * array corresponds to position in the text. Value is set to -1 initially,
     * and any position in array whose value is -1 indicates that a base word ID
     * was not available for this word.
     */
    private final int[] textBaseWordIdByPos;

    private int nbrWordsInConcept = 0;
    private int nbrNonStopWordsInConcept = 0;
    private int nbrStopWordsInConcept = 0;
    private double conceptTaggedIOC = Double.MIN_VALUE;
    private double textTaggedIOC = Double.MIN_VALUE;
    /**
     * The maximum IOC for the concept is the maximum possible IOC, based upon
     * the concept predicate word ID, if all words in the concept are matched in
     * the text. This value is the IOC that EXCLUDES stop words.
     */
    private double conceptMaxIOC = Double.MIN_VALUE;
    private double conceptDeltaIOC = Double.MIN_NORMAL;
    private double fractionConceptMaxIOC = Double.MIN_VALUE;
    private String cui;
    private String conceptName;
    private boolean isCopyOfPrior = false;

    /**
     * Stores word position in concept name that is used to tag the matching
     * position in the text. It is used to map text positions tagged by this
     * concept to word positions in concept name. Length is equal to the number
     * of words in the text. The position in this array corresponds to the
     * position of the word in the text. Value in array is the word position in
     * the concept name. Value set to -1 when text position is not tagged.
     */
    private int[] textPosTaggedWithConceptNamePos = null;
    private int nbrWordsTagged = 0;
    private int nbrStopWordsTagged = 0;
    private int nbrNonStopWordsTagged = 0;
    /**
     * This array stores the current tag set for the text. It is used for
     * finding available text words not previously tagged. Position in the array
     * corresponds to position in the text. The value stored is the concept UID
     * used to tag that position in the text. A value of 0 indicates that no
     * concept was selected for that position.
     */
    private long[] currentTagUidByTextPos = null;
    /**
     * This attribute stores the first position in the text that is tagged by
     * this concept. Used for sorting.
     */
    private int firstTextPosTagged = Integer.MAX_VALUE;
    /**
     * Stores the lexical base word IDs for concept name, where position in this
     * array corresponds to position of word in concept name. Length is equal to
     * the number of words in the concept name. The word ID stored in this array
     * is set to 0 when the word is a stop word and the ignoreStopWords flag is
     * set to true.
     */
    private int[] conceptBaseWordIds = null;
    /**
     * Stores whether or not the word at this position in the concept is a stop
     * word or not. Length of this array is length of concept name. Array
     * position is position of word in concept name.
     */
    private boolean[] isStopWord = null;
    /**
     * Array stores concept word position used to tag a text position. It is
     * used to track which concept words have been used to tag text. Length is
     * equal to number of words in concept. The value stored in the array is the
     * position in the text that is tagged. Position in this array corresponds
     * to the position of word in concept name. Value is -1 when that word in
     * concept is not used to tag text.
     */
    private int[] conceptNamePosTaggedWithTextPos;
    private boolean allWordsInConceptUsed = false;
    private boolean metricComputationDone = false;
    private TaggerUtil taggerUtil = null;

    private boolean isDebug = false;
    /**
     * All calculations use the base word frequency type of conditional
     * probabilities.
     */
    private static CondProbFreqType condProbType = CondProbFreqType.BASE_WORD_FREQ;

    public ConceptTextTag(long conceptUid, int taggingBeginTextPos,
	    int taggingEndTextPos, boolean useStopWords, int documentId,
	    int textId, int textPredicateWordId, int[] textBaseWordIDByPos,
	    int currentTaggerPos, long[] conceptUidTagByTextPos,
	    boolean isDebug, StopWords stopWords, JdbcConnectionPool connPool)
	    throws Exception {
	this.conceptUid = conceptUid;
	this.taggingBeginTextPos = taggingBeginTextPos;
	this.taggingEndTextPos = taggingEndTextPos;
	this.useStopWords = useStopWords;
	this.textPredicateWordId = textPredicateWordId;
	this.textBaseWordIdByPos = textBaseWordIDByPos;
	this.currentTaggerPos = currentTaggerPos;
	/*
	 * Copy current text tags to avoid unintended side effects. If not
	 * copied then changes to the tag set may be convoluted with
	 * computations in other threads for concurrent processing.
	 */
	this.currentTagUidByTextPos = Arrays.copyOf(conceptUidTagByTextPos,
		conceptUidTagByTextPos.length);
	this.isDebug = isDebug;
	this.connPool = connPool;
	this.stopWords = stopWords;
	this.taggerUtil = new TaggerUtil(connPool);
	init();
	this.key = new ConceptPosKey(documentId, textId, firstTextPosTagged);
    }

    /**
     * Instantiate word and tag arrays and marshal values.
     * 
     * @throws Exception
     */
    private void init() throws Exception {

	textPosTaggedWithConceptNamePos = new int[textBaseWordIdByPos.length];
	Arrays.fill(textPosTaggedWithConceptNamePos, -1);

	this.conceptPredicateWordId = textBaseWordIdByPos[currentTaggerPos];

	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement getConcepWordCountQry = conn
		    .prepareStatement(getConceptWordCountSql)) {
		getConcepWordCountQry.setLong(1, conceptUid);
		try (ResultSet rs = getConcepWordCountQry.executeQuery()) {
		    if (rs.next()) {
			nbrWordsInConcept = rs.getInt(1);
			conceptBaseWordIds = new int[nbrWordsInConcept];
			Arrays.fill(conceptBaseWordIds, -1);
			isStopWord = new boolean[nbrWordsInConcept];
			Arrays.fill(isStopWord, false);
			conceptNamePosTaggedWithTextPos = new int[nbrWordsInConcept];
			Arrays.fill(conceptNamePosTaggedWithTextPos, -1);
		    } else
			throw new Exception(String.format(
				"No words found for conceptUid %1$d",
				conceptUid));
		}
	    }

	    if (isDebug && conceptUid == CONCEPTUID_USE) {
		System.out.printf("Test scenario for concept UID %1$d %n",
			conceptUid);
	    }

	    try (PreparedStatement getConceptWordIdsQry = conn
		    .prepareStatement(wordIdsSql)) {
		getConceptWordIdsQry.setLong(1, conceptUid);
		try (ResultSet rs = getConceptWordIdsQry.executeQuery()) {
		    while (rs.next()) {
			int baseWordId = rs.getInt(1);
			int wordPos = rs.getInt(2);
			if (useStopWords) {
			    conceptBaseWordIds[wordPos] = baseWordId;
			} else {
			    if (stopWords.getLexWordIds().contains(baseWordId)) {
				conceptBaseWordIds[wordPos] = 0;
				isStopWord[wordPos] = true;
				nbrStopWordsInConcept++;
			    } else {
				conceptBaseWordIds[wordPos] = baseWordId;
				nbrNonStopWordsInConcept++;
			    }
			}
			// temporary fix for incorrect base word ID
			// if (conceptUid == 78217005L
			// && conceptBaseWordIds[rs.getInt(1)] == 547271)
			// conceptBaseWordIds[rs.getInt(1)] = 547252;
			if (baseWordId == conceptPredicateWordId)
			    conceptPredWordPos = wordPos;
		    }
		}
	    }
	    try (PreparedStatement maxCogencyQry = conn
		    .prepareStatement(maxCogencySql)) {
		maxCogencyQry.setLong(1, conceptUid);
		maxCogencyQry.setInt(2, conceptPredicateWordId);
		maxCogencyQry.setString(3,
			CondProbFreqType.BASE_WORD_FREQ.toString());
		try (ResultSet rs = maxCogencyQry.executeQuery()) {
		    if (rs.next())
			conceptMaxIOC = -rs.getDouble(1);
		}
	    }
	    try (PreparedStatement getConceptNameQry = conn
		    .prepareStatement(getConceptNameSql)) {
		getConceptNameQry.setLong(1, conceptUid);
		try (ResultSet rs = getConceptNameQry.executeQuery()) {
		    if (rs.next()) {
			cui = rs.getString(1);
			conceptName = rs.getString(2);
		    }
		}
	    }
	}

	//debug
	if (isDebug && conceptUid == 14548009L) {
	    System.out.println("Debug allWordsMatched, concept uid 14548009");
	}
	
	/*
	 * Find all words in text that match a word in the concept that is not
	 * tagged. Iterate backwards from the current text position being tagged
	 * to the first word in the text being tagged. For each of these text
	 * position iterate from the first position in the concept name to the
	 * last word in the concept name. Tag the word in the text with the
	 * concept word if neither have been tagged previously. Then move to the
	 * next text word and repeat until all words are processed.
	 */
	for (int textPos = currentTaggerPos; textPos >= taggingBeginTextPos; textPos--) {
	    /*
	     * Continue to next text position if a base word id not found for
	     * that text word, or the text word is tagged at this position
	     */
	    if (textBaseWordIdByPos[textPos] == -1
		    || currentTagUidByTextPos[textPos] != -1L
		    || (!useStopWords && stopWords.getLexWordIds().contains(
			    textBaseWordIdByPos[textPos])))
		continue;
	    // otherwise iterate over concept words to see if a match exists
	    for (int conceptWordPos = conceptBaseWordIds.length - 1; conceptWordPos > -1; conceptWordPos--) {
		/*
		 * Continue to next concept word if already tagged. Debugging
		 * Note: Length is equal to number of words in concept. The
		 * value stored in the array is the position in the text that is
		 * tagged. Position in this array corresponds to the position of
		 * word in concept name. Value is -1 when that word in concept
		 * is not used to tag text.
		 */
		if (conceptNamePosTaggedWithTextPos[conceptWordPos] != -1
			|| (!useStopWords && isStopWord[conceptWordPos]))
		    continue;
		// if concept word and text word match, then save
		int conceptWordBaseId = conceptBaseWordIds[conceptWordPos];

		//if (isDebug && conceptUid == 205438006L) {
		    //System.out.println("Debug ConceptTag.init()");
		//}

		int textWordBaseId = textBaseWordIdByPos[textPos];
		if (conceptWordBaseId == textWordBaseId) {
		    currentTagUidByTextPos[textPos] = conceptUid;
		    textPosTaggedWithConceptNamePos[textPos] = conceptWordPos;
		    conceptNamePosTaggedWithTextPos[conceptWordPos] = textPos;
		    nbrWordsTagged++;
		    if (stopWords.getLexWordIds().contains(conceptWordBaseId))
			nbrStopWordsTagged++;
		    else
			nbrNonStopWordsTagged++;
		    break;
		}
	    }
	}

	/*
	 * Iterate over text words to find first text position tagged
	 */
	for (int i = taggingBeginTextPos; i <= taggingEndTextPos; i++) {
	    if (textPosTaggedWithConceptNamePos[i] != -1) {
		firstTextPosTagged = i;
		break;
	    }
	}

	/*
	 * Calculate IOC max based upon predicate word but without using stop
	 * words. This avoids logic errors when all words in concept are tagged
	 * except for stop words. IOC data in database includes IOC values both
	 * with and without stop words. First see if the concept name has only
	 * one word. If that occurs then the max IOC is set to the word
	 * probability. Otherwise use the max IOC for the concept.
	 */
	if (nbrWordsInConcept == 1
		|| (!useStopWords && nbrNonStopWordsInConcept == 1))
	    setMaxIocEqualToWordProb();
	else
	    getMaxIoc();
    }

    /**
     * This method is used when the number of non-stop words in the concept name
     * is equal to one. Sets the max IOC value of concept tag equal to the word
     * probability.
     * 
     * @throws Exception
     */
    private void setMaxIocEqualToWordProb() throws Exception {
	// get ID for non-stop-word
	// if (isDebug && conceptUid == CONCEPTUID_WITH_PATTERNS) {
	if (isDebug && conceptUid == CONCEPTUID_USE) {
	    System.out.printf("Test scenario for concept UID %1$d %n",
		    conceptUid);
	}
	int baseWordId = 0;
	int wordPosInConcept = 0;
	if (nbrWordsInConcept == 1) {
	    baseWordId = conceptBaseWordIds[0];
	    if (baseWordId == 0) {
		String errMsg = String.format("For concept UID = %1$d, "
			+ "number of words in concept = 1, but word ID = 0.",
			conceptUid);
		throw new Exception(errMsg);
	    }
	} else {
	    for (int i = 0; i < conceptBaseWordIds.length; i++) {
		if (!useStopWords && conceptBaseWordIds[i] == 0)
		    continue;
		baseWordId = conceptBaseWordIds[i];
		wordPosInConcept = i;
		break;
	    }
	}

	// retrieve word probability
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement getWordProbQry = conn
		    .prepareStatement(getWordProbSql)) {
		getWordProbQry.setInt(1, baseWordId);
		try (ResultSet rs = getWordProbQry.executeQuery()) {
		    if (rs.next())
			conceptMaxIOC = -rs.getDouble(1);
		    else
			throw new Exception(String.format(
				"Word probability not found "
					+ "for concept UID = %1$d, "
					+ "base word ID = %2$d, "
					+ "word position in name = %3$d",
				conceptUid, baseWordId, wordPosInConcept));
		}
	    }
	}
    }

    /**
     * Method for retrieving maximum concept IOC when concept does not have a
     * one-word name.
     * 
     * @throws Exception
     */
    private void getMaxIoc() throws Exception {
	// if (isDebug && conceptUid == CONCEPTUID_WITH_PATTERNS) {
	if (isDebug && conceptUid == CONCEPTUID_USE) {
	    System.out.printf("Test scenario for concept UID %1$d %n",
		    conceptUid);
	}
	try (Connection conn = connPool.getConnection()) {
	    try (PreparedStatement getConceptIocQry = conn
		    .prepareStatement(getConceptIocSql)) {
		getConceptIocQry.setLong(1, conceptUid);
		getConceptIocQry.setInt(2, conceptPredicateWordId);
		getConceptIocQry.setString(3, condProbType.toString());
		try (ResultSet rs = getConceptIocQry.executeQuery()) {
		    if (rs.next()) {
			conceptMaxIOC = -rs.getDouble(1);
		    } else {
			String errMsg = String
				.format("Max concept IOC not found in database. "
					+ "Concept UID = %1$d, predicate word ID = %2$d, "
					+ "conditional probability type = %3$s",
					conceptUid, conceptPredicateWordId,
					condProbType.toString());
			System.out.println(errMsg);
			throw new Exception(errMsg);
		    }
		}
	    }
	}
    }

    /**
     * Private constructor for deep copy
     * 
     * @param textWordIDByPos
     */
    private ConceptTextTag(int[] textWordIDByPos) {
	this.textBaseWordIdByPos = textWordIDByPos;
    }

    public ConceptTextTag deepCopy() {

	ConceptTextTag deepCopy = new ConceptTextTag(textBaseWordIdByPos);

	deepCopy.isCopyOfPrior = true;
	deepCopy.conceptUid = this.conceptUid;
	deepCopy.taggingBeginTextPos = this.taggingBeginTextPos;
	deepCopy.taggingEndTextPos = this.taggingEndTextPos;
	deepCopy.conceptPredWordPos = this.conceptPredWordPos;
	deepCopy.textPredicateWordId = this.textPredicateWordId;
	deepCopy.currentTaggerPos = this.currentTaggerPos;
	deepCopy.conceptPredicateWordId = this.conceptPredicateWordId;
	deepCopy.connPool = this.connPool;
	deepCopy.conceptTaggedIOC = this.conceptTaggedIOC;
	deepCopy.conceptMaxIOC = this.conceptMaxIOC;
	deepCopy.conceptDeltaIOC = this.conceptDeltaIOC;
	deepCopy.fractionConceptMaxIOC = this.fractionConceptMaxIOC;
	deepCopy.cui = this.cui;
	deepCopy.conceptName = this.conceptName;
	deepCopy.textPosTaggedWithConceptNamePos = Arrays.copyOf(
		textPosTaggedWithConceptNamePos,
		textPosTaggedWithConceptNamePos.length);
	deepCopy.firstTextPosTagged = this.firstTextPosTagged;
	deepCopy.conceptBaseWordIds = Arrays.copyOf(conceptBaseWordIds,
		conceptBaseWordIds.length);
	deepCopy.conceptNamePosTaggedWithTextPos = Arrays.copyOf(
		conceptNamePosTaggedWithTextPos,
		conceptNamePosTaggedWithTextPos.length);
	deepCopy.key = key;
	deepCopy.taggerUtil = taggerUtil;
	deepCopy.allWordsInConceptUsed = this.allWordsInConceptUsed;
	deepCopy.metricComputationDone = this.metricComputationDone;

	return deepCopy;

    }

    public void computeMetrics() throws Exception {

	// don't recompute if already done
	if (metricComputationDone)
	    return;

	// compute cogency for tagged words in concept
	conceptTaggedIOC = 0.0d;
	textTaggedIOC = 0.0d;

	/**
	 * Iterate over words in concept to compute tagged IOC. Note: matrices
	 * and lists use zero-based indexing
	 */
	for (int i = 0; i < conceptNamePosTaggedWithTextPos.length; i++) {

	    if (conceptNamePosTaggedWithTextPos[i] == -1)
		continue;

	    int conceptAssumedFactWordId = textBaseWordIdByPos[conceptNamePosTaggedWithTextPos[i]];

	    conceptTaggedIOC += -(taggerUtil.getLnCondProb(
		    conceptPredicateWordId, conceptAssumedFactWordId,
		    CondProbFreqType.BASE_WORD_FREQ.toString(), true));
	    textTaggedIOC += -(taggerUtil.getLnCondProb(textPredicateWordId,
		    conceptAssumedFactWordId,
		    CondProbFreqType.BASE_WORD_FREQ.toString(), true));

	}

	fractionConceptMaxIOC = conceptTaggedIOC / conceptMaxIOC;
	conceptDeltaIOC = conceptTaggedIOC - conceptMaxIOC;

	metricComputationDone = true;

	// debug
	if (isDebug
		&& (conceptUid == CONCEPTUID_SPINAL_FUSION_HARRINGTON_ROD || conceptUid == CONCEPTUID_HARRINGTON_ROD)) {
	    int nbrConceptWordsTagged = useStopWords ? nbrStopWordsTagged
		    : nbrNonStopWordsTagged;
	    System.out
		    .printf("Concept UID %1$d | nbr concept words tagged %2$d | concept max IOC %3$.3f "
			    + "| concept tagged IOC %4$.3f | text tagged IOC %5$.3f "
			    + "| fraction concept max IOC %6$.3f | conceptDeltaIOC %7$.3f %n",
			    conceptUid, nbrConceptWordsTagged, conceptMaxIOC,
			    conceptTaggedIOC, textTaggedIOC,
			    fractionConceptMaxIOC, conceptDeltaIOC);
	}

    }

    /**
     * Returns true if the list of word IDs for this concept contains the
     * specified word ID
     * 
     * @param baseWordId
     * @return
     */
    public boolean containsWord(int baseWordId) {
	boolean hasWord = false;
	for (int i = 0; i < conceptBaseWordIds.length; i++) {
	    int wordId = conceptBaseWordIds[i];
	    if (wordId == baseWordId)
		return true;
	}
	return hasWord;
    }

    @Override
    public int compareTo(ConceptTextTag tag) {
	return Comparators.PositionKey.compare(this, tag);
    }

    @Override
    public boolean equals(Object obj) {
	if (obj instanceof ConceptTextTag) {
	    ConceptTextTag conceptTagSet = (ConceptTextTag) obj;
	    return key.equals(conceptTagSet.key);
	}
	return false;
    }

    @Override
    public int hashCode() {
	return key.hashCode();
    }

    // comparators using lambda calculus - requires Java 8
    public static class Comparators {
	public static final Comparator<ConceptTextTag> FractionMaxIOC = (
		ConceptTextTag t1, ConceptTextTag t2) -> Double.compare(
		t1.fractionConceptMaxIOC, t2.fractionConceptMaxIOC);
	public static final Comparator<ConceptTextTag> PositionKey = (
		ConceptTextTag t1, ConceptTextTag t2) -> t1.key
		.compareTo(t2.key);
	public static final Comparator<ConceptTextTag> TaggedIOC = (
		ConceptTextTag t1, ConceptTextTag t2) -> Double.compare(
		t1.conceptTaggedIOC, t2.conceptTaggedIOC);
    }

    public ConceptPosKey getKey() {
	return key;
    }

    public long getConceptUid() {
	return conceptUid;
    }

    public int getTaggingBeginTextPos() {
	return taggingBeginTextPos;
    }

    public int getTaggingEndTextPos() {
	return taggingEndTextPos;
    }

    public int getTextPredicateWordId() {
	return textPredicateWordId;
    }

    public int getConceptPredicateWordId() {
	return conceptPredicateWordId;
    }

    public void setConceptPredicateWordId(int conceptPredicateWordId) {
	this.conceptPredicateWordId = conceptPredicateWordId;
    }

    public int getConceptPredWordPos() {
	return conceptPredWordPos;
    }

    public void setConceptPredWordPos(int conceptPredWordPos) {
	this.conceptPredWordPos = conceptPredWordPos;
    }

    public int getCurrentTaggerPos() {
	return currentTaggerPos;
    }

    public void setCurrentTaggerPos(int currentTaggerPos) {
	this.currentTaggerPos = currentTaggerPos;
    }

    /**
     * Tagged IOC is the IOC for the words in the concept used to tag the text,
     * given the predicate word provided.
     * 
     * @return
     */
    public double getConceptTaggedIOC() {
	return conceptTaggedIOC;
    }

    /**
     * The maximum IOC for the concept is the maximum possible IOC, based upon
     * the concept predicate word ID, if all words in the concept are matched in
     * the text. This value is the IOC that EXCLUDES stop words.
     * 
     * @return
     */
    public double getConceptMaxIOC() {
	return conceptMaxIOC;
    }

    /**
     * Delta IOC is the difference between the tagged IOC and the maximum IOC
     * possible for this concept and predicate word combination.
     * 
     * @return
     */
    public double getConceptDeltaIOC() {
	return conceptDeltaIOC;
    }

    public int getNbrOfWordsInConcept() {
	return nbrWordsInConcept;
    }

    public double getFractionConceptMaxIOC() {
	return fractionConceptMaxIOC;
    }

    /**
     * Stores word position in concept name that is used to tag the matching
     * position in the text. It is used to map text positions tagged by this
     * concept to word positions in concept name. Length is equal to the number
     * of words in the text. The position in this array corresponds to the
     * position of the word in the text. Value in array is the word position in
     * the concept name. Value set to -1 when text position is not tagged.
     */
    public int[] getTextPosTaggedWithConceptNamePos() {
	return textPosTaggedWithConceptNamePos;
    }

    /**
     * This attribute stores the first position in the text that is tagged by
     * this concept. Used for sorting.
     */
    public int getFirstTextPosTagged() {
	return firstTextPosTagged;
    }

    /**
     * Stores the lexical base word IDs for concept name, where position in this
     * matrix corresponds to position of word in concept name. Length is equal
     * to the number of words in the concept name.
     */
    public int[] getConceptBaseWordIds() {
	return conceptBaseWordIds;
    }

    /**
     * Array stores concept word position used to tag a text position. It is
     * used to track which concept words have been used to tag text. Length is
     * equal to number of words in concept. The value stored in the array is the
     * position in the text that is tagged. Position in this array corresponds
     * to the position of word in concept name. Value is -1 when that word in
     * concept is not used to tag text.
     * 
     * @return
     */
    public int[] getConceptNamePosTaggedWithTextPos() {
	return conceptNamePosTaggedWithTextPos;
    }

    public boolean isAllWordsInConceptUsed() {
	return allWordsInConceptUsed;
    }

    public boolean isMetricComputationDone() {
	return metricComputationDone;
    }

    public String getCui() {
	return cui;
    }

    public String getConceptName() {
	return conceptName;
    }

    public boolean isCopyOfPrior() {
	return isCopyOfPrior;
    }

    public int getPmid() {
	return key.getPmid();
    }

    public int getSentenceNbr() {
	return key.getSentenceNbr();
    }

    /**
     * Number of text words tagged with this concept.
     * 
     * @return
     */
    public int getNbrWordsTagged() {
	return nbrWordsTagged;
    }

    /**
     * This array stores the current tag set for the text. It is used for
     * finding available text words not previously tagged. Position in the array
     * corresponds to position in the text. The value stored is the concept UID
     * used to tag that position in the text. A value of -1 indicates that no
     * concept was selected for that position.
     * 
     * @return
     */
    public long[] getCurrentTagUidByTextPos() {
	return currentTagUidByTextPos;
    }

    public int getNbrNonStopWordsInConcept() {
	return nbrNonStopWordsInConcept;
    }

    public boolean isDebug() {
	return isDebug;
    }

    public double getTextTaggedIOC() {
	return textTaggedIOC;
    }

    public int getNbrStopWordsTagged() {
	return nbrStopWordsTagged;
    }

    public int getNbrStopWordsInConcept() {
	return nbrStopWordsInConcept;
    }

    public int getNbrNonStopWordsTagged() {
	return nbrNonStopWordsTagged;
    }

}

/**
 * 
 */
package edu.mst.conceptrecogn.tag;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import edu.mst.db.util.CondProbFreqType;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * This class stores the tags for an arbitrary set of words, i.e., free-form
 * text.
 * 
 * @author George
 *
 */
public class TextTagSet implements Comparable<TextTagSet> {

    public static double textMaxIOC = Double.MIN_NORMAL;

    private int documentId = -1;
    private int textId = -1;
    private UUID uuid;
    private JdbcConnectionPool connPool;
    private final int[] textWordIDByPos;
    private int textLength = 0;
    private double textLenInverse;
    /**
     * Array of concept UIDs used to tag this text. Position in array
     * corresponds to word position in text. Value in array is equal to the UID
     * for the concept used to tag that text. This value set to -1 when that
     * word at this position is not tagged. "Text position" refers to the
     * position of the word in the sequence of words for that text.
     */
    private long[] conceptUidTagByTextPos = null;
    /**
     * Stores word position in concept name that is used to tag the matching
     * position in the text. It is used to map text positions tagged by a
     * concept to word positions in that concept's name. Length is equal to the
     * number of words in the text. The position in this array corresponds to
     * the position of the word in the text. Value in array is the word position
     * in the concept name. Value in array set to -1 when text word is not
     * tagged.
     * 
     */
    private int[] conceptNamePosByTextPosTagged = null;
    /**
     * Key is concept uid, map supplied for quick lookup of concept tag. Each
     * value in the map is the list of instances of the CandidateConceptTag for
     * the same concept uid.
     */
    private Map<Long, Set<ConceptTextTag>> conceptTagByConceptUid;
    /**
     * List of tags ordered by position in text. Used to assign tag identifiers
     * and to instantiate tag spans in database.
     */
    private List<ConceptTextTag> allTags;
    private int textPredicateWordId;
    private double textTaggedIOC = Double.MIN_NORMAL;
    private double fractTextTagged = Double.MIN_NORMAL;
    private double fractionTextMaxIOC = Double.MIN_NORMAL;
    private double avgConceptIOC = Double.MIN_NORMAL;
    private double avgFractConceptMaxIOC = Double.MIN_NORMAL;
    private double totalConceptIOC = Double.MIN_NORMAL;
    private double totalConceptDeltaIOC = Double.MIN_NORMAL;
    private double avgConceptDeltaIOC = Double.MIN_NORMAL;
    private boolean metricComputationDone = false;
    private TaggerUtil taggerUtil = null;
    private int lastTagId = 0;

    private double rankingMetric = Double.MIN_NORMAL;

    /**
     * Constructor
     * 
     * @param textWordIDByPos
     *            Contains base word ID for each word in sentence. Position in
     *            this array corresponds to position in sentence. Value in array
     *            is the base word ID.
     * @param textPredicateWordId
     *            Predicate word ID for computing IOC
     * @param connPool
     *            Pool of JDBC connections to MySQL database.
     * @throws Exception
     */
    public TextTagSet(int[] textWordIDByPos, int textPredicateWordId,
	    JdbcConnectionPool connPool) throws Exception {
	this.textWordIDByPos = textWordIDByPos;
	this.textPredicateWordId = textPredicateWordId;
	this.connPool = connPool;
	init();
    }

    private void init() throws Exception {

	uuid = UUID.randomUUID();

	taggerUtil = new TaggerUtil(connPool);
	conceptTagByConceptUid = new HashMap<Long, Set<ConceptTextTag>>();

	textLength = textWordIDByPos.length;
	textLenInverse = 1.0d / (int) textLength;
	conceptUidTagByTextPos = new long[textLength];
	Arrays.fill(conceptUidTagByTextPos, -1L);
	conceptNamePosByTextPosTagged = new int[textLength];
	Arrays.fill(conceptNamePosByTextPosTagged, -1);

	allTags = new ArrayList<ConceptTextTag>();

	/*
	 * for (ConceptTextTag conceptTag : conceptTags) {
	 * allTags.add(conceptTag); if
	 * (!conceptTagByConceptUid.containsKey(conceptTag.getConceptUid()))
	 * conceptTagByConceptUid.put(conceptTag.getConceptUid(), new
	 * HashSet<ConceptTextTag>());
	 * conceptTagByConceptUid.get(conceptTag.getConceptUid()).add(
	 * conceptTag); for (int i = 0; i <
	 * conceptTag.getTextPosTaggedWithConceptNamePos().length; i++) { if
	 * (conceptTag.getTextPosTaggedWithConceptNamePos()[i] == -1) continue;
	 * conceptUidTagByTextPos[i] = conceptTag.getConceptUid();
	 * conceptNamePosByTextPosTagged[i] = conceptTag
	 * .getTextPosTaggedWithConceptNamePos()[i]; } }
	 */

	allTags.sort(ConceptTextTag.Comparators.PositionKey);

	if (textMaxIOC == Double.MIN_NORMAL) {

	    textMaxIOC = 0.0d;

	    for (int i = textWordIDByPos.length - 1; i >= 0; i--) {

		int textWordId = textWordIDByPos[i];

		if (textWordId == 0)
		    continue;

		if (textWordId == ConceptTextTag.VALID_BASEWORDID_FUSION)
		    textWordId = ConceptTextTag.INVALID_BASEWORDID_FUSION;

		textMaxIOC += -taggerUtil.getLnCondProb(textPredicateWordId,
			textWordId, CondProbFreqType.BASE_WORD_FREQ.toString(),
			true);
	    }
	}

    }

    public void computeMetrics() throws Exception {

	// don't recompute if already done
	if (metricComputationDone)
	    return;

	textTaggedIOC = 0.0d;
	fractTextTagged = 0.0d;
	avgConceptIOC = 0.0d;
	avgFractConceptMaxIOC = 0.0d;
	totalConceptIOC = 0.0d;
	double nbrConceptsTagged = (double) conceptTagByConceptUid.keySet()
		.size();

	for (Long conceptUid : conceptTagByConceptUid.keySet()) {

	    for (ConceptTextTag conceptTag : conceptTagByConceptUid
		    .get(conceptUid)) {
		double conceptTaggedIOC = conceptTag.getConceptTaggedIOC();
		double conceptMaxIOC = conceptTag.getConceptMaxIOC();
		avgConceptIOC += conceptTaggedIOC / nbrConceptsTagged;
		avgFractConceptMaxIOC += conceptTag.getFractionConceptMaxIOC()
			/ nbrConceptsTagged;
		totalConceptIOC += conceptTaggedIOC;
		totalConceptDeltaIOC += (conceptMaxIOC - conceptTaggedIOC);
		avgConceptDeltaIOC += (conceptMaxIOC - conceptTaggedIOC)
			/ nbrConceptsTagged;

		// pull symbol ontology cogency and compute tagged cogency
		try (Connection conn = connPool.getConnection()) {
		    String getSymbolCogencySql = "SELECT lnOntolCogency FROM sps.symbolontolcogencybytype "
			    + "WHERE predicateBaseWordId = ? AND assumedFactBaseWordId = ? AND cogencyType = ?";
		    try (PreparedStatement getSymbolCogencyQry = conn
			    .prepareStatement(getSymbolCogencySql)) {

			for (int i = 0; i < textLength; i++) {

			    int conceptWordPos = conceptTag
				    .getTextPosTaggedWithConceptNamePos()[i];
			    if (conceptWordPos == -1)
				continue;

			    int assumedFactWordId = conceptTag
				    .getConceptBaseWordIds()[conceptWordPos];
			    if (conceptUid == 78217005L
				    && assumedFactWordId == 547252)
				assumedFactWordId = 547271;
			    getSymbolCogencyQry.setInt(1,
				    conceptTag.getConceptPredicateWordId());
			    getSymbolCogencyQry.setInt(2, assumedFactWordId);
			    getSymbolCogencyQry.setString(3,
				    CondProbFreqType.BASE_WORD_FREQ.toString());

			    try (ResultSet rs = getSymbolCogencyQry
				    .executeQuery()) {
				if (rs.next()) {
				    textTaggedIOC += -rs.getDouble(1);
				    fractTextTagged += textLenInverse;
				}
			    }
			}
		    }
		}
	    }
	}

	fractionTextMaxIOC = textTaggedIOC / textMaxIOC;

	rankingMetric = textTaggedIOC + avgConceptIOC - avgConceptDeltaIOC;

	metricComputationDone = true;
    }

    public void addConceptTextTag(ConceptTextTag newTag, int textPos)
	    throws Exception {

	if (!conceptTagByConceptUid.containsKey(newTag.getConceptUid()))
	    conceptTagByConceptUid.put(newTag.getConceptUid(),
		    new HashSet<ConceptTextTag>());
	conceptTagByConceptUid.get(newTag.getConceptUid()).add(newTag);

	allTags.add(newTag);
	allTags.sort(ConceptTextTag.Comparators.PositionKey);

	for (int i = 0; i < newTag.getTextPosTaggedWithConceptNamePos().length; i++) {

	    /*
	     * Check to see if this position in text was already tagged. In
	     * addition, do not tag this position if tagged previously by
	     * another concept (cannot override prior already found optimal).
	     */
	    if (newTag.getTextPosTaggedWithConceptNamePos()[i] == -1
		    || conceptNamePosByTextPosTagged[i] != -1)
		continue;

	    conceptUidTagByTextPos[i] = newTag.getConceptUid();
	    conceptNamePosByTextPosTagged[i] = newTag
		    .getTextPosTaggedWithConceptNamePos()[i];
	}

	computeMetrics();
    }

    @Override
    public int compareTo(TextTagSet t2) {
	return Comparators.UniqueId.compare(this, t2);
    }

    @Override
    public boolean equals(Object obj) {
	if (obj instanceof TextTagSet) {
	    TextTagSet tagSet = (TextTagSet) obj;
	    return tagSet.uuid.equals(this.uuid);
	}
	return false;
    }

    @Override
    public int hashCode() {
	return this.uuid.hashCode();
    }

    public static class Comparators {
	public static final Comparator<TextTagSet> UniqueId = (TextTagSet t1,
		TextTagSet t2) -> t1.uuid.compareTo(t2.uuid);
	public static final Comparator<TextTagSet> RankingMetric = (
		TextTagSet t1, TextTagSet t2) -> Double.compare(
		t1.rankingMetric, t2.rankingMetric);
    }

    public UUID getUuid() {
	return uuid;
    }

    public JdbcConnectionPool getConnPool() {
	return connPool;
    }

    /**
     * Array of concept UIDs used to tag this text. Position in array
     * corresponds to word position in text. Value in array set to 0 when that
     * text position is not tagged.
     * 
     * @return
     */
    public long[] getConcepUidByTextPos() {
	return conceptUidTagByTextPos;
    }

    /**
     * Stores word position in concept name that is used to tag the matching
     * position in the text. It is used to map text positions tagged by a
     * concept to word positions in that concept's name. Length is equal to the
     * number of words in the text. The position in this array corresponds to
     * the position of the word in the text. Value in array is the word position
     * in the concept name. Value in array set to -1 when text word is not
     * tagged.
     * 
     * @return
     */
    public int[] getConceptNamePosByTextPosTagged() {
	return conceptNamePosByTextPosTagged;
    }

    /**
     * Stores set of tags for the same concept. Multiple instances of the same
     * concept may be used for tagging this text.
     * 
     * @return
     */
    public Map<Long, Set<ConceptTextTag>> getConceptTagByConceptUid() {
	return conceptTagByConceptUid;
    }

    public List<ConceptTextTag> getOrderedTags() {
	return allTags;
    }

    public int getTagCountForConcept(Long conceptUid) {
	if (conceptTagByConceptUid.containsKey(conceptUid))
	    return conceptTagByConceptUid.get(conceptUid).size();
	return 0;
    }

    public int getTextPredicateWordId() {
	return textPredicateWordId;
    }

    public double getTextTaggedIOC() {
	return textTaggedIOC;
    }

    public double getTextMaxIOC() {
	return textMaxIOC;
    }

    public double getFractTextTagged() {
	return fractTextTagged;
    }

    public double getFractionTextMaxIOC() {
	return fractionTextMaxIOC;
    }

    public double getAvgConceptIOC() {
	return avgConceptIOC;
    }

    public double getAvgFractConceptMaxIOC() {
	return avgFractConceptMaxIOC;
    }

    public double getTotalConceptIOC() {
	return totalConceptIOC;
    }

    public double getTotalConceptDeltaIOC() {
	return totalConceptDeltaIOC;
    }

    public double getAvgConceptDeltaIOC() {
	return avgConceptDeltaIOC;
    }

    public boolean isMetricComputationDone() {
	return metricComputationDone;
    }

    public double getRankingMetric() {
	return rankingMetric;
    }

    public int getLastTagNbr() {
	return lastTagId;
    }

    public void setLastTagNbr(int lastTagNbr) {
	this.lastTagId = lastTagNbr;
    }

    public int getDocumentId() {
	return documentId;
    }

    public void setDocumentId(int documentId) {
	this.documentId = documentId;
    }

    public int getTextId() {
	return textId;
    }

    public void setTextId(int textId) {
	this.textId = textId;
    }

}

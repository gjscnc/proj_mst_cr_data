package edu.mst.conceptrecogn.tag;

public class ConceptPosKey implements Comparable<ConceptPosKey> {

    private int pmid = -1;
    private int sentenceNbr = -1;
    private int firstSentPosTagged = -1;

    public ConceptPosKey(int pmid, int sentenceNbr, int sentWordPos) {
	this.pmid = pmid;
	this.sentenceNbr = sentenceNbr;
	this.firstSentPosTagged = sentWordPos;
    }
    
    private ConceptPosKey(){}
    
    public ConceptPosKey copy(){
	ConceptPosKey keyCopy = new ConceptPosKey();
	keyCopy.pmid = pmid;
	keyCopy.sentenceNbr = sentenceNbr;
	keyCopy.firstSentPosTagged = firstSentPosTagged;
	return keyCopy;
    }

    @Override
    public int compareTo(ConceptPosKey key) {
	int i = Integer.valueOf(pmid).compareTo(
		Integer.valueOf(key.pmid));
	if (i != 0)
	    return i;
	i = Long.valueOf(sentenceNbr).compareTo(Long.valueOf(key.sentenceNbr));
	if (i != 0)
	    return i;
	return Integer.valueOf(firstSentPosTagged).compareTo(
		Integer.valueOf(key.firstSentPosTagged));
    }

    @Override
    public boolean equals(Object obj) {
	if (obj instanceof ConceptPosKey) {
	    ConceptPosKey key = (ConceptPosKey) obj;
	    return pmid == key.pmid
		    && sentenceNbr == key.sentenceNbr
		    && firstSentPosTagged == key.firstSentPosTagged;
	}
	return false;
    }

    @Override
    public int hashCode() {
	return Integer.valueOf(pmid).hashCode() + 17
		* Long.valueOf(sentenceNbr).hashCode() + 31
		* Integer.valueOf(firstSentPosTagged).hashCode();
    }

    public int getPmid() {
        return pmid;
    }

    public int getSentenceNbr() {
        return sentenceNbr;
    }

    public int getFirstSentPosTagged() {
        return firstSentPosTagged;
    }
    
}

/**
 * 
 */
package edu.mst.conceptrecogn.tag;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import edu.mst.db.lexicon.StopWords;
import edu.mst.db.util.JdbcConnectionPool;

/**
 * @author George
 *
 */
public class TaggerUtil {

    private static final String getCondProbSql = "SELECT lnCpNoStopWords, lnCpWithStopWords "
	    + "FROM sps.condprobbytype "
	    + "WHERE predicateBaseWordId = ? AND assumedFactBaseWordId = ? AND cogencyType = ?";

    private JdbcConnectionPool connPool;
    private StopWords stopWords;

    public TaggerUtil(JdbcConnectionPool connPool) throws Exception {
	this.connPool = connPool;
	stopWords = new StopWords(connPool);
    }

    /**
     * Method to retrieve log of conditional probability when number of words in
     * concept name is greater than one. Since a concept name may multiple
     * occurrences of the same word, the predicate base word and assumed fact
     * base word do not have to differ.
     * 
     * @param predicateBaseWordId
     * @param assumedFactWordId
     * @param cogencyType
     * @param excludeStopWords
     * @return
     * @throws Exception
     */
    public double getLnCondProb(int predicateBaseWordId, int assumedFactWordId,
	    String cogencyType, boolean excludeStopWords) throws Exception {

	double lnCondProb = 0.0d;

	// return zero value for stop words
	if (excludeStopWords && (stopWords.getLexWordIds()
		.contains(predicateBaseWordId)
		|| stopWords.getLexWordIds().contains(assumedFactWordId)))
	    return lnCondProb;

	try (Connection conn = connPool.getConnection()) {

	    try (PreparedStatement getCondProbQry = conn
		    .prepareStatement(getCondProbSql)) {

		getCondProbQry.setInt(1, predicateBaseWordId);
		getCondProbQry.setInt(2, assumedFactWordId);
		getCondProbQry.setString(3, cogencyType);

		try (ResultSet rs = getCondProbQry.executeQuery()) {
		    if (rs.next())
			if (excludeStopWords)
			    lnCondProb = rs.getDouble(2);
			else
			    lnCondProb = rs.getDouble(1);
		}
	    }
	}

	return lnCondProb;
    }

    /**
     * Method for retrieving the probability for single words. This is for
     * scenario where concept name consists of only one word.
     * 
     * @param baseWordId
     * @param cogencyType
     * @return
     * @throws Exception
     */
    public double getLnWordProb(int baseWordId, String cogencyType)
	    throws Exception {

	double lnOntolCogency = 0.0d;

	try (Connection conn = connPool.getConnection()) {

	    String getSymbolCogencySql = "SELECT lnProbability FROM sps.wordprobability "
		    + "WHERE baseWordId = ? AND cogencyType = ?";
	    try (PreparedStatement getSymbolCogencyQry = conn
		    .prepareStatement(getSymbolCogencySql)) {

		getSymbolCogencyQry.setInt(1, baseWordId);
		getSymbolCogencyQry.setString(2, cogencyType);

		try (ResultSet rs = getSymbolCogencyQry.executeQuery()) {
		    if (rs.next())
			lnOntolCogency = rs.getDouble(1);
		}
	    }
	}

	return lnOntolCogency;
    }

}

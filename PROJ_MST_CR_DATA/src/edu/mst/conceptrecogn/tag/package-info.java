/**
 *
 */
/**
 * Tag objects used when performing concept recognition.
 * 
 * @author gjs
 *
 */
package edu.mst.conceptrecogn.tag;
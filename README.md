Collection of utilities for importing, parsing, and generating data for concept recognition.

Source data is SNOMED-CT ontology, UMLS ontology, and corpora from the NLM database.

A variety of jar libraries also required, such as MetaMap, Stanford NLP toolkit, that are too large to upload into the GitHub repo. A list of these will be uploaded soon, but if not yet available, contact the author, George Shannon gjscnc@mst.edu, for details. 